
const navigationPlugin = require("@11ty/eleventy-navigation");
const syntaxHighlightPlugin = require("@11ty/eleventy-plugin-syntaxhighlight");

const markdownItAnchor = require("markdown-it-anchor")
const pluginTOC = require("eleventy-plugin-toc")

module.exports = function (config) {

  config.addPassthroughCopy({
    "./public/": "/",
    //"./content/**/*.{svg,webp,png,jpeg}": "/"
  });

  config.addPlugin(require("@11ty/eleventy-plugin-webc"));

  config.amendLibrary("md", mdLib => mdLib.use(require("markdown-it-anchor")))
  config.amendLibrary("md", mdLib => mdLib.use(require("markdown-it-attrs")))

 

  config.addPlugin(navigationPlugin);
  config.addPlugin(syntaxHighlightPlugin);
  config.addPlugin(pluginTOC);
  config.addPlugin(require("./plugins/shortcode.eleventy.js"))

  // Watch content images for the image pipeline.
  config.addWatchTarget("content/**/*.{svg,webp,png,jpeg}");

  return {
    // Control which files Eleventy will process
    // e.g.: *.md, *.njk, *.html, *.liquid
    templateFormats: [
      "md",
      "njk",
      "html",
      "liquid",
    ],

    // Pre-process *.md files with: (default: `liquid`)
    markdownTemplateEngine: "njk",

    // Pre-process *.html files with: (default: `liquid`)
    htmlTemplateEngine: "njk",

    // These are all optional:
    dir: {
      input: "content",          // default: "."
      includes: "../_includes",  // default: "_includes"
      data: "../_data",          // default: "_data"
      output: "_site"
    },

    pathPrefix: "/",
  };
};

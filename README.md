# Site

Aquest projecte genera el lloc web de [https://xtec.dev](https://xtec.dev).

La documentació del projecte està a [http://xtec.dev/site/edit/](http://xtec.dev/site/edit/)


```pwsh
>  .\start.ps1

> xtec.dev@0.1.0 start
> npx @11ty/eleventy --serve --quiet
...
[11ty] Server at http://localhost:8080/
```


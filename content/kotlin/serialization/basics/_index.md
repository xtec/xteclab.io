---
title: Fonaments
description: La serialització ens permet transformar unes dades en un "string".
mermaid: true
---

## Serialització


**More a HTTP**


## JSON

JSON és el format més utilitzat per intercanvi de dades a Internet, i és el que utilitzarem en aquestes activitats.

Perqué la teva aplicació es pugui comunicar amb altres serveis has de convertir els teus objectes a JSON, i consumir dades en format JSON.

### Encoding



Comencem amb una classe que descriu un projecte i intentem obtenir la seva representació JSON.

```kt
package dev.xtec.data

import kotlinx.serialization.*
import kotlinx.serialization.json.*

data class Project(val name: String, val language: String)

fun main () {
    val project = Project("Data","Kotlin")
    println(Json.encodeToString(project))
}
```

Quan executem aquest codi es produeix una excepció perquè les classes serializables s'han de marcar explícitament. 

```
Exception in thread "main" kotlinx.serialization.SerializationException: Serializer for class 'Project' is not found.
Please ensure that class is marked as '@Serializable' and that the serialization compiler plugin is applied.
```

La serialització de Kotlin no fa servir la reflexió de Java en temps d'execució perque és un procés lent, i més important encara, és un llenguatge multiplataforma que en molts casos no s'executa en una JVM.

Has d'afegir l'anotació `@Serializable` tal com ens indica el missatge d'error:

```kt
package dev.xtec.data

import kotlinx.serialization.*
import kotlinx.serialization.json.*

@Serializable
data class Project(val name: String, val language: String)

fun main () {
    val project = Project("Data","Kotlin")
    println(Json.encodeToString(project))
}
```

L'anotació `@Serializable` indica al plugin de serialització que ha de generar un serialitzador específic per aquesta classe quan es compila el programa. 

Ara si executes el codi tens una representació JSON del projecte:

```json
{"name":"Data","language":"Kotlin"}
```

Fixa't que en cap cas es guarda informació de la classe, en aquest cast `dev.xtec.data.Project`.

### Decoding



## Nom de la propietat

Per defecte, el procés de serialització utilitza els noms de les propietats dels objectes per codificar o descodificar dades.

Però molt cops aquests noms no es poden fer servir perquè les dades codificades són per enviar a un altre servei que utilitza noms diferents o al revés.

**I molt important!** Els serveis de tercers no han de dictar els noms que hem de fer servir.

Amb l'anotació [@SerialName](https://kotlinlang.org/api/kotlinx.serialization/kotlinx-serialization-core/kotlinx.serialization/-serial-name/index.html) podem indicar al procés de serialització que faci servir un altre nom enlloc del nom de la propietat.

Per exemple, enlloc de `language` potser has d'utlitzar el nom de `lang` per compatibilitat:

```kt
@Serializable
class Project(val name: String, @SerialName("lang") val language: String)

fun main() {
    val data = Project("dev.xtec.data", "Kotlin")
    println(Json.encodeToString(data))
}
```

Pots veure que ara el procés de serialitzado codifica el valor de `language` amb el nom de `lang`:

```json
{ "name": "dev.xtec.cat", "lang": "Kotlin" }
```

## Propietats transitòries

Una propietat es pot excloure de la serialització marcant-la amb l'anotació `@Transient`.

Com que aquesta propietat no es deserialitza, ha de tenir un valor predeterminat perquè el seu valor no es pot obtenir de les dades JSON. 


```kt
@Serializable
data class Project(val name: String, @Transient val language: String = "Kotlin")

fun main() {
    val data = Json.decodeFromString<Project>("""
        {"name":"kotlinx.serialization"}
    """)
    println(data)
}
```

Si executes aquest codi el resultat serà aquest:

```sh
Project(name=kotlinx.serialization, language=Kotlin)
```

Una propietat `@Transient` no és llegeix de les dades JSON, però és que tampoc es permet la seva presència en les dades JSON encara que el seu valor sigui el valor per defecte de la propietat.

```kt
@Serializable
data class Project(val name: String, @Transient val language: String = "Kotlin")

fun main() {
    val data = Json.decodeFromString<Project>("""
        {"name":"kotlinx.serialization","language":"Kotlin"}
    """)
    println(data)
}
```

Pots veure que les dades tenen un valor per la propietat `language`, que és identic al valor per defecte, i es produeix un error:

```sh
Exception in thread "main" kotlinx.serialization.json.internal.JsonDecodingException: Unexpected JSON token at offset 42: Encountered an unknown key 'language' at path: $.name
Use 'ignoreUnknownKeys = true' in 'Json {}' builder to ignore unknown keys.
```

### Valors per defecte

En principi, un objecte només es pot deserialitzar quan totes les seves propietats estan presents en l'string que s'ha de decodificar. 

Per exemple, si executes aquest codi:

```kt

@Serializable
data class Project(val name: String, val language: String)

fun main() {
    val data = Json.decodeFromString<Project>("""
        {"name":"kotlinx.serialization"}
    """)
    println(data)
}
```

Es produeix una excepció:

```sh
Exception in thread "main" kotlinx.serialization.MissingFieldException: Field 'language' is required for type with serial name 'example.exampleClasses04.Project', but it was missing at path: $
```

Kotlin permet propietats amb valor per defecte al crear un objecte en presència de valors nuls.

Aquest valor per defecte s'aplica tant quan és contrueix una instància amb codi com quan es deserialitza un objecte JSON.

```kt
@Serializable
data class Project(val name: String, val language: String = "Kotlin")

fun main() {
    val data = Json.decodeFromString<Project>("""
        {"name":"kotlinx.serialization"}
    """)
    println(data)
}
```

Com que hem afegt un valor per defecte a la propietat `language`, el nostre codi decodifica l'objecte JSON sense que es produeixi cap error, i la propietat `language` té el valor per defecte "Kotlin":

```sh
Project(name=kotlinx.serialization, language=Kotlin)
```

### Valor per defecte obligatori

Com hem explicat abans, Kotlin permet propietats amb valors per defecte, i això també s'aplica al procés de serialització.

Però que passa si només volem valors per defecte quan el nostre codi crea objectes, no pas quan es deserialitza un objecte?

Si volem que al decodificar l'objecte JSON ha de tenir el valor per defete si o si, pots utilitzar l'anotació `@Required`:

```kt
@Serializable
data class Project(val name: String, @Required val language: String = "Kotlin")

fun main() {
    val data = Json.decodeFromString<Project>("""
        {"name":"kotlinx.serialization"}
    """)
    println(data)
}
```

Ara durant l'execució del codi es produeix un error:

```sh
Exception in thread "main" kotlinx.serialization.MissingFieldException: Field 'language' is required for type with serial name 'example.exampleClasses07.Project', but it was missing at path: $
```

### Codificar els valors per defecte

En la configuració per defecte del format de codificació json, els valors per defecte no es codififiquen per estalviar espai:

```kt
@Serializable
data class Project(val name: String, val language: String = "Kotlin")

fun main() {
    val data = Project("kotlinx.serialization")
    println(Json.encodeToString(data))
}
```

Pots veure que l'objecte JSON no té la propietat `language` perqué el seu valor és igual al predeterminat:

```kt
{"name":"kotlinx.serialization"}
```

Si vols codificar els valors per defecte:

1. Pots modificar la configuració del format de cofificació tal com s'explica a TODO (enllaç JSON)

2. Utilitzar l'anotació [`EncodeDefault`](https://kotlinlang.org/api/kotlinx.serialization/kotlinx-serialization-core/kotlinx.serialization/-encode-default/index.html): 

```kt
@Serializable
data class Project(
    val name: String,
    @EncodeDefault val language: String = "Kotlin"
)
```

Aquesta anotació indica al framework que sempre ha de serialitzar la propietat, independentment del seu valor o la configuració del format de codificació. 

Si has mofificat la configuració del format de codificació, pots configurar el comportament contrari mitjançant el paràmetre [`EncodeDefault.Mode`](https://kotlinlang.org/api/kotlinx.serialization/kotlinx-serialization-core/kotlinx.serialization/-encode-default/-mode/index.html):

```kt
@Serializable
data class User(
    val name: String,
    @EncodeDefault(EncodeDefault.Mode.NEVER) val projects: List<Project> = emptyList()
)

fun main() {
    val alice = User("Alice", listOf(Project("kotlinx.serialization")))
    val bob = User("Bob")
    println(Json.encodeToString(alice))
    println(Json.encodeToString(bob))
}
```

Com pots veure, la propietat `language` es conserva i `projects` s'omet:

```sh
{"name":"Alice","projects":[{"name":"kotlinx.serialization","language":"Kotlin"}]}
{"name":"Bob"}
```

## Propietats anul·lables

Kotlin és un lleguatge segur perquè no permet valor nuls en les propietats d'un objecte a no ser que de manera explícita indiquis que un propietat pot tenir un valor nul.

Tal com hem vist abans, si intentes descodificar un valor `null` d'un objecte JSON en una propietat que no admet valors nuls:

```kt
@Serializable
data class Project(val name: String, val language: String = "Kotlin")

fun main() {
    val data = Json.decodeFromString<Project>("""
        {"name":"kotlinx.serialization","language":null}
    """)
    println(data)
}
```

Es produeix un error encara que la propietat `language` tingui un valor predeterminat:

```sh
Exception in thread "main" kotlinx.serialization.json.internal.JsonDecodingException: Unexpected JSON token at offset 52: Expected string literal but 'null' literal was found at path: $.language
Use 'coerceInputValues = true' in 'Json {}' builder to coerce nulls if property has a default value.
```

Si vols que una propietat admeti valor nuls la pots marcar com anul.lable amb `?`.

Les propietats anul·lables són compatibles de manera nativa amb la serialització de Kotlin.

A continuació tens un exemple amb la propietat `renamedTo`:

````kt
@Serializable
class Project(val name: String, val renamedTo: String? = null)

fun main() {
    val data = Project("kotlinx.serialization")
    println(Json.encodeToString(data))
}
```

Aquest exemple no codifica la propietat `renamedTo` amb el valor `null` perqué per defecte el valors nuls no es codifiquen:

```sh
{"name":"kotlinx.serialization"}
```

## Constructor

En Kotlin només pot existir un constructor primari i la validació de les dades es fa en un bloc específic.

### Validació de dades

Si vols validar les dades durant el procés de deserialització has de crear un block `init { ... }` on s'executarà la validació:

```kt
@Serializable
class Project(val name: String) {
    init {
        require(name.isNotEmpty()) { "name cannot be empty" }
    }
}
```

El procés de deserialització funciona com un constructor normal i executa tots els blocs `init`, de tal manera que no es pot instanciar una classe que no sigui valida.

Anem a provar-ho:

```kt
fun main() {
    val data = Json.decodeFromString<Project>("""
        {"name":""}
    """)
    println(data)
}
```

L'execució d'aquest codi produeix una excepció:

```sh
Exception in thread "main" java.lang.IllegalArgumentException: name cannot be empty
```

### Propietats del constructor

Encara que Kotlin és molt estricte respecte com es construeix un objecte i això facilita el procés de serialització, el plugin se serialització encara és més estricte.

Si anotates una classe `@Serializable` tots els paràmetres del constructor primari de la classe han de ser propietats.

Normalment això no és un problema.

Però que passa si vols definir la classe `Project` de tal manera que el constructor requereixi un paràmetre `path` de tipus String, i que després el descontrueixi amb les propietats corresponents: 

```kt
@Serializable
class Project(path: String) {
    val owner: String = path.substringBefore('/')
    val name: String = path.substringAfter('/')
}

fun main() {
    println(Json.encodeToString(Project("xtec/data")))
}
```

Es produeix un error com era de preveure:

```
TODO error
``` 

En aquest casos has de:

1. Definir un constructor primari privat amb les propietats de la classe.

2. Convertir el constructor que volies en secundari.

```kt
@Serializable
class Project private constructor(val owner: String, val name: String) {
    constructor(path: String) : this(
        owner = path.substringBefore('/'),
        name = path.substringAfter('/')
    )

    val path: String
        get() = "$owner/$name"
}

fun main() {
    println(Json.encodeToString(Project("xtec/data")))
}
```

Ara el compilador no dona error, i si executes el codi la sortida és l'esperada:

```
{"owner":"xtec","name":"data"}
```

## Referències

Fins ara només hem treballat amb objectes en que les totes les seves propietats són valors primaris.

Però molts objectes tenen propietats que són objectes complexes.

Si el tipus de la propietat és una classe amb l'anotació `@Serializable` no hi ha cap problema.

```kt
@Serializable
class Project(val name: String, val owner: User)

@Serializable
class User(val name: String)

fun main() {
    val owner = User("kotlin")
    val data = Project("kotlinx.serialization", owner)
    println(Json.encodeToString(data))
}
```

Quan es codifica en JSON, el resultat és un objecte JSON imbricat.

```sh
{"name":"kotlinx.serialization","owner":{"name":"kotlin"}}
````

Però que passa si la classe no està marcada amb l'anotació `@Serializable?

Si la propietat és transitoria, està marcada amb l'anotació `@Transient` tot funciona correctament perquè la propietat s'ignora.

En els altres casos, casi tots, has d'indicar de manera específica el serialitzador que s'ha de fer servir per la propietat concreta tal com s'explica a TODO(link serailizers). 



## Propietat genèrica

Un classe genèrica pot tenir propietats genèriques.

Per exemple, considera una classe serialitzable genèrica `Box<T>`:

```kt
@Serializable
class Box<T>(val contents: T)
```

La classe `Box<T>` es pot utilitzar:

1. Amb tipus predefinits com `Int`
2. Amb tipus que has definit tu mateix, com és `Project`:

```kt
@Serializable
class Data(
    val a: Box<Int>,
    val b: Box<Project>
)

fun main() {
    val data = Data(Box(42), Box(Project("dev.xtec.data", "Kotlin")))
    println(Json.encodeToString(data))
}
```

Pots veure que el procés de serialització no té problemes amb un paràmetre genèric sempre que el seu tipus sigui serialitzable:

```json
{
  "a": {"contents":42},
  "b": {"contents":{
    "name":"dev.xtec.data",
    "language":"Kotlin"}}}
````

En canvi, si el tipus real no es pot serialitzar, es produirà un error en temps de compilació.

**TODO** exemple


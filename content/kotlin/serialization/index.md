---
title: Serialització
description: La serialització consiteix en convertir un arbre d'objectes en un string o en una seqüència de bytes.
---

La serialització de dades no és tant fàcil com pot semblar al principi, perquè la majoria de les dades que consumim o produim no s'ajusten exactament a la definició de les nostres classes.

{% pages ["basics/","builtin-classes/", "serializers/", "polymorphism/", "json/"] %}

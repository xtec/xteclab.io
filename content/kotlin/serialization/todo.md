---
title: TODO
---

* [Releases](https://github.com/Kotlin/kotlinx.serialization/releases)
* [kotlinx.serialization 1.0 released](https://blog.jetbrains.com/kotlin/2020/10/kotlinx-serialization-1-0-released/)
* [kotlinx.serialization 1.3 Released: Experimental IO Stream-Based JSON Serialization, Fine-Grained Defaults Control, and More](https://blog.jetbrains.com/kotlin/2021/09/kotlinx-serialization-1-3-released/)
* [kotlinx.serialization 1.2 Released: High-Speed JSON Handling, Value Class Support, Overhauled Docs, and more](https://blog.jetbrains.com/kotlin/2021/05/kotlinx-serialization-1-2-released/)

* [Kotlin Serialization: A Deep Dive](https://dopebase.com/kotlin-serialization-deep-dive)
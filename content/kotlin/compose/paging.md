---
title: Paging
description: The Paging library helps you load and display pages of data from a larger dataset from local storage or over network.
mermaid: true
---


* <https://developer.android.com/codelabs/android-paging-basics>

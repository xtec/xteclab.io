---
title: Compose
icon: compose.png
description: Compose te permite definir la interfaz gráfica mediante funciones descriptivas.
---

### Fundamentos

{% pages ["composition/", "recomposition/", "navigation/", "effects/"] %}

### Acceso a datos

{% pages ["mongodb/"] %}


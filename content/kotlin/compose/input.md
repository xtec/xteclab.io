---
title: Input
---

## TextField

`TextField` és un control d'interfície d'usuari que s'utilitza per permetre que l'usuari introdueixi el text.

El `TextField` és la forma més senzilla d'entrada de text a Compose. Inclou funcions essencials com etiquetes, marcadors de posició i estil bàsic amb modificadors.

```kt
@Composable
fun BasicTextFieldExample() {
    var text by remember { mutableStateOf("") }

    TextField(
        value = text.value,
        onValueChange = { text = it },
        label = { Text("Enter your name") },
        placeholder = { Text("John Doe") },
        singleLine = true,
        modifier = Modifier.fillMaxWidth()
    )
}
```

* <https://www.jetpackcompose.net/textfield-in-jetpack-compose>
* <https://medium.com/@ramadan123sayed/comprehensive-guide-to-textfields-in-jetpack-compose-f009c4868c54>

## TODO


---
title: Recursos
---


## Introducció

Compose Multiplatform provides a special library and Gradle plugin support for accessing resources in common code across all supported platforms. Resources are static content, such as images, fonts, and strings, which you can use in your application.

...

## composeResources

Els recursos s'han de posar en el directori `composeApp/src/commonMain/composeResources`.

Organize the `composeResources` directory structure according to these rules:

1. Images should be in the `drawable` directory.

2. Fonts should be in the `font` directory.

3. Strings should be in the `values` directory.

4. Other files should be in the `files` directory, with any folder hierarchy you may find appropriate.


When you've set up the resources for your project, build the project to generate the special `Res` class which provides access to resources. To regenerate the `Res` class and all the resource accessors, build the project again or re-import the project in the IDE.

After that, you can use the generated class to access the configured multiplatform resources from your code or from external libraries.


## TODO

* <https://www.jetbrains.com/help/kotlin-multiplatform-dev/compose-multiplatform-resources.html#set-up-a-project-with-multiplatform-resources>
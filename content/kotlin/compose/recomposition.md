---
title: Recomposición
icon: compose.png
---

## Introducción


Modelo de interfaz de usuario declarativo que regenera toda la pantalla desde cero y luego aplica solo los cambios necesarios. Este enfoque evita la complejidad de actualizar manualmente una jerarquía de vistas con estado. Compose es un marco de interfaz de usuario declarativo.

Uno de los desafíos de regenerar toda la pantalla es que es potencialmente costoso, en términos de tiempo, potencia informática y uso de la batería. Para mitigar este costo, Compose elige de manera inteligente qué partes de la interfaz de usuario deben volver a dibujarse en un momento dado. Esto tiene algunas implicaciones en la forma en que diseña sus componentes de interfaz de usuario.
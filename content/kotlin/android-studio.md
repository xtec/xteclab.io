---
title: Android Studio
---

## Introducció

Instal.la [Android Studio](https://developer.android.com/studio) amb {% link "/tool/scoop/" %}.

```pwsh
> scoop install android-studio android-clt
```



## Install the SDK

Within Android Studio, you can install the Android 14 SDK as follows:

1. Click Tools > SDK Manager, then click Show Package Details.
2. In the SDK Platforms tab, expand the Android 14.0 ("UpsideDownCake") section and select the Android SDK Platform 34 package.
3. In the SDK Tools tab, expand the Android SDK Build-Tools 34 section and select the latest 34.x.x version.
4. Click Apply > OK to download and install the selected packages.

---
title: Migration
description: Una aplicació que funciona genera noves versions, i de la mateixa manera que s'afegeixen i es modifiquen funcionalitats, l'esquema i les dades de les aplicacions antigues que els usuaris fan servir s'han d'adaptar a la nova versió.
---

## Migració

Per exemple, la nova versió de la nostra aplicació a més del nom del gos, vol conèixer la seva data de naixement.

El primer que has de fer és modificar el fitxer `Dog.sq`, que **sempre** descriu la versió més actualitzada de la base de dades.

Per exemple, volem afegir la data de naixement del gos:

```sql
CREATE TABLE IF NOT EXISTS Dog (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    birth TEXT
);
```

{% panel %}
SQLite does not have a storage class set aside for storing dates and/or times. Instead, the built-in [Date And Time Functions](https://www.sqlite.org/lang_datefunc.html) of SQLite are capable of storing dates and times as TEXT, REAL, or INTEGER values:

* `TEXT` as ISO8601 strings ("YYYY-MM-DD HH:MM:SS.SSS").
* `REAL` as Julian day numbers, the number of days since noon in Greenwich on November 24, 4714 B.C. according to the proleptic Gregorian calendar.
* `INTEGER` as Unix Time, the number of seconds since 1970-01-01 00:00:00 UTC. 

Applications can choose to store dates and times in any of these formats and freely convert between formats using the [built-in date and time functions](https://www.sqlite.org/lang_datefunc.html).

{% endpanel %}

A continuació has de crear un fitxer `1.sqm` en el directori `sqldelight`:

```pwsh
src
└─ main
   └─ sqdelight
      ├─ sql/data
      |  └─ Dog.sq
      └─ migrations
         └─ 1.sqm
```

Aquest fitxer té les sentències SQL per actualitzar un taula antiga:

```sql
ALTER TABLE Dog ADD COLUMN birth TEXT;
```

TODO 

<https://sqldelight.github.io/sqldelight/2.0.2/jvm_sqlite/migrations/>


---
title: Supabase
---

## Introducció

[Supabase](https://supabase.com/) ...


You can use supabase-kt to interact with your Postgres database, listen to database changes, invoke Deno Edge Functions, build login and user management functionality, and manage large files.

## Base de dades

How you connect to your database depends on where you're connecting from:

* For frontend applications, use the Data API
* For Postgres clients, use a connection string


The direct connection string connects directly to your Postgres instance.

The connection string looks like this:

```pg
postgresql://postgres:[YOUR-PASSWORD]@db.apbkobhfnmcqqzqeeqss.supabase.co:5432/postgres
```

Get your project's direct string from the [Database Settings](https://supabase.com/dashboard/project/_/settings/database) page:

* Go to the `Settings` section.
* Click `Database`.
* Under `Connection string`, make sure `Display connection pooler` is unchecked. Copy the URI.


## Entorn de treball


```pwsh
> gradle init --package dev.xtec --project-name supabase --java-version 21 --type kotlin-application --dsl kotlin --test-framework kotlintest --no-split-project --no-incubating --overwrite
```

Configura el fitxer `build.gradle.kts`:

```kt
plugins {
    kotlin("plugin.serialization") version "2.0.0"
    // ...
}

dependencies {
    implementation("io.github.cdimascio:dotenv-kotlin:6.5.0")
    implementation(platform("io.github.jan-tennert.supabase:bom:3.0.3"))
    implementation("io.github.jan-tennert.supabase:postgrest-kt")
    implementation("io.github.jan-tennert.supabase:auth-kt")
    implementation("io.github.jan-tennert.supabase:realtime-kt")
    implementation("io.ktor:ktor-client-cio:3.0.3")
    // ...
```

Crea un fitxer `app/src/main/resources/.env/` per guardar ...

```env
URL = ...
API = ...
```

Modifica el fitxer `App.kt`:


```kt
import io.github.cdimascio.dotenv.dotenv
import io.github.jan.supabase.auth.Auth
import io.github.jan.supabase.createSupabaseClient
import io.github.jan.supabase.postgrest.Postgrest

fun main() {

    val dotenv = dotenv()

    val supabase = createSupabaseClient(
        supabaseUrl = dotenv["URL"],
        supabaseKey = dotenv["KEY"]
    ) {
        install(Auth)
        install(Postgrest)
        //install(Storage)
    }
}
```

Verifica que funciona


## Base de dades 

Crea una taula nova amb el SQL Editor:

{% image "sql-editor.png" %}


```sql
create table if not exists dog (
  id serial primary key,
  name text not null
)
```

Tens més informació a [Managing tables views and data](https://supabase.com/docs/guides/database/tables)

Crear les classes `Dog` i `DogInsert`:

```kt
@Serializable
data class Dog(
    val id: Long,
    val name: String
)

@Serializable
data class DogInsert(
    val name: String
)
```

Necessites la classe `DogInsert` perquè l'atribut `id` és una clau subrogada.

Ja pots inserir el primer gos:

```kt
fun main() {
    // ...

    runBlocking {

        val pg = supabase.postgrest

        pg.from("dog").insert(DogInsert("Trufa"))
    }
}
```

Executa el codi i verifica al "Table Editor" que s'ha inserit el nou registre:

{% image "table-editor-dog.png" %}

Si vols retornar les dades inserides, pots utilitzar el mètode `select()` dins de la sol·licitud:

```kt
fun main() {
    // ...

    runBlocking {

        val pg = supabase.postgrest

        pg.from("dog").insert(DogInsert("Ketsu")) {
            select()
        }.decodeSingle<Dog>().also { println(it) }

        // Dog(id=2, name=Ketsu)
    }
}
```

També pots inserir diversos gossos a la vegada:

```kt
fun main() {
    // ...

    runBlocking {

        val pg = supabase.postgrest

        pg.from("dog").insert(
            listOf(DogInsert("Lassie"), DogInsert("Maverick"))
        ) {
            select()
        }.decodeList<Dog>().onEach { println(it) }

        // Dog(id=3, name=Lassie)
        // Dog(id=4, name=Maverick)
    }
}
```


## Direct connection string

https://supabase.com/docs/guides/database/connecting-to-postgres#direct-connections



## TODO


* <https://supabase.com/docs/guides/database/tables?queryGroups=language&language=kotlin>
Exemple: <https://github.com/supabase/supabase/tree/master/examples/product-sample-supabase-kt>

* <https://medium.com/@cyri113/part-1-configuring-and-testing-a-supabase-database-table-and-policies-be677be03ae>

* [Kotlin Client Library](https://supabase.com/docs/reference/kotlin/introduction)
* <https://github.com/supabase-community/supabase-kt>
* <https://supabase.com/docs/guides/getting-started/tutorials/with-kotlin>
* <https://www.restack.io/docs/supabase-knowledge-supabase-kotlin-integration>
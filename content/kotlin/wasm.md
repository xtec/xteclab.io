---
title: Wasm
description: Kotlin/Wasm has the power to compile your Kotlin code into WebAssembly (Wasm) format. 
---

## Introduction

<https://kotlinlang.org/docs/wasm-overview.html>

[Explore our online demo of an application built with Compose Multiplatform and Kotlin/Wasm](https://zal.im/wasm/jetsnack/)


## TODO

* <https://github.com/Kotlin/kotlin-wasm-examples>
* <https://touchlab.co/kotlin-wasm-getting-started>
---
title: HTTP
---

## Introducció

L'objectiu de la serialització és:

1. Consumir un objecte Json d'una API externa, i convertir-lo en un objecte Kotlin.

2. Produir un objecte Json a partir d'una classe Kotlin, i enviar-lo a una API externa.

{% image "android-post-json.png" %}
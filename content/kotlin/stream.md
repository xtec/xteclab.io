---
title: Kotlin - Stream
---

## TODO

* [File Read/Write with Kotlin IO](https://medium.com/@sujitpanda/file-read-write-with-kotlin-io-31eff564dfe3)
* [Read From Files using InputReader in Kotlin](https://www.geeksforgeeks.org/read-from-files-using-inputreader-in-kotlin/)
* [Kotlin Basic Input/Output](https://www.programiz.com/kotlin-programming/input-output)
* [I/O With Kotlin](https://kotlin-quick-reference.com/050-R-input-output-io.html)
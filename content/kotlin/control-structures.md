---
title: Estructures de control
---

## If

En aquest tema, explorarem les expressions condicionals de Kotlin , centrant-nos en les diverses formes de l' ifexpressió i el seu ús com a construcció d'estil d'expressió.

### Teoria

#### Expressions condicionals

Les expressions condicionals permeten a un programa realitzar diferents càlculs basats en el valor d'una expressió booleana . Hi ha diferents formes d'expressions condicionals, com ara el cas únic si , els casos si-else , els casos si-else-if i els imbricats if 's.

#### Estil d'expressió "if"

A diferència del cas d'altres llenguatges de programació, el de Kotlin ifés una expressió, no una declaració . Això significa que pot retornar un valor com a resultat dels càlculs. El resultat ha de ser l'última expressió del cos. Per exemple:

```kt
val max = if (a > b) {
    println("Choose a")
    a
} else {
    println("Choose b")
    b
}
```

A l'exemple anterior, a la variable maxse li assigna el valor de l'última expressió del cos. És important tenir en compte que si utilitzeu un expression-style if, ha de tenir una elsebranca.

Podeu ometre claus si tots els cossos només contenen una sola declaració:

```kt
val max = if (a > b) a else b
```

Hi ha casos en què no cal declarar una nova variable per emmagatzemar un resultat. Per exemple, considereu l'exemple següent:

```kt
fun main() {
    val a = readln().toInt()
    val b = readln().toInt()

    println(if (a == b) {
        "a equal b"
    } else if (a > b) {
        "a is greater than b"
    } else {
        "a is less than b"
    })
}
```

A l'exemple anterior, l' ifexpressió es passa directament a la println()funció sense declarar una variable i println()imprimeix el resultat.

#### Idioma

Preferiu utilitzar l'estil d'expressió ifquan necessiteu obtenir resultats, ja que pot ajudar a evitar problemes potencials causats per variables mutables o canvis oblidats. Per exemple:

```kt
val max = if (a > b) a else b // one line
```

En resum, ifl'expressió de Kotlin proporciona una manera potent i flexible de gestionar l'execució condicional del codi. En utilitzar expression-style if, podeu aprofitar la seva capacitat per retornar valors i simplificar el vostre codi. Si coneixeu Java, podeu assignar-lo a un operador ternari:

```kt
final String msg = num > 10 
  ? "Number is greater than 10" 
  : "Number is less than or equal to 10";
```

Utilitzant "quan" com una alternativa més potent a les cadenes "si-else-if".

Kotlin proporciona una alternativa més potent i expressiva a les cadenes if-else-if anomenades whenexpressió. L' whenexpressió simplifica el procés de gestió de múltiples condicions i fa que el codi sigui més llegible.

Aquí teniu un exemple de com utilitzar -lo when:

```kt
val number = 5

when (number) {
    1 -> println("One")
    2 -> println("Two")
    3 -> println("Three")
    4 -> println("Four")
    else -> println("Number is greater than four")
}
```

En aquest exemple, l' whenexpressió comprova el valor de la numbervariable amb diferents condicions. Si el valor coincideix amb una de les condicions, s'executarà el bloc de codi corresponent.

#### Utilitzar "when" com a expressió

Similar a l' ifexpressió, whentambé es pot utilitzar com a expressió que retorna un valor. El valor que retorna l' whenexpressió és el resultat de l'última expressió de la branca coincident.

```kt
val number = 3
val message = when (number) {
    1 -> "One"
    2 -> "Two"
    3 -> "Three"
    4 -> "Four"
    else -> "Number is greater than four"
}

println(message) // Output: Three
```

#### Utilitzant "quan" amb intervals i condicions

when les expressions també es poden utilitzar amb intervals i condicions més complexes. Aquí teniu un exemple:

```kt
val number = 15

when {
    number < 0 -> println("Negative number")
    number in 1..10 -> println("Number between 1 and 10")
    number % 2 == 0 -> println("Even number")
    else -> println("Odd number greater than 10")
}
```

En aquest exemple, l' whenexpressió comprova el valor de numberamb diferents condicions, inclosos els intervals i les condicions personalitzades.

### Activitats

**1.-** Quin és el resultat d'aquest codi?

```kt
println(if (true) if (false) { "A" } else "B")
```

1. true
2. false
3. No es compilarà a causa del primer if.
4. No es compilarà a causa del segon if.

{% sol %}
3. No es compilarà a causa del primer if.
{% endsol %}

**2.-**  Rebreu un programa que compara dos nombres enters "númeroA" i "númeroB". La vostra tasca és omplir els espais en blanc del codi per imprimir el nombre que sigui més gran. Utilitzeu una expressió "si" per comparar els dos números i plantilles de cadena per mostrar el resultat. Assegureu-vos que la vostra solució utilitzi operadors relacionals per a la comparació.

```kt
fun main() {
    val numberA = 10
    val numberB = 20
    val result = ____ (numberA > numberB) "Number A ($numberA) is greater than Number B ($numberB)" 
                 ____ "Number B ($numberB) is greater than Number A ($numberA)"
    ____(result)
}
```

{% sol %}
```kt
fun main() {
    val numberA = 10
    val numberB = 20
    val result = if (numberA > numberB) "Number A ($numberA) is greater than Number B ($numberB)" 
                 else "Number B ($numberB) is greater than Number A ($numberA)"
    println(result)
}
```
{% endsol %}

**3.-** Escriu un programa que utilitzi if per trobar el màxim de dos nombres enters. Aquests nombres poden ser positius, negatius o zero.

Sigues creatiu, resol el problema sense elsebranques :)

Utilitzeu la plantilla de codi proporcionada, imprimiu el màxim.

Exemple: 8,11 -> 11

```kt
fun main() {

    val a = readln().toInt()
    val b = readln().toInt()

    val result = if (a > b) a else b
    println(result)
}
```

**4.-** Teniu una funció de Kotlin 'checkNumberRelations' que pren dos nombres enters com a entrada i retorna un missatge de cadena que compara les entrades. Tanmateix, falten algunes parts essencials d'aquesta funció. La vostra tasca és completar aquesta funció substituint els punts en blanc (marcats amb ▭) perquè funcioni correctament. La funció hauria de retornar "El número a és més gran que b". si a és major que b, "El nombre a és menor que b". si a és menor que b i "Els nombres a i b són iguals". si a i b són iguals".

```kt
fun checkNumberRelations(a: Int, b: Int): String {
    ____ (a > b) {
        ____ "The number $a is larger than $b."
    } ____ if (a < b) {
        return "The number $a is less than $b."
    } else {
        return "The numbers $a and $b are equal."
    }
}
```

{% sol %}
```kt
fun checkNumberRelations(a: Int, b: Int): String {
    if (a > b) {
        return "The number $a is larger than $b."
    } else if (a < b) {
        return "The number $a is less than $b."
    } else {
        return "The numbers $a and $b are equal."
    }
}
```
{% endsol %}

**5.-** Ann va veure un programa de televisió sobre salut i va aprendre que almenys havia de dormir A
hores al dia, però dormir en excés tampoc no és saludable i no hauríeu de dormir més B
hores. Ara l'Anna dorm H hores al dia. Si l'horari de son de l'Anna compleix els requisits d'aquest programa de televisió, imprimiu "Normal". Si l'Anna dorm menys A hores, sortida "Deficiency", si dorm més de B hores, sortida "Excess".

L'entrada d'aquest programa són els tres strings amb variables en l'ordre següent: A, B, H.
A sempre és menor o igual a B

Exemple: 6,10,8 -> Normal; 7,9,10 -> Excess; 7,9,2 -> Deficiency

```kt
fun main() {    
    
}
```

**6.-** Escriu un programa que llegeixi un nombre enter i comprovi si és divisible per 2, 3, 5 o 6.

Si el nombre és divisible per M, el programa hauria de sortir "Divided by M" .

El programa hauria de comprovar tots els divisors enumerats anteriorment. L'ordre dels divisors en el resultat pot ser qualsevol.

Consell: utilitzeu l'operador mòdul % per comprovar si un nombre es divideix per un altre.

Exemple: 6 -> Divided by 2, Divided by 3, Divided by 6

{% sol %}
```kt
fun main(){
    val n = readln().toInt()

    for (i in 2..n) {
        if (n % i == 0)
            println("Divided by ${i}")
    }
}
```
{% endsol %}

**7**.- Un any és un any de traspàs?

Un any de traspàs, any bixest o any bissextil és un any civil que té un dia més que els anys comuns, és a dir, té 366 dies.

Només un recordatori: els anys de traspàs són els anys que són divisibles per 4 i no divisibles per 100, o divisibles per 400 (per exemple, l'any 2000 és un any de traspàs, però l'any 2100 no).

El programa hauria de funcionar durant els anys 1900 ≤ n ≤ 3000.

Escriviu "Leap" (distingeix entre majúscules i minúscules) si l'any és de traspàs, en cas contrari imprimiu "Regular".

Exemple: 2100 -> Regular; 2000 -> Leap

{% sol %}
```kt

```
{% endsol %}
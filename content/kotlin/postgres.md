---
title: Postgres
---

## Introducción

Para esta actividad tienes este proyecto de ayuda: <https://gitlab.com/xtec/kotlin/postgres/>

## Entorno de trabajo

Crea un proyecto:

```pwsh
> mkdir postgres
> cd postgres
> gradle init --package dev.xtec --project-name postgres --java-version 21 --type kotlin-application --dsl kotlin --test-framework kotlintest --no-split-project --no-incubating --overwrite
```

Modifica el fichero `app/build.gradle.kts`:

```kt
plugins {
    id("app.cash.sqldelight") version "2.0.2"
    // ...
}

dependencies {
    implementation("app.cash.sqldelight:jdbc-driver:2.0.2")
    implementation("com.zaxxer:HikariCP:6.2.1")
    implementation("io.github.cdimascio:dotenv-kotlin:6.5.0")
    implementation("org.postgresql:postgresql:42.5.0")
    implementation("org.slf4j:slf4j-reload4j:2.0.16")
    // ...
}

sqldelight {
    databases {
        create("Database") {
            packageName.set("dev.xtec.data")
            dialect("app.cash.sqldelight:sqlite-3-38-dialect:2.0.2")
        }
    }
}
```

Añade un fichero `log4j.properties` en el directorio `app/src/main/resources` para configurar slf4j:

```properties
# Root logger option
log4j.rootLogger=INFO, stdout
# Direct log messages to stdout
log4j.appender.stdout=org.apache.log4j.ConsoleAppender
log4j.appender.stdout.Target=System.out
log4j.appender.stdout.layout=org.apache.log4j.PatternLayout
log4j.appender.stdout.layout.ConversionPattern=%d{yyyy-MM-dd HH:mm:ss} %-5p %c{1}:%L - %m%n
```

### Configura el esquema

Crea el directorio `app/src/main/sqldelight/dev/xtec/data` para poner los ficheros `.sq`

Añade un fichero `Customer.sq`:

```sql
CREATE TABLE IF NOT EXISTS customer (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    email TEXT
);

insert:
INSERT INTO customer(name) VALUES (?);

select:
SELECT * FROM customer;
```

Genera el código de acceso a la base de datos:

```pwsh
>  .\gradlew generateSqlInterface 
```

Crea el fichero `app/src/main/resources/.env` para guardar los datos de configuración de la base de datos (este fichero no tiene que estar versionado):

```env
PG_JDBC_URL = jdbc:postgresql://localhost:5432/acme
PG_USERNAME = postgres
PG_PASSWORD = password
```

Modifica el fichero `app/src/main/kotlin/dev/xtec/App.kt`:

```kt
package dev.xtec

import app.cash.sqldelight.db.SqlDriver
import app.cash.sqldelight.driver.jdbc.asJdbcDriver
import com.zaxxer.hikari.HikariDataSource
import dev.xtec.data.Database
import io.github.cdimascio.dotenv.dotenv

fun main() {
   
    val dotenv = dotenv()

    val driver: SqlDriver = HikariDataSource().apply {
        jdbcUrl = dotenv["PG_JDBC_URL"]
        driverClassName = "org.postgresql.Driver"
        username = dotenv["PG_USERNAME"]
        password = dotenv["PG_PASSWORD"]
    }.asJdbcDriver()

    val database = Database(driver)
    Database.Schema.create(driver)

    database.customerQueries.insert("Laura")

    val customer = database.customerQueries.select().executeAsList().first()
    println(customer)

}
```

### Base de datos

Arranca una base de datos tal com se explica en {% link "/data/postgres/" %}.

Crea una base de datos con el nombre de `acme`.

Ejecuta la aplicación:

```pwsh
> .\gradlew run                                                                           
...

Customer(id=1, name=Laura, email=null)
```

## Neon

Crea una base de datos en {% link "/data/postgres/neon/" %}.

A continuación modifica el fichero `.venv` con los datos de conexión de neon.

Por ejemplo:

```properties
PG_JDBC_URL = jdbc:postgresql://ep-ancient-recipe-a9qwtikr.gwc.azure.neon.tech/sales
PG_USERNAME = sales_owner
PG_PASSWORD = npg_pnZrJxz87UiC
```

Verifica que el codigo funciona sin modificar ninguna linea de código.





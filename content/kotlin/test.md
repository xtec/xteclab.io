---
title: Test
description: Les proves unitàries són petites proves aïllades que comproven si un mètode, classe, funcionalitat o component implementa correctament la seva lògica empresarial.
---

## Introducció



## TODO

* <https://stackoverflow.com/questions/35554076/how-do-i-manage-unit-test-resources-in-kotlin-such-as-starting-stopping-a-datab>
* <https://kotlinlang.org/docs/power-assert.html>
* <https://www.baeldung.com/kotlin/kotest-before-test>
* <https://www.lambdatest.com/learning-hub/kotlin-unit-testing>
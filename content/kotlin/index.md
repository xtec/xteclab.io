---
title: Kotlin
icon: kotlin.png
description: Kotlin és un llenguatge de programació de propòsit general, multiplataforma i multiparadigma.
---

## Introducció

[Kotlin](https://kotlinlang.org/) és un llenguatge que es pot utilitzar en una varietat de plataformes d'aplicacions, com ara JVM (Java Virtual Machine), Android, JavaScript i Native.

### Fonaments

{% pages ["computation/", "sequence", "data/", "function/"] %}

### Input/Output

{% pages ["p:serialization/basics/","file/"] %}

### Desenvolupament

{% pages ["android-studio/", "gradle/", "idea/"] %}

### Altres

{% pages ["extension-function/"] %}

{% pages ["thread/", "coroutines/"] %}

### Dades

{% pages ["postgres/","compose/", "sqldelight/", "supabase/", "mongodb/"] %}

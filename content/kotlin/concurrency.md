---
title: Concurrency
---

## Tasques simultànies

8544

Sovint, les aplicacions necessiten fer diverses tasques al mateix temps. Per exemple, quan el vostre navegador està carregant la pàgina Hyperskill, pot mostrar una icona animada de "carregant" per a la pestanya. Com ho fas? En aquest punt, probablement entengueu el significat de concurrència i paral·lelisme . En aquest tema, veurem com Kotlin els dóna suport. Amb aquest coneixement com a base, eventualment podreu executar la vostra pròpia aplicació Kotlin que gestioni múltiples tasques simultàniament!

### Processos

En els ordinadors, les entitats més grans que es poden executar al mateix temps s'anomenen processos , que solen representar diferents aplicacions. Tanmateix, els desenvolupadors de Kotlin rarament treballen amb processos perquè són bastant pesats i, normalment, els programadors volen que les seves aplicacions siguin més compactes. Per aquest motiu, no els tractarem aquí amb detall; si voleu saber com treballar-hi, investigueu-lo vosaltres mateixos, per exemple, a Stack overflow .

Els processos són relativament pesats. Un ordinador modern pot gestionar centenars de processos.

Si voleu una metàfora, penseu en els processos com a empreses (empreses) d'un país. Alguns d'ells treballen en camps totalment diferents, d'altres competeixen en el mateix camp. Quan parles tant de processos com d'empreses, normalment et refereixes al que fan en general. Tanmateix, en realitat són grans, complexos i tenen molts matisos.

exemple de metàfora exemple de creació de metàfores

### Fils

Una manera més còmoda d'executar tasques simultànies en un sol programa és el fil . És compatible amb gairebé tots els llenguatges de programació, inclòs Kotlin.

Cada fil està lligat a un procés i un procés pot crear els seus fils. Per exemple, les aplicacions sovint utilitzen un fil especial per dibuixar gràfics a la pantalla, mentre que la lògica del programa s'executa en un altre. Els fils comparteixen una mica de memòria, de manera que la comunicació entre ells és relativament senzilla.

Els fils són més lleugers que els processos, i avui un ordinador casual en pot executar desenes de milers .

Un fil d'un procés és com un empleat d'una empresa. Una persona i un fil són responsables del seu propi paper.

comunicació entre fils exemple de multiprocés

A Kotlin, els fils depenen de la plataforma, el que significa que heu d'utilitzar fils diferents per a diferents entorns, per exemple, fils Java per a Kotlin/JVM i fils Posix per a Kotlin/Native. Tot i així, els fils s'utilitzen amb freqüència perquè és fàcil definir la seva comunicació i executar-los.

Tanmateix, si teniu un lloc web popular i un servidor web per allotjar-lo, potser no n'hi ha prou: el nombre de connexions simultànies al vostre lloc pot superar fàcilment les desenes de milers.

### Corutines

Una altra manera de donar suport al processament de tasques múltiples s'anomena concurrència interna . A Kotlin, la manera oficial i més popular de fer-ho és la biblioteca de coroutines ( kotlinx.coroutines). És similar al roscat, excepte que les corrutines són encara més lleugeres. A més, la biblioteca està especialment dissenyada per fer que el codi sigui similar en diferents plataformes.

Una corrutina és un fragment de codi que es pot suspendre i reprendre . La idea aquí és que un tros de codi pot esperar alguna cosa, per exemple, el clic de l'usuari. Mentre està esperant, diem que està en estat suspès i no sobrecarrega el processador.

Tornem a l'analogia amb una empresa. Podem comparar una corrutina amb una tasca habitual que pot fer un empleat. Primer, es crea la tasca i després s'assigna a una persona. Quan la persona està preparada, decideix què ha de fer: començar a gestionar la tasca o transferir-la a una altra persona. Quan algú comença a treballar en la tasca, no hi ha cap garantia que es faci immediatament. Per exemple, suposem que la tasca era trucar a un altre departament i la persona va trucar però ningú va respondre. Aleshores aquella persona pot “suspendir” la tasca, és a dir, posposar-la una estona. Durant aquest buit, la persona pot fer una altra tasca i començar a treballar-hi o, si no hi ha més tasques assignades, pot fer una pausa.

L'avantatge aquí és que una persona és cara perquè l'has de pagar, però una tasca és barata. Si una persona pot gestionar diverses tasques, per què no fer-ho?

exemple de creació de Coroutines exemple d'ús de Coroutines

Com que es pot utilitzar un fil per executar corrutines, podeu tenir més corrutines que fils. El nombre de corrutines programades per a l'execució pot ser més d'un milió!

Aquí podeu sospitar d'un defecte: no tothom pot fer diverses tasques simultàniament. Sovint treballem en una tasca i només després en fem una altra. De fet, el mateix passa amb les corrutines: en un sol fil, només es processa una sola corrutina alhora. Per això s'anomena concurrència interna. Tanmateix, el concepte de suspensió de tasques és molt fort i podem dir que aquesta falsa simultaneïtat només funciona. Mostrem-ho.

Recordeu l'exemple del servidor web? Tenim només un nombre limitat de marmessors, però volem atendre un gran nombre de clients. La solució passa per tenir una tasca per a cada client i destinar progressivament les tasques d'espera als executors disponibles. Alguns clients hauran d'esperar més perquè hi haurà cues, però almenys el servidor no fallarà.

### Activitats

1.- En un procés, hi ha tres fils. El primer fil té dues corrutines per fer, el segon en té vint i el tercer en té cinquanta .

Quantes corrutines s'executen al mateix temps en el procés?

{% sol %}
Un fil executa una tasca, però podeu tenir diversos fils, per tant, el terme multifils. En aquest problema, cada fil té una quantitat diferent de tasques, però encara només x quantitat de fils, de manera que el nombre màxim de tasques que es poden executar al MATEIX temps és x quantitat de fils.
{% endsol %}
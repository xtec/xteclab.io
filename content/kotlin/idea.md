---
title: Idea
description: Idea és una IDE per Java i Kotlin.
---

## Introducció

[Idea](https://www.jetbrains.com/es-es/idea/) és una IDE per Java i {% link "/kotlin/" %}.

Instal.la {% link "/tool/box/" %} tal com s'explica en el document:

```pwsh
Set-ExecutionPolicy Remote -Scope CurrentUser
Install-Module Box -scope CurrentUser -SkipPublisherCheck
```

Instal.la la IDE:

```pwsh
scoop bucket add extras
scoop install extras/idea-ultimate
```

## Projecte

Obre un terminal de {% link "/windows/powershell" %}.

Crea un nou projecte amb {% link "./gradle/" %}:

```pwsh
md whale
cd whale
gradle init --package whale --project-name whale --java-version 21 --type kotlin-application --dsl kotlin --test-framework kotlintest --no-split-project --no-incubating --overwrite
...
```

Obre el projecte amb l'editor:

```pwsh
idea.exe .
```

## Tips

* Per ajustat l'aparença de l'editor ves a **Editor|General** mitjançant el menú, o apreta la combinació de tecles `Ctrl`+`Alt`+`s`.

* Per canviar el tamany del text de l'editor: <https://www.jetbrains.com/help/idea/settings-editor-font.html>

* Per formatejar el codi perquè es vegi bé `Ctrl`+`Alt`+`l`


## TODO

* [IntelliJ IDEA](https://hyperskill.org/learn/step/37202)
* [IntelliJ IDEA basics](https://hyperskill.org/learn/step/5819)
* [Navigation through code](https://hyperskill.org/learn/step/5824)
* [Writing code with pleasure](https://hyperskill.org/learn/step/5829)
* [Fixing code](https://hyperskill.org/learn/step/5834)

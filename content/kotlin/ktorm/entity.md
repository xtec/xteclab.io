---
title: Entity API
---

* [Entities & Column Binding](https://www.ktorm.org/en/entities-and-column-binding.html)
* [Entity Query](https://www.ktorm.org/en/entity-finding.html)
* [Entity Sequence](https://www.ktorm.org/en/entity-sequence.html)
* [Sequence Aggregation](https://www.ktorm.org/en/sequence-aggregation.html)
* [Entity Manipulation](https://www.ktorm.org/en/entity-dml.html)
* [Define Entities as Any Kind of Classes](https://www.ktorm.org/en/define-entities-as-any-kind-of-classes.html)
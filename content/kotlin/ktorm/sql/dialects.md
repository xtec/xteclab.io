---
title: Dialectes
---

## Introducció

Encara que SQL sigui un llenguatge "estàndard", la majoria de les bases de dades ofereixen extensions pròpies.

I aquestes extensions són molt útils, tenint en compte que són un dels principals motius al escollir una bade de dades en concret.

El mòdul principal de Ktorm (ktorm-core) només implementa el SQL estàndard.

## Dialecte

Ktorm té una interfície `SqlDialect` que es pot implementar per poder utilitzar les característiques concretes d'una base de dades específica.

A continuació tens una relació de mòduls que proporcionen una implementació de `SqlDialect`:

| Database Name | Module Name | SqlDialect Implementation |
|-|-|
| MySQL | ktorm-support-mysql | org.ktorm.support.mysql.MySqlDialect || PostgreSQL | ktorm-support-postgresql | org.ktorm.support.postgresql.PostgreSqlDialect |
| SQLite | ktorm-support-sqlite | org.ktorm.support.sqlite.SQLiteDialect |
| SqlServer | ktorm-support-sqlserver | org.ktorm.support.sqlserver.SqlServerDialect |
| Oracle | ktorm-support-oracle | org.ktorm.support.oracle.OracleDialect |

Nosaltres treballem amb Postgres i SQLite, anem a veure que tenen de diferent.

## Postgresql

El primer que hem de fer és afegir la depedència en el nostre fitxer de configuració:

```
compile "org.ktorm:ktorm-support-postgresql:${ktorm.version}"
```

Ara ja podem utilitzar totes les característiques de postgres:

* Support standard pagination functions of Ktorm, translating to PostgreSQL’s `limit ? offset ?` clause.

* Support extended syntax for insert statements, like [insertReturning](https://www.ktorm.org/api-docs/org.ktorm.support.postgresql/insert-returning.html), insertOrUpdate, bulkInsert, bulkInsertOrUpdate, etc.
* Support locking clause via locking function, eg. select ... for update.
* Support hstore data type and a series of operators for it.
* Support cube & earth data type and their utility functions.  
* For more functionality, please refer to the API docs https://www.ktorm.org/api-docs/org.ktorm.support.postgresql/index.html


### Activitats

**1.-** Exemple `ilike`.



## SQLite

* Support standard pagination functions of Ktorm, translating to SQLite’s limit ?, ? statement.

* Support extended syntax for insert statements, like insertOrUpdate, bulkInsert, bulkInsertOrUpdate, etc.

* Support some common-used JSON operating functions, like jsonExtract, jsonPatch.

* Support some common-used functions like iif, ifNull, instr, replace, etc.

For more functionality, please refer to the API docs https://www.ktorm.org/api-docs/org.ktorm.support.sqlite/index.html


## JDBC

TODO moure a getting started

Ktorm utilitza JDBC per oferir tota la seva funcionalitat, i en algunes situacions potser t'interessa escriure SQL directament i treballar a nivel de JDBC.

En aquests casos, pots obtindre una connexió a la base de dades amb `database.useConnection` i escriure codi amb JDBC.

A continuació tens un exemple:

```kt
// TODO imports

val names = database.useConnection { conn ->
    val sql = """
        select name from t_employee
        where department_id = ?
        order by id
    """

    conn.prepareStatement(sql).use { statement ->
        statement.setInt(1, 1)
        statement.executeQuery().asIterable().map { it.getString(1) }
    }
}

names.forEach { println(it) }
```

At first glance, there are only boilerplate JDBC code in the example, but actually, it’s also benefited from some convenient functions of Ktorm:

* `useConnection` function is used to obtain or create connections. If the current thread has opened a transaction, then this transaction’s connection will be passed to the closure. Otherwise, Ktorm will pass a new-created connection to the closure and auto close it after it’s not useful anymore. Ktorm also uses this function to obtain connections to execute generated SQLs. So, by calling `useConnection`, we can share the transactions or connection pools with Ktorm’s internal SQLs.

* `asIterable` function is used to wrap `ResultSet` instances as `Iterable`, then we can iterate the result sets by for-each loops, or process them via extension functions of Kotlin standard lib, such as `map`, `flatMap`, etc.

Pots veure que ktorm és una "extensió" de JDBC.

















---
title: DSL SQL
---

* [Schema Definition](https://www.ktorm.org/en/schema-definition.html)
* [Query](https://www.ktorm.org/en/query.html)
* [Joining](https://www.ktorm.org/en/joining.html)
* [Data Manipulation](https://www.ktorm.org/en/dml.html)
* [Operators](https://www.ktorm.org/en/operators.html)

* [Dialects & Native SQL](https://www.ktorm.org/en/dialects-and-native-sql.html). Només dialtecte postgres
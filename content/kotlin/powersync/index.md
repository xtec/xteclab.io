---
title: PowerSync
description:  PowerSync és un motor de sincronització per crear aplicacions locals amb interfície d'usuari/UX de resposta instantània i transferència d'estat simplificada. Sincronitza entre SQLite al costat del client i Postgres, MongoDB o MySQL al costat del servidor.
---

## Introducció

[PowerSync](https://www.powersync.com/) és un servei i conjunt de SDKs clients que manté bases de dades de backend en sincronia amb bases de dades SQLite incrustades en el dispositiu.

Permet crear aplicacions reactives en temps real [local-first](https://docs.powersync.com/resources/local-first-software) i offline-first que romanen disponibles fins i tot quan la connectivitat de xarxa és pobra o inexistent.

{% image "powersync.png" %}

​
Bases de dades de backend recolzades: {% link "/data/postgres/" %}, {% link "/data/mongodb/" %} i MySQL.

## Configurar

**1.-** Crea un compte gratuit a PowerSync Cloud: [Enllaç](https://accounts.journeyapps.com/portal/get-started?powersync=true&s=docs-quickstart)

**2.-** Configureu la vostra base de dades d'origen per a PowerSync: vegeu [Configuració de la base de dades d'origen](https://docs.powersync.com/installation/database-setup)

Nosaltres utilitzarem {% link "/data/postgres/neon/" %}

Obre el teu **projecte** i habilita la replicació lògica:

{% image "neon-logical-replication.png" %}

Creeu un usuari de PowerSync a Postgres:

```sql
-- SQL to create powersync user

CREATE ROLE powersync_role WITH REPLICATION LOGIN PASSWORD 'myhighlyrandompassword';

GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO powersync_role;
```

Creeu la publicació "powersync"

```sql
-- Create publication to replicate tables. Specify a subset of tables if required.

-- NOTE: this must be named "powersync" at the moment

CREATE PUBLICATION powersync FOR ALL TABLES;
```

**3.-** Connecteu la vostra base de dades a la vostra instància del servei PowerSync:

[Connexió a la base de dades](https://docs.powersync.com/installation/database-connection)

Enganxar l'string de connexió (amb la contrasenya) al camp " URI " per simplificar l'entrada de dades.

Copia la contrasenya al camp password:

{% image "connection.png" %}

Fes clic a "Test Connection" i "Save".


* <https://docs.powersync.com/installation/quickstart-guide>



## Entorn de treball

```pwsh
> gradle init --package dev.xtec --project-name powersync --java-version 21 --type kotlin-application --dsl kotlin --test-framework kotlintest --no-split-project --no-incubating --overwrite
```

* <https://docs.powersync.com/installation/client-side-setup>
* <https://docs.powersync.com/client-sdk-references/kotlin-multiplatform>

## TODO


* <https://docs.powersync.com/integration-guides/supabase-+-powersync>
* <https://www.powersync.com/blog/build-local-first-kotlin-multiplatform-apps-with-powersync>

* <https://github.com/powersync-ja/powersync-kotlin>
* <https://blog.stackademic.com/building-an-offline-first-mobile-app-with-powersync-40674d8b7ea1>

---
title: Computació
description: El propòsit inicial dels computadors era computar números.
---

## Introducció

De de fa més de 400 anys una computadora era una persona que calculaba números:

{% image "human-computer.jpg" %}

Una part de la teva infància ha estat aprenent a fer càlculs amb números, i molts cops t'has preguntat que perquè has d'apendre aquestes coses si el teu mòbil té una calculadora que ho fa molt millor que tu.

El mateix van pensar les empreses, els governs i els enginyers, i per això van crear els primers computadors mecànics.

A continuació tens la imatge de l'[ENIAC](https://ca.wikipedia.org/wiki/ENIAC) dels anys 40, molt gran, però molt menys potent que el teu mòbil:

{% image "eniac.jpg" %}

A principis de 1960 ja existien les primeres calculadores comercials.

A continuació tens la imatge del [IBM 608](https://es.wikipedia.org/wiki/IBM_608):

{% image "ibm-608.jpg" %}

Aquestes màquines mai s'equivoquen, no es posen malaltes, no protesten, etc. 

I el que passa sempre, ja pots despedir a molta gent que no necessites i que s'ha de pagar bé perqué preparar una calculadora "humana" porta molt de temps.

Una professió centenària va desaparèixer 😔.

Doncs comencem des del principi, i recorda ..

Encara que pensis que una computadora és per jugar, veure películes, etc. radera d'aquests programes hi ha moltes operacions matemàtiques!

## Entorn de treball

Instal.la `kotlin` i `openjdk21` amb {% link "/tool/scoop/" %}

```pwsh
scoop bucket add extras
scoop install extras/kotlin-interactive-shell
scoop bucket add java
scoop install java/openjdk21
```

A continuació inicia el "kotlin shell":

```pwsh
> ki
ki-shell 0.5.2/1.7.0
type :h for help
[0] 4 + 5
res0: Int = 9
[1]
```

Per sortir de l'interpret escriu `:quit`:

```pwsh
[3] :quit

Bye!
```

## TODO

**A partir d'aquí s'ha de revisar** a partir de docs de {% link "/python/" %}

## Literals


Bàsicament tots els programes realitzen operacions sobre nombres, strings i altres valors. 

Aquests valors s'anomenen **literals**, que vol dir que el que hi ha escrit és el que hi ha, a diferència d'una variable.

### Nombres enters

Utilitzem nombres enters per comptar coses del món real.

Aquí hi ha diversos exemples de literals de nombres enters vàlids separats per comes: `0`, `1`, `2`, `10`, `11`, `100`.

Si un valor enter conté molts dígits, podem afegir guions baixos per dividir els dígits en blocs perquè aquest nombre sigui més llegible: per exemple, `1_000_000` és molt més fàcil de llegir que `1000000`.

Podeu afegir tants guions baixos com vulgueu: `1__000_000`, `1_2_3`. Recordeu que els guions baixos no poden aparèixer al principi ni al final del número. Si escrius `_10` o `100_`, obteniu un error.

### Char

Un sol caràcter pot representar un dígit, una lletra o un altre símbol. 

Per escriure un sol caràcter, emboliquem un símbol entre cometes simples de la manera següent: `'A'`, `'B'`, `'C'`, `'x'`, `'y'`, `'z'`, `'0'`, `'1'`, `'2'`, `'9'`. Els literals de caràcters poden representar lletres de l'alfabet, dígits de `'0'` a `'9'`, espais en blanc (`' '`) o alguns altres símbols (p. ex., `'$'`).

No confongueu els caràcters que representen números (p. ex., `'9'`) i els mateixos números (p. ex., `9`).

Un caràcter no pot incloure dos o més dígits o lletres perquè representa un sol símbol. Els dos exemples següents són incorrectes: `'abc'`, `'543'` perquè aquests literals tenen massa caràcters.

### String

Els strings representen informació de text, com ara el text d'un anunci, l'adreça d'una pàgina web o l'inici de sessió a un lloc web. Una cadena és una seqüència de caràcters individuals.

Per escriure strings, envoltem els caràcters entre cometes dobles en lloc de simples. Aquests són alguns exemples vàlids: `"text"`, `"I want to learn Kotlin"`, `"123456"`, `"e-mail@gmail.com"`. Per tant, els strings poden incloure lletres, dígits, espais en blanc i altres caràcters.

Una string també pot contenir un sol caràcter, com ara `"A"`. No ho confonguis amb el caràcter `'A'`, que no és una string.

## Sortida estàndard

La sortida estàndard és l'operació bàsica que mostra informació en un dispositiu. No tots els programes generen aquesta sortida. Per defecte, la sortida estàndard mostra les dades a la pantalla, però és possible redirigir-les a un fitxer.

### Impressió de text

Kotlin té dues funcions que envien dades a la sortida estàndard: `println` i `print`.

La funció `println` (**print line**) mostra una string seguida d'una nova línia a la pantalla. 

Per exemple, el fragment de codi següent imprimeix quatre línies:

```kt
println("I")
println("know")
println("Kotlin")
println("well.")
```

Sortida:

```
I
know
Kotlin
well.
```

Com pots veure, totes les cadenes s'imprimeixen sense cometes dobles.

També pots imprimir una línia buida:

```kt
println("Kotlin is a modern programming language.")
println() // prints an empty line
println("It is used all over the world!")
```

Sortida:

```
Kotlin is a modern programming language.

It is used all over the world!
```

La funció `print` mostra un valor i col·loca el cursor després de `.` 

Aquest exmemple de codi genera totes les cadenes en una sola línia:

```kt
print("I ")
print("know ")
print("Kotlin ")
print("well.")
```

Sortida:

```
I know Kotlin well.
```

### Impressió de números i caràcters

Amb les funcions `println`i `print`, un programa pot imprimir no només strings, sinó també números i caràcters .

Imprimim dos codis secrets:

```kt
print(108)   // prints a number
print('c')   // prints a character

print("Q")   // prints a string
println('3') // prints a character that represents a digit 

print(22)
print('E')
print(8)
println('1')
```

Sortida:

```
108cQ3
22E81
```

Igual que amb les cadenes, no hi ha cometes.

### L' operador %

En el llenguatge de programació Kotlin, l'operador `$` s'utilitza sovint en plantilles d'string per inserir els valors de variables o expressions directament en una cadena.

Inserint el valor d'una variable:

```kt
val name = "Alice"
println("Hello, $name!") // Output: Hello, Alice!
```

Inserint el valor d'una expressió:

```kt
val a = 5
val b = 10
println("Sum of $a and $b is ${a + b}") // Output: Sum of 5 and 10 is 15
```

Tingueu en compte que quan voleu inserir una expressió més complexa o accedir a les propietats de l'objecte, feu servir claus `{}`al voltant de l'expressió.

Així, l'operador `$` de Kotlin permet la inserció còmoda i ràpida de valors variables i resultats d'expressió directament en literals de l'string.


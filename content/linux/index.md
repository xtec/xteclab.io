---
title: Linux
icon: linux.png
---

{% pages ["shell/", "bash-intro/", "apt/", "filtres/", "processos/", "scripts/", "users/", "archive/", "apparmor/" ] %}

### Seguretat

{% pages ["apparmor/", "ufw/", "netfilter/"] %}


{% pages ["docker/", "kubernetes/"] %}

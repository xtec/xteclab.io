---
title: UFW
description: UFW, o Uncomplicated Firewall, és una interfície de gestió de tallafocs simplificada que amaga la complexitat de les tecnologies de filtratge de paquets de nivell inferior com nftables. 
mermaid: true
---

## Introducció 

Si voleu començar a protegir la vostra xarxa i no esteu segur de quina eina utilitzar, UFW pot ser l'opció adequada per a vosaltres.

## XXX

Crea una màquina server a {% link "/cloud/isard/" %}.

Ha de tenir les interficies "Default", "Wireguard VPN" i "provençana1".

UFW està instal·lat per defecte a Ubuntu:

```sh
$ sudo ufw status
Status: inactive
```

Si s'ha desinstal·lat per algun motiu, pots instal·lar-lo amb `sudo apt install ufw`.

### Ús d'IPv6 amb UFW (opcional)

Si el vostre servidor Ubuntu té IPv6 habilitat, assegureu-vos que UFW estigui configurat per admetre IPv6 de manera que gestionarà les regles del tallafoc per a IPv6 a més d'IPv4.

```sh
$ sudo cat /etc/default/ufw | grep IPV6
IPV6=yes
```

Ara, quan UFW estigui habilitat, es configurarà per escriure regles de tallafocs IPv4 i IPv6. No obstant això, abans d'habilitar UFW, volem assegurar-nos que el vostre tallafoc està configurat perquè us permeti connectar-vos mitjançant SSH. 

### Comencem per establir les polítiques predeterminades.

Si acabeu de començar amb el vostre tallafoc, les primeres regles a definir són les vostres polítiques predeterminades. Aquestes regles controlen com gestionar el trànsit que no coincideix explícitament amb cap altra regla. 

Per defecte, UFW està configurat per:

1. Denegar totes les connexions entrants
2.  Permetre totes les connexions de sortida. 

Això vol dir que qualsevol persona que intenti arribar al vostre servidor no es podria connectar, mentre que qualsevol aplicació del servidor podria arribar al món exterior.


<pre class="mermaid">
flowchart LR

   sv["Server"]
   fw[["Firewall"]]
   net(("Network"))

   sv -- allow --> fw
   net -- deny --> fw 

</pre>

Torneu a establir les vostres regles UFW a les predeterminades perquè puguem estar segurs que podreu seguir aquest document. 

```sh
$ sudo ufw default deny incoming

Default incoming policy changed to 'deny'
(be sure to update your rules accordingly)

$ sudo ufw default allow outgoing

Default outgoing policy changed to 'allow'
(be sure to update your rules accordingly)
```

Aquests valors predeterminats del tallafoc només poden ser suficients per a un ordinador personal, **però normalment els servidors han de respondre a les sol·licituds entrants d'usuaris externs**.

#### SSH

Si ara activem el nostre firewall, denegaria totes les connexions entrants.

Això vol dir, que haurem de crear regles que permetin explícitament connexions entrants legítimes (connexions SSH o HTTP, per exemple) si volem que el nostre servidor respongui a aquest tipus de sol·licituds.

Si utilitzeu un servidor al núvol, probablement voldreu permetre connexions SSH entrants perquè pugueu connectar-vos i gestionar el vostre servidor.

```sh
$ sudo ufw allow ssh
Rules updated
Rules updated (v6)
```

Això crearà regles de tallafoc que permetran totes les connexions al port 22, que és el port que el servidor SSH escolta per defecte. UFW sap quin port permet ssh significa perquè apareix com a servei a la llista del fitxer `/etc/services`.

```sh
$ cat /etc/services | grep ssh
ssh             22/tcp                          # SSH Remote Login Protocol
```

Tanmateix, podem escriure la regla equivalent especificant el port en lloc del nom del servei. Per exemple, aquesta ordre funciona igual que l'anterior:

```sh
$ sudo ufw allow 22
Rules updated
Rules updated (v6)
```

Si heu configurat el vostre servidor SSH per utilitzar un port diferent, haureu d'especificar el port adequat. 
Per exemple, si el vostre servidor SSH escolta al port 2222, podeu utilitzar aquesta ordre per permetre connexions en aquest port:

```sh
$ sudo ufw allow 2222
Rules updated
Rules updated (v6)
```

Ara que el vostre tallafoc està configurat per permetre connexions SSH entrants, podem activar-lo.
Habilitant UFW

Per habilitar UFW, utilitzeu aquesta ordre:

```sh
$ sudo ufw enable
Command may disrupt existing ssh connections. Proceed with operation (y|n)? y
Firewall is active and enabled on system startup
```

Rebràs un avís que indica que l'ordre pot interrompre les connexions SSH existents. 

Ja hem configurat una regla de tallafoc que permet connexions SSH, de manera que hauria d'estar bé continuar. Respon a la sol·licitud amb y i premeu INTRO.

El tallafoc ja està actiu i encara seguim connectats a la màquina per ssh 😅!

Executa l’ordre `sudo ufw status` per veure les regles que s'estableixen.

```sh
$ sudo ufw status
Status: active

To                         Action      From
--                         ------      ----
22/tcp                     ALLOW       Anywhere
22                         ALLOW       Anywhere
2222                       ALLOW       Anywhere
22/tcp (v6)                ALLOW       Anywhere (v6)
22 (v6)                    ALLOW       Anywhere (v6)
2222 (v6)                  ALLOW       Anywhere (v6)
```

Pots veure que s’han creat les mateixes regles per IPv4 i IPv6 (v6).

### Borrar regles

Fixa’t que tenim dos regles pel port 22.

La primera regla `22/tcp` només s’aplica si a més del port 22 la connexió és tcp (les connexions ssh són tcp).

La segona regla s’aplica al port 22 tant si la connexió és tcp com udp.

És millor eliminar una de les regles per evitar confusions.

El primer que hem de fer és que ufw ens mostri una llista de les regles numerades i eliminar la regla amb el número corresponent: 

```sh
$ sudo ufw status numbered
Status: active

     To                         Action      From
     --                         ------      ----
[ 1] 22/tcp                     ALLOW IN    Anywhere
[ 2] 22                         ALLOW IN    Anywhere
[ 3] 2222                       ALLOW IN    Anywhere
[ 4] 22/tcp (v6)                ALLOW IN    Anywhere (v6)
[ 5] 22 (v6)                    ALLOW IN    Anywhere (v6)
[ 6] 2222 (v6)                  ALLOW IN    Anywhere (v6)
```

Borra la regla 2:

```sh
$ sudo ufw delete 2
Deleting:
 allow 22
Proceed with operation (y|n)? y
Rule deleted
```

També pots eliminar una regla indicant la seva sintaxis:

```sh
$ sudo ufw delete allow 2222
Rule deleted
Rule deleted (v6)
```

Si et fixes, quan vas eliminar la regla `allow 22` ho vas fer amb el número de regla.

Per això no es vas eliminar la regla `allow 22 (v6)` a diferència del que ha passat ara:

```sh
$ sudo ufw status
Status: active

To                         Action      From
--                         ------      ----
22/tcp                     ALLOW       Anywhere
22/tcp (v6)                ALLOW       Anywhere (v6)
22 (v6)                    ALLOW       Anywhere (v6)
```

### Verificar el firewall

Arrenca un servidor nginx amb apt:

```sh
$ sudo apt update && sudo apt install -y nginx
```

Verifica que el servidor nginx funciona:

```sh
$ curl -s localhost | head -n 1
<!DOCTYPE html>
```

Verifica que no pots connectar al servidor nginx des de la màquina WSL:

```sh
$ curl 10.2.39.218
curl: (28) Failed to connect to 10.2.39.218 port 80 after 119612 ms: Couldn't connect to server
```

Verifica que no et pots connectar al servidor web de la màquina alpha :

Si et canses d’esperar Ctrl + C.


Recordes que el firewall de la màquina firewall permet totes les connexions de sortida?

1. Crea una nova màquina a isard amb el nom `box-2` connectada a la mateixa xarxa d'institut que `box-1`

2. Arrenca un servidor nginx a la màquina `box-2`

3. Verifica que et pots connecta al servidor nginx de `box-2` des de la màquina  `box-1`.

<pre class="mermaid">
flowchart LR

   b1["box-1"]
   b2["box-2"]
   fw[["Firewall"]]
   net(("Network"))

   b1 -- allow --> fw
   net -- deny --> fw 
   b2 <--> net

</pre>

```sh
box-1$ curl 10.2.39.218

...
```

L'activitat segueix en aquest document: [Google Docs](https://docs.google.com/document/d/1oEcwkPcNzF2wvlmzBuawe-6VLX6l4ZjPTUOH80R5H5g/edit)

## HTTP

Com que tens un servidor web has de permetre el port 80.

Però ha de ser `80` o `80/tcp` ?

Per defecte nginx només utilitza HTTP/1 i HTTP/2 que són connexions TCP, encara que pots habilitar HTTP/3 que són connexions UDP.

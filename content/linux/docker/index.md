---
title : Docker
icon: docker.png
description: Docker és un gestor de contenidors de Linux.
---

### Entorn de treball

#### WSL

Arrenca una màquina virtual Ubuntu amb {% link "/windows/wsl/" %}:

```pwsh
> connect-wsl docker -new
```

Instal.la docker:

```sh
$ install-docker
```

**Observació**. `install-docker` és un alias de `curl -L sh.xtec.dev/docker.sh | sh`.

#### Isard

Arrenca una màquina virtual Ubuntu amb {% link "/cloud/isard/" %}:

```pwsh
> connect-isard docker -new
```

Instal.la docker:

```sh
$ curl -L sh.xtec.dev/docker.sh | sh
```

### Activitats

#### Contenidor

{% pages ["container/", "network/", "storage/", "compose/", "resource/"] %}


#### Imatge

{% pages ["image/", "build/", "registry/", "pipeline/"] %}

#### Cluster

{% pages ["/cluster/nomad/", "/cluster/consul/", "/cluster/vault/" ] %}


#### Desenvolupmanent

{% pages ["development/", "/cloud/azure/app-service/"] %}



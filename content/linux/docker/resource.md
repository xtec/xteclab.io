---
title: Recursos
---

## Introducció

Els contenidors proporciones contextos que aïllen els processos, en cap cas un sistema de virtualització.

Si tu crees una màquina virtual amb 2GB de RAM, els processos que s'executen en la màquina virtual només disposen de com a màximm 2GB de RAM de la màquina real encara que la màquina real tingui 32 GB de RAM.

En canvi, si tu executes un contenidor en una màquina amb 32GB de RAM, el contenidor disposa de tota la RAM com qualsevol altre procés.

A {% link "/linux/docker/network/" %} i {% link "/linux/docker/storage/" %} hem vist com poden limitar un contenidor, però hi ha moltes altres limitacions que permeten aïllar més un contenidor com veurem a continuació.

## Memòria

Al crear un contenidor podem limitar la memòria que pot utilitzar amb el flag `-m` o `--memory`.

Aquest flag té un valor enter seguit per un sufix `b`, `k`, `m` o `g` per indicar bytes, kilobytes, megabytes o gigabytes.

El valor mínim de la memoria ha de ser `6m` (6 megabytes):

```sh
$ docker run --rm -d -m 1m --name web -p 80:80 httpd
docker: Error response from daemon: Minimum memory limit allowed is 6MB.
```

Anem a veure si un apache pot funcionar només amb `7m` (7 megabytes):

```sh
$ docker run --rm -d -m 7m --name web -p 80:80 httpd
$ curl localhost
<html><body><h1>It works!</h1></body></html>
```

Sembla que de moment funciona tal com mostra [`stats`](https://docs.docker.com/reference/cli/docker/container/stats/):

```sh
$ docker stats web --no-stream
CONTAINER ID   NAME      CPU %     MEM USAGE / LIMIT   MEM %     NET I/O         BLOCK I/O   PIDS
4c77f01a428a   web       0.03%     4.836MiB / 7MiB     69.08%    1.79kB / 626B   0B / 0B     109
```

Instal.la el paquet `apache2-utils` per poder executar [ApacheBenchmark](https://httpd.apache.org/docs/current/programs/ab.html)

```
$ sudo apt install -y apache2-utils
```

Ja podem fer un benchmark 😆!

Primer monitoritzem el contenidor web en mode stream (és el mode per defecte):

```sh
$ docker stats web --no-stream
CONTAINER ID   NAME      CPU %     MEM USAGE / LIMIT   MEM %     NET I/O         BLOCK I/O   PIDS
4c77f01a428a   web       0.03%     4.836MiB / 7MiB     69.08%    1.79kB / 626B   0B / 0B     109
```

Obre un altre terminal i dona feina al servidor durant un màxim de 30 segons amb 1000 peticions concurrents:

```sh
$ ab -kc 1000 -t 30 http://localhost
...
```

Torna a l'altre terminal on pots veure que encara que la CPU % va al màxim, la memòria està limitada:

```
CONTAINER ID   NAME      CPU %     MEM USAGE / LIMIT   MEM %     NET I/O           BLOCK I/O   PIDS
4c77f01a428a   web       94.36%    6.711MiB / 7MiB     95.87%    10.5MB / 21.2MB   0B / 0B     142
```

Torna a l'altre terminal i quan acabi el test pots veure que apache a tingut alguns problemes ja que de les 1000 peticions concurrents no ha respós a 390.

```sh
$ ab -kc 1000 -t 30 http://localhost

Concurrency Level:      1000
Time taken for tests:   30.507 seconds
Complete requests:      749
Failed requests:        390
   (Connect: 0, Receive: 0, Length: 390, Exceptions: 0)
Keep-Alive requests:    359
Total transferred:      116922 bytes
HTML transferred:       16155 bytes
Requests per second:    24.55 [#/sec] (mean)
Time per request:       40730.849 [ms] (mean)
Time per request:       40.731 [ms] (mean, across all concurrent requests)
Transfer rate:          3.74 [Kbytes/sec] received
```

Pot ser per un problema de memòria 🙄 ... ? 🤣

Anem a veure ara sense límits 🚀

Crea un nou contenidor `web_unlimited`:

```sh
$ docker run --rm -d --name web_unlimited -p 81:80 httpd

$ docker stats web_unlimited
CONTAINER ID   NAME            CPU %     MEM USAGE / LIMIT     MEM %     NET I/O       BLOCK I/O   PIDS
dd37fe9355c2   web_unlimited   0.01%     22.32MiB / 7.698GiB   0.28%     1.02kB / 0B   0B / 0B     82
```

Pots veure que el límit de memòria és de 7.698GiB, té tota la capacitat de la màquina per ell si vol.

Let's go!

```sh
$ ab -kc 1000 -t 30 http://localhost:81/

Concurrency Level:      1000
Time taken for tests:   4.881 seconds
Complete requests:      50000
Failed requests:        14945
   (Connect: 0, Receive: 0, Length: 14945, Exceptions: 0)
Keep-Alive requests:    35055
Total transferred:      11408269 bytes
HTML transferred:       1577475 bytes
Requests per second:    10243.67 [#/sec] (mean)
Time per request:       97.621 [ms] (mean)
Time per request:       0.098 [ms] (mean, across all concurrent requests)
Transfer rate:          2282.47 [Kbytes/sec] received

 ```

El servidor `web_unlimited` a pogut respondre a totes les peticions en només 5 segons.

Potser serà que tenia més memòria disponible ...

```
CONTAINER ID   NAME            CPU %     MEM USAGE / LIMIT     MEM %     NET I/O           BLOCK I/O   PIDS
dd37fe9355c2   web_unlimited   0.01%     72.79MiB / 7.698GiB   0.92%     13.4MB / 19.1MB   0B / 0B     271
```

Pots veure que el `MEM USAGE` és de `72.79MiB` mentres que el contenidor `web` al tenir limitada la memòria el seu `MEM USAGE` era de `6.711MiB`.

## CPU

Igual que passa amb la memòria, en els sistemes operatius la CPU és un recurs compartit per tots els processos que s'estan executant



## Referències

* [Resource constraints](https://docs.docker.com/config/containers/resource_constraints/)

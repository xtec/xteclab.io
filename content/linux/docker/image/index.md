---
title: Imatge
---

## Imatges

Amb l'ordre `docker images` podem veure totes les imatges que s'han descarregat i que estan en la cache de la màquina virtual:

```sh
$ docker images
REPOSITORY                   TAG       IMAGE ID       CREATED         SIZE
sail-8.3/app                 latest    be7554cb158e   4 days ago      1.66GB
redis                        alpine    eac30ee4acc6   5 days ago      46.1MB
...
```

Pots verificar que no tinc cap imatge `alpine:3.1`:

```sh
$ docker images alpine:3.1
REPOSITORY   TAG       IMAGE ID       CREATED       SIZE
$
``` 

Per executar un contenidor s’ha de descarregar la imatge corresponent:

```sh
$ docker run --name alpine alpine:3.1 echo "Hello!"
Unable to find image 'alpine:3.1' locally
3.1: Pulling from library/alpine
0f253aa151d7: Pull complete 
Digest: sha256:4dfc68bc95af5c1beb5e307133ce91546874dcd0d880736b25ddbe6f483c65b4
Status: Downloaded newer image for alpine:3.1
Hello!
```

La imatge queda guardada en la màquina virtual:

```sh
$ docker images alpine:3.1
REPOSITORY   TAG       IMAGE ID       CREATED       SIZE
alpine       3.1       a1038a41fe2b   5 years ago   5.05MB
``` 

El contenidor `alpine` necessita aquesta imatge perquè fa servir el sistema de fitxers de la imatge.

Si intento eliminar la imatge docker em diu que no puc:

```sh
$ docker image rm alpine:3.1
Error response from daemon: conflict: unable to remove repository reference "alpine:3.1" (must force) - container 22a5e3a77bb2 is using its referenced image a1038a41fe2b
```

Si vols eliminar les imatges que cap contenidor estigui fent servir per tal d’alliberar espai al disc, utilitza aquesta comanda:

```sh
$ docker image prune -a
WARNING! This will remove all images without at least one container associated to them.
Are you sure you want to continue? [y/N] y
```

## Crear una imatge

Si volem que la nostra aplicació web sigui portable necessitem juntar comportament (el servidor web) amb les dades (el nostre lloc web).

Per tal de construir una nova imatge a partir d’un contenidor hem de fer tres coses:

1. Hem d’escollir l’imatge de partida

2. Modificar el sistema de fitxers del contenidor. Aquests canvis es fan en una nova capa del sistema de fitxers propietat del contenidor.

3. Fer un commit per crear una nova imatge

Arrenquem un contenidor apache:

```sh
$ docker run --rm -d --name apache -p 80:80 httpd
Unable to find image 'httpd:latest' locally
latest: Pulling from library/httpd
09f376ebb190: Already exists 
dab55b4abfc3: Pull complete 
4f4fb700ef54: Pull complete 
1a6d0283f224: Pull complete 
1abf9110528c: Pull complete 
7bacb8f85f3a: Pull complete 
Digest: sha256:43c7661a3243c04b0955c81ac994ea13a1d8a1e53c15023a7b3cd5e8bb25de3c
Status: Downloaded newer image for httpd:latest
49a08aa9247bf05de9fcaa68a4518558d98638d0912c08f94a4ae0bad50b9c20
```

Pots veure que es baixen diferents "layers" (en parlarem més endavant).

Modifica el contingut de la pàgina `index.html` del contenidor:

```sh
$ docker exec -it apache bash
... $ echo "A poc a poc i bona lletra" > htdocs/index.html 
    $ exit
    exit
```

O en una sóla línia:

```sh
$ docker exec apache bash -c "echo 'A poc a poc i bona lletra' > htdocs/index.html"
```

Amb l'ordre `container diff` pots veure les diferències entre els sistema de fitxers del contenidor i el de les imatges de les quals deriva el contenidor:

```sh
$ docker container diff apache
C /root
A /root/.bash_history
C /usr
C /usr/local
C /usr/local/apache2
C /usr/local/apache2/logs
A /usr/local/apache2/logs/httpd.pid
C /usr/local/apache2/htdocs
C /usr/local/apache2/htdocs/index.html
```

També que el contenidor respon amb el **nou** fitxer `index.html`:

```sh
$ curl localhost
A poc a poc i bona lletra
```

Si parem el contenidor aquest s'esborra i es perd el sistema de fitxers (a "layer") del contenidor.

Si volem, podem crear una nova imatge a partir del contenidor:

```sh
$ docker container commit apache apache-dixit
sha256:6509481cba1c9ba72c6288cbdd76d9477ca43ce69ddccfa6882323b6c9a301c0

$ docker images apache-dixit
REPOSITORY     TAG       IMAGE ID       CREATED          SIZE
apache-dixit   latest    6509481cba1c   30 seconds ago   147MB
```

Eliminem el contenidor apache i fem servir la imatge `apache-dixit` que hem creat per aixecar un nou contenidor:

```sh
$ docker stop apache
apache

$ docker run --rm -d --name apache -p 80:80 apache-dixit
cf254be6de0e6ece208ae61aaf644a093fd077d0d5e2dd2c9c4df3c147ca6fc3

$ curl localhost
A poc a poc i bona lletra
```

A partir d’aquesta imatge es poden crear tants contendiors apache com vulguis que enlloc de respondre amb `<html><body><h1>It works!</h1></body></html>` responen amb `A poc a poc i bona lletra`.

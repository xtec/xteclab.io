---
title: Registre
description: Un registre és un lloc on es guarden les imatges que es fan servit per desplegar contenidors
---

## Distribuir una imatge

Una imatge és un conjunt de capes de sistemes de fitxers superposades una sobre l’altre.

## Tar

Una de les formes d’empaquetar una imatge és crear un fitxer tar amb totes els sistemes de fitxers.

```sh
$ docker image save -o site.tar site
```

Aquest arxiu tar el pots distribuir com vulguis, per exemple:

```sh
$ curl --upload-file site.tar  https://transfer.sh/site.tar
```

La comanda et retorna una url temporal que permet descarregar el fitxer:

https://transfer.sh/n2BnLD/site.tar


Esborra la imatge del site que tens en la cache, i verifica que ja no la tens:

```sh
$ docker image rm site
Untagged: site:latest
Deleted: sha256:ae0acf5500684875b8c2c141c0b06194f986ff051b6d7db31fbb66daccafb7aa
Deleted: sha256:f3b40104f0f4674567b45200e81992a1bd38a4a2bf48a06ba01a77d356a4dfdb

$ docker image ls
```

Si ara intentes arrencar un contenidor amb la imatge site et donarà un error:

```sh
$ docker run --rm -d --name site -p 8000:80 site
Unable to find image 'site:latest' locally
docker: Error response from daemon: pull access denied for site, repository does not exist ...
```

Pots tornar a carregar la imatge site a la cache:

```sh
$ docker image load -i site.tar
bd2fe8b74db6: Loading layer [==================================================>]  83.97MB/83.97MB
384534ba6a14: Loading layer [==================================================>]  62.21MB/62.21MB
1341eea4a0c3: Loading layer [==================================================>]  3.584kB/3.584kB
50ec2edf53d1: Loading layer [==================================================>]  4.608kB/4.608kB
b74ced7dfeca: Loading layer [==================================================>]  3.584kB/3.584kB
8342f56cc886: Loading layer [==================================================>]  7.168kB/7.168kB
ded7881c9c2f: Loading layer [==================================================>]  13.31kB/13.31kB
Loaded image: site:0.1.0
```

Com pots veure, la imatge està formada per 7 capes de sistemes de fitxers.

Ara ja pots executar un contenidor amb la imatge site:

```sh
$ docker run --rm -d --name site -p 8000:80 site
$ curl localhost:8000
```

## Registre

Un dels altres mecanismes de distribució de imatges són registres especialitzats com Docker Hub.

Una imatge està formada per varies capes, i moltes d’aquestes capes son comunes en moltes imatges. 

Aquests registres guarden i distribueixen capes. 

Si nosaltres pujem la nostra imatge, només es pujaran les capes que no estan registrades en el registre.

Crea un compte a docker hub, un servei gratuït dins d’uns límits, podem pujar la nostre imatge.

Primer ens hem d’autenticar:

```sh
$ docker login
Username: xtecd
Password:
```

Després has de marcar la imatge amb un tag que comença amb el teu non d'usuaria Docker Hub:

```sh
$ docker tag site xtecd/site
$ docker image ls
REPOSITORY   TAG   	IMAGE ID   	CREATED     	SIZE
xtecd/site  latest	edba83a8dda1   2 minutes ago   142MB
site     	latest	edba83a8dda1   2 minutes ago   142MB
nginx    	1.23.3	9eee96112def   2 days ago  	142MB
```

Ara tenim dos imatges `site` amb repositori i tag diferent, però la mateixa id.

La id d’una imatge és el hash, i com que els dos hash són idèntics, tenen el mateix hash, i per tant la mateixa id.

Ja podem pujar la imatge marcada:

```sh
$ docker image push xtecd/site
Using default tag: latest
The push refers to repository [docker.io/xtecd/site]
39e94faaa3f2: Pushed
8342f56cc886: Mounted from library/nginx
b74ced7dfeca: Mounted from library/nginx
50ec2edf53d1: Mounted from library/nginx
1341eea4a0c3: Mounted from library/nginx
384534ba6a14: Mounted from library/nginx
bd2fe8b74db6: Mounted from library/nginx
latest: digest: sha256:bc375af2611bbe7d7dc85db6a0019a05723bed91fd881da90b68b86da1a1be3a size: 1777
```

Com pots veure docker només puja el sistema de fitxers `9e94faaa3f2` perquè els altres pertanyen a nginx i ja els té.

A l’adreça [https://hub.docker.com/r/xtecd/site/tags](https://hub.docker.com/r/xtecd/site/tags) pots trobar la image.

Borra la imatge `xtecd/site` i executa un contenidor amb la imatge `xtecd/site`:

```sh
$ docker image rm xtec/site
$ docker image ls
$ docker run --rm -d --name site -p 8000:80 xtecd/site
$ docker image ls
```

## Codi font

Avui en dia el software es distribueix en contenidors, i el mateix codi del projecte ja està preparat per construir una imatge.

Per exemple, aquest és un projecte per crear una aplicació web: <https://gitlab.com/xtec/python/vite>

Tu com administrador, no has de saber res de Python, ni de dependències, etc. 

Tampoc has de saber de programari d'integració contínua ni els de desplegament han de conèixer les interioritats del projecte.

Ara veuràs perquè.

Clona el projecte, construeix la imatge i arrenca un contenidor:

```sh
$ git clone https://gitlab.com/xtec/python/vite
$ cd vite
$ docker build -t xtec/vite .
$ docker run --rm -d --name vite -p 80:80 xtec/vite
$ docker images
```

Pots veure l'aplicació a <http://localhost>

És així de senzill !

## Referències

* [Docker - Registry](https://docs.docker.com/registry/)

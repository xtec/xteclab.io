---
title: Cerca
layout: default.njk
mermaid: true
description: Cerca de fitxers i directoris eficient amb la shell de GNU/Linux.
---

## Context

És molt important trobar ràpidament els fitxers que tenim de forma predeterminada al nostre sistema operatiu, així com els que hem usat recentment.

Per aconseguir-ho, els Sistemes Operatius tenen eines del terminal per aconseguir-ho; i és especialment important dominar-les en discs durs grans (>256GB).

## Ús comanda `find`

Crea, dentro de tu directorio personal, la carpeta shell. 
Entra en ella y desde aquí realizarás las preguntas de ésta actividad.

find : Busca ficheros recursivamente a partir del nodo que se especifica.

```sh
Sintaxis: find 	nodo_ruta 	criterio_nombre_expresión 	acción
```

Exemple:

Cerquem tots els fitxers amb extensió `.pdf` que tenim dins del nostre directori d'usuari:

```sh
find /home/isard -name *.pdf 
```

## Referència

- <https://docs.google.com/document/d/1o065V-yailPPNGFfnRXH_iExwEsGGWlz/edit?usp=sharing&ouid=116086546581088882175&rtpof=true&sd=true>

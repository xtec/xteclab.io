---
title: Usuaris
description: Una màquina Linux està compartida per molts usuaris, la majoria són usuaris de sistema i altres són usuaris "humans".
---

## Introducció

Crea una màquina virtual amb {% link "/tool/box/" %} tal com s'explica a {% link "/windows/wsl/" %}: 

```pwsh
> connect-wsl user -new
```

Com probablement saps, cada sistema Linux té una entitat anomenada Usuari que realitza una sèrie de tasques de gestió del sistema. Cada usuari tindrà un ID únic anomenat UID (ID d'usuari) i GID (ID de grup). De la mateixa manera, hi ha una altra entitat disponible anomenada grups que no és altra cosa que la col·lecció d'usuaris que té el seu propi paper. La gestió d'usuaris i grups és una part integral de l'administració del sistema Linux que s'ha d'entendre amb detall.

Bàsicament hi ha dos tipus d'usuaris a Linux:

* Usuaris humans
* Usuaris del sistema

### Usuaris humans

Cada usuari té un únic `UID` (User ID) i `GID`(Group ID). Sempre que es crea un usuari, és propietari d'un directori personal on es poden emmagatzemar tots els fitxers i carpetes personals.
Exemple. Creea un nou usuari. Observarem que cada vegada que es crea un nou usuari, també es crea el seu directori d'inici:

```sh
$ ls /home
box

$ sudo adduser eric
Adding user `eric' ...
Adding new group `eric' (1002) ...
Adding new user `eric' (1001) with group `eric' ...
Creating home directory `/home/eric' ...
Copying files from `/etc/skel' ...
New password:
Retype new password:
passwd: password updated successfully
Changing the user information for eric
...
Is the information correct? [Y/n] y
```

```sh 
$ ls /home/
box  eric
```

Els usuaris humans són només de dos tipus:

1. Usuari `root`. També conegut com a superusuari que té tots els privilegis i té tot el control per fer qualsevol cosa al sistema.

2. Usuari comú. També conegut com a usuaris sense privilegis té drets limitats per operar amb els fitxers i el directori que els pertany . Els usuaris normals poden rebre diferents nivells de privilegis o privilegis complet de root segons la necessitat i el requisit.

Per suprimir un compte d'usuari i el seu grup principal, utilitzeu la sintaxi següent:

```sh
$ sudo userdel eric
$ ls /home/
box eric
```

**L'eliminació d'un compte no elimina la seva carpeta d'inici respectiva**. Depèn de tu si vols o no suprimir la carpeta manualment o conservar-la segons les vostres polítiques de retenció desitjades.

Recorda que qualsevol usuari afegit posteriorment amb el mateix UID/GID que l'anterior propietari ara tindrà accés a aquesta carpeta si no heu pres les precaucions necessàries.

És possible que vulgueu canviar aquests valors UID/GID per alguna cosa més apropiat, com ara el compte root, i potser fins i tot reubicar la carpeta per evitar conflictes futurs:

```sh
$ sudo chown root:root /home/eric/
$ sudo mv /home/eric /home/archived-eric
$ ls -l /home
total 8
drwxr-x--- 2 root root 4096 Sep 15 07:36 archived-eric
drwxr-x--- 4 box  box  4096 Sep 15 07:36 box
```

Més endavant veurem que fan exactament aquestes comandes

### Usuaris del sistema

Els usuaris del sistema executen principalment serveis i processos del sistema en segon pla, també coneguts com a processos no interactius. Els usuaris del sistema no són propietaris del directori d'inici. Podem trobar tots els detalls dels usuaris creats al fitxer `/etc/passwd` tots els detalls dels grups actius al fitxer `/etc/group`.

L'usuari root té els privilegis per afegir, suprimir i actualitzar qualsevol usuari i grup . A continuació es mostra l'ordre útil per realitzar qualsevol operació sobre usuaris i grups.

#### useradd

L’ordre `useradd` crear un nou usuari de sistema

```sh
$ sudo useradd robot
$ grep robot /etc/passwd
robot:x:1002:1003::/home/robot:/bin/sh
```

Si fas un `ls /home` pots veure que no s’ha creat una carpeta d’usuari:

```sh
$ ls /home
archived-eric box
```

Abans hem vist que el fitxer `/etc/passwd` diu que el home de robot és `/home/robot`, però si no té carpeta llavors que passa:

```sh
box@user:~$ sudo su robot
$ cd
sh: 1: cd: can't cd to /home/robot
$ exit
box@user:~$
```

El sistema operatiu no distingeix entre usuaris humans i de sistema, intenta anar al directori home indicat, no existeix i dona error.

Si vols crear un usuari de sistema “pur” pots fer servir aquests modificadors en l’ordre `useradd` :

```sh
box@user:~$ sudo useradd -r -d /tmp -s /bin/false discover
box@user:~$ grep discover /etc/passwd
discover:x:998:998::/tmp:/bin/false

box@user:~$ sudo su discover
box@user:~$
```

El home de l'usuari discover és `/tmp` i no pot iniciar una sessió interactiva.

#### userdel

L’ordre `userdel` elimina un usuari existent: 

```sh
$ sudo userdel robot
$ grep robot /etc/passwd
```

`groupadd`  → per crear un nou grup

```sh
$ sudo groupadd smx
$ grep smx /etc/group
smx:x:1003:
```

`groupdel` → per eliminar un grup existent

```sh
$ sudo groupdel smx
$ grep smx /etc/group
```

`usermod` → fer canvis als usuaris existents. Hi ha moltes operacions que es poden realitzar un cop creat l'usuari, com afegir el comentari, canviar la contrasenya, canviar el directori d'inici, etc. 

Un d'aquests exemples es mostra a continuació. 

Estem canviant el directori d'inici de l'usuari alumne des de `/home/alumne` a `/home/david`.

```sh
$ sudo adduser alumne
$ grep alumne /etc/passwd
alumne:x:1001:1002:,,,:/home/alumne:/bin/bash
$ sudo usermod -d /home/david alumne
$ grep alumne /etc/passwd
alumne:x:1001:1002:,,,:/home/david:/bin/bash
``` 

Ara alumne ja no pot accedir al home modificat perquè no existeix la carpeta `/home/david`:

```sh
$ sudo su alumne
$ cd
bash: cd: /home/david: No such file or directory
$
```

`passwd` → crear o canviar la contrasenya per a qualsevol usuari 

```sh
$ sudo passwd alumne
New password:
Retype new password:
passwd: password updated successfully
```

### Com trobar l'UID i el GID d'un usuari

Per trobar l'UID o el GID de qualsevol usuari, només cal que executeu l'ordre següent.

`id` → per veure l'UID i el GID de l'usuari actual (puc veure a tots els grups als que pertanyo)

```sh
$ id
uid=1000(box) gid=1001(box) groups=1001(box),27(sudo),1000(docker)
```

Aquesta informació està en el fitxer `/etc/group`:

```sh
$ grep box /etc/group
sudo:x:27:box
docker:x:1000:box
box:x:1001:
```

`id user_name` → per veure l'UID i el GID d'un usuari concret 

```sh
$ id alumne
uid=1001(alumne) gid=1002(alumne) groups=1002(alumne)
```

### Crea un usuari del sistema

Els usuaris del sistema també es poden crear utilitzant `useradd`  amb alguns indicadors addicionals a l'ordre. A continuació estem creant un usuari del sistema anomenat `robot` on

* `r` → crear un usuari del sistema amb un ID real en l'interval numèric correcte per als usuaris del sistema
* `s` → especifica l'intèrpret d'ordres d'inici de sessió
* `/bin/false` → ordre dummy que impedeix que l'usuari iniciï sessió al sistema.

```sh
$ sudo useradd -rs /bin/false robot
$ grep robot /etc/passwd
robot:x:998:998::/home/robot:/bin/false
$ grep alumne /etc/passwd
alumne:x:1001:1002:,,,:/home/david:/bin/bash
```

## root

En tot sistema Linux existeix un usuari `root` que pot fer el que vulgui en el sistema operatiu.

Els desenvolupadors d'Ubuntu van prendre una decisió conscient de desactivar el compte administratiu root de manera predeterminada a totes les instal·lacions d'Ubuntu. Això no vol dir que el compte root s'hagi suprimit o que no s'hi pugui accedir. Simplement se li ha donat un hash de contrasenya que no coincideix amb cap valor possible, per tant pot no iniciar sessió directament per si mateix.

En canvi, es recomana als usuaris que facin ús d'una eina amb el nom de `sudo` (tal com hem fet abans) per dur a terme les tasques administratives del sistema. `sudo` permet a un usuari autoritzat elevar temporalment els seus privilegis utilitzant la seva pròpia contrasenya en lloc d'haver de conèixer la contrasenya que pertany al compte root. Aquesta metodologia senzilla però eficaç proporciona responsabilitat per a totes les accions de l'usuari i ofereix a l'administrador un control granular sobre quines accions pot realitzar un usuari amb aquests privilegis.

Si per algun motiu vols treballar directament com a root pots executar aquesta comanda:

```sh
box@user:~$ sudo su
root@user:/home/box#
```
Com pots veure el prompt ha canviat:

* L’usuari és root enlloc de box.
* La carpeta `/home/box` no és el directori d’inici de root, per això es mostra el camí complet enlloc de fer servir el símbol `~`.
* El símbol final és `#` (de root) enlloc de `$` (qualsevol usuari menys root) 

Si vols anar al directori d'inici de root:

```sh
# cd
# pwd
/root
```

Per tornar a l’usuari anterior fes servir la comanda `exit` o Ctrl + D:

```sh
# exit
exit
$
```

### sudo

Sudo significa **SuperUser DO** i s'utilitza per accedir a fitxers i operacions restringides. De manera predeterminada, Linux restringeix l'accés a determinades parts del sistema evitant que els fitxers sensibles es vegin compromesos.

L’ordre `sudo` eleva temporalment els privilegis permetent als usuaris completar tasques sensibles sense iniciar sessió com a usuari root.

Per a la majoria de distribucions modernes de Linux, un usuari ha d'estar al grup `sudo`, `sudoers` o `wheel` per utilitzar l’ordre `sudo`.

A Debian/Ubuntu, el grup `sudo` controla els usuaris `sudo`.

Com has comprovat abans l’usuari box pot executar l'ordre `sudo` ja que pertany al grup sudo.

### Exemple

Si canviem a l'usuari `alumne`, podem verificar que aquest no pot fer servir la comanda `sudo`:

```sh
box@user:~$ sudo su alumne
alumne@user:/home/box$ sudo apt update
[sudo] password for alumne:
alumne is not in the sudoers file.  This incident will be reported.
alumne@user:/home/box$ exit
exit
box@user:~$
```

Ara afegim a l'usuari alumne al grup `sudo` amb l'ordre següent:

```sh
$ sudo usermod -aG sudo alumne
$ id alumne
uid=1003(alumne) gid=1004(alumne) groups=1004(alumne),27(sudo)
$ grep ^sudo /etc/group
sudo:x:27:box,alumne
```

Ara podem canviar a l'usuari `alumne` i podem fer servir l'ordre `sudo`:

```sh
box@user:~$ sudo su alumne
To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.

alumne@user:/home/box$ sudo apt update
[sudo] password for alumne:
Get:1 http://security.ubuntu.com/ubuntu jammy-security InRelease [110 kB]
...
alumne@user:/home/box$ exit
exit
box@user:~$
```

Per eliminar a l'usuari `alumne` del grup `sudo`:

```sh
box@user:~$ sudo gpasswd -d alumne sudo
Removing user alumne from group sudo
```

## Contrasenya

Una política de contrasenyes sòlida és un dels aspectes més importants de la vostra postura de seguretat. Moltes bretxes de seguretat reeixides impliquen simples atacs de força bruta i diccionaris contra contrasenyes febles. Si teniu intenció d'oferir qualsevol forma d'accés remot que inclogui el vostre sistema de contrasenyes local, assegureu-vos d'abordar adequadament els requisits mínims de complexitat de la contrasenya, la vida útil màxima de la contrasenya i les auditories freqüents dels vostres sistemes d'autenticació.

### Longitud mínima de la contrasenya

Per defecte, Ubuntu requereix una longitud mínima de contrasenya de 6 caràcters, així com algunes comprovacions bàsiques d'entropia. Aquests valors es controlen al fitxer `/etc/pam.d/common-password`, que es descriu a continuació:

```sh
$ grep ^password /etc/pam.d/common-password
password    	[success=1 default=ignore]  	pam_unix.so obscure yescrypt
password    	requisite                   	pam_deny.so
password    	required                    	pam_permit.so
```

Has d’editar el fitxer amb `nano`:

```sh
$ sudo nano /etc/pam.d/common-password 
```

I fer aquesta modificació (afegir `minlen=8` ):

```
password  [success=1 default=ignore] pam_unix.so obscure yescrypt minlen=8
```

**Nota**. Les comprovacions bàsiques d'entropia de contrasenya i les regles de longitud mínima no s'apliquen a l'administrador que utilitza ordres de nivell sudo per configurar un nou usuari.

#### Activitat

Crea un usuari nou que s’anomeni `toto` amb una contrasenya de menys de 8 caràcters (no cumpleix la política de contrasenyes):

{% sol %} 
```sh
$ sudo adduser toto
Adding user `toto' ...
Adding new group `toto' (1006) ...
Adding new user `toto' (1004) with group `toto' ...
Creating home directory `/home/toto' ...
Copying files from `/etc/skel' ...
New password: pp
Retype new password: pp
```
{% endsol %} 

Prova de canviar la contrasenya de `toto` desde l'usuari `toto` (ell si que ha de cumplir la política de contrasenyes):

{% sol %} 
```sh
$ sudo su toto
$ passwd
Changing password for toto.
Current password: pp
New password: tt
Retype new password: tt
You must choose a longer password.
New password: tttttttt
Retype new password: tttttttt
```
{% endsol %} 

### Caducitat de la contrasenya

Quan crees comptes d'usuari, haus d'establir com a política una edat mínima i màxima de la contrasenya que obligui els usuaris a canviar les seves contrasenyes quan caduquin.

Per veure fàcilment l'estat actual d'un compte d'usuari, utilitzeu la sintaxi següent:

```sh
$ sudo chage -l toto
Last password change                                	: Sep 11, 2023
Password expires                                    	: never
Password inactive                                   	: never
Account expires                                     	: never
Minimum number of days between password change      	: 0
Maximum number of days between password change      	: 99999
Number of days of warning before password expires   	: 7
```

La sortida següent mostra fets interessants sobre el compte d'usuari, és a dir, que no s'apliquen polítiques.

Per establir qualsevol d'aquests valors, només cal que utilitzeu la sintaxi següent i seguiu les instruccions interactives: 

```sh
$ sudo chage toto
Changing the aging information for toto
Enter the new value, or press ENTER for the default

    	Minimum Password Age [0]:
      ...
```

El següent és també un exemple de com podeu canviar manualment la data de caducitat explícita (-E) a 31/01/2024, edat mínima de la contrasenya (-m) de 5 dies, edat màxima de la contrasenya (-M) de 90 dies, inactivitat període (-I) de 30 dies després de la caducitat de la contrasenya i un període d'advertència (-W) de 14 dies abans de la caducitat de la contrasenya:

```sh
sudo chage -E 01/31/2024 -m 5 -M 90 -I 30 -W 14 alumne
```

Per verificar els canvis, utilitzeu la mateixa sintaxi que s'ha esmentat anteriorment:

```sh
$ sudo chage -l alumne
Last password change                                	: Sep 11, 2023
Password expires                                    	: Dec 10, 2023
Password inactive                                   	: Jan 09, 2024
Account expires                                     	: Jan 31, 2024
Minimum number of days between password change      	: 5
Maximum number of days between password change      	: 90
Number of days of warning before password expires   	: 14
```

#### Activitat

Fes que el compte de l'usuari `toto` caduqui avui. 

{% sol %} 
Per exemple si avui estem a 14-09-2023:

```sh
$ sudo chage toto
Changing the aging information for toto
Enter the new value, or press ENTER for the default

    	Minimum Password Age [0]: 15
    	Maximum Password Age [99999]: 1
    	Last Password Change (YYYY-MM-DD) [2023-09-14]:
    	Password Expiration Warning [7]: 0
    	Password Inactive [-1]: 0
    	Account Expiration Date (YYYY-MM-DD) [-1]: 2023-09-14


$ su toto
Password: tttttttt
Your account has expired; please contact your system administrator.
su: Authentication failure
```
{% endsol %}


## Fitxers

Abans de començar, hi ha alguns conceptes molt bàsics que cal entendre abans de saltar a detalls més complexos.

Als sistemes Linux, els drets es divideixen essencialment en dues categories:

* **Propietat** : l'usuari i el grup propietaris del fitxer, és a dir, o bé l'han creat o se'ls ha assignat com a propietaris del fitxer o directori.

* **Permisos** : donat un fitxer o un directori, els permisos representen el conjunt d'accions que podeu dur a terme en funció de qui sou (el compte d'usuari al qual heu iniciat sessió) i el grup al qual pertanyeu.

Per saber quin és el teu usuari i a quins grups pertany:

```sh
$ whoami     # Usuari
box
$ groups     # Grups als que pertany l’usuari
box sudo docker
```

La manera més habitual de comprovar els permisos de Linux és mitjançant l'ordre `ls`, amb l'opció  `la` (**l**ist **a**ll)

```sh
$ cd
$ ls -la
total 48
drwxr-x--- 6 box  box  4096 Aug 23 16:22 .
drwxr-xr-x 4 root root 4096 Aug 22 10:39 ..
-rw------- 1 box  box  2778 Aug 23 17:00 .bash_history
-rw-r--r-- 1 box  box   220 Jan  6  2022 .bash_logout
-rw-r--r-- 1 box  box  3771 Jan  6  2022 .bashrc
drwx------ 2 box  box  4096 Aug 22 08:42 .cache
-rw-rw-r-- 1 box  box	 59 Aug 22 10:40 .gitconfig
-rw------- 1 box  box	 33 Aug 22 15:56 .lesshst
drwxrwxr-x 3 box  box  4096 Aug 23 16:11 .local
-rw-r--r-- 1 box  box   807 Jan  6  2022 .profile
drwx------ 2 box  box  4096 Aug 22 10:39 .ssh
-rw-r--r-- 1 box  box 	  0 Aug 22 10:36 .sudo_as_admin_successful
```

Aquests són els **permisos** del teu usuari (`box`) al teu directori d'inici.

Però què representen aquestes columnes?

{% image "ls.png" %}

### Entendre els tipus de fitxers de Linux

Probablement ho heu sentit abans, però a Linux, [tot és un fitxer](https://www.howtogeek.com/117939/htg-explains-what-everything-is-a-file-means-on-linux/) (TODO: incorporar).

Com a conseqüència, **els enllaços són fitxers, però els directoris també són fitxers**.

Quan llegiu la primera columna de la sortida de `ls`, has de parar atenció al primer bit.

Els fitxers Linux poden tenir diversos tipus, però la majoria de les vegades són un **fitxer** (`-`) , un **directori** (`d`) o un **enllaç** (link: `l`) .

```sh
box@shell:~$ ls -la
total 48
drwxr-x--- 6 box  box  4096 Aug 23 16:22 .
drwxr-xr-x 4 root root 4096 Aug 22 10:39 ..
-rw------- 1 box  box  2778 Aug 23 17:00 .bash_history
-rw-r--r-- 1 box  box   220 Jan  6  2022 .bash_logout
-rw-r--r-- 1 box  box  3771 Jan  6  2022 .bashrc
drwx------ 2 box  box  4096 Aug 22 08:42 .cache
-rw-rw-r-- 1 box  box	 59 Aug 22 10:40 .gitconfig
-rw------- 1 box  box	 33 Aug 22 15:56 .lesshst
drwxrwxr-x 3 box  box  4096 Aug 23 16:11 .local
-rw-r--r-- 1 box  box   807 Jan  6  2022 .profile
drwx------ 2 box  box  4096 Aug 22 10:39 .ssh
-rw-r--r-- 1 box  box 	  0 Aug 22 10:36 .sudo_as_admin_successful
```

Quan executes ls `-la` en el directori d’inici pots veure que `.cache`, `.local` i `.ssh` són directoris, mentres que `.bash_history`, `.bash_logout`, etc. són fitxers.

A continuació creem un fitxer i un enllaç al fitxer (**l**ink) per veure la sortida amb `ls`:

```sh
$ echo "Hola" > hello.txt
$ ln -s hello.txt  hello-link.txt
$ ls -la | grep hello
lrwxrwxrwx 1 box  box 	9 Aug 28 07:40 hello-link.txt -> hello.txt
-rw-rw-r-- 1 box  box 	0 Aug 28 07:40 hello.txt
```

### Entendre la propietat dels fitxers

Si mires una altra vegada la sortida de l'ordre `ls`, pots veure que `box` apareix en dues columnes separades:

```sh
box@shell:~$ ls -la
total 48
drwxr-x--- 6 box  box  4096 Aug 23 16:22 .
drwxr-xr-x 4 root root 4096 Aug 22 10:39 ..
-rw------- 1 box  box  2778 Aug 23 17:00 .bash_history
...
```

La tercera columna s'anomena columna **"usuari"** i està dedicada a mostrar qui és realment el propietari del fitxer.

En aquest cas, com que estic al meu directori d'inici, sóc el **propietari** real d'aquest fitxer.

Si vols veure tots els usuaris del sistema, pot mirar el contingut del fitxer `/etc/passwd`. 

Com pots suposar l’usuari box serà un dels usuaris que estarà en el fitxer:

```sh
$ more /etc/passwd | grep box
box:x:1000:1001::/home/box:/bin/bash
```

La quarta columna s'anomena **"grup"**.

A Linux, **els usuaris pertanyen a grups**, per exemple, el grup d'administradors, el grup sudo o el grup d'usuaris normal.

En aquest cas, l’usuari `box` ha de pertanyer a un grup si o si, i com que s'ha de crear un grup si o si, la solució més senzilla es crear un grup amb el mateix nom: `box`.

A problemes difícils solucions senzilles 😂!

Com podeu veure a la segona línia, la carpeta `..` (que és en aquest cas el directori `/home`) és propietat de l'usuari `root` que pertany a un grup ...  adivina ...  anomenat `root` 😮‍.

Si vols veure tots els grups del sistema, pots mirar el contingut del fitxer `/etc/group`. 

El group `box` en forma part:

```sh
$ more /etc/group | grep ^box
box:x:1001:
```

El símbol `^` indica que només volem veure aquelles linies que comencen amb `box`. 

Si no fem servir aquest símbol la sortida seria aquesta:

```sh
$ more /etc/group | grep box
sudo:x:27:box,ubuntu
docker:x:1000:box
box:x:1001:
```

Pots veure l'usuari `box` pertany als grups `sudo`, `docker` i `box` (per defecte), i el grup `box` només té l'usuari `box`.

### Entendre els permisos de fitxers

#### Permisos de fitxers

Ara que tens una millor comprensió dels diferents tipus de fitxers, és hora de centrar-te en la resta de la primera columna: **els permisos**.

Els permisos es divideixen en tres categories: **permisos d'usuari**, **permisos de grup** i els **"altres" permisos**.

A cadascuna d'aquestes categories, tens una lletra o un guió:

* Les lletres poden ser `r` (**r**ead) per a l'accés de **lectura** , `w` (**w**rite) per a l'accés d'**escriptura** i `x` (e**x**ecute) per al permís per **"executar-lo"**.

* Un guió vol dir simplement que no tens permís.


Fem una ullada a la primera línia de la nostra sortida de `ls` anterior.

```sh
-rw------- 1 box  box  2778 Aug 23 17:00 .bash_history
```

* El primer guió indica que el `.bash_history` és un fitxer.
* Aleshores, per a l'usuari, teniu els següents permisos establerts: `rw-`, que significa que l'usuari `box` pot llegir i escriure al fitxer però no executar-lo.
* Per al grup, teniu els següents permisos establerts: `---`, el que significa que el grup `box` no pot llegir, escriure ni executar el fitxer.

* Finalment, per als “altres”, tens els mateixos permisos que l’usuari group que significa que no poden fer cap acció amb aquest fitxer.

Per exemple, en aquest fitxer:

```sh
-rw-r--r-- 1 box  box  3771 Jan  6  2022 .bashrc
```

* És un fitxer.
* L'usuari `box` pot llegir i modificar el fitxer.
* Els usuaris que pertanyen al grup `box` poden llegir el fitxer.
* Qualsevol usuari (“altres”) pot llegit el fitxer.

Aquí hi ha una taula del que signifiquen els permisos de lectura, escriptura i execució dels fitxers.

| Permís | Descripció |
|-|-|
| `r` (o llegir, **r**ead) | L'usuari, el grup o altres poden llegir el fitxer, amb una ordre com ara cat , o vi (en mode de només lectura) |
| `w` (o escriure, **w**rite) | L'usuari, el grup o altres poden modificar i desar el fitxer amb ordres com `nano` o `vi` |
| `x` (o executar, e**x**ecute) | L'usuari, grup o altres poden executar el fitxer. Això s'utilitza la majoria del temps per als scripts. |

#### Permisos de directori

```sh
drwx------ 2 box  box  4096 Aug 22 08:42 .cache
drwxrwxr-x 3 box  box  4096 Aug 23 16:11 .local
drwx------ 2 box  box  4096 Aug 22 10:39 .ssh
```

Amb els directoris s'apliquen els mateixos permisos de lectura , escriptura i execució que amb elss fitxers perquè un directori és un tipus especial de fitxer que segueix sent un fitxer.

Però què significa que un directori sigui executable, o què podem escriure en un directori?

| Permís | Descripció |
|-|-|
| `r` (o llegir, read) | L'usuari, el grup o altres poden **enumerar** el contingut del directori (utilitzant una ordre `ls` , per exemple) |
| `w` (o escriure, write) | L'usuari, el grup o altres poden **afegir** o **eliminar** fitxers del directori |
| `x` (o executar, execute)  | L'usuari, grup o altres poden atravessar el directori per tenir accés al seu contingut. |

## Gestionar permisos

L'ordre `chmod` modifica els permisos d'un fitxer utilitzant la forma octal o la forma simbòlica .

### Modificació de permisos mitjançant la forma decimal

Ja saps que els ordinadors treballen amb codificació binaria: `0` i `1`.

Per exemple una adreça IP es codifica en 32 bits, encara que quan escrius una adreça IP ho fas amb una anotació especial en format decimal.

Per exemple, la adreça IP `101.34.56.17` en veritat és `01100101.00100010.00111000.00010001`.

I la representació en decimals està molt bé fins que has de fer subneting, per exemple `101.34.56.17/22` 🙄!

Des del principi de la informàtica és fa servir la notació octal perquè és més fàcil, però que segur que no la coneixes perqué potser és massa difícil? o perquè amb decimals ja es suficient? 😂

En resum, Linux fa servir 9 bits per representar els permisos d'un fitxer i tu pots utitlizar la notació octal per representar eld bits.

El sistema de numeració octal ( o en base 9) utilitza els dígits de 0 a 7.

Els nombres octals poden construir-se a partir de nombres binaris **agrupant cada tres dígits** consecutius d'aquests últims (de dreta a esquerra) i obtenint el seu valor decimal.

| Binari | | Octal |
|-|-|-|
| 001 | 0 + 0 + 1 | 1 |
| 101 | 4 + 0 + 1 | 5 |
| 111 | 4 + 2 + 1 | 7 |
| 101 001 | (4 + 0 + 1) (0 + 0 +1) | 5 1 |
| 001 101 010 | (0 + 0 + 1) (4 + 0 + 1) ( 0 + 2 + 0) | 1 5 1 |

La notació `rwx` es representa amb 3 bits, `0` per indicar que no i `1` per indicar que si.

| Permisos | Binari | Octal |
|-|-|-|
| `r--` | 100 | 4 |
| `r-x` | 101 | 5 |
| `---` | 000 | 0 |
| `rwx` | 111 | 7 |

Per representa un permís de fitxer fem servir la notació octal:

| Permís | Binari | Octal |
|-|-|-|
| `rwxrwxrwx` | 111111111 | 777|
| `rw-rw-r--` | 110110100 | 662 |
| `rwx------` | 111000000 | 700 |


Per modificar els permisos mitjançant la forma octal, has de seguir aquesta sintaxi:

Crea un fitxer `hello.txt`.

```sh
$ touch hello.txt
$ ls -l hello.txt
-rw-r--r-- 1 david david 0 Aug  9 13:18 hello.txt
```

Pots veure que els permisos del fitxer `hello.txt` són `rw-r--r--` o `644`.

Modifica els permisos del fitxer:

```sh
$ chmod 400 hello.txt
$ ls -l hello.txt
-rw------- 1 david david 0 Aug  9 13:18 hello.txt
```

Has modificat els permisos a `600` que en llenguatge "humà" vol dir `rw-------`.

A continuació es mostren alguns exemples dels permisos de fitxer resultants donats diferents operacions chmod.

| Comandament | Permisos resultants |
| chmod 777 fitxer | `rwxrwxrwx` ( no recomanat! ) |
| chmod 444 fitxer | r--r--r-- ( només permisos de lectura ) |
| chmod 421 fitxer | r---w---x (el propietari pot llegir , el grup pot escriure , els altres poden executar ) |
| chmod 000 fitxer | --------- ( no hi ha cap permís) |

#### Activitat

Crea un fitxer `coyote.txt`, canvia els permisos a `---------` i mira que passa si el vols llegir:

{% sol %}
```sh
$ echo "coyote" > coyote.txt
$ ls -l coyote.txt
-rw-rw-r-- 1 box box 7 Sep 20 15:57 coyote.txt
$ cat coyote.txt
coyote

$ chmod 000 coyote.txt
$ cat coyote.txt
cat: coyote.txt: Permission denied
```
{% endsol %}

Dona només permís de lectura al grup i verifica que l'usuari el pot llegir:

{% sol %}
```sh
$ chmod 040 coyote.txt
$ cat coyote.txt
coyote
```

Com propietari no el pots llegir, però com que el teu usuari pertany al grup si que el pots llegir.
{% endsol %}

### Modificació de permisos mitjançant la forma simbòlica

A vegades l'únic que vols es afegir a eliminar un permís, no tenir que torna a escriure tots els permisos.

Crea un script:

```sh
echo -e '#!/bin/bash\necho Hello $USER' > hello.sh
```

Si vols veure el contingut que s'ha escrit:

```sh
$ more hello.sh
#!/bin/bash
echo Hello $USER
```

Si el vols executar no pots, perquè per defecte els fitxers es creen amb el permís `642`:

```sh
$ ./hello.sh
-bash: ./hello.sh: Permission denied
```

Si mires els permisos del fitxer pots veure que no és executable: 

```sh
$ ls -l hello.sh
-rw-rw-r-- 1 box box   31 Aug 28 08:21 hello.sh
```

Quan es crea un fitxer mai és executable, i a diferència de Windows en que qualsevol fitxer és executable, en Linux sempre s'ha de donar permís d’execució.

La manera més fàcil és dir afegeix al propietari (**u**ser) el permís `x`, que no pas dir modifica el permís de `642` a `742`:

```sh
$ chmod u+x hello.sh
$ ls -l hello.sh
-rwxrw-r-- 1 box box   31 Aug 28 08:21 hello.sh
```

Ja pots executar el fitxer:

```sh
$ ./hello.sh
Hello box
```

I ara fem que ja no sigui executable:

```sh
$ chmod u-x hello.sh
$ ./hello.sh
-bash: ./hello.sh: Permission denied
```

A continuació tens una taula amb diferents exemples:

| Comandament | Conseqüència sobre els permisos |
|-|-|
| chmod u+rwx fitxer | Afegir la lectura, escriptura i execució a l' usuari (o propietari del fitxer) |
| chmod go+r fitxer | Afegint el permís de lectura al grup i a la altres dels categoria |
| chmod o+rx fitxer | Afegint els permisos de lectura i execució a la categoria d'altres |
| chmod u-r fitxer | Eliminació del permís de lectura per al propietari del fitxer. |


```sh
$ chmod 644 coyote.txt
$ chmod u-r coyote.txt
$ ls -l coyote.txt
--w-r--r-- 1 box box 7 Sep 20 15:57 coyote.txt
$ cat coyote.txt
cat: coyote.txt: Permission denied

$ chmod u+r coyote.txt
$ cat coyote.txt
coyote
```

### Canviar propietari del fitxer

**`chown` és una ordre que estableix el propietari d'un fitxer o directori.**

Només es pot utilitzar amb privilegis `sudo`! 

Encara que siguis propietari d'un fitxer no pot transferir la propietat del fitxer a una altre usuari.

Aquí hi ha alguns exemples amb l'ordre `chown`:

| Comandament | Permisos resultants |
|-|-
| chown bob secretfile | S'està assignant a Bob com a propietari del fitxer secret |
| chown bob fitxer1 directoy1 | Assignar a bob com a propietari del fitxer1 i del directori1 |
| chown bob:users file1 | Assignació de bob com a propietari i usuaris com a grup del fitxer1 |
|chown :users file1 | Assignació d'usuaris com a grup per al fitxer1 |

A continuació anem a veure un exemple.

Crea un usuari `piolin`:

```sh
$ sudo adduser piolin
```

Crea un fitxer, canvia el propietari a `coyote` i verifica que encar el pots llegir i modificar:

```sh
$ echo 'Fitxer piolin' > piolin.txt
$ sudo chown piolin piolin.txt
$ ls -l piolin.txt
-rw-rw-r-- 1 piolin box 14 Sep 20 16:48 piolin.txt
``` 

Ja no sóc propietari del fitxer, però el puc modificar?

```sh
$ echo 'On està el gat?' >> piolin.txt
$ cat piolin.txt
Fitxer piolin
On està el gat?
```

Com pots veure al executa l'ordre `ls`, el fitxer encara pertany al grup `box`, i com que l’usuari `box` pertany al grup `box`, pot modificar el fitxer.

```sh
$ sudo chown :piolin piolin.txt
$ echo 'El gat està amagat sota la taula!' >> piolin.txt
-bash: piolin.txt: Permission denied
```

Ara ja no tens permís per modificar el fitxer perquè l’usuari `box` no pertany al grup `coyote`.

També pots modificar el fitxer a l'usuari `root` i grup `root`.

```sh
$ ls -l piolin.txt
-rw-rw-r-- 1 piolin piolin 31 Sep 20 16:49 piolin.txt
```

No pots modificar el fitxer, però el pots llegir?

```sh
$ cat piolin.txt
Fitxer piolin
On està el gat?
```

Com usuari no puc, com grup tampoc, però com altres si!

```sh
$ ls -l piolin.txt
-rw-rw-r-- 1 piolin piolin 31 Sep 20 16:49 piolin.txt
```

### Ús de `chgrp`

Encara que pots fer servir la comanda `chown` per canviar el grup propietari d'un fitxer, existeix l'ordre específica `chgrp` és que estableix la propietat del grup per a un fitxer o un directori.

El poder canviar a la vegada la propietat d’usuari i de grup d’un fitxer amb `chown` és una facilitat perquè acostuma a ser una operació habitual, però si només vols modificar el grup fes servir la comanda `chgrp` perquè així queda perfectament clar que és el que estàs fent.

De la mateixa manera, requereix que s'executin privilegis `sudo`.

Aquests són alguns exemples amb l'ordre chgrp.

| Comandament | Permisos resultants |
|-|-|
| chgrp users file1 | Assignació d'usuaris com a grup per al fitxer1 |
| chgrp -R users directory1 | Aplicació d'usuaris del grup recursiva al directori1 i als fills. |
| chgrp -c users file1 | Assignar el grup d'usuaris al fitxer1 i donar tots els canvis fets al terminal |

### Activitat
El sistema operatiu Linux utilitza ids no noms per gestionar usuaris.

Crea un usuari amb `adduser` i nom `yuriko`:

```sh
$ sudo adduser yuriko
Adding user `yuriko' ...
Adding new group `yuriko' (1007) ...
Adding new user `yuriko' (1006) with group `yuriko' ...
Creating home directory `/home/yuriko' ...
...

$ id yuriko
uid=1006(yuriko) gid=1007(yuriko) groups=1007(yuriko)
```

Borra l’usuari `yuriko`:

```sh
$ sudo userdel  yuriko
```

Al borrar `yuriko` la carpeta de `yuriko` encara existeix:

```sh
$ ls -l /home/
...
drwxr-x--- 2   1006   1007 4096 Sep 20 17:27 yuriko
```

Crea un nou usuari `yuriko` amb `adduser`, canvia a usuari `yuriko` i ves al home de `yuriko`:

```sh
$ sudo adduser yuriko
Adding user `yuriko' ...
Adding new group `yuriko' (1007) ...
Adding new user `yuriko' (1006) with group `yuriko' ...
Creating home directory `/home/yuriko' ...
...

$ id yuriko
uid=1006(yuriko) gid=1007(yuriko) groups=1007(yuriko)

$ sudo su yuriko
$ cd
```

Cap problema.

Borra l'usuari `yuriko` i ara crea un nou usuari `yuriko` amb `useradd`!:

```sh
$ id yuriko
uid=1009(yuriko) gid=1010(yuriko) groups=1010(yuriko)
```

Aquest cop Linux crea un usuari `yuriko` amb un uid diferent!

Si canvies a usuari `yuriko` i intentes anar al home no pots encara que la comanda `ls` mostri que el propietari és `yuriko`.

```sh
$ sudo su yuriko
$ cd
sh: 1: cd: can't cd to /home/yuriko
```

El propietari de la carpeta `/home/yuriko` és `yuriko` amb id 1006, no `yuriko` amb id 1009.

La solució es canviar la propietat del fitxer de yuriko (1006) a yuriko (1009):

```sh
$ sudo chown yuriko:yuriko /home/yuriko
```

El nou usuari `yuriko` ja pot accedir a la carpeta yuriko.

```sh
$ sudo su yuriko
$ cd
$ pwd
/home/yuriko
$
```

El primer usuari yuriko era un usuari humà, l’últim un usuari de sistema !





























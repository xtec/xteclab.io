---
title: apt
layout: default.njk
mermaid: true
description: Instal·lació programes en distribucions basades en Ubuntu (apt) i Debian (paquets deb amb dpkg). 
---

## Instal.lació de programari en distribucions Debian/Ubuntu 

Els sistemes operatius ja proporcionen molts programes útils; alguns en `entorn gràfic` i d’altres per `terminal (les comandes són programes)`, però en el dia a dia a dia segur que necessitem instal·lar més `programari`.

Hem de tenir en compte que `GNU/Linux`, al ser un Sistema Operatiu lliure, té diverses versions anomenades `distribucions`. 

Aquestes, tenen formes diferents de gestionar la `instal·lació del software`.

Per posar-nos en context, una forma de **classificar aquestes distribucions és per la forma amb la que gestionen els programes instal·lats**; i també si són derivades d’altres. 

Aquest tutorial està especialment pensat per a treballar amb distribucions basades en `Debian` i/o amb `Ubuntu`; les més habituals en servidors d'aplicacions web.  

Hi ha moltissimes distribucions estables basades en `Ubuntu` i la seva predecessora `Debian`, algunes de les més usades són: 
- **Mint** 
- **PopOS**
- **LUbuntu**
- **Kali**
- **Zorin**

Amb altres distribucions habituals (RedHat, ArchLinux, Fedora ...) les comandes i els mecanismes d'instal·lació utilitzen comandes diferents.

--- 

## Introducció a apt.

Tot i que programari de Linux es pot gestionar perfectament amb programes gràfics (pex. Centre Software Ubuntu, Synaptic ...), el més habitual és gestionar-lo des del terminal.

En Ubuntu, el gestor de paquets més habitual és `apt` (aptitude), que conté versions estables del programari més habitual.

Per provar `apt`, hem d'obrir el terminal de Linux. La seva icona està representada per una pantalla negra en diverses distribucions (les basades en Debian o Ubuntu). 

{% image "bash-icon.png" %}

També pots obrir-lo amb la combinació de tecles **Ctrl + Alt + T**

## Introducció a dpkg

Ubuntu, Mint i moltes altres distribucions són versions `fork` de Debian. És a dir, que es basen en el codi de Debian.

Per això, apart d'incorporar instal·lador de software `apt` també incorporen el de Debian: `dpkg` que funciona amb paquets `.deb` en comptes de programes sencers.

**En algunes ocasions que indicarem clarament ens interessarà usar comandes de `dpkg`**; tot i que si usem `Ubuntu` o distribucions similars usarem més sovint `apt`.

---

### Tutorial apt i dpkg

Si únicament vols repassar les comandes, recomanem aquest catàleg de 2 pàgines en PDF de les comandes més útils:

- <https://lsi.vc.ehu.eus/pablogn/docencia/manuales/comandos_basicos.pdf>

---

### apt update

Abans d'instal·lar programes heu d'actualitzar el llistat de les últimes versions existents:

```sh
sudo apt update
```

### apt install

Un cop heu executat:

```sh
sudo apt update
```

Podeu instal·lar programes amb `apt install` i el nom del programa. 

Per exemple, podem instal·lar el visor de propietats de maquinari **inxi**:

```sh
sudo apt update
sudo apt install inxi
```

Descomposem les parts de la comanda, per entendre-la:

- **sudo** → Comanda que permet executar com a usuari admin. Alguns programes requereixen aquests permisos.
Alternativa: sudo su 
- **apt** → gestor de paquets que usen distros de Linux basades en Debian: Ubuntu, Mint...
- **update** → Li diem al apt que actualitzi el programari, si cal. Important fer-ho abans d'instal·lar res.
- **install** → Li diem al apt el/s programes/paquets que volem instal.lar (altres opcions: upgrade, autoremove, purge, list...)
- **inxi** → El nom del programa.

Hi ha altres formes per instal·lar programari a distros basades en Debian (paquets de Debian .deb, el nou gestor de paquets Snapd, entre d'altres); que veurem en una altra ocasió.

### Executar programes.

Per executar el programa instal.lat posa el seu nom:

```sh 
inxi -F
```

Fixa't com n'és d'útil el programa **inxi**, que mostra les característiques del teu maquinari.

{% image "inxi-demo.png" %}

Així, todem **executar programes ja preinstal·lats.**

Per exemple, en moltes distros tindreu el navegador `firefox`; per obrir-lo és tant fàcil com cridar-lo des del terminal.

```sh 
firefox https://xtec.dev/
```

I heu de saber que les comandes que usem al terminal també són programes ja instal·lats. Per exemple, si posem:

```sh
ls -la
```

Hem executat un programa que ens mostra el llistat de directoris i fitxers en format llarg, inclosos els ocults.

---

{% panel %}

⚠️A vegades ens pot sortir aquest aquest error a l'instal·lar un programa. ⚠️

{% image "apt-lock.png" %}

**Causa més probable:**
S'han executat les actualitzacions automàtiques

**Possibles solucions:**

1. El podem solucionar tancant i obrint el terminal.
2. Si no funciona la 1, aplicar comanda:

```sh
sudo apt update
```

3. Si no funciona la ni 1 ni la 2, reiniciar la màquina. 
4. Si no voleu reiniciar, podeu matar el procés

```sh
Waiting for cache lock: Could not get lock /var/lib/dpkg/lock-frontend. It is hWaiting for cache lock: Could not get lock /var/lib/dpkg/lock-frontend. It is hWaiting for cache lock: Could not get lock /var/lib/dpkg/lock-frontend. It is h^Cd by process 4293 (unattended-upgr)... 2s
miquel@miquel-VirtualBox:~$ kill -9 4293
```

⚠️ Heu de matar el **procés que hi diu** (en el meu cas té id **4293**); no el que veieu a la captura! ⚠️

{% endpanel %}

**Exercici: Instal.la un programes més amb la comanda apt install i executa'l per veure que fa.**

Programes interessants:
- **Editors de text consola:** nano, vim
- **Editors de text gràfics:** gedit, geany
- **Descàrrega:** wget, curl
- **Utilitats del terminal:** tree, banner
- **Jocs:** csmash, ninvaders, pychess
- **Rendiment:** inxi, htop
- **Reparació(avançat!):** gparted, grub-customizer
- **Visor imatges:** lximage-qt, eog
- **Altres:** oneko, sl, cowsay, fortune


{% sol %}

Instal·lem el visor de directoris en forma d'arbre `tree`:

```sh
sudo apt update
sudo apt install tree
```

Executem-lo dins del nostre directori d'usuari:

```sh
cd /home/isard
tree -h
```

```sh
.
├── [4.0K]  Documents
│   └── [ 12K]  informe.docx
├── [4.0K]  Downloads
│   ├── [700M]  pel·licula.mp4
│   └── [ 50M]  programa.deb
├── [4.0K]  Music
│   └── [  5M]  canço.mp3
├── [4.0K]  Pictures
│   ├── [1.2M]  selfie.jpg
│   └── [4.0K]  vacances
│       └── [2.3M]  platja.png
├── [4.0K]  Videos
└── [4.0K]  Desktop

7 directories, 7 files
```

{% endsol %}

---

{% panel %}

### apt o apt-get

En diversos recursos trobareu `apt-get` o altres en comptes d'`apt`; sobretot antics.

La bona notícia és que funcionament és exactament el mateix; i són 100% compatibles entre si. 

```sh 
apt-get install tree
```

Fa el mateix que:
```sh 
apt install tree
```

{% endpanel %}

### apt list

Per revisar la llista de programes instal·lats executa la comanda:

```sh 
sudo apt list --installed
```

O bé:
```sh 
sudo dpkg -l
```

Fixa't en un detall important respecte l'ús del `sudo`. 

* Si poses `sudo dpkg -l` com a usuari administrador podràs veure la llista del programari que té el teu equip i tots els seus usuaris. 
* Si poses `dpkg -l` només podràs veure els programes que hagis instal·lat únicament per a tu i el programari de la teva distro de Linux, que no requereixi permisos d'administrador.

---

### apt remove

Desinstal·lar un programa és molt senzill.

Per exemple, volem desinstal·lar el joc `ninvaders`.

```sh 
sudo apt remove ninvaders -y
```

La `-y` és per acceptar la desinstal·lació sense que ens pregunti `[S/N]`.


A vegades cal aplicar una comanda si tenim **paquets (llibreries de programes) d’un programa incompatibles entre si**; normalment se us indica; per exemple:

```sh 
sudo apt autoremove 
```

I això resoldra els problemes automàticament!

---

### apt dist-<full>-upgrade

Quan necessitem actualitzar tot el sistema operatiu, tenim les comandes:

- **dist-upgrade**, per a actualitzar el sistema sense canviar la distribució arrel (pex. passar d'Ubuntu 22.04.3 a la 22.04.6)
- **full-upgrade**, per actualitzar el sistema i la distribució sencera, incloent el Kernel (pex. passar d'Ubuntu de la 22 a la 24 i el Kernel de la versió 6 a la 7)

⚠️⚠️ Queda fora de l'abast d'aquest tutorial explicar com aplicar actualitzacions de les distribucions o el Kernel del sistema operatiu GNU/Linux.


#### Resum diferències apt dist-upgrde i full-upgrade.
- <https://ubunlog.com/similitudes-y-diferencias-entre-update-upgrade-dist-upgrade-y-full-upgrade/>

---

### add apt repository

TODO:

- <https://launchpad.net/~danielrichter2007/+archive/ubuntu/grub-customizer?ref=itsfoss.com>

---

### Instal·lació paquets Debian.

A vegades necessitem instal·lar algún programa que no ha estat incorporat a aptitude (apt); per exemple, últimes versions d’un navegador, de Visual Studio Code, de Packet Tracer…

Anem a provar d’instal·lar Google Chrome des de la web oficial; des de dins de Linux:

- [Enllaç descàrrega web Google Chrome](https://www.google.com/intl/es_es/chrome/?brand=FHFK&ds_kid=43700059038707842&gclid=CjwKCAiA7t6sBhAiEiwAsaieYuOKEJeeqqlc79LsyK8THJl-hqyZjdhN5fZ_4Xt7Ye20i2H8vg2m1hoCfjQQAvD_BwE&gclsrc=aw.ds)

**Seleccionem la opció .deb** 

Això ens descarregarà un arxiu `.deb` (que vol dir programa de Linux Debian).

Un cop descarregat, si l’intentem obrir pot ser que no funcioni.

Per instal·lar el Chrome, el millor és entrar al terminal, a la carpeta de Descargas, i executar la comanda dpkg (per instal·lar paquets de Debian, distribució pare d’Ubuntu)

```sh
sudo dpkg -i google-chrome-stable_current_amd64.deb
```

{% image "deb-install.png" %}



- <https://medium.com/@nickjabs/understanding-package-management-on-ubuntu-debian-vs-red-hat-based-systems-c55fcd4cafb4>

TODO:
- <https://docs.google.com/document/d/12rVm_I8Be5W8i0uvPE3wkoSie5hKZco3BDpfHuhvXMY/edit?tab=t.0>

---

### Recordatori comandes d'ajuda

#### history --> Historial de comandes: 

Hi ha un historial que guarda totes les comandes que has executat.

Amb les fletxes del teclat `amunt` i `avall` et carrega les comandes que has posat anteriorment (així no et cal escriure-les de nou).

Amb la instrucció **history** veus totes les comandes que has usat fins ara:

```sh
$ history
```

Fins i tot, pots guardar en un fitxer totes les comandes que has fet: 

```sh
$ history > historial.txt
```

#### man --> Manual d'Ajuda. 

La comanda **man** (de manual) és la que ens permet consultar com funciona cada comanda.

Pensa que el terminal de `GNU/Linux` està basat en el del seu sistema predecessor `Unix` (creat als 70) i llavors era especialment útil tenir localitzada l'ajuda, ja que no hi havia Internet.

I ho segueix sent, ja que ens estalviem sortir del terminal per consultar ajuda.

Prova de consultar com funciona la comanda `ls`:

```sh 
man ls
```

Per sortir, pitja q.
 
---

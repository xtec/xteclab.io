---
title: Fitxer
---

## Introducció

És habitual treballar amb un conjunt de bytes que es gestionen com un tot.

Aquest bytes poden representar una imatge, una película, una cançó, però des del punt de vista del sistema operatiu són bytes i el que representen no l'interessa, només que molts cops els vols guardar en un sistema de fitxers del disc dur o d'una unitat extraible.

És molt important que entenguis que és un fitxer pel sistema operatiu per entender perqué escribim programes d'una manera determinada.

### Què és un fitxer

El sistema de fitxers treballa amb inodes (index nodes) que són un conjunt de metadades com el nom del fitxer, permisos d'accés, etc. que s'associen al conjunt de bytes que és el contingut del fitxer.

Encara que les dades més importants d'un inode són les adreces físiques on està el contigut del fitxer!

Com ja saps una de les metadates més importants és el nom perquè et dona molta informació respecte el contingut del fitxer.

Però és important per a tú, al sistema operatiu no l'interessa per res. 

L'única limitació es respecte els noms que pots utilitzar, no pel seu significat (pots escriure una paraulota si vols), sinó perquè no permeen alguns caracters com espais en blanc, etc. coses que pel sistema operatiu són importants i que abans o després tindras que apendre.

### Extensions de fitxers

Com hem explicat abans un inode té moltes metadades, però resulta que no en té una de molt important per a nosaltres: quin és el format del fitxer.

Com ja deus saber un fitxer de text no el puc obrir amb el reproductor de música, i una canço de "Supertramp" (ep, ja tinc una certa edat!) no la puc obrir amb l'Office.

Quan es van dissenyar el primers sistemes operatius van pensar que sabner quina aplicació has de fer servir per obrir un fitxer és assumpte teu.

Genial! Excepte que això és una mic caòtic i van apareixer les extensions de fitxers, que es afegir un `.` i un codi d'extensió al nom del fitxer per indicar quin és el format del contingut del fitxer.

A continuació tens alguns exemples;

| Extensió | Exemple | Descripció |
|-|-|
| `txt` | informet.txt | Fitxers de text sense format |
| `jpg` | everest.jpg | Imatge amb format jpg |
| `mp3` | the-river.mp3 | Fitxer de música |

Però l'extensió de fitxer no converteix un conjunt de dades en un format concrect, és només una informació que pot estar equivocada.

Als Windows no els importa, és el que fan servir per decidir quin format té un fitxer.

Mira que Microsoft té diners, però clients poc exigents. 

En canvi els sistemes operatius Linux no fan cas de l'extensió i fan servir el que s'anomena "magic numbers.

Per exemple, un fitxer `jpg` sempre comenca pels bytes `FFD8`, i un Linux sempre reconeixerà que el contingut d'un fitxer és jpg encara que l'entensió del nom del fitxer no sigui `.jpg`.

Arrenca una màquina WSL i verifica el que acabem d'explicar.

### Metadades del fitxer

Metadades significa "dades sobre dades". 

El contingut del fitxer són dades, encara que per ser exactes són una seqüència de bytes.

Una canço és una seqüeǹcia de bytes, el mateix una película, però com que al principi els ordinadors només treballaven amb dades, aquest és el nom que ha quedat.

No és una cosa tant extranya si penses que la potència d'un motor es mesura en cavalls, de quan es comparava un cotxe de gasolina amb un carro tirat per cavalls.

**Quan escrivim o llegim el contingut de fitxers, escribim o llegim bytes, no dades!**

Les metadates més habituals d'un fitxer, a més del nom que només ens interessa a nosaltres, són permisos d'accés, escritura i execució, la mida del fitxer, l'hora de creació, l'hora de l'últim accés, etc.

### Camins absoluts i relatius

Un sistema de fitxers s'organitza mitjançant carpetes que poden tenir fitxers o carpetes.

És una analogia de quan els documents es guardaven en arxivadors físics.

Quan vols accedir a un fitxer, has d'indicar la localització del fitxer mitjançant un "path", que és el camí que ha de recorrer el sistema operatiu per localitzar el fitxer.

Per exemple, si el "path" és aquest:

```sh
$ cat /home/david/hello.txt
```

El sistema operatiu:

1. Ha d'accedira a la carpeta arrel "/" per saber quin inode correspon el nom `home`
2. A  continuació accedir al inode "home" per saber quin inode correspon la carpeta `david`
3. Finalment accedir al inode "david" per saber quin inode correspon el nom `hello.txt` : 

```
/ -> home -> david -> hello.txt
```

El path que hem descrit és absolut perquè descriu una ruta desde el directori arrel.

També pots utilitzar un "path" relatiu:

```sh
$ pwd
/home/david
$ cat hello.txt
```

Quan obres una sessió de shell, aquest sempre té un directori de treball (**p**ath **w**orking **d**irectory).

Per aquest motiu pots utilitzar la ruta relativa `hello.txt` perqué el shell ja sap com transformar aquesta ruta en una ruta absoluta.

I per acabar, una fet bastant evident, no pot haver-hi dos fitxers amb el mateix nom en una mateixa carpeta.
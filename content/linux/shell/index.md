---
title: Shell
mermaid: true
description: El shell és un terminal que et permet interactuar amb la màquina mitjançant ordres escrites.
---

## Introducció

{% image "gui-vs-cli.png" %}

Els humans i els ordinadors solen interactuar de moltes maneres diferents, com ara mitjançant un teclat i un ratolí, interfícies de pantalla tàctil o mitjançant sistemes de reconeixement de veu. La forma més utilitzada per interactuar amb ordinadors personals s'anomena **interfície gràfica d'usuari (GUI)**. Amb una GUI, donem instruccions fent clic amb el ratolí i utilitzant interaccions basades en menús.
 
Tot i que l'ajuda visual d'una GUI fa que l'aprenentatge sigui intuïtiu, aquesta manera de lliurar instruccions a un ordinador s'escala molt malament. Imagineu la tasca següent: per a una cerca bibliogràfica, heu de copiar la tercera línia de mil fitxers de text en mil directoris diferents i enganxar-la en un sol fitxer. Si utilitzeu una GUI, no només fareu clic al vostre escriptori durant diverses hores, sinó que també pots cometre un error en el procés de completar aquesta tasca repetitiva. Aquí és on aprofitem l'intèrpret d'ordres Unix. 

L'intèrpret d'ordres Unix és alhora una **interfície de línia d'ordres (CLI)** i un llenguatge de script, que permet que aquestes tasques repetitives es facin automàticament i ràpidament. Amb les ordres adequades, l'intèrpret d'ordres pot repetir tasques amb o sense alguna modificació tantes vegades com vulguem. Utilitzant l'intèrpret d'ordres, la tasca de l'exemple de la literatura es pot realitzar en segons

## Shell

Crea una màquina virtual amb {% link "/tool/box/" %} tal com s'explica a {% link "/windows/wsl/" %}:

```pwsh
> Connect-Wsl shell -New
```

Al finalitzar l'activitat borra la màquina amb:

```pwsh
> Remove-Wsl shell
```

El `shell` és un programa on els usuaris poden escriure ordres. Amb l'intèrpret d'ordres, és possible invocar programes complicats com el programari de modelització del clima o ordres simples que creen un directori buit amb només una línia de codi. L'**intèrpret d'ordres Unix més popular és Bash** (el Bourne Again SHell, anomenat així perquè es deriva d'un shell escrit per Stephen Bourne).

L'ús d’un Shell requerirà un esforç i una mica de temps per aprendre. Tot i que una GUI us ofereix opcions per seleccionar, les opcions CLI no se us presenten automàticament, de manera que heu d'aprendre algunes ordres com ara vocabulari nou en un idioma que esteu estudiant. Tanmateix, a diferència d'una llengua parlada, un petit nombre de "paraules" (és a dir, ordres) us permetrà un llarg camí, i avui en parlarem d'aquestes poques.

La gramàtica d'un shell us permet combinar les eines existents en potents canalitzacions i gestionar grans volums de dades automàticament. Les seqüències d'ordres es poden escriure en un script, millorant la reproductibilitat dels fluxos de treball. 

A més, la línia d'ordres és sovint la manera més senzilla d'interactuar amb màquines remotes i superordinadors. La familiaritat amb l'intèrpret d'ordres és gairebé essencial per executar una varietat d'eines i recursos especialitzats, inclosos sistemes informàtics d'alt rendiment. A mesura que els clústers i els sistemes de computació en núvol es fan més populars per a l'anàlisi de dades científiques, poder interactuar amb el shell s'està convertint en una habilitat necessària. Podem basar-nos en les habilitats de línia d'ordres que es tracten aquí per abordar una àmplia gamma de qüestions científiques i reptes computacionals.

Quan l'intèrpret d'ordres s'obre per primera vegada, et mostra un missatge que indica que l'intèrpret d'ordres està esperant l'entrada.

```sh
$
```

El shell normalment utilitza `$` com a indicació, però pot utilitzar a símbol diferent. En els exemples d'aquesta lliçó, mostrarem la sol·licitud com `$`. 

**Important**. Només has d’escriure l'ordre que segueix la sol·licitud sense `$`. Tingues en compte també que després d'escriure una ordre, has de prémer el botó `Enter` per executar-lo. 

L'indicador va seguit d'un **cursor de text**, un caràcter que indica la posició on apareixerà la teva escriptura. El cursor és generalment un bloc intermitent o sòlid, però també pot ser un guió baix o a canonada. És possible que l'hagueu vist en un programa d'editor de text, per exemple.

Tingueu en compte que la vostra sol·licitud pot semblar una mica diferent. En particular, els entorns d'intèrpret d'ordres més populars de manera predeterminada poseu el vostre nom d'usuari i el fitxer nom d'amfitrió abans de `$`. 

Aquesta indicació podria semblar, per exemple:

```sh
box@shell $
```

El missatge pot incloure més que això. No et preocupis si el teu prompte no és només un curt `$`. Aquesta activitat no depèn sobre aquesta informació addicional i tampoc hauria d'interposar-vos en el vostre camí. L'únic element important en què centrar-se és el caràcter `$` i més endavant veurem per què.

Així que provem la nostra primera comanda, `ls`, que és l'abreviatura de **list**. 

Aquesta ordre enumerarà el contingut del directori actual:

```sh
$ ls
```

Com que és una distribució servidor el directori de l’usuari box està buit.

Pots indicar de manera explícita el directori que vols que llisti:

```sh
$ ls /home/
box
```

Si l'intèrpret d'ordres no pot trobar un programa el nom del qual és l'ordre que has escrit, imprimirà un missatge d'error com ara:

```sh
$ ks
ks: command not found
```

Això pot passar si l'ordre s'ha escrit malament o si el programa corresponent a aquesta ordre no està instal·lada.


## Navegació per fitxers i directoris

La part del sistema operatiu responsable de la gestió dels fitxers i directoris s'anomena **sistema de fitxers**. Organitza el nostre dades en fitxers, que contenen informació, i directoris (també anomenats 'carpetes'), que contenen fitxers o altres directoris.

Sovint s'utilitzen diverses ordres per crear, inspeccionar, canviar el nom i eliminar fitxers i directoris. Per començar a explorar-los, anirem al nostre obrir la finestra de la closca.

Primer, anem a esbrinar on som executant una ordre anomenada `pwd` (que significa "imprimir el directori de treball" o  "**p**rint **w**orking **d**ir"). 

Els directoris són com llocs , en qualsevol moment mentre estem utilitzant el shell, estem exactament en un lloc anomenat el nostre corrent directori de treball . Les ordres llegeixen i escriuen principalment fitxers al fitxer directori de treball actual, és a dir, "aquí", per saber on ets abans executar una ordre és important. 

`pwd` et mostra on ets:

```sh
$ pwd
/home/box
```

Aquí, la resposta de l'ordinador és `/home/box`, el qual és el directori d'inici de l’usuari `box`.

Per entendre què és un "directori d'inici", fem una ullada a com el sistema de fitxers com un tot està organitzat. Pel bé d'aquest exemple, il·lustrarem el sistema de fitxers a l'ordinador de l’usuari box. 

Després d'aquesta il·lustració, aprendràs ordres per explorar les vostres sistema de fitxers, que es construirà de manera similar, però no serà exactament idèntic.

A l'ordinador de box, el sistema de fitxers té aquest aspecte:

<pre class="mermaid">
flowchart TD
    / --> bin
    / --> dev
    / --> home
    / --> tmp
</pre>

El sistema de fitxers sembla un arbre al revés. 

El directori més alt és el **directori arrel** que conté tota la resta. Per referir-te a ell fas servir directament el caràcter de barra inclinada, `/`; aquest caràcter és la primera barra inclinada a `/home/box`. 

Dins d'aquest directori hi ha diversos altres directoris:

* `bin` (que és on s'emmagatzemen alguns programes integrats)
* `dev` (dispositius units al sistema de fitxers local)
* `home` (on els usuaris es troben els directoris personals) 
* `tmp` (per a fitxers temporals que no s'han d'emmagatzemar a llarg termini), etc. 

Sabem que el nostre directori de treball actual `/home/box` s'emmagatzema a l'interior de `/home` perquè `/home` és la primera part del seu nom. De la mateixa manera, saps que `/home` s’emmagatzema dins del directori arrel `/` perquè el seu nom comença amb `/`.

**Barra inclinada**. Observa que hi ha dos significats pel caràcter `/`.

1. Quan apareix al capdavant d'un nom de fitxer o directori, es refereix a el directori arrel. 
2. Quan apareix dins d'un camí, només és un separador.

A sota `/home`, trobem un directori per a cada usuari amb un compte a la màquina shell, em aquest cas només box. 

<pre class="mermaid">
flowchart TD
    / --> bin
    / --> dev
    / --> home
    / --> tmp
    home --> box
</pre>

Els fitxers de l'usuari box emmagatzemen a `/home/box` i  els fitxers de l'usuari ubuntu emmagatzemen a /home/ubuntu. box és l’usuari dels nostres exemples; per tant, considerem `/home/box` el nostre directori d'inici. 

Normalment, quan obres un nou indicador d'ordres, estaràs al teu directori personal al començar.

### `ls` (list)

Tal com s’ha explicat abans en el directori de l'usuari `box` no hi ha cap directori.

**El primer que farem** és baixar uns fitxers que ens serviran per treballar els diferents exemples:

```sh
$ cd
$ curl https://gitlab.com/xtec/security/-/raw/main/shell-data.tar.gz | tar -xz
```

**Ara** anem a conèixer l'ordre que ens permetrà veure el contingut del nostre propi sistema de fitxers. 

Pots veure què hi ha al nostre directori d'inici executant `ls`: 

```sh
$ ls
shell-data
```

`ls` **imprimeix els noms dels fitxers i directoris** al fitxer directori actual. Podem fer que la seva sortida sigui més comprensible mitjançant l'ús de l’opció `-F` opció que indica a `ls` que classifiqui la sortida afegint un marcador als noms de fitxers i directoris indicant que són:

* un barra inicial `/` indica que és un directori
* `@` indica un enllaç
* `*` indica un executable


Depenent de la configuració predeterminada de l'intèrpret d'ordres, també pots utilitzar colors per indicar si cada entrada és un fitxer o directori.

```sh
$ ls -F 
shell-data/
```

Aquí, pots veure que el directori d'inici només conté **subdirectoris**. Qualsevol nom a la sortida que no ho faci tenen un símbol de classificació són fitxers en l'actual directori de treball.

Si la pantalla està massa desordenada, pots esborrar el teu terminal utilitzant la comanda `clear` o `Ctrl + D`.

Pots accedir a les ordres anteriors utilitzant les tecles  `↑` i  `↓` per moure's línia per línia, o per desplaçant-te pel teu terminal. 

### Ajuda

`ls` té moltes altres opcions . 

N'hi ha dues maneres habituals d'esbrinar com utilitzar una ordre i quines opcions accepta:

1. Pots passar l’opció `--help` a qualsevol comanda, per exemple: `ls --help`
2. Pots llegir el seu manual amb `man` (manual): `man ls`

També hi ha l'opció de Google i ChatGPT, però abans o després descobrirás que aquestes opcions que t'expliquem també són molt interessants, i estan escrites en Anglés ... 😳 (pel professor no és cap problema 🤨)

#### --help

La majoria de les ordres i programes bash que la gent ha escrit per executar-los des de dins de bash, donen suport a l’opció `--help` que mostra més informació sobre com utilitzar l'ordre o el programa.

```sh
$ ls --help
Usage: ls [OPTION]... [FILE]...
List information about the FILEs (the current directory by default).
Sort entries alphabetically if none of -cftuvSUX nor --sort is specified.

Mandatory arguments to long options are mandatory for short options too.
  -a, --all              	do not ignore entries starting with .
...
```

Si intentes utilitzar una opció que no és compatible, `ls` i altres ordres solen imprimir un missatge d'error similar a:

```sh
$ ls -j
ls: invalid option -- 'j'
Try 'ls --help' for more information.
```

### man (manual)

L'altra manera d'aprendre ls és escriure

```sh
$ man ls
```

Aquesta ordre convertirà el vostre terminal en una pàgina amb una descripció de l'ordre `ls` i les seves opcions.

Per navegar per la pàgines man,

* Pots utilitzar `↑` i `↓`per moure's línia per línia

* Provar `B` i la barra d’espai per saltar amunt i avall una pàgina sencera. 

* Per buscar per a un caràcter o paraula en el pàgines man, fes servir la tecla `/` seguit del caràcter o paraula que cerques. De vegades a la cerca donarà lloc a diverses visites. Si és així, us podeu moure entre les hits utilitzant `N` (per avançar) i `Shift + N` (per anar enrere).

Per abandonar el les pàgines man, prem `Q`.

### Explorant altres directoris

No només pots utilitzar `ls` al directori de treball actual, però **el podem utilitzar per llistar el contingut d'un directori diferent**. 

Anem a fer una ullada al nostre directori `shell-data` executant `ls -F shell-data` , és a dir, l'ordre `ls` amb l’opció `-F` i l’argument `shell-data`. 

L'argument `shell-data` diu a ls que volem una llista d'alguna cosa que no sigui el nostra directori de treball actual:

```sh
$ ls -F shell-data
exercise-data/  north-pacific-gyre/
```

Tingues en compte que si passes com argument un directori que no existeix al teu directori de treball actual, aquesta ordre retorna un error:

```sh
$ ls -F kkk
ls: cannot access 'kkk': No such file or directory
```

Organitzar les coses de manera jeràrquica ens ajuda a poder trobar les coses quan les busquem.

Si vols ho pots guardar tot directament al teu directori "home", en aquets cas `/home/box`, Linux no té cap problema, li és completament indiferent.

Però potser **t'interessa més a tu** guardar alguns fitxers en carpetes separades (recorda que a Linux tan li fa) amb noms que expliquin que guarden, per exemple `/home/box/travel/egypt` per guardar les fotografies tel teu viatge a egipte.

Ara que saps que el directori `exercise-data` està situat al directori `shell-data`, pots utilitzar la mateixa estratègia que abans.

Pots mirar-ne el contingut passant un nom de directori a `ls`:

```sh
$ ls -F shell-data/exercise-data
alkanes/  animal-counts/  creatures/  numbers.txt  writing/
```

### Canviar de directori 

Una altra opció es canviar la nostra ubicació a un directori diferent, de manera que ja no ens trobem al nostre directori d'inici.

L'ordre per canviar d'ubicació és `cd` seguit del nom del directori per canviar el nostre directori de treball. 

`cd` vol dir "**c**hange **d**ir", que és una mica enganyós perquè l'ordre no canvia el directori sinó que canvia el directori de treball actual del shell. En altres paraules, canvia la configuració de l'intèrpret d'ordres per a quin directori som. 

L'ordre `cd` és semblant a fer doble clic en una carpeta en una interfície gràfica per entrar a aquesta carpeta.

Per exemple pots entrar al directori `exercise-data`:

```sh
box@shell:~$ pwd
box@shell:~$ cd shell-data
box@shell:~/shell-data$ cd exercise-data
box@shell:~/shell-data/exercise-data$
```

Notaràs que `cd` no imprimeix res. Això és normal. Moltes ordres del shell no mostren res per  pantalla encara que s'hagi executat correctament. 

El que si notarás és que el prompt a canviat perquè et mostra on estás respecte el teu directori.

Si vols conèixer la ruta absoluta pots executar la comanda `pwd` ("**p**ath **w**orking **d**irectory"):

```sh
$ pwd
/home/box/shell-data/exercise-data
```

Si ara executes l'ordre `ls -F` sense arguments, enumerarà els continguts de `/home/box/shell-data` perquè és on estem ara:

```sh
$ ls -F
alkanes/  animal-counts/  creatures/  numbers.txt  writing/
```
Ara sabem com baixar per l'arbre de directoris (és a dir, com entrar a un subdirectori), però com pugem (és a dir, com deixem un directori i anar al seu directori principal)? 

Pots provar el següent:

```sh
$ cd shell-data
-bash: cd: shell-data: No such file or directory
```

Però tenim un error! Per què és això?

Amb els nostres mètodes fins ara, `cd` només pot veure subdirectoris dins del vostre directori actual. 

Hi ha diferents maneres de veure directoris per sobre de la vostra ubicació actual; començarem amb el més simple.

Hi ha una drecera a l'intèrpret d'ordres per pujar un nivell de directori.

Això funciona de la següent manera:

```sh
box@shell:~/shell-data/exercise-data$ cd ..
box@shell:~/shell-data$
```

`..` és un nom de directori especial que significa "el directori que conté aquest”, o més concretament, el pare del directori actual. 

Si executem `pwd` després d’haver executat `cd` pots veure que hem tornat a `/home/box`:

```sh
$ pwd
/home/box/shell-data 
```

El directori especial `..` no apareix quan executes `ls`. 

Si el vols mostrar, pots afegir l'opció `-a` a `ls -F`:

```sh
$ ls -F -a
./  ../  exercise-data/  north-pacific-gyre/
```

`-a` significa **"mostrar-ho tot"** (inclosos els fitxers ocults); això força a `ls` per mostrar-nos els noms de fitxers i directoris que comencen amb `.`, tal com `..` (per exemple si som a `/home/box` fa referència al directori `/home`). 

Com pots veure, també mostra un altre directori especial que s’anomena `.`, que significa "el directori de treball actual". Pot semblar redundant tenir un nom pel directori actual, però aviat veurás alguns usos.

Tingues en compte que a la majoria de les eines de línia d'ordres hi poden haver diverses opcions que es poden combinar amb un sol `-` sense espais entre les opcions; `ls -F -a` és equivalent a `ls -Fa`.

Aquestes tres ordres són les ordres bàsiques per navegar sistema de fitxers al teu ordinador: `pwd`, `ls` i `cd`. 

Què passa si escrius `cd` pel seu compte, sense pasar un directori com argument?

```sh
box@shell:~/shell-data$ cd
box@shell:~$ 
```

Tal com t’indica el prompt i pots verificar amb la comanda `pwd` has tornat al teu directori:

```sh
$ pwd
/home/box
```

Resulta que `cd` sense argument et torna al teu directori d'inici, que és fantàstic si t’has perdut en el teu sistema de fitxers.

Intenta tornar al directori `exercise-data`. La darrera vegada, vas utilitzar dos ordres, però en realitat podem encadenar conjuntament la llista de directoris per mouren's a `exercise-data` en un sol pas:

```sh
box@shell:~$ cd shell-data/exercise-data
box@shell:~/shell-data/exercise-data$
```

Comprova que t'has mogut al lloc correcte executant `pwd` i `ls -F`:

```sh
$ pwd
/home/box/shell-data/exercise-data
$ ls -F
alkanes/  animal-counts/  creatures/  numbers.txt  writing/
```

Si vols pujar un nivell des del directori de `exercise-data` pots utilitzar `cd ..`. 

Però hi ha una altra manera de moure's a qualsevol directori, independentment de la teva ubicació actual.

Fins ara, en especificar noms de directoris, o fins i tot una ruta de directori (com anterior), has estat utilitzant camins relatius . Quan utilitzes un camí relatiu amb una ordre com `ls` o `cd`, l’ordre intenta trobar aquest lloc des d'on som enlloc de desde l’arrel del sistema de fitxers.

**Tanmateix**, és possible especificar el camí absolut a un directori incloent-hi el camí complet des del directori arrel, que s'indica amb una barra inclinada inicial. La barra inclinada `/` diu l'ordinador per seguir el camí des de l'arrel del sistema de fitxers, per tant sempre es refereix a exactament un directori, sense importar on som quan estem executa l'ordre.

Això ens permet passar al nostre directori `shell-data` des de qualsevol lloc del sistema de fitxers (inclòs des de dins `exercise-data`). 

Per trobar el camí absolut que estem buscant podem utilitzar `pwd` i després extreure el troç que necessitem per moure's cap a `shell-data`.

```sh
$ pwd
/home/box/shell-data/exercise-data
$ cd /home/box/shell-data
```

Executa `pwd` i `ls -F` per assegurar-te que som al directori que esperem.

#### Dues dreceres més

L'intèrpret interpreta el caràcter tilda (`~`) al començament d'una ruta com "el directori inicial de l'usuari actual". 

Per exemple, si el directori inicial de box és `/home/box`, llavors `~/shell-data` és equivalent a `/home/box/shell-data`. 

Això només funciona si `~` és el primer caràcter de la ruta; `kkk/~/xxx` no és equivalent a `kkk/home/box/xxx`. 

Una altra drecera és el caràcter `-` (guionet).

`cd -` es tradueix a l'anterior directori on estava, que és més ràpid que haver de recordar i després escriure, el camí complet.

Aquesta és una molt manera eficient de moure's d'anada i tornada entre dos directoris , és a dir, si executes `cd -` dues vegades, acabeu de nou al directori inicial.

La diferència entre `cd ..` i `cd -` és que el primer et fa pujar , mentre que el segon et porta enrere.

Intenta-ho! Primer navega a `~/shell-data` (ja hauríes de ser-hi).

**Ajuda**. Per escriure el caràcter `~` es la combinació `AltGr + 4` (a la vegada) i espai.

```sh
$ cd ~/shell-data
```

Ara fes un `cd` al directori `exercise-data/creatures`:

```sh
$ cd exercise-data/creatures
```

Ara si executes `cd -` veuràs que has tornat a `~/shell-data`

```sh
$ cd -
$ pwd
```

Executa `cd -` de nou i tornes a `~/shell-data/exercise-data/creatures`.

### Camins absoluts vs relatius

Començant des de `/home/box/shell-data` quines de les següents ordres pots utilitzar per navegar al directori d'inici, que és `/home/box`?

```sh
$ cd .
$ cd /
$ cd /home/box
$ cd ../..
$ cd ~
$ cd home
$ cd ~/shell-data/..
$ cd
$ cd ..
```

Prova cada opció (recorda que amb `cd -` pots tornar on estaves abans):

{% sol %}
```sh
$ cd /home/box
$ cd ~
$ cd ~/shell-data/..
$ cd
$ cd ..
```
{% endsol %}

### Tabulador

Torna al directori home de l'usuari box i mostra els fitxers que estan a la carpeta `shell-data/exercise-data/alkanes/`

```sh
$ cd
$ ls shell-data/exercise-data/alkanes/
cubane.pdb  ethane.pdb  methane.pdb  octane.pdb  pentane.pdb  propane.pdb
```

Això és molt a teclejar, però pot permetre que la terminal faci la major part del treball a través del que s'anomena autocompletat amb el tabulador. Si escrius

```sh
$ ls sh
```

i després prem el tabulador (la tecla de tabulador al teclat), la terminal completa automàticament el nom del directori per ella:

```sh
$ ls shell-data/
```

Si pressiones el tabulador de nou no fa res, ja que hi ha 2 possibilitats; pressionar el tabulador dues vegades mostra una llista de tots els fitxers, i així successivament. 
Això s'anomena autocompletat amb el tabulador, i ho veurem en moltes altres eines a mesura que avancem.

## Treballant amb arxius i directoris

Ara sabem com explorar fitxers i directoris, però com els creem en primer lloc?

Torna al directori de l’usuari box i crea un nou directori anomenat `thesis` usant l'ordre `mkdir thesis` (que no genera una sortida):

```sh
$ cd
$ mkdir thesis
```

Com el seu nom suggereix, `mkdir` significa "**m**a**k**e **dir**ectory”, que significa “crear directori” en anglès. 

Atès que `thesis` és una ruta relativa (és a dir, no inicia amb una barra obliqua `/`), el nou directori es crea a la carpeta de treball actual:

```sh
$ ls -F
shell-data/ tesi/
```

**Dues maneres de fer el mateix**. Usar la terminal per crear un directori no és diferent de fer servir un navegador de fitxers gràfic. Si estas en un entorn d'escriptori, pots obrir el directori actual utilitzant el navegador de fitxers gràfic del vostre sistema operatiu, el directori `thesis` apareixerà allà també. Si bé són dues maneres diferents d'interactuar amb els fitxers, els fitxers i els directoris amb què treballem són els mateixos.

**Bona nomenclatura per a arxius i directoris**. Usar noms complicats per a arxius i directoris poden fer la teva vida molt complicada quan es treballa a la línia d'ordres. T'oferim alguns consells útils per nomenar els teus fitxers d'ara endavant:

1. No utilitzeu espais en blanc com fas amb el Windows.

  Els espais en blanc poden fer un nom més significatiu, però, atès que s'utilitzen per separar arguments a la línia d'ordres, és millor evitar-los en noms de fitxers i directoris. Pots utilitzar `-` o `_` en comptes d'espais en blanc.

2. No comences el nom amb un `-` (guió). Les ordres tracten els noms que comencen amb `-` com a opcions.

3. Utilitza únicament lletres, números, `.` (punt), `-` (guió) i `_` (guió baix).

   Molts altres caràcters tenen un significat especial en la línia de comandes i els aprendrem durant aquesta lliçó. Alguns només faran que la teva comanda no funcioni, altres poden fins i tot fer que perdis dades.

Si et cal referir-te a noms de fitxers o directoris que tinguin espais en blanc o un altre caràcter no alfanumèric, cal posar el nom entre cometes dobles (`""`).

Atès que acabem de crear el directori `thesis`, encara es troba buit:

```sh
$ ls -F thesis
```

Canviem el nostre directori de treball a `thesis` utilitzant `cd`, i a continuació, executem un editor de text anomenat [`nano`](https://www.nano-editor.org/) per crear un fitxer am el nom `draft.txt`:

```sh
$ cd thesis
$ nano draft.txt
```

Quin editor utilitzar?. Quan diem, “nano és un editor de text”, realment volem dir “text”: només funciona amb dades de caràcters simples, no amb taules, imatges o qualsevol altre format amigable amb l'usuari com el Word o el OpenOffice.

Als sistemes Unix (com Linux i Mac OS X) molts programadors utilitzen [Emacs](https://www.gnu.org/software/emacs/) o [Vim](https://www.vim.org/), però tots dos requereixen més temps per familiaritzar-se amb ells.

I el que es més important, tots el fitxers de configuració de Linux són en format text, si intentes editarlos amb Word o OpenOffice ...

Escrivim algunes línies de text. Quan estiguem contents amb el nostre text, podem pressionar `Ctrl-O` (pressiona la tecla `Ctrl` o Control, i mentrés la mantens pressionada, preme la tecla `O`) per escriure les dades al disc (se'ns preguntarà a quin arxiu volem guardar això: pressiona `Enter` per acceptar el valor predeterminat suggerit `draft.txt`).

{% image "nano.png" %}

Quan el nostre arxiu està guardat, podem usar `Ctrl-X` per sortir de l'editor i tornar a la terminal.

{% panel "Tecla Control, Ctrl o ^" %}
La tecla Control també s'anomena tecla “Ctrl”. 

Hi ha diverses maneres dindicar l'ús de la tecla Control. 

Per exemple, una instrucció per pressionar la tecla Control i, mentre la mantens premuda, pressionar la tecla X, pot ser descrita de qualsevol de les maneres següents:

* Control-X
* Control+X
* Ctrl-X
* Ctrl+X
* ^X
* C-x

En nano, al llarg de la part inferior de la pantalla es llegeix `^G Get Help ^O WriteOut`. 

Això significa que pots fer servir `Control-G` per obtenir ajuda i `Control-`O per desar el vostre fitxer.
{% endpanel %}


`nano` no deixa cap sortida a la pantalla després que sortir del programa, però `ls` ara mostra que hem creat un fitxer anomenat `draft.txt`:

```sh
$ ls
draft.txt
```

Netegem una mica executant `rm draft.txt`:

```sh
$ rm draft.txt
```

Aquesta ordre elimina fitxers (`rm` és l'abreviatura de **r**e**m**ove, remoure en anglès).

Si executes de nou `ls`, la sortida estarà buida una vegada més, indicant-nos que el nostre arxiu ha desaparegut:

```sh
$ ls
```

{% panel "Eliminar és per sempre" %}
La terminal de Linux **no té una paperera de reciclatge** des d'on puguem restaurar fitxers eliminats (encara que la majoria de les interfícies gràfiques de Linux sí que en tenen).

En el seu lloc, quan eliminem fitxers, aquests es desvinculen del sistema de fitxers perquè el seu espai d'emmagatzematge en disc pugui ser reciclat. 

Hi ha eines per trobar i recuperar arxius eliminats, com veurem a l'activitat {% link "/security/recovery/" %}, però no hi ha garantia que funcionin en totes les situacions, ja que l'ordinador pot reciclar l'espai en disc de larxiu en qüestió immediatament, perdent de manera permanent.
{% endpanel %}

Creem de nou el fitxer i després pugem un directori a `/home/box` utilitzant `cd ..` :

Si intentes eliminar tot el directori `thesis` utilitzant `rm thesis`, obtenim un missatge d'error:

```sh
$ rm thesis/
rm: cannot remove 'thesis/': Is a directory
```
Això passa perquè `rm` normalment treballa només amb fitxers, no amb directoris.

Per realment desfer-nos de `thesis` també hem d'eliminar l'arxiu `draft.txt`. 

Pots fer-ho amb l'opció recursiva per `rm`:

```sh
$ rm -r thesis/
```

**Eliminar els fitxers en un directori recursivament pot ser una operació molt perillosa**. Si ens preocupa què podríem eliminar, podem afegir l'opció “interactiva” `-i` a `rm`, que ens demanarà confirmar cada pas.

```sh
$ rm -ri shell-data/
rm: descend into directory 'shell-data/'? y
rm: descend into directory 'shell-data/north-pacific-gyre'? y
rm: remove regular file 'shell-data/north-pacific-gyre/NENE01843A.txt'?
```

En qualsevol moment pots cancelar amb l'ordre `^C`.

Crearem el directori i el fitxer una vegada més. (Tingues en compte que aquesta vegada estem executant `nano` amb la ruta d'accés `thesis/draft.txt`, en lloc d'anar al directori `thesis` i executar `nano draft.txt`)

```sh
$ ls
$ mkdir thesis
$ nano thesis/draft.txt
$ ls thesis
```

`draft.txt` no és un nom particularment informatiu, així que canviem el nom del fitxer usant l'ordre `mv`, que és l'abreviatura de "**m**o**v**e" (moure):

```sh
$ mv thesis/draft.txt thesis/quotes.txt
```

El primer paràmetre diu a `mv` el que estem "movent", mentre que el segon indica on cal moure'l. 

En aquest cas estem movent `thesis/draft.txt` a `thesis/quotes.txt`, que té el mateix efecte que canviar el nom del fitxer. 

Com esperem, `ls` ens mostra que `thesis` ara conté un fitxer anomenat `quotes.txt`:

```sh
$ ls thesis
quotes.txt
```

**Cal anar amb compte en especificar el nom del fitxer destí**, ja que `mv` reemplaça silenciosament qualsevol fitxer existent amb el mateix nom, provocant pèrdua de dades.

Un flag addicional, `mv -i` (o `mv --interactiu`), es pot utilitzar per fer que `mv` et demani confirmació abans de sobreescriure.

Només pel gust de la consistència, `mv` també funciona en directoris, és a dir, no existeix una ordre separada `mvdir`. 

Mourem `quotes.txt` al directori de treball actual. Utilitzem `mv` una vegada més, però aquesta vegada només farem servir el nom d'un directori com el segon paràmetre per indicar a `mv` que volem mantenir el nom de fitxer, però posar el fitxer en algun lloc nou (és per això que l'ordre s'anomena “moure”.) 

En aquest cas, el nom de directori que fem servir és el nom de directori especial `.` que hem esmentat abans:

```sh
$ mv thesis/quotes.txt .
```

El resultat és moure el fitxer des del directori on era al directori de treball actual. 

`ls` ara ens mostra que tesi està buit:

```sh
$ ls thesis
```

A més, `ls` amb un nom de fitxer o un nom de directori com a paràmetre només llista aquest fitxer o directori. 

Podem fer servir això per veure que `quotes.txt` encara està al nostre directori actual:

```sh
$ ls quotes.txt
quotes.txt
```

L'ordre `cp` funciona de manera semblant a `mv`, excepte que copia un fitxer en lloc de moure'l.

Pots comprovar que va fer el correcte usant `ls` amb dues rutes com a paràmetres — com la majoria de les ordres Linux, `ls` pot rebre múltiples rutes alhora:

```sh
$ cp quotes.txt thesis/quotations.txt
$ ls quotes.txt thesis/quotations.txt
quotes.txt tesi/quotations.txt
```

Per provar que vam fer una còpia, eliminem el fitxer `quotes.txt` del directori actual i després executem el mateix ls novament.

```sh
después ejecutemos el mismo ls de nuevo.
$ rm quotes.txt
$ ls quotes.txt thesis/quotations.txt
ls: cannot access quotes.txt: No such file or directory
thesis/quotations.txt
```

Aquest cop l'error ens diu que no es pot trobar `quotes.txt` al directori actual, però troba la còpia a `thesis` que no hem esborrat.

{% panel "Què hi ha en un nom?" %}
En aquesta part de la lliçó, fem servir sempre l'extensió `.txt`. 

Això és només una convenció: podem anomernar l'arxiu com `mythesis` o gairebé qualsevol cosa que vulguem **en Linux, no pas en Windows 😂 !!**

No obstant això, la majoria de la gent fa servir noms de dues parts perquè sigui més fàcil (per a ells i els seus programes) diferenciar entre tipus de fitxers.

La segona part del nom `thesis.txt` es diu la extensió de nom de fitxer i indica quin tipus de dades conté el fitxer: `.txt` assenyala un fitxer de text sense format, `.pdf` indica un document PDF, `.cfg` és un fitxer de configuració ple de paràmetres per a algun programa, `.png` és una imatge PNG, i així successivament.

Això és només una convenció, encara que una de important. Els fitxers contenen només bytes: depèn de nosaltres i dels nostres programes interpretar aquests bytes d'acord amb les regles per a fitxers de text, documents PDF, fitxers de configuració, imatges, etc.

Anomenar una imatge PNG d'una balena com `balena.mp3` no ho converteix màgicament en un enregistrament del cant de les balenes, encara que podria fer que el sistema operatiu intenti obrir-lo amb un reproductor de música quan algú hi fa doble clic.
{% endpanel %}

## Pipes i filtres

Ara que ja sabem algunes ordres bàsiques, podem veure finalment la característica més poderosa de la terminal: la facilitat amb què ens permet combinar els programes existents de noves maneres. 

Començarem amb un directori anomenat `alkanes` que conté sis fitxers que descriuen algunes molècules orgàniques simples. 

L'extensió `.pdb` indica que aquests fitxers estan en format Protein Data Bank, un format de text simple que especifica el tipus i la posició de cada àtom a la molècula.

```sh
$ ls ~/shell-data/exercise-data/alkanes
cubane.pdb  ethane.pdb  methane.pdb  octane.pdb  pentane.pdb  propane.pdb
$ nano ~/shell-data/exercise-data/alkanes/cubane.pdb 
```

Entra a aquest directori usant l'ordre `cd` i executa la comanda `wc *.pdb`:

```
$ cd ~/shell-data/exercise-data/alkanes/
box@shell:~/shell-data/exercise-data/alkanes$ wc *.pdb
  20  156 1158 cubane.pdb
  12   84  622 ethane.pdb
   9   57  422 methane.pdb
  30  246 1828 octane.pdb
  21  165 1226 pentane.pdb
  15  111  825 propane.pdb
 107  819 6081 total
```


`wc` és l'ordre "**w**ord **c**ount" (recompte de paraules): compte el nombre de línies, paraules i caràcters d'un fitxer. 

El `*` a `*.pdb` coincideix amb zero o més caràcters, per la qual cosa la terminal converteix `*.pdb` en una llista de tots els fitxers `.pdb` al directori actual.

{% panel "Caràcters especials" %}

**`*`** és un caràcter especial o comodí. Correspon a zero o més caràcters, així que `*.pdb` coincideix con `ethane.pdb`, `propane.pdb`, i cada fitxer que acaba amb `.pdb`. 

D'altra banda, `p*.pdb` només coincideix amb `pentane.pdb` i `propane.pdb`, perquè la `p` a l'inici fa coincidir els noms de fitxers que comencen amb la lletra `p`.

**`?`** també és un caràcter especial, però només coincideix amb un sol caràcter. 

Això vol dir que `p?.pdb` pot coincidir amb `pi.pdb` o `p5.pdb` (si existissin al directori `molecules`), però no `propane.pdb`. 

Podem fer servir qualsevol nombre de caràcters especials alhora: per exemple, `p*.p?*` coincideix amb qualsevol cosa que comenci amb una `p` i acabi amb `.`, `p`  i almenys un caràcter més (ja que `?` ha de coincidir amb un caràcter, i el final `*` pot coincidir amb qualsevol nombre de caràcters).

Per tant, `p*.p?*` coincidirà amb `preferred.practice` o inclús `p.pi` (atès que el primer `*` pot no coincidir amb cap caràcter), però no `quality.practice` (ja que no comença amb `p`) o `preferred.p` perquè no hi ha almenys un caràcter després de `.p`.

Quan la terminal reconeix un caràcter especial l'expandeix per crear una llista de noms de fitxer coincidents abans d'executar l'ordre seleccionada.

Com a excepció, si una expressió amb caràcters especials no coincideix amb cap fitxer, la terminal passarà l'expressió com un paràmetre a l'ordre tal com està escrita. 

Per exemple, executar `ls *.pdf` al directori `molecules` (que conté només els fitxers amb noms que acaben amb `.pdb`) dóna com a resultat un missatge d'error indicant que no hi ha cap fitxer anomenat `*.pdf`. 

No obstant això, generalment comandes com `wc` i `ls` veuen les llistes de noms de fitxer que coincideixen amb aquestes expressions, però no els comodins per ells mateixos. És la terminal, no pas els altres programes, la que s'ocupa d'expandir els caràcters especials, aquest és un altre exemple de disseny ortogonal.
{% endpanel %}

**Activitat**. Al directori `alkanes`, quina variació de l'ordre `ls` produirà aquesta sortida: `ethane.pdb methane.pdb`?

```sh
$ ls *t*ane.pdb
$ ls *t?ne.*
$ ls *t??ne.pdb
$ ls ethane.*
```

Si executes `wc -l` en lloc de `wc`, la sortida només mostra el nombre de línies per fitxer:

```sh
$ wc -l *.pdb
  20 cubane.pdb
  12 ethane.pdb
   9 methane.pdb
  30 octane.pdb
  21 pentane.pdb
  15 propane.pdb
 107 total
```

També podem fer servir `wc -w` per obtenir només el nombre de paraules, o `wc -c` per obtenir només el nombre de caràcters.

```sh
$ wc -w *.pdb
$ wc -c *.pdb
```

### Redirect

**Quin d'aquests fitxers és el més curt?** 

És una pregunta fàcil de respondre quan només hi ha sis fitxers, però i si n'hi hagués 6,000? 

El nostre primer pas cap a una solució és executar la comanda:

```sh
$ wc -l *.pdb > lengths.txt

```

El símbol “més gran que” `>` li diu a la terminal que redireccioneu la sortida de l'ordre a un fitxer en lloc d'imprimir-lo a la pantalla. 

És per això que no hi ha sortida de pantalla: en comptes de mostrar-ho, tot allò que `wc` imprimeix s'ha enviat al fitxer `lengths.txt`.

Si no existeix el fitxer, la terminal el crearà. Si el fitxer existeix, serà sobreescrit silenciosament, cosa que pot provocar pèrdua de dades i, per tant, requereix certa precaució. 

`ls lengths.txt` confirma que el fitxer existeix:

```sh
$ ls lengths.txt 
lengths.txt
```

Ara podem enviar el contingut de `lenghts.txt` a la pantalla usant `cat lengths.txt`.

`cat` significa "con**cat**enate" (concatenar): imprimeix el contingut dels fitxers un darrere l'altre. 

En aquest cas només hi ha un fitxer, així que `cat` només ens mostra el que conté:

```sh
$ cat lengths.txt
  20 cubane.pdb
  12 ethane.pdb
   9 methane.pdb
  30 octane.pdb
  21 pentane.pdb
  15 propane.pdb
 107 total
```

Sortida Pàgina per pàgina. Seguirem utilitzant `cat` en aquesta lliçó, per conveniència i consistència, però té el desavantatge que sempre bolca tot l'arxiu a la pantalla. 

A la pràctica és més útil l'ordre `less`, que s'utilitza com `$ less lengths.txt`. 

Aquesta ordre mostra només el contingut del fitxer que cap en una pantalla i després s'atura. Pots avançar a la següent pantalla prement la barra espaiadora, o retrocedir prement `b` (**b**ack). Per sortir, prem `q` (**q**it).

Ara utilitzarem l'ordre `sort` **per ordenar el contingut**. 

També farem servir l'indicador `-n` per especificar que el tipus d'ordre que requerim és numèric en lloc d'alfabètic. 

Això no canvia el fitxer; només desplega el resultat ordenat a la pantalla:

```sh
$ sort -n lengths.txt 
   9 methane.pdb
  12 ethane.pdb
  15 propane.pdb
  20 cubane.pdb
  21 pentane.pdb
  30 octane.pdb
 107 total
 ```

 Podem posar la llista de línies ordenada en un altre fitxer temporal anomenat `sorted-lengths.txt` posant `> sorted-lengths.txt` després de l'ordre, així com fem servir `> lengths.txt` per posar la sortida de `wc` a `lengths.txt`.

 ```sh
 $ sort -n lengths.txt > sorted-lengths.txt
 ```
 
 Quan has fet això, pots executar una altra ordre anomenada `head` per obtenir les primeres línies de `sorted-lengths.txt`:

```sh
$ head -n 1 sorted-lengths.txt
   9 methane.pdb
```

El paràmetre `-n 1` amb cap indica que només volem la primera línia del fitxer; `-n 20` aconseguirà les primeres 20, i així successivament. 

Atès que `sorted-lengths.txt` conté les longituds dels nostres arxius ordenats de menor a major, la sortida de `head` ha de ser el fitxer amb menys línies.

### Pipe

Si creus que és confús, no estàs sol: fins i tot una vegada que entenguis el que fan `wc`, `sort` i `head`, tots aquests fitxers intermedis fan difícil seguir el fil del que està passant. 

Podem fer-ho més fàcil d'entendre executant `sort` i `head` junts:

```sh
$ sort -n lengths.txt | head -n 1
   9 methane.pdb
```

La barra vertical `|` entre les dues ordres es denomina "pipe" (pronunciat paip). 

{% image "pipe.webp" %}

Una pipe li diu a la terminal que volem fer servir la sortida de l'ordre a l'esquerra com a entrada a l'ordre de la dreta. 

L'ordinador pot crear un fitxer temporal si cal, copiar dades d'un programa a un altre a la memòria, o qualsevol altra cosa que sigui necessària; no cal que ho entenguem per fer-ho funcionar.

Res no ens impedeix encadenar pipes consecutivament. 

Per exemple, pots enviar la sortida de `wc` directament a `sort`, i després la sortida resultant a `head`. 

Així, primer fas servir una pipe per enviar la sortida de `wc` a `sort`:

```sh
$ wc -l *.pdb | sort -n
   9 methane.pdb
  12 ethane.pdb
  15 propane.pdb
  20 cubane.pdb
  21 pentane.pdb
  30 octane.pdb
 107 total
```

I ara enviem la sortida d'aquesta pipe, a través d'un altre pipe, a `head, perquè el pipeline complet es converteixi en:

```sh
$ wc -l *.pdb | sort -n | head -n 1
   9 methane.pdb
```

Quan un ordinador executa un programa (qualsevol programa) crea un procés en memòria per emmagatzemar el programari del programa i el seu estat actual. 

Cada procés té:

1. Un canal d'entrada anomenat entrada estàndard. (Per aquest punt, et pot sorprendre que el nom és tan memorable, però no et preocupis, la majoria de els programadors d'Unix en diuen "stdin"). 

2. Un canal de sortida predeterminat anomenat sortida estàndard (el "stdout”). 

3. Un canal de sortida anomenat error estàndard (stderr) també existeix. Aquest canal sol utilitzar-se per a missatges derror o de diagnòstic i permet a l' usuari canalitzar la sortida dun programa a un altre mentre segueix rebent missatges derror al terminal.

La terminal és realment un altre programa.

Sota circumstàncies normals, el que ingressem al teclat s'envia a l'entrada estàndard de la terminal, i el que produeix a la sortida estàndard es mostra a la nostra pantalla. Quan diem a la terminal que executi un programa, aquesta crea un nou procés i envia temporalment el que teclegem al nostre teclat a l'entrada estàndard d'aquest procés, i el que el procés envia a la sortida estàndard, la terminal l'envia a la terminal pantalla.

### Pipeline

{% image "pipeline.png" %}

Quan executem `wc -l *.pdb > lengths.txt`: 

* La terminal comença dient-li a l'ordinador que creï un nou procés per executar el programa `wc`
* Com hem proporcionat alguns noms de fitxer com a paràmetres, `wc` els llegeix en comptes de l'entrada estàndard. 
* I ja que hem utilitzat `>` per redirigir la sortida a un fitxer, la terminal connecta la sortida estàndard del procés a aquest fitxer.

Si executem `wc -l *.pdb | sort -n`:

* La terminal crea dos processos (un per a cada procés al pipe) de manera que `wc` i `sort` funcionen simultàniament. 
* La sortida estàndard de `wc` és alimentada directament a l'entrada estàndard `sort`
* Ja que no hi ha redirecció amb `>`, la sortida de ordenar va a la pantalla. 

I si executem ` wc -l *.pdb | sort -n | head -n 1`, obtenim tres processos amb dades que flueixen dels arxius, a través de `wc` a `sort`, de `sort` a `head` i finalment a la pantalla.

Aquesta senzilla idea és la raó per la qual Unix ha tingut tant d'èxit.

En lloc de crear programes enormes que tracten de fer moltes coses diferents, els programadors de Unix se centren en crear moltes eines simples que fan bé la seva feina i són capaços de cooperar entre si. 
 
Aquest model de programació es diu “pipes i filtres”. Ja hem vist pipes; i filtre és un programa com `wc` o `sort` que transforma una entrada en una sortida. 
 
Gairebé totes les eines estàndard d'Unix poden funcionar d'aquesta manera: tret que se'ls indiqui el contrari, llegeixen d'entrada estàndard, fan alguna cosa amb el que han llegit, i escriuen a la sortida estàndard.

La clau és que qualsevol programa que llegeixi línies de text dentrada estàndard i escriviu línies de text a la sortida estàndard pot combinar-se amb qualsevol altre programa que es comporti daquesta manera també. 

Pots i has escriure els teus programes d'aquesta manera perquè tu i altres persones puguin posar aquests programes a canonades i així multiplicar-ne el poder.

## Activitats

### Nelle Nemo

Nelle Nemo, una biòloga marina, acaba de tornar d'un estudi de sis mesos del [Gir del Pacífic Nord](https://en.wikipedia.org/wiki/North_Pacific_Gyre), on ha estat mostrant la vida marina gelatinosa a la [Gran Taca de fem del Pacífic](https://en.wikipedia.org/wiki/Great_Pacific_garbage_patch).

Nelle ha processat les seves mostres a la seva màquina d'assaig, generant 17 fitxers al directori `shell-data/north-pacific-gyre/`. 

Com una revisió ràpida, a partir del seu directori d'inici, Nelle tecleja:

```sh
$ cd ~/shell-data/north-pacific-gyre/
$ wc -l *.txt
  300 NENE01729A.txt
  300 NENE01729B.txt
  300 NENE01736A.txt
  300 NENE01751A.txt
  300 NENE01751B.txt
  300 NENE01812A.txt
  300 NENE01843A.txt
  300 NENE01843B.txt
  300 NENE01971Z.txt
  300 NENE01978A.txt
  300 NENE01978B.txt
  240 NENE02018B.txt
  300 NENE02040A.txt
  300 NENE02040B.txt
  300 NENE02040Z.txt
  300 NENE02043A.txt
  300 NENE02043B.txt
 5040 en total
```

Si et fixes bé hi ha un fitxer que només de 240 linies.

Així és més fàcil de veure:


```sh
$ wc -l *.txt | sort -n | head -n 5
  240 NENE02018B.txt
  300 NENE01729A.txt
  300 NENE01729B.txt
  300 NENE01736A.txt
  300 NENE01751A.txt
```
Quan Nelle torna i ho revisa veu que va fer aquell assaig a les 8:00 un dilluns al matí. Algú probablement va fer servir la mateixa màquina aquest cap de setmana, i va oblidar reiniciar-la.

Abans de tornar a analitzar aquesta mostra, decideix comprovar si alguns fitxers tenen massa dades:

```sh
$ wc -l *.txt | sort -n | tail -n 5
  300 NENE02040B.txt
  300 NENE02040Z.txt
  300 NENE02043A.txt
  300 NENE02043B.txt
 5040 total
```
Aquestes xifres semblen bones, no hi ha cap fitxer amb més de 300 linies.

Però, què és aquesta `Z` a l'antepenúltima línia? 

Totes les mostres han d'estar marcades amb “A” o “B”; per convenció, el seu laboratori utilitza `Z` per indicar mostres amb informació que falta. 

Per trobar altres fitxers com aquest, Nelle fa el següent:

```sh
$ ls *Z.txt
NENE01971Z.txt  NENE02040Z.txt
```

Com esperava, quan comprova el registre a l'ordinador portàtil, no hi ha profunditat registrada per a cap daquestes mostres. 

Ja que és massa tard per obtenir la informació duna altra manera, ella ha d'excloure aquests dos arxius de la seva anàlisi. 

Podria simplement esborrar-los usant `rm`, però en realitat hi ha algunes anàlisis que podria fer més tard, on la profunditat no importa, de manera que, en comptes d'esborrar-los, només tindrà cura en seleccionar fitxers utilitzant l'expressió amb caràcters especials `*[AB].txt`. 

Com sempre, el `*` coincideix amb qualsevol nombre de caràcters; l'expressió `[AB]` coincideix amb una 'A' o una 'B', per la qual cosa coincideix amb els noms de tots els fitxers de dades vàlids que té.

```sh
$ ls *[AB].txt
``` 

### Ordenar números

Crea un fitxer `exemple.txt` amb la informació següent (amb `nano`):

```
10
2
19
22
6
```

També pots crear el fitxer amb aquesta comanda:

```sh
$ echo $'10\n2\n19\n22\n6' > exemple.txt
```

Si executem `sort` en aquest fitxer la sortida és:

```
$ sort exemple.txt
10
19
2
22
6
```

I el motiu és que s'ordenen alfabéticament com si fos un diccionari.

Per l'ordinador els caràcters que representen lletres, números o altres coses són el mateix!

Si vols dir-li a `sort` que són números i que els ha d'ordenar com a números has d'utlitzar el flag -n 

```sh
$ sort -n exemplet.txt
2
6
10
19
22
```

### Redirecció

Si executes la comanda `echo` el que escriguis sortirà per pantalla:

```sh
$ echo "Hola classe"
Hola classe
```

Si vols pots rediregir la sortida de la comanda a un fitxer enlloc de al terminal amb `>` :

```sh
$ echo "Hola classe" > classe.txt
$ ls classe.txt
classe.txt
$ cat classe.txt
Hola classe
```

En moltes activitats crearem fitxers d’aquesta manera enlloc de fer servir `nano`.


### pipe

Al principi de l’activitat em baixats uns fitxers comprimits amb aquesta comanda:

```sh
$ curl https://gitlab.com/xtec/smx-6/-/raw/main/shell-data.tar.gz | tar -xz
```

Em fet servir una pipe `|` per encadenar dues comandes. 

Ara anem a fer-ho pas a pas.

Primer borra el directori shell data:

```sh
$ cd
$ rm -rf shell-data/
```

A continuació descarrega el fitxer `shell.data.tar.gz`:

```sh
$ curl https://gitlab.com/xtec/smx-6/-/raw/main/shell-data.tar.gz -o shell-data.tar.gz
  % Total	% Received % Xferd  Average Speed   Time	Time 	Time  Current
                             	Dload  Upload   Total   Spent	Left  Speed
100  441k  100  441k	0 	0   321k  	0  0:00:01  0:00:01 --:--:--  322k
box@user:~$ ls *gz
shell-data.tar.gz
```
Ara continuació hem de descomprimir el fitxer amb la comanda `tar` tal com explicarem a {% link "/linux/archive/" %}, i borrar el fitxer:

```sh
$ tar xfz shell-data.tar.gz
$ rm shell-data.tar.gz
$ ls -F
classe.txt  shell-data/
```
 
Com hem fet en aquesta activitat molts cops farem servir pipes perquè és més ràpid com pots tornar a comprovar:

```sh
$ rm -rf shell-data/
$ curl https://gitlab.com/xtec/smx-6/-/raw/main/shell-data.tar.gz | tar -xz
$ ls -F
classe.txt  shell-data/
```

















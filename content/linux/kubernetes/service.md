---
title: Servicio
icon: kubernetes.png
description: Un servicio es un recurso que proporciona un punto único y permanente de entrada a un grupo de Pods que ofrecen el mismo servicio.
---

## Introducción

Un servicio tiene una dirección IP y un puerto que nunca cambia mientras exista el servicio, y todas las conexiones que recibe las enruta a uno de los pods que respalda a ese servicio.





---
title: Directoris
layout: default.njk
mermaid: true
description: Gestió de directoris i fitxers en el shell de GNU/Linux. Redirecció i pipes.
---

## Introducció

Abans de començar a moure'ns pel terminal, cal entendre com organitza la estructura de `fitxers` i `directoris` el sistema operatiu `GNU/Linux`.

Funciona de forma molt diferent a Windows, però quan l'entens te n'adones que va molt bé; així que atenció!

Per a poder provar en directe el sue funcionament, obre el `terminal de Linux`. 

La seva icona està representada per una pantalla negra en diverses distribucions (les basades en Debian o Ubuntu). 

{% image "bash-icon.png" %}

També pots obrir-lo amb la combinació de tecles **Ctrl + Alt + T**

## Estructura de directoris de Linux al disc

Fixeu-vos que els usuaris es troben dins del directori **/home/**

Per exemple, si tenim un usuari anomenat miquel tindriem el seu espai a:
```sh
/home/miquel
```

L'arbre de directoris més habitual a qualsevol distribució GNU/Linux té la següent forma:

{% image "linux-directories.png" %}

Llista directis habituals:
* `/` El directori més alt és el directori arrel que conté tota la resta. Fem servir la barra inclinada per referir-nos a ell. `/`
* `/root` directori de l'usuari superadministrador
* `/home` Com hem explicat, els usuaris es troben els directoris personals que pengen de /home 
* `/boot` directori amb informació necessaria per arrencar Linux, no confondre amb `/root`.
* `/dev` és on hi ha les **unitats de discos i particions a Linux** (per exemple, en comptes de tenir la unitat C: tindrem `/dev/sda`). Té moltes altres útilitats avançades.
* `/media` en canvi, els dispositius extraibles (pendrives, imatges ISO de CD/DVD, carpetes compartides ...) es creen en aquest directori. 
* `/bin` conté els programes essencials del sistema operatiu (per exemple, comandes com mkdir, ls...)  Alguns programes del sistema també estan a `/sbin`.
* `/tmp` (per a fitxers temporals que no s'han d'emmagatzemar a llarg termini), etc. 

Veiem un exemple que pot simular el nostre sistema operatiu, amb només un usuari.

<pre class="mermaid">
flowchart TD
    / --> bin
    / --> dev
    / --> home
    / --> tmp
    home --> box
</pre>

**Observació:** També poden tenir subcarpetes com `/usr/bin` amb programes instal·lats per a tots els usuaris (firefox, nano, libreoffice...) i els programes que només estan instal·lats per a un usuari es troben a `/usr/local/bin`

Sempre parlarem de directoris a Linux (però el concepte és el mateix que el de la carpeta).

---

## Navegació/rutes de fitxers i directoris: cd, ls, pwd 

Obrir i moure'ns per carpetes des de l'entorn gràfic de Linux és igual de senzill que a Windows o a Mac. 

Si no estàs habituat a usar Linux però Windows o Mac sí n'aprendràs aviat.

Però en el món professional de la informàtica necessitem **programar automàticament l'accés i escriptura de directoris i fitxers del sistema.** I no sempre ho podrem fer amb llenguatges de programació com Python, Java o altres.

Per això necessitem usar un terminal. En aquest cas ens centrarem en el **terminal de Linux**, que és el més potent. 

És tant bo que fins i tot a Windows han creat el projecte [Wsl - Windows Subsystem for Linux](https://xtec.dev/linux/shell/), el qual introduïm a la següent sessió.

Les comandes que treballarem primer són: **pwd, cd i ls**.

Les rutes serveix per moure'ns pels diferents directoris del sistema. 

### /home --> Directori d'usuaris

Quan entres en un terminal de Linux:

```sh
miquel@mint:~$ /home/miquel/
```

El directori arrel del sistema és root i es representa amb una barra:

```sh 
/
```

A diferència de Windows, que la carpeta arrel és:
C:\

O el nom de la unitat assignada.


El directori de l'usari és la **home**, per defecte se situa a:

```sh
usuari1@mint:~$ /home/<nom_usuari>
```

```sh
miquel@mint:~$ /home/miquel/
```

Tant a Linux com a Windows, per defecte la carpeta de cada usuari conté altres carpetes:
Desktop, Music, Documents, Downloads, etc...


### Comanda cd (change directory)

Ens serveix per **moure'ns pels diferents directoris del sistema**.

Li podem passar `rutes absolutes` (la ruta sencera). 

O bé una `ruta relativa` (una ruta que depèn del directori on ens trobem).

**Exemples rutes relatives.**

```sh
cd Documents 
```

Accedeix al directori Documents (si existeix). 

Sempre tenim aquest directori dins del nostre directori d'usuari (com el Desktop, el Downloads...)

```sh
cd ..
```

Retrocedeix a la carpeta anterior (carpeta pare)

```sh
cd ../../..
```

Ves a 3 carpetes anteriors

```
cd
```

**Exemples rutes absolutes.**

Si tenim un usuari que es diu isard, anem a la seva carpeta de `Downloads`.

```sh
cd /home/isard/Downloads/
```

---

### Comanda pwd.

Serveix per veure per pantalla a quina carpeta estem; és important per assegurar-nos que fem bé la ruta relativa.
	
```sh 
usuari1@mint:~$ pwd
/home/usuari1/Desktop
``` 
	
### Comanda ls

Serveix per llistar fitxers i directoris, i tota la seva info.

```sh 
ls
``` 

Llista fitxers i directoris		

```sh 
ls -l
```

Llista fitxers i directoris, format llarg (permisos, tamany...)

```sh 
ls -R	
```

Llista fitxers i directoris, de forma recursiva (els subdirectoris)

```sh 
ls -a
```

Llista fitxers i directoris ocults.

Ls admet molts paràmetres alhora; per exemple:
```sh
ls -la
```
Llista fitxers i carpetes, format llarg (permisos, tamany...), incloent els ocults.
 
<strong>I la comanda més potent i completa:

```sh 
ls -lisah
```

</strong>

Que aplica 5 filtres a ls:
[Explicació comanda ls -lisah](https://explainshell.com/explain?cmd=ls+-lisah)

---

### Recordatori comandes d'ajut'.

#### history --> Historial de comandes : 

Hi ha un historial que guarda totes les comandes que has executat.

Amb les fletxes del teclat `amunt` i `avall` et carrega les comandes que has posat anteriorment (així no et cal escriure-les de nou).

Amb la instrucció **history** veus totes les comandes que has usat fins ara:

```sh
$ history
```

Fins i tot, pots guardar en un fitxer totes les comandes que has fet: 

```sh
$ history > historial.txt
```

#### man --> Manual d'Ajuda. 

La comanda **man** (de manual) és la que ens permet consultar com funciona cada comanda.

Pensa que el terminal de `GNU/Linux` està basat en el del seu sistema predecessor `Unix` (creat als 70) i llavors era especialment útil tenir localitzada l'ajuda, ja que no hi havia Internet.

I ho segueix sent, ja que ens estalviem sortir del terminal per consultar ajuda.

Prova de consultar com funciona la comanda `ls`:

```sh 
man ls
```

Per sortir, pitja q.
 

#### Tecla TAB --> Escriu el nom dels directoris més ràpid

Si el nom del directori on vols anar existeix (per exemple: Desktop, Documents...) només cal que posis les primeres lletres i la tecla **TAB**.

Se t'escriurà sol 😀

També pot ser que et surtin tots els directoris que comencin per una lletra (la D)

---

## Comandes gestió de directoris.

### mkdir --> Creació directoris

Et permet crear un o més directoris.

És molt potent, permet crear moltes carpetes alhora:

<em>Exemple 1: Crea un directori m01 i dins un directori anomenat Linux.</em>

```sh
mkdir m01
mkdir m01/linux
```

<em>Exemple 2: Crea el directori m01, entra-hi i dins crea 3 directoris: fedora, mint, ubuntu.</em>

```sh
mkdir m01
cd m01
mkdir fedora mint ubuntu
```

La comanda mkdir és molt potent i ens permet crear directoris i subdirectoris en la mateixa línia, o fins i tot directoris seguint 
patrons definits.

<em>Exemple 3. Creem un arbre de carpetes amb els mòduls de dawbio1</em>

```sh
miquel@mint:~$ mkdir -p dawbio1/{m01/{pts,pes},m02,m03,m04,m05,m14}
```

<em>Exemple 4. Creem carpetes per a 10 usuaris, començant per user1 i acabant per user10.</em>

```sh
miquel@mint:~$ mkdir -p user{1..10}
```

Recorda:
- **mkdir**, per a crear directoris
- **ls**, llista directoris i arxius
- **cd**, salta a un altre directori
- **pwd**, mostra en quin directori estàs

És molt bàsic, però els professionals tendeixen a oblidar-ho.

<a href="https://www.ionos.es/digitalguide/servidores/configuracion/comando-mkdir-de-linux/#:~:text=El%20comando%20de%20Linux%20conocido,crear%20jerarqu%C3%ADas%20de%20carpetas%20complejas">Més exemples d'ús de mkdir a Linux</a>

### Fitxers i carpetes ocultes.

Per a crear un fitxer o carpeta **oculta és tan senzill com que el seu nom comenci per punt.**

```sh
mkdir .secretofmonkey
touch .secretofmonkey.txt
```

**Exercici 11.** 
Crea el directori Debian, dintre d'aquest els directoris Kali, MX i Ubuntu i dins d'ubuntu els directoris Mint, PopOS i Lubuntu. Mostra que s'han creat tots.

**Exercici 12.** 
Crea el directori futfem i dintre els directoris oshoala,aitana,mapi,patri i els directoris ocults .tactiques i .sous (si vols crea’n unes altres semblants). Mostra els directoris creats, inclos els ocults.

{% sol %}
	
Ex11. 
```sh
miquel@mint:~$ mkdir Debian
miquel@mint:~$ cd Debian
miquel@mint:~$ mkdir Kali,MX,Ubuntu
miquel@mint:~$ cd Ubuntu
miquel@mint:~$ mkdir Mint,PopOS,Lubuntu
miquel@mint:~$ cd ../..
miquel@mint:~$ ls
```

Ex12. 
```sh
miquel@mint:~$ mkdir -p futfem/{oshoala,aitana,mapi,patri,.tactiques,.sous}
miquel@mint:~$ ls -la
```

{% endsol %}

</details>

### tree --> arbre directoris i fitxers

Apart de ls, si només ens interessa els noms dels directoris (i dels fitxers) podem usar la comanda tree.

En algunes distros cal instal·lar-la:

```sh
$ sudo apt install tree
$ tree
```

---

### cp --> Còpia de directoris (i fitxers)

Per a fer còpies de seguretat, usem la comanda cp

La sintaxis és:

**cp [opcions] origen desti**

- [opcions]: L'argument més habitual de cp és el -r; que ens fa una còpia recursiva del directori i tot els subdirectoris (i fitxers dins).
- origen: El fitxer o directori que tenim. Ha d'existir, òbviament.
- desti: El nom del fitxer o directori que volem. Pot existir o no; si no existeix el pot crear.

**Exemple: Fes una còpia de tot el contingut del directori futfem (si no el tens crea'l) cap a una nova carpeta futfem2023.**

```sh
$ cp -r futfem futfem2023
$ ls
```

---

### mv --> Moure i renombrar directoris (i fitxers)

La comanda mv a Linux serveix per dues coses: la més òbvia és la de moure un directori o fitxer cap a un altre lloc.

Però també serveix per renombrar el nom d'un fitxer o directori.

<em>Exemple: Crea el directori Apu. Canvia el seu nom, es dirà Apunts. Mostra si s'han canviat el nom.</em>

```sh
$ mkdir Apu
$ mv Apu Apunts
$ ls
```

---

### rm --> Esborrar directoris (i fitxers)

Si volem esborrar un directori el més habitual és usar la comanda rm.

La sintaxis és :

**rm [opcions] origen desti**
- [opcions]: L'argument més habitual de rm és el -r; que ens elimina recursivament directori i tot els subdirectoris (i fitxers dins).
També s'usa el f (forçar l'esborrat sense demanar permís).
- origen: El fitxer o directori volem eliminar. Ha d'existir, clar.

<em>Exemple: Crea el directori lindows. Esborra el directori i tot el seu contingut.</em>
```sh
$ mkdir lindows
$ rm -r lindows
$ ls
```

⚠ **rm - rf** és perillós. Eliminar és una operació irreversible ⚠

La terminal de Linux (i de Unix) no té una paperera de reciclaje per a restaurar arxius eliminats. 

Això només ho podem fer si usem una distribució gràfica moderna basada en Debian (Ubuntu). 

Quan eliminem arxius i directoris, es desvinculen del sistema per a què la memòria es pugui tornar a usar. Hi ha mètodes per recuperar algunes dades però no hi poden confiar d'entrada.

La comanda rmdir només ens serveix per esborrar directoris buits.


**Exercici 13.** 
Crea un directori que es digui `da`. Renombra'l a `dawbio1`. Demostra que s'ha fet correctament el canvi.

**Exercici 14.** 
Crea un directori que es digui `institut`. 
A dins crea 3 directoris anomenats `aules`, `secretaria` i `consergeria`.
Fes una copia del directori `institut`, que es dirà `institut-2024`.
Esborra el directori `institut`
Demostra que t'ha sortit bé (comanda `ls` o `tree`)

{% sol %}
	
Ex13. 
```sh
$ mkdir da
$ mv da dawbio1
$ ls
```

Ex14. 
```sh
miquel@mint:~$ mkdir -p institut/{aules,secretaria,consergeria}
miquel@mint:~$ cp -r institut institut-2024
miquel@mint:~$ rm -r institut
miquel@mint:~$ tree
```

{% endsol %}

---


## Comandes gestió fitxers.

### cp, rm i mv --> Copiar i moure i eliminar fitxers

Les comandes funcionen igual que en els directoris, que ja les hem analitzat i provat.

### touch --> Crear fitxers buits

Crea un fitxer buit (o més, seguits per comes). 

També serveix per fer que canvii els permisos de modificació al dia i hora actuals (d'aquí el nom de tocar) si aquest existeix.

Prova-ho tu mateix/a:

```sh
$ touch demo.txt
$ touch demo2.txt
$ touch demo.txt
```

---

### nano --> Editor de text del terminal.

Editor de text que funciona sense sortir del terminal. 

Si voleu publicar les vostres aplicacions us haureu d'acostumar a usar-lo, ja que els servidors d'aplicacions web més potents no tenen entorn gràfic (consumeix recursos innecessaris) i cal usar comandes del terminal per arrencar-los, organitzar el seu contingut ...

Aquí en teniu una mostra:
![editor nano gif](https://media.geeksforgeeks.org/wp-content/uploads/20200304140712/cutpaste.gif)

Si no veieu el gif el teniu a:
<https://media.geeksforgeeks.org/wp-content/uploads/20200304140712/cutpaste.gif>

Escriu:

```sh
nano fitxer.txt
```

Aquí podràs escriure el seu contingut.

Per a guardar el fitxer, cal fer la combinació

```sh
: w
```
Per a sortir

```sh
: q
```

Els : inicials el que fan és canviar del mode d'escriptura del fitxer al mode del menú de programa.

<em>Exemple. Crea un fitxer anomenat debian.txt des del terminal, usant l’editor nano. Ha de contenir el text:</em>

```sh
Debian és un sistema operatiu FOSS (lliure), creat l’any 1993 per Ian Murdock.
```

Recorda, els passos a seguir són:
1. nano debian.txt
2. Escriu el text
3. Ctrl + X 
4. INTRO
5. nano debian.txt (veure contingut fitxer)

---

### cat --> Visualitza el contingut d'un fitxer.

Visualitza el contingut d'un fitxer que només tingui text (no val per Writer/Word) sense necessitat d'obrir-lo.

Tot és per millorar el rendiment.

**Exemple: Crear un fitxer amb el nano i visualitzar el seu contingut amb el cat.**

```sh
nano debian.txt
cat debian.txt
```

{% image "cat-head-tail.jpg" %}

---

### head, tail --> Primeres / últimes línies fitxers.

Si vols veure només les n primeres línies o les n últimes linies:

```sh
head -n 3 <nom_fitxer>
```

```sh
tail -n 3 <nom_fitxer>
```

**Exercici 15:**
Per si no ho sabies, per consultar els usuaris que hi ha al SO ho pots fer des del fitxer /etc/passwd. Mostra els 5 primers i 5 últims usuaris.

Per mostrar-los tots ho fas amb:
```sh
cat /etc/passwd
```

**Exercici 16:**
Dins el directori `Linux` creeu un altre arxiu anomenat `mint.txt` amb el següent contingut i al final mostra per pantalla el contingut creat:

````sh
Mint també és un sistema operatiu lliure, basat en Ubuntu i Debian.
Mint també és un sistema operatiu multiusuari.
```` 

{% sol %}

**Solució Exercici 15 /etc/passwd:**

```sh
head -n 5 /etc/passwd
```

**Solució Exercici 16**

{% image "bash-ex16.png" %}

{% endsol %}

---

#### Curiositats del terminal
	
El cd també funciona a MS-DOS (terminal de Windows). 
	
Tanmateix, moltes comandes a Windows són diferents (per exemple. L’ls de Linux és dir amb Windows), i moltes d’altres ni existeixen.
Per a tenir una shell tant avançada com la de Unix i GNU/Linux cal usar la terminal com PowerShell; i ni tan sols així s'aconsegueixen  tantes prestancions com amb Linux.

Una altra opció per gaudir de Linux a Windows, disponible a partir de Windows, és instal.lar l'<a href="https://learn.microsoft.com/es-es/windows/wsl/install">WSL (Windows Subsystem for Linux)</a> si som administradors/es.

---

### Exercicis Repàs. «Directoris, rutes, i creació de fitxers»

Escriu la o les comandes necessàries per cada cas:

<em> Comandaments 1-6: cd, ls, pwd, mkdir, cat, touch, nano. </em>

**1.** Situa't al teu directori home.
	
**2.** Mostra la ruta del directori actual.
	
**3.** Llista els fitxers del directori actual, inclosos els ocults.
	
**4.** Llista els fitxers del directori arrel, sense canviar de directori.

{% sol %}

**1.**
```sh
cd
```

**2.**
```sh
pwd
```

**3.**
```sh
ls -a
``` 

```sh
ls -lisah
```

**4.**
```sh
ls /
```

{% endsol %}


<em> Comandaments 5-10: mkdir, cd, cat, touch, nano. </em>

**5.** Crea un directori al teu directori home que es digui "practica".

**6.** Sense canviar de directori, crea un arxiu al directori "practica" que es digui "test1.txt".

**7.** Edita "test1.txt" afegint "Línia 1" sense canviar de directori, utilitzant una ruta relativa.

**8.** Edita "test1.txt" afegint "Línia 2" sense canviar de directori, utilitzant una ruta absoluta.

**9.** Mostra els continguts de test1.txt sense canviar de directori i utilitzant una ruta relativa.

**10.** Mostra els continguts de test1.txt sense canviar de directori i utilitzant una ruta absoluta.

{% sol %}

**5.** Crear el directori "practica":

```bash
mkdir practica
```

**6.** Crear l'arxiu "test1.txt" dins el directori "practica":

```bash
touch practica/test1.txt
```

**7.** Editar "test1.txt" afegint "Línia 1" amb una ruta relativa:

```bash
nano practica/test1.txt
```
(*Afegir el text "Línia 1" dins de l'editor nano i guardar els canvis.*)

**8.** Editar "test1.txt" afegint "Línia 2" amb una ruta absoluta:

```bash
nano /home/alumne/practica/test1.txt
```
(*Afegir el text "Línia 2" dins de l'editor nano i guardar els canvis.*)

**9.** Mostrar els continguts de "test1.txt" amb una ruta relativa:

```bash
cat practica/test1.txt
```

**10.** Mostrar els continguts de "test1.txt" amb una ruta absoluta:

```bash
cat /home/alumne/practica/test1.txt
```

{% endsol %}

---

### Més exercicis Repàs - «Gestió i edició de fitxers i directoris»

<em> Comandaments: cp, mv, cat, mkdir, nano, ls, rm </em>

Escriu la o les comandes necessàries per cada cas, sense moure't del teu directori home:

1. Fes una còpia de "test1.txt" i deixa-la al directori "practica".
	
2. Mou "test2.txt" al teu directori home.

3. Renombra "test2.txt" a "diari.txt".
	
4. Converteix "diari.txt" en un arxiu ocult.
	
5. Edita el diari, esborrant tot el text i escrivint "Això és el meu diari personal.".
	
6. Mostra els continguts del diari.
	
7. Crea un directori ocult dins de "practica" que es digui "secrets".
	
8. Llista els fitxers del directori "practica", inclosos els ocults.
	
9. Copia el diari al directori "secrets".
	
10. Llista els continguts del diari dins del directori secrets.
	
11. Esborra el diari que està al directori home.
	
12. Fes una còpia oculta del directori "secrets" que es digui "copia-seguretat" dins de "practica".
	
13. Llista els fitxers del directori "copia-seguretat", inclosos els ocults.


{% sol %}

**1.-**
```sh
cp practica/test1.txt practica/test2.txt
```

**2.-**
```sh
mv practica/test2.txt ~
```

**3.-**
```sh
mv test2.txt diari.txt
```

**4.-**
```sh
mv diari.txt .diari.txt
```

**5.-**
```sh
nano .diari.txt
```

**6.-**
```sh
cat .diari.txt
```

**7.-**
```sh
mkdir practica/.secrets
```

**8.-**
```sh
ls -a practica
```

**9.-**
```sh
cp .diari.txt practica/.secrets
```

**10.-**
```sh
cat practica/.secrets/.diari.txt
```

**11.-**
```sh
rm .diari.txt
```

**12.-**
```sh
cp -r practica/.secrets/ practica/.copia-seguritat
```

**13.-**
```sh
ls -a practica/.copia-seguritat/
```

{% endsol %}

---

## Comandes de redirecció ( >, >>, |, echo, /dev/null)

### Redirecció text sortida del terminal amb > i >>.

Per defecte el resultat de les comandes surten per la consola (sortida estàndard)

```sh
	ls -l
```

Si fiquem `>` després de la comanda i el nom d’un fitxer, posem la sortida en un fitxer en comptes del terminal. 

Si el fitxer ja existeix el sobreescriu.

```sh
  ls -l > fitxersCarpeta.txt
```

Fixa’t que per veure si s’ha creat bé podem usar cat:
```sh
  cat fitxersCarpeta.txt
```

Si posem `>>` després de la comanda i el nom d’un fitxer fa el mateix que >; excepte en el cas que el fitxer existeixi, 
que en comptes de sobreescriure posa les linies a continuació.
```sh
  ls -l >> fitxersCarpeta2.txt
```

Personalment, em va sorprendre molt descobrir aquesta comanda com a programador 😲, ja que abans vaig aprendre com fer el mateix amb Java i és més llarg.

---

### echo --> Mostrar text i variables.

Comanda bàsica, com el print dels llenguatges de programació.

```sh
$ echo ”Hello Linux”
```

Per a escriure un text dins d’un fitxer sense obrir-lo ho fem així:

```sh
$ echo ”Hello” > /home/miquel/practica/test1.txt
$ cat test1.txt
$ Hello
```

### Variables d'entorn

`echo` no només et permet mostrar text literal, també interpreta variables del sistema (variables d'entorn).

Per exemple, prova les comandes:

```sh
echo $USER
echo $PATH
echo $LANG
```

Alguns valors de configuració es poden obtenir amb altres comandes, no cal l'echo.

```sh
whoami
printenv
lsb_release -a
```

---

### Concaternació fitxers amb cat.

**Exercici 4.1** 

Creeu un nou fitxer de nom total.txt que guardi el contingut dels fitxers `mint.txt` i `debian.txt`; sense tornar a copiar el text.</em>

Escriu els fitxers sense cap editor.

**debian.txt** té el text:
```sh
Debian és un sistema operatiu lliure, creat l’any 1993 per Ian Murdock.
```

**mint.txt** té el text:
```sh
Mint també és un sistema operatiu lliure, basat en Ubuntu i Debian.
Mint també és un sistema operatiu multiusuari.
```

**Exercici 4.2** 

Creeu un nou fitxer de nom **total.txt** que guardi el contingut dels fitxers **ubuntu.txt**,**mint.txt** i **debian.txt**; sense tornar a copiar el text.

**Pistes:**
- La comanda cat et permet unir més d’un fitxers.
- La comanda > et permet guardar el que surt pel terminal a un nou fitxer.

{% sol %}

```sh
echo "Debian és un sistema operatiu lliure, creat l’any 1993 per Ian Murdock." > debian.txt
echo "Mint també és un sistema operatiu lliure, basat en Ubuntu i Debian." >> mint.txt
echo "Mint també és un sistema multiusuari." >> mint.txt
cat mint.txt debian.txt >> total.txt
```

{% endsol %}

**Exercici 4.3**

Guarda l'arbre de directoris (de forma recursiva) en un fitxer anomenat tree.txt des de la teva carpeta personal.

{% sol %}

```sh
$ echo ”Debian és un sistema operatiu lliure, creat l’any 1993 per Ian Murdock.” > ~/practica11/debian.txt
$ echo ”Mint també és un sistema operatiu lliure, basat en Ubuntu i Debian.” > ~/practica11/mint.txt
$ echo ”Mint també és un sistema operatiu multiusuari.” >> ~/practica11/mint.txt
$ cat debian.txt mint.txt > total.txt
```

```sh
tree > tree.txt
```
{% endsol %}

---

### Redirecció d'errors (/dev/null)

Per defecte, quan una comanda ha funcionat Linux no ens dona cap feedback, però si hi ha hagut algún error ens informa de perquè no ha funcionat.

Exemples típics d'**operacions que donen error**:
- Crear un directori que ja existeix
- Esborrar un directori que no existeix.
- Llegir un fitxer que no existeix.
- Demanar una llista d'un directori inexistent

```sh
miquel@mint:~$ cat pruebas.txt
cat: pruebas: No existe el fichero o el directorio
```

Si, pel que sigui (per exemple, fem un script), no volem que ens surti aquest error per pantalla el que hem de fer és redirigir-lo a /dev/null.

#### I què és això de /dev/null ? 

**/dev/null** és com un forat negre, tot el que posis en aquest directori s'esborrarà automàticament.

Per tant, si redirigeixes el missatge de la sortida d'error (que per cert, es redirigeix amb **2>**) a /dev/null ja no sortirà.

Prova-ho; i tingues en compte que entre > i / no hi ha cap espai!

```sh
miquel@mint:~$ cat pruebas.txt 2>/dev/null
```

També és útil usar la redirecció d'errors per guardar un registre dels errors en un fitxer.

```sh
cat pruebas.txt 2 >err.log
```

- [https://itsfoss.com/es/redireccion-linux/](Bloc Itsfoss, redireccions a Linux)

---

### Comandes pipe (|) per filtrar la sortida.

La comanda de canonada | s'utilitza molt per agafar la sortida d'una comanda i aplicar la següent comanda al resultat de la sortida.

{% image "linux-pipes.png" %}

Per exemple, això és molt útil si volem comptar les línies i tamany d'un fitxer sense obrir-lo amb la comanda `wc`.

```sh
cat mint.txt | wc 
```

#### Exemple absurd de pipe: cowsay. 

Instal·la el programa cowsay. Aquest programa dibuixa una vaca (o qualsevol altre animal si canvies la opció corresponent, -f dragon)

```sh
$ sudo apt install cowsay
$ cowsay Hey!
 __________________
< Hey!              >
 ------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```

Usa l'operador per | redirigir la comanda que llegeix contingut d'un fitxer cap a cowsay.

```sh
$ cat mint.txt | cowsay
 ________________________________________
/ Mint també és un sistema operatiu lliure \
\ , basat en Ubuntu i Debian.              /
 -----------------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```

Ja has vist com cowsay ha agafat el text de la sortida i l'ha reconvertit gràcies al `|`.

També pots crear altres formes amb la opció `-f`:

```sh
$ cat mint.txt | cowsay -f tux
 ___________________________
/ Mint també és un sistema  \
| operatiu lliure, basat en |
\ Ubuntu i Debian.          /
 ---------------------------
   \
    \
        .--.
       |o_o |
       |:_/ |
      //   \ \
     (|     | )
    /'\_   _/`\
    \___)=(___/
```

Si et sembla divertit pots aprofundir en cowsay:

- [Tutorial Linux cowsay](https://opensource.com/article/18/12/linux-toy-cowsay)

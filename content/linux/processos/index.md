---
title: Processos
mermaid: true
description: Gestió de programes i processos dels SO en general i a Linux des del terminal. Gestió interativa (monitor, top), estats dels processos, gestió per comandes (ps, jobs), primer i segon pla (&, bg), parar vs finalitzar (kill, pkill), scripts com a processos, paral·lelisme.
---


## Processos del SO: 

**Definició: Un procés és un programa en execució**

Un programa es una col·lecció d'instruccions. Exemple: la comanda `ls` (i moltes altres) és un programa executable guardat en algún directori. 

Per defecte a `/bin/ls`

Un procés ocupa espai a la memòria RAM i necessita la CPU per poder funcionar, que s'executin les instruccions que necesita.

Un procés només pot tenir un únic número natural PID (identificador). Això és així en tots els SO.

El **primer procés que arrenca a Linux és l’init. Té el PID 1 i és el responsable de l'inici i l'apagada del sistema.** 
Tots els processos tenen un procés pare (PPID, no confoneu amb PID) excepte aquest.


### Estats dels processos

Els més habituals:

**running (execució)** 

**waiting o blocked.** 

En general poden estar en aquest estat perquè esperen una entrada (I/O) de l’usuari, com per exemple que li demani introduïr un text. També poden estar blocked per què l’usuari els ha aturat o minimitzat.

Hi ha altres estats menys comuns, que succeeixen quan falta memòria RAM o CPU: 
**zombie (falta CPU o ha finalitzat però no pot avisar el seu procés pare)**, 
**swapped (falta memòria RAM i per tant cal enviar-lo a la memòria virtual=swap a Linux).**

**Els estats dels processos a Linux són (extret de l’ajuda de la comanda ps):**

```sh
man ps
```

```bash
PROCESS STATE CODES    
      Here are the different values that the s, stat and state output
       specifiers (header "STAT" or "S") will display to describe the
       state of a process:
               D    uninterruptible sleep (usually IO)
               I    Idle kernel thread
               R    running or runnable (on run queue)
               S    interruptible sleep (waiting for an event to
                    complete)
               T    stopped by job control signal
```

Si consultem la llista de processos mentre tenim obert el terminal:
```sh
man ps a
```
{% image "linux-ps1.png" %}

La columna **STAT, la primera lletra, ens indica l’estat del procés.**
Tots estan amb S (esperen entrada de l’usuari) excepte l’últim que hi ha una R de running. 

Els altres símbols són: + → en primer pla, s → gestiona la sessió. 
tty → és el terminal

---

## Gestió interactiva (en directe) dels processos.

### top

Comanda per a veure en pantalla, de forma interactiva (cada 5 segons s'actualitza l'estat), tots els processos a la CPU, i la memòria que ocupen.

Ens permet fer moltíssimes coses:

[https://www.layerstack.com/resources/tutorials/How-to-use-top-and-htop-Linux-command-for-Process-Management]

### htop

És una variant del top, fa el mateix però de forma una mica més ordenada i atractiva.

{% image "linux-htop.png" %}


### system-gnome-monitor

Per facilitar la vida als usuaris no experts en sistemes operatius, algunes distribucions inclouen Administradors de tasques com el del Windows. 

A les distribucions basades en Ubuntu i Debian tenim el `system-gnome-monitor`

Si no hi és, el pots instal·lar amb:

```sh
$ sudo apt install gnome-system-monitor
```

## Comandes gestió de processos. 

### ps

Mostra la llista de processos que hi ha. 

#### Combinacions importants:

Llista processos en estat d'execució:

```sh
ps -ef
```

Mostra informació d'un procés concret, de tots els terminals i usuaris.

```sh
ps -aux | grep firefox
```

`| grep` serveix per filtrar a on apareix la paraula que indiquem (pex firefox). 

### kill i pkill 

**Kill** normalment serveix per a forçar la finalització de processos.

**pkill** fa el matex però rep com a paràmetre el nom del procés en comptes de PID (més fàcil de recordar)

```sh
kill -15 <PID_PROCES>
pkill -15 <NOM_PROCES>
```

Tot i el seu nom, la comanda kill no només serveix per finalitzar processos. Segons el senyal que li enviem pot fer altres operacions com finalitzar-lo a poc a poc, aturar-lo (19), continuar un procés parat (18) o reiniciar (1)

El comando kill:
```sh
Finalizar un proceso SIGTERM (15)
Terminar un proceso de forma inmediata SIGKILL (9)
Suspender un proceso SIGSTOP (19)
Reactivar un proceso SIGCONT (18)
Reiniciar un proceso SIGHUP (1)
```

---

## Execució de processos propis.

### En primer pla, posem el nom del programa:

```sh
firefox
```

Veiem que ja no podem fer res al terminal fins que no el tanquem.


### Per arrencar en segon pla, posem el nom del programa seguit del caràcter &:

```sh
firefox &
```

Podem seguir usant el terminal si el procés està en segon pla.

Altres programes que poden ser processos són:
* Totes les comandes de Unix: que estan a la carpeta /bin (ls, mkdir, pwd, …)
* gedit
* gnome-calculator
* sleep
* libreoffice

---

## Gestió de processos de la sessió del terminal.

Obrir terminal: Ctrl+ Alt + T

No són tant importants, però en ocasions ens poden ajudar:


### jobs 

Serveix per mostrar l'estat dels processos del terminal i el seu PID.

```sh
jobs -l
```

### bg i fg

bg, envia un procés a segon pla, amb l'id que indica al comanda jobs.

```sh
bg %1
```

fg funciona igual que bg, i envia un procés a primer pla, amb l'id que indica al comanda jobs.


### Ctrl+C

Mata (finalitza) el procés. 

### Ctrl+Z

Para el procés. Llavors es pot tornar a reanudar amb altres comandes.


## Exercicis. Bloc 1. Comandes de processos.

Escriu la o les comandes necessàries per cada cas:

<strong>Comandaments: bg, fg, jobs, kill, ps, top, &, Ctrl+C, Ctrl+Z</strong>

1.	Mostra els processos corrent al terminal actual.

2.	Mostra els processos corrent al sistema a tots els terminals i usuaris.

3.	Mostra els processos corrent al sistema dinàmicament.

4.	Llença un programa gràfic (gedit, xlogo, etc.) al foreground del terminal.

5.	Mata el procès des del terminal.

6.	Llença un programa gràfic (gedit, xlogo, etc.).

7.	Llença un programa gràfic (gedit, xlogo, etc.) al background del terminal.

8.	Mostra el PID del programa que està corrent al background.

9.	Mata el procés utilitzant el seu PID.

10.	Llença un programa gràfic (gedit, xlogo, etc.) al background del terminal.

11.	Mostra el jobspec del procès que està corrent al background.

12.	Fes que el procés vagi al foreground del terminal.

13.	Pausa el procés i mira com es comporta la finestra.

14.	Fes que el procés torni a còrrer al foreground.

15.	Torna a pausar el pocés i fes que torni a córrer al background.

#### Solucions Exercicis Bloc 1.

{% sol %}
```console
1	ps
2	ps aux
3	top
4	libreoffice
5	Cotrol + c (parar el proceso)
6	geany
7	Geany & (puedes continuar con la terminal)
8	ps
9	kill -9 5405
10	geany &
11	Jobs (muestra los procesos que se han ejecutado en el terminal)
12	fg %1 (pondremos el numero que esta en el segundo plano Control+z)
13	Control + z
14	fg %1
15	bg %1 (Si un programa se para, se puede continuar con el bg%1 para que siga corriendo)
```

{% endsol %}

## Exercicis. Bloc 2. Activitat guiada processos.

**a. Obre el Firefox**

```sh
firefox
```

**b. Cerca la comanda per trobar el PID del Firefox.**

Aquesta comanda ens llista tots els processos en execució:

```sh
ps -e 
```

No ens serveix; usarem aquesta cadena de comandes:

ps -e | grep <nom_programa>

```sh
ps -e | grep firefox
```

{% image "linux-ps-grep.png" %}

Si el tenim arrencat ens apareix l’id 5329
Sinó, no ens apareix cap linia.


**c. Finalitza (Mata) el procés.**

Opció 1. 
Ir al terminal donde hemos ejecutado el proceso.
Pulsar:
Ctrl + C

Opció 2. 

Consultem el PID:

```sh
ps -e | grep firefox
```

I ara fem la comanda:
kill -9 PID

```sh
kill -9 6412
```

Opció 3.

Tanquem totes les sessions de Firefox alhora. Ens pregunta si estem segurs (y/n).

```sh
pkill -i firefox
```

**d. Obre Firefox en segon pla. Tanca'l. Pista: &**

```sh
firefox &
```

Fixeu-vos que podem seguir treballant amb el terminal.

**e. Executa el procés Sleep 600 i Sleep 1000 en segon pla**

```sh
sleep 600 &
sleep 1000 &
```

{% image "linux-ps-jobs.png" %}

**f. Visualitza els processos en curs**

```sh
ps -l
```

**g. Visualitza els treballs en curs**

```sh
jobs -l
```

**h. Porta els 2 processos en primer pla**

```sh
fg %1 
fg %2
```

**i. Vuelve a visualizar los trabajos y procesos en curso**

```sh
ps -e | grep firefox
```

{% image "linux-ps-segonpla.png" %}

---

## Scripts com a processos

Podem crear els nostres propis programes amb la shell Bash de Linux, i executar-los com a processos per tal d'automatitzar tasques. 

Començarem creant un script no interactiu, és a dir, que en cap cas demani info a l’usuari, i farà totes les operacions seguides. 
Això són scripts en batch.

Anem a crear-ne un:

```sh
nano hacking.sh
```

O 

```sh
gedit hacking.sh
```

Ara, escriviu o copieu aquest text:
```sh
#!/bin/bash
for i in {1..100}
do
 echo "Hacking. $i% completed."
 sleep 2
done
echo "Hacking completed :)"
```

Per convertir aquest fitxer a progama executable ho fem amb la comanda:

```sh
chmod +x hacking.sh
```

I l'executem amb aquesta:

```sh
./infinit.sh
```

O bé es pot executar directament amb aquesta comanda (si no tenim permisos d'administració):

```sh
bash hacking.sh
```

Ens ha de sortir text com aquest, cada 2 segons (donat que hem posat la instrucció sleep 2)
```sh
Hacking. 1% completed.
Hacking. 2% completed.
```

Quina por, no para de sortir text, com l’aturem ????

Al primer terminal `Ctrl + Z` o `Ctrl + C`

Provem d'executar-lo en segon pla:

```sh
./hacking.sh &
```

Així pots seguir usant el terminal.

No para de sortir text cada 2 segons, com aturo això ??

La opció més còmode és:

```sh
kill -9 hacking
```

### Activitat guiada 4. Scripts com a processos i estats Entrada/Sortida.

**Pas 0.**
Obriu 2 terminals, un per executar l’script i l’altre per aplicar el ps per veure l’estat dels perfils.

**Pas 1.**
```sh
nano script.sh
```

```sh
#/bin/bash
read -p “Introdueix el teu nom: ” nom
while true; do
echo “Hola $nom”
done;
```

**Pas 2.**
```sh
chmod u+x script.sh
```

**Pas 3.**
```sh
./script.sh
```

**Pas 4.**
Al segon terminal, aplica 

```sh
ps j
```

{% image "linux-ps-j1.png" %}

Pas 5.
Al primer terminal

```sh
Ctrl + Z
```

Pas 6.
Al segon terminal

```sh
ps j
```

{% image "linux-ps-j2.png" %}

I amb aquests exemples hem pogut analitzar els 3 estats (execució, bloqueig i finalització).


---


## Referències (any 2023-2024)
[Presentació Processos](https://drive.google.com/file/d/1AnivHc-O48dvOUyi6kQFCd85MHggZO07/view)
[Linux Activitats Guiades Processos](https://docs.google.com/document/d/1cVYFbcZ7YfGg9ZK5GSdNK3WsOjrF-Vbw7zEKcfxpfxQ/edit#heading=h.6t2z67qzrf37) Feta!
[Exercicis](https://docs.google.com/document/d/1960fC7HxLHBIh9nwZ87H8DQR4rlZB0XSofR2GazjVGk/edit) Pendent!
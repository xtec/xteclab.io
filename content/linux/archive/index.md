---
title: Arxivar
description: Molts cops has d'arxivar un conjunt de fitxers en un de sol, comprimir un fitxer que no fas servir perquè ocupi menys espai, o fer les dos coses a la vegada.
---

## Introducció

Arrenca una màquina virtual Linux amb {% link "/windows/wsl/" %}.

```pwsh
> connect-wsl archive -new
```

## Empaquetar

**tar** és un programa que permet empaquetar un conjunt de fitxers en un sol fitxer.

El nom "tar" prové de la frase "**T**ape **AR**chive", que era un cinta on es guardaven dades:

{% image "tape.png" %}

I avui en dia encara es fan servir les cintes per guardar dades en volums de Terabytes.


### Crear

Crea dos fitxers:

```sh
$ echo "one" > one.txt
$ echo "two" > two.txt
```

A continuació guarda el fitxers en el fitxer `numbers.tar` amb el flag `c` (**c**reate):

```sh
$ tar cf numbers.tar one.txt two.txt
```

El flag `f` (**f**ile) és necessari per indicar a `tar`el fitxer que ha de crear.

Si no passes el flag `f`, `tar`protesta:

```sh
$ tar c numbers.tar one.txt two.txt 
tar: Refusing to write archive contents to terminal (missing -f option?)
tar: Error is not recoverable: exiting now
``` 

Diu que no pot escriure al terminal 😢

De totes maneres tampoc has vist si ha passat quelcom, només que hi ha un nou fitxer molt més gran que els demés:

```
$ ls -l 
-rw-r--r-- 1 box box 10240 Oct 11 16:18 numbers.tar
-rw-r--r-- 1 box box     4 Oct 11 16:17 one.txt    
-rw-r--r-- 1 box box     4 Oct 11 16:17 two.txt  
```

A partir de dos de mida `4` ha creat un de mida `10240`.

Anem a veure que fa `tar` amb els fitxers amb el flag `v` (**v**erbose) per tal que s'expliqui una mica:

```sh
$ tar cfv numbers.tar one.txt two.txt
one.txt
two.txt
```

No s'explica massa 🤔


### Llistar

Amb el flag `t` (lis**t**) pots veure els fitxers que forment part del fitxer "tar".

```sh
$ tar tf numbers.tar 
one.txt
two.txt      
```

I afegint l'opció `v` pots veure metadades d'aquests fitxers:

```sh
$ tar tvf numbers.tar 
-rw-r--r-- box/box           4 2024-10-11 16:17 one.txt
-rw-r--r-- box/box           4 2024-10-11 16:17 two.txt
```

Això ja ho sabiem 🤔

Perqué ets tu qui a creat el fitxer 😅


### Extreure

Si guarder un conjunt de fitxers en un fitxer "tar" és per poder recuperar-los si et fan falta.

Borra els fitxers i comencem de nou:

```sh
$ rm *
```

Baixem uns fitxers i el guardem en un `.tar`:

```sh
$ wget -q https://ca.wikipedia.org/wiki/Girona -O girona.html
$ wget -q https://ca.wikipedia.org/wiki/Barcelona -O barcelona.html
$ tar cf cities.tar *.html
$ rm *.html
```

Com que has borrat els "html" els has perdut ... és un simulacre 🤫

Anem a veure que té el "tar":

```sh
$ tar tf cities.tar 
barcelona.html
girona.html
```

Doncs si, té Barcelona i Girona!

Si vols recuperar Girona pots utlitzar el flag `x` (e**x**tract) amb el nom del fitxer:

```sh
$ tar xf cities.tar girona.html
$ ls
cities.tar  girona.html
```

Borra el fitxer `girona.html` ...

I si vols recuperar tots els fitxers, doncs no diguis cap fitxer:

```sh
$ tar xf cities.tar 
$ ls
barcelona.html  cities.tar  girona.html
```

## Comprimir

Comprimir un fitxer vol dir redur la seva mida.

Al comprimir un fitxer puc perdre informació, com passa amb els audios i els videos.

O no puc perdre gens d'informació, com és el nostre cas.

A continuació veurem alguns eines de compressió de diferent qualitat.

Baixa el fitxer <https://gitlab.com/xtec/linux/archive/-/archive/main/archive-main.tar>.

{% sol %}
```sh
$  wget -q https://gitlab.com/xtec/linux/archive/-/archive/main/archive-main.tar -O archive.tar
``` 
{% endsol %}

Desempaqueta el fitxer "tar":

{% sol %}
```sh
$ tar xf archive.tar
$ cd archive
``` 
{% endsol %}


Tenim 3 fitxers `unix.*` que tenen el mateix en formats diferents:

```sh
$ ls
README.md  unix.md  unix.odt  unix.pdf
```


### gzip

`gzip` és el mètode "clàssic", i existeix des de l'any 1992

És l'any en que es van celebrar les Olimpiades a Barcelona 

`gzip` utilitza l'algorisme de compressió **"DEFLATE"**, que també s'utilitza en les imatge PNG, en el protocol web HTTP i el protocol {% link "/network/ssh/" %}.

El seu punt fort és la velocitat, l'ús eficient de la memòria i, sobretot, que s'entén amb tothom.

Com que `gzip` és una eina tan antiga, gairebé tots els Linux la tenen.

Normalment, els fitxers gzip s'emmagatzemen amb l’extensió `.gz`.

Comprimeix el fixer `unix.md`:

```sh
$ gzip unix.md 
$ ls
README.md  unix.md.gz  unix.odt  unix.pdf
```

A més d'estar comprimit, a canviat de nom, ara té `.gz` al final.

Si vols saber el rati de compressió, utlitza el flag `-l`:

```sh
$ gzip -l unix.md.gz 
         compressed        uncompressed  ratio uncompressed_name
                714                1213  43.3% unix.md
```

No està malament.

Anem a veure com ho fa amb els altres:

```sh
$ gzip *
$ gzip -l unix.md.gz unix.odt.gz unix.pdf.gz
         compressed        uncompressed  ratio uncompressed_name
                714                1213  43.3% unix.md
              52958               54972   3.7% unix.odt
              51078               56563   9.7% unix.pdf
             104750              112748   7.1% (totals)
```

Els formats `odt` i `pdf` ja estan comprimits, i `gzip` no pot fer massa per comprimir-los més. 

Amb el flag `-d` (**d**eflate) pots descomprimir un fitxer:

```sh
$  gzip -d unix.md.gz
$ ls
README.md.gz  unix.md  unix.odt.gz  unix.pdf.gz
```

Si vols descomprimir tots els fitxers:

```sh
$ gzip -d *.gz
$ ls
README.md  unix.md  unix.odt  unix.pdf
```

Si vols crear **un fitxer nou** has d'utilitzar una "canalització" amb el flag `-c` i el `>` corresponent:

```sh
$ gzip -c unix.md > unix.md.gz
$ ls
unix.md.gz  unix.md  unix.odt  unix.pdf
```

Si vols, pots escullir el nivell de compressió amb un flag "numéric": del `-1` al `-9`.

* El flag `-1` (o `--fast`) és el més ràpid i el menys eficient
* El flag `-9` (o `--slow`) és el més lent i el més eficient.

Per defecte, l'opció predeterminada és `-6`.

Anem a veure la diferència:

```sh
gzip -c -1  unix.md > unix-1.md.gz
gzip -c -6  unix.md > unix-6.md.gz
gzip -c -9  unix.md > unix-9.md.gz
```

```sh
$ gzip -l unix-*
         compressed        uncompressed  ratio uncompressed_name
                732                1213  41.8% unix-1.md        
                714                1213  43.3% unix-6.md        
                714                1213  43.3% unix-9.md        
               2160                3639  41.4% (totals)
``` 

**Activitat**. Crea un espai de treball:

```sh
$ mkdir ~/gzip
$ cd ~/gzip
```

Baixa diferents arxius de viquipedia, comprimeix els fitxers i compara la seva mida.

{% sol %}
```sh
$ wget -q https://ca.wikipedia.org/wiki/Lleida -O lleida.html
$ gzip lleida.html
$ gzip -l lleida.html.gz
```
{% endsol %}

### bzip2

`bzip2` és una implementació d'un algorisme anomenat "Burrows-Wheeler".

Comprimeix més que `gzip` a canvi d'estar més estona per fer comprimir i necessitar més memòria, **però no per descomprimir**.

Si has d'enviar fitxers per Internet, `bzip2` és una bona opció perqué són més petits si l'has d'enviar molts cops.

L'extensió que fan servir aquests fitxers és `bzip2`.

Instal.la `bzip2`:

```sh
$ sudo apt install -y bzip2
```

Descarrega un fitxer:

```sh
$ wget -q https://ca.wikipedia.org/wiki/Madrid -O madrid.html
```

`bzip2` funciona com `gzip`:

```sh
$ bzip2 madrid.html
$ bzip2 -d madrid.html.bz2
```

A continuació compara `gzip` amb `bzip2`:

```sh
$ gzip -c madrid.html > madrid.html.gz
$ bzip2 -c madrid.html > madrid.html.bz2
$ ls -l madrid.html.*
-rw-r--r-- 1 box box 66049 Oct 11 09:37 madrid.html.bz2
-rw-r--r-- 1 box box 75445 Oct 11 18:14 madrid.html.gz 
``` 

Pots veure que el fitxer `.bz2` és més petit.

### xz

`xz` va apareixer el 2009, i utilitza un algorisme de compressió conegut com LZMA2.

El problema és que fa servir més memòria que els altres.

Els fitxers "xz" tenen l'extensió `.xz`.

```sh
$ xz madrid.html
$ xz -d madrid.html.xz
```

A diferència de `bzip2`, `xz` també té l'opció `-d` per saber el rati de compressió del fitxer:

```sh
$ $ xz madrid.html
$ xz -l madrid.html.xz 
Strms  Blocks   Compressed Uncompressed  Ratio  Check   Filename      
    1       1     60.4 KiB    396.7 KiB  0.152  CRC64   madrid.html.xz
```

No està malament!

**Activitat**. Comprimeix el fitxer amb `gzip`, `bzip2` i `xz` i compara resultats:

{% sol %}
```sh
TODO
```
{% endsol %}

## Empaquetar i comprimir

Empaquetar paquets amb `tar` no redueix la mida del fitxer `tar`.

El que es fa normalment és combinar `tar` amb un dels algorismes de compressió.

Crea una carpeta `ossos`:

```sh
$ cd
$ mkdir ossos
```

Baixa alguns documents d'ossos a la carpeta `ossos`:

```sh
$ wget -q https://ca.wikipedia.org/wiki/Panda_gegant -O ossos/panda-gegant.html
$ wget -q https://ca.wikipedia.org/wiki/Os_polar -O ossos/os-polar.html
```

Pots comprimir una carpeta només amb `tar`:

```sh
$ tar cf ossos.tar ossos/
```

Però pots passar a `tar`el flag `z` per comprimir amb `gzip`:

```sh
$ tar cf ossos.tar ossos/
$ tar cfz ossos.tar.gz ossos/
$ ls -l ossos.tar*
-rw-r--r-- 1 box box 706560 Oct 11 18:50 ossos.tar   
-rw-r--r-- 1 box box 119253 Oct 11 18:50 ossos.tar.gz
```

Borra la carpeta `ossos`:

```sh
$ rm -rf ossos
```

Recupera el contingut de la carpeta `ossos`:

```sh
$ tar xfz ossos.tar.gz 
$ ls -l ossos
total 684
-rw-r--r-- 1 box box 271405 Oct 11 09:37 os-polar.html    
-rw-r--r-- 1 box box 424106 Oct 11 09:37 panda-gegant.html
```

## TODO


Continua llegint aquest document: [Document](https://docs.google.com/document/d/1oyxPKUDe2C7X0XiAQhbqmVQitwKYQtCe_dYV8BMjoq0/edit?usp=sharing)





















---
title: Filtres
description: Comandes de filtre de fitxers (cut, sort, grep, sed, awk...) aplicades aplicats al bigdata i la bioinformàtica.
---

## Introducció

La forma més ràpida i eficient de llegir i tractar fitxers de text i cercar informació dins dels discos i unitats del nostre sistema operatiu és aprofitar el terminal que ens proporciona. 

Molt més que utilitzar l'entorn gràfic o usar altres llenguatges de programació que també ho poden fer, però amb un rendiment inferior degut a la càrrega de llibreries.

Per això val la pena **aprendre i dominar les comandes de filtre de fitxers de GNU/Linux (cut, sort, grep, sed, awk...) i de cerca de directoris i fitxers (find, locate, ls)**, ja que són eines molt potents pel processament de grans volums de dades relacionades amb la bioinformàtica.

Per exemple, si volem filtrar 6 de les 30 columnes i 300 dels possibles 150.000 registres que necessitem d'un fitxer de dades d'enfermetats o de gens amb poques línies de codi és una meravella usar la shell de Linux. 

**Comandes com el grep, el cut o el sed fan meravelles.**

El rendiment de realitzar aquest preprocessament i filtratge abans d'utilitzar aquest fitxer per un programari en Pyhton que visualitzi les dades o com a dades d'entrenament per a la nostra futura IA és molt superior que volcar directament el fitxer en un programa fet amb un llenguatge d'alt nivell (Python, PHP, Java, JavaScript).

## Preparació entorn i repàs comandes.

En aquesta activitat repassarem algunes comandes i en veurem d'altres de noves, i la millor manera és aplicant-les primerament en un fitxer senzill.

Entra a la teva màquina, crea una carpeta separada i un arxiu de nom "**empleades.csv**" que contingui les següents dades:

```csv
Nom;Cognom;Ciutat;Telèfon;Edat;Email
Cristina;Ouviña;Valencia;96123456;30;ouvina@esfuerzo.com
Silvia;Domínguez;Salamanca;923294400;34;sdom@perfumerias.es
Alba;Torrens;Ekaterinburgo;0073436898812;31;a.torrens@mvp.es
Laia;Palau;Girona;972414114;41;laia@mestitolsqueanys.com
Marta;Xargay;Barcelona;0034904221336;25;marta@hotmail.com
Tamara;Abalde;Vitoria;945139291;32;mihermano@tambienjuega.es
Aitana;Bonmati;Barcelona;003469482919;24;aitanabonmati@fcb.cat
Queralt;Casas;Valencia;00346758410;28;queralt@cultura.es
Maria;Conde Valencia;Kraków;0048531601710;22;conde@wisla.pl
Nogaye;LoSylla;LaSeu;976379156;24;nogaye-losylla@feb.com
Aida;Riko;Fujimaki;0000000000;32;seirin@manga.com
Uliana;Semiónova;Getafe;918765432;39;tachenko@ijosomdelamateixaepoca.es
Alexia;Putellas;Barcelona;0034654848419;28;alexiap@fcb.cat
Claudia;Masip;Tarragona;0034652736471;18;claudia.masip@nastic.com
Nora;Chaib;LHospitalet;003465736471;21;norachaib@uecornella.cat
Sandra;Vericat;LHospitalet;00345574361;33;sandrav2udbellvitge.cat
Jennifer;Hermoso;Barcelona;003495431336;27;jennihermoso@fcb.es
Danae;Boronat;Barcelona;0000000000;41;danae.boronat@fcb.cat
```

Recorda com es fa:

```sh
mkdir m01-a041-empleades
cd m01-a041-empleades
nano empleades.csv
```

Copia i enganxa les dades i guarda-les (Ctrl+X, Y)

Fixa't que el format `csv` és molt ventatjós pels món científic, ja que permet obrir el fitxer tant en un editor de text pla com gedit o notepad++, com en Libre Office Calc, per a crear els nostres gràfics i taules dinàmiques si volem. 

Ara, repassa i prova les comandes que et posem d'exemple per verificar el funcionament i per repassar-les, ja que les hem vist prèviament.

## Repàs de comandes bàsiques.

***cat** → mostra per pantalla un fitxer sense llegir-lo*

**operador \>** → et permet guardar la sortida de comandes en un
fitxer.

**operador \|** → anomenat pipe o filtre, concatena el text de la
sortida d'una comanda.

***head, tail** → mostren les primeres / últimes línies*

***wc -l** → compta les línies d'un text ( també es poden comptar amb -w
-c)*

I què podem fer amb aquestes ? 

Mostrar les 10 primeres empleades per pantalla, i la capçalera.

```sh
head -11 empleades.csv 
```

Mostrar les 10 últimes empleades i guardar-les en un fitxer separat.

```sh
tail -11 empleades.csv > 10ultempleades.csv
```

Comptar quantes empleades hi ha al fitxer.

```sh
cat empleades.csv | wc -l
```

Volem mostrar les 5 primeres empleades, però sense que es vegi la capçalera.

```sh
cat empleades.csv | head -n 6 | tail -n+2 
```

## Filtre de fileres amb grep.

El filtre `grep` (Global Regular Expression Print) és un dels més apreciats de la Shell, et permet **filtrar si un (o més fitxers) contenen una paraula concreta (o no)** de forma molt ràpida. L'únic requisit és que sigui informació textual.

Veiem exemples amb el nostre fitxer `empleades.csv`.

Per exemple, mostra les empleades de Barcelona:

```sh
grep Barcelona empleades.csv
```

```sh
Marta;Xargay;Barcelona;0034904221336;25;marta@hotmail.com
Aitana;Bonmati;Barcelona;003469482919;24;aitanabonmati@fcb.cat
Alexia;Putellas;Barcelona;0034654848419;28;alexiap@fcb.cat
Jennifer;Hermoso;Barcelona;003495431336;27;jennihermoso@fcb.es
Danae;Boronat;Barcelona;0000000000;41;danae.boronat@fcb.cat
```

Grep té opcions per fer-lo més flexible, com `-c`, `-n` i `-v`

Mostra les empleades que no són de Barcelona (això es fa amb -v, invert )

```sh
grep -v Barcelona empleades.csv
```

Mostra el númer d'empleades que són de Barcelona (sixò es fa amb -c, és equivalent a fer | wc -l)
```sh
grep -c Barcelona empleades.csv
```
5

També és interessant l'ús de la opció `-n` que retorna el número de posició dins del fitxer de les linies trobades.

Si vols filtrar per fileres que compleixin 2 valors (almenys un dels 2) ho podem fer així:
```sh
grep -E 'Hospitalet|Barcelona' empleades.csv 
```



Per a realitzar filtres més avançats necessitarem expressions regulars (pròximament)

## Filtre i ordenació de columnes: cut, sort

Si només ens interessen unes columnes d'un fitxer (separat per comes o altres paràmetres) podem usar la comanda `cut`.

Per exemple, si només ens interessen el nom i el correu del fitxer de empleades.txt; aplicariem la comanda cut d'aquesta manera:

```sh
cat empleades.csv | cut -d ";" -f 1,2 
```

Els paràmetres més importants que tenim d'aquesta comanda són:

**-d** és per indicar el separador. -d ";" en el nostre cas. Per defecte és \t. 
**-f** identificador de les columnes que volem, separades per comes. Comença comptant per 1. En el nostre fitxer el nom és la columna 1, el cognom la 2 i el email la última que és el 6.

```sh
1       2       3       4            5      6
Marta;Xargay;Barcelona;0034904221336;25;marta@hotmail.com
```

Podem aplicar alhora comandes com el grep i el sed per obtenir resultats precisos ràpidament.

Mostra el cognom i l'edat de totes les empleades i guarda-les en un fitxer

```sh
cat empleades.csv | grep -v Barcelona | cut -d ";" -f 1,2,5 
```

**Ordenació de columnes, sort**

La comanda sort agafa la sortida del terminal i la ordena mitjançant diversos criteris.

Per defecte, **ordena per la primera columna de la sortida del terminal de forma alfanumèrica** 

Mostra el nom, cognom i l'edat de les empleades que no són de Barcelona.

```sh
cat empleades.csv | grep -v Barcelona | cut -d ";" -f 1,2,5 
```

Ara, mostra el cognom i edat de les totes empleades, ordenades alfabèticament per congom, i guarda el resultats dins el fitxer consulta_empleades.txt

```sh
cut -d ";" -f2,5 empleades.csv | sort > consulta_empleades.txt
```

Si en comptes d'ordenar per cognom volem ordenar numèricament per edat, ho fem amb dues comandes i un fitxer intermig (`empleades_cognom_edat.txt`), ja que és més entenedor.

```sh
cut -d ";" -f2,5 empleades.csv > empleades_cognom_edat.txt 
sort -t ';' -k2,2n empleades_cognom_edat.txt > informe_empleades.txt
```

A la primera comanda hem filtrat el fitxer per a què només tingui cognom i edat, abans d'ordenar-lo.

```sh
1       2
Xargay;25;
``` 

I llavors, el sort ens permet aquestes opcions:

**-t** és per indicar el separador. -d ";" en el nostre cas. Per defecte és \t. 
**-k<columna>** identificador de la columnes que volem ordenar, separades per comes. **-<columna>n** serveix per dir-li que volem ordenar la columna numèricament, ja que és un número; sinó ens la ordenaria malament. Per defecte ordena com si fos una cadena de caràcters.

## Comandes de reemplaçament de contingut: sed, tr

**sed (stream editor)** reemplaça una cadena de caràcters indicada per l'usuari per un altre, dins d'una cadena de text més llarga.

En resum, el `grep fa el mateix que el Ctrl+F` i el `sed fa com el Ctrl+H` de les aplicacions ofimàtiques, totalment automatitzat.

Anem a estudiar un exemple del sed. Imaginem-nos que tenim aquesta web. Hi apareix 3 vegades la paraula **Departament** i volem canviar-lo pel nostre nom.

```html
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Web del Departament</title>
</head>
<body>
	<h2>Web del Departament</h2>
	<p><a href="https://chiquitoipsum.com/">Lorem fistrum</a> pecador hasta luego Lucas hasta luego Lucas torpedo fistro condemor. Al ataquerl sexuarl se calle ustée fistro diodeno tiene musho peligro ahorarr a gramenawer papaar papaar caballo blanco caballo negroorl no te digo trigo por no llamarte Rodrigor.</p>
	<h3>Made with Love by Departament, CC-BY-SA 4.0</h3>
</body>
</html>
```

Ho reemplaçem molt ràpidament amb el `sed`:

```sh
$ sed -i 's/Departament/Miquel/g' web_miquel.html
```

La sintaxi bàsica de `sed` és la següent:

{% image "linux_sed.png" %}

Comentem algunes opcions del sed (n'hi ha més):

**/g** g de global. Ho posem si en comptes de la primera ocurrència volem canviar-les totes.
**> fitxer_nou** per tal de crear una còpia i no perdre la informació anterior 

Anem a verificar que ha funcionat; que ara apareix la paraula **Miquel** allà on apareixia **Departament**.

```html
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Web del Miquel</title>
</head>
<body>
	<h2>Web del Miquel</h2>
	<p><a href="https://chiquitoipsum.com/">Lorem fistrum</a> pecador hasta luego Lucas hasta luego Lucas torpedo fistro condemor. Al ataquerl sexuarl se calle ustée fistro diodeno tiene musho peligro ahorarr a gramenawer papaar papaar caballo blanco caballo negroorl no te digo trigo por no llamarte Rodrigor.</p>
	<h3>Made with Love by Miquel, CC-BY-SA 4.0</h3>
</body>
</html>
```

Un altre exemple, sobre el fitxer empleades.csv; eliminar els espais en blanc si n'hi ha:

```sh
sed -i 's/ //g' empleats.csv > empleats2.csv
```

**tr** reemplaça les aparicions d'un caràcter per un altre que indiqui l'usuari, dins d'una cadena de text.

Molt útil per convertir de majúscules a minúscules (o al revés):

```sh
tr '[:upper:]' '[:lower:]' < empleades.csv > empleades_minuscules.csv
```

O eliminar espais repetits per a què només hi hagi un.

```sh
tr -s ' ' < empleades.csv > empleades_sense_espais.csv
```

---

## Casos pràctics.

Ara et toca aplicar aquestes comandes de Linux en casos pràctics, on Linux és la millor opció preprocessar grans volums de dades; ordenats per dificultat.

### Cas pràctic 1. Nivell bàsic-intermig.

* [Activitats ús del Bash aplicat a la genòmica, CC-BY-SA 4.0](https://github.com/miquelamorosaldev/bio-bash/blob/main/bash-intro/readme.md)

### Cas pràctic 2. Nivell intermig-avançat.

* [Ús Bash aplicat grans volums de dades mèdiques, projecte Tycho, CC-BY-SA 4.0](https://github.com/miquelamorosaldev/bio-bash/blob/main/filtres-tycho/readme.md)

---

## Referències.

* [Uso del sed - Geekland ](https://geekland.eu/uso-del-comando-sed-en-linux-y-unix-con-ejemplos/)

* [Uso del grep - Geekland ](https://geekland.eu/uso-del-comando-grep-en-linux-y-unix-con-ejemplos/)

* [Dataset del Projecte Tycho, històric enfermetats EEUU](https://www.tycho.pitt.edu/data/)

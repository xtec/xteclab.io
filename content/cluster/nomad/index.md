---
title: Nomad
description: Nomad es una eina d'orquestació de contenidors que ofereix capacitat multi-cluster.
---

## Introducció

Els contenidors estan esdevenint la forma natural de desplegar aplicacions que proporcionen serveis i l’orquestació de contenidors és un dels elements que avui en dia ha de conèixer un administrador de serveis en xarxa.

En el transport de mercaderies els contenidors van canviar completament la manera en que es gestiona la logística.

Els contenidors són de tamany estàndar i no ens importa el que porta dins:

{% image "ship.png" %}

En el món de la informàtica, fins fa poc, els administradors havien de preparar el sistema operatiu perquè les aplicacions que havien de desplegar poguessin funcionar. 

Per exemple:

* Una aplicació Java necesita una màquia virtual Java concreta.
* Una aplicació Python necessita unes versions concretes de Pyhton i unes llibreries instal·lades.
* Una aplicació PHP necessita unes versions concretes de PHP i unes llibreries instal·lades.

A més, potser no pots desplegar dos aplicacions PHP diferents perquè necessiten versions PHP diferents o llibreries diferents, algunes aplicacions necessiten uns fitxers concrets, etc.

En canvi, amb els contenidors no ens hem de preocupar de res de tot això. 

{% image "jetty.png" %}

** Només hem de tenir un sistema operatiu Linux i un gestor de contenidors com docker, perquè cada aplicació que s’ha de desplegar ja porta incorporada totes les seves dependències en la imatge que es farà sevir per crear el contenidor**.

D’aquesta manera és possible l’alta disponiblitat perquè la nostra única ocupació com administradors es crear i eliminar contenidors. Ni el sistema operatiu ni els administradors necessitem conèixer que porten dins els contenidors.

{% image "docker.png" %}

Nomad pot crear, moure, eliminar, etc. contenidors de la mateixa manera que els vaixells i els molls de càrrega treballen amb les mercaderies: tot estar empaquetat en contenidors estàndard i el que hi ha dins no interessa.


## Entorn de treball

### Virtualbox


Baixa l’aplicació box.exe:

```sh
curl.exe https://box.xtec.dev/box.exe -o box.exe
```

Borra totes les màquines que gestiona box perquè coincideixin les IPs !

Crea cinc màquines virtuals:

```sh
.\box.exe create box-1 box-5 box-6 box-7 box-8
```

Arrenca les cinc màquines

```sh
.\box.exe start box-1 box-5 box-6 box-7 box-8
```

Nota: Per parar una màquina. Exemple la box-1   `.\box.exe stop box-1`

Entra a la màquina `box-1` amb l’opció de copiar la clau ssh:

```sh
.\box.exe ssh -c box-1
```

**Atenció!** Si dona error borra el fitxer `.ssh/know_host` del windows (està pendent el patch a l’aplicació box):

```sh
rm .\.ssh\known_hosts
```

### Isard

### Ansible

Clona el projecte [cluster](https://gitlab.com/xtec/cluster):

```sh
git clone https://gitlab.com/xtec/cluster.git
```

Des de la **màquina anfitriona** (PS)  llista les màquines virtuals, i compara que les ips del fitxer `inventory.yml` de la màquina `box-1` són les correctes. 

```sh
./box.exe list
```

Si no fos així, has d’actualitzar el fitxer `inventory.yml` 

Instal.la nomad:

```sh
./init.sh
```

Un cop estigui el cluster en funcionament, ves a la interfície gràfica. Es la ip de la màquina on has instal.lat ansible, i el port és el `4646`, per exemple:

[http://192.168.56.15:4646](http://192.168.56.15:4646)

Els clients poden tardar uns quants segons a conectar-se i formar el cluster.


### Wireguard

En un entorn real hauries de configurar una xarxa privada amb {% link "/network/wireguard/" %} i utilitzar l’adreça i la interfície wireguard per connectar els servidors i els clients.

### Arquitectura

Mirant només una regió, Nomad es composa de clients i servidors:

{% image "architecture.png" %}

Nomad es basa en un agent de llarga durada a cada màquina del clúster. L'agent es pot executar en mode servidor o client.

Els **servidors del clúster** són els responsables de gestionar el clúster: acceptar feines d'usuaris, gestionar clients i calcular la ubicació de les tasques. Cada regió pot tenir clients de diversos centres de dades, cosa que permet que un nombre reduït de servidors gestionen clústers molt grans.

Tots els altres agents del clúster haurien d'estar en mode client. Un client Nomad és un procés molt lleuger que registra la màquina amfitriona, dona senyals de vida cada poc temps  i executa les tasques que li assignen els servidors. L'agent s'ha d'executar a tots els nodes que formen part del clúster perquè els servidors puguin assignar treball a aquestes màquines.

### Nomad

### Web Interface 

Mentres Nomad s'estigui executant, la interfície d'usuari de Nomad també s'està executant. Està allotjat a la mateixa adreça i port que l'API Nomad HTTP sota l'espai de noms `/ui`.

Navega a l’adreça IP de la màquina servidor al port `4646` per obrir la interfície d'usuari de Nomad, per exemple [http://192.168.56.15:4646](http://192.168.56.15:4646). 

Mitjançant la interfície web pots veure la informació rellevant del clúster: servidors, clients i topologia de clúster.

#### Inspeccionar un job

Quan obres la interfície web Nomad, comences a la pàgina "Job list". 

Seleccioneu el treball d'exemple de la llista per veure els detalls del treball.

La pàgina "job details" mostra informació rellevant sobre la feina, inclòs l'estat general i els estats d'assignació desglossats per grup de tasques. El resultat és semblant a executar l'ordre `job status`.

{% image "view-job.png" %}

#### Vista servidors

La pàgina de servidors proporciona una visió classificable i filtrable dels agents del servidor Nomad. Podeu consultar els detalls d'un servidor individual des d'aquesta pàgina.

{% image "view-servers.png" %}

#### Vista clients

La pàgina de clients proporciona una vista classificable i filtrable dels agents de client Nomad. Podeu consultar els detalls d'un client individual des d'aquesta pàgina de llista.

{% image "view-clients.png" %}

#### Topologia del clúster

La interfície web Nomad proporciona una vista de la topologia del clúster i la càrrega de treball en execució. En entorns més complexos, aquesta vista et pot ajudar a entendre on i com s'executen les càrregues de treball, i et proporciona una visió d'alt nivell de la capacitat de clúster utilitzada i disponible.

{% image "view-topology.png" %}

### CLI

Nomad escolta només a l’adreça on està el servidor, en aquest exemple `192.168.56.15`. Per tant, has d’exportar la variable `NOMAD_ADDR` perquè per defecte la comanda `nomad` intenta conectar-se per localhost.

Utilitzeu la comanda `nomad node status` a per veure els nodes registrats del clúster Nomad

Hauria d’aparèixer els 4 nodes clients:

```sh

```


[Google Docs - Nomad](https://docs.google.com/document/d/1_I5TLXOFXWQUUOVUnnN3lUDacMqBAxvCpBWvorEFOMY/edit)
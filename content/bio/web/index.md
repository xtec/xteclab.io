---
title: Web
layout: default.njk
---

## Python a la web (pendent completar i revisar)

Cal explicar breument un microframework web com Flask per a presentar llistats de dades dels Pandas o Polars en JSON o en una web; i els gràfics generats (estàtics de Matplotlib i Seaborn, dinàmics de Plolty Bokeh) en una web.

No estaria de més donar seguretat bàsica de les dades en Python aquí (amb JWT), o més endavant.

La part d'HTTP (Flask o Quartz), templates (Jinja) i serveis web Rest està molt ben aconseguida.

La de gràfics és correcta però cal revisar com està estructurada.

* [HTTP amb Python](hhttps://docs.google.com/document/d/1pd9QLN7UGu9UZeU-TYra5MhyFLVbxOexWz7To2PApLE/edit)

* [Gràfics en Python a la web](https://docs.google.com/document/d/1H0ScniU7zV1TkZaqGAB17pbPUIQlNV86jFHaTNfu_Mo/edit)

---
title: Fàrmacs
---

* [Pharmaceutical Chemistry of Molecular Therapeutics (de Araujo, Saqib, Keillor, and Gunning) ](https://chem.libretexts.org/Bookshelves/Biological_Chemistry/Pharmaceutical_Chemistry_of_Molecular_Therapeutics_(de_Araujo_Saqib_Keillor_and_Gunning))
* [Medicines by design](https://chem.libretexts.org/Bookshelves/Biological_Chemistry/Medicines_by_Design_(Davis))
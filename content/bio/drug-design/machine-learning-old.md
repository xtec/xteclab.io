---
title: Aprenentatge automàtic
description: Aquest activitat repassa els algorismes d'aprenentatge automàtic d'ús habitual per a la classificació, que s'utilitzaran amb les dades de bioactivitat arxivades a PubChem per construir un model predictiu de la bioactivitat de molècules petites.
---

# Aprenentatge automàtic

## Conceptes previs Intel·ligència Artificial

**La intel·ligència artificial (IA)** és una disciplina que combina la computació, el processament
de dades i la gestió automàtica del coneixement per resoldre problemes complexos. Intenta simular difenents comportaments humans per part de les màquins.

**L'aprenentatge automàtic, o machine learning,** és una branca de la intel·ligència artificial que se
centra en algorismes de
processament intensiu de dades
per aprendre patrons complexos de
la realitat que permetin modelar-la.
Alguns poden millorar-ne
gradualment la precisió a mesura
que processen més dades.
Les xarxes neuronals artificials són un
subconjunt de l'aprenentatge automàtic i
es troben al cor dels algorismes
d'aprenentatge profund. Tant el nom com
l'estructura s'inspiren en el cervell humà,
imiten la manera en què les neurones
biològiques es comuniquen entre elles.
Les xarxes neuronals artificials s'han
d'entrenar amb conjunts de dades
per aprendre i poden millorar la seva
precisió a mesura que processen més
dades.
L'aprenentatge profund, o deep
learning, és un tipus de xarxa
neuronal artificial que utilitza
operadors més complexos i permet
resoldre problemes més complexos
amb major qualitat i menys temps de
computació.
La IA generativa fa referència a models
d'aprenentatge profund que, a partir de dades en
brut, poden "aprendre" a generar resultats més
plausibles per a cada consulta, ja siguin texts,
imatges, vídeo, codi, música o altres continguts
Intel·ligència artificial
Machine learning
Xarxes neuronals artificials
Deep learning
IA generativa
Fonts: ACCIÓ a partir d'IBM


## Introducció a l'aprenentatge automàtic.

**L'aprenentatge automàtic [1,2] és una aplicació d'intel·ligència artificial (IA)** que proporciona als sistemes informàtics la capacitat d'aprendre automàticament a partir de dades, identificar patrons i prendre prediccions o decisions amb una intervenció humana mínima. 

Se centra en el desenvolupament de models computacionals que realitzen una tasca específica sense utilitzar instruccions explícites. 

Els algorismes d'aprenentatge automàtic s'utilitzen ara en una gran varietat d'aplicacions en moltes àrees. Aquest capítol repassa els algorismes d'aprenentatge automàtic d'ús habitual per a la classificació, que s'utilitzaran amb les dades de bioactivitat arxivades a PubChem per construir un model predictiu de la bioactivitat de molècules petites a la tasca (enllaç al quadern de la conferència 8). 

Hi ha diversos articles que proporcionen revisions exhaustives sobre l'aplicació de l'aprenentatge automàtic en el descobriment i desenvolupament de fàrmacs, inclosos els documents següents: 

* [Machine Learning Basics](https://chem.libretexts.org/Courses/Intercollegiate_Courses/Cheminformatics/08%3A_Machine-learning_Basics)
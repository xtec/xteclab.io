---
title: Descobriment i desenvolupament de fàrmacs
---

## Introducció

El procés de descobriment i desenvolupament de fàrmacs requereix molt de recursos i de temps. Introduir un nou fàrmac al mercat (des del descobriment de fàrmacs a través d'assaigs clínics fins a l'aprovació de màrqueting) sol trigar entre 10 i 15 anys i costa de mitjana 1.395 milions de dòlars (dòlars del 2013) [1]. La figura 1 mostra un diagrama esquemàtic del procés de descobriment i desenvolupament de fàrmacs. 

{% image "drug-discovery-and-development.png" %}

El procés que es presenta a la figura 1 es simplifica, ignorant molts detalls dins de cada etapa. En realitat, el procés de descobriment i desenvolupament de fàrmacs és molt més complicat, tal com s'il·lustra en aquesta figura [4DM map for small-molecule drug development]( https://www.nature.com/articles/nrd.2017.217/figures/1).

Aquesta activiat descriu alguns enfocaments computacionals utilitzats en l'etapa de descobriment de fàrmacs. Per aprofundir en el descobriment de fàrmacs assistit per ordinador (o assistit per ordinador), cal conèixer els termes que s'utilitzen habitualment a continuació.

* Actius
  
  Substàncies que compleixen un nivell llindar d'activitat en una pantalla primària, que normalment mesura l'activitat dels compostos contra l'objectiu en una concentració única. Com que l'activitat es va mesurar només a una concentració única, no és possible saber si un compost pot interactuar amb l'objectiu d'una manera dosi-resposta. Sovint, l'estructura i la puresa de les substàncies de cribratge no es confirmen.

* Accessos
  
  Els hits són compostos amb activitat intrínseca (IC50, EC50, etc.) contra la diana i es caracteritzen mitjançant assaigs secundaris (que mesuren l'activitat dels compostos a concentracions múltiples). En general, com que els hits tenen una potència i/o selectivitat limitades, no són adequats per a estudis in vivo (en animals o humans). Tanmateix, proporcionen un punt de partida per a l'anàlisi de la relació de l'activitat estructural (SAR) per ajudar a millorar la potència/selectivitat i altres propietats del fàrmac.

* Condueix
    
  Un plom representa una sèrie composta que mostra una relació entre l'estructura química i l'activitat basada en la diana (en assajos bioquímics i basats en cèl·lules). Els compostos de la sèrie tenen propietats fisicoquímiques, potència i selectivitat que es consideren adequades per a l'avaluació in vivo.

* Candidats a drogues
  
  Compostos amb un fort potencial terapèutic i l'activitat i especificitat dels quals s'han optimitzat mitjançant el pas d'optimització de plom. Aquests compostos passen a l'etapa de desenvolupament preclínic per a proves in vivo amb animals.

{% image "drug-attrition-rate.png" %}

* [](https://chem.libretexts.org/Courses/Intercollegiate_Courses/Cheminformatics/07%3A__Computer-Aided_Drug_Discovery_and_Design/7.01%3A__Reading)
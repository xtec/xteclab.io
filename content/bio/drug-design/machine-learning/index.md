---
title: Aprenentatge automàtic
description: Aquest activitat repassa els algorismes d'aprenentatge automàtic d'ús habitual per a la classificació, que s'utilitzaran amb les dades de bioactivitat arxivades a PubChem per construir un model predictiu de la bioactivitat de molècules petites.
---

# Aprenentatge automàtic

## Conceptes previs Intel·ligència Artificial

{% image "ia-schema.png" %}


**La intel·ligència artificial (IA)** és una disciplina que combina la computació, el processament de dades i la gestió automàtica del coneixement per resoldre problemes complexos amb un intervenció humana mínima. Intenta simular difenents comportaments humans per part de les màquines.

Com podeu endevinar, la **IA permet moltes noves possibilitats en comparació a la programació "tradicional"** que, molt resumidament, només té els propòsits de: llegir, manipular i representar informació, resoldre problemes concrets, automatizar tasques repetitives o assolir resultats específics.



**L'aprenentatge automàtic, o machine learning,** és una branca de la intel·ligència artificial que se centra en algorismes de processament intensiu de dades per aprendre patrons complexos de la realitat que permetin modelar-la.
Alguns poden millorar-ne gradualment la precisió a mesura que processen més dades.

Les **xarxes neuronals** artificials són un nsubconjunt de l'aprenentatge automàtic i es troben al cor dels algorismes d'aprenentatge profund. Tant el nom com l'estructura s'inspiren en el cervell humà, imiten la manera en què les neurones
biològiques es comuniquen entre elles.
Les xarxes neuronals artificials s'han d'entrenar amb conjunts de dades per aprendre i poden millorar la seva precisió a mesura que processen més dades.

L'**aprenentatge profund, o deep learning,** és un tipus de xarxa neuronal artificial que utilitza operadors més complexos i permet resoldre problemes més complexos amb major qualitat i menys temps de computació.

La **IA generativa** fa referència a models d'aprenentatge profund que, a partir de dades en brut, poden "aprendre" a generar resultats més plausibles per a cada consulta, ja siguin texts, imatges, vídeo, codi, música o altres continguts.

* [Acció Gencat - Píndoles IA](https://www.accio.gencat.cat/web/.content/bancconeixement/documents/pindoles/ACCIO-inteligencia-artificial-pindola-tecnologica.pdf)

En aquest tutorial ens centrarem en l'`aprenentatge automàtic.` 


## Introducció a l'aprenentatge automàtic.

**L'aprenentatge automàtic és una aplicació d'intel·ligència artificial (IA) que proporciona als sistemes informàtics la capacitat d'aprendre automàticament a partir de dades, identificar patrons i prendre prediccions o decisions amb una intervenció humana mínima.**

{% image "what-is-ml.png" %}

Se centra en el desenvolupament de models computacionals que realitzen una tasca específica sense utilitzar instruccions explícites (com es fa a la programació de software tradicional)

Els algorismes d'aprenentatge automàtic s'utilitzen avui en dia en una gran varietat d'aplicacions en moltes àrees. 
Aquest capítol repassa els algorismes d'aprenentatge automàtic d'ús habitual per a la **classificació, que s'utilitzaran amb les dades de bioactivitat arxivades a PubChem per construir un model predictiu de la bioactivitat de molècules petites a la tasca (enllaç al quadern de la conferència 8).**

Hi ha diversos articles que proporcionen revisions exhaustives sobre l'aplicació de l'aprenentatge automàtic en el descobriment i desenvolupament de fàrmacs, inclosos els documents següents: 

* [Machine Learning Basics](https://chem.libretexts.org/Courses/Intercollegiate_Courses/Cheminformatics/08%3A_Machine-learning_Basics)


### Categories principals de tècniques d'aprenentatge automàtic (a revisar): 

#### Aprenentatge supervisat
En l'aprenentatge supervisat, un model es construeix a partir d'un conjunt de dades d'entrenament, que consisteix en un conjunt de dades d'entrada (representades amb "descriptors" o "característiques") i les seves sortides conegudes (també anomenades "etiquetes" o "objectius"). 

Aquest model s'utilitza per predir una sortida per a noves dades d'entrada (que no s'inclouen al conjunt de dades d'entrenament). En poques paraules, l'aprenentatge supervisat consisteix a construir un model, y=f(X), que prediu el valor de y a partir de les variables d'entrada (X). [Tingueu en compte que X està en majúscula per reflectir que les dades d'entrada normalment es representen amb un "vector" de múltiples descriptors.] 

Un exemple d'aprenentatge supervisat és construir un model que predigui l'afinitat d'unió de molècules petites contra una proteïna determinada en funció de la seva estructures moleculars representades amb empremtes dactilars moleculars. [Aquí, les empremtes dactilars moleculars corresponen a l'entrada i l'afinitat d'unió correspon a la sortida.]

Els algorismes d'aprenentatge supervisat es poden dividir en dues categories (algorismes de regressió i algorismes de classificació), segons el tipus de dades de sortida que pretén predir l'aprenentatge supervisat.

**Regressió**

Els algorismes de regressió tenen com a objectiu construir una funció de mapeig des de les variables d'entrada (X) fins a la variable de sortida numèrica o contínua (y). La variable de sortida pot ser un nombre enter o un valor de coma flotant i normalment representa una quantitat, mida o força. 

Un exemple de problemes de regressió és predir el valor IC50 d'un compost contra una proteïna objectiu a partir de la seva estructura molecular, per tal de mesurar l'eficàcia d'una substància en inhibir un procés biològic específic (pex una malaltia)

Un exemple més general seria preveure el curs d'una malaltia (per exemple les fases del càncer) i ajustar els tractaments preventius.


**Classificació**

Els algorismes de classificació intenten predir la variable "categòrica" ​​a partir de les variables d'entrada. 

Un exemple de problemes de classificació és predir si un compost és agonista, antagònic o inactiu contra una proteïna diana.

Exemples més generals de classificació es poden aplicar a agrupar pacients en grups de risc per a malalties cardiovasculars basant-nos en dades de historial clínic, estils de vida, i biomarcadors; o fins i tot en classificació d'imatges de radiografies per identificar presència de tumors, fractures o altres anomalies.


####  Aprenentatge no supervisat

Els mètodes d'aprenentatge no supervisat identifiquen patrons ocults o característiques intrínseques a les dades d'entrada (X) i els utilitzen per agrupar les dades. 

Al contrari de l'aprenentatge supervisat, l'aprenentatge no supervisat no utilitza etiquetes assignades a les dades d'entrenament d'entrada [és a dir, no hi ha valors de sortida/objectiu/etiqueta (y) associats a les dades d'entrada]. 
Per tant, es pot utilitzar per analitzar dades sense etiquetar. 

Un exemple dels problemes que pot gestionar l'aprenentatge no supervisat és agrupar un conjunt de compostos en petits clústers segons la seva similitud estructural (calculada mitjançant empremtes dactilars moleculars) i identificar les característiques estructurals que caracteritzen els clústers individuals. 

[Per a aquesta tasca, les dades d'entrada (X) són les empremtes dactilars moleculars dels compostos.]

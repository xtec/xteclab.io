---
title: Similitut molecular
description: La similitud molecular és un concepte fonamental en quiminformàtica, jugant un paper important en els mètodes computacionals per predir les propietats dels compostos químics, així com per dissenyar productes químics amb propietats desitjades.
---

## Introducció

La similitud molecular (també anomenada semblança química o semblança de l'estructura química) és un concepte fonamental en quiminformàtica, jugant un paper important en els mètodes computacionals per predir les propietats dels compostos químics, així com per dissenyar productes químics amb propietats desitjades. El supòsit subjacent en aquests mètodes computacionals és que és probable que les molècules estructuralment similars tinguin propietats biològiques i fisicoquímiques similars (comunament anomenat principi de similitud). La similitud molecular és un concepte senzill i fàcil d'entendre, però no hi ha una definició matemàtica absoluta de semblança molecular en la qual tothom estigui d'acord. Com a resultat, hi ha un nombre pràcticament infinit de mètodes de similitud molecular, que quantifiquen la similitud molecular. 

## Similar-Structure, Similar-Property Principle 

The Similar-Structure, Similar Property Principle is the fundamental assertion that similar molecules will also tend to exhibit similar properties. These properties can either be physical (e.g. boiling points) or biological (e.g. activity).

Example 1: Hexane and heptane should have similar boiling points and water solubility.

{% panel "Hexane" %}
<iframe style="width: 500px; height: 300px;" frameborder="0" src="https://embed.molview.org/v1/?mode=balls&cid=8058"></iframe>
{% endpanel %}

{% panel "Heptane" %}
<iframe style="width: 500px; height: 300px;" frameborder="0" src="https://embed.molview.org/v1/?mode=balls&cid=8900"></iframe>
{% endpanel %}

Example 2: Cocaine and procaine are both local anesthetics

{% panel "Cocaine" %}
<iframe style="width: 500px; height: 300px;" frameborder="0" src="https://embed.molview.org/v1/?mode=balls&cid=446220"></iframe>
{% endpanel %}

{% panel "Procaine" %}
<iframe style="width: 500px; height: 300px;" frameborder="0" src="https://embed.molview.org/v1/?mode=balls&cid=4914"></iframe>
{% endpanel %}

Quantitative Structure-Property Relationships (QSPR) and Quantitative Structure-Activity Relationships (QSAR) use statistical models to relate a set of predictor values to a response variable. Molecules are described using a set of descriptors, and then mathematical relationships can be developed to explain observed properties. In QSPR and QSAR physico-chemical properties of theoretical descriptors of chemicals are used to predict either a physical property or a biological outcome.

In either case, a set of known molecules is used to create a **training set** that a statistical model can be derived from. These molecules have known properties or activities. An outside test set is used to validate the model. The test set consists of other molecules with known properties that are excluded from the training set. After the model is validated, it can be used to predict properties or activities of molecules that are outside the previous sets. One caveat- new test molecules cannot be sufficiently different from the ones used in previous sets.

## Molecular Descriptors

If we want to develop a computational model to predict properties, we need to be able to describe them in ways that can be tied to a biological or physical properties. There are many ways that we can represent organic molecules.

## TODO

* [Quantitative Structure Property Relationships ](https://chem.libretexts.org/Courses/Intercollegiate_Courses/Cheminformatics/05%3A_5._Quantitative_Structure_Property_Relationships)

* [Molecular Similarity ](https://chem.libretexts.org/Courses/Intercollegiate_Courses/Cheminformatics/06%3A_Molecular_Similarity)
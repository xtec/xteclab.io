---
title: Docking
description: L'acoblament molecular és una tècnica computacional que s'utilitza per predir com molècules petites, com ara candidats a fàrmacs, s'uneixen a una proteïna diana o àcid nucleic (ADN/ARN) d'interès.
---

* [AutoDock](https://autodock.scripps.edu/)
* [Python and Molecular Docking](https://www.linkedin.com/pulse/python-molecular-docking-ganeshkumar-palanisamy) 
* [Dockstring](https://github.com/dockstring/dockstring)
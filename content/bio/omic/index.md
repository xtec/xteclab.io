---
title: Omics
description: Los análisis de metodologías individuales, com la secuenciación de ADN y ARN, secuenciación de proteínas, expresión genética, ensamblajes de genoma, análisis de PTM, la interacción proteína-proteína e identificación y cuantificación de metabólicos, entre otros…
---

### Àcidos nucleicos y proteínas

{% pages ["nucleic-acid/", "gene-expression/", "sequence-formats/", "entrez/", "protein/"] %}

### Análisis de secuencias

{% pages ["sequence-alignment/", "blast/", "multiple-sequence-alignment/"] %}
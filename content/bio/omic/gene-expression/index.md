---
title: Expressió gènica
description: L’expressió gènica és el procés mitjançant el qual la informació continguda en els gens d'una seqüència d'ADN es transcriu en RNA i, posteriorment, es tradueix en proteïnes funcionals, proporcionant una visió detallada del comportament molecular dels gens en diferents condicions i contextos biològics.
---

## Introducció

L’expressió gènica és el procés mitjançant el qual la **informació continguda en els gens d'una seqüència d'ADN es transcriu en RNA i, posteriorment, es tradueix en proteïnes funcionals**, proporcionant una visió detallada del comportament molecular dels gens en diferents condicions i contextos biològics.

Una `molècula d'ADN` no és només una llarga cadena de nucleòtids. En realitat, `es divideix` en unitats funcionals anomenades `gens`. 

Cada gen proporciona les instruccions per formar un producte funcional, és a dir, una molècula necessària per exercir un treball a la cèl·lula. En molts casos, el producte funcional és una proteïna. 

Per exemple, a l'experiment de Mendel, el gen del color de les flors té les instruccions per fer una proteïna que ajuda a produir molècules acolorides (pigments) als pètals de les flors.

{% image "mendel-flors.png" %}

En aquest esquema, veiem com el gen del color de les flors que va estudiar Mendel es compon d´una tira d´ADN que es troba en un cromosoma. 

L´ADN del gen especifica la producció d´una proteïna que ajuda a formar pigments. Quan la proteïna és present i és funcional, es produeixen pigments i les flors de la planta tenen un color lila.

El **producte funcional de la majoria dels gens són proteïnes, o per ser més exactes, polipèptids.**

El terme `polipèptid` només és una paraula per designar una `cadena d'aminoàcids`. Tot i que moltes proteïnes es conformen d'un sol polipèptid, algunes estan fetes de diversos polipèptids. Els gens que especifiquen polipèptids es coneixen com a gens codificants de proteïnes.

No tots els gens codifiquen proteïnes. Per contra, alguns proporcionen instruccions per produir molècules d'ARN funcionals, com els `ARN de transferència` i els `ARN ribosomals` que exerceixen `papers en la traducció`.


## Com pot una seqüència d'ADN d'un gen determinar quina proteïna es forma?

Molts gens proporcionen instruccions per produir polipèptids. Com dirigeix ​​exactament l'ADN la construcció d'un polipèptid? 

Aquest procés consta de dos passos: `transcripció` i `traducció`.

* A la `transcripció`, la seqüència d'ADN d'un gen es copia per obtenir una molècula d'ARN. Aquest procés és anomenat transcripció perquè implica tornar a escriure, o transcriure, la seqüència d'ADN en un "alfabet" d'ARN similar. En eucariotes, la molècula d'ARN ha de sotmetre's a un processament per convertir-se en un `ARN missatger (ARNm) madur`.

* A la `traducció`, la seqüència d'ARNm es descodifica per especificar la seqüència d'aminoàcids d'un polipèptid. El nom traducció reflecteix que la seqüència de nucleòtids de l'ARNm s'ha de traduir a l'idioma, completament diferent, dels aminoàcids.

Per exemple, suposem que les dues cadenes de ADN tienen las següents seqüències: 

```sh
            5'-ATGATCTCGTAA-3' 
            3'-TACTAGAGCATT-5'
```

La `transcripció` d'una de les cadenes d'ADN produeix un ARNm amb una seqüència gairebé idèntica a l'altra cadena d'ADN. A causa de les diferències bioquímiques entre l'ADN i l'ARN, `les T de l'ADN es reemplacen amb U a l'ARNm`. 

La seqüència d'ARNm és: 
```sh
            5'-AUGAUCUCGUAA-5'
```

Un cop completada la transcripció, la `traducció` implica llegir els `nucleòtids de l'ARNm en grups de tres`, cadascun dels quals especifica un `aminoàcid` (o proporciona un senyal de terminació que indica que ha finalitzat la traducció).

```sh
            3'-AUG AUC UCG UAA-5'
```

* AUG →metionina    
* AUC →isoleucina     
* UCG →serina     
* UAA →"stop"

Per tant, la seqüència del polipèptid és: 

```sh
(extremo-N) metionina-isoleucina-serina (extremo-C)
```

Por lo tanto, durante la expresión de un gen codificante de proteína, la información fluye unidireccionalmente de 

`ADN → ARN → proteïna`. 

Aquest fluxe d'informació es coneix com `el dogma central de la biologia mol·lecular`.

{% image "dogma.png" %}

Els gens no codificants (gens que produeixen ARN funcionals) també es transcriuen per produir ARN, però aquest ARN no es tradueix en un polipèptid.

**Per a qualsevol tipus de gen, el procés de passar d'ADN a producte funcional es coneix com a expressió gènica.**

## Transcripció

En **la transcripció**, una cadena d'ADN del gen, anomenada `cadena motlle`, serveix de plantilla perquè una enzim (una molècula que accelera reaccions químiques dins dels éssers vius), l'ARN polimerasa, sintetitzi una cadena d'ARN complementària, anomenada **transcrit primari**. 

**Les dues cadenes d'ADN tenen les següents seqüències:**
- **5'-ATGATCTCGTAA-3'** (cadena codificant)  
- **3'-TACTAGAGCATT-5'** (cadena motlle)

Durant el procés, l'ADN s'obre formant una bombolla, i la cadena inferior (motlle) dirigeix la síntesi de l'ARN. El transcrit primari és complementari a la cadena motlle, però gairebé idèntic a la cadena codificant. L'única diferència és que en l'ARN les **T (timines)** es substitueixen per **U (uracils)**.

**Seqüència de l'ARNm resultant:**  
**5'-AUGAUCUCGUAA-5'**

{% image "adn-motlle-arn.png" %}

Així doncs, el transcrit primari conserva la informació de la `cadena codificant`, però amb **uracils (U)** en lloc de **timines (T)**. Aquesta substitució és una diferència bioquímica clau entre l'ADN i l'ARN.

### Transcripció i ARNm en bacteris i eucariotes

En **bacteris**, el transcrit primari pot funcionar directament com a ARN missatger (**ARNm**). El seu nom prové del fet que actua com a missatger entre l'ADN i els **ribosomes**. Els ribosomes són estructures compostes per ARN i proteïnes situades al citosol, on es formen les proteïnes.

En **eucariotes** (com els humans i les plantes), el transcrit primari ha de passar per passos addicionals per convertir-se en un ARNm madur. Durant aquest **processament**, s'afegeixen **casquets** als dos extrems de l'ARN i es retiren algunes parts mitjançant un procés conegut com a **empalmament**. Aquests passos no tenen lloc en els bacteris.

{% image "eukariotes-prokariotes.PNG" %}

#### **Cèl·lula eucariota**  
- La transcripció ocorre al **nucli**, on es troba l'ADN.  
- El transcrit primari és processat dins el nucli per convertir-se en **ARNm madur**.  
- L'ARNm és exportat al **citosol**, on es combina amb un ribosoma per dirigir la síntesi de polipèptids en el procés de **traducció**.

#### **Bacteri**  
- La transcripció ocorre al **citosol**.  
- L'ARNm no necessita ser transportat i pot ser traduït immediatament.  
- De fet, els ribosomes poden començar a traduir l'ARNm **mentre encara s'està transcrivint**.

### Diferència principal en la ubicació
En eucariotes, la transcripció té lloc al nucli, mentre que la **síntesi de proteïnes** (traducció) ocorre al citosol. Per això, l'ARNm ha de sortir del nucli abans de ser traduït en un polipèptid. En canvi, els bacteris, que no tenen nucli, duen a terme la transcripció i la traducció **simultàniament al citosol**. 

## Traducció

Després de la transcripció (i d'alguns passos de processament en eucariotes), la molècula d'ARNm està a punt per dirigir la síntesi de proteïnes. 

El procés de fer servir informació d'un ARNm per produir un polipèptid s'anomena traducció.

### El codi genètic

Durant la traducció, la `seqüència de nucleòtids d'un ARNm` es `tradueix` en la `seqüència d'aminoàcids d'un polipèptid`. 

L’ADN fa servir una codificació de base 4 (A,T,C,G). Per tant, **un codó pot tenir 43 valors possibles, que equivalen a 64 codons.**

Específicament, **els nucleòtids de l'ARNm es llegeixen en triplets (grups de tres) anomenats codons**:

* Hi ha 61 codons que especifiquen aminoàcids. 
* Un d'aquests codons és un **codó d'inici que assenyala on comença la traducció**. El codó d'inici codifica per a l'aminoàcid metionina, per la qual cosa la majoria dels polipèptids comencen amb aquest aminoàcid. 
* **Tres codons més de "terminació" indiquen el final d'un polipèptid**. 

Aquestes relacions s'anomenen `codi genètic`.

{% image "aminoacids_table.png" %}

{% image "arn_prot.png" %}


## Passos de la traducció

La **traducció ocorre dins d'estructures conegudes com a ribosomes.** Els ribosomes són màquines moleculars la funció de les quals és **construir polipèptids.** Quan un ribosoma es munta sobre un ARNm i troba el codó d'inici, es desplaçarà ràpidament per l'ARNm un codó alhora. 

En avançar, construirà a poc a poc una cadena d'`aminoàcids` que reflecteix exactament la seqüència de `codons` a l'ARNm.

Com "sap" el **ribosoma quin aminoàcid inserir per a cada codó**? Doncs resulta que aquesta correspondència no la fa el ribosoma per ell mateix; depèn d'un grup de molècules d'ARN especialitzades anomenades ARN de transferència (`ARNt`).

Cada ARNt té tres nucleòtids que sobresurten en un extrem i poden reconèixer (complementar les bases amb) un o uns quants codons en particular. 

A l'altre extrem, l'ARNt transporta un aminoàcid: específicament, l'aminoàcid que correspon amb aquests codons.

{% image "trad-ribosomes.png" %}

Aquest esquema ilustra peça per peça com es construeix la cadena d'aminoàcids. La traducció acaba quan el ribosoma troba un codó de finalització i llibera el polipèptid.

### Taula de traducció

El conjunt complet de relacions entre els codons i els aminoàcids (o senyals d'acabament) es coneix com el `codi genètic`. 

Amb freqüència, el codi genètic es resumeix com una taula.

{% image "gene_code.png" %}

Cada seqüència de tres lletres de nucleòtids d' ARNm correspon a un aminoàcid en específic o a un codó d' acabament. 

`UGA`, `UAG` i `UAA` són codons d' acabament. 

`AUG` és el codó de metionina a més de ser el codó d'inici.

Observa com a la taula molts aminoàcids estan representats per més d'un codó. 

Com a exemple, hi ha sis formes diferents d'escriure leucina en el llenguatge de l'ARNm (tracta de veure si pots trobar les sis). 

En total són 20 aminoàcids. 

Una característica important del **codi genètic és que és universal**. 

És a dir, amb petites excepcions, pràcticament totes les espècies (des dels bacteris fins a tu mateix) fan servir el codi genètic que es mostra a dalt per a la síntesi de proteïnes.

### Marc de lectura

El marc de lectura determina com es divideix l'`ARNm` en codons. Per exemple, una seqüència pot donar lloc a proteïnes completament diferents segons el marc triat:

{% image "cds_marc_lect.png" %}

L'ARNm a continuació pot codificar tres proteïnes totalment diferents, segons el marc de lectura amb què es llegeixi.

Així, com sap una cèl·lula quina d'aquestes proteïnes fer? 

La clau és el codó d'inici. 

A la imatge que has vist, la posició del codó d'inici assegura que es tria el marc 3 per a traduïr el ARNm.

La posició del codó d'inici assegura el marc correcte. Si hi ha mutacions (com la inserció o eliminació de nucleòtids), el marc de lectura pot alterar-se, causant errors en la proteïna:

{% image "cds_mutacio.png" %}

<em>La il·lustració mostra una mutació de marc de referència on el marc de lectura s'altera per la eliminació de dos aminoàcids.</em>

---

### Un gen és igual a un enzim ?

Allò inicialment descobert entre els gens i els enzims es va denominar hipòtesis "un gen, un enzim". 

Aquesta hipotesi ha patit algunes actualitzacions importants: 

* **Alguns gens codifiquen proteïnes que no són enzims.** Els enzims són només una categoria de les proteïnes. A les cèl·lules hi ha moltes proteïnes que no són enzims i també estan codificades per gens. 

* **Alguns gens codifiquen una subunitat d'una proteïna, no una proteïna sencera.** En general, un gen codifica un polipèptid, és a dir una cadena d'aminoàcids. Algunes proteïnes es conformen de diversos polipèptids de diferents gens. 

* **Alguns gens no codifiquen polipèptids.** Alguns gens de fet codifiquen molècules d'ARN funcional en lloc de polipèptids.

Tot i que el concepte d'"un gen, un enzim" no és del tot precís, la seva idea central -que un gen típicament especifica una proteïna en una relació un a un- encara és útil per als genetistes avui dia.

--- 

### Activitat.

Respòn les següents preguntes:

**1.** Què és l'expressió gènica?

a) El procés pel qual l'ADN es duplica abans de la divisió cel·lular.

b) El procés mitjançant el qual la informació dels gens es transcriu en ARN i es tradueix en proteïnes funcionals.

c) La síntesi de lípids a partir de proteïnes.

d) La degradació de proteïnes en aminoàcids.

**2.** Quins són els dos passos principals de l'expressió gènica?

a) Transcripció i traducció.

b) Replicació i transcripció.

c) Transcripció i mutació.

d) Traducció i replicació.

**3.** Durant la transcripció, quina base nitrogenada de l'ARN es complementa amb la timina (T) de l'ADN?

a) Adenina (A).

b) Guanina (G).

c) Citosina (C).

d) Uracil (U).

**4.** Quin dels següents símbols representa una base ambigua no ambigüa ?

a) N

b) Y

c) T

d) X

**5.** Quin orgànul cel·lular està present tant en cèl·lules procariotes com en cèl·lules eucariotes, malgrat les diferències en estructura i funció?

a) Nucli

b) Mitocondri

c) Ribosoma

d) Flagel


**6.** Què és un polipèptid?

a) Una cadena de nucleòtids que forma l'ADN.
b) Una cadena d'aminoàcids que constitueix una proteïna.
c) Una molècula de lípid coplex.
d) Un tipus de sucre present en l'ARN


**7.** Què determina el marc de lectura d'una seqüència d'ARN?

a) La presència de timina en comptes d'uracil.
b) El punt d'inici de la traducció.
c) Les proteïnes que s’uneixen al promotor de l'ADN.
d) El nombre de ribosomes associats a la seqüència.


**8.** Què passa si es produeix una mutació en el marc de lectura d'una seqüència d'ARN?

a) Es tradueix una proteïna incorrecta, sovint no funcional.
b) El procés de transcripció no pot tenir lloc.
c) L'ARN no pot ser exportat fora del nucli.
d) La síntesi de proteïnes no es veu afectada.


{% sol %}

1. b) El procés mitjançant el qual la informació dels gens es transcriu en ARN i es tradueix en proteïnes funcionals.

2. a) Transcripció i traducció.

3. d) Uracil (U).

4. c) T
(T és una base específica, no ambigua. X significa una base desconeguda. 
N significa qualsevol base. Y significa una base pirimidina C o T).

5. c) Ribosoma.

6. b) Una cadena d'aminoàcids que constitueix una proteïna.

7. b) El punt d'inici de la traducció.

8. a) Es tradueix una proteïna incorrecta, sovint no funcional.

{% endsol %}

---

## Biopython

Clona el projecte que hem preparat i segueix les instruccions del `readme.md`

```sh
git clone https://gitlab.com/xtec/bio/sequence.git
```

L’objecte `Seq` permet traduir una seq d’`ARNm` a la seqüència de proteïnes corresponent, aprofitant de nou un dels mètodes biològics de l'objecte `Seq`:

Crea un programa per provar trossos de codi; per exemple a **tests/test.py**

```py
from Bio.Seq import Seq

def test():
   rna = Seq("AUGGCCAUUGUAAUGGGCCGCUGAAAGGGUGCCCGAUAG")
   assert Seq("MAIVMGR*KGAR*") == rna.translate()
```

Prova que funcioni des de VSCode o amb la comanda:

```sh
pytest tests/tests_seq.py
```

Com ja saps de <https://xtec.dev/bio/omic/nucleic-acid/> a efectes de computació l’única diferència entre l’ADN codificant i l’ARNm és que les Ts són Us (i a l’inversa).

Per això pots també pots traduir directament des de la seqüència d'ADN de cadena codificant:

```py
from Bio.Seq import Seq

def test():
   dna = Seq("ATGGCCATTGTAATGGGCCGCTGAAAGGGTGCCCGATAG")
   rna = Seq("AUGGCCAUUGUAAUGGGCCGCUGAAAGGGUGCCCGAUAG")

   assert Seq("MAIVMGR*KGAR*") == dna.translate()
   assert Seq("MAIVMGR*KGAR*") == rna.translate()
```

Recorda que: 
1. El codó d’inici és la `Metionina (AUG)`
2. Els codons de parada són `UAA, UAG i UGA` .
3. Que el símbol de l’`*` indica el final d’un tros de seqüència, el codó de parada

La seqüència ARN anterior comença pel codi d’inici `AUG`, i té dos codons de parada `UGA` i `UAG`.

`AUG` GCC AUU GUA AUG GGC CGC `UGA` AAG GGU GCC CGA `UAG`

Per tant, la seqüència de ARNm està codificant 2 proteïnes.

Pots especificar un altre símbol de parada no si t’agrada l'asterisc predeterminat; tot i que per ara no és necessari:

```py
from Bio.Seq import Seq

def test():
   rna = Seq("AUGGCCAUUGUAAUGGGCCGCUGAAAGGGUGCCCGAUAG")
   assert Seq("MAIVMGR@KGAR@") == rna.translate(stop_symbol="@")
```

Si només vols traduir els nucleòtids fins al primer codó de parada  (com passa a la natura) tens el paràmetre to_stop:

```py
from Bio.Seq import Seq

def test():

   rna = Seq("AUGGCCAUUGUAAUGGGCCGCUGAAAGGGUGCCCGAUAG")

   assert Seq("MAIVMGR") == rna.translate(to_stop=True)
```

Pots veure que quan fas servir l'argument to_stop el codó de parada en si no es tradueix i el símbol de parada no s'inclou al final de la seqüència de proteïnes.

#### seq3. Presentar 3 primeres lletres.

A vegades ens facilita la vida mostrar la traducció de ADN/ARN a aminoàcids amb 3 caràcters (pex `Met` en comptes d'`M`). 

Això ho podem aconseguir fàcilement amb el mètode `seq3()` de Biopython; que automàticament tradueix els aminoàcids amb lletres de 3 caràcters.

```py
from Bio.Seq import Seq
from Bio.SeqUtils import seq3

dna: str = "ATGACGTCAACG"
print(f"ADN(3): {dna}")
aminoacids = Seq(dna).translate()
seq3_aminoacids = seq3(aminoacids)
print(f"CDS(3): {seq3_aminoacids}")
```

---

### Taules de traducció disponibles.

Les **taules de traducció disponibles a Biopython es basen en les de l'NCBI.**

Els objectes Seq utilitzen objectes de taula de codons derivats de la informació de l'NCBI.

En aquesta adreça web pots obtenir més informació <https://www.ncbi.nlm.nih.gov/Taxonomy/Utils/wprintgc.cgi>

Per defecte, la traducció utilitzarà el codi genètic estàndard (ID de la taula NCBI 1). 

Però si estem davant d'una seqüència mitocondrial hem de dir a la funció de traducció que utilitzi el codi genètic rellevant:

```py
>>> coding_dna.translate(table="Vertebrate Mitochondrial")
Seq('MAIVMGRWKGAR*')
```

També pots especificar la taula utilitzant el número de taula NCBI que és més curt i sovint s'inclou a característica d'anotació dels fitxers GenBank:

```py
from Bio.Seq import Seq

def test():
   rna = Seq("AUGGCCAUUGUAAUGGGCCGCUGAAAGGGUGCCCGAUAG")
   assert Seq("MAIVMGRWKGAR*") == rna.translate(table=2)
```

Pots verificar que sigui per número d'identificació o nom la referència és a la mateixa taula.

```py
from Bio.Data import CodonTable
from Bio.Seq import Seq
import pytest

def test():
   mito_table = CodonTable.unambiguous_dna_by_name["Vertebrate Mitochondrial"]

   assert CodonTable.unambiguous_dna_by_id[2] == mito_table
```

Pots **imprimir les taules si vols.**

Per exemple:

```py
from Bio.Data import CodonTable

print(CodonTable.unambiguous_dna_by_name["Standard"])
```

```sh
  |  T      |  C      |  A      |  G      |
--+---------+---------+---------+---------+--
T | TTT F   | TCT S   | TAT Y   | TGT C   | T
T | TTC F   | TCC S   | TAC Y   | TGC C   | C
T | TTA L   | TCA S   | TAA Stop| TGA Stop| A
T | TTG L(s)| TCG S   | TAG Stop| TGG W   | G
--+---------+---------+---------+---------+--
C | CTT L   | CCT P   | CAT H   | CGT R   | T
C | CTC L   | CCC P   | CAC H   | CGC R   | C
C | CTA L   | CCA P   | CAA Q   | CGA R   | A
C | CTG L(s)| CCG P   | CAG Q   | CGG R   | G
--+---------+---------+---------+---------+--
A | ATT I   | ACT T   | AAT N   | AGT S   | T
A | ATC I   | ACC T   | AAC N   | AGC S   | C
A | ATA I   | ACA T   | AAA K   | AGA R   | A
A | ATG M(s)| ACG T   | AAG K   | AGG R   | G
--+---------+---------+---------+---------+--
G | GTT V   | GCT A   | GAT D   | GGT G   | T
G | GTC V   | GCC A   | GAC D   | GGC G   | C
G | GTA V   | GCA A   | GAA E   | GGA G   | A
G | GTG V   | GCG A   | GAG E   | GGG G   | G
--+---------+---------+---------+---------+--
```

Una taula té varies propietats:

```py
from Bio.Data import CodonTable
from Bio.Seq import Seq
import pytest

def test():
   std_table = CodonTable.unambiguous_dna_by_name["Standard"]
   mito_table = CodonTable.unambiguous_dna_by_name["Vertebrate Mitochondrial"]

   assert std_table.stop_codons == ["TAA", "TAG", "TGA"]
   assert mito_table.stop_codons == ["TAA", "TAG", "AGA", "AGG"]

   assert std_table.start_codons == ["TTG", "CTG", "ATG"]
   assert mito_table.start_codons == ["ATT", "ATC", "ATA", "ATG", "GTG"]

   assert std_table.forward_table["ACG"] == "T"
   assert mito_table.forward_table["ACG"] == "T"
```

---

### CDS - Coding Seqüence.

La regió de codificació d'un gen, també coneguda com a CDS per les sigles en anglès (Coding Sequence), és aquesta porció de l'ADN d'un gen o bé ARN que codifica la proteïna.

La regió comença generalment a l'extrem 5' per un codó d'inici i acaba a l'extrem 3' amb un codó de terminació.

{% image "biopython-cds.png" %}

Suposa que teniu una seqüència de codificació completa `CDS`, és a dir, **una seqüència de nucleòtids (per exemple, ARNm - després de qualsevol empalmament) que és un nombre sencer de codons** (és a dir, la longitud és múltiple de tres), **comença amb un codó inicial, acaba amb un codó de parada i no té codons de parada interns dins del marc.** 

Donat un CDS complet, t, el mètode de traducció predeterminat farà el que vulgueu (potser amb l'opció `to_stop`). 

Tanmateix, què passa si la vostra seqüència utilitza un codó d'inici no estàndard? 

Això passa molt en els bacteris, per exemple el `gen yaaX` a `E. Coli K12`: <https://biocyc.org/gene?orgid=ECOLI&id=G6081>, que és un bacteri que viu a la flora intestinal dels mamífers, i és interessant per a realitzar estudis.

Aquests bacteris són inofensius, encara que al <a href="https://www.youtube.com/watch?v=5YjkaVCApFQ">capítol 1x03 de Rick And Morty</a> s'hagin pensat el contrari. 


```py
from Bio.Seq import Seq

def test():
   gene = Seq(
       "GTGAAAAAGATGCAATCTATCGTACTCGCACTTTCCCTGGTTCTGGTCGCTCCCATGGCA"
       "GCACAGGCTGCGGAAATTACGTTAGTCCCGTCAGTAAAATTACAGATAGGCGATCGTGAT"
       "AATCGTGGCTATTACTGGGATGGAGGTCACTGGCGCGACCACGGCTGGTGGAAACAACAT"
       "TATGAATGGCGAGGCAATCGCTGGCACCTACACGGACCGCCGCCACCGCCGCGCCACCAT"
       "AAGAAAGCTCCTCATGATCATCACGGCGGTCATGGTCCAGGCAAACATCACCGCTAA"
   )

   assert (
       Seq(
           "VKKMQSIVLALSLVLVAPMAAQAAEITLVPSVKLQIGDRDNRGYYWDGGHWRDHGWWKQH"
           "YEWRGNRWHLHGPPPPPRHHKKAPHDHHGGHGPGKHHR*"
       ) == gene.translate(table="Bacterial")
   )
```

En el codi genètic bacterià, GTG és un codó d'inici vàlid i, tot i que normalment codifica Valina, si s'utilitza com a codó d'inici s'ha de traduir com a Metionina. 

Si dius a Biopython que seqüència és un CDS complet la transcripció és correcta:

```py
from Bio.Seq import Seq

def test():
   gene = Seq(
       "GTGAAAAAGATGCAATCTATCGTACTCGCACTTTCCCTGGTTCTGGTCGCTCCCATGGCA"
       "GCACAGGCTGCGGAAATTACGTTAGTCCCGTCAGTAAAATTACAGATAGGCGATCGTGAT"
       "AATCGTGGCTATTACTGGGATGGAGGTCACTGGCGCGACCACGGCTGGTGGAAACAACAT"
       "TATGAATGGCGAGGCAATCGCTGGCACCTACACGGACCGCCGCCACCGCCGCGCCACCAT"
       "AAGAAAGCTCCTCATGATCATCACGGCGGTCATGGTCCAGGCAAACATCACCGCTAA"
   )

   assert Seq(
       "VKKMQSIVLALSLVLVAPMAAQAAEITLVPSVKLQIGDRDNRGYYWDGGHWRDHGWWKQH"
       "YEWRGNRWHLHGPPPPPRHHKKAPHDHHGGHGPGKHHR*"
   ) == gene.translate(table="Bacterial")

   assert Seq(
       "MKKMQSIVLALSLVLVAPMAAQAAEITLVPSVKLQIGDRDNRGYYWDGGHWRDHGWWKQH"
       "YEWRGNRWHLHGPPPPPRHHKKAPHDHHGGHGPGKHHR"
   ) == gene.translate(table="Bacterial", cds=True)
```

Fixa’t en que el primer codon és diferent en funció del valor de l’argument cds.

Quant passes com argument `cds=True`, si la seqüència codificant no és un cds la funció retornarà una excepció: 

```py
from Bio.Data.CodonTable import TranslationError
from Bio.Seq import Seq
import pytest

def test():
   gene = Seq("GTTAAAAAGATGCAATCTATCGTACTCGCACTTTCCCTGGTTCTGGTCGCTCCCATGGCA")

   with pytest.raises(TranslationError):
       gene.translate(table="Bacterial", cds=True)
```

---

## Exercicis

### Projecte Web

A partir del projecte <https://gitlab.com/xtec/bio/sequences>, amb l'ajuda de la llibreria Bionode, crea una pàgina web que:

**1.-** Donada una seqüència d’ADN o ARNm tradueix aquesta seqüència a aminoàcids.

**2.-** Marca en colors (o d’una altra manera diferenciada) els codons d’inici i de parada; o bé els codons codificants

**3.-** Fes altres millores que consideris adients. Alguns exemples.

- Valida que la seqüència d'ADN/ARN no tingui caràcters ambigus.
- Comprova que el número de nucleòtids sigui múltiple de 3.
- Mostra el segments de proteines codificades (CDS) i les que no estan completes.
- Ofereix una traducció dels símbols dels aminoàcids als seus noms.

---

### Projecte API

Per aquest projecte pots utilitzar qualsevol dels 2 projectes que fins ara hem treballat en ciències Òmniques:

- <https://gitlab.com/xtec/bio/biopython-api>
- <https://gitlab.com/xtec/bio/sequence>

Necessitaràs un entorn que treballi amb Python, Biopython, Pytest i FastAPI.

**1.-** Donada una seqüència d’ADN o ARNm tradueix aquesta seqüència a aminoàcids.

Pots enviar un missatge d'error o advertència si la cadena és ambigua o la seva longitud no és múltiple de 3.

{% sol %}

```py
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from Bio.Seq import Seq

app = FastAPI()

class DNARequest(BaseModel):
    dna: str
    codon_table: str | None = None  # Opcional, amb valor per defecte

class ProteinResponse(BaseModel):
    protein: str
    response: str

@app.post("/api/sequence/translate", response_model=ProteinResponse)
def translate_dna(request: DNARequest):
    try:
        dna = request.dna.upper()
        # Validació de la seqüència
        unambiguous = set(dna).issubset({"A", "C", "G", "T", "U"})
        multiple3 = len(dna) % 3 == 0

        if not unambiguous or not multiple3:
            raise HTTPException(
                status_code=400,
                detail="Seqüència no vàlida. Només es permeten A, C, G, T, U i la longitud ha de ser múltiple de 3."
            )

        # Traducció de la seqüència
        dna_seq = Seq(dna)
        protein_seq = str(dna_seq.translate(
            table=request.codon_table if request.codon_table else 1))
        return ProteinResponse(protein=protein_seq, response="Traducció correcta")

    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Error en la traducció: {str(e)}")
```

Amb FastAPI; hem generat la següent prova:

```sh
curl -X 'POST' \
  'http://localhost:8000/api/sequence/translate' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "dna": "ATGTGCTAGATGTTAGATTAG",
  "codon_table": "2"
}'
```

El resultat ha estat:

```sh
200	
Response body

{
  "protein": "MC*MLD*",
  "response": "Traducció correcta"
}

Response headers

 content-length: 54  content-type: application/json  date: Thu,06 Feb 2025 22:29:37 GMT  server: uvicorn 
```

{% endsol %}

**2.-** Envia un array/llista amb totes les proteïnes (pots usar el mètode split aplicat al separador '*' o un altre separador)

**3.-** Ofereix l’opció d’utilitzar diferents taules de traducció (per provar, és suficient la estàndar i la mitocondrial; si tens temps la bacterial també).
Per cada taula de traducció mostra els codons d’inici i parada.

### Projecte Full Stack

Ara que ja has conseguit que funcioni la web en `React` i la API en `FastAPI`, tracta que funcionin 

### Solució (parcial)

Tens una solució parcial dels exercicis integrada al projecte:

- <https://gitlab.com/xtec/bio/sequence>
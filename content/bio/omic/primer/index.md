---
title: Primer (PCR)
layout: default.njk
---

[https://gitlab.com/xtec/bio-primer](https://gitlab.com/xtec/bio-primer)

## Introducción

La idea de la reacción en cadena de la polimerasa, PCR (por sus siglas en inglés Polymerase Chain Reaction) fue desarrollada en los años 80 por Kary Mullis. Con esta técnica, revolucionó la biología molecular y por ello
obtuvo el premio Nobel.

La PCR es una técnica in vitro, utilizada para amplificar enzimáticamente una región específica de ácido
desoxirribonucleico (ADN). La región de ADN de interés se limita por dos iniciadores ("primers"), que son fragmentos cortos de ADN. Esta reacción puede iniciar a partir de una sola cadena de ADN, que se amplifica hasta obtener millones de copias del original en unas pocas horas.

Para mejorar la especificidad de la reacción, se deben seleccionar cuidadosamente los iniciadores y de esta
manera disminuir el riesgo de formación de dímeros y de productos inespecíficos que alterarían el resultado.

## PCR

La PCR utiliza altas temperaturas repetidamente para desnaturalizar el molde de ADN o separar sus cadenas.

La ADN polimerasa que normalmente se utiliza en la PCR se llama **Taq polimerasa**, por la bacteria tolerante al calor de la que se aisló (**T**hermus **aq**uaticus).

T. aquaticus vive en aguas termales y fuentes hidrotermales. Su ADN polimerasa es muy termoestable y su mayor actividad se presenta cerca de los 70º C (temperatura a la que la ADN polimerasa de ser humano o de E. coli no funcionaría). La Taq polimerasa es ideal para la PCR gracias a esta estabilidad térmica.

### Primer

Al igual que otras ADN polimerasas, la Taq polimerasa solo puede hacer ADN si hay un **cebador** ("primer"), una corta secuencia de nucleótidos que proporciona un punto de partida para la síntesis de ADN. En una reacción de PCR, la región de ADN que será copiada, o amplificada, se determina por los cebadores que se elijan.

Los cebadores para PCR son pedazos cortos de ADN de cadena sencilla, generalmente de unos 20 nucleótidos de longitud. En cada reacción de PCR se utilizan dos cebadores que están diseñados para flanquear la región blanco (la región que debe ser copiada). Es decir, les agregan secuencias que harán que se unan a cadenas opuestas del molde de ADN solo en los extremos de la región a copiar. Los cebadores se unen al molde mediante complementariedad de bases.

{% image "./molde.png" %}

Cuando los cebadores se unen al molde, la polimerasa los extiende y la región que se encuentra entre ellos se copia.

{% image "./molde2.png" %}

Ambos cebadores, apuntan "hacia adentro" al unirse, es decir, en dirección 5' a 3' hacia la región a copiar. Al igual que otras ADN polimerasas, la Taq polimerasa solo puede sintetizar ADN en dirección 5' a 3'. Al extenderse los cebadores, la región que se encuentra entre ellos se copia.

{% image "./molde3.png" %}

### Pasos

Los ingredientes clave para una reacción de PCR son _Taq_ polimerasa, cebadores, ADN molde y nucleótidos (los bloques básicos del ADN). Los ingredientes se colocan en un tubo, junto con los cofactores que necesite la enzima, y se someten a ciclos repetidos de calentamiento y enfriamiento que permiten la síntesis del ADN.

Los pasos básicos son:

1. **Desnaturalización** (96 ºC): la reacción se calienta bastante para separar, o desnaturalizar, las cadenas de ADN. Esto proporciona los moldes de cadena sencilla para el siguiente paso.

2. **Templado** (55 - 65 ºC): la reacción se enfría para que los cebadores puedan unirse a sus secuencias complementarias en el molde de ADN de cadena sencilla.

3. **Extensión** (72 ºC): la temperatura de la reacción se eleva para que la _Taq_ polimerasa extienda los cebadores y sintetice así nuevas cadenas de ADN.

{% image "./pasos.png" %}

Este ciclo se repite ‍25 - 35 veces en una reacción de PCR típica, que generalmente tarda ‍2 -4 horas, según la longitud de la región de ADN que se copia. Si la reacción es eficiente (funciona bien), puede producir miles de millones de copias a partir de una o unas cuantas copias de la región blanco.

Eso es porque no solo se usa el ADN original como molde en cada ciclo. En realidad, el nuevo ADN que se produce en una ronda puede servir como molde en la siguiente ronda de síntesis de ADN. Hay muchas copias de los cebadores y muchas moléculas de Taq polimerasa flotando en la reacción, por lo que el número de moléculas de ADN casi puede duplicarse en cada ciclo. La siguiente imagen muestra este patrón de crecimiento exponencial.

{% image "./rondas.png" %}

## Diseño de cebadores

La propiedad principal de los cebadores es que deben corresponder a secuencias de la molécula plantilla (deben ser complementarias a la cadena plantilla). Sin embargo, no es necesario que los cebadores se correspondan completamente con la cadena plantilla; lo único esencial es que el extremo 3' del cebador corresponda completamente a la cadena de ADN molde para que pueda continuar el alargamiento.

Generalmente se utiliza una guanina o citosina en el extremo 3', y el extremo 5' del cebador suele tener tramos de varios nucleótidos. Además, ambos extremos 3' de los cebadores hibridados deben apuntar uno hacia el otro.

El tamaño del primer también es muy importante. Los cebadores cortos se utilizan principalmente para amplificar un fragmento pequeño y simple de ADN. Por otro lado, se utiliza un cebador largo para amplificar una muestra de ADN genómico eucariota. Sin embargo, un cebador no debe ser demasiado largo (cebadores > 30 unidades) ni demasiado corto. Los cebadores cortos producen un producto de amplificación de ADN inexacto y no específico, y los cebadores largos dan como resultado una tasa de hibridación más lenta. En promedio, el fragmento de ADN que debe amplificarse debe tener un tamaño de entre 1 y 10 kB.

La estructura de la imprimación debe ser relativamente simple y no contener ninguna estructura secundaria interna para evitar el plegado interno. También es necesario evitar la hibridación cebador-cebador, que crea dímeros de cebador e interrumpe el proceso de amplificación. Al diseñar, si no está seguro de qué nucleótido colocar en una determinada posición dentro del cebador, se puede incluir más de un nucleótido en esa posición, lo que se denomina sitio mixto. También se puede utilizar un inserto molecular basado en nucleótidos (inosina) en lugar de un nucleótido normal para lograr capacidades de emparejamiento más amplias.

Teniendo en cuenta la información anterior, las imprimaciones generalmente deberían tener las siguientes propiedades:

    Longitud de 18-24 bases
    40-60% de contenido de G/C
    Comience y termine con 1-2 pares G/C
    Temperatura de fusión (Tm) de 50-60°C
    Los pares de cebadores deben tener una Tm con una diferencia de 5 °C entre sí.

    Los pares de cebadores no deben tener regiones complementarias.

    Nota: Si va a incluir un sitio de restricción en el extremo 5' de su cebador, tenga en cuenta que se debe agregar una "abrazadera" de 3 a 6 pares de bases en sentido ascendente para que la enzima se escinda eficientemente (por ejemplo, GCGGCG-sitio de restricción-su secuencia).

## Características de iniciadores

### Longitud del iniciador

Cada iniciador sentido y antisentido debe contar con una longitud de entre 18-24 bases, ya que la cantidad de bases influye en la especificidad de la secuencia a amplificar, siempre y cuando la temperatura de alineamiento sea la óptima. La longitud del iniciador influye en la eficiencia del alineamiento, cuanto más largo sea el iniciador, menos eficaz será la alineación. Al unirse una menor cantidad de iniciador a la secuencia de ADN molde en cada ciclo, se tiene como consecuencia la disminución de la cantidad del producto amplificado de forma significativa. Sin embargo, los iniciadores no deben ser demasiado cortos, ya que pueden llegar a unirse de forma inespecífica. Esto tiene como consecuencia, amplificados de productos no deseados y una disminución del producto de interés.

### Especificidad

Los iniciadores, deben ser específicos para delimitar la región que se quiere amplificar. La especificidad del iniciador depende al menos en parte de su longitud. Los iniciadores deben elegirse de forma que tengan una secuencia única dentro del ADN a amplificar. Como la ADN-polimerasa puede activarse a diferentes temperaturas, la extensión del iniciador se produce a una temperatura inferior a la de alineamiento. Si la temperatura es demasiado baja, puede darse un alineamiento inespecífico. Los mejores resultados se obtienen con una temperatura de fusión de entre 55 a 72ºC, que corresponde a una longitud del iniciador de entre 18 y 24 bases.

### Temperatura de fusión (Tf)

La Tf es la temperatura a la que la mitad de las hebras de ADN se encuentran como banda simple y la mitad como banda doble. Es deseable que los iniciadores tengan Tf similares o muy próximas, con 5 oC de diferencia como máximo. Si los iniciadores no tienen Tf semejantes, la amplificación es menos eficaz e incluso puede no llevarse a cabo. El iniciador con la Tf mayor, funciona mal a temperaturas más bajas y el iniciador con la Tf más baja, no se une a temperaturas más elevadas. Para evitar estos problemas durante la reacción, se analizan los iniciadores por medio de herramienta bioinformáticas que indican la Tf de cada uno de ellos. Otra forma de conocer la Tf es calculándola por medio de la fórmula Tf = 4(G+C) + 2 (A+T), esta fórmula da una buena aproximación del valor de Tf de cada iniciador y es válida para los iniciadores de entre 18 y 24 bases.

### Temperatura de alineamiento

Los iniciadores, deben contar con una temperatura de alineamiento de al menos 50 oC. La relación entre temperatura de alineamiento y temperatura de fusión es:

La temperatura de alineamiento debe ser inferior en 5 ºC a la temperatura de fusión (T<sub>alineamineto</sub> = Tf <sub>iniciador</sub> – 5 ºC).

Esta temperatura sirve de referencia, ya que es posible que la temperatura de alineamiento determinada empleando esta regla no sea la adecuada y se tengan que efectuar varios experimentos para determinar la temperatura óptima de la reacción.

La manera más sencilla de calcular la temperatura de alineamiento, es con un termociclador de gradiente, donde se prueba en una solo corrida varias temperaturas y así determinar la temperatura óptima de una reacción en particular. Una T<sub>alineamineto</sub> alta, evita la unión de los iniciadores y una T<sub>alineamineto</sub> baja, favorece la unión inespecífica de los iniciadores. Ésto tiene como consecuencia, la obtención de tamaños diversos de amplificados, que al final del proceso se observan en el gel de agarosa como bandas inespecíficas.

### Secuencias complementarias del iniciador

Se deben evitar regiones con capacidad para formar estructuras secundarias internas, como la formación de dímeros, complementariedad entre ellos o formación de horquillas. Es indispensable que los iniciadores no presenten secuencias con homología interna de más de 3 pares de bases. Si el iniciador tiene zonas de auto homología, se pueden formar estructuras en horquillas que interfieren con el alineamiento al ADN molde. Una homología parcial en las regiones centrales de los iniciadores también puede interferir con la alineación. Si la homología se produce en el extremo 3’ de cualquiera de los iniciadores, pueden formar dímeros de iniciadores, que en general impiden la formación del producto deseado por un mecanismo de competencia.

### Contenido de G/C y tramos de polipirimidina (T, C) o polipurina (A, G)

El contenido de G:C (Guanina:Citocina) debe estar en el rango de entre 40 y 55%. Entre más número de G y C tenga el iniciador, mayor será la temperatura de fusión (Tf). Por otro lado, también se deben evitar secuencias de poli X (X= G o C o T o A), la presencia de secuencias de poli G o poli C favorecen la hibridación inespecífica. Las secuencias de poli-A y poli-T pueden reducir la eficacia de la amplificación, así como de los tramos de polipirimidinas (T, C) y polipurinas (A, G). Lo ideal es que los iniciadores tenga una mezcla aleatoria de nucleótidos, un contenido de GC del 50 % y una longitud aproximada de 20 bases. De esta manera, la Tf estará entre 56 – 62 ºC.

### Secuencia en el extremo 3’

Un punto importante es la inclusión de un residuo de G o C en el extremo 3’ de los iniciadores. Extremos 3’ con G/C son adecuados debido a que los triples enlaces que forman estas bases favorece la eficacia de la reacción. Además, minimizan la posibilidad de que se abra la doble cadena formada entre el iniciador y el ADN a amplificar.

## Análisis de iniciadores

Un par de iniciadores que no se analizan adecuadamente pueden llevar a obtener poco producto, productos inespecíficos o incluso ningún producto, debido a una amplificación inespecífica y/o a la formación de dímeros de iniciadores que compiten durante la reacción.

Para realizar el análisis de iniciadores se utilizan herramientas bioinformáticas. Con estas herramientas bioinformáticas, podemos contrastar las secuencias de los iniciadores contra bases de datos y así caracterizarlos. Con los resultados obtenidos del análisis, determinamos si los iniciadores analizados son adecuados o no, para realizar la reacción de PCR en el laboratorio.

Dos de las herramientas más utilizadas, [BLAST](blast) y [Primer-BLAST](https://www.ncbi.nlm.nih.gov/tools/primer-blast/), estas herramientas no son excluyentes, se complementan para el análisis de los iniciadores.

### BLAST

Como ejemplo, se analiza un par de iniciadores para el gen gliceraldehído-3-fosfato deshidrogenasa (gadph) de _Ovis aries musimon_ [Muflón común](https://es.wikipedia.org/wiki/Ovis_orientalis_musimon).

A continuación tienes un ejemplo de secuencia de iniciador sentido e iniciador antisentido para el gen de gadph de _Ovis aries musiman_:

Iniciador sentido      `5'-CCACTGGGGTCTTCACTACC-3'`
Iniciador antisentido  `5'-AAGCAGGGATGATGTTCTGG-3'`

Ves a [Nucelotide BLAST](https://blast.ncbi.nlm.nih.gov/Blast.cgi?PROGRAM=blastn&PAGE_TYPE=BlastSearch&LINK_LOC=blasthome) del NCBI.

Se transcribe la secuencia del iniciador sentido, en el sentido 5' a 3', señalamos la opción otros (Others) y utilizamos la base de datos de colección de
nucleótidos nr/nt (Nucleotide collection nr/nt). Damos clic en la opción BLAST.

{% image "blast.png" %}

En este momento, la secuencia se compara contra la base de datos nr/
nt. Se despliegan los resultados con las secuencias que BLAST reporta como similares (o idénticas) al iniciador que se analiza.

{% image "blast-descriptions.png" %}

En el ejemplo, es evidente que para la secuencia que se analizó, BLAST encontró varias secuencias similares. Esto significa que en varias especies se presenta una secuencia homóloga al iniciador sentido, está secuencia puede pertenecer a la misma familia de proteínas que se esta analizando, para ver la especificidad de la especie de interés se analizan los valores que arroja el análisis.

La tercera sección, cor responde a las descripciones de los alineamientos:

{% image "blast-alignments.png" %}

Ésta es una lista de las secuencias encontradas (ordenadas de acuerdo a su valor _E_), cada uno de los alineamientos es evaluado para determinar su significancia estadística. Los alineamientos resultantes, se llaman pares de alta puntuación _High Score Pairs_ o _HSPs_ y el valor de E ó e-valor (_e-value_) de corte, permite definir, qué alineamiento es conveniente de acuerdo a su significancia estadística, cuanto menor sea el valor de _E_ más significativo es un alineamiento. El valor de _E_, depende de la base de datos empleada y de la longitud de la secuencia del iniciador. Además, se despliega una serie de datos como la(s) especie(s) con las que coincide la secuencia del iniciador sentido y la referencia de la secuencia. Se busca entre las especies la de interés y se comprueba que efectivamente sea el gen de interés:

**TODO**. En nuestra búsqueda no encontramos al ovis aries musiman!

{% image "blast-ovis-aries.png" %}

Es importante verificar que la referencia del gen analizado sea la misma que se reportó en el artículo de referencia de la secuencia. Si la secuencia del iniciador sentido fue diseñada, se tendrá que verificar que el número de referencia del iniciador sentido reportado en _PubMed_, coincida con la del iniciador antisentido.

Para el análisis del iniciador antisentido, se toma la secuencia de dicho iniciador, se elabora su secuencia complementaria y posteriormente se transcribe la secuencia complementaria de derecha a izquierda.

**TODO** Hacer con biopython.

Iniciador sentido      `5'-CCACTGGGGTCTTCACTACC-3'`
Iniciador antisentido  `5'-AAGCAGGGATGATGTTCTGG-3'`


## Bibliografía

* [Primer-Blast](https://www.ncbi.nlm.nih.gov/tools/primer-blast/)
* [Primer Design](https://www.addgene.org/protocols/primer-design/)
* [Análisis de iniciadores](https://drive.google.com/file/d/1FvCixnrebtUeXcESdzgeYumHBdOLhYhj/view?usp=sharing)
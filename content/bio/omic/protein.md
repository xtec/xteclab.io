---
title: Proteines
description: Les proteïnes són una de les molècules orgàniques més abundants en els sistemes vius i són molt més diverses en estructura i funció que altres classes de macromolècules. Totes les proteïnes es componen d'una o més cadenes d'aminoàcids.
---

## Índex:

- [Google Docs - Proteines](https://docs.google.com/document/d/1qWPkHGEHht9on0ta0vKyrWG6zqEyAYsaysA0ut7IclE/edit?usp=sharing)
- [Protein Data Bank](https://xtec.dev/bio/protein/protein-data-bank/)
- [Uniprot](https://xtec.dev/bio/protein/uniprot/)

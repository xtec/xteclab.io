---
title: Proteines
description: Les proteïnes són una de les molècules orgàniques més abundants en els sistemes vius i són molt més diverses en estructura i funció que altres classes de macromolècules. Totes les proteïnes es componen d'una o més cadenes d'aminoàcids.
---

{% pages ["protein/", "uniprot/", "protein-data-bank/"] %}

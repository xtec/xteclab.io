---
title: Atom
description: Un àtom és la unitat més petita de matèria que conserva totes les propietats químiques dun element.
---

## Matèria, elements i àtoms

Els àtoms i molècules segueixen les regles de la química i la física tot i que formen part d'un organisme. Alguns àtoms tendeixen a guanyar o perdre electrons, o formar enllaços entre ells, i això segueix sent cert fins i tot quan els àtoms o molècules formen part d'un organisme viu. **De fet, les interaccions senzilles entre àtoms** – dutes a terme moltes vegades i en moltes combinacions diferents en una cèl·lula individual o en un organisme més gran – **són les que fan possible la vida**.

### Matèria i elements

El terme **matèria** es refereix a qualsevol cosa que ocupi espai i tingui massa, en altres paraules “allò” del que està fet l'univers. Tota la matèria està composta de substàncies anomenades elements, que tenen propietats físiques i químiques específiques i que no es poden dividir en altres substàncies per mitjà de reaccions químiques ordinàries. L'or, per exemple, és un element, igual que el carboni. Hi ha 118 elements però només 92 d'ells ocorren de manera natural. La resta dels elements han estat creats a laboratoris i són inestables.

Cada element es denota amb el símbol químic, que pot ser una sola lletra majúscula o, quan la primera lletra ja està "ocupada" per un altre element, una combinació de dues lletres. Alguns elements usen el nom en espanyol, com C per al carboni o Ca per al calci. Altres símbols químics provenen dels seus noms en llatí, per exemple, el símbol del sodi és Na, una forma abreujada de *natrium*, la paraula en llatí per a sodi.

**Els quatre elements comuns a tots els organismes vius són l'oxigen (O), el carboni (C), l'hidrogen (H) i el nitrogen (N), que en conjunt formen al voltant del 96% del cos humà**. Al món no viu, els elements es troben en proporcions diferents i alguns elements que són comuns en els organismes vius són relativament rars a la Terra. Tots els elements i les reaccions químiques entre aquests obeeixen les mateixes lleis físiques i químiques, sense importar si formen part d'organismes vius o no.

### L'estructura de l'àtom

Un **àtom** és la unitat més petita de matèria que conserva totes les propietats químiques dun element. Per exemple, una moneda d'or és simplement un gran nombre d'àtoms d'or modelat amb la forma d'una moneda (amb quantitats petites d'altres elements contaminants). Els àtoms d'or no es poden dividir en una mica més petit i conservar-ne les característiques. Un àtom d'or obté les seves propietats de les partícules subatòmiques diminutes de les quals es compon.

Un àtom està compost de dues regions. 

1. La primera és el petit **nucli atòmic**, que es troba al centre de l'àtom i conté partícules carregades positivament anomenades **protons**, i partícules neutres, sense càrrega, anomenades **neutrons**. 

2. La segona, que és molt més gran, és un "núvol" d'**electrons**, partícules de càrrega negativa que orbiten al voltant del nucli. L'atracció entre els protons de càrrega positiva i els electrons de càrrega negativa és allò que manté unit a l'àtom.  

{% image "atom.png" %}

La majoria dels àtoms tenen aquests tres tipus de **partícules subatòmiques**, protons, electrons i neutrons. L'hidrogen (H) és una excepció perquè generalment té un protó i un electró, però no té neutrons. 

El nombre de protons al nucli defineix quin element és l'àtom, mentre que el nombre d'electrons que envolta el nucli determina en quin tipus de reaccions pot participar.

Els protons i neutrons no tenen la mateixa càrrega, però sí que tenen aproximadament la mateixa massa, al voltant de 1.67×10−24 grams. Atès que els grams no són una unitat de mesura molt convenient per mesurar masses tan petites, els científics van decidir definir una mesura alternativa, el **dalton** o **unitat de massa atòmica** (uma). Un únic protó o neutró té un pes molt proper a 1 uma. Els electrons són molt més petits en massa que els protons, tan sols 1/1800 d'una unitat de massa atòmica, així que no contribueixen gaire a la massa atòmica total de l'element. Per contra, els electrons tenen un gran efecte a la càrrega de l'àtom, ja que cada electró té una càrrega negativa igual a la càrrega positiva d'un protó. En àtoms neutres, sense càrrega, el nombre delectrons que orbiten el nucli és igual al nombre de protons dins del nucli. Les càrregues positives i negatives es cancel·len i generen un àtom sense càrrega neta.

Els protons, els neutrons i els electrons són molt petits i la major part del volum d'un àtom — més del 99 per cent — és en realitat espai buit. Amb tant d'espai buit podries preguntar-te com és que els anomenats objectes sòlids no passen els uns a través dels altres. La resposta és que els núvols d'electrons de càrrega negativa dels àtoms es repel·lirien entre ells si s'aproximessin massa, cosa que dóna com a resultat la nostra percepció de la solidesa.

## Enllaços químics

Els enllaços químics mantenen unides a les molècules i creen connexions temporals que són essencials per a la vida. Els tipus d'enllaç químic que sinclouen són covalent, iònic, així com enllaços dhidrogen i forces de dispersió de London.

Els éssers vius es componen d'àtoms, però en la majoria dels casos, aquests àtoms no estan flotant individualment. Per contra, generalment interactuen amb altres àtoms (o grups d'àtoms).
Com a exemple, els àtoms podrien estar connectats per enllaços forts i organitzats en molècules o vidres; o podrien formar enllaços temporals i febles amb altres àtoms amb què xoquen o freguen. Tant els enllaços forts que mantenen unides a les molècules com els enllaços més febles que creen connexions temporals són essencials per a la química dels nostres cossos i l'existència de la vida mateixa.

Per què formar enllaços químics? La resposta fonamental és que els àtoms intenten assolir l'estat més estable (de menor energia) possible. Molts àtoms esdevenen estables quan el seu **orbital de valència** és ple d'electrons o quan satisfan la **regla de l'octet** (en tenir vuit electrons de valència). Si els àtoms no tenen aquest arranjament, "desitjaran" aconseguir-ho en guanyar, perdre o compartir electrons mitjançant els enllaços.

En aquest apartat veurem amb més detall la taula periòdica, com els àtoms organitzen els electrons i com això ens permet predir la reactivitat dels elements.

### La taula periòdica

La posició de cada element a la taula periòdica brinda una informació important sobre la seva estructura, propietats i comportament en les reaccions químiques. Específicament, la posició d'un element a la taula periòdica ajuda a conèixer la seva configuració electrònica, la manera com s'organitzen els electrons al voltant del nucli. Els àtoms usen els seus electrons per participar en reaccions químiques, així que conèixer la configuració electrònica d'un element et permet predir-ne la reactivitat, és a dir, si interactuarà, i de quina manera, amb àtoms d'altres elements.

Per convenció, els elements estan organitzats a la taula periòdica, una estructura que captura els patrons importants del seu comportament. Dissenyada pel químic rus Dmitri Mendeleev (1834–1907) el 1869, la taula organitza els elements en columnes — **grups** — i files — **períodes** — que comparteixen certes propietats. Aquestes propietats determinen l'estat físic d'un element a temperatura ambient – gas, sòlid o líquid –, així com la seva **reactivitat química**, la capacitat de formar enllaços químics amb altres àtoms.

A més de listar el nombre atòmic de cada element, la taula periòdica també mostra la massa atòmica relativa de l'element, la mitjana ponderada dels seus isòtops que tenen lloc naturalment a la Terra.

{% image "taula-periodica.png" %}

*Si veiem l'hidrogen, per exemple, apareixen el nom i el símbol H, així com el nombre atòmic d'1 — a la cantonada superior esquerra — i la massa atòmica relativa de 1.01.*

Les diferències en la reactivitat química entre els elements es basen en el nombre i la distribució espacial dels seus electrons. Si dos àtoms tenen patrons delectrons complementaris, poden reaccionar i formar un enllaç químic, la qual cosa crea una molècula o compost. Com veurem a continuació, la taula periòdica organitza els elements de manera que reflecteixin el nombre i el patró d'electrons, cosa que la fa útil per predir la reactivitat d'un element: què tan probable és que formi enllaços i amb quins altres elements.

### Les capes delectrons i el model de Bohr

El científic danès Niels Bohr (1885-1962) va desenvolupar un primer model de l'àtom el 1913. El model de Bohr mostra l'àtom com un nucli central compost per protons i neutrons, amb els electrons en capes circulars a distàncies específiques del nucli, de manera semblant als planetes que orbiten al voltant del Sol. Cada capa d'electrons té un nivell d'energia diferent, les més properes al nucli són de menor energia que les més llunyanes. Per convenció, a cada capa se li assigna un número i el símbol n: la capa d'electrons més propera al nucli, per exemple, s'anomena 1n. Per moure's entre capes, un electró ha d'absorbir o alliberar una quantitat d'energia que correspongui exactament a la diferència d'energia que hi ha entre les capes. Per exemple, si un electró absorbeix energia d'un fotó, es pot excitar i moure's a una capa de més energia; per contra, quan un electró torna a una capa de menor nivell energètic, allibera energia, sovint en forma de calor.

{% image "bohr-model.png" %}

Els àtoms, com altres coses governades per les lleis de la física, tendeixen a prendre la configuració més estable i de menor energia possible. Així, les capes d'electrons d'un àtom s'omplen cap enfora, on els electrons omplen les capes de menor energia més properes al nucli abans de moure's cap a les capes exteriors de més energia. La capa més propera al nucli, 1n, pot contenir dos electrons; la segona, 2n, pot contenir vuit, i la tercera, 3n, fins a divuit electrons.

El nombre delectrons de la capa externa dun àtom particular determina la seva reactivitat o tendència a formar enllaços químics amb altres àtoms. A aquesta capa externa se'l coneix com a **capa de valència** i als electrons que es troben dins d'ella se'ls anomena electrons de valència. En general, els àtoms són més estables, menys reactius, quan la seva capa delectrons externa es troba completa. 

La majoria dels elements importants en la biologia necessiten vuit electrons a la seva capa externa per ser estables i aquesta regla es coneix com a **regla de l'octet**. Alguns àtoms poden ser estables amb un octet fins i tot quan la seua capa de valència és la capa 3n que pot contenir fins a 18 electrons. Veurem per què passa això quan expliquem els orbitals atòmics més endavant.

A continuació es mostren exemples d'alguns àtoms neutres i les configuracions electròniques. En aquesta taula, pots veure que l'heli té una capa de valència completa, amb dos electrons a la primera i única capa, 1n. De manera similar, el neó té una capa externa 2n completa amb vuit electrons. Aquestes configuracions electròniques fan que l'heli i el neó siguin molt estables. Encara que l'argó tècnicament no té una capa de valència completa, ja que la capa 3n pot contenir fins a divuit electrons, és estable com el neó i l'heli perquè té vuit electrons a la seva capa 3n i per tant compleix amb la regla de l'octet. En contrast, el clor té únicament set electrons a la seva capa més externa, mentre que el sodi només en té un. Aquests patrons no omplen la capa exterior ni compleixen amb la regla de l'octet, cosa que fa que el clor i el sodi siguin reactius, àvids per guanyar o perdre electrons per assolir una configuració més estable.

{% image "bohr-model-elements.png" %}

### Configuració electrònica i la taula periòdica

Els elements a la taula periòdica s'ordenen d'acord amb el nombre atòmic, quants protons tenen. En un àtom neutre, el nombre delectrons serà igual al nombre de protons, de manera que podem determinar fàcilment el nombre delectrons a partir del nombre atòmic. Addicionalment, la posició d'un element a la taula periòdica — la columna o el grup, i la fila o el període — proporciona informació útil sobre com estan disposats els seus electrons.

Si considerem només les primeres tres files de la taula, que inclouen els principals elements importants per a la vida, cada fila correspon a omplir una capa d'electrons diferent: l'heli i l'hidrogen col·loquen els electrons a la capa 1n, mentre que els elements de la segona fila com el Li comencen a omplir la capa 2n i els elements de la tercera fila com el Na continuen amb la capa 3n. De manera similar, el número de columna d'un element ens dóna informació sobre el nombre d'electrons de valència i la reactivitat. En general, el nombre d'electrons de valència és el mateix dins una columna i augmenta d'esquerra a dreta dins una fila. Els elements del grup 1 tenen només un electró de valència i els del grup 18 en tenen vuit, excepte l'heli, que només té dos electrons en total. D'aquesta manera, el número de grup pot predir què tan reactiu serà cada element:

* L'heli (He), el neó (Ne) i l'argó (Ar), com a elements del grup 18, tenen la capa externa completa o satisfan la regla de l'octet. Això els fa molt estables com a àtoms individuals. A causa de la seva manca de reactivitat són anomenats **gasos inerts** o gasos nobles**.

* L'hidrogen (H), el liti (Li) i el sodi (Na), com a elements del grup 1, tenen només un electró a la capa exterior. Són inestables com a àtoms individuals però poden estabilitzar-se en perdre o compartir un electró de valència. Si aquests elements perden completament un electró — com fan normalment el Li i el Na — es converteixen en ions de càrrega positiva: Li<sup>+</sup> i Na<sup>+</sup>.

* El fluor (F) i el clor (Cl), com a elements del grup 17, tenen set electrons a la capa exterior. Tendeixen a assolir un octet estable en prendre un electró d'altres àtoms i es converteixen en ions amb càrrega negativa: F<sup>−</sup> i Cl<sup>−</sup>.

*  El carboni (C), com un element del grup 14, té quatre electrons a la capa exterior. Generalment, el carboni comparteix electrons per obtenir una capa de valència completa, i així forma enllaços amb molts altres àtoms.

Aleshores, les columnes de la taula periòdica reflecteixen el nombre d'electrons que es troben a la capa de valència de cada element, la qual cosa al seu torn determina com reaccionarà.

### Subcapes i orbitals

El model de Bohr és útil per explicar la reactivitat i la formació d'enllaços de molts elements però, en realitat, no dóna una descripció gaire precisa de com estan distribuïts els electrons a l'espai al voltant del nucli. Específicament, els electrons no circumden el nucli, sinó que passen la major part del seu temps a regions de l'espai que de vegades tenen formes complicades al voltant del nucli anomenades **orbitals electrònics**. Realment no podem saber on és un electró en qualsevol moment donat, però podem determinar matemàticament el volum d'espai en què és més probable trobar-lo, diguem-ne, el volum d'espai on passa el 90% del seu temps. Aquesta regió d'alta probabilitat és allò que conforma un orbital i cada orbital pot contenir fins a dos electrons.

Així que, com encaixen aquests orbitals definits matemàticament amb les capes delectrons del model de Bohr? Podem dividir cada capa delectrons en una o més subcapes, que simplement són conjunts dun o més orbitals. Les subcapes es designen amb les lletres *s*, *p*, *d* i *f*, i cadascuna indica una manera diferent. Per exemple, les subcapes *s* tenen un únic orbital esfèric, mentre que les *p* tenen tres orbitals en forma de manuella amb angles rectes entre ells. La major part de la química orgànica, la química dels compostos que contenen carboni i que són fonamentals per a la biologia, tracta sobre interaccions entre electrons de les capes *s* i *p*, així que aquestes són les capes amb què cal familiaritzar-se. No obstant això, els àtoms amb molts electrons poden distribuir-ne alguns a les subcapes *d* i *f*. Les subcapes *d* i **f tenen formes més complexes i contenen cinc i set orbitals, respectivament.

{% image "orbital.png" %}

*Diagrama tridimensional dels orbitals circulars 1s i 2s i els orbitals 2p amb forma de manuella. Hi ha tres orbitals 2p, i tenen angles rectes entre si.*

La primera capa delectrons, 1n, correspon a un sol orbital 1s. L'orbital 1s és el més proper al nucli i és el primer a omplir-se amb electrons abans que qualsevol altre orbital. L'hidrogen té només un electró, així que només té un lloc ocupat en el seu orbital 1s. Això es pot escriure en una forma abreujada anomenada **configuració electrònica** com a 1s<sup>1</sup>, on el superíndex 1 es refereix a l'únic electró de l'orbital 1s. L'heli té dos electrons, així que podeu completar l'orbital 1s amb els seus dos electrons. Això s'escriu 1s<sup>2</sup>, i es refereix als dos electrons de l'heli a l'orbital 1s. A la taula periòdica, l'hidrogen i l'heli són els únics dos elements a la primera fila, o període, cosa que reflecteix que només tenen electrons a la seva primera capa. L'hidrogen i l'heli són els únics dos elements que tenen electrons exclusivament a l'orbital 1s1s en el seu estat neutre, sense càrrega.

La segona capa d'electrons, 2n, conté un altre orbital esfèric més tres orbitals *p* en forma de manuelles, cadascun dels quals pot tenir dos electrons. Quan l'orbital 1s està complet, es comença a omplir la segona capa d'electrons, en què els electrons entren primer a l'orbital 2s i després omplen els tres orbitals p. Els elements a la segona fila de la taula periòdica distribueixen els seus electrons a les capes 2n i 1n. Per exemple, el liti (Li) té tres electrons: dos omplen l'orbital 1s i el tercer es col·loca a l'orbital 2s, cosa que dóna una configuració electrònica de 1s<sup>2</sup> 2s<sup>1</sup>. En canvi, el neó (Ne) té un total de deu electrons: dos a l'orbital 1s més intern i vuit que omplen la seva segona capa (dos a l'orbital 2s<sup>2</sup> i dos a cadascun dels tres orbitals *p*, 1s<sup>2</sup> 2s<sup>2</sup> 2p<sup>6</sup> ). Com que la seva capa 2n està completa, és energèticament estable com a àtom individual i poques vegades formarà enllaços químics amb altres àtoms.

La tercera capa d'electrons, 3n, també té un orbital *s* i tres orbitals *p*, i els elements de la tercera fila de la taula periòdica distribueixen els seus electrons en aquests orbitals de la mateixa manera que els elements de la segona fila ho fan amb la seva capa 2n . La capa 3n també té un orbital *d*, però aquest orbital té una energia considerablement més gran que els orbitals 3s i 3p, i no comença a omplir-se sinó fins a la quarta fila de la taula periòdica. Aquesta és la raó per la qual els elements de la tercera fila, com l'argó, poden ser estables amb només vuit electrons de valència: les seues subcapes *s* i *p* estan completes encara que la seua capa 3n no estiga totalment plena.

Tot i que les capes delectrons i els orbitals estan estretament relacionats, els orbitals proporcionen una imatge més precisa de la configuració electrònica d'un àtom. Això és perquè els orbitals realment especifiquen la forma i la posició de les regions de l'espai que ocupen els electrons.

### Activitat 

Digues quines són les propietats dels 4 àtoms més comuns en els organismes vius.

{% sol %}
Segur que pots tu sól! I és una bona pregunta d'examen 🤔 ...
{% endsol %}

## TODO

Incorporar: [Atoms, Molecules, and Ions](https://2012books.lardbucket.org/books/beginning-chemistry/s07-atoms-molecules-and-ions.html)
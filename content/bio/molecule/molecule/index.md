---
title: Molécules
description: Els quatre elements comuns a tots els organismes vius són l'oxigen (O), el carboni (C), l'hidrogen (H) i el nitrogen (N), que en conjunt formen al voltant del 96% del cos humà. 
---

## Introducció

Els àtoms i molècules segueixen les regles de la química i la física tot i que formen part d'un organisme. Alguns àtoms tendeixen a guanyar o perdre electrons, o formar enllaços entre ells, i això segueix sent cert fins i tot quan els àtoms o molècules formen part d'un organisme viu. **De fet, les interaccions senzilles entre àtoms** – dutes a terme moltes vegades i en moltes combinacions diferents en una cèl·lula individual o en un organisme més gran – **són les que fan possible la vida**.

## Tipus d'enllaços

### Els ions i els enllaços iònics

Alguns àtoms es tornen més estables en guanyar o perdre un electró complet (o diversos electrons). Quan ho fan, els àtoms formen **ions**, o partícules carregades. El guanyar o perdre electrons pot donar a un àtom una capa electrònica externa plena i fer que sigui energèticament més estable.

#### La formació d'ions

Els ions poden ser de dos tipus:

1. Els cations són **ions positius** que es formen en perdre electrons. Per exemple, un àtom de sodi perd un electró per convertir-se en un catió sodi, Na<sup>+</sup>. 

2. Els **ions negatius** es formen en guanyar electrons i s'anomenen anions. Els anions reben noms que acaben en "-uro"; per exemple, l'anió del clor (Cl<sup>−</sup>) s'anomena clorur.

Quan un àtom perd un electró i un altre àtom guanya un electró, el procés es coneix com a transferència d'electrons. Els àtoms de sodi i de clor són un bon exemple de **transferència delectrons**.

El sodi (Na) només té un electró a la seva capa electrònica externa, per la qual cosa és més fàcil (més electrònicament estable) que el sodi doni aquest electró a trobar set electrons més per omplir la seva capa externa. A causa d'això, el sodi tendeix a perdre el seu únic electró i formar Na<sup>+</sup>.

D'altra banda, el clor (Cl) té set electrons a la seva capa externa. En aquest cas, és més fàcil per al clor guanyar un electró que perdre'n set, llavors tendeix a prendre un electró i contertir-se a Cl<sup>−</sup>

{% image "ion.png" %}

*El sodi transfereix un dels seus electrons de valència al clor, cosa que resulta en la formació d'un ió sodi (que no té electrons a la tercera capa, cosa que significa que la seva segona capa està completa) i un ió clorur (amb vuit electrons a la seva tercera capa, cosa que li dóna un octet estable).*

Quan es combinen el sodi i el clor, el sodi donarà el seu electró per buidar-ne la capa més externa, i el clor acceptarà aquest electró per omplir la seva. Ara tots dos ions satisfan la regla de l'octet i tenen capes externes completes. Com que el nombre d'electrons ja no és igual al nombre de protons, cada àtom s'ha convertit en un ió i té una càrrega +1 (Na<sup>+</sup>) o –1 (Cl<sup>−</sup>).

En general, un àtom ha de perdre un electró alhora que un altre àtom guanya un electró: perquè un àtom de sodi perdi un electró, necessita tenir un receptor adequat com un àtom de clor.

#### La formació d'un enllaç iònic

Els **enllaços iònics** són enllaços que es formen entre ions amb càrregues oposades. Per exemple, els ions sodi carregats positivament i els ions clorur carregats negativament s'atreuen entre si per formar clorur de sodi o sal de taula. La sal de taula, igual que molts compostos iònics, no es compon només d'un ió sodi i un ió de clorur; per contra, conté molts ions acomodats en un patró tridimensional predictible i repetit (un vidre).

A la fisiologia, certs ions es coneixen com **electròlits** (com sodi, potassi i calci). Aquests ions són necessaris per conduir impulsos nerviosos, contracció muscular i equilibri d'aigua. Moltes begudes esportives i suplements dietètics proporcionen ions per reposar els que es perden durant l'exercici per la sudoració.

### Enllaços covalents

Una altra manera com els àtoms es tornen més estables és en compartir electrons (en lloc de guanyar-los o perdre'ls completament), formant així **enllaços covalents**. Aquests enllaços són més comuns que els enllaços iònics a les molècules dels organismes vius.

Per exemple, els enllaços iònics són clau per a l'estructura de les molècules orgàniques basades en el carboni, com el nostre ADN i les proteïnes. També hi ha enllaços covalents en molècules inorgàniques més petites, com ara H<sub>2</sub>O, CO<sub>2</sub>, i O<sub>2</sub>. Es poden compartir un, dos o tres parells d'electrons, cosa que resulta en enllaços simples, dobles o triples, respectivament. Com més electrons comparteixin dos àtoms, més fort serà l'enllaç. Com a exemple denllaç covalent, examinem laigua. Una sola molècula d‟aigua, H<sub>2</sub>O, està composta de dos àtoms d‟hidrogen units a un àtom d‟oxigen. Cada hidrogen comparteix un electró amb l'oxigen i l'oxigen comparteix un dels seus electrons amb cada hidrogen:

{% image "ionic-link.png" %}

*Àtoms d'hidrogen que comparteixen electrons amb un àtom d'oxigen per formar enllaços covalents en crear una molècula d'aigua.*

Els electrons compartits divideixen el seu temps entre les capes de valència dels àtoms d'hidrogen i oxigen, i donen a cada àtom una cosa que s'assembla a una capa de valència completa (dos electrons per a l'H, i vuit per a l'O). Això fa que una molècula daigua sigui molt més estable del que serien els àtoms que la componen per si sols.

#### Enllaços covalents polars

Hi ha dos tipus principals denllaços covalents: polar i no polar.

En un **enllaç covalent polar**, els electrons es comparteixen de manera no equitativa entre els àtoms i passen més temps a prop d'un àtom que de l'altre. A causa de la distribució desigual d'electrons entre els àtoms de diferents elements, apareixen càrregues lleugerament positives (δ<sup>+</sup>) i lleugerament negatives (δ<sup>–</sup>) a diferents parts de la molècula.

{% image "molecule-water.png" %}

En una molècula d'aigua, l'enllaç que uneix l'oxigen amb cada hidrogen és un enllaç polar. L'oxigen és un àtom molt més electronegatiu que l'hidrogen, per això l'oxigen de l'aigua té una càrrega parcialment negativa (té una densitat d'electrons alta), mentre que els hidrògens porten càrregues parcialment positives (tenen una densitat electrònica baixa).

En general, l'electronegativitat relativa dels dos àtoms en un enllaç, és a dir, la seva tendència a acaparar els electrons compartits determinarà si l'enllaç és polar o no polar. Sempre que un element sigui significativament més electronegatiu que un altre, l'enllaç entre ells serà polar; això vol dir que un dels seus extrems tindrà una càrrega lleugerament positiva i l'altre una càrrega lleugerament negativa.

#### Enllaços covalents no polars

Els enllaços covalents no polars es formen entre dos àtoms del mateix element o entre àtoms de diferents elements que comparteixen electrons de manera més o menys equitativa. Per exemple, l'oxigen molecular (O<sub>2</sub>) no és polar perquè els electrons es comparteixen equitativament entre els dos àtoms d'oxigen.

Un altre exemple d'enllaç covalent no polar es pot trobar al metà (CH<sub>4</sub>). El carboni té quatre electrons a la capa exterior i en requereix quatre més per tornar-se un octet estable. Els aconsegueix en compartir electrons amb quatre àtoms d'hidrogen, cadascun dels quals proveeix d'un electró. De la mateixa manera, els àtoms d'hidrogen necessiten un electró addicional cadascun per omplir-ne la capa més externa, els quals reben en forma d'electrons compartits del carboni. Encara que el carboni i l'hidrogen no tenen exactament la mateixa electronegativitat, són força similars, així que els enllaços carboni-hidrogen es consideren no polars.

{% image "covalent-link.png" %}

*Taula que mostra l'aigua i el metà com a exemples de molècules amb enllaços polars i no polars, respectivament*

### Enllaços d'hidrogen i forces de dispersió de London

Tant l'enllaç covalent com l'iònic es consideren enllaços forts. Tot i això, també es poden formar altres tipus d'enllaç més temporals entre àtoms o molècules. Dos tipus denllaços febles que es veuen amb freqüència en la biologia són els enllaços dhidrogen i les forces de dispersió de London.
Per exemple, els enllaços d'hidrogen proporcionen moltes de les propietats de l'aigua que sostenen la vida, i estabilitzen les estructures de les proteïnes i l'ADN, tots dos ingredients clau de les cèl·lules.

#### Enllaços d'hidrogen

En un enllaç covalent polar que conté hidrogen (per exemple, un enllaç `O-H` en una molècula daigua), lhidrogen tindrà una lleugera càrrega positiva perquè laltre element jala els electrons denllaç més fortament. A causa d'aquesta lleu càrrega positiva, l'hidrogen serà atret a qualsevol càrrega negativa veïna. Aquesta interacció s'anomena un **enllaç d'hidrogen**.

Els enllaços d'hidrogen són comuns, i les molècules d'aigua en particular en formen molts. Els enllaços dhidrogen individuals són febles i es trenquen fàcilment, però molts enllaços hidrogen junts poden ser molt forts.

#### Forces de dispersió de London

Com els enllaços d'hidrogen, les **forces de dispersió de London** són atraccions febles entre les molècules. Tot i això, a diferència dels enllaços de l'hidrogen, poden passar entre àtoms o molècules de qualsevol tipus i depenen de desequilibris temporals en la distribució d'electrons.

Com funciona això? Com que els electrons estan en constant moviment, hi haurà moments en què els electrons en un àtom o molècula estiguin agrupats junts, cosa que crea una càrrega parcialment negativa en una part de la molècula (i una càrrega parcialment positiva en una altra). Si una molècula amb aquest tipus de desequilibri de càrregues està molt a prop d'una altra molècula, pot causar una redistribució de càrregues similar a la segona molècula, i les càrregues positives i negatives temporals de les dues molècules s'atrauran entre si.

Els enllaços d'hidrogen i les forces de dispersió de London són exemples de **forces de van der Waals**, un terme general per a les interaccions intermoleculars que no impliquen enllaços covalents ni ions.

###  Com funciona això en una cèl·lula?

Tant els enllaços forts com els febles tenen papers clau en la química de les nostres cèl·lules i cossos. Per exemple, els components fonamentals químics que conformen un fil d'ADN estan units per enllaços covalents forts. Tot i això, els dos fils de la doble hèlix d'ADN estan units per enllaços d'hidrogen més febles. Aquests enllaços febles mantenen estable a l'ADN, però també permeten que s'obri per ser copiat i utilitzat per la cèl·lula.

De manera més general, els enllaços entre els ions, les molècules d'aigua i les molècules polars, contínuament s'estan formant i trencant en l'ambient aquós d'una cèl·lules. En aquest mitjà, les molècules de diferents tipus poden interactuar entre si mitjançant atraccions febles basades en les càrregues. Per exemple, l'ió Na<sup>+</sup> podria interactuar amb una molècula d'aigua en un moment, i amb la part negativament carregada d'una proteïna al següent.

El que és realment sorprenent és pensar que milers de milions d'aquestes interaccions d'enllaços químics, forts i febles, estables i temporals, estan passant en els nostres cossos ara mateix, i ens mantenen units com un tot i funcionant!

## Dibuixant molecules

Les molécules tenen una estructura tridimensional i es poden representar en 3D.

{% panel "Ehtanol" %}
<iframe style="width: 500px; height: 300px;" frameborder="0" src="https://embed.molview.org/v1/?mode=balls&cid=702"></iframe>
{% endpanel %}

Per exemple, a PubChem pots veure l'alcohol etílic ( o etanol) en 3D: [Ethanol](https://pubchem.ncbi.nlm.nih.gov/compound/Ethanol#section=3D-Conformer)

També pots veure una representacó de la molécula a Molview: [Ethanol](https://molview.org/?cid=702)

Les molècules petites sovint són gràfics planars i es poden dibuixar com una representació bidimensional, amb algunes notacions especials per gestionar la quiralitat.

```
  H H
  | |
H-C-C-O-H
  | |
  H H
```

Aquesta és una representació molt detallada amb tots aquells hidrògens que s'enganxen als àtoms pesats.

Es pot compactar una mica movent les `H` al costat de l'àtom pesat:

`CH<sub>3</sub>-CH<sub>2</sub>-OH`


Els àtoms tenen valències: el carboni té una valència de 4, el que significa que necessita 4 enllaços simples, o 1 enllaç doble i 2 enllaços simples, o 2 enllaços dobles, o 1 enllaç triple i 1 enllaç simple.

Com que escriure tants hidrògens és molt pesat, els químics assumeixen que les valències sempre s'ompliran, en lloc d'enumerar els recomptes d'hidrogen de manera explícita, van decidir utilitzar una representació implícita d'hidrogen, on el nombre d'hidrogens en un àtom és la valència de l'àtom més la càrrega menys la suma dels seus ordres d'enllaç.

A continuació tens un alcohol etílic dibuixat amb hidrògens implícits:

`C-C-O`

A diferència dels químics generals els químics orgànics també tenen que escriure moltes `C`. 

Suposo que ja saps per quin motiu, sinò ja pots tornar al principi de l'activitat!

La pràctica estàndard és que els carbonis "normals" no es dibuixen en absolut. Qualsevol final d'enllaç o flexió sense símbol d'element s'assumeix que és un carboni sense càrrega de pes molecular mitjà.

Aquí tens un alcohol etílic tal com ho dibuixaria un químic:

`--O`

Tot això perquè t'interessa?

1. Si mires l'estructura 2D de l'etanol a PubChem no pensarás que algún error ha d'haver-hi: [Etanol](https://pubchem.ncbi.nlm.nih.gov/compound/Ethanol#section=2D-Structure)

2. Que en química orgánica les molècules estan plenes de H i C.

## Kekule

[Kekule.js](http://partridgejiang.github.io/Kekule.js/) és una biblioteca JavaScript de codi obert per a quimioinformàtica.

Actualment, està centrat en les molècules, centrant-se en proporcionar la capacitat de representar, dibuixar, editar, comparar i cercar estructures de molècules als navegadors web.

Tota la biblioteca està dividida en diversos mòduls, cadascun ofereix diferents característiques i funcions. Els usuaris poden utilitzar la combinació de mòduls per a un propòsit específic.

La taula següent enumera les característiques principals de cada mòdul a Kekule.js que ens interessa: 

| Mòdul | Caracterísitques principals |
|-|-|
| Core | Representació de conceptes químics com element, àtom, enllaç, molècula i reacció |
| IO | Llegir/escriure diferents formats de dades químiques. Aixó inclou: Format Kekule JSON/XML, CML, MDL MOL2000/3000, SMILES |
| Render | Proporciona mètodes de renderització de navegador creuat de baix nivell per dibuixar molècules (i altres objectes químics) en el context del navegador web |

### Molécula

#### Atom

Els àtoms i els enllaços són la base de diverses molècules. 

Estan representats com objectes a Kekule.js.

Crea un projecte `kekule` tal com s'explica a {% link "/typescript/module/" %}:

```sh
$ mkdir kekule && cd kekule
```

Afegeix la llibreria `kekule`:

```sh
$ npm add kekule
```

Crea un fitxer `index.html`:

```html
<!doctype html>
<html>

<head>
  <title>Kekule</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href=" https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css">
  <script type="module" src="index.js"></script>
</head>

<body>
  <div id="canvas"/>
</body>

</html>
```

Arrenca un servidor htt`p:

```sh
$ npm install http-server --save-dev
$ node_modules/http-server/bin/http-server -p 8000 -c1
...
```

A continuació creem el fitxer `index.js`:

```js
import { Kekule } from './node_modules/kekule/dist/kekule.esm.mjs'

const mol = new Kekule.Molecule();
const a1 = (new Kekule.Atom()).setSymbol('C')
mol.appendNode(a1);

draw(mol)

function draw(mol) {

  const drawBoxWidth = 500
  const drawBoxHeight = 500

  const condensed = true
  const canvas = document.getElementById('canvas')
  var bridge = new Kekule.Render.CanvasRendererBridge()

  var context = bridge.createContext(canvas, drawBoxWidth, drawBoxHeight)

  var baseCoord = { 'x': drawBoxWidth / 2, 'y': drawBoxHeight / 2 }
  var options = {
    moleculeDisplayType: condensed ? Kekule.Render.MoleculeDisplayType.CONDENSED : Kekule.Render.MoleculeDisplayType.SKELETAL,
    retainAspect: true,
    autoScale: true,
    zoom: 4
  }

  bridge.clearContext(context)

  var painter = new Kekule.Render.ChemObjPainter(Kekule.CoordMode.COORD2D, mol, bridge)
  painter.draw(context, baseCoord, options)
}
```

Aquest fitxer té una funció `draw` que s'ocupa de renderitzar la mólecula.

Obre un navegador a [http://localhost:8000/](http://localhost:8000/):

{% image "kekule-c.png" %}

El codi que crea la molécula és aquest: 

```js
const mol = new Kekule.Molecule();
const a1 = (new Kekule.Atom()).setSymbol('C')
mol.appendNode(a1);
```

Llavors com és que apareixen 4 `H` (hidrogen) ? Suposo que ja saps la resposta (s'ha explicat abans).

Pots provar amb un `O` (Oxigen) a veure que passa (i pensa perqué passa)

{% sol %}
```js
const mol = new Kekule.Molecule();
const a1 = (new Kekule.Atom()).setSymbol('O')
mol.appendNode(a1);
```
{% endsol %}


També amb una `He` (Heli) a veure que passa (i pensa perqué passa).

Pots modificar les propietats de l'àtom:

| Property | ValueType | Descripció |
|-|-|-|
| atomicNumber | `int` | Nombre atòmic de l'àtom, p. ex. 6 per al carboni. Es canviarà automàticament quan la propietat symbolestà establert. |
| symbol | string | Símbol de l'àtom, per exemple, "C" per carboni. Es canviarà automàticament quan la propietat atomicNumberestà establert. |
| massNumber | int | Nombre de massa de l'isòtop. |
| isotopeId | string | Una propietat de drecera per establir el nombre atòmic i la massa nombre tots alhora, per exemple, '13C' o 'C13'. |
| charge | float | Càrrega en aquest àtom. Es permet la càrrega parcial (per exemple, +0,5). |


```js
const mol = new Kekule.Molecule();
const a1 = (new Kekule.Atom()).setSymbol('C')
// set atom to represent C13
a1.setSymbol('C').setMassNumber(13);  // note that the property can be set cascadedly
// same effect as the previous line
a1.setIsotopeId('C13');
// atom O2-
a1.setAtomicNumber(8).setCharge(-2);
mol.appendNode(a1);
```

Alguns valors de propietats es poden establir durant la creació de l'àtom:

```js
var atom = new Kekule.Atom('atom1', 'C', 13);   // create C13 atom
```

#### Enllaç

Per crear un vincle:

```js
var bond = new Kekule.Bond('bond1');  // create a bond with an id
```

Dues de les propietats més importants de l'enllaç són el seu tipus i ordre:

| Property | ValueType | Descripció |
|-|-|-|
| bondType | string | Tipus de vincle, es pot configurar a:
`Kekule.BondType.COVALENT/IONIC/COORDINATE/METALLIC/HYDROGEN` |
| bondOrder | int | L'ordre de l'enllaç de covalència es pot establir a:
`kekule.BondOrder.SINGLE/DOUBLE/TRIPLE/…` |

```js
// create a double covalent bond
bond.setBondType(Kekule.BondType.COVALENT).setBondOrder(2);
```

Si la propietat `bondType` o  `bondOrder` no està fixada, per defecte es considera que l’enllaç és simple covalent.

Els àtoms es poden connectar entre si mitjançant enllaços, així es crea una molècula.

El codi següent crea una nova molècula d'oxirane amb tres àtoms i tres enllaços:

```js
// create molecule first
const mol = new Kekule.Molecule()
// add three atoms to molecule, property setter can be called cascadely
const a1 = (new Kekule.Atom()).setSymbol('C').setCoord2D({ 'x': -0.4, 'y': 0.23 })
const a2 = (new Kekule.Atom()).setSymbol('C').setCoord2D({ 'x': 0.4, 'y': 0.23 })
const a3 = (new Kekule.Atom()).setSymbol('O').setCoord2D({ 'x': 0, 'y': -0.46 })
mol.appendNode(a1);
mol.appendNode(a2);
mol.appendNode(a3);
// add three bonds to molecule
const b1 = (new Kekule.Bond()).setBondOrder(1).setConnectedObjs([a1, a2])
const b2 = (new Kekule.Bond()).setBondOrder(1).setConnectedObjs([a2, a3])
const b3 = (new Kekule.Bond()).setBondOrder(1).setConnectedObjs([a3, a1])
mol.appendConnector(b1)
mol.appendConnector(b2)
mol.appendConnector(b3)
```

Els àtoms o enllaços també es poden eliminar fàcilment de la molècula existent:

```js
// remove atom O related bonds in molecule
mol.removeNodeAt(2);  // the atom index starts from 0
        // or mol.removeNode(mol.getNodeAt(2));
mol.removeConnectorAt(1);
        // or mol.removeConnector(mol.getConnectorAt(1));
mol.removeConnectorAt(2);
```

Molècula després d'eliminar l'àtom `O`:

[TODO] dibuix

A part dels àtoms normals, també s'admeten pseudoàtoms i àtoms variables:

```js
const mol = new Kekule.Molecule();

// add atoms to molecule
mol.appendNode(new Kekule.Atom().setSymbol('C').setCoord2D({x: 0, y: 0.80}));
// explicit set mass number of an atom
mol.appendNode(new Kekule.Atom().setSymbol('C').setMassNumber(13).setCoord2D({x: -0.69, y: 0.40}));
mol.appendNode(new Kekule.Atom().setSymbol('C').setCoord2D({x: -0.69, y: -0.40}));
// a pseudo atom
mol.appendNode(new Kekule.Pseudoatom().setAtomType(Kekule.PseudoatomType.ANY).setCoord2D({x: 0, y: -0.80}));
mol.appendNode(new Kekule.Atom().setSymbol('C').setCoord2D({x: 0.69, y: -0.40}));
mol.appendNode(new Kekule.Atom().setSymbol('C').setCoord2D({x: 0.69, y: 0.40}));
// a variable atom
mol.appendNode(new Kekule.VariableAtom().setAllowedIsotopeIds(['F', 'Cl', 'Br']).setCoord2D({x: 1.39, y: 0.80}));

// add bonds to molecule
//   here a shortcut method appendBond(atomIndexes, bondOrder) is used
mol.appendBond([0, 1], 1);
mol.appendBond([1, 2], 2);
mol.appendBond([2, 3], 1);
mol.appendBond([3, 4], 2);
mol.appendBond([4, 5], 1);
mol.appendBond([5, 0], 2);
mol.appendBond([5, 6], 1);
```

Fins i tot el subgrup:

```js
const mol = new Kekule.Molecule()

// add atoms to molecule
mol.appendNode(new Kekule.Atom().setSymbol('C').setCoord2D({x: 0, y: 0.80}));
mol.appendNode(new Kekule.Atom().setSymbol('C').setCoord2D({x: -0.69, y: 0.40}));
mol.appendNode(new Kekule.Atom().setSymbol('C').setCoord2D({x: -0.69, y: -0.40}));
mol.appendNode(new Kekule.Atom().setSymbol('C').setCoord2D({x: 0, y: -0.80}));
mol.appendNode(new Kekule.Atom().setSymbol('C').setCoord2D({x: 0.69, y: -0.40}));
mol.appendNode(new Kekule.Atom().setSymbol('C').setCoord2D({x: 0.69, y: 0.40}));

// create a sub group
const sub = new Kekule.SubGroup();
// add atoms/bonds to sub structure
sub.appendNode(new Kekule.Atom().setSymbol('C').setCoord2D({x: 1.39, y: 0.80}));
sub.appendNode(new Kekule.Atom().setSymbol('C').setCoord2D({x: 2.0, y: 0.40}));
sub.appendBond([0, 1], 1);
// atom 0 in subgroup connected with main body
sub.appendAnchorNode(sub.getNodeAt(0));
sub.setAbbr('Et');

// then add sub structure to molecule
mol.appendNode(sub);

// add bonds to molecule
//   here a shortcut method appendBond(atomIndexes, bondOrder) is used
mol.appendBond([0, 1], 1);
mol.appendBond([1, 2], 2);
mol.appendBond([2, 3], 1);
mol.appendBond([3, 4], 2);
mol.appendBond([4, 5], 1);
mol.appendBond([5, 0], 2);
mol.appendBond([5, 6], 1);  // bond connecting subgroup
```

#### Enllaços multicèntrics

El conjunt d'eines admet enllaços multicèntrics (per exemple, enllaç `B-H-B` en [diborà](https://ca.wikipedia.org/wiki/Dibor%C3%A0) i enllaç Cp-Fe en [ferrocè](https://ca.wikipedia.org/wiki/Ferroc%C3%A8)). 

Aquest enllaç es pot crear assignant múltiples àtoms connectats:

```js
// create molecule
const mol = new Kekule.Molecule();

// add atoms to molecule
mol.appendNode(new Kekule.Atom().setSymbol('B').setExplicitHydrogenCount(2).setCoord2D({x: -1, y: 0}));
mol.appendNode(new Kekule.Atom().setSymbol('B').setExplicitHydrogenCount(2).setCoord2D({x: 1, y: 0}));
mol.appendNode(new Kekule.Atom().setSymbol('H').setCoord2D({x: 0, y: 1}));
mol.appendNode(new Kekule.Atom().setSymbol('H').setCoord2D({x: 0, y: -1}));
// add two multicenter bond: B-H-B
mol.appendBond([0, 2, 1], 1);
mol.appendBond([0, 3, 1], 1);
```

#### Connexió enllaç-enllaç 

També s'admet una connexió especial d'enllaç (per exemple, a la [sal de Zeise](https://ca.wikipedia.org/wiki/Sal_de_Zeise)):


```js
// create molecule
var mol = new Kekule.Molecule();

var atomPt = new Kekule.Atom();  // Pt atom
mol.appendNode(atomPt.setSymbol('Pt').setCoord2D({x: 0.35, y: 0}));
mol.appendNode(new Kekule.Atom().setSymbol('Cl').setCoord2D({x: 0.35, y: 0.80}));
mol.appendNode(new Kekule.Atom().setSymbol('Cl').setCoord2D({x: 0.35, y: -0.80}));
mol.appendNode(new Kekule.Atom().setSymbol('Cl').setCoord2D({x: 1.14, y: 0}));
mol.appendNode(new Kekule.Atom().setSymbol('C').setCoord2D({x: -0.45, y: 0.40}));
mol.appendNode(new Kekule.Atom().setSymbol('C').setCoord2D({x: -0.45, y: -0.40}));

mol.appendBond([1, 0], 1, Kekule.BondType.IONIC);  // Pt-Cl
mol.appendBond([2, 0], 1, Kekule.BondType.IONIC);  // Pt-Cl
mol.appendBond([3, 0], 1, Kekule.BondType.IONIC);  // Pt-Cl
var doubleBond = mol.appendBond([4, 5], 2);        // C=C
// create bond-bond connection
var coordinateBond = new Kekule.Bond();            // (C=C)-Pt
coordinateBond.setBondType(Kekule.BondType.COORDINATE);
coordinateBond.setConnectedObjs([doubleBond, atomPt]);
mol.appendConnector(coordinateBond);
```

### Fitxers

És molt rar crear una molècula mitjançant codis JavaScript purs. Normalment l'usuari carrega una molècula de dades externes (per exemple, un fitxer o un string)

El mòdul Kekule.IO proporciona una sèrie de mètodes per realitzar l'entrada i sortida de molècules (i altres objectes químics).

#### Guardar una mòlecula

Per guardar una molècula, només cal utilitzar `saveFormatData`:

```js
var data = Kekule.IO.saveFormatData(mol, 'mol');
        // or Kekule.IO.saveMimeData(mol, 'chemical/x-mdl-molfile');
console.log(data);


var data = Kekule.IO.saveFormatData(mol, 'smi');
        // or Kekule.IO.saveMimeData(mol, 'chemical/x-daylight-smiles');
console.log(data);
```

Les dades de retorn solen ser un string.

#### Carregant desde string

Si la font de dades és un string el procés de càrrega és bastant senzill amb `Kekule.IO.loadFormatData`:

```js
var cmlData = `<cml xmlns="http://www.xml-cml.org/schema">
<molecule id="m1">
 <atomArray>
   <atom id="a2" elementType="C" x2="7.493264658965051" y2="35.58088907877604"/>
   <atom id="a3" elementType="O" x2="8.186084981992602" y2="35.18088907877604"/>
   <atom id="a1" elementType="C" x2="6.800444335937501" y2="35.18088907877604"/>
 </atomArray>
 <bondArray>
   <bond id="b2" order="S" atomRefs2="a2 a3"/>
   <bond id="b1" order="S" atomRefs2="a2 a1"/>
 </bondArray>
</molecule>
</cml>`;

var mol = Kekule.IO.loadFormatData(cmlData, 'cml');
  // or Kekule.IO.loadMimeData(cmlData, 'chemical/x-cml');
```


La molècula llegida de la cadena es retornarà immediatament. Per exemple, la molècula d'etanol carregada a l'exemple anterior:

#### Carregant des del fitxer 

És una sol·licitud habitual per carregar molècules des del fitxer font. 

El conjunt d'eines proporciona dos mètodes per a aquesta tasca: `Kekule.IO.loadUrlData` i `Kekule.IO.loadFileData`.

##### loadUrlData

El mètode `loadUrlData` obté dades d'un URL de fitxer remot, ja sigui una ruta absoluta (com ara http://www.mysite.com/myMolecule.cml ) o un camí relatiu (com data/myMolecule.cml ). El mètode carrega el fitxer font mitjançant [AJAX](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Client-side_web_APIs/Fetching_data) , així que comparteix les mateixes limitacions amb AJAX (per exemple, [same-origin policy](https://en.wikipedia.org/wiki/Same-origin_policy).

Com que la transferència de dades a través de la xarxa pot costar molt de temps, `loadUrlData` utilitzar el model asíncron per retornar el resultat. 
Accepta dos paràmetres, el primer és l'URL de la font, la segona és una funció de devolució de trucada que es cridarà quan finalitzi el procés de càrrega. L'usuari hauria de comprovar la molècula carregada en aquesta funció de devolució de trucada:

```js
var url = 'data/mol2D/quinone.mol';
Kekule.IO.loadUrlData(url, function(mol, success){
  if (success)
  {
    console.log('Loading from ' + url + ' Successful');
    showMolecule(mol);
  }
  else
  {
    console.log('Loading from ' + url + ' Failed');
  }
});
```

##### loadFileData

En els navegadors web moderns, també es pot carregar el fitxer desde una font local amb `Kekule.IO.loadFileData` i  [File API](https://developer.mozilla.org/en-US/docs/Web/API/File). 

Aquest és l'exemple:

```js
document.getElementById('inputFile').addEventListener('change', function()
  {
    var file = document.getElementById('inputFile').files[0];
    if (file)
    {
      Kekule.IO.loadFileData(file, function(mol, success)
      {
        if (success && mol)
          showMolecule(mol);
      });
    }
  });
```
loadFileDatatambé utilitza el model asíncron i necessita una callback function.

### Draw

Per dibuixar fem servir una classe especial que s’anomena painter.

El següent fragment de codi s'utilitza per dibuixar molècules amb Painter:

```js
var renderType = Kekule.Render.RendererType.R2D//R3D  // do 2D or 3D drawing

// parent element, we will draw inside it
var parentElem = document.getElementById('parent');
// clear parent elem
Kekule.DomUtils.clearChildContent(parentElem);

// create painter, bind with molecule
var painter = new Kekule.Render.ChemObjPainter(renderType, mol);

// create context inside parentElem
var dim = Kekule.HtmlElementUtils.getElemOffsetDimension(parentElem); // get width/height of parent element
var context = painter.createContext(parentElem, dim.width, dim.height); // create context fulfill parent element

// at last, draw the molecule at the center of context
painter.draw(context, {'x': dim.width / 2, 'y': dim.height / 2});
```

De fet, les molècules i altres tipus d'objectes es poden dibuixar a la pàgina HTML d'aquesta manera. 

Per exemple, el codi següent dibuixa una fletxa d'equilibri:

```js
// Create arrow glyph with initial parameters
var glyph = new Kekule.Glyph.StraightLine('glyph1', 1, {
      'startArrowType': Kekule.Glyph.ArrowType.OPEN,
      'startArrowSide': Kekule.Glyph.ArrowSide.REVERSED,
      'startArrowWidth': 0.25,
      'startArrowLength': 0.25,
      'endArrowType': Kekule.Glyph.ArrowType.OPEN,
      'endArrowSide': Kekule.Glyph.ArrowSide.SINGLE,
      'endArrowWidth': 0.25,
      'endArrowLength': 0.25,
      'lineLength': 1.5,
      'lineGap': 0.1,
      'lineCount': 2
    });
// create new painter, bind with glyph
var painter = new Kekule.Render.ChemObjPainter(Kekule.Render.RendererType.R2D, glyph);
// draw the glyph at the center of context we previous created
painter.draw(context, {'x': dim.width / 2, 'y': dim.height / 2});
```

El resultat:

## Formats de fitxers

### SDF

El format de fitxer de dades d'estructura (SDF) es basa en el format de fitxer MOL, ambdós desenvolupats per MDL Information Systems que després va ser adquirit per Biovia (ara anomenada Biovia, que pertany a Dassault Systems). 

El fitxer MOL original només codificava una sola molècula, mentre que els fitxers en format SDF poden codificar una o diverses molècules.

Els fitxers SDF són fitxers ASCII amb format que emmagatzemen informació sobre les posicions dels àtoms individuals que formen la molècula. També es codifica la informació sobre l'estat d'hibridació i la connectivitat, tot i que aquestes últimes dades s'utilitzen amb menys freqüència i sovint de manera inconsistent.

Múltiples molècules estan delimitades per línies que consisteixen en quatre signes de dòlar ($$$$).

En aquest enllaç tens un exemple d'una molécula d'aigua:
[https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/water/SDF](https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/water/SDF)

```
962
  -OEChem-06162407452D

  3  2  0     0  0  0  0  0  0999 V2000
    2.5369   -0.1550    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
    3.0739    0.1550    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
    2.0000    0.1550    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
  1  2  1  0  0  0  0
  1  3  1  0  0  0  0
M  END
> <PUBCHEM_COMPOUND_CID>
962
...
```

* [Chemical Data Formats](http://www.structuralchemistry.org/pcsb/capp_cdf.php)
* [PubChem - PUG REST Documentation](https://pubchem.ncbi.nlm.nih.gov/docs/pug-rest#section=Compound-Property-Tables)

### Descàrrega directa de mol·lècules.

```sh
$ curl https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/adenine/SDF -o adenine.mol
```

En general, per a mol·lècules que existeixin les podem baixar amb la instrucció:

```sh
curl https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/{nom_molecula}/SDF -o {nom_molecula}.mol
```

Per descarregar-vos molècules des de TypeScript o Javascript ho podeu fer amb les classes que proporciona el mòdul Kekule.IO:

```js
  var reader = new Kekule.IO.MdlReader();
  //var multiple = parseInt(document.getElementById('editMultiple').value);
  var mol1;
  var result;
  var r = reader.readData(data, Kekule.IO.ChemDataType.TEXT);
  mol1 = r;
  result = r;
```

### Informació addicional des del web de l’NCBI:

El subportal d’NCBI pubchem et permet cercar tota la informació sobre mol·lècules:
[PubChem](https://pubchem.ncbi.nlm.nih.gov/)

Quan el trobem, podem veure tota la informació, descarregar-la en diversos formats i fins i tot visualitzar-la amb 2D i 3D. A més a més, et pots descarregar la visualització 2D o 3D.

En el cas de l'Ethanol (l'alcohol), aquestes són les URL que tenim (qualsevol de les 2 val).

* [https://pubchem.ncbi.nlm.nih.gov/compound/Ethanol](https://pubchem.ncbi.nlm.nih.gov/compound/Ethanol)

* [https://pubchem.ncbi.nlm.nih.gov/compound/702](https://pubchem.ncbi.nlm.nih.gov/compound/702)

El paracetamol:  [https://pubchem.ncbi.nlm.nih.gov/compound/1983](https://pubchem.ncbi.nlm.nih.gov/compound/1983)

### MOL

Molfile és un format de fitxer basat en text utilitzat per emmagatzemar i transmetre estructures moleculars. 

Tot i que els molfiles s'utilitzen més habitualment en química orgànica, el format té èxit en diferents graus a l'hora de representar també molècules inorgàniques i organometàl·liques.

* [Anatomy of a MOL file](https://chem.libretexts.org/Courses/University_of_Arkansas_Little_Rock/ChemInformatics_(2017)%3A_Chem_4399_5399/2.2%3A_Chemical_Representations_on_Computer%3A_Part_II/2.2.2%3A_Anatomy_of_a_MOL_file)

* [Stereochemistry and the V2000 Molfile Format](https://depth-first.com/articles/2021/12/29/stereochemistry-and-the-v2000-molfile-format/)


## Projecte

Per treballar amb la llibreria farem servir {% link "/typescript/react/" %}.

Per aquesta activitat farem servir el projecte [bio-kekule](https://gitlab.com/xtec/bio-kekule):

```sh
$ git clone https://gitlab.com/xtec/bio-kekule
$ cd bio-kekule
$ ./init.sh
```

Tanca el terminal i torna'l a obrir:

```sh
$ npm run next-dev
```

Obre el navegador a [http://localhost:3000/molecule](http://localhost:3000/molecule).

A partir del projecte inicial has de realitzar modificacions per ampliar la seva funcionalitat.

Per exemple:

1. Opció de renderitzar les molècules en 3D

2. A part de mostrar la molècula, també una descripció de la molécula que pots renderitzar a partir de wikipedia.

3. Que l’usuari pugui modificar el fitxer SDF i es dibuixi la nova molècula

4. Cada cop que es renderitza una molècula enviar el nom al servidor per saber quines són les més buscades.

5. Poder editar una molècula

En aquest enllaç tens vàries demos a partir de kekule: [Kekule.js Demos](https://partridgejiang.github.io/Kekule.js/demos/)

Aquí tens un altre exemple: [Hack-a-Mol](https://chemapps.stolaf.edu/jmol/jsmol/hackamol.htm)

I segur que en troves d’altres a Internet de visualització i manipulació de molècules.

Al finalitzar l’activitat has de construir una imatge i desplegar-la a Azure.

### Informació de la mol.lecula

Nosaltres no ho treballem a DAWBIO, però la informàtica mol.lecular també té molts problemes interessants.

* [Get Molecule Information — Kekule Tutorial](https://partridgejiang.github.io/Kekule.js/documents/tutorial/content/getMolInfo.html)


## TODO

Incorporar: [Atoms, Molecules, and Ions](https://2012books.lardbucket.org/books/beginning-chemistry/s07-atoms-molecules-and-ions.html)


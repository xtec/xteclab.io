---
title: Bases de dades
description: Una base de dades és una col·lecció organitzada d'informació.
---

## Introducció

La informació d'una base de dades pot tenir qualsevol format, inclosos textos, números, imatges, àudios, vídeos i molts altres (i una combinació d'aquests), però aquesta informació s'ha d'"organitzar" per a una recuperació eficient. 

### Bases de dades primàries vs secundàries

Les bases de dades sovint es classifiquen en bases de dades primàries i secundàries.

* Les bases de dades primàries contenen dades derivades experimentalment que són enviades directament pels investigadors (també anomenades "dades primàries"). 

  En essència, aquestes bases de dades serveixen com a arxius que guarden les dades originals. Per tant, també es coneixen com a bases de dades d'arxiu.

* Les bases de dades secundàries contenen dades secundàries, que es deriven de l'anàlisi i la interpretació de dades primàries. Aquestes bases de dades sovint proporcionen informació de valor afegit relacionada amb les dades primàries, utilitzant informació d'altres bases de dades i literatura científica.

  Essencialment, les bases de dades secundàries serveixen com a biblioteques de referència per a la comunitat científica, proporcionant ressenyes molt curades sobre dades primàries. Per aquest motiu, també es coneixen com a bases de dades curades o bases de coneixement.

Cal tenir en compte que la distinció entre bases de dades primàries i secundàries no sempre és clara i que moltes bases de dades tenen les característiques tant de bases de dades primàries com secundàries. 

És molt comú que una base de dades primària guardi les seves dades amb informació extreta de bases de dades secundàries. A més, com que moltes bases de dades secundàries fan que la seva informació de valor afegit estigui disponible en el domini públic, l'intercanvi de dades i la integració entre bases de dades es produeix amb molta freqüència. Com a resultat, pràcticament tots els proveïdors de dades també es converteixen en consumidors de dades en aquests dies.

### Procedència de les dades

El terme "procedència de les dades" es refereix a un registre que descriu l'origen o la font d'una dada i el procés pel qual s'ha introduït en una base de dades. En poques paraules, la procedència de les dades tracta les preguntes "d'on provenen les dades" i "com i per què les dades estan al seu lloc actual".

Tot i que la informació de procedència de les dades és fonamental per a la fiabilitat d'una font de dades (i les seves dades), aquesta informació no és fàcil de gestionar. A més, la informació prevista en una base de dades pot no ser adequada per utilitzar-la en altres bases de dades, però pot acabar integrant-s'hi de totes maneres. 

Per tant, les bases de dades han de documentar la procedència de les dades i idear una manera de notificar aquesta informació als usuaris. Al seu torn, els usuaris sempre haurien de prestar atenció al problema de la procedència de les dades quan utilitzen una base de dades.

## Bases de dades públiques

TODO

## PubChem 

[PubChem](https://pubchem.ncbi.nlm.nih.gov) és un agregador de dades, és a dir, recopila dades d'altres fonts de dades. 

Les dades de PubChem provenen de més de 500 organitzacions, incloses agències governamentals, laboratoris universitaris, empreses farmacèutiques, venedors de substàncies i altres bases de dades. Una llista actualitzada de les fonts de dades de PubChem està disponible a la pàgina [Data Sources](https://pubchem.ncbi.nlm.nih.gov/sources). 

Per entendre millor les característiques d'aquesta pàgina, pots llegir l'article [New PubChem Data Sources Page](https://pubchem.ncbi.nlm.nih.gov/docs/data-sources-page)

PubChem organitza les seves dades en tres bases de dades interconnectades: substància, compost i bioassaig.


| Base de dades | URL | Identificador |
|-|-|
| Substància | https://www.ncbi.nlm.nih.gov/pcsubstance | SID |
| Compost    | https://www.ncbi.nlm.nih.gov/pccompound  | CID |
| Bioassaig  | https://www.ncbi.nlm.nih.gov/pcassay     | AID |

Els identificadors únics utilitzats per localitzar registres en aquestes tres bases de dades s'anomenen SID (Substance ID), CID (Compound ID) i AID (Assay ID) per a les bases de dades de substàncies, compostos i bioassaigs, respectivament.

### Compostos i substàncies

Els col·laboradors de dades individuals dipositen informació sobre substàncies químiques a la base de dades de substàncies: [Substance](https://www.ncbi.nlm.nih.gov/pcsubstance). 

Diferents contribuents de dades poden proporcionar informació sobre la mateixa molècula, per tant, la mateixa estructura química pot aparèixer diverses vegades a la base de dades de substàncies. 

Per proporcionar una visió no redundant, les estructures químiques de la base de dades de substàncies es normalitzen mitjançant un procés anomenat "estandardització" i les estructures químiques úniques s'identifiquen i s'emmagatzemen a la base de dades de compostos: [Compound](https://www.ncbi.nlm.nih.gov/pccompound). 

La base de dades de Compound conté informació que no és enviada pels dipositants de dades, però sí anotada per l'equip de PubChem.

En el context de les bases de dades científiques, l'anotació es refereix al procés d'afegir informació addicional a una entrada de base de dades (per exemple, un compost a la base de dades de compostos i un assaig a la base de dades BioAssay). 

La informació anotada sempre es presenta amb la seva informació de procedència (és a dir, la font de la informació). 

La llista de totes les fonts d'anotacions utilitzades a PubChem està disponible a la pàgina [Fonts](https://pubchem.ncbi.nlm.nih.gov/sources) de PubChem. Des d'aquesta pàgina, es poden descarregar totes les anotacions d'una font concreta.

La diferència entre les bases de dades de substàncies i compostos s'explica amb més detall en aquesta aquest article: [Quina diferència hi ha entre una substància i un compost a PubChem?](http://1.usa.gov/1nl9ePL)
### BioAssay

Les descripcions d'experiments biològics sobre substàncies químiques s'emmagatzemen a la base de dades [BioAssay]( https://www.ncbi.nlm.nih.gov/pcassay). 

## Consultes

### Consultes simples      


Després de visitar un lloc web que proporciona informació química com PubChem, probablement hi havia un camp de text disponible que permetia a l'usuari introduir un nom alfanumèric, un número o una combinació d'ambdós per recuperar una substància química. 

Les cerques senzilles d'introducció de text poden semblar la forma més bàsica de buscar alguna cosa en una base de dades i sovint ho són, però moltes permeten introduir caràcters per refinar com es realitza la cerca. Utilitzant una cerca senzilla a Google com a exemple, podeu canviar com es realitza la cerca simplement posant els vostres termes entre parèntesis. Això indica al motor de cerca que tot el que està entre parèntesis s'ha de trobar abans d'incloure's als resultats. Hi ha algunes pràctiques comunes per a aquestes alteracions que s'utilitzen entre molts motors de cerca diferents, però això no s'ha d'interpretar per pensar que totes funcionaran igual.

Abans de realitzar una sèrie de cerques, busca la documentació facilitada per l'amfitrió del cercador per veure quines alteracions es poden fer perquè els resultats trobats tinguin més rellevància amb el que cerca l'usuari. La raó d'això es deu simplement al fet que les bases de dades químiques poden contenir moltes dades i això estalviarà en intentar ordenar milers de resultats. 

TODO Reviseu la secció 1.3 per veure algunes altres alteracions que sovint es permeten a les cerques de text. Preneu nota de l'ús de caràcters booleans juntament amb algunes de les diferents maneres en què es poden representar els productes químics a l'activitat 2.


### Paràmetres de cerca personalitzats

És possible que les cerques químiques avançades hagin d'utilitzar diversos paràmetres personalitzats per reduir una gran llista de productes químics a un conjunt de resultats més rellevant i més reduït. La interfície d'usuari per a una cerca personalitzada acostuma a funcionar de la mateixa manera que emplenar un formulari electrònic. Després de navegar a la cerca avançada en un lloc web, l'usuari tindrà moltes opcions per definir una cerca molt específica. Moltes d'aquestes cerques avançades inclouran caselles de selecció per seleccionar coses com ara grups funcionals, propietats experimentals i altres detalls del compost. L'usuari també pot especificar intervals per a coses com la densitat, el nombre d'àtoms o altres propietats. Les paraules clau poden ser valuoses per cercar el context d'una pàgina química per trobar una estructura que compleixi determinades categories.

### Disponibilitat i intercanvi de dades

Totes les bases de dades esmentades a l'apartat 3.4 i 3.5 (inclòs PubChem) són bases de dades públiques que ofereixen els seus continguts de manera gratuïta, i en molts casos també ofereixen una manera de descarregar dades massivament i integrar-les a la pròpia base de dades. Per tant, és molt habitual que els grups de bases de dades intercanviïn la seva informació entre ells. Això sovint planteja algunes preocupacions tècniques. Per exemple, diferents bases de dades poden utilitzar diferents representacions químiques per referir-se a la mateixa molècula. Això pot provocar una concordança incorrecta de l'estructura química entre les bases de dades, la qual cosa condueix a una integració de dades incorrecta. A més, quan una base de dades té informació incorrecta, aquest error sovint es propaga a altres bases de dades. El problema de la propagació d'errors és un problema greu, però molt comú. 1 , 2 Per tant, quan s'utilitza informació en aquestes bases de dades, s'ha de tenir en compte diversos problemes de precisió i qualitat de les dades prevalents en aquestes bases de dades. L'objectiu d'aquest curs és ajudar els estudiants a desenvolupar la capacitat d'avaluar críticament la informació química disponible a les bases de dades públiques. 

## Python

### Normalització de l'estructura

PubChem conté més de 200 milions de registres químics enviats per centenars de col·laboradors de dades. Aquests registres proporcionats pel dipositant s'arxiven en una base de dades anomenada " substància " i cada registre d'aquesta base de dades s'anomena substància . 

Els registres de la base de dades de substàncies són molt redundants, ja que diferents contribuents de dades poden enviar informació sobre el mateix producte químic, independentment els uns dels altres. 

Per tant, PubChem extreu estructures químiques úniques de la base de dades de substàncies mitjançant un procés anomenat estandardització [PubChem chemical structure standardization](https://doi.org/10.1186/s13321-018-0293-8). Aquestes estructures úniques s'emmagatzemen a la base de dades composta i els registres individuals d'aquesta base de dades s'anomenen "compostos". 

Per obtenir més informació sobre els compostos i substàncies de PubChem, llegeix aquest article[What is the difference between a substance and a compound in PubChem?](https://pubchem.ncbi.nlm.nih.gov/docs/compound-vs-substance).

Les cel·les de codi següents mostren els efectes de l'estandardització de l'estructura química.

**1.-** Baixa una llista dels SID associats amb el CID 1174 (uracil).

```py
import requests

cid = 1174

url = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/" + str(cid) + "/sids/txt"
res = requests.get(url)
sids = res.text.split()
print(len(sids))
```

La sol·licitud anterior retorna més de 360 ​​substàncies, totes estandarditzades amb la mateixa estructura (CID 1174).

```sh
367
```

**2.-** Baixa les dades d'estructura dels SID

```py
import time

chunk_size = 50

if len(sids) % chunk_size == 0 :
    num_chunks = int( len(sids) / chunk_size )
else :
    num_chunks = int( len(sids) / chunk_size ) + 1

f = open("cid2sids-uracil.sdf", "w")

for i in range(num_chunks):
    
    print("Processing chunk", i)
    
    idx1 = chunk_size * i
    idx2 = chunk_size * (i + 1)
    str_sids = ",".join(sids[idx1:idx2])
    
    url = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/substance/sid/" + str_sids + "/record/sdf"
    res = requests.get(url)

    f.write(res.text)
    time.sleep(0.2)

f.close()
```

Baixem les dades en grups de 50 i les guardem al fitxer `cid2sids-uracil.sdf`.

```
Processing chunk 0
Processing chunk 1
        .
        .
        .
Processing chunk 7
```

**3.-** Converteix les estructures del fitxer SDF a les cadenes SMILES i identifica els SMILES únics i les seves freqüències.

```py
from rdkit import Chem

unique_smiles_freq = dict()

suppl = Chem.SDMolSupplier('cid2sids-uracil.sdf')

for mol in suppl:

    smiles = Chem.MolToSmiles(mol,isomericSmiles=True)

    unique_smiles_freq[ smiles ] = unique_smiles_freq.get(smiles,0) + 1

sorted_by_freq = [ (v, k) for k, v in unique_smiles_freq.items() ]
sorted_by_freq.sort(reverse=True)
for v, k in sorted_by_freq :
    print(v, k)
```

La sortida del programa mostra que els 360+ SID associats al CID 1174 es representen amb sis cadenes SMILES diferents. A més, 12 registres de substàncies que van donar lloc a cadenes SMILES "buides", la qual cosa implica que els dipositants d'aquests registres de substàncies no van proporcionar informació estructural. 

```sh
247 O=c1cc[nH]c(=O)[nH]1
87 Oc1ccnc(O)n1
12 
7 O=c1ccnc(O)[nH]1
5 O=c1nccc(O)[nH]1
5 O=c1nc(O)cc[nH]1
4 O=c1cc[nH]c(O)n1
```

És possible que vulguis saber quines són aquestes 12 substàncies, però el codi anterior no diu quines són. 

Això es pot fer mitjançant aquest codi:

```py
for mol in suppl:

    smiles = Chem.MolToSmiles(mol,isomericSmiles=True)
    
    if ( smiles == "" ) :
        print(mol.GetProp('PUBCHEM_SUBSTANCE_ID'), ":", mol.GetProp('PUBCHEM_SUBS_AUTO_STRUCTURE'))

```

De vegades, un dipositant de dades no proporciona l'estructura d'una substància química sinó els seus sinònims químics. En aquest cas, PubChem utilitza els sinònims químics per assignar una estructura a aquest registre sense estructura:

```sh
50608295 : Deposited Substance chemical structure was generated via Synonym "CID1174" to be CID 1174
76715622 : Deposited Substance chemical structure was generated via Synonym(s) "uracil" and MeSH to be CID 1174
            .
            .
            .
384995482 : Deposited Substance chemical structure was generated via Synonym(s) "66-22-8" and Synonym Consistency to be CID 1174
```

Per exemple, el SID 50608295 (una de les 12 estructures sense cadenes SMILES a la sortida anterior) no tenia una estructura proporcionada pel dipositari, però els seus sinònims proporcionats pel dipositari inclouen "CID1174". Per tant, PubChem assigna el SID 50608295 al CID 1174, tot i que el dipositant no va proporcionar l'estructura del SID 50608295.

Si us plau, comprova l'estructura i els sinònims del SID 50608295 emmagatzemats al fitxer `cid2sids-uracil.sdf`.

**4.-** Genera les imatges de l'estructura a partir dels SMILES

Ara volem veure com són aquestes strings SMILES, dibuixant-ne estructures moleculars.

```py
from rdkit.Chem import Draw

for mysmiles in unique_smiles_freq.keys() :

    if mysmiles != "" :
        
        print(mysmiles)
        img = Draw.MolToImage( Chem.MolFromSmiles(mysmiles), size=(150, 150) )
        display(img)
```

TODO adjuntar imatges

```
O=c1cc[nH]c(=O)[nH]1
Oc1ccnc(O)n1
O=c1nc(O)cc[nH]1
O=c1nccc(O)[nH]1
O=c1ccnc(O)[nH]1
O=c1cc[nH]c(O)n1
``` 

És possible que vulguis escriure aquestes imatges de molècules en fitxers, en lloc de mostrar-les ...

```py
from rdkit.Chem import Draw

index = 1

for mysmiles in unique_smiles_freq.keys() :

    if mysmiles != "" :
        
        filename = 'image' + str(index) +'.png'
        Draw.MolToFile( Chem.MolFromSmiles(mysmiles), filename )
        index += 1
```

També és possible que vulguis mostrar totes les imatges en una sola figura:

```py
from PIL import Image

images = []

for mysmiles in unique_smiles_freq.keys() :

    if mysmiles != "" :
        
        img = Draw.MolToImage( Chem.MolFromSmiles(mysmiles), size=(150, 150) )
        images.append(img)

big_img = Image.new('RGB', (900,150))  # enought to arrange six 150x150 images

for i in range(0,len(images)):

    #paste the image at location i,j:
    big_img.paste(images[i], (i*150, 0 ) )

display(big_img)

big_img.save('image_grid.png')
```

Com es mostren aquestes imatges químiques, les més de 360 ​​substàncies associades al CID 1174 (uracil) corresponen a sis formes tautomèriques d'uracil, que es diferencien entre si en la posició dels àtoms d'hidrogen "mòbils". 

Compareu aquestes estructures amb la seva estructura estandarditzada (CID 1174).

```py
res = requests.get('https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/1174/property/isomericsmiles/txt')
img = Draw.MolToImage( Chem.MolFromSmiles( res.text.rstrip() ), size=(150, 150) )
img
```

Alternativament, pots obtenir la imatge de l'estructura del CID 1174 de PubChem.

```py
from IPython.display import Image
Image(url='https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/1174/record/PNG?image_size=300x300')
```

## Activitats

**1.-** La funció `MolToSmiles()` genera l'string SMILES canònic per defecte. 

Llegeix el manual RDKit sobre els arguments disponibles per a aquesta funció [rdkit.Chem.rdmolfiles module](https://www.rdkit.org/docs/source/rdkit.Chem.rdmolfiles.html) i escriu un codi que generi strings SMILES no canònics per al més de 360 registres associats a l'uracil (CID 1174):

* Ignora/omete els registres sense estructura mitjançant una instrucció condicional (és a dir, una sentència `if`).
* Imprimeix el número de SMILES únics no canònics.
* Imprimeix SMILES únics no canònics, ordenats per freqüència.
* Per a una molècula determinada, hi pot haver diverses maneres d'escriure strings SMILES: una d'elles es selecciona com a SMILES "canònics" i totes les altres es consideren "no canònics". Tanmateix, per a aquest exercici, volem generar només un SMILES no canònic per a cada registre (perquè la funció només retornarà una cadena SMILES (els SMILES canònics o un dels possibles SMILES no canònics)).

**2.-** La funció RDKit `MolsToGridImage()` et permet dibuixar una "imatge de quadrícula" que mostra múltiples estructures. 

Llegeix el manual RDKit sobre " MolsToGridImage() " [rdkit.Chem.Draw package](https://www.rdkit.org/docs/source/rdkit.Chem.Draw.html) i mostra les estructures representades pels SMILES únics no canònics generats a partir de l'activitat anterior.

**3.-** Recupera els registres de substàncies associats a la guanina (CID 135398634) i mostra les estructures úniques generades a partir d'ells, seguint aquests passos:

1. Recupera els SID associats al CID 135398634
2. Baixa les dades d'estructura dels SID recuperats (en SDF)
3. Genera strings SMILES canònics a partir de les dades d'estructura del fitxer SDF i identifica els strings SMILES canònics únics.
4. Dibuixa les estructures representades pels strings de SMILES canònics únics en una sola imatge.

**4.-** Recupera els registres de substàncies el sinònim dels quals és "glucosa" i mostra estructures úniques generades a partir d'ells, seguint aquests passos:

1. Recupera els SID el sinònim dels quals és "glucosa".
2. Baixa les dades d'estructura dels SID recuperats (en SDF)
3. Genera strings SMILES canònics a partir de les dades d'estructura del fitxer SDF i identifica strings SMILES canònics únics.
4. Dibuixa les estructures representades pels strings SMILES canònics únics en una sola imatge.

**5.-** Recupera els registres compostos associats als SID recuperats a l'exercici anterior i visualitza estructures úniques generades a partir d'ells, seguint aquests passos:

1. Recupera els CID associats als SID el nom dels quals és "glucosa", utilitzant una sol·licitud PUG-REST única (és a dir, utilitzant la conversió de llista que es descriu al quadern anterior, TODO "lecture03-list-conversion.ipynb").
2. Identifica els CID únics dels CID retornats mitjançant la funció set() a Python.
3. Recupera els SMILES isomèrics dels CID únics mitjançant PUG-REST.
4. Dibuixa les estructures representades pels strins SMILES retornats en una sola imatge.

### TODO

Crear imatges amb les versions javascript i wasm de RDKit.

## Activitats (2)

### Pàgina web PubChem

Llegeix aquests articles:

* [What is the difference between a substance and a compound in PubChem?](https://pubchemblog.ncbi.nlm.nih.gov/2014/06/19/what-is-the-difference-between-a-substance-and-a-compound-in-pubchem/)

* [Compound Summary Page Redesigned](https://pubchemblog.ncbi.nlm.nih.gov/2014/10/20/compound-summary-page-redesigned/)

* [Substance Record Page Released](https://pubchemblog.ncbi.nlm.nih.gov/2015/04/09/substance-record-page-released/)

* [PubChem adds a “legacy” designation for outdated data](https://pubchemblog.ncbi.nlm.nih.gov/2015/11/16/pubchem-adds-a-legacy-designation-for-outdated-data/)

* [Getting the most out of PubChem for virtual screening](http://www.tandfonline.com/doi/full/10.1080/17460441.2016.1216967)  TODO Si no teniu accés a aquest article, el manuscrit original de l'autor d'aquest article està disponible com a fitxer adjunt al final del mòdul 4

Respon a les següents preguntes:

**1.-** Explica la diferència entre les bases de dades PubChem Substance i Compound en dues o tres frases.

**2.-** Expliqca què és la pàgina "Compound Summary" d'un compost.

**3.-** Explica què és la pàgina "Substance Record" d'una substància.

**4.-** Explica el motiu pel qual es va introduir la designació "legacy" a PubChem en dues o tres frases.

**5.-**  Entre els menús disponibles a la part superior de la pàgina d'inici de [PubChem](https://pubchem.ncbi.nlm.nih.gov) hi ha "Today's Statistics". El nombre de compostos/substàncies/assaigs que es mostra en aquest menú no inclou registres "non-live". Què vol dir aquí "non-live"?
### Versions

Tot i que la base de dades de substàncies de PubChem és un arxiu per naturalesa, els proveïdors de dades sovint volen actualitzar la informació de la seva substància arxivada a PubChem. Per aquest motiu, PubChem conserva totes les "versions" diferents d'un registre de substàncies i mostra la versió més recent a la seva pàgina de registre de substàncies de manera predeterminada (feu clic [aquí](https://pubchemblog.ncbi.nlm.nih.gov/2014/10/20/compound-summary-page-redesigned/) per llegir què és la pàgina de registre de substàncies).

Ves a la pàgina d'inici de [PubChem]( https://pubchem.ncbi.nlm.nih.gov ) i segueix els passos que es descriuen a continuació.

**1.** Després de seleccionar la pestanya "Compound" a sobre del quadre de cerca, escriu "60823" al quadre de cerca i fes clic al botó "Go". Això us dirigirà a la pàgina de resum compost per al CID 60823 (atorvastatina). (Feu clic [aquí](https://pubchemblog.ncbi.nlm.nih.gov/2014/10/20/compound-summary-page-redesigned/) per llegir què és la pàgina Compund Summary)

TODO Aprendràs a cercar PubChem amb molt més detall per als dos mòduls següents (mòduls 5 i 6).

**2.-** Desplaceu-vos cap avall fins que vegeu "Contents" a la columna de l'esquerra. Amplieu aquesta taula de continguts fent clic al signe "+" abans de "Contents". Localitza la secció "Related Substances" i feu clic al recompte de registres per a l'element "Same" sota aquesta secció. 

## Recursos


* [Database Resources in Cheminformatics](https://chem.libretexts.org/Courses/Intercollegiate_Courses/Cheminformatics/03%3A_Database_Resources_in_Cheminformatics)


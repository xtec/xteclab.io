---
title: Reprsentació de molecules
description: Les molecules es poden representar de maneres molt diferents.
---

## Introducció

En els vostres estudis de química, començats als primers anys, heu trobat moltes maneres de representar els productes químics, i aquí n'enumerem algunes.

1. Noms trivials (aspirina)
2. Noms sistemàtics (àcid 2-acetiloxibenzoic)
3. Fórmula (C<sub>9</sub>H<sub>8</sub>O<sub>4</sub>)
4. Imatges  (TODO)

Ara veurem diverses altres maneres de representar productes químics, sobretot les taules de connexió i la notació de línies. 

### Representació Química per a Química Informàtica

Molt sovint, les dades i la informació sobre compostos químics es refereixen directament a l'estructura molecular (per exemple, una fórmula estructural 2D o coordenades atòmiques 3D per a una conformació particular d'un compost), o estan lligades a una estructura molecular (per exemple, propietats físiques). d'un compost, que identifiqueu per la seva fórmula estructural). La noció d'indexació, ordenació, cerca i recuperació d'informació utilitzant estructures moleculars es va originar dins del domini de la química moderna.

Gairebé tots els químics es dediquen a tasques de comunicació per registrar, cercar, visualitzar i publicar estructures moleculars. La majoria de les formes de representació química es van desenvolupar tenint en compte aquests usos. La química informàtica implica emmagatzemar, trobar i analitzar aquestes estructures utilitzant el poder de processament de dades dels ordinadors per fer coincidir compostos químics amb publicacions bibliogràfiques, propietats mesurades, procediments sintètics, espectres i estudis computacionals. Per fer aquest treball, els ordinadors han d'utilitzar la representació química per identificar, intercanviar i validar informació sobre compostos químics.

Per tal que els químics (humans) confiïn en els coneixements de la quimformàtica, és important entendre la manera com els ordinadors emmagatzemen i analitzen l'estructura química, els mètodes que utilitzen els programes informàtics i els resultats que produeixen. Per tant, la quimformàtica depèn de l'ús de representacions d'estructures moleculars i dades relacionades que siguin comprensibles tant per als científics humans com per als algorismes de màquines .
Formulació de dades d'estructura química

Interactuar amb una màquina és una forma de comunicació. En què difereix la comunicació entre químics de la comunicació entre un químic i una màquina? En cheminformatics, esteu treballant dins d'un sistema regit per regles estrictes que es defineixen explícitament. Si coneixeu les regles, podeu fer que el sistema funcioni per a vosaltres. Si no coneixeu les regles d'una forma determinada de representació, de vegades les característiques dissenyades per satisfer els requisits en un context apareixeran com a errors en un altre context.

Si un químic ha de recomanar a un altre que s'ha de realitzar una reacció utilitzant "cloroform" com a dissolvent per a una reacció, aquest seria generalment un exercici de comunicació reeixit. A tots els efectes pràctics, aquesta paraula l'entén tots els químics i no té cap ambigüitat. Tanmateix, com que "cloroform" és un nom anomenat trivial , no hi ha cap fórmula per convertir-lo en l'estructura química real que representa, i una màquina no podrà participar en aquest intercanvi d'informació tret que s'hagi indicat explícitament. quant a l'estructura química que representa aquesta paraula, expressada en un format amb el qual la màquina pugui treballar.

Una manera més descriptiva de comunicar la composició que és el cloroform és mitjançant la fórmula química, en aquest cas CHCl 3 . Un programa informàtic podria interpretar les regles bàsiques d'estructura molecular per determinar que la substància que es descriu té 5 àtoms: 1 de carboni, 1 d'hidrogen i 3 de clor. Ensamblar-ho en una molècula amb enllaços es pot basar en regles de valència, identificant 4 dels àtoms com a normalment monovalents i un com a normalment tetravalent. És bastant senzill crear un algorisme de programari que pugui unir els àtoms de la manera més òbvia, que també resulta correcta.

Més enllà de molècules tan minúscules i senzilles, aviat sorgeixen dificultats. Algunes d'aquestes ambigüitats afecten els químics humans de la mateixa manera que afecten les màquines. Considereu la fórmula molecular de C 3 H 6 O, que està associada a múltiples estructures raonables, incloent una cetona, un aldehid, un alcohol cíclic, alquens oxigenats i èters cíclics, un dels quals existeix com a dos enantiòmers:

{% image "cho.png" %}

Figura 2.1. 1

: Diferents maneres de dibuixar C 3 H 6 O (crèdit d'imatge: Evan Hepler-Smith)

Les representacions ambigües poden referir-se a més d'una entitat química. Això és cert per a la majoria de noms químics quan s'utilitzen de manera no sistemàtica, com ara "octà", quan s'utilitza com a terme comú per a tots els hidrocarburs saturats amb vuit àtoms de carboni, en lloc d'indicar sistemàticament només l'isòmer de cadena recta. Les fórmules empíriques i moleculars també solen ser ambigües.

En un sistema de representació inequívoc , cada nom o fórmula es refereix exactament a una entitat química, normalment d'una manera que us permet dibuixar una fórmula estructural. Tanmateix, cada entitat química pot estar representada per més d'un nom o fórmula. Una forma canònica és una representació completament única dins d'un sistema. Per exemple, "dietil cetona" i "3-pentanona" són noms inequívocs: cadascun representa un i només un compost. Tanmateix, com que representen el mateix compost, no són noms únics. Dins del sistema de noms preferits de la IUPAC (vegeu més avall), "3-pentanona" és un nom canònic: una representació única i inequívoca d'aquest compost.

Tingueu en compte que, com que els noms canònics són necessàriament canònics dins d'un sistema, és possible que no funcionin correctament si esteu interessats en la informació estructural que no s'adreça al sistema, o si no teniu informació estructural que el sistema requereix. Per exemple, dins d'un sistema que no aborda l'estereoquímica, els diferents enantiòmers d'un compost quiral tindran la mateixa representació "canònica". Dins d'un sistema que requereix l'especificació de l'estereoquímica, en canvi, s'haurà de triar entre representacions canòniques estereoespecífiques. Si esteu treballant amb una barreja racèmica o un compost de configuració estèreo desconeguda, això pot provocar una tergiversació i un malentès.

Una representació d'estructura química conté dos tipus d'informació: explícita i implícita . La informació explícita és la que es representa directament en una estructura de dades i com a mínim hauria de contenir allò que d'altra manera no es coneixeria, com ara l'àtom específic d'un esquelet de carboni al qual s'uneix un substituent. La informació implícita és allò que vostè (o un ordinador) podeu esbrinar a partir d'una estructura de dades, tenint en compte un cert coneixement dels principis generals i una mica de treball.

En general, les estructures de dades que contenen informació menys explícita són més simples i compactes, però requereixen més càlcul per extreure'n conclusions químiques. Les estructures de dades que contenen informació més explícita ocupen més espai i tenen un major risc de contenir inconsistències, però es poden analitzar més ràpidament d'una varietat més àmplia de maneres.

Per automatitzar les funcions de les dades químiques, l'estructura de les dades s'ha de definir sistemàticament i aplicar-se de manera coherent. Aquestes definicions formen part del que constitueix informació explícita que un algorisme pot identificar i analitzar fàcilment. L'equilibri del nivell d'informació explícita també pot afectar l'ambigüitat d'un sistema i la capacitat d'intercanviar amb precisió estructures químiques entre sistemes. Aquestes són consideracions especialment importants per a les operacions que abasten una part important del corpus de compostos químics reportats (molt més de 100 milions), més enllà de l'escala a la qual és possible la validació humana dels resultats.
Representació de l'estructura química
Fórmula estructural

En general, la manera més eficaç de comunicar-se amb un altre químic sobre l'estructura d'un compost és dibuixar-ne la fórmula estructural. Una fórmula estructural és qualsevol fórmula que indiqui la connectivitat d'un compost, és a dir, quins dels seus àtoms estan units entre si per enllaços covalents. Malauradament, la fórmula estructural és més valuosa per a molècules petites, ja que poden arribar a ser complexes a mesura que augmenta la mida de la molècula. D'altra banda, un ordinador no "veu" una fórmula com ho fa un humà, sinó que la "llegeix" com una forma de dades, i veurem dues estructures de dades que els ordinadors poden "llegir", taules de connexió i anotacions de línies. .
Noms sistemàtics

Els noms sistemàtics descriuen la fórmula estructural dels compostos. Si coneixeu les regles i el vocabulari, hauríeu de poder escriure un nom a partir d'una fórmula estructural i viceversa. Els químics han desenvolupat diverses maneres de traduir fórmules en noms, de manera que gairebé sempre és possible escriure més d'un nom sistemàtic per a un determinat compost.

de la IUPAC (Unió Internacional de Química Pura i Aplicada) [La nomenclatura](https://iupac.org/what-we-do/nomenclature/) és un conegut sistema internacional de noms químics que és generalment sistemàtic però flexible, que permet l'ús de certs noms trivials ben establerts. Com que els noms IUPAC sistemàtics es fan segons regles formalitzades, en principi podrien ser utilitzats tant per humans com per ordinadors. Tanmateix, els noms IUPAC solen ser força difícils de llegir per als químics, i molt menys d'escriure, i les regles no són canòniques, la qual cosa dóna lloc a nombroses opcions diferents per anomenar cada compost. La IUPAC ha introduït encara més regles per determinar els noms IUPAC preferits (PIN) canònics que estan orientats a fer que les màquines puguin llegir més fàcilment els noms sistemàtics.

{% image "iupac-name.png" %}

Les tecnologies semàntiques permeten a més la classificació i organització sistemàtica de termes científics, incloses les descripcions d'estructures químiques, com les proporcionades per ChEBI (Entitats químiques d'interès biològic). ChEBI descriu petites entitats moleculars basades en la nomenclatura, el simbolisme i la terminologia avalades per la IUPAC i el Comitè de Nomenclatura de la Unió Internacional de Bioquímica i Biologia Molecular (NC-IUBMB). Aquest conjunt de dades està molt seleccionat tant per experts humans com per processos de màquines, es pot cercar obertament i amb programació, i inclou referències completes a fonts autoritzades originals.


Visualitzacions gràfiques

La química informàtica aprofita la disciplina matemàtica de la teoria de grafs a l'hora de representar i comparar estructures químiques. Un gràfic representa la relació entre dues coses i la teoria de grafs implica la relació de parelles entre dos objectes, on l'objecte és un node (vèrtex o punt del gràfic) i la connexió entre els nodes són les vores (enllaços o línies) de el gràfic. En química els àtoms són els vèrtexs i els enllaços les arestes. De fet, feu servir la teoria de gràfics quan feu servir Google Maps per triar una ruta entre dues ciutats, on les ciutats són els vèrtexs i les carreteres que les connecten són les vores.

Keningsberg_bridges_marked.png

Ponts de Konigsberg al mapa (crèdit: Maksim, Wikimedia Commons)
	500px-Königsberg_graph.svg.pngProblema del pont de Konigsberg en termes de gràfic (crèdit: Riojajar, Wikimedia Commons) 	

gràfic químic.JPG

(crèdit: Office of Naval Research, Technical Report No. 41, An introduction to Graph Theory, DH Rouvray)

Figura 2.1. 2

: A l'esquerra hi ha un mapa de Konigsberg (esquerra), un gràfic que descriu el mapa (centre) i algunes molècules simples i els seus gràfics (dreta).

El 1736 Leonhard Euler va formular els fonaments de la teoria dels grafs quan va abordar el problema del pont de Konigsberg , que consistia a determinar si es podia caminar per tots els ponts fins a aquesta illa de la ciutat de Knogsberg només una vegada i caminar per tots els ponts, (i va va demostrar que no podia). Matemàticament, Euler va tractar les masses terrestres com els nodes i els ponts com les vores que uneixen els nodes. L'any 1878 el matemàtic James Sylvester va introduir el concepte de quimicògraf al seu article del Journal of Nature " Chemistry and Algebra ", el mateix any va publicar els quimiogràfics de la figura 3 al volum 1, núm. 1 del seu article de l'American Journal of Mathematics "On una aplicació de la nova teoria atòmica a la representació gràfica dels invariants i covariants de la quàntica binària, amb tres apèndixs". En el quimicograf de Sylvester, els àtoms es van convertir en els nodes i els enllaços covalents en les vores, i tingueu en compte que un enllaç doble o triple es va tractar com si tingués dues o tres arestes connectant els nodes (àtoms). Es pot veure ràpidament una relació entre aquestes i les estructures de Lewis Dot que cobreixen els estudiants de química de secundària i de primer any de química, però com veurem, els ordinadors poden manejar estructures molt més complicades del que podem dibuixar en un paper.

sylvester 1.JPG

Figura 2.1. 3

: Els primers onze de quaranta-cinc quimicografies de l'article de Sylvester de 1878 sobre l'aplicació de la nova teoria atòmica a la representació gràfica dels invariants i covariants de la quàntica binària, amb tres apèndixs".

Un dels avantatges de la teoria de grafs és que es pot utilitzar per determinar si dos gràfics tenen un mapa un a un de nodes i arestes, és a dir, si són isomòrfics (idèntics) i si un subgraf d'un graf és isomòrfic a un subgraf d'un altre, aquestes parts són idèntiques. Tot i que aquest curs d'introducció bàsica no aprofundirà en la teoria de grafs, és important que els estudiants entenguin les estructures de dades bàsiques que utilitzen els algorismes basats en la teoria de grafs, i sí, farem servir aquests algorismes.
Gràfics químics en ordinadors
Taules de connexió

Una taula de connexió fa per als ordinadors el que fa la nomenclatura sistemàtica per als químics humans: organitzen la informació estructural definida en un gràfic molecular en una forma llegible per màquina. La diferència és que els ordinadors poden llegir, ordenar, cercar i agrupar taules de connexió molt més ràpidament que els humans poden treballar amb noms sistemàtics o qualsevol altre tipus de fórmula o notació. Les taules de connexió bàsicament proporcionen informació sobre els àtoms d'una molècula, on es troben els enllaços i quins tipus d'enllaços hi ha. Es tracten amb més profunditat a la secció 2.2 i hi ha molts tipus de fitxers de dades estructurals que utilitzen taules de connexió ( secció 2.5 ). A més de les taules de connexió, altres formes habituals de representacions llegibles per màquina són visualitzacions gràfiques, notacions de línies i altres formes descriptives com la nomenclatura.

Els químics pensen amb més freqüència en l'estructura química en 2D, tot i que les molècules realment existeixen a l'espai físic 3D. La majoria dels sistemes de dades químiques ofereixen visualitzacions en 2D i 3D que els químics humans poden utilitzar en la recerca i l'anàlisi. Les coordenades 2D emmagatzemades en una taula de connexió es poden utilitzar per inferir i mostrar informació química, inclosa la fórmula estructural bàsica i informació addicional, com ara la geometria E/Z dels enllaços dobles semblants als alquens, la isomeria cis/trans dels lligands en un quadrat. complex metàl·lic pla, o substituents en un alcà cíclic. Les representacions en 2D estan dissenyades per imitar l'experiència de dibuixar fórmules estructurals en paper. Els humans solen convertir aquests dibuixos electrònics en dos fitxers d'imatges per utilitzar-los en publicacions i presentacions, però aquests fitxers d'imatge (jpeg, gif, ping,....) ja no estan connectats directament a dades químiques i, per tant, no són llegibles per màquina.

Les coordenades 3D (x,y,z) també es poden emmagatzemar per a cada àtom i utilitzar-les per mostrar la conformació d'una molècula. Aquestes coordenades es poden determinar experimentalment (normalment mitjançant cristal·lografia de raigs X) o calcular-se (utilitzant camps de força, química quàntica, dinàmica molecular o models compostos com ara l'acoblament). Comprendre la forma real d'una molècula, ja sigui en solució, al buit o al lloc d'unió d'una proteïna, obre un domini completament nou de la química computacional. La majoria de les molècules tenen certa flexibilitat, i fins i tot si una conformació determinada és la més estable, sovint hi ha diverses formes competidores a tenir en compte. Conèixer com es va determinar un conjunt de coordenades particular és crucial per fer-ne un ús intel·ligent amb finalitats de quimformàtica.
Notacions de línia

Les notacions de línia representen estructures químiques com una cadena lineal de caràcters simbòlics que es poden interpretar mitjançant conjunts de regles sistemàtiques i que es tractaran a la secció 2.3 . La notació de línies es podria considerar com una nomenclatura per a ordinadors, ja que com una taula de connexió, un ordinador pot "llegir" una notació de línies i desenvolupar una molècula de la mateixa manera que un humà pot llegir la nomenclatura IUPAC i generar la molècula. Moltes formes de notació de línies són llegibles tant per màquines com per humans.

La notació de línies s'utilitza àmpliament en química informàtica perquè:

    molts processos computacionals funcionen amb més eficàcia en dades estructurades com a cadenes lineals que en dades estructurades com a taules.
    Les anotacions de línia poden ser raonablement llegibles per als químics humans que dissenyen funcions amb aquestes eines.

Les representacions lineals són especialment adequades per a moltes funcions d'identificació i caracterització, com ara determinar:

    si les molècules són iguals;
    com de semblants són, segons alguna mètrica;
    si una entitat molecular és una subestructura d'una altra;
    si dues molècules estan relacionades per una transformació específica;
    què passa quan les molècules es tallen en trossos i s'empelten juntes en diferents posicions.

En aquestes i altres aplicacions de la quimformàtica, les representacions de notació lineal tenen avantatges clau per a la velocitat i l'automatització, especialment quan us agradaria manejar un gran nombre d'estructures (per exemple, cercant una gran base de dades).

Exemples de notacions de línia inclouen la Notació de Fórmula de Línia de Wiswesser (WLN), la Notació de Línia Sybyl (SLN) i la Representació del diagrama d'estructura disposat linealment (ROSDAL). Actualment, les notacions lineals més utilitzades són el sistema simplificat d'entrada de línies d'entrada molecular (SMILES) i l'identificador químic de la IUPAC (InChI). En aquesta classe ens centrarem en els SMILES i la notació de línies InChI.


## Recursos

* [Representing Small Molecules on Computers](https://chem.libretexts.org/Courses/Intercollegiate_Courses/Cheminformatics/02%3A_Representing_Small_Molecules_on_Computers)

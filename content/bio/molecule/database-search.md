---
title: Consultes en bases de dades
description: L pàgina d'inici de PubChem proporciona una interfície de cerca que permet als usuaris realitzar qualsevol cerca de terme/paraula clau/identificador amb les tres bases de dades principals de PubChem. 
---

* [Searching databases](https://chem.libretexts.org/Courses/Intercollegiate_Courses/Cheminformatics/04%3A_Searching_Databases_for_Chemical_Information)
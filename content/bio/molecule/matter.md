---
title: Materia
---

## Introuducció

La química és l'estudi de les interaccions de la matèria amb una altra matèria i amb l'energia.

És una definició molt senzilla excepte que la definició fa servir alguns termes que també s'han de definir.

La matèria és tot allò que té massa i ocupa espai. 

Un llibre és matèria, un ordinador és matèria, el menjar és matèria i la brutícia del terra és matèria. 

De vegades la matèria pot ser difícil d'identificar. Per exemple, l'aire és matèria, però com que és tan poc densa en comparació amb una altra matèria (per exemple, un llibre, un ordinador, menjar i brutícia), de vegades oblidem que l'aire té massa i ocupa espai. 

Les coses que no són matèria inclouen pensaments, idees, emocions i esperances.

**1.-** Quins d'aquests conceptes són matèria?

1. Un entrepà de pernil.
2. L'amor
3. un arbre

{% sol %}
1. Un entrepà de pernil té massa i ocupa espai, així que és matèria.
2. L'amor és una emoció, i les emocions no són matèria.
3. Un arbre té massa i ocupa espai, per tant és matèria.
{% endsol %}

**2.-** Quins d'aquests conceptes són matèria

1. La lluna
2. Una idea per a un nou invent

{% sol %}
1. La lluna és matèria.
2. La invenció en si pot ser matèria, però la idea no ho és. 
{% endsol %}

## Propietats

Per entendre la matèria i com canvia, hem de ser capaços de descriure la matèria. 

Hi ha dues maneres bàsiques de descriure la matèria: per les seves propietats físiques o per les seves propietats químiques. 

Les **propietats físiques** són característiques que descriuen la matèria tal com existeix. 

Algunes de les moltes característiques físiques de la matèria són la forma, el color, la mida i la temperatura.

Una propietat física important és la fase (o estat) de la matèria, que fonamentalment són  són sòlida, líquida i gasos.

Per exemple l'aigua pot estar en estat sólid (glaç) o gas (vapor).

Les **propietats químiques** són característiques de la matèria que descriuen com la matèria canvia en presència d'una altra matèria

Per exemple, que una matèria es pugui cremar és una propietat química com també ho és que es comporti violentament quan entra en contacte amb l'aigua.

### Canvis

Si la matèria es mantingués sempre igual, la química seria més aviat avorrida. Afortunadament, una part important de la química implica canvis. 

Un **canvi físic** es produeix quan una mostra de matèria canvia una o més de les seves propietats físiques. 

Per exemple:

1. Un sòlid es pot fondre, com quan el gel sòlid es fon en aigua liquida.

2. L'alcohol d'un termòmetre canvia de volum a mesura que canvia la temperatura. 

Un canvi físic no afecta la composició química de la matèria.

Un **canvi químic** és un procés que implica una propietat química.

Per exemple, la fusta és inflamable i a mesura que un tros de fusta es va cremant, la seva composició química canvia i es creen noves formes de matèria amb noves propietats físiques. 

Tingues en compte que *els canvis químics solen anar acompanyats de canvis físics*, ja que la nova matèria probablement tindrà propietats físiques diferents de la matèria original.


**1.-** Descriu cada procés com un canvi físic o com un canvi químic.

1. L'aigua de l'aire es converteix en neu.
2. A una persona li tallen el cabell.
3. La massa de pa es converteix en pa al posar-la al forn.

{% sol %}
1. Com que l'aigua passa d'una fase gasosa a una fase sòlida, aquest és un canvi físic.
2. El cabell llarg s'ha escurçat. Això és un canvi físic.
3. A causa de la temperatura del forn, es produeixen canvis químics a la massa de pa per fer pa. De fet, la majoria de la cuina implica canvis químics.
{% endsol %}

**2.-** Identifica cada procés com un canvi físic o com un canvi químic.

1. Hi ha un foc en una llar de foc.
2. L'aigua s'escalfa per fer una tassa de cafè.

{% sol %}
1. Canvi químic
2. Canvi físic. L'aigua segueix siguent aigua encara que s'evapori.
{% endsol %}

## Substància

Una mostra de matèria que té les mateixes propietats físiques i químiques a tot arreu s'anomena substància . De vegades s'utilitza la frase substància pura , però la paraula pura no és necessària. 

La definició del terme substància és un exemple de com la química té una definició específica per a una paraula que s'utilitza en el llenguatge quotidià amb una definició diferent i més vaga. Aquí, utilitzarem el terme substància amb la seva definició química estricta.

La química reconeix dos tipus diferents de substàncies: elements i compostos. 

Un **element** és el tipus més simple de substància química i no es pot descompondre en substàncies químiques més simples per mecanismes químics ordinaris.

Hi ha uns 115 elements coneguts per la ciència, dels quals 80 són estables (la resta d'elements són radioactius). 

Cada element té el seu propi conjunt únic de propietats físiques i químiques. Alguns exemples d'elements inclouen ferro, carboni i or.

Un **compost** és una combinació de més d'un element. Les propietats físiques i químiques d'un compost són diferents de les propietats físiques i químiques dels seus elements constitutius; és a dir, es comporta com una substància completament diferent. 

Es coneixen més de 50 milions de compostos i cada dia se'n descobreixen més. 

Alguns exemples de compostos inclouen aigua, penicil·lina i clorur de sodi (el nom químic de la sal de taula comuna).

### Mescla

Els elements i els compostos no són les úniques maneres en què la matèria pot estar present. Sovint ens trobem amb objectes que són combinacions físiques de més d'un element o compost. Les combinacions físiques de més d'una substància s'anomenen mescles. 

Hi ha dos tipus de mescles:

1. Una mescla heterogènia és una mescla formada per dues o més substàncies. És fàcil constatar, de vegades a simple vista, que hi ha més d'una substància. 

2. Una mescla homogènia és una combinació de dues o més substàncies que està tan íntimament barrejada que la mescla es comporta com una sola substància. Una altra paraula per a una mescla homogènia és solució. 

Per exemple:

1. Una combinació de sal i llana d'acer és una barreja heterogènia perquè és fàcil veure quines partícules de la matèria són cristalls de sal i quines són llana d'acer. 

2. D'altra banda, si agafes cristalls de sal i els dissol a l'aigua, és molt difícil saber que tens més d'una substància present només mirant, encara que facis servir un microscopi potent. El motiu és que els cristalls de sal s'han dissolt a l'aigua tan finament que no es pot dir que hi ha sal. La mescla homogènia apareix com una sola substància.

**1.-** Identifica les següents combinacions com a mescles heterogènies o com a mescles homogènies.

1. Aigua de soda (el diòxid de carboni es dissol a l'aigua).
2. Una barreja de llimadures metàl·liques de ferro i pols de sofre (tant el ferro com el sofre són elements.)

{% sol %}
1. Com que el diòxid de carboni es dissol a l'aigua, podem inferir del comportament dels cristalls de sal dissolts a l'aigua que el diòxid de carboni dissolt a l'aigua és (també) una mescla homogènia.

2. Suposant que el ferro i el sofre es barregen simplement, hauria de ser fàcil veure què és ferro i què és sofre, de manera que es tracta d'una barreja heterogènia.
{% endsol %}

**2.- ** Les combinacions següents són mescles homogènies o mescles heterogènies?

1. El cos humà
2. Una amalgama, una combinació d'alguns altres metalls dissolts en una petita quantitat de mercuri

{% sol %}
1. Mescla heterogènia
2. Mescla homogènia
{% endsol %}

Hi ha altres descriptors que podem utilitzar per descriure la matèria, especialment els elements.

Normalment podem dividir els elements en metalls i no metalls, i cada conjunt comparteix certes propietats (però no sempre):

1. Un **metall** és un element sòlid a temperatura ambient (tot i que el mercuri és una excepció coneguda), és brillant i platejat, condueix bé l'electricitat i la calor, es pot picar en làmines fines (una propietat anomenada mal·leabilitat ) i es pot estirar en fils prims (una propietat anomenada ductilitat ).

2. Un **no metall** és un element que és fràgil quan és sòlid, no condueix molt bé l'electricitat ni la calor i no es pot convertir en làmines fines o filferros. Els no metalls també existeixen en una varietat de fases i colors a temperatura ambient. 

Alguns elements tenen propietats tant de metalls com de no metalls i s'anomenen semimetalls (o metaloides) . Més endavant veurem com aquestes descripcions es poden assignar amb força facilitat a diversos elements.
El mercuri elemental és l'únic metall que existeix com a líquid a temperatura ambient. Té totes les altres propietats esperades d'un metall. A la dreta, el sofre elemental és un no metall groc que normalment es troba en pols.

A continuació tens un diagrama de flux de les relacions entre les diferents maneres de descriure la matèria:

## Exemples

### Pel matí

La majoria de la gent té un ritual matinal, un procés que passen cada matí per preparar-se per al dia. La química apareix en moltes d'aquestes activitats.

* Si et prens una dutxa o un bany al matí, és probable que facis servir sabó, xampú o tots dos. Aquests articles contenen productes químics que interactuen amb l'oli i la brutícia del cos i el cabell per eliminar-los i rentar-los. Molts d'aquests productes també contenen substàncies químiques que fan que faci bona olor; s'anomenen fragàncies.

* Quan et raspalles les dents al matí, normalment fas servir pasta de dents, una forma de sabó, per netejar-te les dents. Les pastes de dents normalment contenen partícules minúscules i dures anomenades abrasius que freguen les dents físicament. Moltes pastes de dents també contenen fluor, una substància que interacciona químicament amb la superfície de les dents per ajudar a prevenir les càries.

* Potser prens vitamines, suplements o medicaments cada matí. Les vitamines i altres suplements contenen substàncies químiques que el teu cos necessita en petites quantitats per funcionar correctament. Els medicaments són substàncies químiques que ajuden a combatre malalties i a promoure la salut.

* Potser fas uns ous ferrats per esmorzar. Fregir ous implica escalfar-los prou perquè es produeixi una reacció química per cuinar els ous.

* Després de menjar, els aliments de l'estómac reaccionen químicament perquè el cos (sobretot els intestins) pugui absorbir els aliments, l'aigua i altres nutrients.

* Si conduïu o agafeu l'autobús a l'escola o a la feina, esteu utilitzant un vehicle que probablement crema gasolina, un material que es crema amb força facilitat i proporciona energia per alimentar el vehicle. Recordeu que la crema és un canvi químic.

Aquests són només alguns exemples de com la química afecta a la teva vida quotidiana. I encara no hem arribat ni a dinar!

### Begudes carbonatades

Alguns dels principis químics senzills que es discuteixen en aquesta activitaat es poden il·lustrar amb begudes carbonatades: refrescs, cervesa i vins escumosos. 

Cada producte es fa d'una manera diferent, però tots tenen una cosa en comú: són solucions de diòxid de carboni dissolts en aigua.

El diòxid de carboni és un compost compost de carboni i oxigen. En condicions normals, és un gas. Si el refredes prou, es converteix en un sòlid conegut com a gel sec. El diòxid de carboni és un compost important en el cicle de la vida a la Terra.

Tot i que és un gas, el diòxid de carboni es pot dissoldre a l'aigua, igual que el sucre o la sal es pot dissoldre a l'aigua. Quan això passa, tenim una mescla homogènia, o una solució, de diòxid de carboni a l'aigua. No obstant això, molt poc diòxid de carboni es pot dissoldre a l'aigua. Si l'atmosfera fos diòxid de carboni pur, la solució seria només al voltant del 0,07% de diòxid de carboni. En realitat, l'aire només té un 0,03% de diòxid de carboni, de manera que la quantitat de diòxid de carboni a l'aigua es redueix proporcionalment.

No obstant això, quan es fan refrescs i cervesa, els fabricants fan dues coses importants: fan servir gas diòxid de carboni pur i l'utilitzen a pressions molt altes. Amb pressions més altes, es pot dissoldre més diòxid de carboni a l'aigua. Quan el recipient de refresc o cervesa està segellat, l'alta pressió del gas diòxid de carboni roman dins del paquet. (Per descomptat, hi ha més ingredients a la sosa i la cervesa a més del diòxid de carboni i l'aigua.)

Quan obriu un recipient de refresc o cervesa, escolteu un xiulet característic mentre s'escapa l'excés de gas de diòxid de carboni. Però també passa una altra cosa. El diòxid de carboni de la solució surt de la solució com un munt de petites bombolles. Aquestes bombolles donen una sensació agradable a la boca, tant és així que la indústria del refresc és molt important.

Alguns vins escumosos s'elaboren de la mateixa manera, forçant el diòxid de carboni al vi normal. Alguns vins escumosos (inclòs el xampany) s'elaboren tancant una ampolla de vi amb una mica de llevat. El llevat fermenta, un procés pel qual el llevat converteix els sucres en energia i l'excés de diòxid de carboni. El diòxid de carboni produït pel llevat es dissol al vi. Aleshores, quan s'obre l'ampolla de xampany, s'allibera l'augment de la pressió del diòxid de carboni i la beguda fa bombolles com un got de refresc car.

El refresc, la cervesa i el vi escumós aprofiten les propietats d'una solució de diòxid de carboni a l'aigua.


## Activitats

**1.-** Identifiqueu cadascun com a matèria o no matèria.

1. Un llibre
2. Odi
3. Llum
4. Un cotxe
5. Un ou ferrat

{% sol %}
1. Matèria
2. No matèria
3. No matèria
4. Matèria
5. Matèria
{% endsol %}

**2.-** Posa un exemple de matèria en cada fase: sòlida, líquida o gasosa.

**3.-** Cada enunciat representa una propietat física o una propietat química?

1. El sofre és groc.
2. La llana d'acer crema quan s'encén per una flama.
3. Un litre de llet pesa més de 800 grams.

{% sol %}
1. Propietat física
2. Propietat química
3. Propietat física
{% endsol %}

**4.-** Cada enunciat representa una propietat física o una propietat química?

1. Un munt de fulles es podreix lentament al pati del darrere.
2. En presència d'oxigen, l'hidrogen pot interactuar per fer aigua.
3. L'or es pot estirar en filferros molt prims.

**5.-** Cada enunciat representa un canvi físic o un canvi químic?

1. L'aigua bull i es converteix en vapor.
2. Els aliments es converteixen en forma utilitzable pel sistema digestiu.
3. L'alcohol de molts termòmetres es congela a uns -40 graus Fahrenheit.

{% sol %}
1. Canvi físic
2. Canvi químic
3. Canvi físic
{% endsol %}

**6.- **Cada enunciat representa un canvi físic o un canvi químic?

1. El grafit, una forma de carboni elemental, es pot convertir en diamant, una altra forma de carboni, a temperatures i pressions molt elevades.

2. La casa de l'altre costat del carrer ha estat pintada d'un nou color.

3. Els elements sodi i clor s'uneixen per formar una nova substància anomenada clorur de sodi.

**7.-** Distingir entre un element i un compost. Aproximadament quants de cadascun se'n coneixen?

{% sol %}
Un element és una part química fonamental d'una substància; hi ha uns 115 elements coneguts. 

Un compost és una combinació d'elements que actua com una substància diferent; hi ha més de 50 milions de substàncies conegudes.
{% endsol %}

**8.-** Quina diferència hi ha entre una mescla homogènia i una barreja heterogènia?

**9.-** Identifiqueu cadascun com una mescla heterogènia o com una mescla homogènia.

1. La sal es barreja amb pebre.
2. El sucre es dissol en aigua.
3. La pasta es cuina en aigua bullint.

{% sol %}
1. Heterogeni
2. Homogeni
3. Heterogeni
{% endsol %}

**10.-** Identifiqueu cadascun com una mescla heterogènia o com una mescla homogènia.

1. Aire
2. Brutícia
3. Un televisor

**11.-** A l'exercici 9, quines opcions també són solucions?

{% sol %}
L'opció b és una solució.
{% endsol %}


**12.-** A l'exercici 10, quines opcions són també solucions?

**13.-** Per què el ferro es considera un metall?

{% sol %}
El ferro és un metall perquè és sòlid, és brillant i condueix bé l'electricitat i la calor.
{% endsol %}

**14.-** Per què es considera l'oxigen un no metall?

**15.-** Distingir entre un metall i un no metall.

{% sol %}
Els metalls solen ser brillants, condueixen bé l'electricitat i la calor, i són mal·leables i dúctils; Els no metalls tenen una varietat de colors i fases, són fràgils en la fase sòlida i no condueixen bé la calor ni l'electricitat.
{% endsol %}

**16.-** Quines propietats tenen els semimetalls?

**17.-** El carboni elemental és un sòlid negre d'aspecte avorrit que condueix bé la calor i l'electricitat. És molt fràgil i no es pot convertir en làmines primes ni fils llargs. D'aquestes propietats, com es comporta el carboni com a metall? Com es comporta el carboni com a no metall?

{% sol %}
El carboni es comporta com un metall perquè condueix bé la calor i l'electricitat. És un no metall perquè és negre i trencadís i no es pot convertir en làmines o filferros.
{% endsol %}

**18.-** El silici pur és brillant i platejat, però no condueix bé l'electricitat ni la calor. D'aquestes propietats, com es comporta el silici com a metall? Com es comporta el silici com a no metall?



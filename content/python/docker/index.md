---
title: Docker
---

## Introducción

Docker és una eina molt útil, però en molts casos volem interactuar directament amb un llenguatge de programació enlloc de fer servir el shell.

[Docker SDK for Python](https://docker-py.readthedocs.io/) és una llibreria que permet interactuar directament amb l’API HTTP de Docker Engine, i el seu objectiu no és replicar el client Docker (escrit en Golang), sinó parlar amb l'API HTTP de Docker Engine. El client Docker és extremadament complex i és difícil de duplicar en un altre idioma. Per això, moltes de les funcions que es trobaven al client Docker no poden estar disponibles a docker-py.
És una llibreria molt utilitzada; per exemple docker-compose està escrit en Python i aprofita docker-py.

Una altra opció és la llibreria Python-on-whales, que té una relació 1 a 1 entre la CLI de Docker i la biblioteca de Python. Això s’aconsegueix comunicant-se amb la CLI de Docker en lloc d’utilitzar directament a l'API HTTP de Docker Engine.

En aquest dibuix pots veure la relació que hem exposat:

{% image "./docker-clients.png"  %}

## Docker SDK for Python

Instal.la `docker` i inicia una sessió REPL:

```sh
$ pip install docker
$ python3
```

Conecta't a Docker fent servir la configuració del teu entorn:

```py
>>> import docker
>>> client = docker.from_env()
```

Amb l'objecte `client` pots executar contenidors:

```py
>>> client.containers.run("alpine", "echo hello world")
b'hello world\n'
```

També pots executar un contenidor en segon pla:

```py
>>> container = client.containers.run("httpd", name="apache", detach="True", ports = { 80:80})
```

Des d'un altre terminal pots veure que el contenidor està en execució:

```sh
$ docker ps
CONTAINER ID   IMAGE     COMMAND              CREATED         STATUS         PORTS                               NAMES
ec4298ce9221   httpd     "httpd-foreground"   3 minutes ago   Up 3 minutes   0.0.0.0:80->80/tcp, :::80->80/tcp   apache
```

I que et pots conectar al port 80 d'apache:

```sh
$ curl localhost
<html><body><h1>It works!</h1></body></html>
```

Pots gestionar contenidors:

```py
>>> client.containers.list()
[<Container: ec4298ce9221>]

>>> container = client.containers.get('ec4298ce9221')

>>> container.attrs['Config']['Image']
'httpd'

>>> container.logs()
b'AH00558: ...\n'

>> container.stop()
```

Per obtenir més informació, [consulta la referència](https://docker-py.readthedocs.io/en/stable/client.html).

En els documents {% link "/bio/omic/blast/" %} i {% link "/bio/omic/primer/" %} tens diversos exemples que fan servir la llibreria `docker`.

---
title: Visual Studio Code
description: Existeixen moltes extensions dissenyades per crear un entorn de desenvolupament complet en Python.  
---

## Introducció

Amb "Visual Studio Code" pots treballar amb Python instal.lant l'extensió [Python](https://marketplace.visualstudio.com/items?itemName=ms-python.python) de Mircrosoft.

Comença per l'activitat {% link "/project/vscode/" %}

A continuació mira aquest tutorial:

* [Python in Visual Studio Code](https://code.visualstudio.com/docs/languages/python)
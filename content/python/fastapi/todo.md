---
title: FastAPI
---

* [FastAPI Tutorial in Visual Studio Code](https://code.visualstudio.com/docs/python/tutorial-fastapi)
* [History, Design and Future](https://fastapi.tiangolo.com/history-design-future/)

* https://python-gino.org/

* https://python-gino.org/docs/en/master/tutorials/fastapi.html

## Docker

* [Blazing fast Python Docker builds with Poetry](https://medium.com/@albertazzir/blazing-fast-python-docker-builds-with-poetry-a78a66f5aed0)

* [FastAPI in Containers - Docker](https://fastapi.tiangolo.com/deployment/docker/)

* [Choosing the Right Python Docker Image: Slim Buster vs. Alpine vs. Slim Bullseye](https://medium.com/@arif.rahman.rhm/choosing-the-right-python-docker-image-slim-buster-vs-alpine-vs-slim-bullseye-5586bac8b4c9)

* [https://github.com/svx/poetry-fastapi-docker](https://github.com/svx/poetry-fastapi-docker?tab=readme-ov-file)

* [Build a dockerized FastAPI application with poetry and gunicorn.](https://scheele.hashnode.dev/build-a-dockerized-fastapi-application-with-poetry-and-gunicorn)

## Security


https://github.com/k4black/fastapi-jwt
https://github.com/fastapi/fastapi/pull/3305
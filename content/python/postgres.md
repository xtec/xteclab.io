---
title: Postgres
---

## Introducción

Para acceder a una base de dados puedes utilizar un ORM o ejecutar sentencias SQL directamente.

El ecosistema asyncio tiene varias bibliotecas cliente de Postgres, de las cuales [asyncpg](https://github.com/MagicStack/asyncpg) y [aiopg](https://github.com/aio-libs/aiopg) son las más populares.


### asyncpg

asyncpg es una biblioteca de interfaz de base de datos diseñada específicamente para PostgreSQL y Python/asyncio. asyncpg es una implementación limpia y eficiente del protocolo binario del servidor PostgreSQL para usar con `asyncio`de Python. asyncioestructura.

asyncpg es una nueva biblioteca cliente Python de código abierto con todas las funciones para PostgreSQL. Está construido específicamente para asyncio y Python 3.5. async / await. asyncpg es el controlador más rápido entre las implementaciones comunes de Python, NodeJS y Go. 

En este enlace está la documentación: [asyncpg docs](https://magicstack.github.io/asyncpg/current/)

Crea un contenedor postgres tal como se explica en [/data/postgres/](/data/postgres/):

```sh
$ docker run -d --name pgpy -p 5432:5432 -e POSTGRES_PASSWORD=password -e POSTGRES_DB=test postgres:16
```

```

Crea un entorno virtual e instala `asyncpg`:

```sh
$ python3 -m venv .venv
$ source .venv/bin/activate
$ pip install asyncpg
```

Crea un script `db.py`:

```py
import asyncio
import asyncpg
import datetime


async def main():

    # Establish a connection to an existing database named "test"
    conn = await asyncpg.connect("postgresql://postgres:password@localhost/test")

    try:
        # Execute a statement to create a new table.
        await conn.execute(
            """
          create table if not exists users(
            id serial primary key,
            name text unique,
            dob date
        )"""
        )

        # Insert a record into the created table.
        await conn.execute(
            """
            insert into users(name, dob) values($1, $2) on conflict(name) do nothing""",
            "Alice",
            datetime.date(1980, 25, 1),
        )

        # Select a row from the table.
        row = await conn.fetchrow("select * from users where name = $1", "Alice")
        # *row* now contains <Record id=1 name='Alice' dob=datetime.date(1980, 25, 1)>
        print(row)

    finally:
        # Close the connection.
        await conn.close()


asyncio.run(main())
```

Puedes ver que asyncpg utiliza la sintaxis nativa de PostgreSQL para los argumentos de consulta: `$n`. 

Ejecuta el script:

```sh
$ python3 db.py
<Record id=1 name='Alice' dob=datetime.date(1980, 25, 1)>
```

Sigue leyendo: [asyncpg-usage](https://magicstack.github.io/asyncpg/current/usage.html)

## PEP-249

asyncpg tiene una API agradable y es la de mayor rendimiento, pero no implementa [PEP-249](https://www.python.org/dev/peps/pep-0249/), sino que prefiere el estilo paramatrizado de Postgres para evitar que asyncpg interprete o reescriba la consulta


## Referencias

* [1M rows/s from Postgres to Python](http://magic.io/blog/asyncpg-1m-rows-from-postgres-to-python/)
* [How to Use the Postgres Docker Official Image](https://www.docker.com/blog/how-to-use-the-postgres-docker-official-image/)
formats: HTML, JSON.

Una altra utilitat interessant és importar taules HTML; ja que no sempre disposarem les taules en CSV. Anem a veure el següent exemple:

<a href="https://www.geeksforgeeks.org/scraping-wikipedia-table-with-pandas-using-read_html/">Scraping Wikipedia table with Pandas using read_html() - GeeksforGeeks</a>

```py

import pandas as pd
import numpy as np

dfs = pd.read_html('https://en.wikipedia.org/wiki/Demographics_of_India',
                   match='Population distribution by states/union territories')

my_df = dfs[0].head()

print(my_df.dtypes)

# Calcular la població total
print('Total population = ', my_df['Population'].sum()) 
```

Necessitaràs importar `lxml` ja que pandas l'usa internament.
```sh
poetry add lxml
```

Aquesta tècnica d’obtenir dades es coneix com a web scrapping. Només s’ha d’utilitzar per a obtenir dades obertes i públiques que no han estat publicades amb formats com CSV o JSON; sinó ens podem trobar amb problemes ètics i legals.

Apart de tenir el compte si les dades de la web són obertes, també cal tenir en compte que l’estat d’aquestes taules no és tan estable que d’altres fonts (CSV, JSON…), ja que el disseny web sol canviar més sovint que el back-end; així que no podem confiar que sempre hi podrem accedir, caldrà decidir guardar-les als nostres servidors o usar altres fonts.
 
**Exercici. Obtenir la taula HTML i guardar-la en un fitxer CSV.**

```py
import os.path
import pandas as pd
import numpy as np
from urllib.request import urlretrieve

url = 'https://en.wikipedia.org/wiki/Demographics_of_India'
html_file = "demo-india.html"
csv_file ="demo-india.csv"

if not os.path.isfile(csv_file):
    urlretrieve(url,html_file)
   
    dfs = pd.read_html(html_file,
        match='Population distribution by states/union territories')
    df = dfs[0]
     
    # Using drop() function to delete last row
    df = df.head(df.shape[0] -1)

    df.to_csv(csv_file, index=False)
```

**Altres formats**

Podeu utilitzar la funció `read_excel()` per llegir les dades d'un full de càlcul. Té una interfície similar, però només llegeix fitxers .xlsx.

Per llegir un fitxer JSON, utilitzeu `read_json()` en canvi. 
El llistat de <a href="https://pandas.pydata.org/pandas-docs/version/2.1/user_guide/io.html">formats que permet tractar Pandas</a> és força ampli.


<hr/>

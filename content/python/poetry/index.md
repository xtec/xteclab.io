---
title: Poetry
description: Poetry és una eina de gestió de dependències i creació de biblioteques. 
---

## Instal.lació

Instal.la `poetry`:

```sh
curl -sSL https://install.python-poetry.org | python3 -
```

Tanca la sessió i obre una nova sessió.

Verifica que `poetry` funciona:

```sh
$ poetry --version
Poetry (version 1.8.3)
```

Afegeix les ajudes de Shell per tal que Poetry t'ajudi a completar les ordres amb la tecla "Tab":

```sh
$ poetry completions bash >> ~/.bash_completion
```

## Projecte

Crea un projecte amb el nom de `poetry`:

```sh
$ poetry new genbu --name app
```

Pots veure que es crea un projecte amb aquesta estructura:

```
genbu
├── pyproject.toml
├── README.md
├── app
│   └── __init__.py
└── tests
    └── __init__.py
```

Obre la carpeta amb {% link "/project/vscode/" %}:

```sh
$ code genbu
```

**TODO** Operation mode <https://python-poetry.org/docs/basic-usage/>

Versiona el projecte amb {% link "/project/git/" %}:

```sh
$ git init --initial-branch=main
```

### Dependències

En la biblioteca estàndar de Python no està tots el moduls que necessites per implementar un projecte.

Però això hi ha repositoris de paquets: un d'ells és [Pyhton Package Index](https://pypi.org/).


Per exemple, instal.la el paquet [Emoji](https://github.com/carpedm20/emoji/):

```sh
$ poetry add emoji
```

Pots veure que la secció [`tool.poetry.dependencies`] del fitxer `pyproject.toml` s'ha modificat:

{% image "add-emoji.png" %}

De manera automàtica Poetry busca una versió adequada a [PyPI](https://pypi.org/), a no ser que hagis registrar altres "repositoris" en la secció `tool.poetry.source`,  i instal.la la llibreria i les seves dependències.

Poetry et permet [especificar les depenències](https://python-poetry.org/docs/dependency-specification/) de manera molt flexible, inclós [restriccions mútliples](https://python-poetry.org/docs/dependency-specification/#multiple-constraints-dependencies).

## Entorn virtual

Crea el fitxer `test_emoji.py` a la carpeta `tests`:

```py
import emoji

result = emoji.emojize('Python is :thumbs_up:')

print(result)
```

Amb `import` pots importar un mòdul a l'entorn d'execució de l'script.

Executa l'script:

```sh
$ python tests/test_emoji.py
Traceback (most recent call last):
  File "/home/box/genbu/tests/test_emoji.py", line 1, in <module>
    import emoji
ModuleNotFoundError: No module named 'emoji'
```

Python diu que no pot trobar el mòdul `emoji` encara que poetry diu que l'ha instal.lat!

<p class="fs-1 text-center">🤔</p>


Poetry gestiona un projecte en un entorn virtual, que és un espai aïllat en que només estan les depndències que el projecte vol i cap altre.

Si preguntes a `poetry` et dirà que l'"evn" està actiu:

```sh
$ poetry env list
app-gLP7tx0M-py3.12 (Activated)
```

El que has de fer és executar l'script des de `poetry`:

```sh
$ poetry run python tests/test_emoji.py 
Python is 👍
```

### Shell

Si vols pots activar un shell anidat on pots executar les ordres dins de l'entorn virtual:

```sh
/$ poetry shell
Spawning shell within /home/box/.cache/pypoetry/virtualenvs/app-gLP7tx0M-py3.12
:~/genbu$ . /home/box/.cache/pypoetry/virtualenvs/app-gLP7tx0M-py3.12/bin/activate
(app-py3.12) :~/genbu$ 
```

Ara pots executar directament el fitxer `test_emoji.py`:

```sh
(app-py3.12) :~/genbu$ python tests/test_emoji.py 
Python is 👍
```

Pots sortir del shell amb l'ordre `deactivate`:

```sh
(app-py3.12) :~/genbu$ deactivate 
$
```

I que passa si executo directament amb el botó "Run Pyhton File"?

{% image "vscode-run.png" %}

Doncs que VS Code ja sap el que ha de fer (des de fa molt poc!):

<p class="fs-1 text-center">😄</p>

## Gitlab

Crea un projecte a {% link "/project/gitlab/" %}:

1. Escull **Create blank project**
2. Desactiva l'opció de "Initialize repository with a README"

Segueix les instruccions de "Push an existing folder" per **HTTPS**:

```sh
git remote add origin https://gitlab.com/...
git add .
git commit -m "Initial commit"
git push --set-upstream origin main
```

## Activitats

### Colorama 

Afageix el paquet [Colorama](https://github.com/tartley/colorama):

```sh
$ poetry add colorama
```

Crea un fitxer `test_colorama` que faci un print al terminal amb el text `ALARMA!`, **però en color vermell**:

{% sol %}
```py
from colorama import Fore

print(Fore.RED + "ALARMA!")
```
{% endsol %}

Executa l'script.

### Pendulum

Afegeix una dependència a [Pendulum](https://pendulum.eustace.io/):

```sh
$ poetry add pendulum
```

Crea el fitxer `test_pendulum.py`:

```py
import pendulum

now = pendulum.now("Europe/Paris")

# Changing timezone
now.in_timezone("America/Toronto")

# Default support for common datetime formats
now.to_iso8601_string()

# Shifting
now.add(days=2)
``` 

Executa el fitxer.

### Flask

Afegeix una dependència a [Flask](https://flask.palletsprojects.com/en/3.0.x/).

```sh
$ poetry add flask
```

Crea el fitxer `server.py`:

```py
from flask import Flask

app = Flask(__name__)

@app.route("/")
def home():
    return "<h1>Hello, World!</h1>"
```

Excuta flask:

```sh
$ poetry run flask --app server.py run
 * Serving Flask app 'server.py'
 * Debug mode: off
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Running on http://127.0.0.1:5000
Press CTRL+C to quit
```

Obre el navegador a <http://127.0.0.1:5000>

## TODO

```sh
$ poetry config --list
$ poetry env list
```

* <https://www.markhneedham.com/blog/2023/07/24/vscode-poetry-python-interpreter>

* [How To Test And Build Python Packages With Pytest, Tox And Poetry](https://pytest-with-eric.com/automation/pytest-tox-poetry/)

* [Blazing fast Python Docker builds with Poetry](https://medium.com/@albertazzir/blazing-fast-python-docker-builds-with-poetry-a78a66f5aed0)
* [Using Poetry and Docker to Package Your Model for AWS Lambda](https://medium.com/towards-data-science/using-poetry-and-docker-to-package-your-model-for-aws-lambda-cd6d448eb88f)

* <https://python-poetry.org/docs/faq/#poetry-busts-my-docker-cache-because-it-requires-me-to-copy-my-source-files-in-before-installing-3rd-party-dependencies>




---
title: Python
icon: python.webp
description: Python és un llenguatge de programació d'alt nivell i propòsit general molt utilitzat en la ciència, l'anàlisi de dades i la intel.ligència artificial.
---

### Fonaments

{% pages ["computation/", "algorithm/", "sequence/", "data/", "function/", "object/", "typing/", "module/", "textual/"] %}

### Input/Output

{% pages ["json/", "file/", "http/", "pydantic/"] %}

### Desenvolupament

{% pages ["vscode/", "poetry/", "test/", "debug/", "refactoring/",  "profiling/"] %}

### Web

{% pages ["fastapi/", "vite/"] %}

### Anàlisi de dades

{% pages ["numpy/", "matplotlib/", "pandas/", "statistics/", "polars/", "patito/"] %}

### Avançat

{% pages ["postgres/", "multiprocessing/", "functional/", "docker/", "dynamic-programming/"] %}







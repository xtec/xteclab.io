---
title: PyCharm
description: PyCharm és una IDE per Python
---

## Introducció

<https://www.jetbrains.com/pycharm/>


Instal.la `pycharm` amb {% link "/tool/scoop/" %}

```pwsh
> scoop bucket add extras
> scoop install extras/pycharm-professional
```

Activa la IDE amb la teva llicència educativa: {% link "/tool/jetbrains" %}.




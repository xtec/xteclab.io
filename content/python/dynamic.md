---
title: Programació dinàmica
---

## Introucció

## Lineal

Si  `while`

{% sol %}
```py
ts = [ 23.5, 25.5, 21.2, 27.4, 25.5 ]

sum = 0

i = 0
ts_len = len(ts)
while i < ts_len:
    sum += t[i]
    i += 1

medium = sum / size

print(f"{medium:.2f")
```
{% endsol %}

Per recòrrer una llista al revés has de començar pel l'index de l'últim element i anar restant fins arribar a zero:

```py
ts = [3,5,5,6,3,4,3,7,8,9,7,6,6,4]

i = len(ts) - 1
while i <= 0:
    print(t[i])
    i =-1
```
Recorrer la llista, saltant de 3 en 3:

```py
ts = [3,5,5,6,3,4,3,7,8,9,7,6,6,4]

i = 0
ts_len = len(ts)
while i < len(ts):
    print(t[i])
    i +=3
```

I un una mica més complicat, a la inversa i saltant de 3 en 3:

```py
ts = [3,5,5,6,3,4,3,7,8,9,7,6,6,4]

i = len(ts) - 1
while i <= 0:
    print(t[i])
    i =-3
```



### Activitat

**1.-** Imagina que algú a escrit aquest codi:

```py
ts = [4,5,6]

i = 0
while i < 100:
    print(ts[i])
    i += 1
```

Funcionarà correctament?

{% sol %}

```sh
4
5
6
Traceback (most recent call last):
  File "/home/box/py/test.py", line 5, in <module>
    print(ts[i])
          ~~^^^
IndexError: list index out of range
```

El programa intenta accedir al quart element de la llista en una llista que només té tres elements i  💥

Si no saps que vol dir `IndexError: list index out of range`, per això està Google.


{% endsol %}

---
title: Estadística
layout: default.njk
description: Estadística descriptiva, mesures de centralització, dispersió, outliers, correlació entre variables i corbes distribució. Amb gràfics per il·lustrar tots els conceptes.
---

## Introducció a l'Estadística

* [Python Statistics Fundamentals: How to Describe Your Data](https://realpython.com/python-statistics/)
* [Statitical functions](https://docs.scipy.org/doc/scipy/reference/stats.html#module-scipy.stats)
* [Normalització de dades en el Machine Learning](https://jorgeiblanco.medium.com/por-qu%C3%A9-la-normalizaci%C3%B3n-es-clave-e-importante-en-machine-learning-y-ciencia-de-datos-4595f15d5be0)


### Història de l'Estadística

L'estadística va sorgir a Europa al 1663, inicialment enfocada en la "ciència de l'estat" per prendre decisions polítiques basades en dades econòmiques i demogràfiques. Avui en dia, té aplicacions molt més àmplies.

{% image "stats-twain.jpeg" %}

### Branques de l'Estadística

La variabilitat de les DADES.
Els processos aleatoris que generen aquestes dades, és a dir, la PROBABILITAT.

- **Estadística Descriptiva (se centra amb les dades)**

Resum de les dades observades.
  
- **Estadística Inferencial (se centra amb les probabilitats)**

Extrapolar una informació que tinc d’unes mostres a tota la població.

**Definicions Clau**

- **Dada**: Unitat d'informació rellevant.
- **Probabilitat**: Mesura de la certesa d'un esdeveniment, sempre entre 0 i 1. 0 vol dir que el succés és impossible que passi, i 1 que segur que passa.
- **Aleatorietat**: Falta aparent de predictibilitat en esdeveniments; ja que el resultat d'un succés 
  
### Relació entre Dades i Probabilitat

- Si tenim dades observades, podem deduir probabilitats.
- Si tenim probabilitats, podem predir esdeveniments futurs.


## Llibreries útils per a la estadística.

- **Statistics** llibreria inclosa al nucli de Python per obtenir estadístiques de llistes. Si uses Pandas potser ja no et caldrà.

- **Numpy** Té moltes útilitats: generar números i mostres aleatoris, tractament valors `NaN` i `arrays`.

- **Pandas** Ens ve molt bé organitzar les dades tabulars en `DataFrames` o `Series` i proporciona mètodes per obtenir estadístiques totals i per calcular estadístiques de dades seleccionades ràpidament.

- **Polars** Tot i no ser tant coneguda, ofereix millor rendiment i la mateixa compatibilitat en gràfics per a tractar Dataframes.

- **Matplotlib** Llibreria de baix nivell per dibuixar trames i generar tot tipus de gràfics. Funciona molt bé amb estructures de Python, arrays, series, dataFrames...  Requereix més codi que Seaborn, però ofereix el màxim nivell de personalització. 

- **Seaborn** Llibreria d'alt nivell. Ideal per gent amb deadlines ajustats. Amb menys codi que Matplotlib i un rendiment no gaire inferior, s'obtenen gràfics molt bons.



## Estadística Descriptiva

### Definicions i Conceptes

- **Població**: Conjunt d'elements o esdeveniments d'interès.
- **Mostra**: Subconjunt de la població que es pot observar.
- **Variable**: Característica mesurable de la mostra.

Si llenço un dau 10 cops només, no tinc suficient informació per saber si el dau està trucat.

Però si el llenço 10.000 cops, i cada cara no surt aproximadament el mateix número de cops, llavors sí que puc dubtar del dau.

Per exemple, si vull saber la satisfacció dels alumnes dels cicles formatius, per a què la mostra sigui significativa n'escolliré de diverses families professionals (Salut, Mecànica, ...), de diversos nivells (PFI, CFGM, CFGS), a alumnes de diverses edats, que no hi hagi un excés d'homes i dones, etc...

**Conclusió: El tamany de la mostra IMPORTA.**

### Tipus de Variables

- **Quantitatives**:

  - **Continues**: Exemples com pes, la edat o alçada. En alguns estudis ens interessa molt agrupar-les per a què siguin discretes, per exemple, en grups d'edat (grup50_60,grup60_70 ...) per tal de poder representar gràfics o estudiar per separat cada franja. 

  - **Discretes**: Exemples com nombre de fills, nombre de gols o punts en un esport, o nombre d'aparicions d'una lletra o una paraula en un text.

- **Qualitatives (o de categories)**:
  
  - **Ordinals**: Amb un ordre implícit (Excel·lent, Notable ...)
  
  - **Nominals**: Sense ordre explícit, com classes de malalties (Sida,Càncer,Grip...), gènere

El valor d'una variable també es pot anomenar **"freqüència"**

   - **Freqüència absoluta**: El valor absolut.

   - **Freqüència relativa**: El valor dividit per la mida de la mostra.

És molt important calcular aquestes freqüències per fer gràfics com els diagrames de barres, de línies, de punts, mapes de calor...

## Estadística descriptiva: Mesures de Tendència Central

En estadística descriptiva es fan servir diferents mesures per intentar descriure la tendència central de les nostres dades.

- **Mitjana**: La mitjana aritmètica o mitjana és el valor obtingut en `sumar totes les dades i dividir el resultat entre el nombre total d'elements.`
En anglès es diu MEAN (Pandas, Numpy) o AVERAGE (LO Calc).
  
- **Mitjana Ponderada**: La mitjana ponderada ens interessa quan algunes de les dades tenen més pes dins del valor de la mitjana que altres.
Per exemple, en calcular les notes finals d'un mòdul on la Pràctica Pt1 compta un 70%, i l'examen compta el 30% restant.
En anglès es diu `WEIGHTED MEAN`.
  
**Per exemple, suposem que un alumne ha fet 2 pràctiques, i la primera compta un 30% de la nota i la última un 70%. L'alumne ha tret un 5 del primer i un 8 del segon. Quina nota mitjana (ponderada) tindrà al final.**

Podem calcular-ho amb Python fàcilment.

```py
notes = [5, 8]
pesos = [0.3, 0.7]

mitja_pond = 0
for i in range(len(notes)):
  mitja_pond += notes[i]*pesos[i]

print(mitja_pond)
```

I amb Pandas i Numpy encara més!

- **Moda**: La moda és el `valor (o valors si hi ha un empat) que té/nen més freqüència absoluta.`
Per calcular-ho hem de comptar el número de vegades que apareix cada valor d'una variable (les freqüències) o usar una funció que ens ho calculi automàticament.
Si tots els elements apareixen un sol cop la variable no té moda. 
També pot passar que 2 o més valors empatin en màxim número d'aparicions, en aquest cas diem que tenim una variable multimodal.

**Per exemple, suposem tenim una llista amb les notes de 10 persones i ens demanen la mitjana aritmètica i la moda.**

```py
llistaNotes = [8, 5, 6, 3, 10, 8, 6, 4, 8, 7]
```

El resultat que esperem és:

Mitjana = 6,5 

L'obtenim sumant tots els elements 8 + 5 + ... + 7 = 65; pel número d'elements. 
65 / 10 = 6,5

Moda = 8 

És la nota que més apareix de totes, 3 vegades.

 [**8**, 5, 6, 3, 10, **8**, 6, 4, **8**, 7]

**8 -> 3 vegades**
6 -> 2 vegades
5-> 1 vegada
...
3 -> 1 vegada

Calcular la moda d'uns números en Python no és fàcil. 

Primer hem de comptar les freqüències absolutes de cada valor, ordenar-les i obtenir el/s valors en té més (la `moda`). 

Podem automatizar aquests càlculs amb la llibreria `statistics` que ens posa les coses més fàcils:

```py
import statistics

llista_notes: list[int] = [8, 5, 6, 3, 10, 8, 6, 4, 8, 7]
mitjana_notes: float = sum(llista_notes) / len(llista_notes)

# Càlcul de la moda
try:
    moda_notes = statistics.multimode(llista_notes)
    print(f"Les modes (nombre de notes més freqüents): {moda_notes}")
except statistics.StatisticsError:
    print("No hi ha dades per calcular la moda.")

# Provem que obtenim els resultats esperats.
assert(mitjana_notes == 6.5)
assert(moda_notes == [8])  # Actualitzat per reflectir que és una llista
```

Però ara veurem que `pandas` ens facilita més encara la vida.

**Exemple**

Tenim les notes de 4 alumnes en 3 examens en un DataFrame. Seran números enters.

Volem calcular-ne la mitjana ponderada tenint en compte els pesos 0.2, 0.3 i 0.5 per cada examen, **en dues columnes calculades noves dins del mateix dataframe**, que es diran `Mitjana_Aritmetica` i `Mitjana_Ponderada`. 
Cal arrodonir-les a un decimal.

També necessitem la moda de tots els resultats (els 12 examens). Tingues en compte que pot haver més d'una moda.

```py
import pandas as pd
import numpy as np

# DataFrame amb les dades dels alumnes (columnes) i les seves notes
data = {
    'Alumne': ['Anna', 'Bernat', 'Carla', 'David'],
    'Examen1': [7, 4, 10, 6],
    'Examen2': [6, 8, 7, 9],
    'Examen3': [8, 7, 9, 8]
}
df = pd.DataFrame(data)

# Calculem la mitjana aritmètica d'un examen (així el professor sap si l'ha fet molt difícil)
print("Mitjana aritmètica examen 1 = ",df['Examen1'].mean(axis=0))

# Pesos per a la mitjana ponderada
pesos = [0.2, 0.3, 0.5]

df['Mitjana_Ponderada'] = np.average(df[['Examen1', 'Examen2', 'Examen3']], weights=pesos, axis=1)
print(df['Mitjana_Ponderada'])
```

Fixa't que la màgia es troba a la funció de numpy que `np.average` que li pots passar **les columnes del dataframe, els pesos (weights) i l'eix 1 (columnes).**

Ara, crea un gràfic de barres amb la mitjana ponderada de cada alumne (són 4 en total) amb Matplotlib.

```py
import matplotlib.pyplot as plt

plt.figure(figsize=(6, 6))

# Definim colors alternatius (opcional)
colors = ['skyblue' if i % 2 == 0 else 'lightgreen' for i in range(len(df))]

# Barres del gràfic.
plt.bar(df['Alumne'], df['Mitjana_Ponderada'], color=colors, label=df['Mitjana_Ponderada'])
plt.xlabel('Alumnes')
plt.ylabel('Mitjana Ponderada')
plt.title('Mitjanes Ponderades dels Alumnes')

# # Afegim els números de cada barra (opcional)
for i, v in enumerate(df['Mitjana_Ponderada']):
    plt.text(i, v + 0.1, str(round(v, 1)), ha='center', va='bottom', fontsize=10)

# Ajustar la visualització
plt.tight_layout()
plt.savefig('stats-plot1.png')
```

{% image "stats-plot1.png" %}


## Estadística descriptiva: Mesures de Variabilitat o dispersió

Per complementar les mesures de tendència central, es fan servir les mesures de variabilitat, que descriuen com varien les dades respecte al seu centre.

**Mediana**: La mediana és el valor que ocupa el lloc central de totes les dades quan aquestes estan ordenades de menor a major. Recomanable fer la gràfica de totes les observacions ordenades.
En anglès es diu median.
  
**Rang**: El rang és la **diferència entre l'observació més alta (màxim) i la més baixa (mínim)**.

**Rang = | Màx - Mín |**
  
**Desviació respecte a la mitjana**: La desviació respecte a la mitjana és la diferència en valor absolut entre cada valor de la variable estadística i la mitjana aritmètica. Aquest càlcul es fa per a calcular el pendent de les rectes de regressió.

**Variança**: La variància és la **mitjana aritmètica del quadrat de les desviacions respecte a la mitjana d'una distribució estadística**. La variància intenta descriure la dispersió de les dades. En resum, la variància seria la mitjana de les desviacions al quadrat.
Es representa com σ2 (sigma minúscula al quadrat).
  
**Desviació típica**: La desviació típica és l'**arrel quadrada de la variància**. Es representa amb la lletra grega σ.
En anglès **es conèix com std = Standard Desviation.**

**Quantil**: Els quantils són punts presos a **intervals regulars de la funció de distribució d’una variable aleatòria**. Les mostres s'ordenen segons els valors de menor a major, i el quantil és el valor en aquest punt.
  
Si el punt es pren en tant per cent de les observacions, s'anomenen percentil. Si les observacions es divideixen en quatre quarts, les tres divisions es diuen quartils **(Q1=25%, Q2=50%, Q3=75%).** Q2 és la Mediana. 
També hi ha decils **(D1=10%, … D9=90%)** si volem filar més prim.
  
L'aplicació pràctica que haureu vist dels quantils és ordenar les publicacions científiques per H-Index, on les del Q1 són les més recomanades.
  
**Rang interquartílic**: 

És la diferència (resta) entre el primer i el tercer quartil.

**IQR = Q3 − Q1.**



### Com mostrar estadístiques descriptives ?

Molt senzill! Pandas proporciona una funció que ja hem vist des del primer dia: `describe`.

Les que aquesta funció no proporcioni (com la moda) són senzilles de calcular. 

Provem-ho amb el dataset dels examens dels alumnes; amb 4 alumnes nous.

```py
import pandas as pd
import numpy as np

data1 = {
    'Alumne': ['Anna', 'Bernat', 'Carla', 'David'],
    'Examen1': [7, 4, 10, 6],
    'Examen2': [6, 8, 7, 9],
    'Examen3': [8, 7, 9, 8]
}
data2 = {
    'Alumne': ['Wally', 'Xavi', 'Yaiza', 'Zulema'],
    'Examen1': [5, 6, 10, 6],
    'Examen2': [3, 8, 7, 8],
    'Examen3': [6, 7, 8, 10]
}
df1 = pd.DataFrame(data1)
df2 = pd.DataFrame(data2)
df_total = pd.concat([df1, df2], ignore_index=True)
print(df_total.describe())
print(df_total['Examen1'].describe())
```

Resultat:
```sh
         Examen1  Examen2    Examen3
count   8.000000  8.00000   8.000000
mean    6.750000  7.00000   7.875000
std     2.187628  1.85164   1.246423
min     4.000000  3.00000   6.000000
25%     5.750000  6.75000   7.000000
50%     6.000000  7.50000   8.000000
75%     7.750000  8.00000   8.250000
max    10.000000  9.00000  10.000000
count     8.000000
mean      6.750000
std       2.187628
min       4.000000
25%       5.750000
50%       6.000000
75%       7.750000
max      10.000000
```

Fixeu-vos totes les estadístiques que hem aconseguit:

- **count:**      Recompte de files/elements
- **Mean:**       Mitjana aritmètica
- **std:**        Desviació típica
- **max i min:**  Màxim i mínim
- **50%:**        Mediana
- **25% i 75%:**  Quartil1 i Quartil3

Estadístiques generals, i també de tots els examens individualemnt (fa estadístiques de totes les dades numèriques).

Per calcular l'**IQR**, també ho tenim molt fàcil:

```py
iqr = df['Examen1'].quantile(0.75) - df['Examen1'].quantile(0.25)
print(iqr)
```

El resultat és **7.75 - 5.75 = 2**

**1.-** **Estadístiques d'un institut.**

Donat el dataframe de les notes dels 8 alumnes; realitza el següent.
- Mostra totes les estadístiques de les notes de l'examen 3.
- Mostra totes les notes de l'Anna i la Yaiza.
- Calcula l'IQR de les notes de la columna `Examen1`.
- Crea una columna anomenada `Final` amb la `mitjana aritmètica dels 3 examens`.
- Mostra la mitjana aritmètica i desviació típica de tots els examens (columna `Final`)

Assegura't que el codi passi els tests que hem posat al final de tot. 

```py
import pandas as pd
import numpy as np

data = {
    'Alumne': ['Anna', 'Bernat', 'Carla', 'David', 'Wally', 'Xavi', 'Yaiza', 'Zulema'],
    'Examen1': [7, 4, 10, 6, 5, 6, 10, 7],
    'Examen2': [6, 8, 7, 9, 3, 8, 7, 8],
    'Examen3': [8, 7, 9, 8, 6, 7, 8, 10]
}
df1 = pd.DataFrame(data)

#El teu codi va aquí

# Tests
assert IQR == 2, "Error: IQR incorrecte."
assert df1['Final'].iloc[0] == 7, "Error: Nota Final incorrecte per Anna."
assert round(mean_final, 2) == 7.25, "Error: Mitjana incorrecta."
assert round(std_final, 2) == 1.32, "Error: Desviació típica incorrecta."
```

{% sol %}

```py
import pandas as pd

data_total = {
    'Alumne': ['Anna', 'Bernat', 'Carla', 'David', 'Wally', 'Xavi', 'Yaiza', 'Zulema'],
    'Examen1': [7, 4, 10, 6, 5, 6, 10, 7],
    'Examen2': [6, 8, 7, 9, 3, 8, 7, 8],
    'Examen3': [8, 7, 9, 8, 6, 7, 8, 10]
}
df1 = pd.DataFrame(data_total)
# print(df1.describe())

# 1. Estadístiques de les notes de l'examen 3
print(df1['Examen3'].describe())

# 2. Notes de l'Anna i la Yaiza
print("\nNotes de l'Anna i la Yaiza:")
print(df1[df1['Alumne'].isin(['Anna', 'Yaiza'])])

# 3. Calcula l'IQR de les notes de la columna Examen1
Q1 = df1['Examen1'].quantile(0.25)
Q3 = df1['Examen1'].quantile(0.75)
IQR = Q3 - Q1
print(f"IQR:{IQR}")

# 4. Columna Final amb la mitjana aritmètica dels 3 examens
df1['Final'] = df1[['Examen1', 'Examen2', 'Examen3']].mean(axis=1)
print(df1[['Alumne','Final']])

# 5. Mostra la mitjana aritmètica i desviació típica de la columna Final
mean_final = df1['Final'].mean()
std_final = df1['Final'].std()
print(f"mu = {mean_final:.2f}, sigma = {std_final:.2f}")

# Tests preguntes 3, 4 i 5.
assert IQR == 2, "Error: IQR incorrecte."
assert df1['Final'].iloc[0] == 7, "Error: Nota Final incorrecte per Anna."
assert round(mean_final, 2) == 7.25, "Error: Mitjana incorrecta."
assert round(std_final, 2) == 1.32, "Error: Desviació típica incorrecta."
```

{% endsol %}

### Valors indefinits: nan.

I què passaria **si algú no fes l'examen en l'exemple anterior ? Com calcularia la mitjana ?**

I si després el fes, com substituïriem la nota ? 

Primer, ho representem amb el valor `Nan`, que es defineix així: `np.nan`.

```py
import numpy as np

# Afegir un nou alumne amb np.nan
nou_alumne = pd.DataFrame({
    'Alumne': ['Marc'],
    'Examen1': [np.nan],
    'Examen2': [6],
    'Examen3': [7]
})
df1 = pd.concat([df1, nou_alumne], ignore_index=True)

# Calcular la mitjana per a Marc (actualment amb np.nan)
mitjana_alumne = df1.loc[df1['Alumne'] == 'Marc', ['Examen1', 'Examen2', 'Examen3']].mean(axis=1)
print(f"Mitjana aritmètica de Marc (amb np.nan)= {mitjana_alumne.iloc[0]}")

# Reemplaçar np.nan per un 8
df1.loc[df1['Alumne'] == 'Marc', 'Examen1'] = 8 

# Tornar a calcular la mitjana per a Marc
nova_mitjana_alumne = df1.loc[df1['Alumne'] == 'Marc', ['Examen1', 'Examen2', 'Examen3']].mean(axis=1)
print(f"Mitjana aritmètica de Marc (sense np.nan)= {nova_mitjana_alumne.iloc[0]}")
```

Com podem veure, per a realitzar **operacions estadístiques pandas ignora els nan i només té en compte els valors existents.** 

Però per operacions matemàtiques si que és necessari ometre o reemplaçar per números els `np.nan`.


### Valors atípics (outlier).

Un **valor atípic (`outlier`) és una observació que s'allunya massa de la moda; aquesta molt lluny de la tendència principal de la resta de dades.**

Concretament, un valor outlier serà:

- Un valor inferior al `quartil1 - 1,5 * IQR` 

- Un valor superior al `quartil3 + 1,5 * IQR`

Recordm que l'`IQR és el rang interquartílic: q3 - q1.`

{% image "plotbox1.jpg" %}


**Sempre cal considerar-ne la causa.** Si són causats per errors en la lectura de les dades o mesures inusuals s'esborren, però si no cal considerar-les.

Pandas és una molt bona llibreria, però encara no ha previst com calcular els `outliers`.

De tota manera, amb aquest senzill codi podrem trobar-los i eliminar-los.

Si només volem trobar-los, ens oblidem del loc.

```py
def remove_outliers(df_in, col_name):
    q1 = df_in[col_name].quantile(0.25)
    q3 = df_in[col_name].quantile(0.75)
    iqr = q3-q1 #Interquartile range
    fence_low  = q1-1.5*iqr
    fence_high = q3+1.5*iqr
    df_out = df_in.loc[(df_in[col_name] > fence_low) & (df_in[col_name] < fence_high)]
   return df_out
```

- **Boxplot**: Un boxplot és una gràfica que mostra diversos descriptors alhora d'una o més variables: Els `tres quartils`, `el "mínim" i "màxim" calculats` i els `outliers`. Són usats en molts àmbits. 

Les llibreries com Seaborn o Matplotlib els generen molt fàcilment.


**2.-** **Estadístiques de dispersió i outliers**

Ens han donat un dataframe amb la edat, altura, pressió sistòlica i diastòlica de diversos pacients.

La pressió arterial normal, en el cas de la majoria dels adults, es defineix com una pressió sistòlica de menys de 120 i una pressió diastòlica de menys de 80.

- [Extret de l'article del NIH. Presión arterial personas mayores](https://www.nia.nih.gov/espanol/presion-arterial-alta/presion-arterial-alta-personas-mayores#:~:text=La%20presi%C3%B3n%20arterial%20normal%2C%20en,diast%C3%B3lica%20de%20menos%20de%2080.)

Doncs bé, ens demanen mostrar estadístiques centrals i de dispersió de totes les variables, calcular l'amplitud interquartilica de les pressions i crear un nou dataframe sense els outliers de la pressió (sigui sis o dia).

Codi de partida:

```py
df_pacients = pd.DataFrame({
    'edat' : [50,43,22,61,64,54,38,98],
    'altura' : [1.75,1.57,1.6,1.66,1.63,1.79,1.70,1.61],
    'pr_sis' : [125,105,150,800,130,114,105,119],
    'pr_dia' : [81,74,84,76,80,78,69,81]
})

# Tractament dels outliers
```

Solució esperada:

```sh
 edat  altura  pr_sis  pr_dia
0    50    1.75     125      81
1    43    1.57     105      74
2    22    1.60     150      84
4    64    1.63     130      80
5    54    1.79     114      78
6    38    1.70     105      69
7    98    1.61     119      81
            edat    altura      pr_sis     pr_dia
count   7.000000  7.000000    7.000000   7.000000
mean   52.714286  1.664286  121.142857  78.142857
std    23.949351  0.083238   15.826440   5.080307
min    22.000000  1.570000  105.000000  69.000000
25%    40.500000  1.605000  109.500000  76.000000
50%    50.000000  1.630000  119.000000  80.000000
75%    59.000000  1.725000  127.500000  81.000000
max    98.000000  1.790000  150.000000  84.000000
```

{% sol %}

```py
def iqr(df_in, col_name):
    '''
      Calcula l'amplitud interquartilica (-1,5*iqr,+1,5*iqr)
      i retorna els valors inferior i superior.
    '''
    q1 = df_in[col_name].quantile(0.25)
    q3 = df_in[col_name].quantile(0.75)
    iqr = q3-q1 #Interquartile range
    fence_low  = q1-1.5*iqr
    fence_high = q3+1.5*iqr
    # Si, es poden retornar 2 valors.
    return fence_low, fence_high

def remove_outliers(df_in, col_name):
    '''
      Esborra valors outliers (-1,5*iqr,+1,5*iqr)
      d'una columna d'un dataframe.
    '''
    fence_low, fence_high = iqr(df_in,col_name)
    df_out = df_in.loc[(df_in[col_name] > fence_low) & (df_in[col_name] < fence_high)]
    return df_out

df_pacients = pd.DataFrame({
    'edat' : [50,43,22,61,64,54,38,98],
    'altura' : [1.75,1.57,1.6,1.66,1.63,1.79,1.70,1.61],
    'pr_sis' : [125,105,150,800,130,114,105,119],
    'pr_dia' : [81,74,84,76,80,78,69,81]
})

print('IQR pr_sis',iqr(df_pacients,'pr_sis'))
print('IQR pr_dia',iqr(df_pacients,'pr_dia'))
# la edat també té un iqr, però no l'esborrem perquè no passa res per 
# tenir pacients grans.
# print('IQR edat',iqr(df_pacients,'edat'))

df_pacients_clean = remove_outliers(df_pacients,'pr_sis')
print(df_pacients_clean)
print(df_pacients_clean.describe())
```

{% endsol %}

### Creació de boxplot per estudiar els quartils de variables amb Seaborn.

Anem a crear gràfics de boxplot sobre l'exemple anterior dels pacients, concretament per a la pressió sistòlica.

Ho podem fer amb Matplotlib, però serà més senzill usar la `llibreria de gràfics Seaborn`. Cal importar-la amb la comanda:

```sh
poetry add seaborn
```

En el codi que hem usat abans, hem d'afegir la creació de 2 boxplot, un amb els outliers i l'altre sense els outliers (que podem esborrar amb la funció remove_outliers)

```py
import pandas as pd
import numpy as np
import seaborn as sns

def iqr(df_in, col_name):
    '''
      Calcula l'amplitud interquartilica (-1,5*iqr,+1,5*iqr)
      i retorna els valors inferior i superior.
    '''
    q1 = df_in[col_name].quantile(0.25)
    q3 = df_in[col_name].quantile(0.75)
    iqr = q3-q1 #Interquartile range
    fence_low  = q1-1.5*iqr
    fence_high = q3+1.5*iqr
    # Si, es poden retornar 2 valors.
    return fence_low, fence_high

def remove_outliers(df_in, col_name):
    '''
      Esborra valors outliers (-1,5*iqr,+1,5*iqr)
      d'una columna d'un dataframe.
    '''
    fence_low, fence_high = iqr(df_in,col_name)
    df_out = df_in.loc[(df_in[col_name] > fence_low) & (df_in[col_name] < fence_high)]
    return df_out

df_pacients = pd.DataFrame({
    'edat' : [50,43,22,61,64,54,38,98],
    'altura' : [1.75,1.57,1.6,1.66,1.63,1.79,1.70,1.61],
    'pr_sis' : [125,105,150,800,130,114,105,119],
    'pr_dia' : [81,74,84,76,80,78,69,81]
})

#print(df_pacients.describe())
print('IQR pr_sis',iqr(df_pacients,'pr_sis'))
print('IQR pr_dia',iqr(df_pacients,'pr_dia'))
# la edat també té un iqr, però no l'esborrem perquè no passa res per 
# tenir pacients grans.
# print('IQR edat',iqr(df_pacients,'edat'))

plt.figure(figsize=(8, 4))
plt.title('Pressió sistolica dels pacients (amb outliers).')
sns.boxplot(data=df_pacients, x="pr_sis")
plt.show()

df_pacients_clean = remove_outliers(df_pacients,'pr_sis')
print(df_pacients_clean)
print(df_pacients_clean.describe())

plt.figure(figsize=(8, 4))
plt.title('Pressió sistolica dels pacients (sense outliers).')
sns.boxplot(data=df_pacients_clean, x="pr_sis")
plt.show()
```

{% image "plotbox-out.png" %}

{% image "plotbox-not-out.png" %}

Fixeu-vos amb la sintaxis bàsica del gràfic `boxplot` de `Seaborn`:

1. **data**   Admet DataFrames, Series, dict, array, or list of arrays.
2. **x**      Variable que ens interessa (si a data hi ha DataFrame)
3. **y**      Si volem crear diversos plotbox classificats per un camp categòric com el gènere (si a data hi ha DataFrame)
4. **hue**    Si volem classificar encara més els boxplots.


Per tant, podem generar **diversos plotbox d'una mateixa variable**, classificats en camps categòrics (per exemple, un plotbox per homes i un altre per dones). 

Ho podreu veure amb un **exemple sobre un dels <a href="https://www.kaggle.com/c/titanic/data">datasets més coneguts del món, el dels passatgers del Titanic, que té un munt de carecterístiques de cadascun</a>**

No ens caldrà descarregar-lo perquè Seaborn ja incorpora uns quants dataSets descarregats que podem importar fàcilment a un dataFrame.

```py
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

titanic_df = sns.load_dataset("titanic")
print(titanic_df.info())

sns.boxplot(data=titanic_df, x="age", y="class")
plt.title("Distribució edats passatgers/es del Titanic en funció del tipus de bitllet")
plt.show()
```

{% image "plotbox-titanic.png" %}

Fixeu-vos! Amb només dues linies noves ho hem aconseguit :)

### Recompte de variables agrupat per categories amb Seaborn.

Partint del mateix exemple dels passatgers del Titanic i Seaborn; imaginem-nos que volem saber el número de passatgers homes i dones de cada classe.

Com ho fem ? Amb el gràfic de Seaborn `catplot`, que genera diagrames de barres classificables per diversos criteris (la `x` i el `hue`) i que permet fer operacions automàtiques dins del grup amb el paràmetre `kind`.

```py
# Now let separate the gender by classes passing 'Sex' to the 'hue' parameter
sns.catplot(x='class', data=titanic_df, hue='sex', kind='count')
```

{% image "catplot-titanic.png" %}

Teniu més d'exemples d'ús de `catplot` a la <a href="https://seaborn.pydata.org/generated/seaborn.catplot.html">web de Seaborn</a>


**3.-** Crea un gràfic que que mostri quants superivents hi ha hagut de cada classe de bitllet (First,Second,Third)

{% sol %}

```py
sns.catplot(x='alive', data=titanic_df, hue='class', kind='count')
```

{% endsol %}

---

## Estadística descriptiva: Mesures de correlació entre dues variables.

En moltes ocasions, ens pot interessar analitzar si ha una relació directa o inversa entre 2 variables d'una mostra.

Per exemple entre el temps i les temperatures, entre el pes i l'alçada de persones, l'edat i el nivell de sucre o el pes i el nivell de sucre. 

Fins i tot, podem mostrar un mapa de correlacions, entre una variable i les altres variables (sempre i quan siguin quantitatives i del mateix tipus).

Anem a veure-ho amb un exemple senzill, el dataset `tips`; que conté dades de propines proporcionades per clients d'un bar, on tenim les dades de: preu del menjar(bill), gènere(sex), diners propina (tips), si és dinar o sopar(time), el número de persones(size), etc...

```py
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# Carregar el dataset de propines
tips = sns.load_dataset("tips")

# Seleccionar només les variables numèriques
numeric_tips = tips.select_dtypes(include='number')

# Calcular la matriu de correlació
corr_matrix = numeric_tips.corr()

# Crear el heatmap de correlació
plt.figure(figsize=(10, 6))
sns.heatmap(corr_matrix, annot=True, cmap='coolwarm', linewidths=0.5)
plt.title('Heatmap de Correlació de Propines')
plt.show()

# Crear el scatter plot
plt.figure(figsize=(10, 6))
sns.scatterplot(data=tips, x='total_bill', y='tip', hue='time')
plt.title('Scatter Plot de Total Bill vs Tip')
plt.xlabel('Total Bill')
plt.ylabel('Tip')
plt.legend(title='Time')
plt.show()
```

Fixeu-vos amb el mapa; com correlaciona totes les variables mumèriques: total_bill, tips, size:

{% image "stats-corr.png" %}

Podem observar una correlació directa: 

- 0,68 entre el preu del menjar (total_bill) i els diners de propina (tips)

Diem que 2 variables estan fortament correlacionades directament si aquesta correlació és de 0,7 o superior, on el màxim és 1.

Per tant, en el **nostre cas obtenim la informació que sovint (no sempre) es compleix com més val el que s'ha menjat més s'agraeix al servei en diners de propina superiors.**

Finalment, comentar que hem dibuixat un altre gràfic de punts (scatterplot) que mira si hi ha correlació entre les propines i el preu del menjar, agrupat entre el temps de dinar i de sopar; perquè sempre va bé observar aquests gràfics per fer-nos una idea de la correlació de 2 variables a simple vista.

Seaborn també proporciona un altre tipus de gràfic molt potent per analitzar la distribució i correlació entre les variables, i fins i tot agrupar-les per una variabla categòrica (com poden ser 'Time', 'Sex' o 'Smoker').

```py
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

tips = sns.load_dataset("tips")

# Crear el pairplot amb línies de millor ajustament
sns.pairplot(tips, kind='reg', hue='time', diag_kind='kde', markers=["o", "s"])
plt.suptitle('Pairplot de Propines amb Línies de Millor Ajustament', y=1.02)
plt.show()
```

{% image "stats-pairplot.png" %}

**4.-** Agafa el dataset dels pingüins de Palmer (també inclòs a Seaborn) i dibuixa la matriu de correlació entre totes les 4 variables numèriques. 
**Comenta si has vist una forta correlació entre algún parell de variables.**

Completa el codi per aconseguir-ho:

```py
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
df = sns.load_dataset('penguins')
```

{% sol %}

```py
numeric_peng = df.select_dtypes(include='number')
matrix = numeric_peng.corr()
sns.heatmap(matrix, annot=True, vmax=1, vmin=-1, center=0, cmap='vlag')
plt.show()
```

Hi ha una forta correlació entre la longitud de les aletes i el pes dels pingüins (de 0,87 sobre 1).

{% endsol %}

---

## Histogrames i corbes de distribució normal

Una de les tasques més comuns en estadística (tant descriptiva = dades, com inferencial = probabilitats) és **generar gràfics per veure quina distribució presenten les dades un cop sabem la freqüència de cada ocurrència d'una variable**; i gràcies a Numpy i Matplotlib podem generar aquest gràfic molt fàcilment.

Una de les distribucions teòriques més utilitzada a la pràctica és la `distribució normal, també anomenada distribució gaussiana` en honor al matemàtic Carl Friedrich Gauss. 

`Charles Darwin` i el seu cosí `Francis Galton` van descobrir que les **variables associades a fenòmens naturals i quotidians que experimenten totes les espècies (inclòs l'esser humà)** segueixen, aproximadament, aquesta distribució.

<em>Caràcters morfològics (com la talla o el pes) i/o genètics, així com controls de qualitat són exemples de variables de les quals freqüentment s'assumeix que segueixen una distribució normal.</em>

### Forma i propietats distribució normal.

- La mitjana, el mode i la mediana són tots iguals. 
- L'àrea total sota la corba és igual a 1. 
- La corba és simètrica al voltant de la mitjana.
- És ideal per a representar variables numèriques contínues (com la alçada, la edat de persones...)

{% image "normald.png" %}

D'aquesta corba podem deduïr els següents `intèrvals de confiança (IC)`:

- El **68% aprox.** de les dades es troben dins d'una desviació estàndard de la mitjana. 
- El **95% aprox.** de les dades es troben dins de dues desviacions estàndard de la mitjana. 
- El **99,7% aprox.** de les dades es troben dins de tres desviacions estàndard de la mitjana.

I per a què ens serveixen aquestes propietats ? 

Si volem estudiar probabilitats, ens va bé per deduïr que hi ha un 2,38% de probabilitats (aprox) que un esportista tingui una alçada superior a la mitjana + desviació típica multiplicada per 2.

Ho pots entendre millor veient la corba de distribució:

{% image "normald_select2sigma.png" %}

Si tens curiositat com hem creat la corba, aquí tens el codi:

{% sol %}

```py
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

# Càlcul del percentatge dins de +2 sigma
mu = 0
sigma = 1
percent_within_2sigma = norm.cdf(mu + 2*sigma, mu, sigma) * 100
print(f"Percentatge des de l'inici fins al +2σ: {percent_within_2sigma:.2f}%")

# Dibuixar una corba de distribució normal estàndard
x = np.linspace(-4, 4, 1000)  # Valors de x des de -4 fins a 4
y = norm.pdf(x, 0, 1)  # Funció de densitat de probabilitat per a una normal estàndard

# Àrea sota la corba des de l'inici fins a 2 sigma
x_fill = np.linspace(-4, 2, 1000)
y_fill = norm.pdf(x_fill, 0, 1)

plt.figure(figsize=(10, 6))
plt.plot(x, y, 'b-', label='Distribució Normal')
plt.fill_between(x_fill, y_fill, 0, alpha=0.3, color='b', label='Àrea fins a 2σ')

plt.title('Corba de Distribució Normal i Àrea fins a 2σ')
plt.xlabel('x')
plt.ylabel('Densitat de probabilitat')
plt.legend()

plt.grid(True)
plt.show()
```

{% endsol %}

La segona distribució més comuna és la `distribució binomial`; que sol donar-se en obtenir mostres del clima (temperatures, pluges...) i també en esdeveniments on intervé l'atzar (l'aletorietat): probabilitat d'obtenir una cara d'un dau, una moneda...

Aprofundir en els `tipus de distribucions en estadística` queda fora de l'abast d'aquest tutorial.

De fet, ja ho vam veure a la <a href="https://xtec.dev/python/matplotlib/#histogrames-i-corbes-de-distribuci%C3%B3-normal">sessió de Matplotlib</a>


### Exemple distribució normal.

Anem a veure com dibuixar un **histograma i una corba d'una distribució Normal.** que servirà per a comparar si la nostra mostra té una forma semblant a la corba normal.

Per aquest exemple usarem una funció molt útil de Numpy que genera dades que segueixin una distribució normal.

Veiem-ho amb un exemple fictici del nivell de colesterol a la sang de 300 pacients.

```py
import numpy as np
import matplotlib.pyplot as plt

# Paràmetres per a la distribució normal
mean = 200  # mitjana del colesterol (mg/dL)
std_dev = 30  # desviació estàndard (mg/dL)
n_samples = 300  # nombre de mostres

# Generar dades aleatòries amb una distribució normal
cholesterol_levels = np.random.normal(mean, std_dev, n_samples)

# Crear l'histograma
plt.hist(cholesterol_levels, bins=20, density=True, alpha=0.6, color='g', edgecolor='black')

# Crear la corba de distribució normal
xmin, xmax = plt.xlim()
x = np.linspace(xmin, xmax, 100)
p = np.exp(-0.5*((x-mean)/std_dev)**2) / (std_dev * np.sqrt(2 * np.pi))
plt.plot(x, p, 'k', linewidth=2)

# Afegir títol i etiquetes
plt.title('Distribució dels Nivells de Colesterol')
plt.xlabel('Nivells de Colesterol (mg/dL)')
plt.ylabel('Freqüència')

# Afegir línia vertical a la mitjana
plt.axvline(mean, color='b', linestyle='dashed', linewidth=2, label='Mitjana')

# Afegir línies verticals als intervals de confiança (±1 desviació estàndard)
plt.axvline(mean - std_dev, color='r', linestyle='dotted', linewidth=2, label='-1 Desviació estàndard')
plt.axvline(mean + std_dev, color='r', linestyle='dotted', linewidth=2, label='+1 Desviació estàndard')

plt.legend()
plt.savefig('normald1.png')
```

{% image "normald1.png" %}


La sintaxis de la funció `np.random.normal` és:

1. **loc**   Mitjana aritmètica (mean) de la distribució. 
2. **scale** Desviació típica (std_dev) de la distribució. Ha de ser no negatiu. 
3. **size**  Tamany (número d'elements) de la mostra


En quant a la sintaxi de `plt.hist()` remarquem el següent:

1. **data**    El més important, on li passem les dades.
2. **bins**    Matplotlib divideix els valors de la variable contínua en 20 intèrvals discrets, en 20 barres (bins=20). 
3. **density**   Les àrees de les barres es normalitzen perquè la suma total sigui 1 (density=True). 
4. **color**     Lletra per representar el color 'g' -> green.
5. **alpha**     Transparència del color del 60%.
6. **edgecolor** Color de les vores.

Us animem a provar aquest diagrama, i a cercar altres exemples. És molt satisfactori realitzar aquest diagrama tan complet amb només 3 llibreries de Python i la comprensió de conceptes estadístics bàsics :)

---

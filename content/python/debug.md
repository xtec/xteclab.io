---
title: Debug
---

Error tolerant pytest discovery

As part of the rollout of our new testing architecture, the Test Explorer panel now supports error-tolerant pytest discovery. When an error is thrown by pytest during discovery that can be handled, for example, an error from an unknown import, all other tests will still be discovered outside of the file with the error. This feature is only available on the new testing rewrite, which is still experimental. The rewrite is currently active for 100% of Insiders and 25% of Stable users but will be universally adopted shortly. In the meantime, you can opt in or out with the pythonTestAdapter setting.

Debugger configuration for running files with arguments

The new Debugpy extension now provides a Python File with Arguments launch.json configuration, which is useful when you want to provide different input values for your Python file without modifying your code or the debugger configuration each time you run it.

To use this configuration, make sure you have the Debugpy extension installed. Open the Run and Debug view by pressing Ctrl+Shift+D and select either Create a launch.json file or the gear icon to access the launch.json file. Select Debugpy, and then select Python: File with Arguments from the available configurations.

Now when you want to debug (F5) a Python file that requires command-line arguments, a prompt appears allowing you to enter the desired arguments to pass to the Python file.
After entering your arguments, press Enter, and the debugger will start, letting you step through your code.

Theme: Catppuccin Macchiato (preview on vscode.dev)

npm package for the Python extension API

The Python extension now provides an npm package to make it easier for other extension authors to access and track changes in the Python extension API. Check out the @vscode/python-extension npm module to work with Python environments available on the user's machine.

Dev Container for the Python extension

There is now a Dev Container for the Python extension with a Microsoft official Node.js image. With this custom dev container, users are able to open the Python extension repository in a GitHub Codespace, and start working on developing and testing the Python extension instantly, with no installation required. Python versions 3.7, 3.8, 3.9, 3.10, and 3.11 are pre-installed so users can readily switch between Python versions using pyenv.

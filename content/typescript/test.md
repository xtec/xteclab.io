---
title: Test
---


## AVA


[AVA](https://github.com/avajs/ava)

Crea una nou projecte `test` amb compatibilitat `esm`:

```sh
$ mkdir test && cd test
$ npm init -y esm
Wrote to /home/david/test/package.json:

{
  "name": "test",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC"
}
```

Pots veure que s'ha creat un fitxer `package.json`.

Si executem `npm run test` es produirà un error perqué l'script amb index `test` executa ` ... && exit 1` tal com pots veure en la definició del fitxer `package.json` :

```sh
$ npm run test

> test@1.0.0 test
> echo "Error: no test specified" && exit 1

Error: no test specified
```

A continuació instal.lem  i configurem `ava`:

```sh
$ npm init ava
```

Pots veure que s'ha modificat el fitxer `package.json`:

```sh
$ cat package.json 
{
  "name": "test",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "module": "main.js",
  "scripts": {
    "test": "ava"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "dependencies": {
    "esm": "^3.2.25"
  },
  "devDependencies": {
    "ava": "^6.1.3"
  }
}
```

S'ha instal.lat una dependència amb el paquet `ava` i s'ha modificat l'script `"test"` perqué executi l'ordre `ava`.

Quan ara executem `npm run test`, l'script executa `ava` i es produeix un error perquè `ava`no ha pogut trobar cap test.

```sh
$ npm run test

> test@1.0.0 test
> ava


  ✘ Couldn’t find any files to test
```

Crea un nou fitxer en el directori arrel amb el nom `test.js`:

```js
const test = require("ava");

test('foo', t => {
	t.pass();
});

test('bar', async t => {
	const bar = Promise.resolve('bar');
	t.is(await bar, 'bar');
});
```

Executa l'script `test` (que executara `ava`):

```sh
$ npm test

> test@1.0.0 test
> ava


  ✔ foo
  ✔ bar
  ─

  2 tests passed
```




## Mocha

* [NodeJS Unit Testing Tutorial: A Comprehensive Guide](https://medium.com/backenders-club/nodejs-unit-testing-tutorial-a-comprehensive-guide-85c9734e47b6)

* [Jest vs Mocha](https://medium.com/@aghaei.dev/jest-vs-mocha-ed1e69fe2b01)
---
title: MongoDB
----

## Introducció


## Entorn de treball

Crea un projecte amb {% link "/typescript/bun" %}:

```pwsh
> mkdir database
> cd database
> bun init -y
```

Afegeix una dependència amb `mongodb`:

```pwsh
> bun add mongodb
```

Obre el projecte amb {% link "/project/fleet/" %}.

Arrencar la base de dades amb les dades de mostra carregades:

```pwsh
...
```

Modifica el fitxer `index.ts`:

```ts
import {MongoClient} from "mongodb"

const uri = "mongodb://localhost:27017"
const client = new MongoClient(uri)

async function run() {
  try {
    const database = client.db('sample_mflix')
    const movies = database.collection('movies')
    // Query for a movie that has the title 'Back to the Future'
    const query = { title: 'Back to the Future' }
    const movie = await movies.findOne(query)
    console.log(movie)
  } finally {
    // Ensures that the client will close when you finish/error
    await client.close()
  }
}

run().catch(console.dir)
```

Al vostre shell, executeu l'ordre següent per iniciar aquesta aplicació:

```pwsh
> bun index.ts
```

La sortida inclou detalls del document de pel·lícula recuperat:

```js
{
  _id: new ObjectId('573a1398f29313caabce9682'),
  plot: "A young man is accidentally sent 30 years into the past in a time-traveling DeLorean invented by his friend, Dr. Emmett Brown, and must make sure his high-school-age parents unite in order to save his own existence.",
  genres: [ "Adventure", "Comedy", "Sci-Fi" ],
```

Després de completar aquests passos, teniu una aplicació de treball que utilitza el controlador per connectar-vos al vostre desplegament de MongoDB, executa una consulta sobre les dades de mostra i imprimeix el resultat.

## Find

Pots consultar un sol document d'una col·lecció amb el mètode `collection.findOne()`. El mètode `findOne()` utilitza un document de consulta que proporcioneu per coincidir només amb el subconjunt dels documents de la col·lecció que coincideixen amb la consulta. Si no proporcioneu un document de consulta o si proporcioneu un document buit, MongoDB coincideix amb tots els documents de la col·lecció. 

L'operació `findOne()` només retorna el primer document coincident

### Exemple

El fragment següent troba un sol document de la col·lecció `movies`. Utilitza els paràmetres següents:

* Un **document de consulta**que configura la consulta per retornar només pel·lícules amb el títol exactament del text `'The Room'`.

* Un **sort** que organitza els documents coincidents en ordre descendent per puntuació, de manera que si la nostra consulta coincideix amb diversos documents, el document retornat serà el document amb la puntuació més alta.

* Una projecció que exclou explícitament el camp  `_id` dels documents retornats i que inclou explícitament només els camps `title` i `imdb` de l' objecte (i els seus camps incrustats).

```ts
import {MongoClient} from "mongodb"

const uri = "mongodb://localhost:27017"
const client = new MongoClient(uri)

type Movie = {
  title: string
  year: number
  released: Date
  plot: string
  type: "movie" | "series"
  imdb: IMDB
}

type IMDB = {
  rating: number
  votes: number
  id: number
}


type MovieSummary = Pick<Movie, "title" | "imdb">

async function run(): Promise<void> {
  try {
    const database = client.db("sample_mflix")
    // Specifying a Schema is always optional, but it enables type hinting on
    // finds and inserts
    const movies = database.collection<Movie>("movies")
    const movie = await movies.findOne<MovieSummary>(
      {title: "The Room"},
      {
        sort: {rating: -1},
        projection: {_id: 0, title: 1, imdb: 1},
      }
    );
    console.log(movie)
  } finally {
    await client.close()
  }
}

run().catch(console.dir)
```





## TODO

* <https://www.mongodb.com/docs/drivers/node/current/usage-examples/findOne/>
* <https://www.mongodb.com/docs/drivers/node>
* <https://www.mongodb.com/resources/products/compatibilities/using-typescript-with-mongodb-tutorial>
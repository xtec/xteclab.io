---
title: React
description: React es una biblioteca basada en componentes para crear interfaces de usuario de manera declarativa.
---

{% pages ["tsx/","component/", "router/", "state/", "effect/", "forms/", "game/"] %}

#### Desktop

{% pages ["tauri/"] %}

---
title: Tauri
---


## Introducción

[Tauri](https://v2.tauri.app/) ...

## Entorn de trabajo




Crea una nueva aplicación Tauri usando `create-tauri-app`:

```pwsh
> bunx create-tauri-app tauri  
✔ Identifier · com.tauri.app
✔ Choose which language to use for your frontend · TypeScript / JavaScript - (pnpm, yarn, npm, deno, bun)
✔ Choose your package manager · bun
✔ Choose your UI template · React - (https://react.dev/)
✔ Choose your UI flavor · TypeScript

...
```

Elegiremos las siguientes opciones:

* Lenguaje de interfaz: JavaScript/TypeScript
* Gestor de paquetes: bun
* Plantilla de interfaz de usuario: React
* Estilo de interfaz de usuario: TypeScript

Ejectuta la aplicación de escritorio que viene por defecto:

```pwsh
> cd tauri
> bun install
> bun run tauri dev
```


https://medium.com/@chrisatmachine/getting-started-with-tauri-bun-shadcn-ui-e53c3c17f06f


## WebView2

* <https://github.com/oven-sh/bun/discussions/790>

* <https://dev.to/noseratio/some-thoughts-on-the-new-microsoft-teams-2-0-architecture-webview2-reactjs-1gf1>


Microsoft Teams have migrated from Electron/Angular to WebView2/React for the upcoming version 2.0, which is also a part of Windows 11. 
---
title: Componente
description: Las aplicaciones de React se construyen a partir de piezas independientes de UI llamadas componentes.
mermaid: true
plotly: true
icon: react.png
---

## Introducción

Un componente de React es una función de TypeScript a la que le puedes agregar {% link "/typescript/react/tsx/" %}.

Crea un proyecto con {% link "/typescript/bun/" %}

```pwsh
$ bun create vite react-component --template react-swc-ts
Scaffolding project in /home/box/react-component...
...
```

## Componente

Define el componente `Greeting` que es utilitzado por el componente `App`:

```tsx
export default function App() {
  return <Greeting/>
}

function Greeting() {
  return <p className="fs-3 p-4 border">Hello X</p>
}
```

**¡Los componentes de React son funciones regulares de TypeScript, pero sus nombres deben comenzar con letra mayúscula o no funcionarán!**

El componente `Greeting` està aninado dentro de el component `App`.

Puedes añadir tantos componentes `Greeting` com quieras dentro de el componente `App`:

```tsx
export default function App() {
  return (
    <>
      <Greeting />
      <Greeting />
      <Greeting />
    </>
  )
}

function Greeting() {
  return <p className="fs-3 m-2 p-2 border text-center">Hello X</p>
}
```

Al final lo que el navegador ve es esto:

```html
<p class="fs-3 m-2 p-2 border text-center">Hello X</p>
<p class="fs-3 m-2 p-2 border text-center">Hello X</p>
<p class="fs-3 m-2 p-2 border text-center">Hello X</p>
```

### Anidar y organizar componentes

Los componentes son funciones regulares de TypeScript, por lo que puedes tener múltiples componentes en el mismo archivo.

Dado que los componentes `Greeting` se renderizan dentro de `App` (¡incluso varias veces!) podemos decir que `App` es un componente padre, que renderiza cada `Greeting` como un «hijo». 

Esta es la parte mágica de React: puedes definir un componente una vez, y luego usarlo en muchos lugares y tantas veces como quieras.

{% panel "⚠ Atención!" %}
Los componentes pueden renderizar otros componentes, pero nunca debes anidar sus definiciones:

```tsx
export default function App() {
  
  // 🔴 ¡Nunca definas un componente dentro de otro componente!
  function Greeting() {
    // ...
  }
  // ...
}
```

El fragmento de código de arriba es **muy lento y causa errores**. 

En su lugar, define cada componente en el primer nivel:

```tsx
export default function App() {
  // ...
}

// ✅ Declara los componentes en el primer nivel
function Greeting() {
  // ...
}
```
{% endpanel %}

## Propiedades

Los componentes de React utilizan *props* para comunicarse entre sí. Cada componente padre puede enviar información a sus componentes hijos mediante el uso de props.

Las props son los datos que se pasan a un elemento TSX.

Por ejemplo, `className`, `src` y `alt` son algunas de las props que se pueden pasar a un elemento `<img/>`:

```tsx
export default function App() {

  return (
    <>
      <img
        className="img-fluid"
        src="https://shorturl.at/tQoUY"
        alt="Rough Collie"
      />
    </>
  )
}
```

Las props que puedes utilizar con una elemento `<img/>` están predefinidas (ReactDOM se ajusta al estándar HTML).

Sin embargo, puedes pasar cualquier prop a tus propios componentes para personalizarlos.

### Pasar props a un componente 

En este código, el componente `App` no está pasando ninguna prop a su componente hijo, `Greeting`:

```tsx
export default function App() {
  return <Greeting/>
}

function Greeting() {
  return <p className="fs-3 p-2"><span className="fs-1">🧔</span> Hello David!</p>
}
```

{% panel %}
<p class="fs-3 p-2"><span class="fs-1">🧔</span> Hello David!</p>
{% endpanel %}


Lo lógico sería que el componente padre pasara al componente hijo el nombre i el emoji.

Primero, pasa las props `name` y `emoji` al elemento `Greeting`. 

```tsx
export default function App() {
  return <Greeting name="Laura" emoji="👩‍🦰"/>
}
```

El componente `Greeting` a estas props especificando sus nombres `name` y `emoji` separados por comas dentro de `({` y `})`:

```tsx
function Greeting({name, emoji}: { name: string, emoji: string }) {
  return <p className="fs-3 p-2"><span className="fs-1">{emoji}</span> Hello {name}!</p>
}
```

Ahora el componente `Greeting` puede saludar correctamente:

{% panel %}
<p class="fs-3 p-2"><span class="fs-1">👩‍🦰</span> Hello Laura!</p>
{% endpanel %}

Las props cumplen el mismo papel que los argumentos de una función — de hecho, ¡las props son el único argumento de tu componente! 

Las funciones de los componentes de React aceptan un único argumento, un objeto `props`:

```tsx
function Greeting(props: { name: string, emoji: string }) {
  return <p className="fs-3 p-2"><span className="fs-1">{props.emoji}</span> Hello {props.name}!</p>
}
```

En general, no necesitas acceder al objeto completo de `props`, por lo que puedes desestructurarlo en props individuales;

```tsx
function Greeting({name, emoji}: { name: string, emoji: string }) {
  ...
}
```

Esta sintaxis se conoce como ["desestructuración"](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment#desempacar_campos_de_objetos_pasados_como_par%C3%A1metro_de_funci%C3%B3n).

### Asignar un valor predeterminado para una prop 

Si quieres asignar un valor predeterminado para una prop en caso de que no se especifique ningún valor, puedes hacerlo mediante la desestructuración colocando `=` seguido del valor predeterminado justo después del parámetro:

```tsx
function Greeting({name, emoji = "👽"}: { name: string, emoji?: string }) {
  return <p className="fs-3 p-2"><span className="fs-1">{emoji}</span> Hello {name}!</p>
}
```

Fíjate que ahora el tipo de props és `{ name: string, emoji?: string }`, emoji és opcional.

Ahora, si renderizas `Greeting` sin la prop `emoji`, el valor de `emoji` se establecerá automáticamente en `"👽"`.

```tsx
export default function App() {
  return <Greeting name="Armengol"/>
}
```

{% panel %}
<p class="fs-3 p-2"><span class="fs-1">👽</span> Hello Armengol!</p>
{% endpanel %}


Y ya tienes un componente completament reutilizable:

```tsx
export default function App() {
  return (
    <>
      <Greeting name="Miquel" emoji="🧙‍♂️"/>
      <Greeting name="Armengol"/>
      <Greeting name="Nuria" emoji="🐱"/>
    </>
  )
}
```

{% panel %}
<div class="container"><p class="fs-3 p-2"><span class="fs-1">🧙‍♂️</span> Hello Miquel!</p><p class="fs-3 p-2"><span class="fs-1">👽</span> Hello Armengol!</p><p class="fs-3 p-2"><span class="fs-1">🐱</span> Hello Nuria!</p></div>
{% endpanel %}

##  Renderización

Una aplicación de React está formada por muchos componentes anidados entre sí.

React, como framework de UI, es independiente de la plataforma, y una aplicación React se puede renderizar en la web, la cual utiliza marcado HTML como sus primitivas, o podría renderizarse en una plataforma móvil o de escritorio, que podría utilizar diferentes primitivas de UI 

### Estructura de árbol

Al igual que los navegadores y las plataformas móviles, React utiliza estructuras de árbol para gestionar y modelar la relación entre los componentes en una aplicación de React. 

Una característica importante de los componentes es la capacidad de componer componentes de otros componentes. Al anidar componentes, tenemos el concepto de componentes padre e hijo, donde cada componente padre puede ser a su vez un hijo de otro componente.

<pre class="mermaid">
flowchart 
  a([App])
  g1([Greeting])
  g2([Greeting])
  g3([Greeting])
  a --> g1
  a --> g2
  a --> g3
  style a fill:#00f
  style g1 fill:#080
  style g2 fill:#080
  style g3 fill:#080
</pre>

Cuando renderizamos una aplicación de React, podemos modelar esta relación en un árbol, conocido como el árbol de renderizado.

El nodo raíz en un árbol de renderizado de React es el componente raíz de la aplicación. En este caso, el componente raíz es `App` y es el primer componente que React renderiza. Cada flecha en el árbol apunta desde un componente padre hacia un componente hijo.

* El árbol de renderizado está compuesto únicamente por componentes de React.
* Un árbol de renderizado representa un único pase de renderizado de una aplicación de React.
* Con renderizado condicional, un componente padre puede renderizar diferentes hijos dependiendo de los datos pasados.
* El árbol de renderizado puede ser diferente para cada pase de renderizado.

### Componentes puros

React está diseñado con la suposición de que cada componente que escribes es una función pura.

Esto significa que un componente siempre debe devolver *el mismo TSX dadas las mismas entradas, y no cambiar cualquier objeto o variable que existiera antes de renderizar.

A continuación tienes un componente que rompe esta regla:


```tsx
let evil = "🤗"

export default function App() {
  return (
    <div className="container">
      <Greeting name="Miquel" emoji="🧙‍♂️"/>
      <Greeting name="Armengol"/>
      <Greeting name="Nuria" emoji="🐱"/>
    </div>
  )
}

function Greeting({name, emoji = "👽"}: { name: string, emoji?: string }) {
  evil =  `${evil} ${emoji}`
  return <p className="fs-4 p-2"><span className="fs-2">{evil}</span> Hello {name}!</p>
}
```

Puedes ver que los emojis aparecen duplicados:

{% panel %}
<div class="container"><p class="fs-4 p-2"><span class="fs-2">🤗 🧙‍♂️ 🧙‍♂️</span> Hello Miquel!</p><p class="fs-4 p-2"><span class="fs-2">🤗 🧙‍♂️ 🧙‍♂️ 👽 👽</span> Hello Armengol!</p><p class="fs-4 p-2"><span class="fs-2">🤗 🧙‍♂️ 🧙‍♂️ 👽 👽 🐱 🐱</span> Hello Nuria!</p></div>
{% endpanel %}

Y más importante, si cambias el orden de los `Greeting`, devuelven un TSX diferente:

```tsx
export default function App() {
  return (
    <div className="container">
      <Greeting name="Nuria" emoji="🐱"/>
      <Greeting name="Miquel" emoji="🧙‍♂️"/>
      <Greeting name="Armengol"/>
    </div>
  )
}
```

{% panel %}
<div class="container"><p class="fs-4 p-2"><span class="fs-2">🤗 🐱 🐱</span> Hello Nuria!</p><p class="fs-4 p-2"><span class="fs-2">🤗 🐱 🐱 🧙‍♂️ 🧙‍♂️</span> Hello Miquel!</p><p class="fs-4 p-2"><span class="fs-2">🤗 🐱 🐱 🧙‍♂️ 🧙‍♂️ 👽 👽</span> Hello Armengol!</p></div>
{% endpanel %}

## Importar y exportar componentes

La magia de los componentes reside en su reusabilidad: puedes crear componentes que se componen a su vez de otros componentes.

Pero mientras anidas más y más componentes, a menudo tiene sentido comenzar a separarlos en diferentes archivos.

Crea un fichero `src/Greeting.tsx`:

```tsx
export default function Greeting({name, emoji = "👽"}: { name: string, emoji?: string }) {
    return <p className="fs-4 p-2"><span className="fs-2">{emoji}</span> Hello {name}!</p>
}
```

Importa `Greeting` con un import por defecto desde `Greeting.tsx`:

```tsx
import Greeting from './Greeting.tsx'

export default function App() {
  return (
    <div className="container">
      <Greeting name="Nuria" emoji="🐱"/>
      <Greeting name="Armengol"/>
    </div>
  )
}
```

Si quieres puedes canviar el nombre del componente cuando lo utilizas:

```tsx
import Saluto from './Greeting.tsx'

export default function App() {
  return (
    <div className="container">
      <Saluto name="Nuria" emoji="🐱"/>
      <Saluto name="Armengol"/>
    </div>
  )
}
```

En lugar de utilizar un export por defecto puedes utilitzar un export por nombre (sin la palabra clave `default`):

```tsx
export function Greeting({name, emoji = "👽"}: { name: string, emoji?: string }) {
    return <p className="fs-4 p-2"><span className="fs-2">{emoji}</span> Hello {name}!</p>
}
```

Ahora tienes que importar el componente `Greeting`  usando un import con nombre (con llaves):

```tsx
import {Greeting} from './Greeting.tsx'

export default function App() {
  return (
    <div className="container">
      <Greeting name="Nuria" emoji="🐱"/>
      <Greeting name="Armengol"/>
    </div>
  )
}
```

**¡Un archivo solo puede contener un export por defecto, pero puede tener múltiples exports con nombre!**

## Librerias

Hay muchas librerías de react que puedes utilizar: [Awesome React Components](https://github.com/brillout/awesome-react-components).

 [Pigeon Maps - Docs](https://pigeon-maps.js.org/) te proporciona un componente que te permite añadir mapas sin dependencias externas.

Instala la librería `pigeon-maps`:

```pwsh
bun add pigeon-maps
```

A continuación tenemos un mapa de Barcelona (Spain) con un marcador en la Sagrada Familia.

```tsx
import {Map, Marker} from "pigeon-maps"

export default function App() {
  return (
    <div className="container p-5">
      <Map height={600} defaultCenter={[41.40369, 2.17433]} defaultZoom={14}>
        <Marker width={50} anchor={[41.40369, 2.17433]}/>
      </Map>
    </div>
  )
}
```

{% panel %}
<div class="container p-5"><div style="width: 100%; height: 600px; position: relative; display: inline-block; overflow: hidden; background: rgb(221, 221, 221); touch-action: none;" dir="ltr"><div style="height: 600px; position: absolute; top: calc(50% - 300px); left: calc(50% - 522px); overflow: hidden; will-change: transform; transform: scale(1); transform-origin: left top 0px; width: 1044px;" class="pigeon-tiles-box"><div class="pigeon-tiles" style="position: absolute; width: 1280px; height: 768px; will-change: transform; transform: translate(-234.781px, -50.5359px);"><img src="https://tile.openstreetmap.org/14/8288/6117.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 0px; top: 0px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/14/8288/6118.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 0px; top: 256px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/14/8288/6119.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 0px; top: 512px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/14/8289/6117.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 256px; top: 0px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/14/8289/6118.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 256px; top: 256px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/14/8289/6119.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 256px; top: 512px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/14/8290/6117.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 512px; top: 0px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/14/8290/6118.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 512px; top: 256px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/14/8290/6119.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 512px; top: 512px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/14/8291/6117.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 768px; top: 0px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/14/8291/6118.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 768px; top: 256px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/14/8291/6119.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 768px; top: 512px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/14/8292/6117.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 1024px; top: 0px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/14/8292/6118.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 1024px; top: 256px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/14/8292/6119.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 1024px; top: 512px; will-change: transform; transform-origin: left top 0px; opacity: 1;"></div></div><div class="pigeon-overlays" style="position: absolute; height: 600px; top: calc(50% - 300px); left: calc(50% - 522px); width: 1044px;"><div style="position: absolute; transform: translate(497px, 242.379px); pointer-events: none; cursor: pointer;" class="pigeon-click-block"><svg width="50" height="58.62068965517241" viewBox="0 0 61 71" fill="none" xmlns="http://www.w3.org/2000/svg"><g style="pointer-events: auto;"><path d="M52 31.5C52 36.8395 49.18 42.314 45.0107 47.6094C40.8672 52.872 35.619 57.678 31.1763 61.6922C30.7916 62.0398 30.2084 62.0398 29.8237 61.6922C25.381 57.678 20.1328 52.872 15.9893 47.6094C11.82 42.314 9 36.8395 9 31.5C9 18.5709 18.6801 9 30.5 9C42.3199 9 52 18.5709 52 31.5Z" fill="#93C0D0" stroke="white" stroke-width="4"></path><circle cx="30.5" cy="30.5" r="8.5" fill="white" opacity="0.6"></circle></g></svg></div></div><div class="pigeon-attribution" style="position: absolute; bottom: 0px; right: 0px; font-size: 11px; padding: 2px 5px; background: rgba(255, 255, 255, 0.7); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgb(51, 51, 51);"><span><a href="https://pigeon-maps.js.org/" style="color: rgb(0, 120, 168); text-decoration: none;" target="_blank" rel="noreferrer noopener">Pigeon</a> | </span><span> © <a href="https://www.openstreetmap.org/copyright" style="color: rgb(0, 120, 168); text-decoration: none;" target="_blank" rel="noreferrer noopener">OpenStreetMap</a> contributors</span></div></div></div>
{% endpanel %}

## Actividades

**1.-** DogCard

* Crea un componente `DogCard` con las propiedades `name`,`description` e `image?`. 
* La propiedad `image?` tiene una imagen por defecto
* Utiliza este componente para mostrar una colección de perros.

{% panel %}
<div class="container"><div class="row p-2 justify-content-evenly"><div class="col-3"><div class="card"><img class="card-img-top img-fluid" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/Rough-Collie-japan08_%28cropped%29.jpg/1200px-Rough-Collie-japan08_%28cropped%29.jpg" alt="Rough Collie"><div class="card-body"><h5 class="card-title">Rough Collie</h5><p class="card-text">A dog breed that's well-known for herding and protecting abilities, rough collie dogs are described as strong, loyal, affectionate, responsive, and fast.</p></div></div></div><div class="col-3"><div class="card"><img class="card-img-top img-fluid" src="https://d29fhpw069ctt2.cloudfront.net/icon/image/39024/preview.png" alt="Affenpinscher"><div class="card-body"><h5 class="card-title">Affenpinscher</h5><p class="card-text">Affenpinscher dogs are best known for their expressive face, with a short muzzle and dark, round eyes that give them an appearance not unlike a monkey.</p></div></div></div></div></div>
{% endpanel %}

{% sol %}
```tsx
export default function App() {

  return (
    <div className="container">
      <div className="row p-2 justify-content-evenly">
        <div className={"col-3"}>
          <DogCard
            name="Rough Collie"
            description="A dog breed that's well-known for herding and protecting abilities, rough collie dogs are described as strong, loyal, affectionate, responsive, and fast."
            image="https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/Rough-Collie-japan08_%28cropped%29.jpg/1200px-Rough-Collie-japan08_%28cropped%29.jpg"/>
        </div>
        <div className="col-3">
          <DogCard name="Affenpinscher"
                   description="Affenpinscher dogs are best known for their expressive face, with a short muzzle and dark, round eyes that give them an appearance not unlike a monkey."/>
        </div>
      </div>
    </div>
  )
}

function DogCard({name, description, image = "https://d29fhpw069ctt2.cloudfront.net/icon/image/39024/preview.png"}: {
  name: string,
  description: string,
  image?: string
}) {
  return (
    <div className="card">
      <img className="card-img-top img-fluid"
           src={image}
           alt={name}/>
      <div className="card-body">
        <h5 className="card-title">{name}</h5>
        <p className="card-text">{description}</p>
      </div>
    </div>
  )
}

```
{% endsol %}


**2.-** `Happy.tsx`

* Crea el fichero `Happy.tsx` con dos funciones con nombre: `HappyName` y `HappyImage`.
* `HappyName` tiene una propiedad `name` y muestra el nombre de forma divertida.
* `HappyImage` tiene una propiedad `image` y muestra la imagen de forma divertida.
* El componente `App` tiene que utilizar los dos componentes de `Happy.tsx`

{% panel %}
<div class="container p-5"><span class="fs-3 border border-5 border-danger rounded-pill m-2 p-2 ps-5 pe-5 fw-semibold font-monospace">Laura</span><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Laura_Dern_Deauville_2017.jpg/440px-Laura_Dern_Deauville_2017.jpg" class="img-fluid img-thumbnail w-25 p-2 border border-5 border-primary"></div>
{% endpanel %}

{% sol %}
`App.tsx`
```tsx
import {HappyName,HappyImage} from "./Happy.tsx";

export default function App() {
  return (
    <div className="container p-5">
      <HappyName name="Laura"/>
      <HappyImage image="https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Laura_Dern_Deauville_2017.jpg/440px-Laura_Dern_Deauville_2017.jpg"/>
    </div>
  )
}
```

`Happy.tsx`
```tsx
export function HappyName({name}: { name: string }) {
    return <span
        className={"fs-3 border border-5 border-danger rounded-pill m-2 p-2 ps-5 pe-5 fw-semibold font-monospace"}>{name}</span>
}

export function HappyImage({image}: { image: string }) {
    return <img src={image} className={"img-fluid img-thumbnail w-25 p-2 border border-5 border-primary"}/>
}
```
{% endsol %}

**3.-** Pigeon Maps

* Busca en Internet las coordendas geográficas y un GeoJSON de París.
* Crea un mapa de París y alrededores marcando los límites de París.

{% panel %}
<div class="container p-5"><div style="width: 100%; height: 600px; position: relative; display: inline-block; overflow: hidden; background: rgb(221, 221, 221); touch-action: none;" dir="ltr"><div style="height: 600px; position: absolute; top: calc(50% - 300px); left: calc(50% - 522px); overflow: hidden; will-change: transform; transform: scale(1); transform-origin: left top 0px; width: 1044px;" class="pigeon-tiles-box"><div class="pigeon-tiles" style="position: absolute; width: 1280px; height: 768px; will-change: transform; transform: translate(-175.376px, -6.04838px);"><img src="https://tile.openstreetmap.org/12/2072/1408.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 0px; top: 0px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/12/2072/1409.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 0px; top: 256px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/12/2072/1410.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 0px; top: 512px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/12/2073/1408.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 256px; top: 0px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/12/2073/1409.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 256px; top: 256px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/12/2073/1410.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 256px; top: 512px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/12/2074/1408.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 512px; top: 0px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/12/2074/1409.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 512px; top: 256px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/12/2074/1410.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 512px; top: 512px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/12/2075/1408.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 768px; top: 0px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/12/2075/1409.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 768px; top: 256px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/12/2075/1410.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 768px; top: 512px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/12/2076/1408.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 1024px; top: 0px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/12/2076/1409.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 1024px; top: 256px; will-change: transform; transform-origin: left top 0px; opacity: 1;"><img src="https://tile.openstreetmap.org/12/2076/1410.png" srcset="" width="256" height="256" loading="lazy" alt="" style="position: absolute; left: 1024px; top: 512px; will-change: transform; transform-origin: left top 0px; opacity: 1;"></div></div><div class="pigeon-overlays" style="position: absolute; height: 600px; top: calc(50% - 300px); left: calc(50% - 522px); width: 1044px;"><div class="pigeon-zoom-buttons pigeon-drag-block" style="position: absolute; top: 10px; left: 10px;"><button class="pigeon-zoom-in" type="button" style="width: 28px; height: 28px; border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; background: white; line-height: 26px; font-size: 20px; font-weight: 700; color: rgb(102, 102, 102); margin-bottom: 1px; cursor: pointer; border: medium; display: block; outline: none;">+</button><button class="pigeon-zoom-out" type="button" style="width: 28px; height: 28px; border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; background: white; line-height: 26px; font-size: 20px; font-weight: 700; color: rgb(102, 102, 102); margin-bottom: 1px; cursor: pointer; border: medium; display: block; outline: none;">–</button></div><div style="position: absolute; left: 0px; top: 0px; pointer-events: none; cursor: pointer;" class="pigeon-click-block"><svg width="1044" height="600" viewBox="0 0 1044 600" fill="none" xmlns="http://www.w3.org/2000/svg"><g clip-rule="evenodd" style="pointer-events: auto;"><path d=" M 496.36522951116785 217.86959368595853 519.3494328889064 223.49793702410534 538.1072924444452 229.61322904611006 528.346797511098 255.72420271101873 468.09154275548644 234.10327807394788 462.03019093337934 227.78884387854487 461.62532408884726 226.88993012846913 496.36522951116785 217.86959368595853Z" fill="#93c0d099" stroke-width="2" stroke="white" r="30"></path></g><g clip-rule="evenodd" style="pointer-events: auto;"><path d=" M 561.4963626665995 235.82576780294767 568.498520177789 242.33037046605023 574.1200526221655 256.70269558078144 579.246424177778 289.3314087697072 567.2489671111107 286.58228191471426 559.372996266582 282.9477156404173 550.6202993777115 276.32919885381125 545.3745066666743 270.3347327908268 535.3722567111254 265.4513809795026 525.9962396443589 261.8208955656737 537.8713628444821 229.71064724901225 561.4963626665995 235.82576780294767Z" fill="#93c0d099" stroke-width="2" stroke="white" r="30"></path></g><g clip-rule="evenodd" style="pointer-events: auto;"><path d=" M 530.7352206222713 373.73646821884904 512.3560135110747 369.97057197766844 486.0629703111481 361.0268651719671 503.87128604447935 313.9525027711643 508.59861617779825 298.4506514079403 509.4753422222566 297.2200182362576 516.3464277333114 300.49578926182585 525.9875015111174 306.25483147159684 539.4762666666647 311.6064898141194 557.4768213334028 321.2471164811868 572.0986311112065 337.2386546275811 568.9820302221924 339.9738755211001 559.9788401777623 359.3540288871154 530.7352206222713 373.73646821884904Z" fill="#93c0d099" stroke-width="2" stroke="white" r="30"></path></g><g clip-rule="evenodd" style="pointer-events: auto;"><path d=" M 427.82913706672844 329.0327894182992 435.31771733332425 324.60663027584087 450.11428977770265 313.7179025929072 458.6194062222494 307.97676045657136 463.33799822221044 306.45402702566935 471.89845617779065 287.0338316615671 478.48409599997103 274.4564990287181 489.0834517333424 277.5112494613859 498.092467200011 285.21434618346393 503.68487253331114 291.8680124768871 509.37339733343106 297.42364865739364 508.74716444441583 298.4196643256 504.05478684441186 313.549698538729 486.2319075556006 360.991461360245 438.08479288883973 336.7827811971074 427.82913706672844 329.0327894182992Z" fill="#93c0d099" stroke-width="2" stroke="white" r="30"></path></g><g clip-rule="evenodd" style="pointer-events: auto;"><path d=" M 407.62074737774674 333.64032718748786 401.2448227555724 327.69609867344843 350.3743231999688 278.83053445408586 364.6233059555525 262.5824169481639 380.49758151115384 255.3345753881731 433.24969244445674 253.83361827512272 471.4994147554971 272.2605944419047 478.437492622179 274.4564990287181 471.87224177783355 287.3924146286445 468.4352426666301 295.5112860259833 465.1875697778305 303.2624500467209 462.8107975110179 307.0117736244574 458.81164515553974 308.0077462026384 435.99929173337296 324.82794033229584 419.6211171555333 333.2021504834993 412.93353244441096 336.2737951910822 407.62074737774674 333.64032718748786Z" fill="#93c0d099" stroke-width="2" stroke="white" r="30"></path></g><g clip-rule="evenodd" style="pointer-events: auto;"><path d=" M 384.3743999999715 250.45532067579916 379.47813262220006 248.10863540956052 376.3615317333024 232.3497786312946 371.9924650667235 221.45208840130363 366.8660935111111 212.11268232553266 364.04367644444574 212.4138167813071 362.46789973333944 210.30143850832246 363.54560284444597 207.55133095534984 366.48744106665254 207.43176028731978 373.36143928882666 192.22804550547153 372.9827868444845 190.99239336163737 374.17699840001296 189.08354083966697 376.3615317333024 190.0136102483375 406.10031217779033 180.22997035033768 428.2369166222634 176.32790923700668 459.1145671111299 166.98217127792304 458.06307839998044 200.38583402056247 457.36985315557104 202.61788016726496 458.43299271108117 209.61059344478417 458.1213326221332 211.80711900349706 456.43487288884353 215.04872581042582 457.06110577774234 215.6067025254015 455.0629859555047 228.61247593560256 453.12020764441695 229.2412682407885 453.4347804444842 230.8929465705296 440.35379484447185 257.11003070534207 433.104056888842 253.95316390460357 384.7239253333537 255.2327408612473 384.3743999999715 250.45532067579916Z" fill="#93c0d099" stroke-width="2" stroke="white" r="30"></path></g><g clip-rule="evenodd" style="pointer-events: auto;"><path d=" M 495.20014506659936 173.35591873672092 524.0971520000603 165.516050539969 525.1166008888977 179.19799038453493 523.1912988444092 187.22339030803414 522.582542222226 194.5753227113746 520.0746979555115 201.48414525215048 519.2329244444845 209.40688230964588 519.3494328889064 223.49793702410534 496.48465066659264 217.86959368595853 454.97851733339485 228.44420726958197 457.10188373329584 215.6067025254015 456.4902143999934 215.0841529452009 458.15337244444527 211.7539775131154 458.441730844439 209.814304392552 457.4251946666045 202.46287768922048 457.9786097777542 200.19982915988658 458.99805866670795 167.11948118242435 461.2088064000709 166.22475059772842 465.72642133326735 161.66245429241098 488.8125695999479 172.20431706443196 495.20014506659936 173.35591873672092Z" fill="#93c0d099" stroke-width="2" stroke="white" r="30"></path></g><g clip-rule="evenodd" style="pointer-events: auto;"><path d=" M 580.1231502222363 164.1960906383465 581.2503694222542 167.69529598410008 584.3698830222711 170.58320610353258 584.4980423111701 188.32176679605618 586.1204223999521 190.5716500193812 585.5000149332918 193.06952509935945 603.9986431999132 217.2141990151722 592.2458538666833 223.6972069843323 566.8703146666521 236.33498456713278 564.749860977754 238.8323437537765 561.6245219554985 235.82576780294767 537.9995221333811 229.71064724901225 519.497981155524 223.32523624715395 519.1222414221847 209.08802972210106 520.1213013333036 201.08113652141765 522.4980735999998 194.32288088457426 523.3747996444581 185.70425578742288 525.1253390222555 179.19799038453493 523.9981198222376 165.4451804095297 552.3708387556253 162.45532612444367 568.6208540444495 162.9469938237453 580.1231502222363 164.1960906383465Z" fill="#93c0d099" stroke-width="2" stroke="white" r="30"></path></g><g clip-rule="evenodd" style="pointer-events: auto;"><path d=" M 622.124444444431 239.3238419807749 623.8749838222284 243.95095652807504 634.619975111098 258.3276040588971 640.9958997333888 277.7148979930207 648.499043555581 281.57975747354794 654.5895224888809 286.1085963874357 666.4646456888877 309.20733389933594 668.8414179555839 320.8089152754983 668.4045112889726 323.4912242279388 659.1857806221815 322.393517557648 625.3750300444663 314.3420270468341 612.8736739555607 312.95655690773856 588.2467015110888 304.209746761946 584.4980423111701 300.8322170578176 581.3727032889146 301.4563778004376 574.2452991999453 256.70269558078144 568.37036088889 242.4499223018065 564.749860977754 238.69950620789314 566.8703146666521 236.33498456713278 603.7481500444701 217.2141990151722 622.124444444431 239.3238419807749Z" fill="#93c0d099" stroke-width="2" stroke="white" r="30"></path></g><g clip-rule="evenodd" style="pointer-events: auto;"><path d=" M 502.30716017773375 366.7799094693037 512.1870762666222 369.9528707866557 522.3699143111007 372.333668282954 531.0002773333108 373.82054742833134 560.2438968889182 359.21683804970235 568.9820302221924 339.9738755211001 571.9792099555489 337.2386546275811 637.8093937778613 419.1744200477842 628.2236615110887 424.4083235007711 609.4104604444001 441.29931789200054 564.3479068444576 464.9569340705639 545.2987761778058 463.5900986226043 538.3111822222127 458.9012139668339 530.3565681776963 456.74252430384513 516.2881735110423 463.72722488921136 508.1646222223062 463.5900986226043 508.3102577778045 458.32615524949506 509.7374862222932 450.5804113507038 507.8413112888811 446.61675070854835 503.41690311115235 441.38337148248684 500.6207004444441 431.6639109089738 501.17411555547733 419.4973948437255 500.18379377771635 397.28182610461954 499.6012515554903 394.9897925091209 502.30716017773375 366.7799094693037Z" fill="#93c0d099" stroke-width="2" stroke="white" r="30"></path></g><g clip-rule="evenodd" style="pointer-events: auto;"><path d=" M 384.1239068445284 423.83317531784996 441.1198378666304 352.7023882851936 442.6198840888683 354.9461775840027 451.4949148443993 343.19590473070275 485.87073137785774 360.70380517991725 502.4993991111405 366.9480731335934 499.62164053332526 394.8349245195277 500.7488597333431 408.9451666454552 501.12168675556313 423.07220773678273 500.7488597333431 433.451226480538 503.6207928889198 442.0779183532577 508.1238442666363 447.32455244835 509.62389048887417 450.6998509176192 508.4966712888563 458.578296563006 508.2461781332968 463.3202692697523 483.8726115556201 461.9534237332409 475.62381368887145 454.5793900163844 384.1239068445284 423.83317531784996Z" fill="#93c0d099" stroke-width="2" stroke="white" r="30"></path></g><g clip-rule="evenodd" style="pointer-events: auto;"><path d=" M 357.6211484443629 414.701391960145 312.8702549333684 386.9542134302901 304.8719502222957 401.7065343565773 307.12056319997646 404.45865834289 300.6252174222609 408.2062706355937 294.1211335111875 411.9493951208424 286.1228287999984 411.697200775845 284.6227825777605 398.7021670775721 294.3716266666306 387.71530499320943 290.12198115559295 379.9670937808696 281.37510968884453 382.7017874850426 275.3720120888902 380.70608391729183 316.37424639996607 319.45889601367526 343.8702392888954 289.9556019039592 350.6248163555283 278.45865625777515 406.87218062218744 333.4544341461733 412.8752782222582 335.9551250967197 427.86991502228193 329.20098172250437 438.12265813327394 336.4596858678851 451.37258097785525 343.4481770186103 442.8703772444278 354.711620791466 440.87517013330944 352.1934388343361 384.1239068445284 423.46154031390324 357.6211484443629 414.701391960145Z" fill="#93c0d099" stroke-width="2" stroke="white" r="30"></path></g><g clip-rule="evenodd" style="pointer-events: auto;"><path d=" M 321.80936533329077 189.82759667898063 365.118466844433 209.46002436295385 372.2487836444052 221.26610122609418 377.06058239995036 233.51435177854728 379.5975537778577 247.4976076205494 384.43556693335995 250.5748681278783 385.10257777781226 255.9455815665424 362.61644800007343 263.4767584858928 347.61598577781115 281.49564326537075 339.11086933338083 295.96724435535725 314.1168952889275 324.00023958901875 275.1186062222114 380.94503828464076 257.1151388444705 378.4802558622323 245.60992995556444 362.46071479679085 248.11777422227897 335.4638411368942 235.09795555553865 334.97255608811975 210.1039815110853 322.4953212275286 168.10560000000987 321.4684298920911 164.11227306665387 290.9693586381036 180.6011306666769 241.95843100908678 203.1163875554921 224.47657221823465 224.61219555558637 198.47706243395805 249.10809600003995 207.95432791661005 258.1054606222315 179.9597958024242 321.80936533329077 189.82759667898063Z" fill="#93c0d099" stroke-width="2" stroke="white" r="30"></path></g><g clip-rule="evenodd" style="pointer-events: auto;"><path d=" M 321.5763484444469 189.5220026188763 331.0717866666382 169.98968163738027 356.12110222224146 144.96719227946596 373.1022080000257 139.5273392252857 397.8748159999959 115.32546152186114 420.8735829333309 101.82123256655177 443.5898168889107 90.95256575406529 467.6196835555602 90.12399079697207 454.48335644439794 148.64831997989677 459.1145671111299 166.98217127792304 429.1136426667217 176.4962174911052 405.1129031111486 180.4824282367481 374.238165333285 190.45207018672954 365.3543964443961 209.23859905672725 321.5763484444469 189.5220026188763Z" fill="#93c0d099" stroke-width="2" stroke="white" r="30"></path></g><g clip-rule="evenodd" style="pointer-events: auto;"><path d=" M 584.8533930666745 90.47846174624283 584.8533930666745 108.24559115117881 589.1088639999507 114.22672300884733 584.5912490667542 118.21405451372266 575.2472718222998 148.63503082020907 573.9336391111137 153.01155096007278 568.37036088889 162.72552202211227 551.0950712888734 162.72552202211227 524.0971520000603 165.9722764621256 495.6079246222507 173.98043867840897 488.369837511098 172.13787832378875 465.87205688888207 162.64136267954018 461.1825919999974 166.64554018422496 459.06213831109926 167.13719858322293 454.433840355603 148.51542834751308 467.85270044440404 89.99106404476333 534.8421432888135 87.99715318635572 584.8533930666745 90.47846174624283Z" fill="#93c0d099" stroke-width="2" stroke="white" r="30"></path></g><g clip-rule="evenodd" style="pointer-events: auto;"><path d=" M 696.6141184000298 191.75415822205832 677.8562588444911 199.98282329179347 657.3099946666043 203.26003213529475 642.6153671110515 201.975726337987 635.3598037332995 206.48404974717414 631.8645503999433 205.96147864998784 626.6245831110282 209.46002436295385 603.6112526222132 216.72265173960477 584.8533930666745 190.72223193896934 584.3611448887968 169.98968163738027 580.6037475555204 167.4738289298257 579.8726570666768 163.96133184537757 568.4344405333977 162.76095752522815 574.3734584888443 152.63946151797427 578.9376767999493 137.07315983169246 584.5912490667542 117.99696831282927 588.8467200000305 113.73494715051493 584.5621219555615 108.51585318619618 584.8533930666745 90.20817769877613 603.8471822222928 92.47235026443377 637.6055039999774 90.73545282974374 647.8553343999665 94.47065163450316 653.1011271111201 101.466779771843 663.1208533333847 156.2185816700803 676.6037930666935 174.46765159466304 690.354702222161 181.73585523676593 696.6141184000298 191.75415822205832Z" fill="#93c0d099" stroke-width="2" stroke="white" r="30"></path></g><g clip-rule="evenodd" style="pointer-events: auto;"><path d=" M 440.35379484447185 257.3801111902576 453.3444864000194 231.2516203983687 453.1114695111755 229.12170927785337 461.46803768887185 226.77479805459734 468.12066986667924 234.1387021620758 528.2448526222724 255.74191302765394 517.7794816000387 283.5586456861929 516.4949760000454 284.23155219329055 513.7453767111292 291.31022776453756 509.55981084448285 297.33511371462373 503.3586488888832 291.8680124768871 497.7342037333874 284.9929965659976 488.3581866667373 277.6130737506319 471.4906766222557 272.24288544547744 440.35379484447185 257.3801111902576Z" fill="#93c0d099" stroke-width="2" stroke="white" r="30"></path></g><g clip-rule="evenodd" style="pointer-events: auto;"><path d=" M 568.1664711111225 332.84806766558904 557.2408917333232 321.11432832852006 539.0976142222062 311.3364754085778 525.8739057778148 306.3389362829039 516.6114844444674 300.49578926182585 509.62389048887417 297.2687124844524 513.7453767111292 291.20840976864565 516.7163420444122 284.2669682686683 517.715401955531 283.45682412886526 525.9875015111174 261.8386053950526 535.1042872888502 265.33626894047484 545.243434666656 270.1133683102089 550.483401955571 276.3646161921206 559.4865920000011 282.96542378130835 567.231490844395 286.7327985992306 578.9726293332642 289.11449017660925 581.4193066667067 302.65600095828995 572.9695317333099 328.55919461289886 568.1664711111225 332.84806766558904Z" fill="#93c0d099" stroke-width="2" stroke="white" r="30"></path></g><g clip-rule="evenodd" style="pointer-events: auto;"><path d=" M 713.3622072888538 329.21868616814027 707.5979519999819 374.71444020961644 699.1190499555087 384.7107480970444 712.1126542222919 387.2418355490081 735.6182328889845 377.7191325924359 730.1102961777942 356.7518140176544 727.3432206221623 350.2372918067267 729.3529912888771 343.46588032634463 734.5987839999143 339.2303236674052 742.3465955555439 351.48533330648206 782.8594943999778 356.7518140176544 785.3673386666924 339.2303236674052 790.1150577778462 339.70389567222446 789.8529137778096 332.9808487885748 806.8631466666702 333.23755873413756 806.1058417777531 337.49093369586626 832.841617066646 341.2219748108764 855.6219306666171 349.47607155528385 874.3506631111959 374.71444020961644 872.3700195555575 385.2505978452391 861.094914844376 396.98094273672905 858.3598791111726 414.2279801456025 863.3697422222467 415.73227587202564 859.8424490666948 434.71649562579114 853.3471032888629 450.24421074270504 842.6021119999932 460.45386505452916 828.0938979556086 461.21470435359515 812.6011875554686 455.99936398339923 780.3574755555019 454.7519107032567 779.8623146666214 449.48333380022086 764.1016348444391 436.22065234853653 751.6090168888913 429.4872596170753 735.0910321777919 429.21738876064774 728.59568639996 429.7084649921162 721.6080924443668 427.4964013104327 703.3745208889013 425.7223102899734 690.6168462221976 416.96225126390345 681.3515121778473 406.70192384318216 674.3639182221377 404.98076161032077 658.3731342222309 412.23698481335305 644.0979370666901 416.00216314313 637.867648000014 419.2275118273683 567.8431601778138 332.22399532195413 573.3481841777684 328.23608729580883 581.3610524444375 301.2394710750086 584.8533930666745 301.2394710750086 588.2467015110888 304.3956643335405 613.3717475555604 313.4655964637059 625.8643655111082 314.7448298392119 659.3634559999919 322.4953212275286 713.3622072888538 329.21868616814027Z" fill="#93c0d099" stroke-width="2" stroke="white" r="30"></path></g><g clip-rule="evenodd" style="pointer-events: auto;"><path d=" M 709.8698666666169 247.4976076205494 712.3165440000594 275.0276077447925 711.8505102222553 298.23374174069613 713.87484444445 303.70511256717145 715.6079075555317 310.5441361553967 713.1029760000529 329.24966894462705 668.3346062222263 323.50892912974814 669.0919111111434 320.45481265807757 666.3248355555115 309.5127629401395 654.8487537777983 286.22369763668394 640.9842488889117 277.59536518191453 634.3432675555814 257.7166044486803 623.0710755555192 243.5126024512574 622.1681351111038 239.204288458277 603.6141653333325 216.66951142833568 627.3527608888689 209.01717352925334 631.8383359999862 205.77547847037204 635.8578773332993 206.48404974717414 643.0814008888556 202.00672691036016 657.4119395555463 203.47703479335178 677.5679004443809 200.19982915988658 696.1218702222686 191.78516022482654 703.0832497777883 207.46276009629946 710.1028835555771 221.72221235383768 709.8698666666169 247.4976076205494Z" fill="#93c0d099" stroke-width="2" stroke="white" r="30"></path></g></svg></div></div><div class="pigeon-attribution" style="position: absolute; bottom: 0px; right: 0px; font-size: 11px; padding: 2px 5px; background: rgba(255, 255, 255, 0.7); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgb(51, 51, 51);"><span><a href="https://pigeon-maps.js.org/" style="color: rgb(0, 120, 168); text-decoration: none;" target="_blank" rel="noreferrer noopener">Pigeon</a> | </span><span> © <a href="https://www.openstreetmap.org/copyright" style="color: rgb(0, 120, 168); text-decoration: none;" target="_blank" rel="noreferrer noopener">OpenStreetMap</a> contributors</span></div></div></div>
{% endpanel %}

{% sol %}
```tsx
import {GeoJson,Map, ZoomControl} from "pigeon-maps"

export default function App() {

  const data = {"type":"FeatureCollection","features":[{"type":"Feature","properties":{"name":"Bourse","cartodb_id":2,"created_at":"2013-02-26T07:07:16.384Z","updated_at":"2013-02-26T18:36:18.682Z"},"geometry":{"type":"MultiPolygon","coordinates":[[[[2.339999,48.87196],[2.34789,48.870689],[2.35433,48.869308],[2.350979,48.863411],[2.330292,48.868294],[2.328211,48.86972],[2.328072,48.869923],[2.339999,48.87196]]]]}},{"type":"Feature","properties":{"name":"Temple","cartodb_id":3,"created_at":"2013-02-26T07:07:16.384Z","updated_at":"2013-02-26T18:36:24.060Z"},"geometry":{"type":"MultiPolygon","coordinates":[[[[2.36236,48.867905],[2.364764,48.866436],[2.366694,48.86319],[2.368454,48.85582],[2.364335,48.856441],[2.361631,48.857262],[2.358626,48.858757],[2.356825,48.860111],[2.353391,48.861214],[2.350172,48.862034],[2.354249,48.869286],[2.36236,48.867905]]]]}},{"type":"Feature","properties":{"name":"Panthéon","cartodb_id":5,"created_at":"2013-02-26T07:07:16.384Z","updated_at":"2013-02-26T18:36:34.699Z"},"geometry":{"type":"MultiPolygon","coordinates":[[[[2.351799,48.83675],[2.345489,48.837601],[2.336462,48.839622],[2.342576,48.850258],[2.344199,48.85376],[2.3445,48.854038],[2.346859,48.853298],[2.350169,48.851997],[2.3548,48.850788],[2.36098,48.84861],[2.366,48.844997],[2.36493,48.844379],[2.361839,48.84],[2.351799,48.83675]]]]}},{"type":"Feature","properties":{"name":"Luxembourg","cartodb_id":6,"created_at":"2013-02-26T07:07:16.384Z","updated_at":"2013-02-26T18:36:40.774Z"},"geometry":{"type":"MultiPolygon","coordinates":[[[[2.316469,48.846851],[2.31904,48.847851],[2.32412,48.850311],[2.32704,48.851608],[2.32866,48.851952],[2.331599,48.856339],[2.33386,48.85918],[2.337499,48.85849],[2.340592,48.85675],[2.342512,48.855247],[2.344465,48.853992],[2.34425,48.853767],[2.342639,48.850349],[2.33652,48.83963],[2.31999,48.8451],[2.316469,48.846851]]]]}},{"type":"Feature","properties":{"name":"Palais-Bourbon","cartodb_id":7,"created_at":"2013-02-26T07:07:16.384Z","updated_at":"2013-02-26T18:36:45.091Z"},"geometry":{"type":"MultiPolygon","coordinates":[[[[2.309531,48.84581],[2.307342,48.847153],[2.289877,48.858192],[2.294769,48.861862],[2.300219,48.863499],[2.31833,48.863838],[2.331462,48.859676],[2.333844,48.85918],[2.33159,48.856258],[2.33041,48.854424],[2.329295,48.852673],[2.328479,48.851826],[2.327106,48.851601],[2.319274,48.847801],[2.313651,48.845909],[2.311355,48.845215],[2.309531,48.84581]]]]}},{"type":"Feature","properties":{"name":"Élysée","cartodb_id":8,"created_at":"2013-02-26T07:07:16.384Z","updated_at":"2013-02-26T18:36:51.084Z"},"geometry":{"type":"MultiPolygon","coordinates":[[[[2.30155,48.864601],[2.299869,48.865131],[2.298799,48.86869],[2.297299,48.871151],[2.295539,48.87326],[2.29457,48.873192],[2.294029,48.873669],[2.294399,48.87429],[2.295409,48.874317],[2.297769,48.87775],[2.297639,48.878029],[2.298049,48.87846],[2.298799,48.87825],[2.309009,48.880459],[2.316609,48.88134],[2.32721,48.88345],[2.326849,48.875908],[2.326611,48.875404],[2.326976,48.873825],[2.326869,48.873329],[2.32629,48.872597],[2.326505,48.872471],[2.325819,48.869534],[2.325152,48.869392],[2.32526,48.869019],[2.320769,48.863098],[2.31828,48.863811],[2.30167,48.863522],[2.30155,48.864601]]]]}},{"type":"Feature","properties":{"name":"Opéra","cartodb_id":9,"created_at":"2013-02-26T07:07:16.384Z","updated_at":"2013-02-26T18:37:00.221Z"},"geometry":{"type":"MultiPolygon","coordinates":[[[[2.339599,48.882011],[2.34952,48.883781],[2.34987,48.880692],[2.349209,48.87888],[2.349,48.87722],[2.348139,48.87566],[2.34785,48.873871],[2.34789,48.870689],[2.34004,48.87196],[2.32579,48.869572],[2.326519,48.872471],[2.326309,48.872589],[2.32688,48.873341],[2.326979,48.873779],[2.32663,48.875439],[2.32682,48.87595],[2.32717,48.883419],[2.327929,48.883621],[2.32948,48.884651],[2.337406,48.882271],[2.339599,48.882011]]]]}},{"type":"Feature","properties":{"name":"Enclos-St-Laurent","cartodb_id":10,"created_at":"2013-02-26T07:07:16.384Z","updated_at":"2013-02-26T18:37:05.756Z"},"geometry":{"type":"MultiPolygon","coordinates":[[[[2.368755,48.884079],[2.369142,48.883289],[2.370213,48.882637],[2.370257,48.878632],[2.370814,48.878124],[2.370601,48.87756],[2.376952,48.872108],[2.372917,48.870644],[2.364205,48.86779],[2.363477,48.867226],[2.362404,48.867905],[2.354293,48.869286],[2.347941,48.870728],[2.347812,48.873943],[2.348155,48.875751],[2.348971,48.877277],[2.349272,48.879223],[2.349873,48.880692],[2.349486,48.883797],[2.359227,48.884472],[2.364806,48.884361],[2.368755,48.884079]]]]}},{"type":"Feature","properties":{"name":"Popincourt","cartodb_id":11,"created_at":"2013-02-26T07:07:16.384Z","updated_at":"2013-02-26T18:37:14.753Z"},"geometry":{"type":"MultiPolygon","coordinates":[[[[2.383175,48.867115],[2.383776,48.86607],[2.387465,48.862823],[2.389654,48.858444],[2.39223,48.857571],[2.394321,48.856548],[2.398398,48.85133],[2.399214,48.848709],[2.399064,48.848103],[2.395899,48.848351],[2.384291,48.85017],[2.379999,48.850483],[2.371544,48.852459],[2.370257,48.853222],[2.369184,48.853081],[2.366737,48.86319],[2.36472,48.866409],[2.363477,48.867256],[2.364205,48.86779],[2.376866,48.872108],[2.383175,48.867115]]]]}},{"type":"Feature","properties":{"name":"Gobelins","cartodb_id":13,"created_at":"2013-02-26T07:07:16.384Z","updated_at":"2013-02-26T18:37:25.726Z"},"geometry":{"type":"MultiPolygon","coordinates":[[[[2.342039,48.838322],[2.345431,48.837605],[2.348927,48.837067],[2.35189,48.836731],[2.36193,48.840031],[2.36493,48.844379],[2.365959,48.844997],[2.38856,48.826481],[2.385269,48.825298],[2.37881,48.82148],[2.363339,48.816132],[2.356799,48.816441],[2.3544,48.817501],[2.351669,48.817989],[2.346839,48.81641],[2.34405,48.816441],[2.3441,48.817631],[2.34459,48.819382],[2.343939,48.820278],[2.34242,48.821461],[2.34146,48.823658],[2.34165,48.826408],[2.34131,48.831429],[2.34111,48.831947],[2.342039,48.838322]]]]}},{"type":"Feature","properties":{"name":"Observatoire","cartodb_id":14,"created_at":"2013-02-26T07:07:16.384Z","updated_at":"2013-02-26T18:37:30.531Z"},"geometry":{"type":"MultiPolygon","coordinates":[[[[2.301464,48.825428],[2.321032,48.841503],[2.321547,48.840996],[2.324594,48.843651],[2.336396,48.839695],[2.342105,48.838284],[2.341117,48.831982],[2.341504,48.828793],[2.341632,48.8256],[2.341504,48.823254],[2.34249,48.821304],[2.344036,48.820118],[2.344551,48.819355],[2.344164,48.817574],[2.344078,48.816502],[2.33571,48.816811],[2.332878,48.818478],[2.301464,48.825428]]]]}},{"type":"Feature","properties":{"name":"Vaugirard","cartodb_id":15,"created_at":"2013-02-26T07:07:16.384Z","updated_at":"2013-02-26T18:37:35.322Z"},"geometry":{"type":"MultiPolygon","coordinates":[[[[2.292365,48.827492],[2.277001,48.833763],[2.274255,48.830429],[2.275027,48.829807],[2.272797,48.82896],[2.270564,48.828114],[2.267818,48.828171],[2.267303,48.831108],[2.27065,48.833591],[2.269191,48.835342],[2.266188,48.834724],[2.264127,48.835175],[2.278204,48.849014],[2.287644,48.855679],[2.289963,48.858276],[2.309274,48.845852],[2.311335,48.845287],[2.316483,48.846813],[2.320003,48.845173],[2.324552,48.843594],[2.321633,48.841049],[2.320948,48.841618],[2.301464,48.825512],[2.292365,48.827492]]]]}},{"type":"Feature","properties":{"name":"Passy","cartodb_id":16,"created_at":"2013-02-26T07:07:16.384Z","updated_at":"2013-02-26T18:37:40.880Z"},"geometry":{"type":"MultiPolygon","coordinates":[[[[2.28007,48.878292],[2.294939,48.873859],[2.297387,48.871193],[2.299039,48.868427],[2.29991,48.865269],[2.301571,48.864574],[2.3018,48.863361],[2.29408,48.86166],[2.28893,48.85759],[2.28601,48.854321],[2.277429,48.847988],[2.26404,48.835121],[2.257859,48.835678],[2.253909,48.839298],[2.25477,48.845398],[2.2503,48.845509],[2.241719,48.848328],[2.2273,48.84856],[2.225929,48.85545],[2.23159,48.86652],[2.23932,48.870468],[2.2467,48.876339],[2.25511,48.874199],[2.258199,48.88052],[2.28007,48.878292]]]]}},{"type":"Feature","properties":{"name":"Batignolles-Monceau","cartodb_id":17,"created_at":"2013-02-26T07:07:16.384Z","updated_at":"2013-02-26T18:37:46.239Z"},"geometry":{"type":"MultiPolygon","coordinates":[[[[2.27999,48.878361],[2.28325,48.882771],[2.29185,48.88842],[2.29768,48.889648],[2.306185,48.895111],[2.314081,48.898159],[2.32188,48.900612],[2.33013,48.900799],[2.32562,48.887589],[2.32721,48.88345],[2.31691,48.881302],[2.30867,48.880402],[2.29807,48.878151],[2.29502,48.873909],[2.27999,48.878361]]]]}},{"type":"Feature","properties":{"name":"Butte-Montmartre","cartodb_id":18,"created_at":"2013-02-26T07:07:16.384Z","updated_at":"2013-02-26T18:37:52.398Z"},"geometry":{"type":"MultiPolygon","coordinates":[[[[2.370379,48.900719],[2.370379,48.896709],[2.37184,48.895359],[2.370289,48.894459],[2.367081,48.887592],[2.36663,48.886604],[2.36472,48.884411],[2.358789,48.884411],[2.34952,48.883678],[2.339739,48.88187],[2.337254,48.882286],[2.32953,48.88443],[2.32792,48.883526],[2.327192,48.883415],[2.325603,48.887619],[2.33021,48.900829],[2.353209,48.901279],[2.370379,48.900719]]]]}},{"type":"Feature","properties":{"name":"Buttes-Chaumont","cartodb_id":19,"created_at":"2013-02-26T07:07:16.384Z","updated_at":"2013-02-26T18:37:58.058Z"},"geometry":{"type":"MultiPolygon","coordinates":[[[[2.408749,48.877857],[2.402309,48.875999],[2.395255,48.875259],[2.39021,48.875549],[2.387719,48.874531],[2.386519,48.874649],[2.38472,48.873859],[2.376819,48.872219],[2.370379,48.87809],[2.37021,48.882771],[2.36892,48.883339],[2.368669,48.884132],[2.364742,48.884403],[2.366781,48.886688],[2.368348,48.890202],[2.370289,48.894508],[2.37175,48.89547],[2.370279,48.896648],[2.370379,48.90078],[2.3769,48.900269],[2.38849,48.900661],[2.392009,48.899818],[2.39381,48.898239],[2.39725,48.88588],[2.401879,48.88176],[2.4066,48.880119],[2.408749,48.877857]]]]}},{"type":"Feature","properties":{"name":"Louvre","cartodb_id":1,"created_at":"2013-02-26T07:07:16.384Z","updated_at":"2013-02-26T18:36:13.177Z"},"geometry":{"type":"MultiPolygon","coordinates":[[[[2.320769,48.863037],[2.325229,48.868938],[2.325149,48.869419],[2.328018,48.869949],[2.330302,48.868286],[2.350944,48.863407],[2.347351,48.857124],[2.34691,48.856972],[2.345966,48.855373],[2.344529,48.854012],[2.3424,48.855247],[2.340469,48.8568],[2.33725,48.858467],[2.331459,48.85968],[2.320769,48.863037]]]]}},{"type":"Feature","properties":{"name":"Hôtel-de-Ville","cartodb_id":4,"created_at":"2013-02-26T07:07:16.384Z","updated_at":"2013-02-26T18:36:29.259Z"},"geometry":{"type":"MultiPolygon","coordinates":[[[[2.36465,48.845989],[2.360899,48.84864],[2.35467,48.850849],[2.35013,48.851978],[2.34695,48.853298],[2.344551,48.854027],[2.345966,48.855396],[2.346986,48.856964],[2.347329,48.857147],[2.350169,48.86203],[2.353299,48.86124],[2.35678,48.860161],[2.358579,48.858749],[2.36167,48.857258],[2.364329,48.856407],[2.36836,48.855869],[2.3692,48.85281],[2.366299,48.846958],[2.36465,48.845989]]]]}},{"type":"Feature","properties":{"name":"Reuilly","cartodb_id":12,"created_at":"2013-02-26T07:07:16.384Z","updated_at":"2013-02-26T18:37:20.571Z"},"geometry":{"type":"MultiPolygon","coordinates":[[[[2.414499,48.846809],[2.41252,48.836529],[2.409609,48.83427],[2.41407,48.833698],[2.42214,48.83585],[2.420249,48.840588],[2.419299,48.84206],[2.419989,48.84359],[2.42179,48.844547],[2.42445,48.841778],[2.438359,48.840588],[2.43922,48.844547],[2.44085,48.84444],[2.44076,48.845959],[2.4466,48.845901],[2.44634,48.84494],[2.455519,48.844097],[2.46334,48.842232],[2.46977,48.836529],[2.46909,48.834148],[2.465219,48.831497],[2.46428,48.827599],[2.466,48.827259],[2.464789,48.822968],[2.462559,48.819458],[2.45887,48.81715],[2.453889,48.816978],[2.44857,48.818157],[2.4375,48.818439],[2.43733,48.81963],[2.431919,48.822628],[2.42763,48.82415],[2.421959,48.824211],[2.419729,48.8241],[2.41733,48.8246],[2.41107,48.825001],[2.40669,48.826981],[2.403509,48.8293],[2.40111,48.829689],[2.39562,48.828049],[2.390719,48.827198],[2.38858,48.826469],[2.364539,48.84613],[2.366429,48.847031],[2.36918,48.85313],[2.370379,48.85313],[2.371544,48.852417],[2.38017,48.850368],[2.384459,48.850079],[2.39596,48.848328],[2.414499,48.846809]]]]}},{"type":"Feature","properties":{"name":"Ménilmontant","cartodb_id":20,"created_at":"2013-02-26T07:07:16.384Z","updated_at":"2013-02-26T18:38:05.143Z"},"geometry":{"type":"MultiPolygon","coordinates":[[[[2.4133,48.865269],[2.41414,48.859051],[2.41398,48.853809],[2.414675,48.852573],[2.41527,48.851028],[2.41441,48.846802],[2.39904,48.848099],[2.3993,48.848789],[2.39835,48.851261],[2.39441,48.856522],[2.38965,48.858471],[2.38737,48.862961],[2.3835,48.866169],[2.38319,48.867142],[2.37682,48.872231],[2.38497,48.873959],[2.38651,48.874691],[2.38789,48.874531],[2.39037,48.875542],[2.39529,48.87521],[2.40221,48.87595],[2.40858,48.87785],[2.41097,48.87431],[2.41338,48.87109],[2.4133,48.865269]]]]}}]}

  return (
    <div className="container p-5">
      <Map height={600} defaultCenter={[48.85341, 2.3488]} defaultZoom={12}>
        <ZoomControl/>
        <GeoJson data={data}/>
      </Map>
    </div>
  )
}
```
{% endsol %}


**4.-** Plotly

[Plotly](https://plotly.com/javascript/) es una librería que te permite crear gráficos interactivos.

En la actividad {% link "/typescript/plotly" %} aprenderás a utilizar la librería.

Importa plotly:

```sh
$ bun add plotly.js react-plotly.js
$ bun add --dev @types/plotly.js @types/react-plotly.js
```

Crea un gráfico de lineas:

```tsx
import Plot from 'react-plotly.js'

export default function App() {

  const data = [
    {
      x: [1, 2, 3, 4],
      y: [100, 35, 200, 90],
      mode: "lines",
    },
  ];

  const layout = { title: "Chart Title" };

  return <Plot data={data} layout={layout} />;
}
```

{% panel %}
<div id="plot-lines"></div>
<script>
Plotly.newPlot("plot-lines", [
    {
        x: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        y: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        mode: "lines",
    },
], { title: "Chart Title" })
</script>
{% endpanel %}

Modifica el gráfico.




## TODO

* <https://evilmartians.com/chronicles/how-to-build-a-better-react-map-with-pigeon-maps-and-mapbox>
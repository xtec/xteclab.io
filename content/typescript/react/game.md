---
title: Tres en ratlla
---

## Introducció

Tres en ratlla és un tutorial que no assumeix cap coneixement previ de React.


### Component

El codi en `App.tsx` crea un component. 

En React, un componente es una pieza de código reutilizable que representa una parte de una interfaz de usuario. 

Los componentes se utilizan para representar, administrar y actualizar los elementos de la interfaz de usuario en su aplicación.

```tsx
export default function App() {
    ...
}
```

La primera línea:

* Define una función llamada `App`. 
* La palabra clave de JavaScript `export` hace que esta función sea accesible fuera de este archivo. 
* La palabra clave `default` le dice a otros archivos que usan su código que es la función principal en su archivo.


La segunda línea devuelve un botón:

```tsx
export default function App() {

  return (
      <div className="m-5">
        <button className="btn btn-lg btn-outline-secondary p-2 ps-5 pe-5 fs-1 fw-bold font-monospace">X</button>
      </div>
  )
}
```

La palabra clave `return` significa que lo que viene después se devuelve como un valor a la persona que llama a la función.

`<div>` i `<button>` són elementos JSX: 

* Un elemento JSX es una combinación de código TypeScript y etiquetas HTML que describe lo que te gustaría mostrar. 
* `className="p-3 fs-1 fw-bold"` es una propiedad de botón o `prop` que le dice a CSS cómo diseñar el botón. 
* `X` es el texto que se muestra dentro del botón y `</button>` cierra el elemento JSX para indicar que ningún contenido siguiente debe colocarse dentro del botón.

### Construir el tablero

Volvamos a `App.tsx`. 

Aquí es donde pasarás el resto de la actividad:

Actualmente, el tablero es solo un cuadrado, ¡pero necesitas nueve! 

```tsx
export default function App() {

  return (
    <div className="m-5">
      <div>
          <button className="btn btn-lg btn-outline-secondary p-2 ps-5 pe-5 fs-1 fw-bold font-monospace">1</button>
          <button className="btn btn-lg btn-outline-secondary p-2 ps-5 pe-5 fs-1 fw-bold font-monospace">2</button>
          <button className="btn btn-lg btn-outline-secondary p-2 ps-5 pe-5 fs-1 fw-bold font-monospace">3</button>
      </div>
      <div>
          <button className="btn btn-lg btn-outline-secondary p-2 ps-5 pe-5 fs-1 fw-bold font-monospace">4</button>
          <button className="btn btn-lg btn-outline-secondary p-2 ps-5 pe-5 fs-1 fw-bold font-monospace">5</button>
          <button className="btn btn-lg btn-outline-secondary p-2 ps-5 pe-5 fs-1 fw-bold font-monospace">6</button>
      </div>
      <div>
          <button className="btn btn-lg btn-outline-secondary p-2 ps-5 pe-5 fs-1 fw-bold font-monospace">7</button>
          <button className="btn btn-lg btn-outline-secondary p-2 ps-5 pe-5 fs-1 fw-bold font-monospace">8</button>
          <button className="btn btn-lg btn-outline-secondary p-2 ps-5 pe-5 fs-1 fw-bold font-monospace">9</button>
      </div>
    </div>
  )
}
```

Ara el tauler és veurà complet.


### Pasar datos a través de props

A continuación, querrás cambiar el valor de un cuadrado de vacío a «X» cuando el usuario haga clic en el cuadrado. 

Con la forma en que ha construido el tablero hasta ahora, necesitarás copiar y pegar el código que actualiza el cuadrado nueve veces (¡una vez por cada cuadrado que tengas)! 

En lugar de copiar y pegar, la arquitectura de componentes de React te permite crear un componente reutilizable para evitar el código duplicado desordenado.

Primero, ve a copiar la línea que define el primer cuadrado (`<button className="...">1</button>`) de tu componente `App` en un nuevo componente `Square`:

```tsx
function Square() {
  return <button className="btn btn-lg btn-outline-secondary p-2 ps-5 pe-5 fs-1 fw-bold font-monospace">X</button>
}
```

Luego, actualiza el componente `App` para renderizar ese componente `Square` usando la sintaxis JSX:

```tsx
export default function App() {

  function Square() {
    return <button className="btn btn-lg btn-outline-secondary p-2 ps-5 pe-5 fs-1 fw-bold font-monospace">X</button>
  }

  return (
    <div className="m-5">
      <div>
        <Square />
        <Square />
        <Square />
      </div>
      <div>
        <Square />
        <Square />
        <Square />
      </div>
      <div>
        <Square />
        <Square />
        <Square />
      </div>
    </div>
  )
}
```

Observa cómo, a diferencia de los divs del navegador, tus propios componentes `App` y `Square` deben comenzar con una letra mayúscula.


¡Oh, no! Perdiste los cuadrados numerados que tenías antes. Ahora cada cuadrado dice «1». Para arreglar esto, utiliza las *props* para pasar el valor que debe tener cada cuadrado del componente principal (`App`) al componente secundario (`Square`).

Actualiza el componente `Square` para leer la propiedad value que pasarás desde `App`:

```tsx
function Square({value}:{value:string}) {
  return <button className="btn btn-lg btn-outline-secondary p-2 ps-5 pe-5 fs-1 fw-bold font-monospace">{value}</button>
}
```


Afegix la propietat `value` a tots els components `Square`:

```ts
export default function App() {

  function Square({value}:{value:string}) {
    return <button className="btn btn-lg btn-outline-secondary p-2 ps-5 pe-5 fs-1 fw-bold font-monospace">{value}</button>
  }

  return (
    <div className="m-5">
      <div>
        <Square value="1" />
        <Square value="2"/>
        <Square value="3"/>
      </div>
      <div>
        <Square value="4"/>
        <Square value="5"/>
        <Square value="6"/>
      </div>
      <div>
        <Square value="7"/>
        <Square value="8"/>
        <Square value="9"/>
      </div>
    </div>
  )
}
```

Ahora deberías ver una cuadrícula de números nuevamente.

### Hacer un componente interactivo.

Rellenemos el componente `Square` con una `X` al hacer clic en él. 

Declara una función llamada `handleClick` dentro del `Square`. 

Luego, agrega `onClick` a las props del elemento `<div>` JSX devuelto por el componente `Square`:

```tsx
 function Square({ value }: { value: string }) {
    
    function handleClick() {
      alert("Click!")
    }

    return <button
      onClick={handleClick}
      className="btn btn-lg btn-outline-secondary p-2 ps-5 pe-5 fs-1 fw-bold font-monospace">{value}
    </button>
  }
```

Si haces clic en un cuadrado ahora, apareixerà una finestra d’alerta amb el text “Click!”.

**Continua el tutorial**: [Making an interactive component](https://es.react.dev/learn/tutorial-tic-tac-toe#making-an-interactive-component)


## TODO

* <https://react.dev/learn/typescript>
* [Google Docs - React](https://docs.google.com/document/d/1y3aLfR3Dmhc6af-7P7baW6aKxHUkogPHcnIk3xCh56Q/)

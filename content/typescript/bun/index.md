---
title: Bun
icon: bun.png
description: Bun es un conjunto de herramientas y un "runtime" para Typescript
---

## Introducción

[Bun](https://bun.sh/) es un entorno de ejecución de {% link "/typescript/" %} centrado en el rendimiento y en ser todo en uno (entorno de ejecución, empaquetador, administrador de paquetes, transpilador).

Bun está diseñado para ser un reemplazo directo de [Node.js](https://nodejs.org/es), admite la mayoría de las mismas API y características, y su rendimiento es superior.

Utiliza [JavaScript Core](https://docs.webkit.org/Deep%20Dive/JSC/JavaScriptCore.html) para ejecutar el código al igual que [Safari](https://www.apple.com/es/safari/), en lugar de [V8](https://v8.dev/) que utilizan Node.js y [Google Chrome](https://www.google.com/intl/es_ES/chrome/).

Además, sus librerías están programadas en el lenguaje [Zig](https://ziglang.org/)

{% image "bun.jpg" %}

## Entorno de trabajo

**TODO** move a actividad correspodiente

**Windows**. Instala [Bun](https://bun.sh/) con {% link "/tool/scoop/" %}:

```pwsh
> scoop install bun
``` 

**{% link "/windows/wsl/" %}**. Instala ... : 

```sh
$ sudo apt install -y unzip
$ curl -fsSL https://bun.sh/install | bash
$ source /home/box/.bashrc 
```

## Proyecto

Crea un nuevo proyecto:

```pwsh
> mkdir demo
> cd demo
> bun create 
```

* <https://bun.sh/docs/cli/bun-create>

## Gestor de bibliotecas

* <https://bun.sh/guides/install/add>

## TODO

<https://dev.to/vedansh0412/bun-or-nodejs-in-2024-6e3>
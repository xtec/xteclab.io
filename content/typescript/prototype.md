---
title: Prototype
layout: default.njk
---

## Prototype

Javascript es un lenguaje que utiliza prototipos para compartir propiedades entre objetos distintos.

En Javascript todos los objetos por defecto tienen una propiedad que suele llamarse `__proto__`, y que por defecto es el objeto `Object`.

Para acceder al prototipo de un objeto tienes que utilizar el método `Object.getProperty()`:

```js
let component = { x}
let prototype = Object.getPrototypeOf(city)
console.log(prototype)
```

Si ejecutas ...


```sh
$ node prototype.js
[Object: null prototype] {}
```




```js
todo
```


En el momento de crear el objeto lo puedes crear con cualquier prototipo que tu quieras.

```js


```

En cualquier momento puedes modificar el prototipo de un objeto y las nuevas propiedades serán 

```js
function Vehicle(maxSpeed) {
    this.maxSpedd = maxSpeed
}

Vehicle.prototype.maxSpeed = function() {
     return this.maxSpeed
}

function Car(maxSpeed) {
    Vehicle.call(this, maxSpeed)
}

Car.prototype = new Vehicle()
```

Los prototipos han hecho que Javascript sea un lenguaje muy flexible, facilitando el desarrollo de librerías con su propio model de objetos.























## Bibliografia

* [Javascript- An untold story of objects and prototypes](https://medium.com/@siddiqr67/javascript-an-untold-story-of-objects-and-prototypes-c0071f5602e2)

* [Understanding Objects and Prototypes in Javascript](https://medium.com/nerd-for-tech/understanding-objects-and-prototypes-in-javascript-d847baa50f9a)

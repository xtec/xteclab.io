---
title: Plotly
description: Plotly és una llibreria per generar gràfics interactius amb JavaScript.
plotly: true
---

## Introducció

[Plotly.js](https://plotly.com/javascript/) és una llibreria que ens permet crear gràfics interactius.

L'usuari també pot interactuar amb aquests gràfics des del navegador, per obtenir informació addicional que seria més difícil presentar en una imatge fixe, i en general la experiència és més atractiva.

Pots consultar aquest projecte: <https://gitlab.com/xtec/typescript/plotly>

## Entorn de treball

Hi ha **dues formes de treballar amb Plotly.**

La més clàssica és el projecte de Python `Plotly Express`. Aquesta encara és prou comú degut a que va ser la primera que es va crear, i també perquè el seu ús resulta més senzill per a perfils professionals que no tenen prou coneixements de creació de desenvolupament web.

En canvi, a nosaltres com a especialistes en desenvolupament web ens interessa `Plotly Javascript`. 

Ens interessa delegar la creació del gràfic al client (al navegador, que usa Javascript), ja que `pel servidor és molt costós dibuixar gràfics, `el servidor ja té prou feina gestionant grans volums de dades! 😀`

## Entorn de treball

Crea un projecte react:

```sh
$ bun create vite plot --template react-swc-ts
```

Afegeix les dependències:

```sh
bun add plotly.js react-plotly.js
bun add --dev @types/plotly.js @types/react-plotly.js
```
 
Modifica el fitxer `App.tsx`:

```tsx
import Plot from 'react-plotly.js' 

export default function App() {
  
  const data = [
    {
      x: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      y: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      mode: "lines",
    },
  ];

  const layout = { title: "Chart Title" };

  return <Plot data={data} layout={layout} />;
}
```

Ja tens el teu primer gràfic 😀 !


{% panel %}
<div id="plot-lines"></div>
<script>
Plotly.newPlot("plot-lines", [
    {
        x: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        y: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        mode: "lines",
    },
], { title: "Chart Title" })
</script>
{% endpanel %}


## Atributs

Els gràfics "plotly.js" es descriuen de manera declarativa amb objectes JSON.

Per cada aspecte del gràfic (els colors, les quadrícules, les dades, etc.) tens un atribut JSON.

A l'hora de descriure el gràfic els atributs s'agrupen en dues categories:

1. `traces`. Objectes que descriuen una única sèrie de dades en un gràfic

2. `layout`. Atributs que s'apliquen a la resta del gràfic, com ara el títol, l'eix o les anotacions. 

Els "traces" es classifiquen per tipus de gràfic (per exemple, "scatter" o "heatmap").

També tens un altre atribut opcional de configuració: `config`.

A continuació tens un exemple semblant a l'inicial:

```tsx
import Plot from 'react-plotly.js'

export default function App() {

  const data = [
    {
      x: [1999, 2000, 2001, 2002],
      y: [10, 15, 13, 17],
    },
  ];

  const layout = {
    xaxis: {
      title: {
        text: "Year"
      }
    },
    yaxis: {
      title: {
        text: "Percent"
      }
    }
  };

  return <Plot data={data} layout={layout} />;
}
```

Com a altres llibreries, el tipus de gràfic predefinit és el de línies:

{% panel %}
<div id="plot-tester"></div>
<script>
    Plotly.newPlot("plot-tester", [{
        x: [1999, 2000, 2001, 2002],
        y: [10, 15, 13, 17],
    }], {
            xaxis: {
                title: {
                    text: "Year"
                }
            },
            yaxis: {
                title: {
                text: "Percent"
            }
        }
    });
</script>
{% endpanel %}

## Tipus de gràfics.

En aquest enllaç tens tots els tipus de gràfics que pots utilitzar: <https://plotly.com/javascript/>

A continuació veurem alguns exemples.

### Gràfic de barres

<https://plotly.com/javascript/reference/bar/>

Crea aquest gràfic de barres:

{% panel %}
<div id="plot-bar"></div>
<script>
Plotly.newPlot("plot-bar", [
    {
        x: [1, 2, 3, 4],
        y: [5, 10, 2, 8],
        type: "bar",
    },
], {
    title: {
        text: "Diagrama de barres",
        font: {
            size: 40
        }
    }
})
</script>

{% endpanel %}

**Dades**

```json
x: [1, 2, 3, 4]
y: [5, 10, 2, 8]
```

{% sol %}
```tsx
import Plot from 'react-plotly.js'

export default function App() {

  const data = [
    {
      x: [1, 2, 3, 4],
      y: [5, 10, 2, 8],
      type: "bar",
    },
  ]

  const layout = {
    title: {
      text: "Diagrama de barres",
      font: {
        size: 40
      }
    }
  }

  return <Plot data={data} layout={layout} />;
}
```
{% endsol %}

També pots crear gràfics de barres apilades. 

Aquí tens une exemple amb les dones i homes d'un estudi de l'Alzheimer que es realitzarà en diversos hospitals:

{% panel %}
<div id="plot-alzeimer"></div>
<script>
Plotly.newPlot('plot-alzeimer', [{
    type: 'bar',
    x: ['Bellvitge', 'Clinic', 'Vall Hebron', 'Del Mar'],
    y: [7, 10, 8, 12],
    name: 'Homes'
}, {
    type: 'bar',
    x: ['Bellvitge', 'Clinic', 'Vall Hebron', 'Del Mar'],
    y: [8, 11, 6, 9],
    name: 'Dones'
}], {
    title: {
        text: 'Pacients estudi Alzheimer'
    },
    font: { size: 20 }
})
</script>
{% endpanel %}

**Dades**

```json
x: ['Bellvitge', 'Clinic', 'Vall Hebron', 'Del Mar']
y: [7, 10, 8, 12]
y: [8, 11, 6, 9]
```

{% sol %}
```tsx
import Plot from 'react-plotly.js'

export default function App() {

  const data = [
    {
      type: 'bar',
      x: ['Bellvitge', 'Clinic', 'Vall Hebron', 'Del Mar'],
      y: [7, 10, 8, 12],
      name: 'Homes'
    },
    {
      type: 'bar',
      x: ['Bellvitge', 'Clinic', 'Vall Hebron', 'Del Mar'],
      y: [8, 11, 6, 9],
      name: 'Dones'
    }
  ]


  const layout = {
    title: {
      text: 'Pacients estudi Alzheimer'
    },
    font: { size: 20 }
  };


  return <Plot data={data} layout={layout} />;
}
```
{% endsol %}

### Box

<https://plotly.com/javascript/reference/box/>

Un dels gràfics més útils en estadística són els `plotbox` o diagrames de caixes i cues. 

Són tant útils com les corbes gaussianes.

Ens permeten veure a un cop d'ull **mesures centrals i de dispersió d'una mostra**.

{% panel %}
<div id="plot-box"></div>
<script>
Plotly.newPlot('plot-box', [{
      x: [3, 4, 4, 4, 5, 6, 8, 8, 9, 10],
      type: 'box',
      name: 'Set 1'
    }],{
    title: {
      text: 'Horizontal Box Plot'
    }
})
</script>
{% endpanel %}

**Dades**

```json
x: [3, 4, 4, 4, 5, 6, 8, 8, 9, 10]
```

{% sol %}
```tsx
import Plot from 'react-plotly.js'

export default function App() {

  const data = [
    {
      x: [3, 4, 4, 4, 5, 6, 8, 8, 9, 10],
      type: 'box',
      name: 'Set 1'
    }
  ]


  const layout = {
    title: {
      text: 'Horizontal Box Plot'
    }
  }


  return <Plot data={data} layout={layout} />;
}
```
{% endsol %}

### choropleth

<https://plotly.com/javascript/tile-county-choropleth/>

Plotly també destaca per ser una llibreria que et permet `crear gràfics amb mapes` fàcilment.

Imaginem-nos que tenim les <a href="https://ec.europa.eu/eurostat/web/products-euro-indicators/w/3-02102024-ap">taxes d'atur 5 països d'Europa de l'agost del 2024.</a>

```js
    const countries = ['Spain', 'France', 'Germany', 'Portugal', 'Italy'];
    const unemp_rate = [11.5, 7.5, 3.5, 6.4, 6.2];
```

Doncs per dibuixar el codi és tant senzill com organitzar les dades al mapa de la següent manera. 

- **locationmode** Atribut literal que permet definir la forma en la que informar els països. Els valors disponibles són: `ISO-3`, `USA-states`  i `country names`.
- **locations** Llista de noms de països, en el format definit abans.
- **z** La informació numèrica de cada pais (taxa d'atur en el nostre cas)
- **text** El text de la llegenda que surt quan ens posem a sobre de cada país.

Aquí teniu més informació sobre els `codis ISO-3`:
<https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3>

Aquesta solució només permet fer gràfics de països.


{% panel %}
<script>
<div id="plot-choropleth"></div>
Plotly.newPlot('plot-choropleth', [{
    type: 'choropleth',
    locationmode: 'country names',
    locations: ['Spain', 'France', 'Germany', 'Portugal', 'Italy'],
    z: [11.5, 7.5, 3.5, 6.4, 6.2],
    text: countries,
    autocolorscale: true
  }], {
    title: { text: 'Taxa atur (agost 2024)' },
    geo: {
      scope: 'europe',
      projection: { type: 'robinson' }
    }
})
</script>

{% endpanel %}


{% sol %}
```tsx
import Plot from 'react-plotly.js'

export default function App() {

  const countries = ['Spain', 'France', 'Germany', 'Portugal', 'Italy']
  const unemp_rate = [11.5, 7.5, 3.5, 6.4, 6.2]
  const data = [{
    type: 'choropleth',
    locationmode: 'country names',
    locations: countries,
    z: unemp_rate,
    text: countries,
    autocolorscale: true
  }]
  
  const layout = {
    title: { text: 'Taxa atur (agost 2024)' },
    geo: {
      scope: 'europe',
      projection: { type: 'robinson' }
    }
  }


  return <Plot data={data} layout={layout} />;
}
```

{% endsol %}


## CSV


<https://www.ine.es/dyngs/INEbase/listaoperaciones.htm>

<https://immunizationdata.who.int/global?topic=&location=>

```sh
$ bun add papaparse 
$ bun add --dev @types/papaparse
```


* <https://www.ine.es/prodyser/microdatos.htm>
* <https://typescript.tv/hands-on/parsing-csv-files-in-typescript-with-papa-parse/>
* <https://www.basedash.com/blog/how-to-use-papaparse-with-typescript>
* <https://www.escuelafrontend.com/data-fetching-con-react>

## Functions

<https://plotly.com/javascript/plotlyjs-function-reference/>


## TODO

* <https://plotly.com/javascript/reference/index/>
* <https://plotly.com/javascript/react/>
* <https://open-innovations.org/data/geojson.html>
* <https://docs.google.com/document/d/1u90XI23NT6qDCb28IIsuUcKAUt_8XNgYe_lKSiT2Zfk>
* <https://dmitripavlutin.com/javascript-fetch-async-await/>
* <https://dev.to/colocodes/how-to-debug-a-react-app-51l4>
* <https://blog.tericcabrel.com/read-csv-node-typescript/>
* <https://medium.com/@bhanu.mt.1501/api-calls-in-react-js-342a09d5315f>
* <https://www.c-sharpcorner.com/article/how-to-pretty-print-json-with-reactjs/>
* <https://www.escuelafrontend.com/data-fetching-con-react>

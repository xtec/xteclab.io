---
title: Vite
icon: vite.png
description: Vite es un servidor de desarrollo y una herramienta de compilación.
---

## Introducción

[Vite](https://vite.dev/) ...


## Hello, World!

Puedes crear un proyecto con {% link "/typescript/bun/" %}:

```pwsh
bun create vite
```

¡Y a continuación sigue las instrucciones!

Otra opción es especificar directamente el nombre del proyecto y la plantilla que deseas usar,

Por ejemplo, para montar un proyecto de Vite + Reac, ejecuta:

```pwsh
bun create vite tsx --template react-swc-ts
```

## Tailwind

Instala tailwindcss:

```pwsh
bun add -d tailwindcss @tailwindcss/vite
```

Añade el complemento `@tailwindcss/vite` a tu configuración de Vite.

**`vite.config.js`**

```ts
import {defineConfig} from 'vite'
import react from '@vitejs/plugin-react-swc'
import tailwindcss from '@tailwindcss/vite'

// https://vite.dev/config/
export default defineConfig({
  plugins: [react(), tailwindcss()],
})
```

Agrega un `@import` al archivo `index.css` que importe Tailwind CSS.

```css
@import "tailwindcss";
```


Ejecute su proceso de compilación con npm run dev

```pwsh
bun run dev
```

Comienza a usar las clases de utilidad de Tailwind para darle estilo a tu contenido:

**App.tsx**

```ts
export default function App() {
  return (
    <div className="flex h-screen">
      <header className="m-auto">
        <p className="text-2xl underline underline-offset-8 font-['Menlo']
          hover:underline-offset-4 hover:text-3xl hover:border-red-500
           hover:border-4 hover:p-4 hover:cursor-pointer">
          David de Mingo
        </p>
      </header>
    </div>
  );
}
```

## TODO

* <https://vite.dev/guide/>
* <https://bun.sh/guides/ecosystem/vite>
* <https://www.telerik.com/blogs/getting-started-bun-react-developers>
* <https://tailwindcss.com/docs/installation/using-vite>
* <https://github.com/varun-d/template_react_ts_tw>
* <https://medium.com/@ramsey.rajput/bun-running-a-react-app-with-bun-and-tailwind-using-react-app-69dac8822ab3>


---
title: Computació
icon: typescript.png
description: El propòsit inicial dels computadors era computar números.
mermaid: true
---

**PENDENT DE REVISAR EN PROFUNDITAT**

* Primer expliquem els números
* Després els strings
* Explicar `any` i el union type (i.e `string | null`)

## Introducció

De de fa més de 400 anys una computadora era una persona que calculaba números:

{% image "human-computer.jpg" %}

Una part de la teva infància ha estat aprenent a fer càlculs amb números, i molts cops t'has preguntat que perquè has d'apendre aquestes coses si el teu mòbil té una calculadora que ho fa molt millor que tu.

El mateix van pensar les empreses, els governs i els enginyers, i per això van crear els primers computadors mecànics.

A continuació tens la imatge de l'[ENIAC](https://ca.wikipedia.org/wiki/ENIAC) dels anys 40, molt gran, però molt menys potent que el teu mòbil:

{% image "eniac.jpg" %}

A principis de 1960 ja existien les primeres calculadores comercials.

A continuació tens la imatge del [IBM 608](https://es.wikipedia.org/wiki/IBM_608):

{% image "ibm-608.jpg" %}

Aquestes màquines mai s'equivoquen, no es posen malaltes, no protesten, etc. 

I el que passa sempre, ja pots despedir a molta gent que no necessites i que s'ha de pagar bé perqué preparar una calculadora "humana" porta molt de temps.

Una professió centenària va desaparèixer 😔.

Doncs comencem des del principi, i recorda ..

Encara que pensis que una computadora és per jugar, veure películes, etc. radera d'aquests programes hi ha moltes operacions matemàtiques!

## Bun

Instal.la [Bun](https://bun.sh/) amb {% link "/tool/scoop/" %}:

```pwsh
> scoop install bun
``` 

Executa l'ordre `bun repl` per iniciar una sessió REPL:

```pwsh
 bun repl
3 Welcome to Bun v1.1.34
Type ".help" for more information.
>
```

Tots els llenguatges de programació fan servir números, i pots fer operacions aritmétiques amb aquets números.

Escriu l'ordre `3 + 7` i prem la tecla `Enter`:

```pwsh
> 3 + 7
10
```

Pots veure que `node` evalua l'expressió `3 + 7` i torna el resultat d'evaluar aquesta expressió.

Con javascript puedes realizar las operaciones aritméticas habituales.

Puedes restar dos números:

```pwsh
> 20 - 15
5
```

Puedes mutiplicar dos números:

```pwsh
> 3 * 7
21
```

Puedes dividir dos números:

```pws
> 10 / 5
2
```

Puedes obtener el resto (o módulo) de una división:

```pwsh
> 8 % 3 
2 
```

Puedes elevar un número a una potencia:

```pwsh
> 5 ** 2 
25
```

Puedes construir cualquier expresión compleja para obtener un resultado:

```pwsh
> (5 + 20 ) / 3
8.333333333333334
```

Apreta les tecles `CTRL + D` per finalitzar la sessió.


## Entorn de treball

Crea la carpera `script` i obre-la amb {% link "/project/vscode/" %}:

```pwsh
> md script
> code script
```

Crea l'script `index.ts`:

```ts
console.log("Hello World!")
```

Obre un terminal i executa l'script:

{% image "bun.png" %}

## Sentencias

Las sentencias son construcciones sintácticas y comandos que realizan acciones.

Podemos tener tantas sentencias en nuestro código como queramos, las cuales se pueden separar con un punto y coma.

Por ejemplo, aquí separamos “Hello World” en dos logs:

```ts
console.log("Hello"); console.log("World");
```

Generalmente, las sentencias se escriben en líneas separadas para hacer que el código sea más legible:

```ts
console.log("Hello");
console.log("World");
```

### Punto y coma

Se puede omitir un punto y coma en la mayoría de los casos cuando existe un salto de línea.

Esto también funcionaría:

```js
console.log("Hello")
console.log("World")
```

Aquí, TypeScript interpreta el salto de línea como un punto y coma "implícito". Esto se denomina inserción automática de punto y coma.

En la mayoría de los casos, una nueva línea implica un punto y coma. Pero “en la mayoría de los casos” no significa “siempre”!

Hay casos en que una nueva línea no significa un punto y coma. Por ejemplo:

```ts
console.log(3 +
    1
    + 2)
```

El código da como resultado `6` porque TypeScript no inserta punto y coma aquí. Es intuitivamente obvio que si la línea termina con un signo más "+", es una “expresión incompleta”, un punto y coma aquí sería incorrecto. Y en este caso eso funciona según lo previsto.

**Pero hay situaciones en las que TypeScript “falla” al asumir un punto y coma donde realmente se necesita.**

Los errores que ocurren en tales casos son bastante difíciles de encontrar y corregir.

### Comentarios

A medida que pasa el tiempo, los programas se vuelven cada vez más complejos. Se hace necesario agregar *comentarios* que describan lo que hace el código y por qué.

Los comentarios se pueden poner en cualquier lugar de un script. No afectan su ejecución porque el motor simplemente los ignora.

**Los comentarios de una línea comienzan con dos caracteres de barra diagonal `//`.**

El resto de la línea es un comentario. Puede ocupar una línea completa propia o seguir una sentencia.

Como aquí:

```ts
// Este comentario ocupa una línea propia.
console.log("Hello")

console.log("World") // Este comentario sigue a la sentencia.
```

**Los comentarios de varias líneas comienzan con una barra inclinada y un asterisco `/*` y terminan con un asterisco y una barra inclinada `*/`.**

Como aquí:

```ts
/* Un ejemplo con dos mensajes.
Este es un comentario multilínea.
*/
console.log("Hello")
console.log("World")
```

El contenido de los comentarios se ignora, por lo que si colocamos el código dentro de `/* … */`, no se ejecutará.

A veces puede ser útil deshabilitar temporalmente una parte del código:

```ts
/* Comentando el código
console.log("Hello")
*/
console.log("World")
```

## Variables

Una variable es un “almacén con un nombre” para guardar datos. 

Para generar una variable en TypeScript, se usa la palabra clave `let`.

La siguiente declaración define una variable con el nombre `message` de tipus `string`:

```ts
let message: string
```

Ahora podemos introducir datos en ella, utilizando el operador de asignación `=`:

```ts
let message: string
message = "Hello!" // almacena el string 'Hello' en la variable message
```

Ahora el string está almacenado en el área de la memoria asociada con la variable. 

Podemos acceder utilizando el nombre de la variable:

```js
let message: string
message = "Hello!"

console.log(message) // muestra el contenido de la variable
```

Para ser concisos, podemos combinar la declaración de la variable y su asignación en una sola línea:

```js
let message = "Hello!" // define la variable y asigna un valor
console.log(message); // Hello!
```

És un error assignar un `number` a la variable `message` de tipus `string`:

```ts
let message = "hello"
message = 3
```

También podemos declarar múltiples variables en una sola línea:

```js
let user = "John", age = 25, message = "Hola";
```

### Una analogía de la vida real

Podemos comprender fácilmente el concepto de una “variable” si nos la imaginamos como una “caja” con una etiqueta de nombre único pegada en ella.

Por ejemplo, la variable `message` puede ser imaginada como una caja etiquetada "message" con el valor "Hello!" dentro:

<pre class="mermaid">
flowchart LR
  box["Hello!"]
</pre>

Podemos introducir cualquier valor a la caja.

También la podemos cambiar cuantas veces queramos:

```js
let message: string

message = "Hello!"

message = 'World!'; // valor alterado

console.log(message);
```

Cuando el valor ha sido alterado, los datos antiguos serán removidos de la variable:

<pre class="mermaid">
flowchart LR
  box["World!"]
</pre>


También podemos declarar dos variables y copiar datos de una a la otra.

```js
let hello = "Hello world!"

let message: string

// copia 'Hola mundo' de hello a message
message = hello

// Ahora, ambas variables contienen los mismos datos
console.log(hello) // Hello World!
console.logt(message) // Hello World!
```

{% panel "Declarar dos veces lanza un error" %}

Una variable debe ser declarada solamente una vez.

Una declaración repetida de la misma variable es un error:

```js
let message = "This";

// 'let' repetidos lleva a un error
let message = "That"; // SyntaxError: 'message' ya fue declarado
```

Debemos declarar una variable una sola vez y desde entonces referirnos a ella sin `let`.

{% endpanel %}


### Nombramiento de variables

Existen dos limitaciones de nombre de variables en TypeScript:

1. El nombre únicamente puede incluir letras, dígitos, o los símbolos $ y _.
2. El primer carácter no puede ser un dígito.

Ejemplos de nombres válidos:

```js
let userName: string
let test123: string
``` 

Cuando el nombre contiene varias palabras, se suele usar el estilo [camelCase](https://es.wikipedia.org/wiki/Camel_case) (capitalización en camello), donde las palabras van pegadas una detrás de otra, con cada inicial en mayúscula: `miNombreMuyLargo`.

Es interesante notar que el símbolo del dólar `$` y el guion bajo `_` también se utilizan en nombres. Son símbolos comunes, tal como las letras, sin ningún significado especial.

Los siguientes nombres son válidos:

```js
let $ = 1 // Declara una variable con el nombre "$"
let _ = 2 // y ahora una variable con el nombre "_"

console.log($ + _); // 3
```

Ejemplos de nombres incorrectos:

```js
let 1a: string // no puede iniciar con un dígito

let my-name: string // los guiones '-' no son permitidos en nombres
```

{% panel "La Capitalización es Importante" %}

Dos variables con nombres `manzana` y `MANZANA` son variables distintas.

{% endpanel %}

{% panel "Las letras que no son del alfabeto inglés están permitidas, pero no se recomiendan" %}

Es posible utilizar letras de cualquier alfabeto, incluyendo letras del cirílico, logogramas chinos, etc.:

```js
let имя = "..."
let 我 = "..."
```

Técnicamente, no existe error aquí. Tales nombres están permitidos, pero internacionalmente existe la convención de utilizar el inglés para el nombre de las variables. Incluso si estamos escribiendo un script pequeño, este puede tener una larga vida por delante. Gente de otros países puede necesitar leerlo en algún momento.

{% endpanel %}

{% panel "Nombres reservados" %}

Hay una [lista de palabras reservadas](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Lexical_grammar#Keywords), las cuales no pueden ser utilizadas como nombre de variable porque el lenguaje en sí las utiliza.

Por ejemplo: `let`, `class`, `return`, y `function` están reservadas.

El siguiente código nos da un error de sintaxis:

```js
let let = 5 // no se puede le nombrar "let" a una variable  ¡Error!
let return = 5 // tampoco se le puede nombrar "return", ¡Error!
```
{% endpanel %}

### Constantes

Para declarar una variable constante (inmutable) utiliza `const` en vez de `let`:

```js
const myBirthday = "18.04.1982"
```

Las variables declaradas utilizando `const` se llaman “constantes”. No pueden ser alteradas. Al intentarlo causaría un error:

```js
const myBirthday = "18.04.1982"

myBirthday = '01.01.2001' // ¡error, no se puede reasignar la constante!
```

Cuando un programador está seguro de que una variable nunca cambiará, puede declararla con `const` para garantizar esto y comunicarlo claramente a los demás.

### Constantes mayúsculas

Existe una práctica utilizada ampliamente de utilizar constantes como aliases de valores difíciles-de-recordar y que se conocen previo a la ejecución.

Tales constantes se nombran utilizando letras mayúsculas y guiones bajos.

Por ejemplo, creemos constantes para los colores en el formato “web” (hexadecimal):

```js
const COLOR_RED = "#F00"
const COLOR_GREEN = "#0F0"
const COLOR_BLUE = "#00F"
const COLOR_ORANGE = "#FF7F00"

// ...cuando debemos elegir un color
let color = COLOR_ORANGE
console.log(color) // #FF7F00
```


Ventajas:

* `COLOR_ORANGE` es mucho más fácil de recordar que `"#FF7F00"`.
*  Es mucho más fácil escribir mal `"#FF7F00"` que `COLOR_ORANGE`.
*  Al leer el código, `COLOR_ORANGE` tiene mucho más significado que `"#FF7F00"`.

¿Cuándo se deben utilizar letras mayúsculas para una constante, y cuando se debe nombrarla de manera normal? Dejémoslo claro.

Ser una “constante” solo significa que el valor de la variable nunca cambia. Pero hay constantes que son conocidas previo a la ejecución (como el valor hexadecimal del color rojo) y hay constantes que son calculadas en el tiempo de ejecución, pero no cambian después de su asignación inicial.

Por ejemplo:

```js
const pageLoadTime: number = /* el tiempo que tardó la página web para cargar */;
```

El valor de `pageLoadTime` no está preestablecido. Como no se conoce antes de cargar la página, la nombramos normalmente. Pero podemos declararla como constante, porque después de su asignación inicial, no cambiará.

En otras palabras, las constantes en mayúsculas son utilizadas solamente como alias para valores invariables y preestablecidos.

### Nombrar cosas correctamente

Estando en el tema de las variables, existe una cosa de mucha importancia.

Una variable debe tener un nombre claro, de significado evidente, que describa el dato que almacena.

Nombrar variables es una de las habilidades más importantes y complejas en la programación. Un vistazo rápido a el nombre de las variables nos revela cuál código fue escrito por un principiante o por un desarrollador experimentado.

En un proyecto real, se pasa mucho más tiempo modificando y extendiendo una base de código existente que escribiendo algo nuevo desde cero. Cuando regresamos a nuestro código luego de un tiempo, es mucho más fácil encontrar información que está bien etiquetada. O en otras palabras, cuando las variables tienen los nombres adecuados.

Por favor, dedica tiempo para pensar un nombre correcto para una variable antes de declararla. Hacer esto te rendirá muy bien.

Algunas reglas buenas para seguir:

* Usa términos legibles para humanos como `userName` o `shoppingCart`.
* Evita abreviaciones o nombres cortos `a`, `b`, `c`, a menos que realmente sepas lo que estás haciendo.
* Crea nombres que describan al máximo lo que son y sean concisos. Ejemplos de nombres malos son `data` y `value`. Estos nombres no nos dicen nada, solo son adecuados en el contexto de un código que deje excepcionalmente obvio cuál dato o valor está referenciando la variable.
* Ponte de acuerdo con tu equipo, y con tu propia mente, cuáles términos se utilizarán. Si a un visitante se lo llamara “user”, debemos llamar las variables relacionadas `currentUser` o `newUser` en vez de `currentVisitor` o `newManInTown`.

¿Suena simple? De hecho lo es, pero no es tan fácil crear nombres de variables descriptivos y concisos a la hora de practicar. Inténtelo.


{% panel "¿Reusar o crear?" %}

Una última nota. Existen programadores haraganes que, en vez de declarar una variable nueva, tienden a reusar las existentes.

El resultado de esto es que sus variables son como cajas en las cuales la gente introduce cosas distintas sin cambiar sus etiquetas. ¿Que existe dentro de la caja? ¿Quién sabe? Necesitamos acercarnos y revisar.

Dichos programadores se ahorran un poco durante la declaración de la variable, pero pierden diez veces más a la hora de depuración.

Una variable extra es algo bueno, no algo diabólico.

Los navegadores modernos y los minificadores de TypeScript optimizan el código, así que esto no impacta en el rendimiento. Utilizar diferentes variables para distintos valores incluso puede ayudar a optimizar tu código.
{% endpanel %}

## Tipos de datos

Un valor en TypeScript siempre pertenece a un tipo de dato determinado. Por ejemplo, un string o un número.

Totes les variables tenen un tipus que pots declarar de manera explícita.


### Number

El tipo `number` representa tanto números enteros como de punto flotante.

En aquest codi Typescript determina que la variable `n` ha de ser de tipus `number`

```ts
let n = 123
n = 12.345
```

Si intentes assignar un `string` a `n` tens un error perquè el tipus implícit és `number`:

```ts
let n = 123
n = "hola"
```

Si declares el tipus de manera explícita, el que dius va a misa:

```ts
let n: number = "Hola"
```

Hay muchas operaciones para números. Por ejemplo, multiplicación `*`, división `/`, suma `+`, resta `-`, y demás.

Además de los números comunes, existen los llamados “valores numéricos especiales” que también pertenecen a este tipo de datos: `Infinity`, `-Infinity` y `NaN`.

`Infinity` representa el [Infinito](https://es.wikipedia.org/wiki/Infinito) matemático ∞. Es un valor especial que es mayor que cualquier número.

Podemos obtenerlo como resultado de la división por cero:

```ts
console.log( 1 / 0 ); // Infinity
```

O simplemente hacer referencia a él directamente:

```ts
console.log( Infinity ); // Infinity
```

`NaN` representa un error de cálculo. Es el resultado de una operación matemática incorrecta o indefinida, por ejemplo:

```ts
console.log( "no es un número" / 2 ); // NaN, tal división es errónea
```

`NaN` es “pegajoso”. Cualquier otra operación sobre NaN devuelve NaN:

```ts
console.log NaN + 1 ); // NaN
console.log 3 * NaN ); // NaN
console.log "not a number" / 2 - 1 ); // NaN
```

Por lo tanto, si hay un NaN en alguna parte de una expresión matemática, se propaga a todo el resultado (con una única excepción: `NaN ** 0` es `1`).

{% panel "Las operaciones matemáticas son seguras" %}

Hacer matemáticas es “seguro” en JavaScript. Podemos hacer cualquier cosa: dividir por cero, tratar las cadenas no numéricas como números, etc.

El script nunca se detendrá con un error fatal (y “morir”). En el peor de los casos, obtendremos `NaN` como resultado.

{% endpanel %}

Los valores numéricos especiales pertenecen formalmente al tipo “número”. Por supuesto que no son números en el sentido estricto de la palabra.

### BigInt

`BigInt` representa enteros de longitud arbitraria.

En TypeScript, el tipo `number` no puede representar de forma segura valores enteros mayores que <code>(2<sup>53</sup>-1)</code> (eso es `9007199254740991`), o menor que <code>-(2<sup>53</sup>-1)</code> para negativos.

Para ser realmente precisos, el tipo de dato `number` puede almacenar enteros muy grandes (hasta <code>1.7976931348623157 * 10<sup>308</sup></code>), pero fuera del rango de enteros seguros <code>±(2<sup>53<sup>-1)</code> habrá un error de precisión, porque no todos los dígitos caben en el almacén fijo de 64-bit. Así que es posible que se almacene un valor “aproximado”.

Por ejemplo, estos dos números (justo por encima del rango seguro) son iguales:

```ts
console.log(9007199254740991 + 1) // 9007199254740992
console.log(9007199254740991 + 2) // 9007199254740992
```

Podemos decir que ningún entero impar mayor que <code>(2<sup>53</sup>-1)</code> puede almacenarse en el tipo de dato `number`.

Para la mayoría de los propósitos, el rango <code>±(2<sup>53<sup>-1)</code> es suficiente, pero a veces necesitamos números realmente grandes; por ejemplo, para criptografía o marcas de tiempo de precisión de microsegundos.

Un valor BigInt se crea agregando `n` al final de un entero:

```ts
// la "n" al final significa que es un BigInt
const big = 1234567890123456789012345678901234567890n
console.log(big + 1n) //1234567890123456789012345678901234567891n
```

### String

Un string en JavaScript es una cadena de caracteres y debe colocarse entre comillas.

```ts
let str = "Hola"
let str2 = 'Las comillas simples también están bien'
let phrase = `se puede incrustar otro ${str}`
```

En JavaScript, hay 3 tipos de comillas.

1. Comillas dobles: `"Hola"`.
2. Comillas simples: `'Hola'`.
3. Backticks (comillas invertidas): `` `Hola` ``.

Las comillas dobles y simples son comillas “sencillas” (es decir, funcionan igual). No hay diferencia entre ellas en TypeScript.

Los backticks son comillas de “funcionalidad extendida”. Nos permiten incrustar variables y expresiones en una `string` encerrándolas en `${...}`, por ejemplo:

```ts
let name = "John";

// incrustar una variable
console.log `Hola, ${name}!` ) // Hola, John!

// incrustar una expresión
console.log `el resultado es ${1 + 2}` ) //el resultado es 3
```

La expresión dentro de `${...}` se evalúa y el resultado pasa a formar parte del `string`. Podemos poner cualquier cosa ahí dentro: una variable como `name`, una expresión aritmética como `1 + 2`, o algo más complejo.

Tem en cuenta que esto sólo se puede hacer con los backticks. ¡Las otras comillas no tienen esta capacidad de incrustación!

```ts
console.log("el resultado es ${1 + 2}" ) // el resultado es ${1 + 2} (las comillas dobles no hacen nada)
```

¿Cuál es la salida del script?

```ts
let name = "Ilya";

console.log( `Hola ${1}` ) // ?

console.log( `Hola ${"name"}` ) // ?

console.log( `Hola ${name}` ) // ?
```

{% sol %}
```pwsh
Hola 1
Hola name
Hola Ilya
```
{% endsol %}

### El valor “null” (nulo)

El valor especial `null` no pertenece a ninguno de los tipos descritos anteriormente.

Forma un tipo propio separado que contiene sólo el valor `null`:

```ts
let age = null
```

`null` es sólo un valor especial que representa “nada”, “vacío” o “valor desconocido”.

El código anterior indica que el valor de `age` es desconocido o está vacío por alguna razón.

### El valor “undefined” (indefinido)

El valor especial `undefined` también se distingue. Hace un tipo propio, igual que `null`.

El significado de `undefined` es “valor no asignado”.

Si una variable es declarada, pero no asignada, entonces su valor es `undefined`:

```ts
let age;

console.log(age) // muestra "undefined"
```

Técnicamente, es posible asignar `undefined` a cualquier variable:

```ts
let age = 100;

// cambiando el valor a undefined
age = undefined;

console.log(age); // "undefined"
```

…Pero no recomendamos hacer eso. Normalmente, usamos `null` para asignar un valor “vacío” o “desconocido” a una variable, mientras `undefined` es un valor inicial reservado para cosas que no han sido asignadas.

### any

TypeScript also has a special type, any, that you can use whenever you don’t want a particular value to cause typechecking errors.

When a value is of type any, you can access any properties of it (which will in turn be of type any), call it like a function, assign it to (or from) a value of any type, or pretty much anything else that’s syntactically legal:

```ts
let obj: any = { x: 0 };
// None of the following lines of code will throw compiler errors.
// Using `any` disables all further type checking, and it is assumed
// you know the environment better than TypeScript.
obj.foo();
obj();
obj.bar = 100;
obj = "hello";
const n: number = obj;
```

The any type is useful when you don’t want to write out a long type just to convince TypeScript that a particular line of code is okay.

### Unió de tipus

**TODO**

### prompt

Muestra un mensaje de texto, un campo de entrada para el visitante y los botones OK/CANCELAR.

```ts
let result = prompt(title, [default]);
```

La función prompt acepta dos argumentos:

* `title` El texto a mostrar al usuario.
* `default` Un segundo parámetro opcional, es el valor inicial del campo de entrada.

El usuario puede escribir algo en el campo de entrada de solicitud y presionar OK, así obtenemos ese texto en `result`.

Por ejemplo:

```ts
let age = prompt ("¿Cuántos años tienes?", "100")

alert(`Tienes ${age} años!`) //Tienes 100 años!
```

### confirm

La función `confirm` muestra una pregunta y espera una respuesta si/no del usuario.

```ts
let result = confirm(pregunta)
```

El resultado es `true` si se escribe `y` y `false` en caso contrario.

Por ejemplo:

```ts
let isBoss = confirm("¿Eres el jefe?")

alert( isBoss ) // true si introduce y
```

### Activitat

Escriu un codi que pregunti el nom a l'usuari i el saludi per pantalla:

{% sol %}
```ts
let name = prompt("Com et dius?", "")
alert(`Hello ${name} !`)
```
{% endsol %}

## Conversiones de Tipos

La mayoría de las veces, los operadores y funciones convierten automáticamente los valores que se les pasan al tipo correcto. Esto es llamado “conversión de tipo”.

Por ejemplo, `alert` convierte automáticamente cualquier valor a `string` para mostrarlo. Las operaciones matemáticas convierten los valores a números.

También hay casos donde necesitamos convertir de manera explícita un valor al tipo esperado.
Aún no hablamos de objetos


### ToString

La conversión a string ocurre cuando necesitamos la representación en forma de texto de un valor.

Por ejemplo, `alert(value)` lo hace para mostrar el valor como texto.

También podemos llamar a la función `String(value)` para convertir un valor a `string`:

```ts
let value = true;
console.log(typeof value) // boolean

let strValue = String(value) // ahora value es el string "true"
console.log(typeof strValue) // string
```

La conversión a `string` es bastante obvia. El boolean `false` se convierte en `"false"`, `null` en `"null"`, etc.

### ToNumber

Podemos usar la función `Number(value)` para convertir un valor a un número:

```ts
let str = "123"
console.log(typeof str) // string

let num = Number(str) // se convierte en 123
console.log(typeof num) // number
```

Si el `string` no es un número válido, el resultado de la conversión será `NaN`. 

Por ejemplo:

```ts
let age = Number("un texto arbitrario en vez de un número")
console.log(age) // NaN, conversión fallida
```

Reglas de conversión numérica:

| Valor | Se convierte en… |
|-|-|
| `undefined` | `NaN` |
| `null` | `0` |
| `true` i `false` | `1` i `0` |
| `string`  |Se eliminan los espacios (incluye espacios, tabs `\t`, saltos de línea `\n`, etc.) al inicio y final del texto. Si el `string` resultante es vacío, el resultado es `0`, en caso contrario el número es “leído” del string. Un error devuelve `NaN`. |


Ejemplos:

```ts
console.log(Number("   123   ")) // 123
console.log(Number("123z"))      // NaN (error al leer un número en "z")
console.log(Number(true));       // 1
console.log(Number(false))       // 0
``` 

Ten en cuenta que `null` y `undefined` se comportan de distinta manera aquí: `null` se convierte en `0` mientras que `undefined` se convierte en `NaN`.

{% panel "Adición ‘+’ concatena strings" %}

Casi todas las operaciones matemáticas convierten valores a números. Una excepción notable es la suma `+`. Si uno de los valores sumados es un `string`, el otro valor es convertido a `string`.

Luego, los concatena (une):

```ts
alert( 1 + '2' ); // '12' (string a la derecha)
alert( '1' + 2 ); // '12' (string a la izquierda)
```

Esto ocurre solo si al menos uno de los argumentos es un `string`, en caso contrario los valores son convertidos a número.
{% endpanel %}

## Operadores básicos, matemáticas

Conocemos varios operadores matemáticos porque nos los enseñaron en la escuela. Son cosas como la suma `+`, multiplicación `*+`, resta `-`, etcétera.

### Términos: “unario”, “binario”, “operando”

Antes de continuar, comprendamos la terminología común.

Un **operando** – es a lo que se aplican los operadores. Por ejemplo, en la multiplicación de `5 * 2` hay dos operandos: el operando izquierdo es `5` y el operando derecho es `2`. A veces, la gente los llama “argumentos” en lugar de “operandos”.

Un **operador** es *unario* si tiene un solo operando. Por ejemplo, la negación unaria `-` invierte el signo de un número:

```ts
let x = 1
x = -x
console.log(x) // -1, se aplicó negación unaria
```

** Un **operador** es *binario* si tiene dos operandos. El mismo negativo también existe en forma binaria:

```ts
let x = 1, y = 3
console.log( y - x ) // 2, binario negativo resta valores
```

Formalmente, estamos hablando de dos operadores distintos: la negación unaria (un operando: revierte el símbolo) y la resta binaria (dos operandos: resta).

### Matemáticas

Están soportadas las siguientes operaciones:

* Suma `+`,
* Resta `-`,
* Multiplicación `*`,
* División `/`,
* Resto `%`,
* Exponenciación `**`.

Los primeros cuatro son conocidos mientras que `%` y `**` deben ser explicados más ampliamente.

### Resto `%`

El operador resto `%`, a pesar de su apariencia, no está relacionado con porcentajes.

El resultado de `a % b` es el [resto](https://es.wikipedia.org/wiki/Resto) de la división entera de `a` por `b`.

Por ejemplo:

```ts
console.log(5 % 2) // 1, es el resto de 5 dividido por 2
console.log(8 % 3) // 2, es el resto de 8 dividido por 3
console.log(8 % 4) // 0, es el resto de 8 dividido por 4
```

### Exponenciación `**`

El operador exponenciación `a ** b` eleva `a` a la potencia de `b`.

En matemáticas de la escuela, lo escribimos como a<sup>b</sup>.

Por ejemplo:

```ts
console.log(2 ** 2) // 2² = 4
console.log(2 ** 3) // 2³ = 8
console.log(2 ** 4) // 2⁴ = 16
```

Matemáticamente, la exponenciación está definida para operadores no enteros también.

Por ejemplo, la raíz cuadrada es el exponente ½:

alert( 4 ** (1/2) ); // 2 (potencia de 1/2 es lo mismo que raíz cuadrada)
alert( 8 ** (1/3) ); // 2 (potencia de 1/3 es lo mismo que raíz cúbica)

### Concatenación de strings con el binario +

Normalmente el operador `+` suma números.

Pero si se aplica el `+` binario a una `string`, los une (concatena):

```ts
let s = "my" + "string"
console.log(s) // mystring
```

Tenga presente que si uno de los operandos es una cadena, el otro es convertido a una cadena también.

Por ejemplo:

```ts
console.log("1" + 2); // "12"
console.log(2 + "1") // "21"
```

Vieron, no importa si el primer operando es una cadena o el segundo.

Aquí hay un ejemplo algo más complejo:

```ts
console.log(2 + 2 + "1") // "41" y no "221"
```

Aquí, los operadores trabajan uno después de otro. El primer `+` suma dos números entonces devuelve `4`, luego el siguiente `+` le agrega el string `1`, así que se evalúa como `4 + "1" = 41`.

```ts
console.log("1" + 2 + 2) // "122", no es "14"
```

Aquí el primer operando es una cadena, el compilador trata los otros dos operandos como cadenas también. El `2` es concatenado a `"1"`, entonces es como `"1" + 2 = "12"` y `"12" + 2 = "122"`.

El binario `+` es el único operador que soporta strings en esa forma. 

Otros operadores matemáticos trabajan solamente con números.

### Precedencia del operador

Si una expresión tiene más de un operador, el orden de ejecución se define por su *precedencia* o, en otras palabras, el orden de prioridad predeterminado de los operadores.

Desde la escuela, todos sabemos que la multiplicación en la expresión `1 + 2 * 2` debe calcularse antes de la suma. Eso es exactamente la precedencia. Se dice que la multiplicación tiene una mayor precedencia que la suma.

Los paréntesis anulan cualquier precedencia, por lo que si no estamos satisfechos con el orden predeterminado, podemos usarlos para cambiarlo. Por ejemplo, escriba `(1 + 2) * 2`.

Hay muchos operadores en JavaScript. Cada operador tiene un número de precedencia correspondiente. El que tiene el número más grande se ejecuta primero. Si la precedencia es la misma, el orden de ejecución es de izquierda a derecha.

Aquí hay un extracto de la [tabla de precedencia](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Operadores/Operator_Precedence) (no necesita recordar esto, pero tenga en cuenta que los operadores unarios son más altos que el operador binario correspondiente):

...

### Modificar en el lugar

A menudo necesitamos aplicar un operador a una variable y guardar el nuevo resultado en esa misma variable.

Por ejemplo:

```ts
let n = 2
n = n + 5
n = n * 2
```

Esta notación puede ser acortada utilizando los operadores `+=` y `*=`:

```ts
let n = 2
n += 5 // ahora n = 7  (es lo mismo que n = n + 5)
n *= 2 // ahora n = 14 (es lo mismo que n = n * 2)

console.log(n) // 14
```

Los operadores cortos “modifica y asigna” existen para todos los operadores aritméticos: `/=`, `-=`, etcétera.

Tales operadores tienen la misma precedencia que la asignación normal, por lo tanto se ejecutan después de otros cálculos:

```ts
let n = 2
n *= 3 + 5 // el lado derecho es evaluado primero, es lo mismo que n *= 8
console.log(n) // 16
```

### Activitat

**1.-** ¿Cuáles son los valores de `a` y `x` después del código a continuación? 

```ts
let a = 2
a *= 2
let x = 1 + a
```

{% sol %}
```pwsh
a = 4 (multiplicado por 2)
x = 5 (calculado como 1 + 4)
```
{% endsol %}

**2.-** A continuació tens un codi que demana a l'ususari dos nombres i mostra la suma.

Té un error, arregla el codi.

```ts
let a = prompt("¿Primer número?")
let b = prompt("¿Segundo número?")

console.log(a + b)
```

{% sol %}
let a = prompt("¿Primer número?")
let b = prompt("¿Segundo número?")

console.log(Number(a) + Number(b))
{% endsol %}


## Activitat


### Cercle

Para calcular el area de un círculo debes utilitzar la fórumla `πr²`, donde `r` es el radio del círculo y `π` és el número Pi.

{% image "area-of-circle-formula.webp" %}

Calcula l'area d'un cercle de diametre `5`:

{% sol %}
```ts
let area = 3.141592653 * (5 ** 2)
console.log(area) //78.539816325
```
{% endsol %}

Modifica el codi per tal que demani l'àrea a l'usuari:

{% sol %}
```ts
let radius = Number(prompt("Radi del cercle?"))
let PI = 3.141592653
let area =  PI* (5 ** 2)

console.log(`L'àrea és ${area}`)
```
{% endsol %}


### Cost

Crea un script `cost.ts` que:

* Demani les dimensions en centímetres d'una capsa
* Calculi el volume de la capsa en centémetres.
* Calculi el cost d'enviar la capsa en funció del volume: 1 euro cada 5000 cm<sub>2</sup> 
* Mostri por consola el cose en euros d'enviar aquesta capsa.

A continuació tens un exemple d'una capsa de `23 x 45 x 10` que té  un coste de envío de 2.07€ :

```sh
$  bun .\cost.ts
Llargada:  23
Amplada:  24
Altura:  56
Cost: 6.1824 €
```

{% sol %}
```ts
const boxLength = Number(prompt("Llargada: "))
const boxWidth = Number(prompt("Amplada: "))
const boxHeight = Number(prompt("Altura: "))

const volum = boxLength * boxWidth * boxHeight
const cost = volum / 5000

alert(`Cost: ${cost} €`)
```
{% endsol %}

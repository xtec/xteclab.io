---
title: TypeScript
icon: typescript.png
description: TypeScript és un dels principals llenguatges per desenvolupar aplicacions web.
--- 

## Fonaments

{% pages ["computation/","algorithm/","sequence/", "function/", "object/"] %}


## Web

{% pages ["bun/", "json-schema", "react/", "vite/"] %}

## Dades

{% pages ["mongodb/", "csv/", "plotly/"] %}







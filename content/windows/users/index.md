---
title: Usuaris
description: Un sistema Windows està compartit per diferents usuaris.
---

## Introducció

En aquesta activitat veurem com es gestionen usuaris en un Windows 11 i en un Windows Server.

## Windows 11

Obre un terminal de {% link "/windows/powershell/" %}.

Crea una màquina Windows 11 amb {% link "/cloud/isard/" %}.

```pwsh
connect-isard users -new -os windows
```


### ACL

* [What is the PowerShell equivalent of ICACLS?](https://www.pdq.com/blog/what-is-the-powershell-equivalent-of-icacls/)


## Windows Server

**TODO** new-azure només crear màquines Ubuntu (pendent)

Obre un terminal de {% link "/windows/powershell/" %}.

Crea una màquina a [Azure](https://azure.microsoft.com/es-es/) tal com s'explica a {% link "/cloud/azure/virtual-machine/" %}.

```pwsh
> new-azure users
```

```pwsh
> connect-azure users
```

Incia una nova sessió de powershell:

```pwsh
> powershell
```

**Important**. Recorda que al final de l'activitat has de borrar la màquina!

```pwsh
> remove-azure users
```

### Usuaris

El mòdul `LocalAccounts` de PowerShell, inclòs a Windows Server per defecte, fa que el procés de gestió d'usuaris i grups locals sigui molt senzill.

Per exemple, per veure tots els usuaris locals en un ordinador específic, executa l'ordre:

```pwsh
> Get-LocalUser
``` 


Per veure els grups locals en un ordinador, executa l'ordre

```pwsh
> Get-LocalGroup
```


Per veure els membres d'un grup específic, utilitza el cmdlet `Get-LocalGroupMember`. 

Per exemple, per esbrinar qui és un membre del grup d'administradors locals, executa l'ordre 
`Get-LocalGroupMember Administrators`.

```pwsh
> Get-LocalGroupMember Administrators
```

Pots veure com l'usuari `users/box` és administrador local de la màquina `users`.

**Podeu crear un usuari nou local** mitjançant el cmdlet `New-LocalUser`. 

Quan crees un nou usuari local, primer crees una variable de contrasenya amb `$Password = Read-Host -AsSecureString` per llegir la contrasenya assignada a l'usuari de manera segura. 

Per exemple, per crear un nou usuari anomenat `Ana`, introdueix les ordres següents:

```pwsh
> $Password = Read-Host -AsSecureString
> New-LocalUser -Name Ana -Description "Ana Vila" -Password $Password
```

Importat heu de seguir les polítiques de seguretat: ha de ser mínim 8, 1 majúscula i 1 caràcter especial

Desde un altra terminal del Powershell mira quina és la IP de la màquina:

```pwsh
> Get-AzPublicIpAddress
``` 

Des del nou terminal connecta't a la màquina virtual amb el nou usuari `Ana`:

```pwsh
> ssh Ana@<IP>
```

### Restablir una contrasenya d'usuari

Escriure una contrasenya des de l'indicador a una variable:

```pwsh
$Password = Read-Host -AsSecureString. 
```

Assignar les propietats del compte d'usuari la contrasenya del qual voleu canviar a una variable:

```pwsh
$UserAccount = Get-LocalUser -Name <AccountName>
```

Assignar la nova contrasenya:

```pwsh
$UserAccount | Set-LocalUser -Password $Password per assignar la nova contrasenya.
```

###

Fes servir el cmdlet `Add-LocalGroupMember` per afegir membres a un grup local. 

Per exemple, per afegir el compte Ana que es va crear a l'últim exemple al grup d'administradors locals, executeu l'ordre:

```pwsh
> Add-LocalGroupMember -Group "Administrators" -Member Ana
```

Pots eliminar usuaris o grups d'un grup local mitjançant el cmdlet `Remove-LocalGroupMember`. 

Per exemple, per eliminar el compte `Anap  del grup d'administradors locals, executa l'ordre:

```pwsh
> Remove-LocalGroupMember -Group "Administrators" -Member Ana
```

Pots obtenir més informació sobre els cmdlets que pots fer servir per gestionar els usuaris i els grups locals en aquest document: [Microsoft.PowerShell.LocalAccounts](https://learn.microsoft.com/es-es/powershell/module/microsoft.powershell.localaccounts/?view=powershell-5.1&WT.mc_id=ITOPSTALK-blog-orthomas).



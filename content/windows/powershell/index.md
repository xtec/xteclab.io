---
title: Powershell
description: PowerShell és una interfície de línia d'ordres per a ordinadors Windows.
---

### Introducció

Per començar una sessió de **Powershell** apreta la tecla Windows **⊞** i escriu `powershell`. 

{% image "shell-launch.png" %}

Pots veure que a més de **Powershell** tens altres opcions que **no** has d'escollir:

1. El "Powershell (x86)" és compatible amb versiones anteriors del sistema operatiu perquè és una versió de 32 bits. El nom X86 fa referència als processadors 286, 386 i 486 dels anys 80 i 90, i després van venir els Pentium, ... i segur que no saps de que estem parlant, per tant, aquesta no!

2. Les que acaben amb **(ISE)** tampoc perqué és per arrencar una IDE i són per escriure scripts, i si vols escriure un script en powershell, abans o després ho tindrás que fer si vol ser una administrador de veritat, per això està {% link "/project/vscode/" %}.

Com acostuma a ser habitual, la finestra de la consola té la lletra molt petita.

Has de fer clic amb el botó dret del ratolí a la barra de dalt de la finestra de la consola per obrir la finestra propietats:

{% image "shell-properties.png" %}

Ja pots modificar el tamany del text, colors, etc.

{% image "shell-properties-change.png" %}

### Activitats

{% pages ["intro/"] %}

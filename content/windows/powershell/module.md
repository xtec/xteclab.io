---
title: Mòdul
---

## Introducció

Un mòdul és una unitat que agrupa un conjunt de cmdlets, proveïdors, funcions, variables, etc.

Per defecte Powershell incorpora un conjunt de mòduls que estan a les carpetes `C:\Program Files\WindowsPowerShell\Modules` i `C:\WINDOWS\system32\WindowsPowerShell\v1.0\Modules`, tal com pots veure a continuació:

```pwsh
> Get-Module -ListAvailable

    Directory: C:\Program Files\WindowsPowerShell\Modules


ModuleType Version    Name                                ExportedCommands
---------- -------    ----                                ----------------
Script     1.0.1      Microsoft.PowerShell.Operation.V... {Get-OperationValidation, Invoke-OperationValidation}
...

  Directory: C:\WINDOWS\system32\WindowsPowerShell\v1.0\Modules


ModuleType Version    Name                                ExportedCommands
---------- -------    ----                                ----------------
Manifest   1.0.0.0    AppBackgroundTask                   {Disable-AppBackgroundTaskDiagnosticLog, Enable-AppBackgroundTaskDiagnost...
...
```

De manera predeterminada els mòduls instal.lats es carreguen de manera automàtica el primer cop que executes una ordre desde un mòdul.

## Instal.lar un mòdul publicat

Si vols pots instal.lar mòduls nous que estan publicats en un repositori registrat, com és la [Galeria de PowerShell](https://www.powershellgallery.com/).

Hem d'instal.lar l'última versió del proveïdor de paquets NuGet:

```pwsh
> Install-PackageProvider -Name Nuget -Force                                                  
Name                           Version          Source           Summary
----                           -------          ------           -------
nuget                          2.8.5.208        https://onege... NuGet provider for the OneGet meta-packag...

```

### PowerShellGet

El mòdulo `PowerShellGet` s'inclou amb PowerShell 5.0 i versions posteriors, i proporcionen cmdlets per buscar, instal.lar i publicar mòduls de PowerShell en un repositori registrat.

```pwsh
> Get-Module -ListAvailable PowerShellGet


    Directory: C:\Program Files\WindowsPowerShell\Modules


ModuleType Version    Name                                ExportedCommands
---------- -------    ----                                ----------------
Script     1.0.0.1    PowerShellGet                       {Install-Module, Find-Module, Save-Module, Update-Module...}
```

El mòdul `PowerShellGet` proporciona la funció `Module-Install` que permet instal.lar mòduls nous:

```pwsh
> Get-Command Install-Module

CommandType     Name                                               Version    Source
-----------     ----                                               -------    ------
Function        Install-Module                                     1.0.0.1    PowerShellGet
```

Si tens una versió antiga del mòdul, has d'actual.litzar el mòdul amb la versió que hi ha a la Galeria de Powersehll: [PowerShellGet](https://www.powershellgallery.com/packages/PowerShellGet).

Si intentem actualitzar el mòdul amb `Update-Module` no ens deixa perquè no està instal.lat amb `Install-Module`.

```pwsh
> Update-Module -Name PowerShellGet
Update-Module : Module 'PowerShellGet' was not installed by using Install-Module, so it cannot be updated.
At line:1 char:1
+ Update-Module -Name PowerShellGet
+ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : InvalidOperation: (PowerShellGet:String) [Write-Error], WriteErrorException
    + FullyQualifiedErrorId : ModuleNotInstalledUsingInstallModuleCmdlet,Update-Module
```

Instal.lem l'última versió amb `Install-Module` i el parametre `-Force`:

```pwsh
> Install-Module -Name PowerShellGet -Force
```

**Però ara tenim dos!!!**

```pwsh
> Get-Module -ListAvailable PowerShellGet


    Directory: C:\Program Files\WindowsPowerShell\Modules


ModuleType Version    Name                                ExportedCommands
---------- -------    ----                                ----------------
Script     2.2.5      PowerShellGet                       {Find-Command, Find-DSCResource, Find-Module, Fi...
Script     1.0.0.1    PowerShellGet                       {Install-Module, Find-Module, Save-Module, Updat...
```

El que hem de fer és eliminar la versió antiga manualment!.

Els mòduls `PowerShellGet` estan instal.lats a la carpeta `C:\Program Files\WindowsPowerShell\Modules\PowerShellGet`:

```pwsh
>  Get-ChildItem "C:\Program Files\WindowsPowerShell\Modules\PowerShellGet"


    Directory: C:\Program Files\WindowsPowerShell\Modules\PowerShellGet


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----        19/04/2024     21:34                1.0.0.1
d-----        05/05/2024     11:04                2.2.5
```

Elimar el mòdul àntic que ve preinstal.lat:

```pwsh
> Remove-Item -Force -Recurse "C:\Program Files\WindowsPowerShell\Modules\PowerShellGet\1.0.0.1"
```

Executa el mètode `Get-Module` de nou per veure si la versió antiga ha desaparegut.

#### Activitat

Fes el mateix amb el mòdule `PackageManagement`


https://gist.github.com/adojos/4c9aa1b81f09141f89f37abbdde49e62



### Microsoft.PowerShell.PSResourceGet

El mòdulo `Microsoft.PowerShell.PSResourceGet` s'inclou amb PowerShell 7.4 i versions posteriores, i és l'administrador de paquests que hauries de fer servir enlloc de `PowerShellGet`.

Microsoft.PowerShell.PSResourceGet es el nuevo administrador de paquetes preferido para PowerShell y se puede instalar en versiones anteriores de PowerShell. 

Use el Install-Module cmdlet o Install-PSResource para instalar módulos desde el Galería de PowerShell.

TODO [about_Modules]https://learn.microsoft.com/es-es/powershell/module/microsoft.powershell.core/about/about_modules?view=powershell-7.4

## Crear un mòdul

**TODO** [Debugging Powershell Modules in VS Code](https://essenceofcode.com/2022/05/05/debugging-powershell-modules-in-vs-code/)

`Install-Module` només instal.la paquets Nuget (`*.nupkg`) que troba en la teva llista de repositoris PowerShell.

A continuació registrarem una carpeta local com un `PSRepository` i crearem un paquet amb `Publish-Module` de tal manera que `Install-Module` trobará el paqut i l'instal.larà.

Recorda de modificar `<user>` amb el nom de la teva carpeta.

These steps will register your local folder as a PSRepository and Package your PowerShell module so that Install-Module will find the package and install it. Be sure to replace <user> with your actual username directory.


`Install-Module` only installs NuGet package (*.nupkg) files that it finds in your list of PowerShell repositories. You need to use `Publish-Module` in order to generate the NuGet package. Here are some cheat sheet learnings and steps needed:


These steps will register your local folder as a PSRepository and Package your PowerShell module so that Install-Module will find the package and install it. Be sure to replace <user> with your actual username directory.


Learnings:

* `Import-Module` is session based.
* `Install-Module` makes it available to every session, but it only installs NuGet package files.
* You can install for everyone on the server or just for you
    Scope AllUsers 	this will install the module's files for everyone to "C:\Program Files\WindowsPowerShell\Modules\<version>\*"
    Scope CurrentUser 	(default) this will install the module's files just for you to "C:\Users\<username>\Documents\WindowsPowerShell\Modules\<version>\*"
* `Publish-Module` will help you create a NuGet package file (You may be asking yourself: why didn't they call it Package-Module? It’s because Package isn't an approved verb! Publish is).

* `$env:PSModulePath` will give you the search order where Install-Module will look for NuGet packages.
* `-PSRepository` allows you to manage list of PSRepositories (default is just the PSGallery).


TODO [Creating PowerShell Moduel](https://valoremreply.com/post/creatingapowershellmodule/)

### Powershell Gallery

Crea un compte a [Powershell Gallery](https://www.powershellgallery.com/).

T'has de registrar i obtenir una clau.


## Recursos

* [Update PowerShellGet and PackageManagement](https://www.thomasmaurer.ch/2019/02/update-powershellget-and-packagemanagement/)
* [Install NuGet Powershell](https://ironpdf.com/blog/net-help/install-nuget-powershell-tutorial/)

* [Powershell: Building a Module, one microstep at a time](https://powershellexplained.com/2017-05-27-Powershell-module-building-basics/)
* [Powershell: Let's build the CI/CD pipeline for a new module](https://powershellexplained.com/2017-01-21-powershell-module-continious-delivery-pipeline/?utm_source=blog&utm_medium=blog&utm_content=body&utm_content=module)
* [Creating a PowerShell Module](https://benheater.com/creating-a-powershell-module/)


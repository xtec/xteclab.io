---
title: Test
---

## Instal.lar

[Installation and Update](https://pester.dev/docs/introduction/installation)

You can install Pester using the built-in `Install-Module` command. The examples below will install Pester in your [default installation scope](https://learn.microsoft.com/en-us/powershell/module/powershellget/install-module?view=powershellget-3.x#-scope) depending on your PowerShell-version. You can control this using the `-Scope <AllUsers/CurrentUser>` parameter.

Windows 10 / Windows Server 2016 and later ships with Pester version 3.4.0. This built-in version cannot be updated using the simple `Update-Module` cmdlet. Instead you need to perform a side-by-side installation to get started with the latest version of Pester.

The built-in version is signed by Microsoft while newer versions are community-maintained and signed with a different certificate, causing `Install-Module` to sometimes throw a error requiring us to accept the new publisher certificate.

Run the command below to install the latest version:

```pwsh
# -Force to install side-by-side
# -SkipPublisherCheck to accept the newer certificate

Install-Module -Name Pester -Force -SkipPublisherCheck
```

For any subsequent updates it is enough to run:

```pwsh
Update-Module -Name Pester
```
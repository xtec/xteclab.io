---
title: Introducció
mermaid: true
description: Powershell et permet gestionar un Windows mitjançant un conjunt de cmdlets.
---

## Introducció

Aquesta sessió la faràs directament amb l’ordinador amfitrió perquè no necessitem permisos d’administrador.

Windows PowerShell és una interfície de línia d'ordres per a ordinadors Windows. Una interfície de línia d'ordres (**CLI**, per les sigles en anglès) és un programa que et permet fer que el teu ordinador executi tasques utilitzant ordres escrites en comptes de fer clic sobre les imatges a l'escriptori com en una interfície gràfica d'usuari (GUI, per les sigles en anglès). Tècnicament, PowerShell és més que només una CLI. 

L'ús de la línia d'ordres té molts avantatges. Fa possible automatitzar tasques i fer moltes coses amb una ordre.

### Versió

Pot ser que la versió de powershell que està instal.lada al Windows no sigui de les últimes:

```pwsh
> $PSVersionTable

Name                           Value
----                           -----
PSVersion                      5.1.19041.4648
PSEdition                      Desktop
PSCompatibleVersions           {1.0, 2.0, 3.0, 4.0...}
BuildVersion                   10.0.19041.4648
CLRVersion                     4.0.30319.42000
WSManStackVersion              3.0
PSRemotingProtocolVersion      2.3
SerializationVersion           1.1.0.1
```

Si la versió no és una 7, com passa a l'ordinador de l'institut, has d’instal.lar un PowerShell local amb {% link "/tool/scoop/" %}.

Com que segur ja has utilitzat l'script {% link "/tool/box/" %} que està escrit en Powershell ja tens `scoop` instal.lat.

```pwsh
> scoop install pwsh

Installing 'pwsh' (7.4.4) [64bit] from 'main' bucket
PowerShell-7.4.4-win-x64.zip (105,3 MB) [===========================================================] 100%
...
```

Ara ja pots iniciar una sessió de Powershell 7:

```pwsh
> pwsh
PowerShell 7.4.4
```

**Nota**. Cada cop que obris una sessió de Powershell tens que executar l'ordre `pwsh` per utilitzar una versió 7.

## Sistema de fitxers

### Navegació

Una cosa bona de PowerShell és que sempre sabràs on ets perquè t'ho diu al prompt. 

En el meu cas, jo veig:

```pwsh
C:\Usuaris\david> 
```

Has de veure una cosa similar però amb el vostre nom d'usuari. 

En cas que no sigui així, escriu:

```pwsh
> sl ~
```

Assegureu-vos d'incloure l'espai. Això us portarà al vostre directori personal: `C:\Usuaris\david` on `david` es reemplaça amb el nom del teu compte a la màquina. 

"Directori" és només una altra paraula per a "carpeta", i PowerShell considera la teva carpeta d' usuari com a inici - no l' escriptori. 

L'escriptori és realment una altra carpeta dins de la vostra carpeta d'usuari, és a dir, un subdirectori del directori usuari. 

Introduir `sl ~` és com obrir la carpeta anomenada “usuaris” i des d'aquí l'usuari (ex. david) utilitzant la GUI. 

Comencem aprenent com moure't entre els directoris i veure'n el contingut.

```pwsh
C:\Usuaris\dymunozc> cd..
C:\Usuaris> sl ~
C:\Usuaris\dymunozc>
```

### Veure contingut del directori amb `Get-ChildItem` (gci, ls)

La nostra primera ordre és `Get-ChildItem`. 

Escriviu-la i premeu Enter. Veureu una llista de tot el que hi ha al vostre directori actual. 

En el meu cas es veurà així:

```pwsh
PS C:\Users\david> gci o dir o ls

	Directorio: C:\Users\david

Mode             	LastWriteTime     	Length Name
----             	-------------     	------ ----
d-----    	19/03/2023 	19:19            	.box
d-----    	13/05/2023 	10:39            	.ssh
d-----    	13/05/2023   9:53            	.ssh2
d-----    	03/09/2023 	10:35            	.VirtualBox
d-r---    	23/07/2021   8:47              Contacts
d-----    	05/06/2020 	17:26            	Documents
d-r---    	01/09/2023 	12:04            	Downloads
d-r---    	23/07/2021   8:47            	Favorites
d-r---    	23/07/2021   8:47            	Links
...
```

Les ordres que aprendrem són totes de la forma “Verbo-Substantiu” (verb-substantiu). Són anomenats cmdlets (pronunciat commandlets) i se suposa que la seva forma fa més fàcil recordar el que fan i predir altres cmdlets similars. 

Com que els cmdlets són força llargs, la majoria tenen àlies més elegants que pots utilitzar al seu lloc. 

Per exemple, enlloc de `Get-ChildItem` pots escriure `gci` que és més curt.

Primer presentaré els cmdlets amb els seus noms, però sempre faré servir els àlies estàndard, perquè són molt més ràpids per treballar. 

És important tenir en compte que molts cmdlets tenen diversos àlies. 

Per exemple, `Get-ChildItem`, `gci`, `dir` i `ls`, fan exactament el mateix. 

Encara que no sorprèn que `gci` sigui l'abreviatura de `Get-ChildItem`, és possible que et preguntis d'on provenen `dir` i ls.

PowerShell és relativament nou (es va llançar per primera vegada el 2006), i els seus dissenyadors esperaven que moltes persones que l'utilitzarien ja tindrien experiència amb algunes CLI existents (interfícies de línia d'ordres), específicament amb el CLI més antic de Microsoft anomenat Símbol de sistema (Command Prompt) o amb Linux CLIs com Bash, que ara també és estàndard en OS X. 

Per tant, molts cmdlets tenen un àlies que és l'ordre estàndard en un d'aquests dos sistemes (i sovint per a tots dos), i que fa que els usuaris d'aquests sistemes puguin seguir treballant amb les ordres en que estan habituats sense problemes. 

En aquest exemple, `dir` es fa servir en el **Command Prompt** de Windows, i `ls` de Linux.

Utilitzaré els àlies d'estil “PowerShell” en aquesta activitat, ja que fa més fàcil recordar els noms reals de cmdlet. Tanmateix, intentaré també esmentar altres àlies comunes, particularment aquells familiars per als usuaris de Bash. Si treballes amb molta gent que fa servir OS X o Linux, pot ser bo conèixer aquests noms.

Segueix endavant i intenta fer servir `gci`, `dir` i `ls`. Obtindràs exactament la mateixa llista de coses. La majoria daquestes coses seran directoris. 

Un ha de ser el teu escriptori. Anem a entrar en aquest directori.


### Navegar entre directoris amb `Set-Location` ( sl, cd)

Per desplaçar-te als teus documents, farem servir el cmdlet `Set-Location`. 

Escriu a PowerShell:

```pwsh
> sl documents
```

Això us indica a PowerShell que es mogui a la carpeta `Documents`. 

Observa que pots escriure `documents` usant totes les lletres minúscules, encara que quan vas veure el contingut del directori david, `Documents` es va escriure amb una `D` majúscula. 

A diferència de Linux, **PowerShell no distingeix entre majúscules i minúscules**. 

Ara que heu canviat la vostra ubicació, podeu utilitzar `gci` per veure una llista de tot el que hi ha al teu escriptori, és a dir, tot el directori anomenat `Documents`.

```pwsh
> gci
...
```

Podem tornar al directori `davidp  escrivint:

```pwsh
> sl ..
```

No oblideu l'espai! Ara escriu de nou:

```pwsh
> sl ..
```

Has d'estar al directori `Usuaris`.

Ara tracta de tornar a `documents` i després de nou a `Usuaris`.

```pwsh
> sl david
> sl documents
> sl ..
> sl ..
```

Però en realitat ho pots fer amb només dos ordres. 

Ara mateix has d'estar a `C:\Usuaris>`.

En comptes d'escriure `sl david` i després `sl documents`, pots escriure només:

```pwsh
A sl david\documents
```
I arribar a `Documents` amb una ordre! 

De la mateixa manera, des de `Documents`, escrivint:

```pwsh
> sl ..\..
```
Pots tornar on vas començar amb una ordre.

A l'escriure una ruta també pots fer servir `/` enlloc de `\`: és més fàcil i és la forma que fem servir a Linux.

Per exemple pots escriure `sl ../..`, perqué no només PowerShell no distingeix entre majúscules i minúscules, sinó que tampoc li importa en quina direcció va la barra.

Per a Powershell tot això és el mateix: `Sl ../..` ,  `sL..\..`,  `Set-Location ..\..` i `sEt-LoCaTioN ../..`. 

**Recorda que amb Linux només pots fer servir `/`, i distingeix entre majúscules i minúscules**.

### Creació de nous directoris amb `mkdir`

Abans de començar, fem un directori on puguem emmagatzemar tot el que estem usant per a aquesta activitat. 
 
Navega de tornada a casa escrivint:

```pwsh
> sl ~ 
```

Farem un nou directori dins del directori `david`. 

Per fer-ho, utilitzarem l'ordre `mkdir`. Posa el nom que vulguis al teu directori, però tracta de no fer servir espais, ja que fan que treballar en línia d'ordres sigui més complicat del necessari.

Si vull un directori amb el nom de [Taj Mahal](https://ca.wikipedia.org/wiki/Taj_Mahal):

```pwsh
> mkdir tajMahal
```

Vesu com utilitzo [CamelCase](https://es.wikipedia.org/wiki/Camel_case) per evitar els espais?

Una altra forma comuna d'evitar espais és insesrint un guió o guió baix: `taj-majal` o `taj_mahal`.
Sigui quin sigui el nom del vostre directori, intenta evitar l'ús d'espais. Quan has estat treballant amb PowerShell una mica, probablement et trobaràs anomenant al teu nous arxius sense espais per defecte. Aquest és un bon hàbit ja que simplifica el treball a la línia d'ordres, així com en treballar amb llenguatges de programació com Python.


No obstant això, és probable que tinguis un munt de fitxers ja existents amb espais en els seus noms. Per obrir-los a PowerShell, només has d'utilitzar cometes. 

Mou-te al nou directori i mira el contingut:

```pwsh
> sl tajMahal
> gci
```
I veuràs que no hi ha res aquí. Això és perquè no hi has posat res! Posarem un nou directori dins amb mkdir. 

Anomenarem a aquest directori "Directori amb un nom llarg i molts espais". 

Com que el nom té espais, haurem d'usar cometes per crear-lo.

```pwsh
> mkdir "Directori amb un nom llarg i molts espais"
```

Prem Enter. Ara escriu:

```
PS C:\Users\david\tajMahal> gci

    Directory: C:\Users\david\tajMahal

Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----        11/08/2024      9:24                Directori amb un nom llarg i molts espais
```

I veuràs el teu nou directori. Suposem que volem moure'ns a aquest directori. Hauríem d'escriure sl "Directori amb un nom llarg i molts espais". No només trigarà un temps a escriure'l sinó que, si ens equivoquem, PowerShell no podrà trobar el nostre directori. 

En el seu lloc, escriu simplement:

```pwsh
> sl d
```

I apreta la tecla `Tab` !

Voilà! PowerShell completa el nom del directori per nosaltres, incloses les cometes! 

```pwsh
> sl '.\Directori amb un nom llarg i molts espais\'
```

L'ús del tabulador per completar automàticament us estalviarà molt de temps. 

Notaràs que quan PowerShell va completar el nom, també va posar `.\` al principi del nom del directori. El punt només és una abreviatura de directori actual.

Quan escrius ordres, PowerShell sempre assumeix que hi ha un  `.\` al principi - en altres paraules, que t'estàs referint a alguna cosa al directori actual. Per tant, no cal que escriguis aquesta part, tret que vulguis que PowerShell busqui en un altre lloc el que estàs demanant que faci, i en aquest cas pots escriure la ruta d'aquest directori. 

Per exemple: 

```pwsh
> gci C:\Windows
```

### Ús d'Explorer per veure directoris a la GUI

Fins ara hem fet dos directoris. He esmentat anteriorment que “directori” és només una altra paraula per a “carpeta”. Pots veure'l en mirar els teus nous directoris a la GUI. Windows anomena al vostre GUI “Explorador de fitxers” o simplement “Explorador”. 

Podem arrencar l'Explorador des de PowerShell utilitzant l'ordre `Explorer`. 

Torna a la carpeta `tajMahal` amb:

```pwsh
sl ..
```

Ara escriu:

```pwsh
> explorer .
```

Recorda que el punt només significa “aquest directori”, i no has d'escriure amb majúscula “explorer” perquè les majúscules no importen a PowerShell. 

Explorador hauria d'haver obert una finestra que mostra el contingut del directori `tajMahal`. Organitza les finestres perquè puguis veure tant la imatge a Explorador com a PowerShell. 

{% image "explorer.png" %}

Crea una nova carpeta desde Powershell amb el nom `Pictures` i observa com aquesta apareix al mateix temps a la finestra d'"Explorer".

Pots veure que el que fas a PowerShell apareix a l'Explorador. 

La comanda `Explorer` és extremadament útil. Bàsicament, és com fer doble clic a la GUI. De manera que el podeu utilitzar per obrir fitxers i programes.

### Eliminació amb `Remove-Item` (rm)

Ara que pots veure els resultats del que fas a PowerShell, aprenguem a esborrar coses, per exemple, aquell directori amb el nom llarg. 

Primer crearem alguns directoris més amb el nom `dir`, `dir1` i `dir2`.

Pots crear els tres amb una sola ordre:

```pwsh
> mkdir dir, dir1, dir2
...
> gci

    Directory: C:\Users\david\tajMahal

Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----        11/08/2024      9:45                dir
d-----        11/08/2024      9:45                dir1
d-----        11/08/2024      9:45                dir2
d-----        11/08/2024      9:24                Directori amb un nom llarg i molts espais
d-----        11/08/2024      9:38                Pictures
```

Ara ens desferem d'aquest directori amb el nom llarg. Per això utilitzarem el cmdlet `Remove-Item` o `rm`.
 
Has de ser molt curós amb aquest cmdlet doncs no transfereix els ítems esborrats a la paperera de reciclatge, sinó que els elimina de manera permanent, així que ho pots considerar esborrat sense possibilitat de recuperar-lo. 
 
Escriu `rm` seguit d'un espai i el nom llarg del directori del qual volem desfer-nos. 

Potser voleu utilitzar la tecla de tabulador per completar automàticament el nom. No obstant això, tingues en compte que, com que ara tenim diversos directoris que comencen amb la lletra “d”, hauràs d'escriure alguna cosa més que la primera lletra perquè es completi automàticament. 

Escriu:

```pwsh
> rm dire
```

I tabulador `⇥` i enter `⏎`.

De manera alternativa, pots escriure només `rm` i prémer la tecla de tabulador `⇥` diverses vegades per desplaçar-te per tots els teus directoris. 

Si avances més enllà del que t'interessa, manten apretada la tecla majúscula `⇧` i apreta la tecla de tabulador `⇥` per desplaçar-te cap enrere.

Abans de pressionar la tecla Entra, jo observo amb atenció el que vaig escriure per assegurar-me que estic esborrant l'ítem que vull eliminar. Només llavors faig clic a Entra.

Esborra els altres directoris i observa com desapareixen amb l'ordre gci. 

Igual que amb `mkdir`, pots esborrar els quatre directoris d'una sola vegada amb una ordre.

Intenta-ho.

{% sol %}
```pwsh
> rm dir, dir1, dir2, pictures
> gci
```
{% endsol %}

Acabem d'eliminar els directoris `dir`, `dir1` i `dir2`, però resulta que els necessitem per al següent exemple. Així que els crearem de nou. 

Però ara, en lloc d'escriure la instrucció, oprimirem la fletxa cap amunt del teclat un parell de vegades (o les que siguin necessàries). En algun punt hauràs de veure la comanda que vas fer servir per crear els tres directoris la primera vegada. 

Quan trobis aquesta línia prem Enter i es tornaran a crear.

De la mateixa manera que fer servir el tabulador `⇥` per completar automàticament, l'ús de les fletxes amunt i avall per desplaçar-te per les ordres recents t'estalviarà molt de temps.

**Tingues en compte que no estem desfent l'esborrat que vam fer amb anterioritat**. Per contra, estem usant un “accés directe” per tornar a ingressar una ordre que hem fet servir recentment.

### Entendre l'estructura d'arbre del sistema de fitxers de l'ordinador

Ara has de tenir tres directoris dins del teu directori `tajMahal`. 

Desplaça't a l'interior del directori `dir` amb `sl dir`.

```pwsh
PS C:\Users\david\tajMahal\dir>
```
Observa la ruta al teu directori actual.

<pre class="mermaid">
flowchart LR
    C: --> Users
    Users --> david
    david --> tajMahal
    tajMahal --> dir
</pre>

El que representa aquesta ruta en realitat és una structura semblant a un arbre que segueix l'ordinador per arribar al punt on ets. 

El tronc de l'arbre és `C:`, que és el teu disc dur. En realitat, a la majoria dels ordinadors moderns `C:` és una partició del disc dur. 

Per què es diu `C`? L'ordinador assigna una lletra a cadascuna de les unitats. `A` i `B` estan reservats per a les dues unitats de disquets que fa molt de temps utilitzaven sovint els usuaris per interactuar amb els discos durs dels seus ordinadors. Tot i que la majoria dels ordinadors ja no en tenen, els noms van quedar reservats.

I `C:` és el tronc de l'arbre, cada secció de la ruta després de `C:` és una branca, de la qual en surten unes altres que estan per sobre. 

Així, `Usuaris` és una branca de `C:`, `david` és una branca més petita que surt de `Usuaris` i així successivament. 

També es pot fer servir la metàfora de l'herència en lloc de la de la botànica i anomenar a cada branca un fill del directori per sobre. Aquest és el llenguatge més comú per descriure les relacions entre els directoris (d'aquí el cmdlet `Get-ChildItem`), però ens quedarem amb la metàfora de l'arbre ja que, a la vida real, les relacions d'herència poden ser molt més complexes que l'extremadament jeràrquica estructura segons la qual està organitzat el teu ordinador.

Entendre que la ruta funciona com un arbre és important per poder navegar pels directoris que no estan immediatament per sobre o per sota del vostre directori actual. 

Sabem que hi ha un directori anomenat `dir1`, i que aquest directori també és al directori `tajMahal`. 

Mira que passa si intentes fer servir `sl` per anar al directori directament escrivint:

```pwsh
> sl dir1
sl : Cannot find path 'C:\Users\david\tajMahal\dir\dir1' because it does not exist.
At line:1 char:1
+ sl dir1
+ ~~~~~~~
    + CategoryInfo          : ObjectNotFound: (C:\Users\david\tajMahal\dir\dir1:String) [Set-Location], Item
   NotFoundException
    + FullyQualifiedErrorId : PathNotFound,Microsoft.PowerShell.Commands.SetLocationCommand
```

Tenim un error per intentar saltar entre branques!

{% image "squirrel-jumping.jpg" %}

Ja saps que no pots fer com els esquirols 😔, Powershell busca `C:\Users\david\tajMahal\dir\dir1` i no el troba .

El problema és que intentem saltar d'una branca a una altra i PowerShell només entén el nostre moviment si ens desplacem al llarg de l'arbre.

<pre class="mermaid">
flowchart LR
    C: --> Users
    Users --> david
    david --> tajMahal
    tajMahal --> dir
    tajMahal --> dir1
    tajMahal --> dir2
</pre>

Això significa que primer ens hem de moure fins on es troben les branques de `dir1` i `dir`, i després tornar a `dir1`. 

Pots fer-ho amb una ordre:

```pwsh
> sl ../dir1
```

Això indica a PowerShell pujar un directori a `tajMahal`, i després baixar al directori `dir1`.

### Moure's ràpid amb `Push-Location` (pushd) i `Pop-Location` (popd)

Abans de treballar amb arxius provarem les ordres `pushd` i `popd`. 

Fes el següent: vés fins al tronc de l'arbre `C:` .Han de ser quatre directoris a dalt del directori on ets, per la qual cosa pots escriure:

```pwsh
PS C:\Users\david\tajMahal\dir1> sl ../../../..
PS C:\>
```

Torna de nou a `dir1`, però en comptes d'utilitzar `sl` fes servir `pushd`:

```pwsh
PS C:\> pushd users/david/tajMahal/dir
PS C:\users\david\tajMahal\dir>
``` 

Ha passat el mateix que amb `Set-Location` excepte que pots tornar on estaves abans amb `popd`:

```pwsh
PS C:\users\david\tajMahal\dir> popd
PS C:\>
```
L'ordre `pushd` indica a PowerShell que es mou a un directori determinat des del teu directori actual al qual pots ser tornat amb `popd`. En altres paraules, `popd` sempre et tornarà a l'últim directori on vas estar abans d'usar empès. 

L'ús de `pushd` i `popd` és molt útil quan et mous amb freqüència entre dos directoris.

## Treballar amb arxius

Ara que saps com moure't a través del sistema de fitxers de l'ordinador des de la línia d'ordres, treballarem manipulant arxius. 

Començarem per aprendre a crear nous fitxers, copiar-los i moure'ls.

Ves a la carpeta `tajMahal`:

```pwsh
> sl ~/tajMahal
```

### Crear fitxers amb `New-Item` (ni)

Primer, necessitem alguns fitxers per treballar amb ells. 

Fem un nou document de text pla anomenat `exemple.txt`: 

```pwsh
> ni exemple.txt
```

Confirmia, en efecte, que ara tens l'arxiu exemple.txt a més dels teus directoris.

```pwsh
PS C:\users\david\tajmahal> gci

    Directory: C:\users\david\tajmahal

Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----        11/08/2024     10:00                dir
d-----        11/08/2024     10:00                dir1
d-----        11/08/2024     10:00                dir2
-a----        11/08/2024     11:07              0 exemple.txt
```

Necessitem diversos arxius així que, endavant: crea `exemple1.txt` i `exemple2.txt`. 

No et sorprendrà saber que, incloent-hi una coma, ho pots fer amb una sola ordre:

```pwsh
> ni exemple1.txt, exemple2.txt

    Directory: C:\users\david\tajmahal

Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----        11/08/2024     11:08              0 exemple1.txt
-a----        11/08/2024     11:08              0 exemple2.txt
```
### Copiar i moure fitxers amb `Copy-Item` (cp) i `Move-Item` (mv)

Potser hauríem d'haver posat aquests fitxers en un directori. Movem-los. Posem `exemple.txt` a `dir` escrivint:

```pwsh
> mv exemple.txt dir
```
Ara escriu `gci` i veuràs que `exemple.txt` ha desaparegut. 

Entra a `dir` (`sl dir`) i escriu `gci` perquè comprovis que ara hi és! 

També ho pots fer sense canviar de directori escrivint `gci dir` des del directori `tajMahal`. 

Torna a `tajMahal` i mou `exemple1.txt` a `dir1` i `exemple2.txt` a `dir2`.

Ara l'arbre de fitxers té que tenir aquesta forma:

<pre class="mermaid">
flowchart LR
    C: --> Users
    Users --> david
    david --> tajMahal
    tajMahal --> dir
    dir --> id1([exemple.txt])
    tajMahal --> dir1
    dir1 --> id2([exemple1.txt])
    tajMahal --> dir2
    dir2 --> id3([exemple2.txt])
</pre>

També pots utilitzar `mv` per canviar el nom dels ítems. 

Utilitza `sl` per moure't a `dir`. Escriu gci i hauràs de veure el teu fitxer `exemple.txt`. 

És un nom avorrit, així que el canviem a dita.txt:

```pwsh
> mv exemple.txt dita.txt
```

Utilitza `gci` de nou per confirmar que el teu document ara es diu `dita.txt`.

Et sorprendrà que el mateix *cmdlet* s'utilitza tant per moure com per reanomenar fitxers. De fet, l?operació és la mateixa. 

En tots dos casos li estàs dient a l'ordinador que canviï el "nom" de la ubicació del fitxer, és a dir, que canviï la **ruta** que segueix per trobar el fitxer. 

Al primer exemple, la ruta va començar com `C:\Usuaris\david\tajMahal\exemple.txt` i  després va canviar a `C:\Users\david\tajMahal\dir\exemple.txt`

Al segon exemple, la ruta va canviar de `C:\Users\david\tajMahal\dir\exemple.txt` a `C:\Users\david\tajMahal\dir\dita.txt`.

Dit d'una altra manera, en tots dos exemples `mv` només canvia la ruta. No et preocupis si això no et fa cap sentit per ara. 

Només vés amb compte d'escriure correctament les rutes quan utilitzis `mv` perquè, si no ho fas, pots canviar el nom quan el que vols és moure el fitxer, o viceversa.

A més de moure fitxers, també voldríem copiar-los o eliminar-los. 

Per copiar arxius, utilitzem el *cmdlet* `Copy-Item` o `cp`. 

Fem dues còpies de `dita.txt` i anomenem-les `steven.txt` i `susie.txt`:

```pwsh
> cp dita.txt steven.txt
> cp dita.txt susie.txt
```

També podem eliminar aquests dos nous fitxers amb `rm`, igual que vam fer amb els directoris. 

Intenta fer-ho amb una sola ordre. Com sempre, vés amb compte quan utilitzis rm.

{% sol %}
```pwsh
> rm steven.txt, susie.txt
```
{% endsol %}

Adéu Steven i Susie!

### Escriure en fitxers amb `Write-Output` (write, echo) i redirecció

Tenim un arxiu buit al nostre directori `dir`. 

Això no és molt interessant, així que afegirem una mica de contingut. 

Podríem obrir el fitxer al Bloc de notes i modificar-lo així. 

```pwsh
> notepad.exe dita.txt
```

Però també podem afegir contingut amb ordres des de la línia d'ordres.

El *cmdlet que utilitzem per això és `Write-Output`, o simplement `write`_

```pwsh
> write "Si vols estar ben servit, fes-te tu mateix el llit."

Si vols estar ben servit, fes-te tu mateix el llit.
```

PowerShell ha d'imprimir aquesta frase directament a la finestra de la línia d'ordres.

Això és tot el que fa `write`: diu a PowerShell “Imprimeix el que jo escrigui”. 

Això no és gaire útil atès que volem posar aquest text al nostre document. Per això, farem servir alguna cosa anomenada **redirecció**.

Redirecció és una manera de dir a PowerShell que prengui els resultats d'una ordre i els col·loqueu en algun lloc que no sigui a la finestra de PowerShell.

Per redirigir una ordre, posem `>` entre l'ordre i el lloc on volem que vagi la sortida.

En aquest cas, volem que la sortida de la nostra comanda `write` acabi en el fitxer `dita.txt`. 

Així que fem servir la tecla `⇧` per reuperar la declaració, i afegim `> dita.txt` al final:

```pwsh
> write "Si vols estar ben servit, fes-te tu mateix el llit." > dita.txt
```
Quan pressions ``⏎` semblarà que no passa res. Això és degut a que la instrucció `write` has estat redirigida. 

Per veure què és el que realment ha passat, utilitza `gci` per veure el contingut del vostre directori. 


Tingues en compte que la longitud ("Length") de `dita.txt` ja no és `0`. Això és perquè acabem de posar-hi text!

```pwsh
> gci

    Directory: C:\users\david\tajmahal\dir

Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----        11/08/2024     11:30            108 dita.txt
```

### Llegir fitxers amb `Get-Content` (gc, cat)

Ja que `gci` ens mostra que hi ha alguna cosa a l'arxiu, seria bo poder veure quina frase hi vam posar. 

Pots fer-ho amb l'ordre: bloc de `notepad dita.txt`, el que obriria el document al "Bloc de notes".

Però també hi ha un *cmdlet* per imprimir el contingut del fitxer a PowerShell que s'anomena `Get-Content`:

```pwsh
> gc .\dita.txt
Si vols estar ben servit, fes-te tu mateix el llit.
```

I vet aquí la teva frase!

Utilizar `gc` per si mateix és útil, però no és molt més interessant si ho combinem amb la redirecció.

Per començar, podem posar el contingut d'un fitxer en un altre, gairebé igual que copiar un fitxer. 

Ja saps com fer-ho amb `cp`.

Fes una còpia de `dita.txt` a `dita1.txt` utilitzant `cp`.

{% sol %}
```pwsh
> cp dita.txt dita1.txt
```
{% endsol %}

Ara fes un fitxer `dita2.txt` amb el mateix contingut que `dita.txt`, però usant `gc` i redirecció:

```pwsh
> gc .\dita.txt > dita2.txt
> gci

    Directory: C:\users\david\tajmahal\dir

Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----        11/08/2024     11:30            108 dita.txt
-a----        11/08/2024     11:30            108 dita1.txt
-a----        11/08/2024     11:38            108 dita2.txt
```

Per descomptat que això és només una forma més molesta de fer el que ja podem fer amb `cp`. 

Però la diferència en aquests mètodes és substancial perquè en fer servir `gc` podem afegir informació a un fitxer de text sense reemplaçar el que ja és allà, i també podem obtenir el contingut de diversos fitxers de text i posar-los en un altre.

En primer lloc aprendrem a **adjuntar**.

Necessitem alguna cosa que afegir a text així que fes un nou fitxer anomenat `next.txt` i escrivim la frase "Qui de jove no treballa, de vell dorm a la palla." 

Pots crear primer el fitxer amb `ni`, però no cal. Si diem a PowerShell que escrigui en un fitxer que no és al vostre directori, el crear per a tú.

```pwsh
> write "Qui de jove no treballa, de vell dorm a la palla." > next.txt
```

Utilitza `gc` per comprovar que es va crear `next.txt` i que té el contingut que vols.

Ara afegeix el contingut de `next.txt` a `dita.txt` utilitzant `gc` i redirecció.

```pwsh
> gc next.txt > dita.txt
```

Sembla simple, oi?

Però si verifiques el contingt de `dita.txt` amb `gc` pots veure que efectivament vas posar el contingut de `next.txt` a `dita.txt`, però has reemplaçat el contingut que ja hi era i això no és el que volíem fer!

En utilitzar `>`, li vam ordenar a PowerShell que posés el contingut d'un text en un altre i va sobreescriure el que ja hi era. 

Podem arreglar això usant `>>` per al nostre redireccionament en lloc d'un sol `>`. Això diu a PowerShell que afegiu la informació nova:

```pwsh
> gc next.txt >> dita1.txt
```

Utilitza `gc` per comprovar que `dita1.txt` ara té totes dues frases.

### Treballar amb diversos arxius alhora usant caràcters comodí (*)

Ara has de tenir quatre arxius al teu directori, cadascun amb una o dues dites catalanes. 

És possible que hagis perdut la pista del que hi està exactament. Utilitzem gc per comprovar el contingut.

Podríem veure cadascun individualment. Però com que pots haver endevinat es pot mostrar el contingut dels quatre fitxers amb una sola ordre.

```pwsh
gc dita.txt, dita1.txt, dita2.txt, next.txt
```
i obtindràs la frase impresa tres cops. 

Pots fer-ho encara més ràpid:

```pwsh
> gc *.txt
```

El resultat serà exactament el mateix. 

El que fa `*.txt` és a dir-li a PowerShell que trobi tot el que acabi amb `.txt`. El `*` es diu **comodí**, i es pot fer servir per reemplaçar qualsevol part d'un nom de fitxer. 

Escriu `gc dit*` i obtindràs només els textos que comencin amb "dit". 

Atès que els únics fitxers d'aquest directori són els quatre que volem, fins i tot pots escriure `gc *` i obtenir el contingut que ens interessa fent que PowerShell mostri tot el que està al directori.

### Cerques amb `Select-String` (sls)

Per descomptat que no sempre volem veure tot el contingut sinó que voldríem trobar contingut específic. 

En utilitzar `*`, podem buscar diversos fitxers al mateix temps. 

Una de les nostres oracions tenia alguna cosa sobre "llit", no? On va ser això? 

Podem fer servir el *cmdlet* `Select-String` per cercar fragments específics de text:

```pwsh
> sls "llit" *.txt
```
i PowerShell imprimrà totes les línies que continguin aquesta cadena de caràcters de qualsevol fitxer del nostre directori que acabi a `.txt`.

L'ús de `sls` en arxius tan petits com els nostres no ens estalviarà gaire temps comparat amb el que ocuparíem si llegíssim els arxius nosaltres mateixos. Però l'ús d'aquest cmdlet amb més arxius, i més llargs, pot ser extraordinàriament útil.

### Bucles infinits i avortar processos amb control-c

Vegem una tasca més útil que podem aconseguir combinant `gc`, comodins i redirecció. 

Suposem que tenim molts fitxers diferents que volem combinar en un nou fitxer, per exemple, perquè hem descarregat centenars de lletres de cançons que necessitem analitzar i agrupar les d'un sol artista en un fitxer únic. 

Encara que podríem fer-ho especificant-los tots, és a dir, `gc text1, text2, text3 > noutext`, en tenir centenars de textos pot resultar una tasca força molesta. Els comodins serveixen per evitar això.

Concatenarem els nostres quatre textos i col·locarem el resultat en un cinquè text. Potser fer servir `*.txt` pot semblar un manera pràctica. 

**Estem a punt de fer una ximpleria, així que si us plau, llegeix el proper paràgraf abans d'escriure aquesta ordre!**

```pwsh
> gc *.txt > dites.txt
```

Semblarà que el teu ordinador no fa res. Però, a diferència d'altres vegades quan el teu ordinador aparenta que no ha fet res, aquesta vegada el prompt del símbol del sistema no torna a aparèixer.

Si intentes escriure una altra ordre no passarà res. Això és perquè PowerShell encara està treballant en el teu darrer comandament. 

A mesura que fas més i més coses complicades amb PowerShell, és una cosa que de vegades passa -estàs fent suar el teu ordinador!. 

Però, en aquest cas, PowerShell mai deixarà de treballar amb aquesta ordre ja que està a un **bucle infinit**. 

Afortunadament, pots avortar aquesta tasca amb `control-c` (apreta la tecla `ctrl`, i mentres la tens pulsada apreta la tecla `c`)

L'ordre `control-c` és molt útil, ja que de vegades pots quedar atrapat accidentalment en un bucle infinit o, simplement, pots atipar-te d'esperar que el teu ordinador faci certes tasques extremadament llargues.

Si executes la comanda `gci` pots veure com el tamany del fitxer `dites.txt` és considerable.

Com ens quedem atrapats en aquest bucle? Li vam dir a PowerShell que posés tots els arxius que acabessin a `.txt` en un nou arxiu que acaba en `.txt`.

Atès que aquest nou fitxer queia sota la rúbrica de fitxers que l'equip havia de concatenar i afegir a `dites.txt`, ho va afegir. I després, ja que tenia un arxiu `.txt` amb nou contingut, ho va afegir també, generant nout contingut, etc.

Aquest és un excel·lent exemple d'una cosa que sovint oblidem sobre els nostres ordinadors: són extremadament potents i **fan exactament el que els hi has dit, no el que penses que els hi has dit!**. 😳 

Els humans mirem les instruccions i intentem interpretar-les. “Segur que no pensaves que l’ordre era afegir el contingut del text final de nou en si mateix una vegada i una altra per sempre.” 

Els ordinadors, per altra banda, fan exactament el que els diem, sense importar com n'és d'il·lògics els nostres manaments.

A mesura que adquireixis experiència treballant amb la línia d'ordres, et sentiràs desconcertat per les interpretacions excessivament literals de les ordres, però també aprendràs a donar-li instruccions que pot seguir. 

Els bucles infinits s'han d'evitar costi el que costi, però es produiran, i quan ho facin, recorda: `control-c`.

### Especificació de cmdlets amb paràmetres

Hem vist que el teu ordinador necessita que li diguin coses de manera molt exacta.

Afortunadament, PowerShell proporciona mètodes per refinar els cmdlets afegint paràmetres.

Vegem-ne un exemple: utilitza `gci` per comprovar que tens cinc fitxers al teu directori. 

Un d'ells, `dites.txt`:

```pwsh
> gc dites.txt
```

PowerShell començarà a imprimir una quantitat excessiva de text a la pantalla.

És possible que vulguis interrompre el procés amb control-c, però això no és un bucle infinit, només es tracta d'un fitxer molt gran, de manera que pots esperar que tot s'imprimeixi, només que trigarà un temps. 

Al final, pots fer servir el *cmdlet* `clear` si et molesta el gran bloc de text a la pantalla.

El que volem comprovar és que `dites.txt` està compost per les línies dels altres textos, repetides una vegada i una altra. 

Podem fer això mirant només al principi i al final, i per això, afegim un paràmetre al nostre cmdlet.

```pwsh
> gc dites.txt -totalcount 10 
```
Veuràs les primeres 10 línies del teu text.

Assegure't d'incloure el guió, ja que en cas contrari PowerShell no sabrà que `-totalCount` és un paràmetre. 

Ara escriu:

```pwsh
> gc dites.txt -tail 10
```

i veuràs les darreres 10 línies. 

El que hem fet és especificar al nostre *cmdlet* `gc` els paràmetres `-totalCount` i -`tail`.

Gairebé tots els cmdlets poden ser refinats afegint paràmetres com aquest. Però com sabem quins paràmetres estan disponibles?

### Més informació sobre Get-Help

PowerShell no espera que memoritzis tots els paràmetres possibles per a tots els cmdlets. 

Al seu lloc, proporciona una forma senzilla d'enumerar-los utilitzant el cmdlet `Get-Help`:

```pwsh
> Get-Help gc

NAME
    Get-Content

SYNOPSIS
    Gets the content of the item at the specified location.

SYNTAX
...
```

La teva pàgina pot ser lleugerament diferent, però la part important per mirar en aquest moment és la secció anomenada "SYNTAX": aquesta mostra tots els paràmetres que podem afegir a `Get-Conent`.

Si esteu tractant de recordar el nom exacte d'un paràmetre que heu utilitzat abans, aquesta ajuda serà suficient. Tot i això, no ens diu el que realment fan els paràmetres.

Afortunadament, el mateix `Get-Help` té paràmetres i, afegint `-online` al *cmdlet* `Get-Help`, indiquea a PowerShell que demani al vostre navegador obrir una pàgina al portal de Microsoft:

```pwsh
> Get-Help gc -online
```

En aquesta pàgina s'expliquen tots el paràmetres, i és molt millor que ChatGPT 🧐 !

{% image "help.png" %}

En aquest pàgina podts veure la descripció completa dels paràmetres `-totalCount` i `-tail`.

### Solució del problema de bucle infinit amb el paràmetre `-exclude`

Observa novament l'ajuda de `Get-Content` i veuràs que un dels paràmetres és `-exclude`.

Això sona prometedor per tractar amb el nostre problema del bucle infinit. 

La descripció en línia diu: 

"Especifica, como matriz de cadenas, un elemento o elementos que este cmdlet excluye en la operación. El valor de este parámetro califica el parámetro Path."

Pots veure que és una traducció automàtica de l'anglés, i com sol passar en informàtica és difícil d'entendre perquè la traducció és bastant dolenta.

Per això has d'apendre informàtica i llegir documentació directament en anglès 🤨!

Quan escrivim `gc dita.txt`, `dita.txt` és la ruta o "Path". 

En realitat, és una abreviatura de `.\dita.txt`, que al seu torn és una abreviatura de `C:\Usuaris\david\tajMahal\dir\dita.txt`. 

Aquest string diu a l'ordinador el camí a seguir a través de l'estructura del sistema de fitxers, similar a la d'un arbre, per trobar l'arxiu que desitges.

Aleshores, el que l'ajuda ens està dient és que podem ometre elements específics del nostre *cmdlet* `gc` afegint el paràmetre `-exclude` i a continuació la ruta que volem que exclogui. 

Podem utilitzar això per prendre el contingut de tots els nostres arxius `.txt` i posar-los en un nou fitxer sense crear un bucle infinit.

Primer elimina el fitxer `dites.txt` actual amb `rm`. 

Encara que això no és realment necessari, ja que en fer servir un sol `>` en el rediccionament reemplaçaria el contingut actual de totes maneres, però és agradable tenir un inici net. 

A continuació executa:

```pwsh
> gc *.txt -exclude dites.txt > dites.txt
> gc dites.txt
```

Voilà!

### Obtenir més profit dels cmdlets amb Piping

Tenim ara cinc documents al nostre directori. Per tal de poder fer coses realment útils amb ells necessitem una eina més: **piping**. 

Piping és una mena de redirecció, però en lloc de dir-li a PowerShell que col·loqui els resultats d'un *cmdlet* en un altre lloc, li diu que prengui la sortida d'un cmdlet i el feu servir com a entrada per a un altre.

On fem servir `>` per a la redirecció, per a les canalitzacions fem servir `|`.

Obtindrem encara més profit de `gc`, canalitzant els resultats al cmdlet `Measure-Object` (o senzillament `measure`). 

Aquest *cmdlet* té diverses propietats. Per al nostre propòsit, el farem servir per obtenir el nombre de línies, paraules i caràcters als nostres arxius afegint els paràmetres `-line`, `word` i `-character`, o simplement `-l`, -`w` i `-c`. (Amb els paràmetres, només cal escriure el nom adequat per identificar el paràmetre en qüestió):

```pwsh
> gc dita.txt | measure -w

Lines Words Characters Property
----- ----- ---------- --------
		     11     	  
```

Si vols pots obtenir les mateixes dades amb un processador de text, però treballar amb el shell et permet manipular moltes coses alhora i especificar el que vols fer amb molta més precisió. 

En aquest exemple significa que podem contar paraules de diversos fitxers a la vegada i que podem afegir paràmetres adicionals per especificar exactament el que volem.

```pwsh
> gc *.txt | measure -l -w -c

Lines Words Characters Property
----- ----- ---------- --------
   10   106    	498
```

I el que has fet amb cinc fitxers molt petits,també ho prodries fer amb un directori que contingui milers de fitxers llargs. 

Executa `Get-Help measure` per veure totes els paràmetres que tena a la teva disposició. 

Per exemple pots ignorar els espais en blanc amb `-IgnoreWhiteSpace`.

Apreta la tecla `⇧` per recuperar l'última ordre i afegeix `-ignorewhitespace` o `-ig`-

```pwsh
TODO
```

Veurás el mateix recompte però amb menys caràcters, perquè aquesta vegada PowerShell no va comptar els espais.

No pots utilitzar només `-i` ja que podria fer referència a dos paràmetres diferents: `-IgnoreWhiteSpace` i `-InputObject` tal com indica el missatge d'error:

```pwsh
TODO
```

L'avantatge de la precisió és clar sobre l'ús d'un processador de textos, on és difícil determinar si s'ignora o no l'espai en blanc, deixant de banda les possibilitats de canviar funcions segons les teves necessitats.









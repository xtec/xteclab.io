---
title: JSON
---

##

Para aprovechar JSON usando PowerShell, debemos estar familiarizados con dos cmdlets muy importantes: [ConvertTo-JSON](https://learn.microsoft.com/es-es/powershell/module/microsoft.powershell.utility/convertto-json) y [ConvertFrom-JSON](https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/convertfrom-json) .

Si utlizas PowerShell 7, ten en cuenta que se han realizado cambios y adiciones a los cmdlets JSON.

Hay varias formas de manipular archivos JSON en PowerShell. 

A continuación tienes un ejemplo.

Agregamos el contenido JSON a una variable `$countries`:

```pwsh
> $countries = '[         
>>   { "Country":"Netherlands","Capital":"Amsterdam" },
>>   { "Country":"France", "Capital":"Paris" }]'
```

Convertimos el string `$countries` a un objeto PowerShell:

```pwsh
> $countries | ConvertFrom-Json    

Country     Capital
-------     -------
Netherlands Amsterdam
France      Paris
```

También podemos guardar el objeto en una variable para poder manipular los datos.

Por ejemplo, obtener el valor del país de la primera posición (en ese caso `Netherlands`).

```pwsh
> $object = $countries | ConvertFrom-Json
> $object[0].Country
Netherlands
```

Podemos modificar la capital de Francia y convertir los datos de la variable $object al formato JSON.

```pwsh
> $object[1].Capital = 'Marseille'
> $object | ConvertTo-Json
[
    {
        "Country":  "Netherlands",
        "Capital":  "Amsterdam"   
    },
    {
        "Country":  "France",     
        "Capital":  "Marseille"   
    }
]
```


* [](https://techgenix.com/json-with-powershell/)






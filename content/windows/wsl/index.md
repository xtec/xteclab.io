---
title: Windows Subsytem for Linux (WSL)
description: La majoria del usuaris tenen Windows en el seus ordinadors, però els programadors i administradors necessiten utilitzar màquines Linux.
---

## Introducció

El Subsistema de Windows per a Linux (WSL) proprociona una integració molt bona entre Windows i Linux en el mateix equip.

Obre un terminal de {% link "/windows/powershell/" %}.

**1.-** A l'**institut**, has d'actualizar la versió de WSL si és el primer cop que un usuari utilitza l'ordinador:

```pwsh
wsl --update
```

Encara que et demani permisos d'administrador, digues que no, i la instal.lació segueix.

**2.-** En el **teu ordinador** has d'habilitar WSL:

```pwsh
wsl --install
```

Mira la versió de WSL i demés del teu ordinador:

```pwsh
> wsl --version
WSL version: 2.2.4.0
...
```

## Ubuntu

Per instal.lar una màquina virtual no necessites ser administrador.

El primer que has de fer es mirar les màquines que tens disponibles per instal.lar:

```pwsh
> wsl --list --online
The following is a list of valid distributions that can be installed.
Install using 'wsl.exe --install <Distro>'.

NAME                            FRIENDLY NAME
Ubuntu                          Ubuntu
Debian                          Debian GNU/Linux
kali-linux                      Kali Linux Rolling
...
```

A continuació instal.la la màquina `Ubuntu`

```pwsh
> wsl --install -d Ubuntu
```

Et demanen un nom d'usuari i contrasenya (pots utilitzar `sudo` amb el teu usuari)

{% image "install.png" %}

En una màquina WSL tens accés al sistema de fitxers del Windows en el punt de muntatge `/mnt/c/` per la unitat `C:\`, etc.

Per exemple, pots crear un fitxer `hello.txt` en el sistema de fitxers del Windows:

```sh
$ echo "Hola" > /mnt/c/Users/david/Downloads/Hola.txt
```

Pots verificar que el Windows té accés a aquest fitxer:

{% image "hola.png" %}

També pots accedir als serveis de Ubuntu desde el Windows a través de la interfície `localhost`.

Per exemple, pots arrencar un servidor apache:

```sh
$ sudo apt update && sudo apt install -y apache2
```

I amb el navegador del Windows accedir al servidor Apache a l'adreça <http://localhost>

{% image "apache.png" %}

Surt de la màquina amb l'ordre `exit`.

Mira les màquines que tens instal.lades:

```pwsh
> wsl -l -v
  NAME      STATE           VERSION
* Ubuntu    Stopped         2
```

Pots veure que la màquina `Ubuntu` és la màquina per defecte (té l'`*`) i està aturada.

Per evitar consumir recursos quan surts de la màquina aquesta s'atura.

Pots començar una nova sessió des del terminal:

```pwsh
> wsl -d Ubuntu
```

O desde la interfície gràfica:

{% image "ubuntu-start.png" %}

Instal.la {% link "/linux/docker/" %}:

```sh
$ curl -L sh.xtec.dev/docker.sh | sh
$ su - ${USER}
```

Elimina la màquina `Ubuntu`:

```pwsh
> wsl --unregister Ubuntu
```

## Importar i exportar

La millor manera de fer còpies de seguretat o moure les distribucions és a través de les ordres d'exportació i importació

Pots exportar tota la distribució a un tarball mitjançant l'ordre `wsl --export`. Després, pots tornar a importar aquesta distribució a WSL mitjançant l'ordre `wsl --import`, que pot assignar un nom a una nova ubicació d'unitat per a la importació, cosa que us permet fer còpies de seguretat i desar els estats de (o moure) les distribucions de WSL.

Tingues en compte que els serveis de còpia de seguretat tradicionals que fan còpia de seguretat de fitxers a les carpetes d'AppData (com ara Còpies de seguretat de Windows) no faran malbé els fitxers de Linux.

**TODO** Fer activitat corresponent

## Box

Encara que al principi et pot semblar que WSl té una funcionalitat una mica limitada, nosaltres som informàtics i anem a exprimir WSL al màxim !

<p class="fs-1 text-center">😊</p>

La manera més fàcil de gestionar màquines virtual és amb l'eina {% link "/tool/box/" %}.

Un cop has instal.lat "box" tens aquests *cmdlets* per gestionar WSL:

* `Connect-Wsl`. Et connecta a una màquina
* `Get-Wsl`. Et mostra totes les màquines
* `New-Wsl`. Et crea una màquina
* `Remove-Wsl`. Elimina una màquina
* `Start-Wsl`. Arrenca una màquina
* `Stop-Wsl`. Para una màquina.

L'usuari és `box`, la contrasenya és `password` i pertany al grup `sudo`.

Un cop has instal.lat el mòdul Box crea una màquina nova amb el nom `esther`:

```pwsh
> new-wsl esther
Ubuntu : Comprovant el tamany de la nova imatge ... Fet.
Ubuntu : Descarregant la nova imatge (344 MB)... Fet.
Ubuntu : Verificant el hash del fitxer ... Fet.
mars: Important la màquina virtual  ...
Import in progress, this may take a few minutes.
The operation completed successfully.
```

Com que no tenim una imatge a la cache, primer s'ha de descarregar la imatge de Ubuntu Cloud.

Ja et pots connectar a la màquina virtual `esther`:

```pwsh
> connect-wsl esther
mars: Arrencant ... Fet.
```

Ja estàs dins d'una màquina virtual [Ubuntu Noble](https://releases.ubuntu.com/noble/)

Surt de la màquina virtual amb l'ordre `exit`.

Pots veure que encara que tanquis la connexió, la màquina segueix funcionant:

```pwsh
> Get-Wsl
  NAME            STATE           VERSION
* Ubuntu          Stopped         2
  esther          Running         2
```

Si vols pots crear una màquina i connectar-te directament:

```pwsh
> connect-wsl raquel -new
```

Surt de la màquina `raquel`.

Verifica que les dos màquines `esther` i `raquel` estan executant-se:

```pwsh
> Get-Wsl
  NAME            STATE           VERSION
* Ubuntu          Stopped         2
  raquel          Running         2
  esther          Running         2
```

Para la màquina `esther` i elimina la màquina `raquel`:

```pwsh
> stop-wsl esther
> remove-wsl raquel
```

Verifica l'estat de les màquines registrades:

```pwsh
> get-Wsl
  NAME            STATE           VERSION
* Ubuntu          Stopped         2
  esther          Stopped         2
```

Conecta't de nou a la màquina `esther`:

```pwsh
$ connect-wsl esther
```

Instal.la docker:

```sh
$ install-docker
```

Pots verificar que `docker` fuciona correctament:

```sh
$ docker run --rm -d -p 80:80 nginx
```

Obre un navegador a <http://localhost> per veure que el servidor nginx funciona.

Encara que surtis de la màquina el servidor nginx segueix funcionant.

Crea la màquina `laura`:

```pwsh
> new-wsl laura
```

Pots arrencar la màquina sense conectar-te:

```pwsh
> start-wsl laura
```

Això és útil quan tens una servei com apache, etc. dins la màquina que funciona de manera automàtica.

## Xarxa

Pots connectar les màquines entre elles mitjançant {% link "/network/wireguard/" %}.

De moment s'ha de fer a mà.

## Aplicacions

El subsistema de Windows per a Linux (WSL) admet l'execució d'aplicacions GUI de Linux (X11 i Wayland) a Windows d'una manera totalment integrada.

#### Gnome

Instal.la l'[Editor de text de Gnome](https://apps.gnome.org/es/TextEditor/)

```sh
$ sudo apt install -y gnome-text-editor
```

Crea un fitxer `hello.txt` amb l'editor:

```sh
$ gnome-text-editor hello.txt
```

#### Firefox

Instal.la firefox:

```sh
$ sudo snap install firefox
```

Arrenca firefox:

```sh
$ firefox &
```

## USB

Obre un terminal de {% link "/windows/powershell/" %}.

Instal.la [usbipd-win](https://github.com/dorssel/usbipd-win) (necessites permisos d'administrador)

```pwsh
winget install --interactive --exact dorssel.usbipd-win
```

TODO:

* <https://learn.microsoft.com/es-es/windows/wsl/connect-usb>
* <https://gitlab.com/alelec/wsl-usb-gui>

## Activitats

### Activitats d'aprenentatge 

A continuació tens un conjunt d'activitats que utilitzen un entorn en WSL.

Esculla algunes i crea els entorns corresponents:

**Aplicacions web**

{% pages [ "/web/app/alfresco/", "/web/app/cryptpad/", "/web/app/etherpad/", "/web/app/odoo/", "/web/app/wordpress/" ]  %}


**Based de dades**

{% pages [ "/data/postgres/", "/data/minio/" ]  %}

### Google chrome

Canvia al directori `/tmp` i descarrega el paquet [google-chrome-stable_current_amd64.deb](https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb):

```sh
$ cd /tmp
$ wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
```

Instal.la Google Chrome:

```sh
$ sudo apt install --fix-missing ./google-chrome-stable_current_amd64.deb
```

L'opció `--fix-missing` es fa servir per instal.lar les dependències que falten per instal.lar el paquet.

Configura la màquina Linux perquè pugui accedir a la xarxa WireguardVPN d'Isard tal com s'explica a {% link "/cloud/isard/" %}.

Arrenca Google Chrome:

```sh
$ google-chrome&
```

Pots veure que el navegador pot accedir a la xarxa privada:

{% image "google-chrome.png" %}

Arrenca un navegador des del Windows i verifica que no pots accedir a la xarxa privada, a no ser que també la tinguis configurada en el Windows.

{% image "google-chrome-not.png" %}



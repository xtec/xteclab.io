---
title: Historias de usuario
icon: user-story.png
description: Las historias de usuario son una herramienta que permite a los diseñadores de un producto comunicar-se con los usuarios y con los desarrolladores del producto.
---


## Introducción

Las metodologías ágiles que hemos visto en {% link "/project/scrum" %} y {% link "/project/kanban" %} están pensadas para *desarrollar* productos, **No para diseñar productos**. 

Trabajar en equipo es difícil porque aunque todos digamos lo mismo y leemos lo mismo, muchas veces lo entendemos de forma diferente.

Y es peor cuando te dicen algo que ha dicho otro que ha dicho otro.

**Actividad**. El profesor explicará algo a un alumno al oído, y éste al siguiente, hasta que el último alumno deberá decir que ha dicho el profesor.

**En el mundo profesional, la mayoría de las personas siguen jugando a este juego, salvo que no nos dediquemos a explicar las cosas al oído.**

Lo que hacemos es escribir documentos larguísimos y presentaciones muy formales que damos a otras personas para que realicen una actividad que esa persona interpreta a su manera. Y esta persona utiliza este documento para crear más documentos que va a dar a otras personas que los interpretarán a su manera.

Cuando las personas leen instrucciones escritas, siempre lo interpretan de forma distinta.

Piensa en lo que ocurre cuando lees las instrucciones que da el profesor en las prácticas o los exámenes (si los lees). O los enunciados que debes leer y responder.

## Comprensión compartida

Compartir documentos no es compartir comprensión, porque cada persona entiende el documento de forma diferente.

Si yo tengo una idea en la cabeza y escribo esta idea en un documento, cuando las personas leen este documento es muy probable que se imaginen algo distinto. Y si yo pregunto si todo el mundo está de acuerdo en lo que está escrito todo el mundo dirá que sí.

Como todos sabemos que aunque todo el mundo diga que si, es mejor hablar sobre lo que ha entendido cada uno.

Y como hablar tiene sus limitaciones, tal y como queda demostrado en aquellas reuniones interminables en las que todo el mundo habla y no se decide nada, es mejor **externalizar** lo que estamos pensando mediante dibujos.

{% image "all-agree.png" %}

En el mundo del software utilizamos diagramas de clases y diagramas de casos de uso.

En estas actividades formativas nos dedicamos a dibujar en la pizarra lo que dice un enunciado, que a veces cuesta de entender o está mal escrito, y los alumnos explican lo que entienden, preguntan al profesor, y al final más o menos nos entendemos todos acabando de dibujar lo mismo, aunque algunos estudiantes piensen que el dibujo debería ser algo diferente.

El resultado es que hemos creado una comprensión compartida.

## Historias de usuario

Una historia de usuario es el nexo de unión entre tres roles fundamentales, que describen una funcionalidad que creará valor al usuario si éste la utiliza y le resulta útil:

1. Los **desarrolladores**, que utilizan metodologías ágiles como {% link "/project/scrum/" %} y {% link "/project/kanban/" %} que son muy útiles para desarrollar productos, pero que por sí solas no son sirven para desarrollar un producto que el usuario quiera utilizar o que le guste utilizar.

2. Los **diseñadores** que crean diseños muy bien documentados, que cuando se dan a los desarrolladores pierden toda la esencia del diseño y dejan de ser útiles porque los desarrolladores nunca entienden lo que el diseñador quería que entendieran.

3. Los **usuarios**, que siempre te dicen lo que quieren, pero que en muchos casos no es lo que en realidad quieren. Además, en muchos casos, lo que han dicho no se tiene en cuenta porque los desarrolladores no saben lo que han dicho o piensan que ellos saben mejor lo que quieren los usuarios. 

Al principio, una historia de usuario sólo se compone de una descripción corta, que suele tener esta estructura:

**COMO :rol QUIERO :esto PARA :esto**

Por ejemplo:

"Como profesor, quiero que los alumnos tomen apuntes para que recuerden lo que se les ha enseñado"

Puede pensar que el **PARA** en este caso no es necesario, pero precisamente es lo más importante. 

¿Cuál de estas dos frases describe mejor lo que quiere el usuario?

1. "Como profesor quiero que los alumnos tomen apuntes"
2. "Como profesor quiero que los alumnos recuerden lo que se les ha enseñado"

Los usuarios lo que quieren son resultados y algunos son capaces de decirte exactamente lo que necesitan para obtener los resultados que quieren, y otros no.

**Si diseña un producto sólo pensando en lo que quiere el usuario en lugar de lo que necesita, en muchos casos tu producto no lo utilizará nadie.**

### Usuario

Se suele decir que Henry Ford dijo: **"Si hubiera preguntado a la gente que quería, me hubieran dicho que querían caballos más rápidos"**.  ¿Y por qué no deberían responder otra cosa? 

La mayoría de la gente no diseña productos. No dedican tiempo a pensar los problemas que tienen las soluciones que utilizan, y cómo un producto nuevo podría resolver estos problemas.

Por ejemplo, un caballo necesita un cuidado constante, y no puedes dejarlo aparcado un mes en la calle como haces con un coche, irte de viaje y que esté "operativo" cuando vuelvas. Esto ahora te puede parecer una obviedad, pero en aquel tiempo no.

Los usuarios sólo trabajan en torno a sus problemas, construyendo establos para sus caballos y contratando a gente para cuidarlos. En lugar de pedir algo diferente para resolver sus problemas, piden lo mismo algo mejorado.

**La mayoría de la gente no puede decirnos cómo podemos resolver sus problemas. Y es muy probable que ni siquiera nos pueda decir cuál es su problema**. Y lo peor de todo, la gente es bastante mala para predecir si utilizará un producto y cómo se utiliza, si les proponemos construirlo.

Y que mejor que algunos ejemplos:

* **Atari**. En los años 90 Atari decidió competir con Nintendo en el mercado de las consolas portátiles y preguntó a los usuarios qué querían. Los usuarios dijeron una pantalla más grande y en color y un procesador más potente par tener juegos más llamativos. 

  Vendieron muy pocas consolas porque los usuarios preferían la nueva nintendo más pequeña y en blanco y negro. Era más barata y la batería duraba mucho más tiempo.

  "Como usuario de una consola quiero poder jugar a juegos que me gusten mucho para distraerme muchas horas"

* **iPhone**. Cuando Apple introdujo en el mercado el iPhone, la mayoría de la gente dijo que no servía para nada, que sólo era un iPod que se podía utilizar para realizar llamadas. 

  Además, la batería duraba muy poco, y ¿quién quería un teléfono que debía estar enchufándolo cada dos por tres?

  "Como usuario de un teléfono quiero poder instalar aplicaciones para poder hacer todo lo que hago en el ordenador donde quiera"

* **Nokia** A principios del 2000 el 90% de los teléfonos que se vendían los fabricaba Nokia. En los años 90 unos ingenieros de Nokia mostraron a la dirección un prototipo de Smartphone con un nuevo sistema operativo que se llamaba Symbian. La dirección rechazó la idead porque según ellos los usuarios querían móviles para hacer llamadas y nada más.

  "Como usuario de un teléfono quiero poder comunicar-me por cualquier medio disponible"

* **What's Up** A principios de 2000 What's Up era una aplicación americana que tuvo un gran impacto en Europa, mientras que en Estados Unidos casi no se utilizaba. ¿Cómo es esto posible?

  En Estados Unidos los mensajes multimedia eran gratis, mientras que en Europa las compañías de telefonía cobraban muchísimo por este servicio por considerarlo residual y con poco demanda.

  "Como usuario de un teléfono quiero poder enviar fotografías para poder compartir experiencias visuales"

###  Promesa de una conversación

**Al principio las historias de usuario son cortas porque en un principio lo único que se quiere es priorizar que se desarrolla primero.** 

Cuando se elige una historia, ésta se va desarrollando en dos aspectos fundamentales que deben quedar reflejados en la historia de usuario:

1. Las conversaciones más importantes que ha tenido sobre la historia y que sirven para concretar los detalles de la historia

2. Pruebas que se pueden utilizar para determinar que una historia está completa. 

#### Recordar

El mayor error que puedes cometer es pensar que el objetivo de una historia de usuario es documentar unos requisitos de nuestro producto.

El problema, como hemos visto antes, es que cuando un grupo de gente lee el mismo documento y entienden cosas distintas, no es culpa de quien lo ha escrito ni de quien la ha leído. No es de nadie.

Por ejemplo, la gran diferencia entre la educación a distancia y la educación presencial es que, aunque ambos utilizamos documentos, en la educación presencial hablamos, preguntamos y dibujamos de forma interactiva.

Por tanto, la finalidad no es escribir historias de usuario, sino utilizar historias de usuario como herramienta para hablar, dibujar y acabar pensando todos lo mismo.

Y lo más importante que se ha dicho y dibujado debe guardarse, porque lo que recordamos al cabo de unos días es poco. 

**Recuerda que lo más importante no es lo que está escrito, sino lo que recuerdas cuando lees lo que está escrito.**

Por cierto, en el ámbito educativo a esta actividad se le llama tomar apuntes:

{% image "notes.png" %}

{% youtube "V-BpvNafw_4?si=VPB4dbXggloOC3qp" %}









[Continua - Google Docs](https://docs.google.com/document/d/1N5HI5NgQX1hCr2ArXTLDtDXl5Qd6MHhRgi1a6MGRbMU/edit?usp=drive_link)
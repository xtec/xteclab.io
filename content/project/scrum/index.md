---
title: Scrum
icon: scrum.png
mermaid: true
description: Cuando un grupo de personas se juntan por un trabajo conjunto son necesarias unas reglas básicas para que se obtengan resultados.
---

## Introducción

La mayoría de equipos a nivel profesional utilizan Scrum, y tú debes aprender las reglas básicas de Scrum para poder trabajar en equipo.

En esta actividad formativa utilizaremos el proyecto TIME para aprender los principios básicos de Scrum: <https://gitlab.com/xtec/project/time>

## Rols

Lo primero que debes saber es que al igual que en un equipo de fútbol hay jugadores, entrenadores y directivos, en un equipo Scrum también hay estos tres roles fundamentales.

La gente que creó Scrum pensaba en el rugby, porque a diferencia del fútbol, ​​en el rugby los jugadores se dejan la piel literalmente.

Por tanto, para entender Scrum debemos hablar de un partido de rugby.

{% image "rugby.png" %}

En terminología Scrum existen dos roles fundamentales:

**1.- Developers**

En un partido de rugby el objetivo es ganar y los jugadores deben trabajar en equipo y esforzarse al máximo para conseguir ese objetivo.

El ritmo de un partido de rugby es similar al de un proyecto: reuniones rápidas, reaccionar rápido a los imprevistos, improvisar en muchos casos, etc. con un único objetivo: conseguir puntos.

Desarrollar un proyecto es el mismo, y el equipo de proyecto debe funcionar de forma similar.

**2.- Product Owner**

El product owner es el entrenador.

A diferencia del fútbol, ​​los entrenadores se sitúan en la grada para analizar el desarrollo táctico del partido y verificar que se cumplen los planes de juego.

En todo proyecto se necesita a alguien que lo gestione y que esté fuera del campo de juego para tener una visión global de lo que está pasando.

Ya sabes que muchos jugador piensan que el entrenador no tiene ni idea y que jugarían mejor solos, y muchos entrenadores piensan que los jugadores no saben nada y que lo que deben hacer es callar y hacer lo que les mandan.

Pero la realidad es que los dos roles son necesarios, **y por eso Scrum deja claro que el entrenador es el entrenador y los jugadores son los jugadores**, pero los dos se necesitan el uno al otro

El tercer rol fundamental es el:

**3.- Scrum Master**

Es la persona que ayuda a todo el mundo a entender la teoría, prácticas, reglas y valores del Scrum.

¿Y la directiva? Son los **Stakeholders**, quienes marcan las estrategias de la entidad.

Como puedes ver Scrum no es ninguna novedad, sólo deja claro quién es quien.

## Scrum: Trabajo de equipo

Como estudiantes sabéis por experiencia que trabajar en grupo es complicado porque cada uno quiere hacer las cosas de manera diferente, no se pongan de acuerdo, algunos trabajan menos que los otros, hay compañeros que no responden, pero como es el Instituto no pasa nada.

Pero las empresas necesitan ganar dinero y te pagan, y si un equipo no funciona ya buscarán a otro.

Y el contexto más exigente es un equipo de rugby, donde tus compañeros esperan el máximo de ti y deben ir todos a la una, sobre todo cuando se hace Scrum:

{% image "scrum.png" %}

Y si todavía no tienes idea de que es un Scrum mira este vídeo:

{% youtube "VIfD0nuocVo?si=hBKbAzkH1aZcq6vk" %}

La organización donde trabajará espera al máximo de ustedes y que sepa trabajar en equipo. Nosotros como profesores, queremos que los estudiantes trabajen en equipo cuando hagan un proyecto en grupo.

Y debemos enseñarnos cómo.

**¿Pero cómo podemos conseguir esto? Aplicando la metodología Scrum.**

## Valores

Scrum considera que todos los miembros del equipo deben respetar y creer en **cinco valores fundamentales**.

{% image "values.png" %}

Pero muchos equipos piensan que esto de los valores fundamentales son sólo palabras, y que hay cosas más importantes.

Esto es así porque no juegan a rugby y no han tenido que enfrentarse a la selección de Nueva Zelanda, que es la que más respeta y transmite al campo sus valores fundamentales:

{% image "hakka.png" %}

Y los demás equipos han aprendido a responder a Hakka porque cada equipo tiene sus valores:

{% youtube "vnvI6V-TtLs?si=kyYZfRAn4wwxstXI" %}

{% youtube "yiKFYTFJ_kw?si=SKEUJFSPvNWZ6veG" %}

Lo mismo ocurre en los equipos de trabajo que no respetan los valores fundamentales de Scrum:

No pueden competir con los demás equipos de trabajo que sí respetan los valores fundamentales de Scrum.

## GitLab

Lo primero que debe hacer el Product Owner de tu grupo es crear un nuevo proyecto en {% link "/project/gitlab/" %}.

A continuación tiene que invitar a los otros miembros del equipo.

Modifica el fichero `README.md` y escribe una descripción realista de tu proyecto: [README.md](https://gitlab.com/xtec/project/time/-/blob/main/README.md?ref_type=heads)

## Product Backlog

El "Product Backlog" es un montón de tareas que debes realizar para construir el producto final.

{% image "product-backlog.png" %}

Como ya debes saber la manera es que se hace una tarea es dividiendo la tarea en tareas más pequeñas, y dividir estas tareas en tareas más pequeñas, y… supongo que ya has captado la idea.

{% image "project-calcotada.png" %}

### Product Goal

El proyecto Calçotada está muy bien, pero los alumnos del prueba necesitamos un reto mejor.

¿Qué te parece el proyecto Time?

La primera tarea siempre es el objetivo del proyecto (el **Product Goal**).

Selecciona la opción de crear una nueva tarea (**Issue**):

{% image "new-issue.png" %}

¡Y ya puedes crear la primera tarea!

<https://gitlab.com/xtec/project/time/-/issues/1>


Recuerda que debes cumplir con este objetivo, y si no cumples con ese objetivo el proyecto se considera fallido por mucho esfuerzo que haya dedicado al proyecto.

Los profesores sabemos que a los estudiantes les tenemos que dar un poco de margen, pero en el mundo laboral un proyecto fallido es una inversión perdida y en algunos casos el final de la empresa.

{% image "project-failed.png" %}

### Tareas

A continuación el "Product Owner" debe crear las tareas de segundo nivel que servirán para definir los sprints que se realizarán en el proyecto Scrum.

**¡Recuerda que toda tarea debe ser subtarea de alguna tarea, excepto la primera tarea que es el objetivo del proyecto!**

Haz clic en New Issue y rellena los campos correspondientes:


Y poco a poco vas construyendo el árbol de tareas:

<pre class="mermaid">
flowchart 
  id1[Crear una máquina del tiempo]
  id2[Conseguir 100 procesadores cuánticos]
  id3[Conseguir un mini-reactor nuclear]
  id1 --> id2
  id1 --> id3
</pre>


### Product Backlog

El resultado de crear todas estas tareas es el product backlog, que traducido a lenguaje llano significa:

**Todas las tareas del proyecto pendientes de realizar y que no se han empezado a realizar**.

{% image "issues-top.png" %}

## Sprint

Un Sprint es un **subproyecto** que debe durar pocos minutos en el rugby, y entre **1 y 4 semanas** en nuestro trabajo.

{% image "sprint.png" %}

En Rugby está claro que si no consigues hacer un ensayo el Sprint no habrá servido para nada.

¡Por eso recuerda que aunque el Sprint sea un subproyecto no deja de ser un proyecto!

Por tanto debe dar un **resultado incremental** que sea útil.

{% image "sprint-done.png" %}

### Tablero de trabajo

Scrum no dice nada respecto a tableros porque es una herramienta, no un principio básico.

Para poder trabajar necesita un tablero de trabajo específico para el equipo, y los equipos Scrum normalmente utilizan tableros parecidos a los de {% link "/project/kanban/" %}.

En el tablero puedes ver que en la columna **Open** están todas las tareas pendientes del proyecto:

{% image "issue-board.png" %}

Estar claro que no podemos construir la máquina del tiempo por arte de magia, y de momento no disponemos de una varita mágica para crearla con un abracadabra.

El mundo real es más complejo, pero la solución es muy sencilla.

### Sprint Goal

Lo que debe hacer el "Product Owner" es seleccionar esa tarea de segundo nivel que será el objeto del primer Sprint.

Si queremos hacer muchas cosas al mismo tiempo nunca acabaremos de hacer nada, y Scrum nos dice que debemos centrarnos en un subconjunto de tareas que deben estar relacionadas, y como equipo hacer un scrum.

En nuestro caso seleccionamos la tarea de segundo nivel "Conseguir un mini-reactor nuclear", pero tú puedes elegir otra si quieres.

<pre class="mermaid">
flowchart 
  id1[Crear una máquina del tiempo]
  id2[Conseguir 100 procesadores cuánticos]
  id3[Conseguir un mini-reactor nuclear]
  style id3 fill: #ffe5cc
  id1 --> id2
  id1 --> id3
</pre>

Crea un nuevo "milestone" (equivale a un Sprint):

{% image "milestone-new.png" %}

Rellena los datos pertinentes:

{% image "milestone-edit.png" %}

Añade la issue "Conseguir un mini-reactor nuclear" al milestone:

{% image "milestone-select.png" %}

### Sprint Backlog

Ya sabrás que no es lo mismo conseguir un mini-reactor nuclear que comprar un móvil.

Éste es el motivo por el que es una tarea de segundo nivel y no de cuarto o quinto nivel, y lo primero que debemos hacer es dividir la tarea en subtareas de nivel 3.

<pre class="mermaid">
flowchart 
  id1[Crear una máquina del tiempo]
  id2[Conseguir 100 procesadores cuánticos]
  id3[Conseguir un mini-reactor nuclear]
  style id3 fill: #ffe5cc
  id1 --> id2
  id1 --> id3
  id4[Conseguir autorización]
  id5[Comprar un reactor]
  id3 --> id4
  id3 --> id5

  style id4 fill: #cce5ff
  style id5 fill: #cce5ff
</pre>

**Todos los miembros del equipo** pueden ir dividiendo las tareas en subtareas hasta que estas subtareas sean realizables por el equipo de trabajo.

Por ejemplo, un miembro del equipo puede añadir a la subtarea "Comprar reactor" una subtarea "Negociar con Seaborg":

<pre class="mermaid">
flowchart 
  id1[Crear una máquina del tiempo]
  id2[Conseguir 100 procesadores cuánticos]
  id3[Conseguir un mini-reactor nuclear]
  style id3 fill: #ffe5cc
  id1 --> id2
  id1 --> id3
  id4[Conseguir autorización]
  id5[Comprar un reactor]
  id6[Negociar con Seaborg]
  id3 --> id4
  id3 --> id5
  id5 --> id6
  style id4 fill: #cce5ff
  style id5 fill: #cce5ff
  style id6 fill: #cce5ff
</pre>


Cuando un miembro del equipo considera que ya se puede trabajar con una de las subtareas, la incorpora al inicio de la línea de trabajo del tablero haciendo clic en el icono correspondiente:

{% image "yt-add-card-to-sprint.png" %}

¡Y ya tenemos la tarea al inicio de la línea de trabajo!

{% image "yt-sprint-card-added.png" %}

Si vas a la tarea puedes ver su estado:

{% image "yt-sprint-card-status.png" %}

### Creación continua

En algunas ocasiones se sabe exactamente todo lo que debe hacerse, en otras se sabe más o menos lo que se debe hacer, y lo que es peor, bastantes veces no se sabe mucho lo que se debe hacer.

Ningún problema, al inicio del Sprint no es necesario que estén todas las tareas a realizar en el Sprint Backlog.

Sólo recuerda el principio fundamental de la informática:

**Divide una tarea en tareas más pequeñas, y éstas en otras tareas más pequeñas, hasta que aparezcan las tareas que sí pueden llevarse al tablero de trabajo.**

Pero debe quedar bien claro que, aunque se pueden añadir tareas y modificar tareas, que es lo más habitual:

1. **Nunca** se puede modificar la tarea principal, el Sprint Goal.

2. No se pueden añadir tareas que no sean subtareas, directas o indirectas, del Sprint Goal.

Vosotros como equipo os habéis comprometido a conseguir un objetivo, el Spring Goal.

Cada jugada es diferente, muchas veces pasan cosas inesperadas, hay que adaptarse, cambiar de estrategia, pero si al final no consigue hacer un ensayo, todo el esfuerzo no habrá servido para nada.

{% image "sprint-failed.png" %}

## Documentos

En Scrum hay unos **eventos** obligatorios que deben quedar documentados.

Para eso tienes una wiki:

{% image "wiki.png" %}

### Sprint Planning

El primer documento que debe crear en cada Sprint es el de la planificación del Sprint en el que se debe decidir cuál será el Sprint Goal y definir el trabajo a realizar.

{% image "yt-new-article.png" %}

Todo el equipo Scrum debe participar, y el Product Owner debe asegurarse de que todos los participantes están preparados para poder planificar y trabajar en este Sprint.

El Scrum Team también puede invitar a otras personas a participar en el Sprint Planning y dar consejos.

Este documento se crea en cada Sprint que hagais.

### Daily Scrum

Cada día, lo primero que debeis hacer, es una reunión rápida de máximo 10 o 15 minutos para comentar cómo va el proyecto y planificar que vais a hacer durante el día.

Añade un subartículo:

{% image "yt-daily-scrum.png" %}

### Sprint Review

Una vez terminado el Sprint, debemos valorar los resultados obtenidos y decidir qué hacer a continuación.

### Sprint Retrospective

También debemos hablar de todo lo que no ha funcionado como equipo en este Sprint, y tratar de identificar que debemos hacer para mejorar y cómo podemos hacerlo

{% image "sprint-retrospective.png" %}

## TODO

<https://docs.gitlab.com/user/group/iterations/>
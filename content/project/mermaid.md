---
title: Mermaid
mermaid: true
description: Eina per crear diagrames mitjançant definicions que es renderitzen mitjançant scripts.
---

## Mermaid

[Mermaid](https://mermaid.js.org/).

Arrenca {% link "/project/vscode/" %} i instal.la l'extensió [Markdown Preview Mermaid Support](https://marketplace.visualstudio.com/items?itemName=bierner.markdown-mermaid).

## Git

En aquest enllaç tens explicat com crear gràfics git: [Gitgraph Diagrams](https://mermaid.js.org/syntax/gitgraph.html)

Aquí tens un exemple:

<pre class="mermaid">
gitGraph
   commit
   commit
   branch develop
   checkout develop
   commit
   commit
   checkout main
   merge develop
   commit
   commit
</pre>

A {% link "/project/git/" %} i {% link "/project/gitlab/" %} tens molts exemples amb explicacions.

## UML

UML és el llenguatge de modelatge que s’utilitza de manera genèrica.

Fins l'aparició de les interfícies gràfiques d'usuari la programació orientada a objectes era un paradigme residual, i desde llavors s'ha anat convertint en un paradigme dominant que va començar amb els llenguatges C++ i Java (que té una sintaxi semblant a C).

És per aquest motiu que UML s’adapta molt a C++ i Java, però no a altres llenguatges orientats a objectes com poden ser Python o Javascript.

* [Class Diagrams](https://mermaid.js.org/syntax/classDiagram.html)

* [Explaining Class Diagrams in Mermaid](https://pavolkutaj.medium.com/explaining-class-diagrams-in-mermaid-efc16e72b6e3)

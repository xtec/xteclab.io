---
title: Gestión
description: Gestionar un proyecto ...
---

## Introducción

### README.md

El archivo README es una guía que brinda a los usuarios una descripción detallada de un proyecto en el que han trabajado.

También se puede describir como documentación con pautas sobre cómo utilizar un proyecto. Por lo general, tendrá instrucciones sobre cómo instalar y ejecutar el proyecto.

Cuando escribes el README de tu proyecto, este debe poder responder el **qué**, el **por qué** y el **cómo** del proyecto.

#### Qué incluir en el archivo README

**1.** Título del proyecto

Este es el nombre del proyecto. Describe todo el proyecto en una sola frase y ayuda a las personas a comprender cuál es el objetivo y la meta principal del proyecto.

**2.** Descripción del proyecto

La descripción es un aspecto muy importante de tu proyecto. Una descripción bien redactada te permite mostrar tu trabajo a otros desarrolladores y a posibles empleadores.

La calidad de la descripción de un README suele diferenciar un buen proyecto de uno malo. Un buen proyecto aprovecha la oportunidad para explicar y mostrar:

* Qué hace tu aplicación,
* ¿Por qué utilizaste las tecnologías que utilizaste?
* Algunos de los desafíos que enfrentó y las características que espera implementar en el futuro.

**3.** Tabla de contenidos (opcional)

Si el archivo README es muy largo, es posible que desee agregar una tabla de contenidos para que a los usuarios les resulte más fácil navegar por las distintas secciones. Esto facilitará que los lectores se desplacen por el proyecto con facilidad.

**4.** Cómo instalar y ejecutar el proyecto

Si está trabajando en un proyecto que un usuario necesita instalar o ejecutar localmente en una máquina como un "POS", debe incluir los pasos necesarios para instalar su proyecto y también las dependencias requeridas, si las hubiera.

Proporcione una descripción paso a paso de cómo configurar y poner en funcionamiento el entorno de desarrollo.

**5.** Cómo utilizar el proyecto

Proporcionar instrucciones y ejemplos para que los usuarios y colaboradores puedan utilizar el proyecto. Esto les facilitará las cosas en caso de que surja algún problema: siempre tendrán un lugar donde consultar lo que se espera.

También puede utilizar ayudas visuales incluyendo materiales como capturas de pantalla para mostrar ejemplos del proyecto en ejecución y también la estructura y los principios de diseño utilizados en su proyecto.

Además, si su proyecto requerirá autenticación, como contraseñas o nombres de usuario, esta es una buena sección para incluir las credenciales.

**6.** Incluir créditos

Si trabajaste en el proyecto como equipo o como organización, incluye a tus colaboradores o miembros del equipo. También debes incluir enlaces a sus perfiles de GitHub y redes sociales.

Además, si siguió tutoriales o hizo referencia a cierto material que podría ayudar al usuario a crear ese proyecto en particular, incluya aquí también enlaces a ellos.

Esta es sólo una forma de mostrar su agradecimiento y también para ayudar a otros a obtener una copia de primera mano del proyecto.

**7.** Agregar una licencia

En la mayoría de los archivos README, esta suele considerarse la última parte. 


### CHANGELOG.md

Un registro de cambios ("changelog"), es un archivo que contiene una lista cronológicamente ordenada de los cambios más destacables para cada versión de un proyecto.

El archivo tiene que llamarse `CHANGELOG.md`

Para facilitar a los usuarios y colaboradores ver exactamente qué cambios reseñables se han realizado entre cada versión del proyecto. 


## LICENSE.md

Permite que otros desarrolladores sepan qué pueden y qué no pueden hacer con su proyecto.

Contamos con diferentes tipos de licencias dependiendo del tipo de proyecto en el que estés trabajando. Dependiendo de la que elijas determinará las contribuciones que recibirá tu proyecto.

## TODO

* <https://www.freecodecamp.org/news/how-to-write-a-good-readme-file/>
* <https://keepachangelog.com/es-ES/1.1.0/>
* <https://common-changelog.org/>
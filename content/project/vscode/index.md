---
title: VS Code
description: Visual Studio Code és un editor amb suport per molts llenguatges de programació.
---

## Introducció

???

## Instal.lació

Instal.la {% link "/tool/box/" %} tal com s'explica en el document:

```pwsh
Set-ExecutionPolicy Remote -Scope CurrentUser
Install-Module Box -scope CurrentUser -SkipPublisherCheck
```

Instal.la {% link "/project/vscode/" %}

```pwsh
install-code
```

## Tutorial

La millor manera d'explorar de manera pràctica VS Code és obrir la pàgina **Welcome** i, a continuació, triar un **Walkthrough** per a una visita guiada pels passos de configuració, les funcions i les personalitzacions més profundes que ofereix VS Code. A mesura que descobreixes i aprens, els tutorials fan un seguiment del teu progrés. 

Obriu la pàgina Welcome des del menú **Help** > **Welcome** o utilitzeu l'ordre `Help: Welcome` de la **Command Palette** d'ordres (`Ctrl+Maj+P`).

En aquest enllaç està tota la documentació: [Docs- Getting Started](https://code.visualstudio.com/docs/).


## Remote

VSCode et permet treballar amb màquines remotes des del Windows de la mateixa manera que si ho fessis en la màquina local.

### WSL

Amb Visual Studio Code pots treballar desde Windows amb una màquina WSL.

Crea una màquina Ubuntu tal com s'explica a: {% link "/windows/wsl/" %}.

A continuació crea una carpeta `hello` i obre la carpeta `hello` amb `code`:

```sh
$ mkdir hello
$ code hello
Installing VS Code Server for Linux x64 (eaa41d57266683296de7d118f574d0c2652e1fc4)
Downloading: 100%
...
Compatibility check successful (0)
```

`code` instal.larà una extensió que permet al VSCode que s'executà al Windows comunicar-se amb la màquina virtual Linux.

D'aquesta manera pots treballar en un entorn Linux:

{% image "wsl.png" %}

Pots veure que estas treballant amb una màquina WSL amb el nom de `code` perquè:

1. Al final de nom de la carpeta s'ha afegit `[WSL:Code`]
2. A la cantonada d'abaix a l'esquerra també s'ha afegit `[WSL:Code`]

Si obres un terminal, pots verificar que és el shell de la màquina virtual.

### SSH

Pots conectar VS Code amb una màquina remota.

**Important!** Aquesta activitat només es pot fer en un ordinador en que tinguis permisos d'administrador.

Això és útil perqué d'aquesta manera pots treballar amb una màquina més potent si el teu portatil no té gaire potència, o amb un Internet molt més ràpid que el de l'Institut.

Crea una màquina màquina Ubuntu de 16GB de RAM i 8 CPUs a {% link "/cloud/isard/" %}, i configura el Windows perqué es pugui conectar a la xarxa Wireguard.

Verifica que et pots conecta per ssh a la IP de la màquina:

{% image "windows-isard-ssh.png" %}


**TODO** Explicar com és crea la configuració ..

S’obrirà una nova finestra que et demana que seleccionis la plataforma del host remot.

{% image "remote-ssh-linux.png" %}

Introdueix el "password" de la màquina virtual:

{% image "remote-ssh-password.png" %}

Ja estas conecta't a la màquina virtual:

{% image "remote-ssh-terminal.png" %}

Pots veure que:

* Abaix de la finestra indica que estas treballant amb la màquina virtual

* Si obres un terminal aquest és en el shell de la màquina virtual.

Ja pots treballar amb la màquina virtual.

### Forward

**TODO** Falta explicar com es fa el forward per veure en el navegador una aplicació web.


## TODO

https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack
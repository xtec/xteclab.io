---
title: Kanban
description: Kanban es una metodologia ...
mermaid: true
---

## Principios

Kanban es basa en **tres principios** muchos sencillos:

Visualize       Limit work in process     Mange flow


### Visualizar

Normalmente es difícil saber qué está haciendo un trabajador o un estudiante a menos que le preguntes qué está haciendo.

Y los trabajadores y estudiantes muchas veces no saben que hacer a menos que pregunten a su jefe o al profesor que deben hacer.

El problema es que no tenemos forma de saber con un vistazo qué tareas se están haciendo, quién las está haciendo o qué hacer.

Es una situación absurda pero es la más habitual.

Por eso los japoneses de Toyota inventaron una solución muy sencilla que son las **tarjetas kanban** y uno **sistema pull** de producción:

{% image "card.png" %}

Los informáticos decidimos utilizar el mismo sistema, pero en un entorno de oficina.

Con una pizarra y unas notas adhesivas tenemos suficiente:

{% image "board.png" %}

Cada nota adhesiva es una tarea que lleva anotada a realizar, quien la está haciendo, etc.

{% image "note.png" %}

Y si no estás haciendo nada, más vale que cojas una tarea porque se nota de inmediato que no estás haciendo nada, y no puedes decir que no sabes que tienes que hacer:

Kanbak es un **sistema pull**, nadie te tiene que empujar para que hagas algo (*sistema push*).

Es una idea muy sencilla y útil gracias a las notas adhesivas, que va más allá del entorno laboral, tal y como se puede observar en muchas neveras de casa:

{% image "fridge.png" %}

### Limitar el trabajo en curso (WIP)

Cuando tienes que hacer muchas cosas a la vez, al final no acabas haciendo nada.

{% image "overwhelmed.png" %}

Y cuando un grupo intenta hacer muchas cosas a la vez no es que no se acabe haciendo nada, es que el caos es absoluto.

{% image "chaos.png" %}

Por eso se debe limitar la cantidad de tareas que puede realizar cada persona, y la cantidad de tareas que puede realizar el grupo en su conjunto.

### Gestionar el flujo de trabajo

Uno de los objetivos de visualizar las tareas en un tablero y limitar el trabajo en curso es poder gestionar el flujo de trabajo.

El problema es que a veces el poder gestionar el flujo de trabajo de forma transparente es motivo de discrepancia, porque en muchos casos se deja al descubierto muchas cosas que se están haciendo mal o que no se están haciendo.

Esto ocurre en la empresa y en el Instituto.

Por eso los estudiantes deben utilizar Kanban: 

**Serás más productivo, aprenderás cómo se trabaja en grupo, y los profesores podremos ayudarte porque en todo momento podemos saber que está haciendo.**

## Tablero

Cuando trabajamos en equipo, es necesario saber muchas cosas para colaborar de manera eficaz.

Cosas como quién trabaja en qué tarea, si estás centrando tus esfuerzos en la tarea de máxima prioridad, si hay tareas sin resolver desde hace mucho tiempo, y si hay tareas que llevan mucho tiempo haciéndose y no permiten realizar otras tareas.

{% image "board-dev.png" %}

El tablero tradicional es una pizarra o algún espacio vacío de la pared donde se colocan pegatinas o fichas para visualizar el flujo de trabajo del grupo y cómo avanza el trabajo. El tablero se actualiza con frecuencia para que todo el mundo pueda ver la información más actual y actuar en función de ella.

El uso de un tablero permite discutir lo que está pasando, crear nuevos elementos de trabajo, etc. 

## GitLab

Nosotros no utilizaremos el sistema de pizarra en clase, porque si cada grupo utiliza un pedazo de la pared podría ser caótico (recuerda que la pizarra es del profesor). 

Como somos informáticos, lo primero que debes hacer es ...

(PENDIENTE DE PASAR DE YOUTRACK A GITLAB)




## TODO



[Google Docs](https://docs.google.com/document/d/15SAlaLYwN9qgDXV62_RP4W3hZYZlLdBQtETgUUNNwfI/edit?usp=drive_link)
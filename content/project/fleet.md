---
title: Fleet
---

## Introducció


## Editor

1. Descarrega i instal.la [JetBrains Toolbox](https://www.jetbrains.com/toolbox-app/)

2. Executa "Jetbrains Toolbox", i fes clic a **Install** al costat de l'icona de **Fleet**

Un cop has instal.lat Fleet el pots arrencar des de la línia de comandes:

```pwsh
> fleet
```

A <https://www.jetbrains.com/help/fleet/getting-started.html> tens una explicació de com funciona.

## TODO

* <https://blog.jetbrains.com/fleet/2024/12/fleet-1-44-is-here-with-new-ui-zig-language-support-and-more-enhancements/>
---
title: Tabla
description: Una tabla es una manera ordenada de presentar unos datos que estan relacionados.
mermaid: true
---

## Introducción

## Entorno de trabajo

Instala [Sqlite](https://www.sqlite.org/) amb {% link "/tool/scoop" %}:

```pwsh
> scoop install sqlite
```

Abre una base de datos `hello.db`:

```pwsh
> sqlite3.exe hello.db
SQLite version 3.44.3 2024-03-24 21:15:01 (UTF-16 console I/O)
Enter ".help" for usage hints.
sqlite>
```

Con la orden `.help` puedes ver todas la ordenes que tienes disponibles **para administrar** la base de datos:

```sh
sqlite>  .help
.archive ...             Manage SQL archives
.auth ON|OFF             Show authorizer callbacks
.backup ?DB? FILE        Backup DB (default "main") to FILE
...
```

Con `.quit` puedes salir de SQLite

```pwsh
> .quit
```

Como no has creado ningua tabla no se ha creado el fichero `hello.db`.

### DB Browser

Aunque tengas que aprender a trabajar desde la linea de comandos, muchas veces es más fácil trabajar en un entorno gráfico.

Instala [DB Browser for SQLite ](https://sqlitebrowser.org/):

```pwsh
scoop bucket add extras
scoop install extras/sqlitebrowser
```

Arrenca la aplicación:

{% image "browser.png" %}

## Tabla

Crea una base de datps `pets.db`:

```pwsh
> sqlite3 pets.db
SQLite version 3.46.1 2024-08-13 09:16:08 (UTF-16 console I/O)
Enter ".help" for usage hints.
```

Una taula és una estructura de dades que organitza les dades en columnes (atributs) i files (registres).

A continuació tens una taula de gossos:

| Name | Age | Breed |
|-|-|-|
| Trufa | 12 | Rough Collie |
| Pujol | 6 | Bulldog |
| Ketzu | 2 | Shiba Inu |

## create table

El primer que has de fer és crear una taula `dogs`:

<pre class="mermaid">
classDiagram
class dogs {
    name text
    age integer
    breed string
}
</pre>

Per fer-ho has d'executar una sentència SQL:

```sql
>  create table dogs(name text, age integer, breed text);
```

Al crear la taula, a més de dir els atributs que la composen, has de dir el tipus de cada atribut.

Els tipus més habituals són:

| Storage class | |
|-|-|
| `null` | El valor no existeix o no es coneix. |
| `integer` | El valor és un número enter, i es guarda amb 0, 1, 2, 3, 4, 6, o 8 bytes en funció del tamany del número. |
| `real` | El valor és un número real, i es guarda com un "8-byte IEEE floating point number". |
| `text`| El valor és un string, i es guarda utilitzant la codificació de la base de dades (UTF-8, UTF-16BE o UTF-16LE). |
| `blob` | El valor és una seqüència de bytes, i es guarda tal qual. |

A continuació ja pots introduïr dades a la taula:

```sql
> insert into dogs values ('Trufa', 12, 'Rough Collie');
```

Acaba de introduïr les dades que falten:

{% sol %}
```sql
> insert into dogs values ('Pujol', 6, 'Bulldog'), ('Ketzu', 2, 'Shiba Inu');
```
{% endsol %}

### Activitat

## TODO

* <https://www.sqlitetutorial.net/>
* <https://www.tutorialesprogramacionya.com/sqliteya/>
* <https://dev.to/mliakos/quick-post-about-the-sqlite-upsert-and-the-new-returning-clause-5fhl>
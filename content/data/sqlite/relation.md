---
title: Relació
description: Una relació és una "connexió" entre dues taules que comparteixen atributs.
---

## Introducció

El model relacional es basa en taules que es relacionen unes amb les altres pels valors compartits d'una o més columnes

## Chinook Database

[Chinook](https://github.com/lerocha/chinook-database) is a sample database available for SQL Server, Oracle, MySQL, etc. It can be created by running a single SQL script. Chinook database is an alternative to the Northwind database, being ideal for demos and testing ORM tools targeting single and multiple database servers.

Baixa la base de dades:

<https://github.com/lerocha/chinook-database/releases/download/v1.4.5/Chinook_Sqlite.sqlite>


Obre la base de dades:

```pwsh
> sqlite chinook.db
```

Amb l'ordre `.databases` pots veure totes les base de dades que tens obertes:

```sh
sqlite> .databases
main: C:\Users\david\workspace\sqlite\chinook.db r/w
```

Amb l'ordre `.table` pots veure totes les taules de la base de dades `chinook`:

```sh
sqlite> .tables
.tables
Album          Employee       InvoiceLine    PlaylistTrack
Artist         Genre          MediaType      Track
Customer       Invoice        Playlist
```

Am l'ordre `.schema` pots veure l'estructura d'una taula:

```sh
sqlite> .schema Artist
CREATE TABLE [Artist]
(
    [ArtistId] INTEGER  NOT NULL,
    [Name] NVARCHAR(120),
    CONSTRAINT [PK_Artist] PRIMARY KEY  ([ArtistId])
);
```

Segueix: <https://www.sqlitetutorial.net/sqlite-commands/>


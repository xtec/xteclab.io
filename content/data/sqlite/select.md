---
title: Select
---

### select

Amb `select` pots seleccionar les dades d'una taula.

Si vols totes les dades:

```sql
> select * from dogs;
Trufa|12|Rough Collie
Pujol|6|Bulldog
Ketzu|2|Shiba Inu
```

Pots configurar SQLite per tal que et mostri un resultat més "llegible":

```sh
> .mode column
> .header on
```

Ara el resultat mostra el nom dels atributs (`.header on`), i ajusta l'amplada de les columnes (`.mode colum`):

```sql
sqlite> select * from dogs;

name   age  breed
-----  ---  ------------
Trufa  12   Rough Collie
Pujol  6    Bulldog
Ketzu  2    Shiba Inu
```

Si només vols els noms dels gossos, enlloc de seleccionar tots els atributs amb `*`, pots indicar quins atributs vols:

```sql
sqlite> select name from dogs;
name
-----
Trufa
Pujol
Ketzuu
```

Selecciona el nom i l'edat de la taula `dogs`:

{% sol %}
 select name, age from dogs;
name   age
-----  ---
Trufa  12
Pujol  6
Ketzu  2
{% endsol %}
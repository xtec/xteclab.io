---
title: Indexs
description: Els indexs ajuden al motor de la bases de dades a realitzar consultes de manera molt més eficient.
---

## Introducció

La quantitat de dades contingudes en una col·lecció afecta directament el rendiment de la cerca: com més gran sigui el conjunt de dades, més difícil serà per a MongoDB trobar els documents que coincideixen amb la consulta.

Els índexs són estructures de dades especials que emmagatzemen només un petit subconjunt de les dades contingudes en els documents d'una col·lecció per separat dels mateixos documents.

MongoDB crea un índex únic al camp `_id` durant la creació d'una col·lecció que impedeix als clients inserir dos documents amb el mateix valor per al camp `_id`. Aquest index no es pot eliminar.

**PENDENT**
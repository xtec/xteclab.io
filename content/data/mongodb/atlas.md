---
title: Atlas
description: MongoDB Atlas ofrece una forma sencilla de alojar y gestionar tus datos en la nube.
---

## Introducciónn


## Entorno de trabajo

Instal.la el Atlas CLI:

```pwsh
> scoop install main/mongodb-atlas-cli
```

Regístrate para obtener una cuenta de Atlas mediante tu cuenta de Google o una dirección de correo electrónico:

```pwsh
> atlas auth register
```

Al registrarte has iniciado sesión, pero en cualquier momento puedes volver a iniciar sesión con:

```pwsh
> atlas auth login
```

Atlas crea una organización por defecto:

```pwsh
> atlas organizations list
ID                         NAME
67877b1e25674900296a4ece   David's Org - 2025-01-15
```
Configura por defecto la organización que se ha creado:

```pwsh
> atlas config set org_id 67877b1e25674900296a4ece
Updated property 'org_id'
```

### Base de datos

A continuación crea el proyecto `acme`:

```pwsh
> atlas projects create acme
Project '67879c731bae49103f7c97c5' created.
```

Configura por defecto el nuevo proyecto:

```pwsh
> atlas config set project_id 67879c731bae49103f7c97c5
Updated property 'project_id'
```
A continuación despliega un cluster gratuito (sólo puedes tener uno por proyecto):

```pwsh
> atlas setup --clusterName acme --provider AWS --region EU_WEST_3 --username acme --password P@ssw0rd --skipSampleData --force
We are deploying orders...

Store your database authentication access details in a secure location:
Database User Username: shop
Database User Password: P@ssw0rd

Creating your cluster... [It's safe to 'Ctrl + C']
Cluster created.
Your connection string: mongodb+srv://acme.trjoh.mongodb.net
connection skipped
```

Ya puedes conectarte al cluster:

```pwsh
> mongosh "mongodb+srv://acme.trjoh.mongodb.net/shop" --apiVersion 1 --username acme
```

Puedes ver sus datos, importar documentos y ejecutar consultas.

## TODO

* <https://www.mongodb.com/docs/atlas/getting-started/>
* [Atlas](https://www.mongodb.com/products/platform/atlas-database)

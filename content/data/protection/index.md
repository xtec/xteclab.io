---
title: Protecció de dades
description: De la mateixa manera que hi ha un mercat únic de productes i mercaderies a la UE, el RGPD crea un marc jurídic comú per crear un mercat únic en l'ús de les dades.
---

## Introducció

[El Reglament General de Protecció de dades](https://eur-lex.europa.eu/legal-content/ES/TXT/HTML/?uri=CELEX:32016R0679&from=ES) té **com a objectiu crear un mercat únic de dades dins de la Unió Europea (UE)**.

De la mateixa manera que hi ha un mercat únic de productes i mercaderies que impedeix normes diferents a cada Estat Membre (EM), el RGPD impedeix normes diferents de protecció de dades a cada EM.”

“Les empreses, PIMES i autònoms poden tractar dades de caràcter personal de ciutadans de la UE a tota la UE amb la mateixa normativa, i sense que la lliure circulació d'aquestes dades dins de la UE pugui ser restringida ni prohibida per motius relacionats amb la protecció de les persones físiques pel que fa al tractament de dades personals.”

El RGPD es pot mirar desde dos perspectives diferents:

1.  El ciutadà considera el Reglament com un conjunt de drets que el protegeixen de les empreses respecte l'ús de les seves dades personals

3. L'empresa pot tractar determinades dades personals sense el consentiment del ciutadà.


### Àmbit d'aplicació material

El RGPD s'aplica al:

* Tractament totalment o parcialment **automatitzat** de dades personals.
* Tractament **no automatitzat** de dades personals contingudes o destinades a ser incloses en un fitxer.

El RGPD no s'aplica a (excepcions):

* Tractament de dades personals efectuat per una persona física en l'exercici d'activitats exclusivament personals o domèstiques.

* Tractament de dades personals per part de les autoritats competents amb fins de prevenció, investigació, detecció o enjudiciament d'infraccions penals, o d'execució de sancions penals, inclosa la de protecció davant d'amenaces a la seguretat pública i la seva prevenció.

A continuació tens alguns exemples:

* En el teu telèfon pots tenir dades personals dels teus amics i familiars sense el seu consentiment, com pot ser el seu telèfon, el seu correu electrònic, o la seva data de naixement.

* Si treballes com autònom, en el teu telèfon  només pot tenir les dades personals dels teus clients que el RGPD et permeti tindre.

* Hisenda controla certs moviments del teu compte bancari encara que no li donis permís, però no pot accedir a les teves dades de salut.

* El teu metge de capaçalera pot accedir a totes les teves dades de salut, però no té accés als teus moviments bancaris per controlar que no fas cap delicte financer.

* La policia pot vigilar els teus moviments i escoltar les teves converses encara que tu no vulguis si té una ordre judicial que li permet fer-ho.

## Àmbit territorial

El RGPD s'aplica al tractament de dades personals en el context de les activitats d'un establiment del responsable o de l'encarregat a la Unió, independentment que el tractament tingui lloc a la Unió o no.

Per tant, si ets una empresa de la Unió Europea, no pots escapolir-te del reglament hostatjant les bases de dades en servidors fora de la UE, o subcontractant serveis de tractament de les dades que tens a empreses de fora de la UE.

El RGPD s'aplica al tractament de dades personals d'interessats que resideixin a la Unió per part d'un responsable o encarregat no establert a la Unió, quan les activitats de tractament estiguin 
relacionades amb:

1. L'oferta de béns o serveis a aquests interessats a la Unió, independentment de si a aquests se'ls requereix el pagament, o

2. el control del seu comportament, en la mesura que aquest tingui lloc a la Unió.

Per exemple, Google i Facebook són empreses de fora de la Unió Europea, però quan tracten dades de ciutadans de la UE dins la UE, han de respectar el RGPD. Per això, i per motius fiscals, Google té una societat a Irlanda que és la responsable del tractament dels usuaris europeus a Europa.

Però si Google i Facebook respecten el RGPD és perquè tenen negoci a la UE i poden ser sancionats de manera efectiva. Una empresa de China o de Vietnam pot fer el que vulgui.

El RGPD s'aplica al tractament de dades personals per part d'un responsable que no estigui establert a la Unió sinó en un lloc on el dret dels estats membres sigui aplicable en virtut del dret internacional públic.

El RGPD no només s'aplica dins la UE. Això vol dir que puc tenir servidors en aquests estats on també s'aplica i complir amb el RGPD, com per exemple Suïssa per motius de confidencialitat o Islàndia per cost econòmic.

### Definicions

* **Dades personals**. Tota informació sobre una persona física identificada o identificable («l'interessat»); es considera persona física identificable tota persona la identitat de la qual es pugui determinar, directament o indirectament, en particular mitjançant un identificador, com ara un nom, un número d'identificació, dades de localització, un identificador en línea o un o diversos elements propis de la identitat física, fisiològica, genètica, psíquica, econòmica, cultural o social de la persona esmentada.

* **Tractament**. Qualsevol operació o conjunt d'operacions realitzades sobre dades personals o conjunts de dades personals, ja sigui per procediments automatitzats o no, com ara la recollida, registre, organització, estructuració, conservació, adaptació o modificació, extracció, consulta, utilització, comunicació per transmissió, difusió o qualsevol altra forma d'habilitació d'accés, confrontació o interconnexió, limitació, supressió o destrucció.

* **Limitació del tractament**. El marcatge de les dades de caràcter personal conservades per tal de limitar-ne el tractament en el futur

* **Elaboració de perfils**. Tota forma de tractament automatitzat de dades personals consistent a utilitzar dades personals per avaluar determinats aspectes personals d'una persona física, en particular per analitzar o predir aspectes relatius al rendiment professional, situació econòmica, salut, preferències personals, interessos, fiabilitat, comportament, ubicació o moviments de la persona física esmentada

* **Anonimització**. El tractament de dades personals de tal manera que ja no es puguin atribuir a un interessat sense utilitzar informació addicional, sempre que aquesta informació addicional figuri per separat i estigui subjecte a mesures tècniques i organitzatives destinades a garantir que les dades personals no s'atribueixin a una persona física identificada o identificable

* **Fitxer**. Tot conjunt estructurat de dades personals, accessibles d'acord amb criteris determinats, ja sigui centralitzat, descentralitzat o repartit de manera funcional o geogràfica

* **Responsable del tractament**. La persona física o jurídica, autoritat pública, servei o un altre organisme que, sol o juntament amb altres, determini els fins i els mitjans del tractament; si el dret de la Unió o dels estats membres determina els fins i mitjans del tractament, el responsable del tractament o els criteris específics per al seu nomenament els pot establir el dret de la Unió o dels estats membres.

* **Encarregat del tractament**. La persona física o jurídica, autoritat pública, servei o altre organisme que tracti dades personals per compte del responsable del tractament.

## Principis

### Principis relatius al tractament

Tant les empreses com els autònoms han de respectar aquests principis quan tractin dades de caràcter personal.

#### Licitud, lleialtat i transparència

Les dades personal seran tractades de manera lícita, lleial i transparent en relació amb l'interessat.
   
No pots utilitzar les dades personals per fer una contractació fraudulenta, donant d'alta en un servei de gas o telecomunicacions a un client que no ho ha sol·licitat.

#### Limitació de la finalitat

Les dades personal hna de ser recollides amb fins determinats, explícits i legítims, i no seran tractats ulteriorment de manera incompatible amb aquests fins; d'acord amb l'article 89, apartat 1, el tractament ulterior de les dades personals amb fins d'arxiu en interès públic, fins de recerca científica i històrica o fins estadístiques no es considera incompatible amb les finalitats inicials

No pots demanar dades personals i informar al client que “seran tractats per millorar la seva experiència com a usuari”.

#### Minimització de dades

El tractament de dades personal ha de ser adequat, pertinent i limitat a allò necessari en relació amb els fins per als quals són tractats.

Exemples:

* No pots demanar i tractar dades simplement per si poguessin resultar útils o “per tenir-ne”.

* Si us subscriviu a un servei d'alertes a través de l'enviament de SMS, n'hi hauria prou amb facilitar, a més del teu nom i cognoms, el número de telèfon mòbil, i no cal afegir el número de telèfon fix.

* Si per a una finalitat determinada no cal que el responsable conegui les pautes de navegació de l'usuari, no podrà fer aquest seguiment.

Un resum pràctic del principi de minimització seria el següent:

Recull:

1. Només les dades personals que hagis de tractar,
2. Només quan els vagis a tractar, i
3. Tracta'ls només per a la finalitat declarada.

Ara exprimeix-los al màxim dins d'aquests límits.

#### Exactitud

Exactes i, si cal, actualitzats; s'adoptaran totes les mesures raonables perquè se suprimeixin o rectifiquin sense dilació les dades personals que siguin inexactes respecte a les finalitats per a les quals es tracten.

Exemples:

* Si una companyia subministradora d'electricitat o gas manté dades errònies sobre els seus clients, les conseqüències poden anar des de no proporcionar el servei contractat fins a emetre factures a clients equivocats.

#### Limitació del termini de conservació

Mantinguts de manera que es permet la identificació dels interessats durant no més temps del necessari per als fins del tractament de les dades personals; les dades personals es poden conservar durant períodes més llargs sempre que es tractin exclusivament amb fins d'arxiu en interès públic, fins d'investigació científica o històrica o fins estadístiques, de conformitat amb l'article 89, apartat 1, sense perjudici de l'aplicació de les mesures tècniques i organitzatives apropiades que imposa aquest Reglament a fi de protegir els drets i llibertats de l'interessat ();

Exemples:

* Quan recaptin les teves dades de caràcter personal el responsable t'ha d'informar sobre el període o els criteris de conservació de les teves dades personals.

#### Integritat i confidencialitat

Tractats de manera que es garanteixi una seguretat adequada de les dades personals, inclosa la protecció contra el tractament no autoritzat o il·lícit i contra la seva pèrdua, destrucció o dany accidental, mitjançant l'aplicació de mesures tècniques o organitzatives apropiades.

Exemples:

* Per garantir la seguretat de les dades personals per internet, és recomanable que l'enviament es faci de manera xifrada.

###  Responsabilitat proactiva

El responsable del tractament serà responsable del compliment dels principis anteriorment exposats i capaç de demostrar-ho

Exemples:

1. Entre aquestes mesures hi ha l'anàlisi de risc amb la finalitat d'adoptar les mesures de seguretat corresponents, el registre d'activitats de tractament, la notificació de bretxes de seguretat o la realització d'avaluacions d'impacte de protecció de dades.

### Licitud del tractament

Com s'ha explicat abans, el tractament de dades ha de complir el principi de **Licitud, lleialtat i transparència**.

El tractament només serà lícit si es compleix almenys una de les condicions següents:

1. L'interessat va donar el seu consentiment per al tractament de les seves dades personals per a un o diversos fins específics

   Exemple: Quan et subscrius a un determinat servei per rebre una newsletter

2. El tractament és necessari per a l'execució d'un contracte on l'interessat és part o per a l'aplicació a petició d'aquest de mesures precontractuals;

   Exemple: El tractament de dades de nom, cognoms i fotografia d'un treballador per a la targeta identificativa de l'empresa.

3. El tractament és necessari per al compliment d'una obligació legal aplicable al responsable del tractament

   Exemple: L'obligació de facilitar informació de caràcter tributària a l'AEAT

4. El tractament és necessari per protegir interessos vitals de l'interessat o d'una altra persona física.

   Exemple: Si tens un accident i estas inconscient els metges d'urgència poden accedir al teu historial mèdic per salvar-te la vida encara que no siguin el teu metge de capçalera.

5. El tractament és necessari per al compliment d'una missió realitzada a interès públic o en l'exercici de poders públics conferits al responsable del tractament.

   Exemple: Quan hisenda et realitza una inspecció tributària pot accedir a tots els teus moviments bancaris sense el teu consentiment encara que aquests moviments revelin detalls de la teva vida personal que no tenen res a veure amb el frau fiscal.

6. El tractament és necessari per a la satisfacció d'interessos legítims perseguits pel responsable del tractament o per un tercer, sempre que sobre aquests interessos no prevalguin els interessos o els drets i les llibertats fonamentals de l'interessat que requereixin la protecció de dades personals, en particular quan l'interessat sigui un nen.

   Excepció. Les autoritats públiques sempre poden a l'exercici de les seves funcions.

   Exemple: Els bancs tenen un registre de morositat necessari per a la seva activitat creditícia
   
   Exemple: L'accés a les imatges de videovigilància d'un aparcament per identificar qui ha fet malbé o robat un cotxe.

### Condicions per al consentiment

Com s'ha explicat abans, una de les possibles condicions perquè el  tractament sigui lícit és si el  "interessat va donar el seu consentiment per al tractament de les seves dades personals per a un o diversos fins específics".

1. Quan el tractament es base en el consentiment de l'interessat, el responsable haurà de ser capaç de demostrar que aquell va consentir el tractament de les dades personals.

2. Si el consentiment de l'interessat es dóna en el context d'una declaració escrita que també es refereixi a altres assumptes, la sol·licitud de consentiment es presentarà de manera que es distingeixi clarament dels altres assumptes, de forma intel·ligible i de fàcil accés i utilitzant un llenguatge clar i senzill. No és vinculant cap part de la declaració que constitueixi infracció del present Reglament.

3. L'interessat tindrà dret a retirar-ne el consentiment en qualsevol moment. La retirada del consentiment no afecta la licitud del tractament basada en el consentiment previ a la seva retirada. Abans de donar-ne el consentiment, l'interessat n'informarà. Serà tan fàcil retirar el consentiment com donar-ho.

4. En avaluar si el consentiment s'ha donat lliurement, es tindrà en compte en la major mesura possible el fet de si, entre altres coses, l'execució d'un contracte, inclosa la prestació d'un servei, se supedita al consentiment al tractament de dades personals que no són necessaris per a l'execució del contracte esmentat.

### Consentiment del nen

#### Edat mínima

En el cas dels nens i els serveis de la societat de la informació (Internet), si tractes dades personals perquè han consentit, tens un requisit d'edat mínima.

Han de tenir mínim 16 anys, encar que cada estat de la Unió Europea pot rebaixar aquesta edat fins a un mínim de 13 anys, i en el cas d'Espanya s'ha rebaixat a 14 anys.

Si tens una pàgina web i demanes consentiment tens que tenir en compte l'edat i de quin pais és!

#### Pàtria potestat

I si és menor de l'edat legal de consentiment, el consentiment l'ha de donar o autoritzar el titular de la pàtria potestat o tutela sobre el nen, i només en la mesura que es va donar o autoritzar.

El responsable del tractament farà esforços "raonables" per verificar en aquests casos que el consentiment va ser donat o autoritzat pel titular de la pàtria potestat o tutela sobre el nen, tenint en compte la tecnologia disponible.

Qué vol dir "raonables"?

3. L'apartat 1 no afecta les disposicions generals del dret contractual dels estats membres, com les normes relatives a la validesa, la formació o els efectes dels contractes en relació amb un nen.


### Tractament de categories especials de dades personals

Queden prohibits el tractament de dades personals que:

1. Revelin l’origen ètnics o racials
2. Les opinions polítiques, les conviccions religioses o filosòfiques o l'afiliació sindical.
3. El tractament de dades genètiques, dades biomètriques adreçades a identificar de manera unívoca una persona física.
4. Dades relatives a la salut.
5. Dades relatives a la vida sexual o orientació sexuals d'una persona física.
6. Els Estats membres poden mantenir o introduir condicions addicionals, fins i tot limitacions, pel que fa al tractament de dades genètiques, dades biomètriques o dades relatives a la salut.

Excepcions:

1. L'interessat va donar el seu **consentiment explícit** per al tractament de les dades personals esmentades amb una o més de les finalitats especificades, excepte quan el Dret de la Unió o dels Estats membres estableixi que la prohibició esmentada abans no pot ser aixecada per l'interessat.

2. El tractament és necessari per al **compliment d'obligacions** i l'**exercici de drets específics** del responsable del tractament o de l'interessat en l'àmbit del dret laboral i de la seguretat i protecció social, en la mesura que així ho autoritzi el dret de la Unió dels Estats membres o un conveni col·lectiu d'acord amb el dret dels estats membres que estableixi garanties adequades del respecte dels drets fonamentals i dels interessos de linteressat.

3. El tractament **és necessari per protegir interessos vitals de l'interessat** o d'una altra persona física, en cas que l'interessat no estigui capacitat, físicament o jurídicament, per donar-ne el consentiment.

4. El tractament és efectuat, en l'àmbit de les seves activitats legítimes i amb les garanties degudes, per una fundació, una associació o qualsevol altre organisme sense ànim de lucre, la finalitat del qual sigui política, filosòfica, religiosa o sindical, sempre que el tractament es refereixi exclusivament als membres actuals o antics d'aquests organismes oa persones que hi mantinguin contactes regulars amb relació als seus fins i sempre que les dades personals no es comuniquin fora d'ells sense el consentiment dels interessats.

5. El tractament fa referència a dades personals que l'interessat ha fet manifestament públiques.

6. El tractament és necessari per a la formulació, exercici o defensa de reclamacions o quan els tribunals actuïn en exercici de la seva funció judicial.

7. El tractament és necessari per raons d'un interès públic essencial, sobre la base del Dret de la Unió o dels Estats membres, que ha de ser proporcional a l'objectiu perseguit, respectar essencialment el dret a la protecció de dades i establir mesures adequades i específiques per protegir els interessos i els drets fonamentals de l'interessat.

8. El tractament és necessari per a fins de medicina preventiva o laboral, avaluació de la capacitat laboral del treballador, diagnòstic mèdic, prestació d'assistència o tractament de tipus sanitari o social, o gestió dels sistemes i serveis d'assistència sanitària i social, sobre la base del dret de la Unió o dels estats membres o en virtut d'un contracte amb un professional sanitari.
Condicions i garanties. El tractament l'ha de fer un professional subjecte a l'obligació de secret professional, o sota la seva responsabilitat, d'acord amb el dret de la Unió o dels estats membres o amb les normes establertes pels organismes nacionals competents, o per qualsevol altra persona subjecta també a l'obligació de secret d'acord amb el dret de la Unió o dels estats membres o de les normes establertes pels organismes nacionals competents.

9. El tractament és necessari per raons d'interès públic en l'àmbit de la salut pública, com ara la protecció davant d'amenaces transfrontereres greus per a la salut, o per garantir nivells elevats de qualitat i de seguretat de l'assistència sanitària i dels medicaments o productes sanitaris , sobre la base del dret de la Unió o dels estats membres que estableixi mesures adequades i específiques per protegir els drets i les llibertats de l'interessat, en particular el secret professional.

10. El tractament és necessari amb fins d'arxiu en interès públic, fins de recerca científica o històrica o fins estadístiques, de conformitat amb l'article 89, apartat 1, sobre la base del dret de la Unió o dels estats membres, que ha de ser proporcional a l'objectiu perseguit, respectar essencialment el dret a la protecció de dades i establir mesures adequades i específiques per protegir els interessos i drets fonamentals de l'interessat

### Activitats

**1.-** En el cas d’una persona física, posa un exemple de quan s’aplica el RGPD i quan no.

**2.-** En el cas  d’una autoritat pública, posa un exemple de quan s’aplica el RGPD i quan no.

**3.-** Investiga sobre l’acord Safe Harbour entre la UE i els EEUU respecte la protecció de dades.

**4.-** Investiga en quins països fora de la UE pots tenir servidors que tractin dades de caràcter personal de 
ciutadans de la UE.

**5.-** Posa un exemple de compliment ,i un altre on no, per cada principi de tractament.

**6.-** Una de les principals fonts de dades és el rastre de la nostra interacció a pàgines web mitjançant l’ús de cookies.

Llegeix aquest artícle que explica el [consentimient de cookies segons el RGPD](https://www.iubenda.com/es/help/43944-ejemplos-de-consentimiento-de-cookies-del-rgpd).

Creus que els exemples estan bé?

**7.-** Si la meva aplicació web és només per a adults, com puc assegurar-me que el consentiment no l'estigui donant un nen?

**8.-** Una web de contactes té un interès legítim per a tractar dades d'orientació sexual, ja que sense aquesta dada el servei no funciona.

¿Pot tractar aquesta dada sense el consentiment de l’usuari? 


## Drets de l'interessat

### Informació que cal facilitar quan les dades personals s'obtinguin de l'interessat

Quan s'obtinguin d'un interessat dades personals relatives a ell, el responsable del tractament, en el moment en què s'obtinguin, us facilitarà tota la informació indicada a continuació:

1. La identitat i les dades de contacte del responsable i, si escau, del seu representant
2. Les dades de contacte del delegat de protecció de dades, si escau
3. Les finalitats del tractament a què es destinen les dades personals i la base jurídica del tractament.

4. Quan el tractament es base a l'article 6, apartat 1, lletra f), els interessos legítims del responsable o d'un tercer;

   Els destinataris o les categories de destinataris de les dades personals, si escau;

   Si escau, la intenció del responsable de transferir dades personals a un tercer país o organització internacional i l'existència o absència d'una decisió d'adequació de la Comissió, o, en el cas de les transferències indicades als articles 46 o 47 o el article 49, apartat 1, paràgraf segon, referència a les garanties adequades o apropiades i als mitjans per obtenir-ne una còpia o al fet que s'hagin prestat.

A més de la informació esmentada abans, el responsable del tractament facilitarà a l'interessat, en el moment que s'obtinguin les dades personals, la següent informació necessària per garantir un tractament de dades lleial i transparent:

1. El termini durant el qual es conservaran les dades personals o, quan no sigui possible, els criteris utilitzats per determinar aquest termini;

2. L'existència del dret a sol·licitar al responsable del tractament l'accés a les dades personals relatives a l'interessat, i la rectificació o supressió, o la limitació del tractament, oa oposar-se al tractament, així com el dret a la portabilitat de les dades

3. Quan el tractament estigui basat en l'article 6, apartat 1, lletra a), o l'article 9, apartat 2, lletra a), l'existència del dret a retirar el consentiment en qualsevol moment, sense que això afecti la licitud del tractament basat en el consentiment previ a la seva retirada

4. El dret a presentar una reclamació davant d'una autoritat de control.

5. Si la comunicació de dades personals és un requisit legal o contractual, o un requisit necessari per subscriure un contracte, i si l'interessat està obligat a facilitar les dades personals i està informat de les possibles conseqüències que no facilitar aquestes dades

6. L'existència de decisions automatitzades, inclosa l'elaboració de perfils, a què fa referència l'article 22, apartats 1 i 4, i, almenys en aquests casos, informació significativa sobre la lògica aplicada, així com la importància i les conseqüències previstes del dit tractament per a l'interessat.

Quan el responsable del tractament projecti el tractament ulterior de dades personals per a una finalitat que no sigui aquella per a la qual es van recollir, proporcionarà a l'interessat, amb anterioritat a aquest tractament ulterior, informació sobre aquest altre fi i qualsevol informació addicional pertinent.

### Informació que cal facilitar quan les dades personals no s'hagin obtingut de l'interessat

Quan les dades personals no s'hagin obtingut de l'interessat, el responsable del tractament us facilitarà la informació següent:

1. La identitat i les dades de contacte del responsable i, si escau, del representant.
2. Les dades de contacte del delegat de protecció de dades, si escau.
3. Els fins del tractament a què es destinen les dades personals, així com la base jurídica del tractament
4. Les categories de dades personals de què es tracti.
5. Els destinataris o les categories de destinataris de les dades personals, si escau.
6. Si escau, la intenció del responsable de transferir dades personals a un destinatari en un tercer país o organització internacional i l'existència o absència d'una decisió d'adequació de la Comissió, o, en el cas de les transferències indicades als articles 46 o 47 o l'article 49, apartat 1, paràgraf segon, referència a les garanties adequades o apropiades i als mitjans per obtenir-ne una còpia o al fet que s'hagin prestat.

A més de la informació esmentada a l'apartat 1, el responsable del tractament facilitarà a l'interessat la següent informació necessària per garantir un tractament de dades lleial i transparent respecte de l'interessat:

1. El termini durant el qual es conservaran les dades personals o, quan això no sigui possible, els criteris utilitzats per determinar aquest termini.

2. Quan el tractament es base a l'article 6, apartat 1, lletra f), els interessos legítims del responsable del tractament o d'un tercer.

3. L'existència del dret a sol·licitar al responsable del tractament l'accés a les dades personals relatives a l'interessat, i la rectificació o supressió, o la limitació del tractament, ia oposar-se al tractament, així com el dret a la portabilitat de les dades,

4. Quan el tractament estigui basat en l'article 6, apartat 1, lletra a), o l'article 9, apartat 2, lletra a), l'existència del dret a retirar el consentiment en qualsevol moment, sense que això afecti la licitud del tractament basada en el consentiment abans de la retirada.

5. El dret a presentar una reclamació davant d'una autoritat de control.

6. La font de què procedeixen les dades personals i, si escau, si procedeixen de fonts d'accés públic

## Responsable del tractament i encarregat del tractament

### Responsabilitat del responsable del tractament

Tenint en compte la naturalesa, l'àmbit, el context i les finalitats del tractament així com els riscos de diversa probabilitat i gravetat per als drets i llibertats de les persones físiques, el responsable del tractament aplicarà mesures tècniques i organitzatives apropiades per tal de garantir i poder demostrar que el tractament és conforme amb aquest Reglament. Aquestes mesures es revisaran i actualitzaran quan sigui necessari.

Quan siguin proporcionades en relació amb les activitats de tractament, entre les mesures esmentades anteriorment s'inclourà l'aplicació, per part del responsable del tractament, de les polítiques de protecció de dades oportunes.

L'adhesió a codis de conducta aprovats d'acord amb l'article 40 o a un mecanisme de certificació aprovat d'acord amb l'article 42 poden ser utilitzats com a elements per demostrar el compliment de les obligacions per part del responsable del tractament.

### Protecció de dades des del disseny i per defecte

1. Tenint en compte l'estat de la tècnica, el cost de l'aplicació i la naturalesa, àmbit, context i fins del tractament, així com els riscos de diversa probabilitat i gravetat que comporta el tractament per als drets i llibertats de les persones físiques, el responsable del tractament aplicarà, tant en el moment de determinar els mitjans de tractament com en el moment del propi tractament, mesures tècniques i organitzatives apropiades, com la pseudonimització, concebudes per aplicar de manera efectiva els principis de protecció de dades, com la minimització de dades, i integrar les garanties necessàries en el tractament, per tal de complir els requisits del present Reglament i protegir els drets dels interessats.

2. El responsable del tractament aplicarà les mesures tècniques i organitzatives apropiades amb vista a garantir que, per defecte, només siguin objecte de tractament les dades personals que siguin necessàries per a cadascun dels fins específics del tractament. Aquesta obligació s'aplicarà a la quantitat de dades personals recollides, a l'extensió del tractament, al termini de conservació ia l'accessibilitat. Aquestes mesures han de garantir en particular que, per defecte, les dades personals no siguin accessibles, sense la intervenció de la persona, a un nombre indeterminat de persones físiques.

3. Podrà utilitzar-se un mecanisme de certificació aprovat d'acord amb l'article 42 com a element que acrediti el compliment de les obligacions establertes als apartats 1 i 2 del present article.

### Encarregat del tractament

1. Quan es faci un tractament per compte d'un responsable del tractament, aquest triarà únicament un encarregat que ofereixi garanties suficients per aplicar mesures tècniques i organitzatives apropiades, de manera que el tractament sigui conforme amb els requisits del present Reglament i garanteixi la protecció dels drets de l'interessat.

2. L'encarregat del tractament no recorrerà a un altre encarregat sense l'autorització prèvia per escrit, específica o general, del responsable. En aquest darrer cas, l'encarregat informarà el responsable de qualsevol canvi previst en la incorporació o substitució d'altres encarregats, donant així al responsable l'oportunitat d'oposar-s'hi.

### Registre de les activitats de tractament
 
Cada responsable i, si escau, el seu representant han de portar un registre de les activitats de tractament efectuades sota la seva responsabilitat. 

Aquest registre haurà de contenir tota la informació indicada a continuació:

1. El nom i les dades de contacte del responsable i, si escau, del coresponsable, del representant del responsable, i del delegat de protecció de dades

2. Els fins del tractament;

3. Una descripció de les categories d'interessats i de les categories de dades personals
les categories de destinataris a qui es van comunicar o comunicaran les dades personals, inclosos els destinataris a tercers països o organitzacions internacionals

4. Si és el cas, les transferències de dades personals a un tercer país o una organització internacional, inclosa la identificació del tercer país o organització internacional i, en el cas de les transferències indicades a l'article 49, apartat 1, paràgraf segon, la documentació de garanties adequades

5. Quan sigui possible, els terminis previstos per a la supressió de les diferents categories de dades

7. Quan sigui possible, una descripció general de les mesures tècniques i organitzatives de seguretat a què fa referència l'article 32, apartat 1

### Seguretat del tractament

Tenint en compte l'estat de la tècnica, els costos d'aplicació, i la naturalesa, l'abast, el context i els fins del tractament, així com riscos de probabilitat i gravetat variables per als drets i les llibertats de les persones físiques, el responsable i l'encarregat del tractament aplicaran mesures tècniques i organitzatives apropiades per **garantir un nivell de seguretat adequat al risc**, que si escau inclogui, entre d'altres:

1. La pseudonimització i el xifratge de dades personals.
2. La capacitat de garantir la confidencialitat, integritat, disponibilitat i resiliència permanents dels sistemes i serveis de tractament.
3. La capacitat de restaurar la disponibilitat i accés a les dades personals de forma ràpida en cas d'incident físic o tècnic.
4. Un procés de verificació, avaluació i valoració regulars de l'eficàcia de les mesures tècniques i organitzatives per garantir la seguretat del tractament.

### Activitat

**1.-** Creus necessari que tant el responsable com l'encarregat tinguin obligacions derivades del RGPD? Explica la teva resposta.

**2.-** Per què és necessari impedir que el responsable del tractament encarregui un tractament de dades a aquell que ell consideri millor per als seus interessos?


## AEPD

L'Agència Espanyola de Protecció de Dades (AEPD) és una autoritat administrativa independent responsable de supervisar l'aplicació del RGPD

### Introducció

El que importa d'una llei o d'un reglament **sancionador**, com és el RGPD, no és el que diu que es pot fer o no es pot fer, és el que passa si fas el que no es pot fer.

Per aquest motiu, el primer que has de llegir un cop has aprés els conceptes es l'esquema sancionador, per conèixer les diferents sancions econòmiques que et poden imposar si no cumpleixes el reglament.

L'altre factor important és quina capacitat té l'autoritat sancionadora per vigilar el cumpliment de la llei.

Per exemple, l'agència tributària (AEAT) té els millors sistemes informàtics de l'estat i es qui paga millor als funcionaris perquè és un organisme que fa guanya diners a l'estat.

L'Agència Espanyola de Protecció de Dades és un altre organisme que disposa de bastants recursos, perquè sancionar a grans empreses per incumpliment del RGPD està molt ben vist pel ciutadà i genera ingressos per l'estat.

**Durant l'any 2023 va recaudar 33 milions d'euros.** 

Encara que l'AEAT, per posar un exemple, no pot competir amb un ajuntament com Madrid amb les multes als ciutadans: 211 millons d'euros.

Però a diferència de Madrid amb les multes, la seva activitat està molt ben vista pels ciutadans. 

De la mateixa manera que Hisenda fa por, l'AEPD també fa por a les empreses.

### Sancions

La quantia de la sanció depen de la gravetat de la infracció comesa:

* Infraccions lleus: multes de fins a 40.000 euros.
* Infraccions greus: multes de 40.001 euros a 300.000 euros
* Infraccions molt greus: multes de 300.001 euros a 20 milions d'euros o el 4% de la facturació anual.

A continuació tens alguns exemples de **sancions de les grosses**:

* [Multa récord de 1.200 millones de euros a Meta Platforms Ireland Limited por la transferencia de datos personales de los usuarios de Facebook de la Unión Europea a EEUU sin garantías adecuadas](https://rgpdblog.com/multa-record-de-1-200-millones-de-euros-a-meta-platforms-ireland-limited-por-la-transferencia-de-datos-personales-de-los-usuarios-de-facebook-de-la-union-europea-a-eeuu-sin-garantias-adecuadas/)

I aquí tens **de les petites**, que per una empresa petita no són tant petites:

* Sanció a un metge de Gijón: Va ser a causa de [llançar a la via pública envasos de biòpsies amb dades personals](https://www.europapress.es/asturias/noticia-agencia-proteccion-datos-sanciona-medico-gijon-tirar-envases-biopsias-datos-personales-20091228134816.html), la multa que l'AEPD li va imposar va ser de 60.101 euros, ja que va cometre una infracció tipificada com a molt greu.

* L'AEPD va [obrir un expedient a un Hospital d'Inca](https://www.ultimahora.es/noticias/local/2011/05/13/40271/la-agencia-de-proteccion-de-datos-abre-un-expediente-al-hospital-de-inca.html) per la filtració de dades personals de pacients.

* [Un Centre Mèdic de Cartagena va ser sancionat](https://www.adelopd.com/la-aepd-sanciona-con-6000e-a-un-centro-medico-de-cartagena/) per l'AEPD amb la xifra de 6.000 euros per fer ús de les dades personals d'un client de l'empresa amb què s'havia fusionat.

* [Un Hospital de Conca va ser advertit](https://www.eldiario.es/castilla-la-mancha/proteccion-datos-consentimiento-pacientes-cuenca_1_4199618.html) per cedir dades personals i historials mèdics dels pacients a una clínica privada sense xifrar la informació, malgrat tractar-se de dades especialment protegides. La multa va pujar a un total de 40.001 euros.

#### Activitat

**1.-** Busca a Internet la sanció més alta a Europa en el 2023 per incumpliment de RGPD i resum el motiu de la sanció.

**2.-** Que et sembla aquesta informació oficial de la AEPD: [La AEPD recibe por tercer año consecutivo el mayor número de reclamaciones de su historia](https://www.aepd.es/prensa-y-comunicacion/notas-de-prensa/aepd-recibe-por-tercer-anno-consecutivo-mayor-numero-reclamaciones-historia)

**3.-** Creus que les administracions públiques es prenen seriosament el RGPD mirant aquest document oficial de l'AEPD [Administraciones Públicas sancionadas por no responder a requerimientos y por incumplimiento de medidas](https://www.aepd.es/areas-de-actuacion/administraciones-publicas/administraciones-publicas-sancionadas)

{% sol %}
Si, són molt poques.
{% endsol %}

### Drets de l'interessat

Les grans empreses estan sotmeses a una vigilància estricta del compliment del RGPD, sobretot per les quantioses sancions econòmiques previstes en el RGPD que imposen les autoritats de control.

Per exemple Google té una pàgina de privacitat exemplar perquè no la puguin sancionar, ja que és un objectiu recaptatori molt apetible:  [Política de Privacitat de Google](https://policies.google.com/privacy?hl=es).

En canvi les petities empreses no es preocupen massa, i ja els hi va bé amb la pàgina "Política de Privacitat" que es pot generar de manera automàtica amb el Worpress, encara que no és correcte.

El Wordpress és l'aplicació web més utilitzada per a crear llocs web comercials, blogs, etc..

#### Activitats

**1.-** Què creus que els motiva més a les autoritats de control, el compliment del RGPD o la possible quantia de la sanció.

En base a la teva resposta, un negoci petit o mitjà té suficient amb la pàgina per defecte del Wordpress?

{% sol %}

Depen ... l'AEAT mai anirà d'ofici a indagar, té peixos més grans en el mar per pescar, però com veurem més endavant, qualsevol citudatà pot presentar ua denuncia i llavors l'AEAT ha d'actuar si o si.

{% endsol %}

**2**.- Ves a ChatGPT i que et generi una política de privacitat per una empresa ficticia, a veure que passa.

Segur que és correcta?

{% sol %}
Segurament no, tal com s'explica a aquest article: [AI Generated Privacy Policy Examined: Should You Use ChatGPT?](https://termly.io/resources/articles/ai-privacy-policy-examined/)

Però si soc una petita empresa ja està bé, o potser no ...

{% endsol %}

**3.-** Busca un o més plugins pel {% link "/web/app/wordpress/" %} per tal de cumplir amb el GPDR.

**4.-** Si tens temps, crea un {% link "/web/app/wordpress/" %} que cumpleixi el GPDR.

### Denúncia

Les denúncies per protecció de dades augmenten cada any, ja que les persones som cada cop més conscients dels nostres drets i el valor de les nostres dades i privadesa. 

Tot i això, no tothom sap com posar **una denúncia per protecció de dades** o si és necessari algun pas previ abans de denunciar davant l'AEPD.

{% panel "Roma traditoribus non praemiat" %}
Recorda que a Europa, a diferència d'alguns paisos, qui es queda totes les sancions és l'Estat, tu no tens cap comisió.

Som un pais de tradició llatina: "Roma no paga traïdors"

<p class="text-center fs-1">😒</p>

Però els diners que recauda l'estat serveixen per donar serveis públics gratuïts, com són l'educació i la sanitat:

<p class="text-center fs-1">🧑‍🎓 👩‍⚕️</p>

{% endpanel %}


#### Pas previ

Primer has d'exercir els teus drets ARSULIPO, és a dir, enviar una sol·licitud al responsable del tractament per exercir un d'aquests drets, que normalment seran el d'accés (per saber quines dades teves té l'empresa), el de supressió (perquè eliminin les teves dades), rectificació (perquè es corregeixin errors en les teves dades) o el de limitació del tractament (perquè només siguin usats amb la finalitat que vas consentir).

Per exemple, si el col·legi ha publica't en xarxes socials una foto en què pots ser reconegut i no compte amb el teu consentiment per fer-ho, has d'enviar una sol·licitud de supressió, perquè eliminin la foto en qüestió.

Per realitzar aquesta sol·licitud, has de seguir les instruccions que el mateix responsable facilita en la seva informació sobre la gestió i protecció de dades.

Per exemple, en una pàgina web el trobarem en la "Política de privadesa".

**Has posat aquesta informació en l'activitat anterior? 🙄**

El més normal és que es posi a disposició dels interessats una adreça de correu electrònic a què enviar aquestes reclamacions de drets.

Un cop presentada la sol·licitud del dret que vols exercir, has d'esperar el temps que marca la llei perquè el responsable respongui; 10 dies per a la rectificació, la supressió i l'oposició (30 per al d'accés).

Un cop complert aquest termini, si no has rebut resposta o aquesta no és satisfactòria, pots posar una denúncia a l'AEPD.

#### Activitat

Ves a la [AEPD - Seu electrònica](https://sedeagpd.gob.es/sede-electronica-web/vistas/infoSede/tramitesCiudadano.jsf).

Presenta una reclamació ..

{% image "aepd-reclamacion.png" %}


.. de "**Publicidad y comunicación comercial**" > "**Recibo llamadas telefónicas publicitarias**".

Tria una empresa qualsevol, que no estigui adherida a <https://www.autocontrol.es>

{% image "aepd-reclamacion-llamadas.png" %}

Comença a presentar la reclamació ... , però no la finalitzis!

Amb aquesta captura de pantalla hi ha prou per l'avaluació:

{% image "aepd-formulario.png" %}

Cert, necessites identificació electrònica: [@Clave](https://clave.gob.es/clave_Home/clave.html)



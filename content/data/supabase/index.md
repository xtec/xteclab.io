---
title: Supabase
---

## Introducció

[Supabase](https://supabase.com/) ...

Crea un compte amb un pla gratuit (Free)

Crea un projecte:

{% image "create-new-project.png" %}


## Docker

Crea una màquina {% link "/windows/wsl" %}:

```pwsh
connect-wsl supabase -new
```

Instal.la {% link "/linux/docker/" %}

```sh
install-docker
```

Instal.la supabase:

```sh
git clone --depth 1 https://gitlab.com/xtec/data/supabase.git
cd supabase
cp .env.example .env
docker compose up -d
```

Verifica que tots els contenidors estan funcionant correctament:

```sh
docker compose ps
```

Accedeix a Supabase Studio a través del API gateway: <http://localhost:8000>.

Les credencials per defecte son username: `supabase` i password: `password`.

En aquest enllaç tens més informació: <https://supabase.com/docs/guides/self-hosting/docker/>


## TODO

* <https://supabase.com/customers/chatbase>
* <https://cazzer.medium.com/designing-the-most-performant-row-level-security-strategy-in-postgres-a06084f31945>
* <https://cazzer.medium.com/supa-rls-exploration-dd174ff086b5>     
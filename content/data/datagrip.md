---
title: DataGrip
description: DataGrip és un entorn de gestió de bases de dades autònom per a desenvolupadors.
--- 

## Introducció

[DataGrip](https://www.jetbrains.com/help/datagrip/getting-started.html) ...

Instal.la DataGrip amb {% link "/tool/scoop/" %}:

```pwsh
> scoop bucket add extras
> scoop install extras/datagrip
```

## Tutorial

Segueix aquest tutorial: [Quick start with DataGrip](https://www.jetbrains.com/help/datagrip/quick-start-with-datagrip.html)

##

Sakila dump files:

```pwsh
> git clone https://github.com/DataGrip/dumps.git
```

    You can open the Files tool window by doing one of the following:

        In the main menu, go to View | Tool Windows | Files.

        On the tool window bar, click Files tool window icon Files.

        Press Alt02.

    In the Files tool window , click the Attach Directory to Project button (Attach Directory to Project) in the toolbar.

    Alternatively, right-click in the area of the Files tool window and select Attach Directory to Project.

    In the file browser, navigate to the directory that you want to attach. In our case, it is the dumps directory.

    Click Open.
    

## Idea

El plugin proporciona suport per a totes les funcions disponibles a DataGrip. 

Has d'activar el plugin:

1. Apreta `Ctrl` `Alt` `S` per obrir **Settings** i a continuació selecciona **Plugins**.

2. Obre la pestanya **Installed**, busca el plugin **Database Tools and SQL**,  i marca la casella al costat del nom del plugin. 

## TODO

* <https://www.jetbrains.com/help/idea/relational-databases.html>

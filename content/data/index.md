---
title: Dades
---

#### Bases de datos

{% pages ["sqlite", "postgres/", "mongodb/"] %}


#### Otros

{% pages ["json/", "filebase/", "minio/", "protection/", "redis/", "supabase/"] %}

---
title: Cassandra
---

## Introducció

[Cassandra](https://cassandra.apache.org) és una de les bases de dades NoSQL més utilitzades.


## Single

1.- Crea un volum amb nom cassandra.

```sh
$ docker volume create --driver local cassandra
$ sleep 10s
```

2.- Executa un nou contenidor amb la base de dades Cassandra. La imatge ha de ser cassandra:3.1 i ha d’utilitzar el volum cassandra per montar el directori /var/lib/cassandra/data

```sh
$ docker run -d --volume cassandra:/var/lib/cassandra/data --name cassandra cassandra:3.1
```

3.- Executa un nou contenidor amb la imatge cassandra:3.1 que executi un client que es connecti a la base de dades cassandra.

```sh
docker run -it --rm --link cassandra:cassandra_client cassandra:3.1 cqlsh cassandra
```

4.- Crea un keyspace amb el nom alumne.

```
create keyspace david
  with replication = {
 'class' : 'SimpleStrategy',
 'replication_factor': 1
};
```

5.- Fes un select del keyspace

```
select *
from system_schema.keyspaces
where keyspace_name = 'david';
```




## Cluster


**1.-** Crea un contenidor temporal  per copiar el fitxer de configuració.

```sh
$ docker run --rm -d --name tmp cassandra:4.1
```


TODO [Google Docs](https://docs.google.com/document/d/1ATv5dLYFRivEx4Dbydx53tlC-nRQ4ftHcvhZluYGW3s/edit?usp=sharing)
---
title: Minio
icon: minio.png
description: Minio és una solució d'emmagatzematge d'objectes d'alt rendiment que proporciona una API compatible amb Amazon Web Services S3 i admet totes les funcions bàsiques de S3
---

## Introducció

[MinIO](https://min.io/) és una solució d'emmagatzematge d'objectes d'alt rendiment que proporciona una API compatible amb Amazon Web Services S3 i admet totes les funcions bàsiques de S3.
Minio és un servei que et permet córrer la teva còpia de S3. Minio et permet simular un storage de S3.

## Entorn de treball

Crea una màquina Ubuntu amb {% link "/windows/wsl/" %}:

```pwsh
> connect-wsl minio -new
```

Instal.la {% link "/linux/docker/" %}:

```sh
$ install-docker
```

Començarem mostrant com funciona MinIO amb el desplegament més senzill: només un ordinador("single-node") i un disc dur ("single-drive").

Crea una carpeta on emmagatzemem les dades:

```sh
$ mkdir -p ~/data
```

Arrenca un contenidor minio:

```sh
$ docker run --rm -d \
   -p 9000:9000 \
   -p 9001:9001 \
   --name minio \
   -v ~/data:/data \
   -e "MINIO_ROOT_USER=admin" \
   -e "MINIO_ROOT_PASSWORD=password" \
   minio/minio server /data --console-address ":9001"
```

Amb la comanda `docker ps` pots veure que el servei està actiu al port 9000:

```sh
$ docker ps
CONTAINER ID   IMAGE         COMMAND                  CREATED          STATUS          PORTS                                                           NAMES
25cce10009ac   minio/minio   "/usr/bin/docker-ent…"   14 seconds ago   Up 14 seconds   0.0.0.0:9000-9001->9000-9001/tcp, :::9000-9001->9000-9001/tcp   minio
```

## Consola

Obre la consola web: <http://localhost:9000>.

T'has d'autenticar amb les credentials que vas definir a les variables d'entorn al executar l'ordre `docker run`: `MINIO_ROOT_USER` i `MINIO_ROOT_PASSWORD`.

{% image "login.png" %}

Llegeix el document [MinIO Console](https://min.io/docs/minio/container/administration/minio-console.html) per entendre com funciona la consola web.

#### Activitat

**1.-** Crea un bucket amb el nom "India" i puja algunes fotos de la India:

{% image "bucket-india.png" %}

## Client

Ara treballarem amb MinIO mitjançant la línia d'ordres.

D'aquesta manera pots treballar més ràpid amb el servidor i automatizar tasques, com poden ser les còpies de seguretat.

Obre un terminal de {% link "/windows/powershell/" %}.

Instal.la l’aplicació client amb {% link "/tool/scoop/" %}:

```pwsh
> scoop install main/minio-client
```

Per tal de no tenir que repetir cada cop l'adreça URL del servidor pots configurar un alias ([`mc alias set`](https://min.io/docs/minio/linux/reference/minio-mc/mc-alias-set.html#command-mc.alias.set)):

```pwsh
> mc.exe alias set local http://localhost:9000 admin password
Added `local` successfully.
```

Ja pots executar ordres amb aquest alias.

Pots mirar el contingut del bucket `india`:

```pwsh
> mc.exe ls local/india
[2024-09-06 09:26:04 CEST]  44KiB STANDARD new-delhi.jpg
[2024-09-06 09:26:15 CEST]  77KiB STANDARD taj-majal.webp
```

O la configuració del clúster:

```pwsh
> mc admin info local
●  localhost:9000
   Uptime: 1 hour 
   Version: 2024-08-29T01:40:52Z
   Network: 1/1 OK 
   Drives: 1/1 OK 
   Pool: 1

┌──────┬───────────────────────┬─────────────────────┬──────────────┐
│ Pool │ Drives Usage          │ Erasure stripe size │ Erasure sets │
│ 1st  │ 0.2% (total: 956 GiB) │ 1                   │ 1            │
└──────┴───────────────────────┴─────────────────────┴──────────────┘

121 KiB Used, 1 Bucket, 2 Objects
1 drive online, 0 drives offline, EC:0l
```

Tenim **1** node i **1** drive ... i tot correcte 👌

Si necessites ajuda d'una comanda pots fer servir el flag `--help`.

Per exemple, com puc crear un bucket amb `mc` ... típica pregunta d'examen 🤔

```pwsh
> mc mb --help
```

Un cop que has llegit l'ajuda 😲 ... ja pots crear un bucket amb el nom `tanzania`:

```pwsh
> mc mb local/tanzania
Bucket created successfully `local/tanzania`.
```

Copia alguns objectes al bucket.

**Important** Han d'estar relacionats amb [Tanzània](https://ca.wikipedia.org/wiki/Tanz%C3%A0nia).

En un examen, si les imatges no són de Tanzània és molt probable que el professor et valori molt malament la resposta 😠.

```pwsh
> mc cp .\serengeti.jpg local/tanzania
...oads\serengeti.jpg: 101.80 KiB / 101.80 KiB [==============================================================] 1.69 MiB/s 0sPS
```

Ves a la consola, entra al bucket i previsualitza ("preview") la imatge.

Fes la catpura de pantalla corresponent (per practicar):

{% image "kilimanjaro.png" %}


Mira l’espai que ocupa un bucket:

```pwsh
> mc du local/tanzania
406KiB  2 objects       tanzania
```

Llista els continguts del bucket:

```pwsh
> mc ls local/tanzania
[2024-09-06 11:07:37 CEST] 304KiB STANDARD kilimanjaro.webp
[2024-09-06 11:04:58 CEST] 102KiB STANDARD serengeti.jpg
```

Esborra un objecte:

```pwsh
> mc rm local/tanzania/kilimanjaro.webp
Removed `local/tanzania/kilimanjaro.webp`.
```

En aquest document tens totes les ordres que pots utilitzar: [MinIO Client](https://min.io/docs/minio/linux/reference/minio-mc.html?ref=docs#minio-mc-commands).


## Cluster

MinIO està dissenyat per crear un cluster d'alta disponibilitat i seguretat format per moltes màquines amb "raids" de discos a nivell de software.

{% image "cluster.jpg" %}

Ara simularem un cluster de 4 servidors, on cada servidor gestiona 2 discos: en total estarem creant un raid distribuït de 8 discos.

Entra de nou a la màquina Ubuntu:

```pwsh
> connect-wsl minio
```

El primer que has de fer és eliminar el servei minio que has creat abans:

```sh
$ docker rm -f mino
```

Clona el projecte minio:

```sh
$ git clone --depth 1 https://gitlab.com/xtec/data/minio
```

Ves a la carpeta `minio/docker` on estan els arxius de configuració:

```sh
$ cd minio/docker/
```

Arrenca els contenidors:

```sh
$ docker compose up -d
[+] Running 16/16
 ✔ minio2 Pulled                       9.6s 
 ✔ minio4 Pulled                       9.6s 
 ✔ nginx Pulled 
...
```

Pots veure que tens **4** contenidors actius que simulen 4 servidors:

```sh
$docker ps
CONTAINER ID   IMAGE                                      COMMAND                  CREATED         STATUS                   PORTS                                                                   NAMES
f5febb38a40d   nginx:1.19.2-alpine                        "/docker-entrypoint.…"   2 minutes ago   Up 2 minutes             80/tcp, 0.0.0.0:9000-9001->9000-9001/tcp, :::9000-9001->9000-9001/tcp   docker-nginx-1
0cb9aff8e283   minio/minio:RELEASE.2023-10-16T04-13-43Z   "/usr/bin/docker-ent…"   2 minutes ago   Up 2 minutes (healthy)   9000-9001/tcp                                                           docker-minio4-1
423e2031fe26   minio/minio:RELEASE.2023-10-16T04-13-43Z   "/usr/bin/docker-ent…"   2 minutes ago   Up 2 minutes (healthy)   9000-9001/tcp                                                           docker-minio3-1
ea5e9ef653d2   minio/minio:RELEASE.2023-10-16T04-13-43Z   "/usr/bin/docker-ent…"   2 minutes ago   Up 2 minutes (healthy)   9000-9001/tcp                                                           docker-minio1-1
ae4ec2bc445e   minio/minio:RELEASE.2023-10-16T04-13-43Z   "/usr/bin/docker-ent…"   2 minutes ago   Up 2 minutes (healthy)   9000-9001/tcp                                                           docker-minio2-1
```

Obre la consola <http://localhost:9000>

Pots veure que els 4 servidors estan "online" i que els 8 discos estan "online":

{% image "metrics.png" %}

Pots parar un instancia servidor (o 2 si vols que el clúster només pugui funcionar en mode lectura) i el clúster seguirá operatiu:

```sh
$ docker stop docker-minio4-1
```

Si mires la consola pots veure que apareix que un servidor està caigut. 

{% image "server-offline.png" %}

Com que és un cluster d'alta disponibilitat pot seguir funcionat encara que li falti un servidor.

Prova de pujar, modificar i borrar objectes.


## Seguretat

MinIO és un clúster (o super RAID) de desenes o milers de discos que pot oferir un emmagatzematge de petabytes amb moltes opcions de disponibilitat, confidencialitat i integritat dels objectes.

**TODO** Explicar el més important

* [Gestió d'objectes](https://min.io/docs/minio/linux/administration/object-management.html)
* [Object amb versions](https://min.io/docs/minio/linux/administration/object-management/object-versioning.html)
* [Retenció d'objectes](https://min.io/docs/minio/linux/administration/object-management/object-retention.html)
* [Cicle de vida dels objectes](https://min.io/docs/minio/linux/administration/object-management/object-lifecycle-management.html)
* [Compressió d'objectes](https://min.io/docs/minio/linux/administration/object-management/data-compression.html)


## Activitats

### Azure

**1.-** Crea una màquina a {% link "/cloud/azure/" %}:

```pwsh
> New-Azure minio
> Connect-Azure minio
```

**2.-** Instal.la {% link "/linux/docker/" %} i arrenca un contenidor minio.

TODO baixar l'script de boc.xtec.dev
TODO hem d'obrir els ports 9000 i 90001

**3.-** Obre la consola a la IP pública


**4.-** Xomparteix la IP pública ...

**5.-** Elimina la màquina d'Azure

```pwsh
> Remove-Azure minio
```

### Isard

En la màquina Linux instal.la el client minio:

```sh
$ wget https://dl.min.io/client/mc/release/linux-amd64/mc
$ chmod +x mc
$ sudo mv mc /usr/local/bin/mc
```

TODO 

crea màquina a Isard, arrenca minio i conecta´t amb l'aplicació client des de WSL.





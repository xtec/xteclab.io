---
title: Definició de dades
mermaid: true
---

## Introducció

Un sistema d’informació és una abstracció de la realitat en que només està representat aquelles constructes que tenen utilitat en el sistema que estem dissenyant.


## Hospital

A l’hospital de Sant Socors hi visiten metges (que es caracteritzen per un codi, nom i cognoms, i una especialitat), que atenen pacients (que tenen un codi, nom i cognoms), els quals tenen malalties (que codifiquem amb un nombre i que tenen un nom tècnic i un nom comú).

### Entitats

El disseny d’un sistema d’informació segueix un metodologia concreta per dissenyar un graf dirigit acíclic (DAG).

**1.-** El primer que hem de fer és crear una abstracció d’aquelles entitats que tenen sentit per si soles:

<pre class="mermaid">
classDiagram
direction LR

class Malaltia {
  # codi
  nom_tecnic
  nom_comu
}
class Metge {
  # codi
  especialitat
}
class Pacient {
  # codi
  nom
  cognoms
}
</pre>

**2.-** A banda de seleccionar les entitats hem de valora si hi ha alguna relació de generalització o especialització entre elles.

Si l’hospital també presta atenció sanitària al seus propis metges, llavor un metge també pot ser pacient. Al dissenyar tenim dos opcions: que un metge pot ser pacient, o que tots els metges també són pacients.

La segona opció és la més senzilla, i és la que escollim:

<pre class="mermaid">
classDiagram
direction LR

class Malaltia {
  # codi
  nom_tecnic
  nom_comu
}
class Metge {
  especialitat
}
Metge --|> Pacient
class Pacient {
  # codi
  nom
  cognoms
}
</pre>

**3.-** A continuaciṕ hem de crear una abstracció d’aquelles entitats que relacionen les entitats anteriors:

<pre class="mermaid">
classDiagram
direction LR

class Malaltia {
  # codi
  nom_tecnic
  nom_comu
}
class Metge {
  especialitat
}
Metge --|> Pacient
class Pacient {
  # codi
  nom
  cognoms
}
</pre>

**3.-** A continuaciṕ hem de crear una abstracció d’aquelles entitats que relacionen les entitats anteriors:

<pre class="mermaid">
classDiagram
direction LR

class Malaltia {
  # codi
  nom_tecnic
  nom_comu
}
class Metge {
  especialitat
}
Metge --|> Pacient
class Pacient {
  # codi
  nom
  cognoms
}
class Assignació {
  data
}

Assignació --> Metge
Assignació --> Pacient

class Diagnostic {
  data
}

Diagnostic --> Pacient
Diagnostic --> Malaltia
</pre>

### Taules

UML és un estàndard de representació de constructes, i per representar sistemes d’informació fem servir diagrames de classes.

En UML hi ha una relació directa entre el model i la base de dades:

1. Cada classe és una taula
2. Cada associació és una clau forana

En aquest exemple tindriem una base de dades hospital amb tot un seguit de taules:

```sql
create database hospital;

create table pacient (codi string primary key, nom string, cognoms string);

create table metge (especialitat string) inherits (pacient);

create table assignació (
  metge string references metge(codi),
  pacient string references pacient(codi),
  data date,
  primary key (metge,pacient,data)
);

...
```


**TODO** Seguir importació [Google Docs](https://docs.google.com/document/d/1Jc_e96gGHhKXZWKnm_S1TbeWs5mQIM-aP_WMbkiGOZg/edit?usp=sharing)

##

```sql
create table product (
   product_id int primary key generated always as identity,
   name text not null unique
);

create table product_price (
	product_id int references product,
	since date default CURRENT_DATE,
	price numeric(6,2) check (price > 0),
	primary key (product_id, since)
);

create table "order" (
   order_id int primary key generated always as identity,
   date date not null default CURRENT_DATE,
   comment text
);

create table order_item (
   order_id int references "order" on delete cascade,
   product_id int references product,
   quantity int check (quantity > 0),
   primary key(order_id, product_id)
);
```

[Identity column](https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-identity-column/). Constraint GENERATED AS IDENTITY allows you to automatically assign a unique number to a column.

[Default](https://www.postgresql.org/docs/current/ddl-default.html). A column can be assigned a default value. When a new row is created and no values are specified for some of the columns, those columns will be filled with their respective default values.

## Constraints

[Primary Key](https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-primary-key/). A primary key is a column or a group of columns used to identify a row uniquely in a table.

[Foreign Key](https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-foreign-key/). A foreign key is a column or a group of columns in a table that reference the primary key of another table.

[Not-Null](https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-not-null-constraint/). If a column has a NOT NULL constraint, any attempt to insert or update NULL in the column will result in an error.

[Unique](https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-unique-constraint/). A UNIQUE constraint makes sure that values stored in a column or a group of columns are unique across rows in a table.

[Check](https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-check-constraint/). A CHECK constraint allows you to specify if values in a column must meet a specific requirement.

## Views

A view is a named query that provides another way to present data in the database tables. A view is defined based on one or more tables which are known as base tables.

* [Managing PostgreSQL views](https://www.postgresqltutorial.com/postgresql-views/managing-postgresql-views/) – introduce you to the concept of the view and show you how to create, modify, and remove PostgreSQL views.

*  [Drop view](https://www.postgresqltutorial.com/postgresql-views/postgresql-drop-view/) – learn how to drop one or more views from the database.

* [Create updatable views](https://www.postgresqltutorial.com/postgresql-views/postgresql-updatable-views/) – give you examples of creating updatable views that allow you to issue INSERT, UPDATE, and DELETE statements to update data in the base tables through the views.

* [Materialized views](https://www.postgresqltutorial.com/postgresql-views/postgresql-materialized-views/)  – introduce you to materialized views and provide you with the steps of creating and refreshing data for materialized views.

* [Creating updatable views using the WITH CHECK OPTION clause](https://www.postgresqltutorial.com/postgresql-views/postgresql-views-with-check-option/) – show you how to use the WITH CHECK OPTION clause to check the view-defining condition when you make a change to the base table through the view.

* [Create recursive views](https://www.postgresqltutorial.com/postgresql-views/postgresql-recursive-view/) – introduce you to the recursive view and show you an example of creating a recursive view in PostgreSQL.


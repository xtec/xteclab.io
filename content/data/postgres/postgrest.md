---
title: PostgREST
description: PostgREST és un servidor web que converteix una base de dades PostgreSQL directament en una API RESTful
---

## Introducció

[PostgREST](https://docs.postgrest.org/en/v12/) és un servidor web que converteix una base de dades PostgreSQL directament en una API RESTful. Les restriccions estructurals i els permisos de la base de dades determinen els punts finals i les operacions de l'API.

## Entorn de treball

### Base de dades

Executa una base de dades postgres:

```sh
docker run -d --name postgres --restart=always -p 5432:5432 -e POSTGRES_PASSWORD=password -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=password postgres:17
```

Crea una base de dades per a la API:

```sh
$ docker exec -it postgres psql -U postgres
```

El primer que fem és crear un esquema amb nom per als objectes de la base de dades que s'exposaran a l'API.

```sql
create schema api;
```

La nostra API tindrà un punt final, `/todos`, que vindrà d'una taula.

```sql
create table api.todos (
  id serial primary key,
  done boolean not null default false,
  task text not null,
  due timestamptz
);

insert into api.todos (task) values
  ('Menjar una poma'), ('Meditar una estona');
``` 

A continuació, feu un rol per utilitzar-lo per a sol·licituds web anònimes. Quan arriba una sol·licitud, PostgREST canviarà a aquesta funció a la base de dades per executar consultes.

```sql
create role web_anon nologin;

grant usage on schema api to web_anon;
grant select on api.todos to web_anon;
```

El rol `web_anon` té permís per accedir als objectes de l'esquema `api` i per llegir les files de la taula `todos`.

És una bona pràctica crear un rol dedicat per connectar-se a la base de dades, en lloc d'utilitzar el rol `postgres` altament privilegiat. 

Així que ho farem, anomenarem el rol `authenticator` i també li donarem la possibilitat de canviar al rol `web_anon`:

```sql
create role authenticator noinherit login password 'password';
grant web_anon to authenticator;
```

Ara sortiu de psql; és hora d'iniciar l'API!

\q

### API

Executa PostgREST:

```sh
docker run --name postgrest --network host -p 3000:3000 -e PGRST_DB_URI=postgres://authenticator:password@localhost:5432/postgres -e PGRST_DB_SCHEMA=api -e PGRST_DB_ANON_ROLE=web_anon postgrest/postgrest
```

En aquest enllaç tens més paràmetres de configuració: [Configuration](https://docs.postgrest.org/en/v12/references/configuration.html)


Obriu un terminal nou (deixant obert el que s'està executant PostgREST dins). 

Prova de fer una sol·licitud HTTP per a `todos`.

```sh
$ curl http://localhost:3000/todos

[{"id":1,"done":false,"task":"Menjar una poma","due":null}, 
 {"id":2,"done":false,"task":"Meditar una estona","due":null}]
```

Amb els permisos de rol actuals, les sol·licituds anònimes tenen accés només de lectura a la taula `todos`. 

Si intentem afegir un nou "todo", no podem_

```sh
$ curl http://localhost:3000/todos -X POST \
     -H "Content-Type: application/json" \
     -d '{"task": "do bad thing"}'

{"code":"42501","details":null,"hint":null,"message":"permission denied for table todos"
```

La resposta és 401 no autoritzada.


## TODO

* <https://github.com/mattddowney/compose-postgrest>
* <https://neon.tech/postgresql/tutorial>
---
title: Manipulació de dades
---

## Transaccions

**Transaction**. A database transaction is a single unit of work that consists of one or more operations.

```sql
create table product (
    id int primary key generated always as identity,
    name text not null unique
)
```

**Rollback**. Executa aquest exemple pas a pas per comprovar com funciona un rollback

```sql
begin;
insert into product(name) values ('iphone');
select * from product; -- Iphone is listed
rollback;

select * from product; -- Iphone is not listed
```

**Conflicte**. Obre dues sessions i executa fins abans del commit. Pots comprovar que la segona transacció queda bloquejada fins que executes un `commit` o `rollback` en la primera.

```sql
begin;
insert into product(name) values ('S20');
commit;
```

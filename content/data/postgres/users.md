---
title: Postgres - Usuaris
---

## Introducción

Crea un contenedor postgres y abre un terminal interactivo

```sh
$ docker run -d --name postgres -p 5432:5432 -e POSTGRES_PASSWORD=password postgres:16
$ docker exec -it postgres psql -U postgres
```

Crea una base de datos `test`:

```
postgres=# create database test;
CREATE DATABASE
postgres=# create user david with password 'password';
CREATE ROLE
postgres=# grant all privileges on database test to david;
GRANT
```

Te puedes conectar a la base de datos `test` que acabas de crear con tu nombre de usuario:

```
postgres=# \c test david
You are now connected to database "test" as user "david".
test=> 
```

Puedes seleccionar `current_user` para confirmar que el usuario "david" es el usuario que está actualmente conectado a la base de datos. 

```
test=> select current_user;
 current_user 
--------------
 david
(1 row)

test=>
```

## Referencias

* [PostgreSQL ERROR: permission denied for schema public](https://www.cybertec-postgresql.com/en/error-permission-denied-schema-public/)
* [How to Use the Postgres Docker Official Image](https://www.docker.com/blog/how-to-use-the-postgres-docker-official-image/)
---
title: Entorno de trabajo
description: PostgreSQL 
---

## Introducción

Si no conoces como funciona una base de datos SQL, empieza por las actividades de {% link "/data/sqlite/" %}.

## Windows 

Instal.la postgres con {% link "/tool/scoop/" %}:

```pwsh
> scoop install postgresql
```

[pg_ctl](https://www.postgresql.org/docs/current/app-pg-ctl.html) es una utilidad para inicializar un clúster de base de datos PostgreSQL , iniciar, detener o reiniciar el servidor de base de datos PostgreSQL ( postgres ) o mostrar el estado de un servidor en ejecución.

Ejecuta `pg_ctl start` para iniciar la base de datos:

```pwsh
> pg_ctl.exe start
...
server started
```

En cualquier momento puedes detener la base de datos con `pg_ctl stop`.

### psql

[psql](https://www.postgresql.org/docs/current/app-psql.html) es un "shell" para PostgreSQL que te permite escribir consultas de forma interactiva, enviarlas a PostgreSQL y ver los resultados de la consulta.

Inicia una sesión con el usuario `postgres`:

```pwsh
> psql -U postgres
postgres=# 
```

Un servidor Postgres puede gestionar varias bases de datos al mismo tiempo. 

Utiliza el comando `\l` (o `\list`) para ver todas las bases de datos definidas en el servidor, 

```sql
postgres=# \l
                                                List of databases
   Name    |  Owner   | Encoding | Locale Provider | Collate | Ctype | Locale | ICU Rules |   Access privileges
-----------+----------+----------+-----------------+---------+-------+--------+-----------+-----------------------
 postgres  | postgres | UTF8     | libc            | C       | en    |        |           |
 template0 | postgres | UTF8     | libc            | C       | en    |        |           | =c/postgres          +
           |          |          |                 |         |       |        |           | postgres=CTc/postgres
 template1 | postgres | UTF8     | libc            | C       | en    |        |           | =c/postgres          +
           |          |          |                 |         |       |        |           | postgres=CTc/postgres
(3 rows)
```

`postgres` es la base de datos predeterminada a la que te conectas. 

Crea una base de datos `test`:

```sql
postgres=# create database test;
CREATE DATABASE
```

Conectate a la base de datos `test`:
```
postgres=# \c test 
You are now connected to database "test" as user "postgres".
test=> 
```

Con `\dt` puedes ver que tablas tiene la base de datos:

```sql
postgres=# \dt
Did not find any relations.
```

En qualquier momento puedes "limpiar" el shell con `\! cls`:

```sql
postgres=# \! cls
```

Con `\q` finalizas la sesión:

```
test=# \q
$
```

### pgAdmin

Al instalar `postgres` también has instalado [pgAdmin](https://www.pgadmin.org/).

Aprieta la tecla windows `⊞` i selecciona "pgAdmin 4".

Añade el servidor (en Windows no necesitas password):

{% image "pgadmin.gif" %}


## Linux

Inicia una máquina virtual Linux con {% link "/windows/wsl/" %}.

```pwsh
> connect connect-wsl postgres -new
```

### Apt

Insta.la postgres:

```sh
sudo apt update && sudo apt install -y postgresql
```

### docker

Crea un contenedor postgres con {% link "/linux/docker/" %}:

```sh
docker run -d --name postgres --restart=always -p 5432:5432 -e POSTGRES_PASSWORD=password -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=password postgres:17
```

Instala [pgAdmin](https://www.pgadmin.org/):

```sh
docker run -d --name pgadmin --restart=always --network host -e PGADMIN_DEFAULT_EMAIL=postgres@mail.com -e PGADMIN_DEFAULT_PASSWORD=password dpage/pgadmin4:8
```

Abre el navegador en [http://localhost](http://localhost).

A continuación tienes un video que te muestra como configurar un servidor.

Si no has modificado los parámetros configurados por defecto, el usuario es `postgres@mail.com` y la contraseña `password`.

{% image "pgadmin.gif" %}

## Datagrip

Puedes utilitzar {% link "/data/datagrip/" %} para gestionar la base de datos:

{% image "datagrip.gif" %}

## Scott

Scott és una base de dades demo.

### Windows

Importa la base de dades "Scott":

```pwsh
> wget https://gitlab.com/xtec/postgres-data/-/raw/main/scott.sql -O scott.sql
> psql -U postgres -f scott.sql
```

En la página web de pgAdmin selecciona la base de datos Scoot.

A continuación abre la herramienta "Query Tool".

Ya puedes hacer consultas a la base de datos:

{% image "query.png" %}

En la actividad {% link "/data/postgres/scott/" %} tienes muchos ejemplod de consultas a la base de datos Scott.

### Linux

Con docker:

```sh
wget https://gitlab.com/xtec/postgres-data/-/raw/main/scott.sql
docker cp scott.sql postgres:scoot.sql
cat scott.sql | docker exec -i postgres su postgres -c "psql"
```
---
title: Privilegios
---

To check the privileges of user by following:

```sql
SELECT * FROM pg_user;
```

As the same way, to check roles:

```sql
SELECT * FROM pg_roles;
```
---
title: Neon
description: Neon es un entorno Postgres en la nube sin servidor que separa el almacenamiento y la computación.
---

## Introducción

[Neon](https://neon.tech/) ...

Las cargas de trabajo de las bases de datos se están trasladando a la nube; nadie quiere gestionar una base de datos por sí mismo.

### Arquitectura

El costo es particularmente importante cuando se diseña un sistema para la nube. un servicio Postgres moderno se puede diseñar de manera diferente para que sea más económico y eficiente en entornos de nube mediante un enfoque de **separación de almacenamiento y computación**.

Una de las implicaciones inmediatas es no usar nunca volúmenes EBS y usar en su lugar una combinación de almacenamiento local y S3. Almacenamiento local para datos activos y S3 para datos inactivos.

{% image "architecture.png" %}

Cada usuario obtiene un Postgres dedicado que se ejectua en un contenedor, mientras que los datos se almacenan de forma segura en un sistema multiusuario escrito en Rust.

El almacenamiento consta de dos servicios: 

* Safekeepers implementa un protocolo de consenso.

* Los Pageservers sirven páginas de base de datos con baja latencia y proporcionan "espacio de trabajo" para actualizaciones. Los Pageservers no son parte del sistema de registro: puedes perder todos los Pageservers y no perderás ningún dato.

La combinación de Safekeepers y S3 proporciona el sistema de registro.

Esta arquitectura ofrece una sorprendente cantidad de beneficios:

1. El cómputo y el almacenamiento están separados, por lo que no se produce un exceso de aprovisionamiento. El almacenamiento está respaldado por un S3 confiable y económico, por lo que no hay que triplicar el costo de almacenamiento de los datos. Los "Safekeepers" solo tienen que almacenar una pequeña ventana de los datos más recientes que aún no se guardan en S3. Además, los "Safekeepers" y "Pageservers" se pueden compartir entre usuarios, por lo que el usuario final tampoco paga el triple por el cómputo. Las copias de seguridad y las restauraciones se integran perfectamente, ya que están todas incorporadas en la arquitectura de almacenamiento.

2. Dado que el almacenamiento es independiente, la computación, que es un proceso de Postgres, se vuelve sin estado (salvo el caché del búfer). Esto permite reprogramar dinámicamente la computación y moverla de un nodo a otro. Y esto abre la posibilidad de ejecutar una capa de comptuación que se escala en respuesta a los cambios en el tráfico, incluida la reducción a 0 cuando la base de datos no está en uso.

3. Esta arquitecture permite ramificar instantáneamente su base de datos Postgres de manera inmediata utilizando la técnica "copy on write" para crear entornos de prueba, probar migraciones con un "snapshot" del entorno de producción, etc. 

## Entorno de trabajo

Crea un nuevo proyecto:

{% image "create-project.png" %}

## Documentación

<https://neon.tech/docs/introduction>


## TODO

* <https://neon.tech/postgresql/postgresql-getting-started/postgresql-sample-database>

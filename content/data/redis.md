---
title: Redis
---

## Introduction

Redis is an exceptionally fast remote in-memory database solution. The key-value data model enables Redis to handle large datasets while [maintaining high availability](https://phoenixnap.com/blog/what-is-high-availability) and read-write speeds.

[NoSQL databases](https://phoenixnap.com/kb/what-is-nosql), like Redis, are meant to run efficiently in distributed clusters that scale out horizontally. Using Docker to deploy Redis in a container makes horizontal scaling a routine, straightforward process.

## Start a Docker Redis Container

Install [Docker](../eines/docker.md) in your Ubuntu distribution.

Check the current status of the Docker service by entering the following command in your terminal, and check that the output confirms that Docker is running and active

```shell
sudo systemctl status docker
```

Retrieve and start a Redis container (m7-redis) with the `docker run` command:

```shell
sudo docker run --name m7-redis -d redis
```

The command did not specify a Redis version. The system proceeds to download the latest available version of Redis by default.

Once the installation process is complete, check the status of current docker containers with the `docker ps` command:

```shell
sudo docker ps
```

Among other information, the system provides:

* The unique container ID - `<id>`
* Access Port - `6379` (default Redis port number)
* The defined container name - `m7-redis`

## Connect to Redis with redis-cli

Start the interactive redis-cli command shell using the following command:

```shell
sudo docker exec -it m7-redis sh
```

> **Note**: You can also use the unique container ID instead of the container name.

Once you access the interactive shell, type `redis-cli` to connect to the Redis container instance.

```shell
redis-cli
```

### Try Basic Redis Commands

The Redis `ping` command is useful for testing if a connection to the Redis database is active:

```shell
ping
```

The response, PONG, indicates that the connection is successful.

[Key-value](https://phoenixnap.com/kb/nosql-database-types#htoc-key-value-database) stores use the simplest possible data model. A unique key is paired with a value. Use the `set` command to define the key name and the value pair as david:

```shell
set name david
```

You can retrieve the value using the unique key name and the `get` command:

```shell
get name
```

The result retrieves the previously defined david value. A list of data types and commands is available in phoenixNAP comprehensive guide [Redis Data Types With Commands](https://phoenixnap.com/kb/redis-data-types-with-commands).

Once you have explored redis-cli commands, type `quit` to return to the container terminal interface.

Type `exit` to close the connection with the Docker container.

## Referències

* [How to Deploy and Run Redis in Docker](https://phoenixnap.com/kb/docker-redis)
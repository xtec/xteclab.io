---
title: Database
icon: odoo.png
description: "Odoo utilitza una base de dades per organitzar tota la informació amb moltes taules, i on cada plugin incorpora les seves pròpies taules"
mermaid: true
---

## Introducció

Ves al directori de Odoo i elimima la instal.lació de l'activitat anterior {% link "/web/app/odoo/user/" %}

```sh
cd odoo
docker-compose down
```

Torna a instal.lar Oddo, **amb les dades de Demo!**

```sh
docker-compose up -d
```

Activa els mòduls d'empleats, (...) per tenir més taules i dades.

## Docker

Per instal.lar odoo hem fet servir `docker-compose`.

La instal.lació d'odoo consisteix en dos contenidors, `db` i `odoo`:

```sh
$ docker-compose ps
Name              Command              State                              Ports
-----------------------------------------------------------------------------------------------------------
db     docker-entrypoint.sh postgres   Up      5432/tcp
odoo   /entrypoint.sh odoo             Up      0.0.0.0:8200->8069/tcp,:::8200->8069/tcp, 8071/tcp, 8072/tcp
```

Investiga que és docker i que és docker-compose: {% link "/linux/docker/compose/" %}

### Contenidor `odoo`

Entra al contenidor `odoo`:

```sh
docker exec -it odoo /bin/bash
```

Quan arranca el contenidor es crita el fitxer `entrypoint.sh`.

Mira el contingut d'aquest fitxer:

```sh
$ more /entrypoint.sh

...
# set the postgres database host, port, user and password according to the environment
# and pass them as arguments to the odoo process if not present in the config file
: ${HOST:=${DB_PORT_5432_TCP_ADDR:='db'}}
: ${PORT:=${DB_PORT_5432_TCP_PORT:=5432}}
: ${USER:=${DB_ENV_POSTGRES_USER:=${POSTGRES_USER:='odoo'}}}
: ${PASSWORD:=${DB_ENV_POSTGRES_PASSWORD:=${POSTGRES_PASSWORD:='odoo'}}}
...
```

En aquest fitxer pots veure entre altres coses la configruació d'odoo per accedir a la base de dades.

L'ultima acció del fitxer és executar l'aplicació `odoo`.

Si executes `which odoo` pots veure que el fitxer està localitzat a `/usr/bin/odoo`.

Si mires el contingut del fitxer `odoo` pots veure que es un script en python que importa el mòdul de python `odoo`:

```sh
$ more more /usr/bin/odoo

#!/usr/bin/python3

# set server timezone in UTC before time module imported
__import__('os').environ['TZ'] = 'UTC'
import odoo

if __name__ == "__main__":
    odoo.cli.main()
```

**Activitat**. Mira on està el mòdul `odoo`.

Quan acabis surt del contenidor amb la comanda `exit`

### Contenidor `db`

El contenidor `db` té una base de dades postgres que és la que utilitza el contenidor `odoo`.

Entra al contenidor `db`:

```sh
docker exec -it db /bin/bash
```

Fes servir el client de postgres per conectarte al la base de dades. 

La base de dades és `odoo` i l'usuari és `odoo`:

```sh
psql -d odoo -U odoo
```

Amb la comanda `\d` pots veure el llistat de totes les relacions:

```
$ \d
                                List of relations
 Schema |                         Name                         |   Type   | Owner
--------+------------------------------------------------------+----------+-------
 public | auth_totp_device                                     | table    | odoo
 public | auth_totp_device_id_seq                              | sequence | odoo
 public | auth_totp_wizard                                     | table    | odoo
 ...
```

Un munt de relacions!

Anem a contar quantes taules i vistes hi ha:

```sql
select count(*) from information_schema.tables;
 count 
-------
   412
(1 row)
```

Com que he activat l’aplicació d’empleats tinc una taula de empleats:

```sh
\d hr_employee;
                                                  Table "public.hr_employee"
             Column             |            Type             | Collation | Nullable |                 Default                 
--------------------------------+-----------------------------+-----------+----------+-----------------------------------------
 id                             | integer                     |           | not null | nextval('hr_employee_id_seq'::regclass)
 resource_id                    | integer                     |           | not null | 
 company_id                     | integer                     |           | not null | 
 resource_calendar_id           | integer                     |           |          | 
 ...
```

No està malament la taula !

Com que he carregat les dades de demostració la taula no està buida.
 
Anem a seleccionar tots els noms dels empleats:

```sql
select name from hr_employee;

       name       
------------------
 Mitchell Admin
 Ronnie Hart
 Anita Oliver
...
(20 rows)
```

## Activitats

### Afegir un empleat

A continuació afegeix un treballador a la taula `hr_employee`:

Primer hem de veure quines columnes són obligatòries:

```sql
$ select column_name, data_type, column_default from information_schema.columns where table_name = 'hr_employee' and is_nullable = 'NO';

  column_name  |     data_type     |             column_default              
---------------+-------------------+-----------------------------------------
 id            | integer           | nextval('hr_employee_id_seq'::regclass)
 resource_id   | integer           | 
 company_id    | integer           | 
 employee_type | character varying | 
(4 rows)
```

`is_nullable` no és de tipus booleà, sinò un `varchar(3)` per compatibilitat amb la "SQL 2011 spec".

L'atribut `id` és la clau primària de la relació i es genera de manera automàtica.

Si mirem les claus foràneas podem veure que els atributs `resource_id` i `company_id` tenen restriccions d'integritat:

```sql
$ \d
...
   "hr_employee_company_id_fkey" FOREIGN KEY (company_id) REFERENCES res_company(id) ON DELETE SET NULL
   "hr_employee_resource_id_fkey" FOREIGN KEY (resource_id) REFERENCES resource_resource(id) ON DELETE RESTRICT
```

Sembla que el model de dades és aquest:

<pre class="mermaid">
classDiagram
  class res_company {
    id integer
  }
  class resource_resource {
    id integer
  }
  class hr_employee {
    id interger nextval
    employee_type character varying
  }
  hr_employee --> res_company : company_id
  hr_employee --> resource_resource : resource_id
</pre>

Mira la taula `res_company`:

```sql
$ select id, name from res_company;

 id |    name     
----+-------------
  1 | YourCompany
```

Mira la taula `resource_resource`:

```sql
$ select id, name from resource_resource;
 id |       name       
----+------------------
  1 | Mitchell Admin
  2 | Ronnie Hart
...
(20 rows)
```

Sembla que les dades dades d'empleats estan en dues taules `hr_employee` i  `resource_resource`!

## Còpia de seguretat

Fes una còpia de seguretat de la base de dades

```sh
$ docker exec db bash -c "pg_dump -U odoo -Fc odoo > odoo.bak"
$ docker cp db:odoo.bak .
Successfully copied 2.26MB to /home/david/site/.
```



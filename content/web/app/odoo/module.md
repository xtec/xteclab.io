---
title: Mòdul
description:  "Odoo és pot extendre mijantçant plugins (mòduls)"
---

## Introducció

Odoo, com qualsevol ERP facilita la gestió del 60-70 % de les activitats d’una empresa.

En una empresa petita no es nota massa, però a mida que l’empresa és més gran aquestes activitats s’han d’automatizar. Això s’aconsegueix mitjançant extensions (plugins) que els programadors com vosaltres escriuen, i a diferència de SAP, no es necessita pagar llicència:

* A [Odoo Apps](https://apps.odoo.com/apps) tens mòduls, industires i temes gratuïts i de pagament.

### Instal.lar un mòdul

Un dels mòduls més descarregats és [Odoo Accounting](https://apps.odoo.com/apps/modules/17.0/om_account_accountant/).

### Crear un nou mòdul

Ara que ja coneixeu el producte, podeu pensar en un cas particular que una empresa pot necessitar, bé perquè és una activitat molt específica, o perquè la normativa del país és diferent.

Reproduïr el següent vídeo on s'ensenya com crear un nou mòdul a Odoo 14:

<iframe width="560" height="315" src="https://www.youtube.com/embed/T1tvTfs4gok?si=wSSfTQebAwJLrle0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>


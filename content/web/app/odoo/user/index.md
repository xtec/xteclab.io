---
title: Usuari
icon: odoo.png
description: "En aquesta activiata aprendrem a utilitzar Odoo des del punt de vista d'un usuari"
---

## Introducció

[Odoo](https://www.odoo.com/) és un [ERP](https://ca.wikipedia.org/wiki/Planificaci%C3%B3_de_recursos_empresarials) i CRM de còdi obert creat amb [Python](/python).

Arrenca una màquina virtual amb [WSL](/windows/wsl/):

```pwsh
> connect-wsl odoo -new
```

Instal.la docker:

```sh
$ install-docker
```

Instal.la i arrenca Odoo:

```sh
git clone https://gitlab.com/xtec/web/odoo.git
cd odoo
docker-compose up -d
```

A partir d’ara cada cop que arrenquis la màquina odoo arrencarà de manera automàtica.

Obre el navegador web de la màquina anfitriona a l'adreça [http://localhost:8200](http://localhost:8200).

El primer que has de fer és configurar l’aplicació:

1. Pots modificar el Master Password (el necessites per fer operacions sobre la base de dades desde la interficie gràfica)

2. El nom de la base de dades és **odoo**. 

3. Selecciona que es creïn dades de demostració.

{% image "create-database.png" %}

Tardarà una estona mentres configura la base de dades, i un cop enllestit, ja pots entrar dins l’aplicació un cop t’hagis identificat.

{% image "login.png" %}

Com pots veure Odoo consisteix en un conjunt d’aplicacions que es poden activar ( i actualitzar):

{% image "apps.png" %}

## Demo

La base de dades de demostració que has instal.lat és la mateixa que pots trobar a [https://demo.odoo.com/](https://demo.odoo.com/)

Activa la gestió d’empleats per poder accedir a les dades d’empleats:

{% image "empleats.png" %}

Ja pots accedir a la informació d’empleats:

{% image "employee-list.png" %}

## Acticitat

Ves al directori de Odoo i elima la instal.lació de l’activitat anterior:

```sh
cd odoo
docker-compose down
docker volume prune -f
```

Torna a instal.lar Oddo, **però aquest cop sense les dades de Demo!**

```sh
docker-compose up -d
```

### Configuració

Ves a configuració:

{% image "config.png" %}

Crea un nou usuari:

{% image "config-new-user.png" %}

### La nostra empresa

REPAPER és una societat anònima que es dedica a la comercialització de papers reciclats en diferents presentacions.

Tres gammes de productes:

* Paper per a escriptura i fotocopiadores amb tres tipus diferents.         
* Sobres i bosses amb cinc tipus i mides diferents.         
* Material de congrés.

Les dades bàsiques de Repaper, S.A. són:

| | |
|-|-|
| CIF | A-08098833 |
| Adreça: | Carrer Sant Pius X, Núm. 8 |
| CP i Localitat | 08901 L’Hospitalet de Llobregat |
| Telèfon: | 93 3382553 |
| Fax: | 93 3375735 |
| e-mail: | info@repaper.com |
| Capital Social | 100.000 € |

Ves a `Configuració > Empreses` i modifica la empresa "My Company".

**Important!. No has de crear una empresa nova perquè la versió gratuïta d’Odoo no permet crear departaments en altres empreses que no siguin la principal.**

{% image "config-empreses.png" %}

I actualitza les dades de la nostra empresa

{% image "repaper.png" %}

## Clients

Activa l’aplicació de Vendes.

{% image "vendes.png" %}

Pots crear diversos contactes per una empresa

{% image "contactes.png" %}

## Proveïdors

Activa l’aplicació de Compres.

El mateix que has fet amb clients ho pots fer amb els proveïdors:

{% image "compres.png" %}

## Empleats

Activa l’aplicació d’Empleats.

Repaper s'organitza en quatre departaments:

1. COMPRES - Compres i magatzem (gestion de compras)
2. VENDES - Vendes i màrqueting (gestion de ventas)
3. Administració - Direcció, RRHH (directorio de empleados)
4. COMPTABILITAT - Comptabilitat, tresoreria i liquidació d’impostos

L'empresa REPAPER disposa d’una plantilla de 4 treballadors, distribuïts de la següent manera:

* 1 Gerent amb contracte indefinit
* 1
* 1
* 1

Crea els departaments corresponents:

{% image "departaments.png" %}

Crea els empleats corresponents:

{% image "repaper-empleats.png" %}

A continuació tens una llista dels empleats de Repaper, S.A.

{% image "repaper-llista-empleats.png" %}

## Usuari

Perquè un empleat també sigui usuari d’Odoo l’has de donar d’alta com usuari:

{% image "repaper-usuari.png" %}

I l’has de connectar amb l’empleat:

{% image "repaper-usuari-empleat.png" %}

## Inventari

Activa l’aplicació d’Inventari.

Afegeix un producte per vendre:

{% image "repaper-inventari.png" %}

Afegeix un producte per comprar:

{% image "repaper-inventari-bobina.png" %}

Afegeix un proveïdor del producte per comprar:

{% image "repaper-inventari-bobina-compra.png" %}

### Contactes

Activa l’aplicació de Contactes.

Se encuentran todos los contactos introducidos al ERP(clientes, proveedores, usuarios,..)

{% image "repaper-contactes.png" %}


## Bibliografia

* [Odoo](https://www.odoo.com/es_ES/) 
* [Asociación Española de Odoo](https://www.aeodoo.org/) 

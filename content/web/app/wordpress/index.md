---
title: Wordpress
---

{% pages ["intro/"] %}


* [First steps](https://docs.google.com/document/d/1ri8kqisWGSBlBl7sM1DijYcSI_7mHaRTS57AfeoUMF4/edit?usp=sharing)
* [Temes](https://docs.google.com/document/d/1nwq5exq4D-3JLd2srwGxG1rFJ_L_4lEEIDh5DaQmW-E/edit?usp=sharing)
* [Plugins](https://docs.google.com/document/d/11hdPr_97NqT5v5ee70e2t0dd5b9c5myAnSaeuWD-slI/edit?usp=sharing)
* [WooCommerce](https://docs.google.com/document/d/1Fsfmdt0tEL_ds-npIeHLJTwW785275Y0z-RBidW47_0/edit?usp=share_link)
* [WPForms - TablePress - Imagify](https://docs.google.com/document/d/1zvkay9-wGqC5ZPDX79UB-yuASh1rIEqvnBal-QqlX_A/edit?usp=sharing)
* [Seguretat i còpies](https://docs.google.com/document/d/1tpfsj_btIe5_MbWwZJYDvp4i4AxErZtTiH-a6D3JOVE/edit?usp=share_link)
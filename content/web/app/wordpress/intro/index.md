---
title: Introducció
---

## Introducció 

## Local

Arrenca una màquina virtual amb [WSL](/windows/wsl/).

Clona el projecte <https://gitlab.com/xtec/web/wordpress>:

```sh
$ git clone https://gitlab.com/xtec/web/wordpress
```

Canvia a la carpeta `wordpress`:

```sh
$ cd wordpress
```

Arrenca l'aplicació cryptpad:

```sh
$ docker compose up -d
```

Obre el navegador:

<http://localhost:800>


{% image "select-language.png" %}

{% image "welcome.png" %}

Nota: El lloc de WordPress no està disponible immediatament al port 8000 perquè els contenidors encara s'estan inicialitzant i poden trigar un parell de minuts abans de la primera càrrega.

## Online

Pots crear un Wordpress amb un servei online:

## TasteWP.com

[TasteWP](https://tastewp.com/) ...

## Wordpress.com

Crea un lloc web gratuit a [https://wordpress.com/free/](ttps://wordpress.com/free) utlitzant un subdomini worpress.com.

Busca l’opció gratuïta:

{% image "free.png" %}

Torna a escollir l’opció gratuïta

---
title: Etherpad
description: Etherpad és un editor col·laboratiu en temps real de codi obert i basat en web, que permet als autors editar simultàniament un document de text i veure totes les edicions dels participants en temps real.
---

## Introducció

[Etherpad](https://etherpad.org/) va ser dissenyat per col·laborar ràpidament en un text públic, prenent notes de reunions o classes en línia de manera col·laborativa i en temps real. És fàcil d'utilitzar per a classes en línia o presencials, i posa a tothom literalment "a la mateixa pàgina".

EtherPad ofereix moltes de les mateixes funcions que Google Docs, però no és adequat per a informació sensible, ja que té funcions limitades de protecció de contrasenya i control d'accés.

Tanmateix, és una veritable eina de col·laboració en temps real que pot implicar visualment els estudiants en classes en línia i presencials.

## Instal·lar

Arrenca una màquina virtual amb [WSL](/windows/wsl/).

Clona el projecte <https://gitlab.com/xtec/web/etherpad>

```sh  
$ git clone https://gitlab.com/xtec/web/etherpad
```

Es crea una nova carpeta amb el nom `etherpad`.

Canvia a la carpeta `etherpad`:

```sh
$ cd etherpad
```

En aquesta carpeta hi ha un fitxer amb el nom `docker-compose.yml` .

Aquest fitxer defineix una composició docker que pots executar amb {% link "/linux/docker/compose/" %}.

Arrenca el servei etherpad:

```sh
$ docker compose up -d
```

Obra el navegador a l'adreça [http://localhost:3000](http://localhost:3000) per connectar-te a Etherpad.

{% image "etherpad.png" %}

## Funcions

#### Pad

Les edicions de diferents autors estan codificades per colors i hi ha una finestra de xat que permet parlar en temps real amb altres persones que treballen en el mateix document. Podeu desfer i refer canvis fàcilment, i importar/exportar com a text, HTML, PDF i altres formats habituals.

A més de l'edició col·laborativa en temps real, té més característiques i funcions.

#### Chatbox integrada i pads únics

EtherPad té un ChatBox incorporat per a cada pad, de manera que us podeu comunicar amb altres participants d'un pad en una agenda. EtherPad ofereix URL únics per a cada pad i pots convidar col·laboradors a un pad compartint l'enllaç amb ells o convidant-los a través de l’ opció Invite other users proporcionada a sobre del ChatBox.

#### Time Slider

En combinació amb el control de versions de l'aplicació, Etherpad ofereix una característica interessant anomenada Time Slider.

Time Slider permet veure tots els canvis, edicions i addicions dels col·laboradors d'un pad a través del temps i les diferents revisions. Això crea una representació semblant a un vídeo dels esdeveniments que van passar al llarg del temps al pad. Això és molt útil per a persones que, per exemple, no van poder assistir a un esdeveniment grupal que utilitzava Etherpad per a la documentació. Aquestes persones simplement poden gaudir del gust de tota l'acció amb l'ajuda de Time Slider. També pot ser beneficiós per donar orientacions sobre l'escriptura o la guia d'estil de programació a nous editors, programadors o escriptors.

####Importa/Exporta en diversos formats

Podeu importar o exportar tota la documentació o la discussió present al vostre Etherpad en diversos formats com HTML, Word, Text, PDF, Bookmark File o Open Document. D'aquesta manera, podeu mantenir un registre de totes les vostres reunions, codi de col·laboració, articles, editorials o coses aleatòries fora de línia en un arxiu.

## Tutorial

#### Accediu i creeu un nou pad

Creeu un document nou donant un nom de pad. Recordeu que introduïu un nom nou quan creeu un document nou o introduïu el nom d'un document editat anteriorment per accedir-hi.

Podeu utilitzar la vostra pròpia instància o utilitzar alguna instància pública:

* [https://video.etherpad.com/](https://video.etherpad.com/) (utilitzar només amb finalitats d'avaluació)
* [https://etherpad.wikimedia.org/](https://etherpad.wikimedia.org/)
* [https://pad.riseup.net/](https://pad.riseup.net/)

#### Convida altres usuaris

Per convidar altres persones a col·laborar amb vosaltres en aquest pad, podeu fer clic a la icona `</>` per mostrar les opcions per compartir el pad. En aquest cas, copieu l'enllaç per compartir el pad.

Ara podeu enviar aquest enllaç per correu electrònic o un servei de missatgeria instantània convidant altres persones a unir-se a la redacció del document que acabeu de començar.

Per començar a treballar en un pad existent, normalment se us enviarà un enllaç al pad. Això es produirà normalment mitjançant un correu electrònic escrit per la persona que us convida.

Sovint podreu fer clic a l'enllaç que se us envia. Si no, podeu copiar-lo i enganxar-lo a la barra d'adreces del vostre navegador.

#### Configura els colors d'autoria

A cada autor se li assigna automàticament un color quan comença a escriure. Podeu canviar el color i el nom de visualització. Aquest menú també us permet veure qui està treballant actualment en el text. Els colors de l'autoria no s'inclouen a la versió desada del text, però es fan visibles quan s'obre l'entrada per a una posterior edició.

segueix llegint ⟶ [Etherpad Text-Based Tutorial](https://es.scribd.com/document/410147612/etherpad-text-based-tutorial#fullscreen&from_embed)

#### Connectors

segueix llegint ⟶ [Etherpad plugins](https://static.etherpad.org/index.html)

## Activitats

**1.-** Conectat a [https://video.etherpad.com/p/smx/etherpad](https://video.etherpad.com/p/smx/etherpad) i fes un comentari amb el teu nom respecte els estudis de SMX.

Fes una captura de pantalla quan tothom hagi escrit algun comentari on aparegui el teu nom.

**2.-** Ves a [https://video.etherpad.com](https://video.etherpad.com) i crea un pad nou amb un nom difícil d’adivinar, per exemple proven-xerrada-8756, i la comparteixes amb els teus companys per fer una sessió interactiva de treball.

Heu de crear un document del tema que vulgueu ( i fer servir el chat interactiu)  sense parlar entre vosaltres.

Fes les captures de pantalla que creguis convenient.

**3.-** Crea un nou pad amb el teu nom i escriu un petit text del tema que vulguis utilitzant totes les capacitats d’Etherpad (és un editor molt senzill)

Fes una captura de pantalla

**4.-** Accedeix al Etherpad d’un company i crear un pad  amb el teu nom en la seva instància.

**5.-** Etherpad es pot adaptar a les teves necessitats utilitzant plugins; la llista disponible la pots trobar a [plugins site](https://www.google.com/url?q=https://static.etherpad.org&sa=D&source=editors&ust=1715713027893960&usg=AOvVaw3l8HODjX7mbWA86iy9W3gA).

Pots instal.lar plugins desde la interfície d’aministració web en el path `/admin/plugins`.

{% image "plugins.png" %}

L’usuari és `admin` i la contrasenya es el valor de la variable `ETHERPAD_ADMIN_PASSWORD` que trobràs al fitxer `.env`.  

Busca aquells que et semblin més interessants.

## Referències

* [Etherpad Tutorial](https://etherpad101.weebly.com/)
* [Etherpad Little - Wiki](https://github.com/ether/etherpad-lite/wiki)
* [Teaching S.M.A.R.T. With Web Resources - Etherpad](https://www.youtube.com/watch?time_continue=246&v=lU5RaW_Yu7U&feature=emb_logo)
* [Collaborative work in the classroom with etherpad](https://fablearn.stanford.edu/fellows/archive/fellows/blog/collaborative-work-classroom-etherpad.html)

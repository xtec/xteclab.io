---
title: Cryptpad
description: "CryptPad és una suite de col·laboració encriptada i de codi obert d'extrem a extrem."
---

## Introducció

### Instal·lar

Arrenca una màquina virtual amb [WSL](/windows/wsl/).

Clona el projecte <https://gitlab.com/xtec/web/cryptpad>:

```sh
$ git clone https://gitlab.com/xtec/web/cryptpad
```

Canvia a la carpeta `cryptpad`:

```sh
$ cd cryptpad
```

Arrenca l'aplicació cryptpad:

```sh
$ docker compose up -d
```

Obre el navegador Chrome (de vegades Firefox no funciona bé) 

<http://localhost:3000>

Connecta't al cryptpad :

{% image "cryptpad.png" %}


### Instàncies

CryptPad és de codi obert perquè qualsevol pugui instal·lar i oferir el servei. 

Aquesta pàgina enumera les instàncies obertes al públic [CryptPad Instances](https://cryptpad.org/instances/)

Per al nostre treball col·laboratiu utilitzarem CryptPad.fr allotjat a França i administrat per l'equip de desenvolupament de CryptPad.

Obre aquest Cryptpad públic: [CryptPad.fr](https://cryptpad.fr/)

## Aplicacions

### General

* [General](https://docs.cryptpad.org/en/user_guide/apps/general.html)

#### Activitats

1.- Crea un document sense propietari amb data de finalització i contrasenya

2.- Escriu un text petit en el document utilitzant les capacitats de l’editor.

3.- Crea un snapshot del document.

4.- Segueix editant el document.

5.- Restaura una versió anterior del document.

### Rich Text

* [Rich Text](https://docs.cryptpad.org/en/user_guide/apps/richtext.html)

#### Activitats

1.- Exporta un document a PDF

2.- Comenta una part d’un text

### Spreadsheet

* [Spreadsheet](https://docs.cryptpad.org/en/user_guide/apps/sheets.html)

#### Activitats

1.- Crea una fulla de càlcul i l’imprimeixes.

### Code / Markdown

* [Code / Markdown](https://docs.cryptpad.org/en/user_guide/apps/code.html)

#### Activitats

1.- Escriu una capçalera i un paràgraf utilitzant markdown

### Slides

* [Slides](https://docs.cryptpad.org/en/user_guide/apps/slides.html)

#### Activitats

1.- Crea una mini-presentació

### Forms

* [Forms](https://docs.cryptpad.org/en/user_guide/apps/form.html)

* [How To Create Online Forms with CryptPad, an Alternative to Google Forms](https://www.ubuntubuzz.com/2022/01/how-to-create-online-forms-with-cryptpad-an-alternative-to-google-forms.html)

#### Activitats

1.- Crea un mini-formulari

### Whiteboard

* [Whiteboard](https://docs.cryptpad.org/en/user_guide/apps/whiteboard.html)

#### Activitats

1.- Crea un mini-dibuix

### Poll

* [Poll](https://docs.cryptpad.org/en/user_guide/apps/poll.html)

#### Activitats

1.- Crea una mini-consulta


## Administració

### Compte d'usuari

CryptPad xifra les dades perquè només siguin llegibles per tu i els teus col·laboradors. Per aquesta raó els administradors del servei no poden veure, recuperar o restablir la vostra contrasenya.

Per tant, és important que anoteu la vostra contrasenya en un lloc segur separat del vostre compte de CryptPad.

CryptPad utilitza la combinació del vostre nom d'usuari i contrasenya per identificar-vos. Els noms d'usuari no són únics a CryptPad. És possible crear diversos comptes amb el mateix nom d'usuari i contrasenyes diferents.

segueix llegint ⟶ [User Account](https://docs.cryptpad.org/en/user_guide/user_account.html)

#### Activitats

1.- Pot haver més d’un usuari amb el mateix nom? Explica perquè.

2.- Si no recordes de la teva contrasenya com la pots recuperar? Pots recuperar els teus documents?

3.- Hi ha dos tipus d’usuaris. Explica quins són i que tenen de diferent.

4.- Registra un nou usuari. 

5.- Es pot canviar el nom de l’usuari un cop s’ha creat? I la contrasenya?

6.- Quan mous un pad del que ets propietari a una carpeta compartida, que passa amb aquest pas.

7.- Hi ha dos tipus de backups. Explica les diferències.

8.- Fes un backup

### Seguretat

Quan comparteixes l'enllaç a un document o carpeta compartida a través d'un canal insegur (per exemple, correu electrònic o SMS), algú pot interceptar l'enllaç i accedir a les teves dades. Per evitar que això passi, el [propietaris](https://docs.cryptpad.org/en/user_guide/share_and_access.html#owners) d'un document o carpeta pot afegir una contrasenya.

Quan comparteixes documents amb els teus contactes i equips directament a CryptPad, les comunicacions es xifren i suposem que vols donar-los accés. Per tant, la contrasenya es recorda i s'envia amb el bloc quan la comparteixes. El destinatari, o tu mateix, ho ésno ho van demanar quan van obrir el document.

segueix llegint ⟶ [Security](https://docs.cryptpad.org/en/user_guide/security.html)

#### Activitats

1.- És segur compartir l’enllaç a un document o carpeta compartida mitjançant correu o SMS?

2.- Crea un document amb contrasenya.

3.- Crea una carpeta amb contrasenya.

### Drive

El CryptDrive s'utilitza per emmagatzemar i gestionar documents. Per als usuaris que han iniciat sessió, és la pàgina de destinació predeterminada a CryptPad. També és accessible des de les altres pàgines:

    Feu clic al logotip a la part superior esquerra.
    Menú d'usuari (avatar a la part superior dreta) >CryptDrive.

segueix llegint ⟶ [Drive](https://docs.cryptpad.org/en/user_guide/drive.html)

#### Activitats

1.- Crea una carpeta i dos subcarpetes

2.- Cambia el color de la carpeta

3.- Crea un document docx-el-teu-nom.

4.- Borra el document.

5.- Recupera el document.

6.- Destrueix el document.

7.- Crea dos documents i els marques amb el tag smx

### Col·laboració / Social

segueix llegint ⟶ [Collaboration](https://docs.cryptpad.org/en/user_guide/collaboration.html)

#### Activitats

1.- Modifica el teu avatar (mostra l’anterior buit o amb l’antic avatar, i el nou)

2.- Afegeix un nou esdeveniment en el calendari: examen smx-8-uf1.

3.- Crea tres documents smx-el-teu-nom-permís en la instància pública i enganxa l’enllaç compartit.
Un ha de ser de només lectura, l’altre ha de permetre editar i l’altre només s’ha de poder veure un sol cop.

4.- Crea una carpeta en la instància pública i enganxa l’enllaç.

### Comparteix / Accés

segueix llegint ⟶ [Share and Access](https://docs.cryptpad.org/en/user_guide/share_and_access.html)


## Servidor

La configuració per defecte només funciona amb `localhost`.

Per tant has d’actualitzar el fitxer `config.js` perquè faci servir la ip privada del teu servidor

```
$ nano config.js
```

{% image "configjs.png" %}












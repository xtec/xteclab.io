---
title: Mailu
description: "Mailu és un servidor de correu senzill"
---

## Entorn de treball

Mailu és un servidor de correu senzill però amb totes les funcions com a conjunt d'imatges de Docker.

Arrenca una màquina virtual amb [WSL](/windows/wsl/).

Clona el projecte `webapp`:

```sh
$ git clone https://gitlab.com/xtec/webapp
```

Canvia a la carpeta `webapp/mailu`:

```sh
$ cd webapp/mailu
```

Arrenca l'aplicació cryptpad:

```sh
$ docker-compose -p mailu up -d
```


Abans de poder utilitzar Mailu, heu de crear el compte d'usuari administrador principal. Això podria ser `admin@smx.com`. 

Utilitzeu l'ordre següent, canviant `password` al teu gust:

```sh
$ docker-compose exec admin flask mailu admin admin smx.com 'password'
```


Obriu el vostre navegador i connecteu-vos a mailu ⟶ [http://localhost](http://localhost).

L'usuari és `admin@smx.com`i la contrasenya és `password` — Ho has posat abans.

### Servidor de demostració

Correu web: [https://test.mailu.io/webmail/](https://test.mailu.io/webmail/)

Interfície d'usuari d'administració: [https://test.mailu.io/admin/](https://test.mailu.io/admin/)

Inici de sessió de l'administrador: admin@test.mailu.io

Contrasenya d'administrador: Letmein

El servidor de demostració només té finalitats de demostració i prova. 

Si us plau, sigueu respectuosos i mantingueu el servidor de demostració funcional perquè altres puguin provar-lo.

segueix llegint ⟶ [https://mailu.io/1.9/demo.html](https://mailu.io/1.9/demo.html)

## Configuració

Mailu es basa en el fitxer `mailu.env` per a diferents configuracions.

Obre mailu.env i reviseu la configuració de la variable. Assegureu-vos de llegir els comentaris del fitxer i les instruccions de la Configuració comuna pàgina.

[Configuració](https://mailu.io/1.9/configuration.html)

[Interfície d'administració web](https://mailu.io/1.9/webadministration.html)



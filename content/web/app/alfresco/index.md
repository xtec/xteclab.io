---
title: Alfresco
description: Alfresco és un gestor de continguts.
---

## Introducció

[Alfresco](https://www.hyland.com/es/products/alfresco-platform) és un gestor de continguts …

Nosaltres farem servir [Alfresco Community Edition](https://docs.alfresco.com/content-services/community/):

## Instal.lació

Obre un terminal de {% link "/windows/powershell/" %}:

Inicia una màquina Ubuntu a {% link "/cloud/isard/" %}:

```pwsh
connect-isard alfresco -new
```

**Important!** Alfresco necessita almenys 13GB de RAM per funcionar i 6 CPUs!

Instal.la {% link "/linux/docker/" %}:

```sh
curl -L sh.xtec.dev/docker.sh | sh
su - ${USER}
```

Clona el projecte <https://gitlab.com/xtec/web/alfresco>:

```sh
git clone https://gitlab.com/xtec/web/alfresco
```

Canvia a la carpeta `alfresco`:

```sh
cd alfresco
```

Arrenca l'aplicació alfresco:

```sh
docker compose up -d
```

Obre el navegador per accedir a Alfresco: <http://localhost:8080/share/>

Pots accedir a Alfresco share amb l’usuari `admin` i contrasenya `admin`.

**Atenció!** El primer cop tarda bastant en arrencar perquè s’han d’inicialitzar molts serveis. Per veure com va la cosa mira el últims logs (has d’executar la comanda desde la carpeta on està el fitxer `docker-compose.yml`):

```sh
$ docker-compose logs | tail -n 20
```

**Important!**. De moment només es pot fer servir durant dos dies si vols modificar contingut (solució ràpida, però quan hi hagi temps …)

A partir d’ara alfresco s’iniciarà de manera automàtica al arrencar la màquina.

## Share

[Alfresco Share](https://docs.alfresco.com/content-services/community/using/share/) és una aplicació web que proporciona una interfaz gràfica a l’usuari perquè pugui treballar amb un navegador web.

Per accedir a ella fem servir el path `/share`.

Tens un conjunt de [video tutorials](https://docs.alfresco.com/content-services/latest/tutorial/video/) relacionats amb Alfresco Share, i un [tour of the available features](https://docs.alfresco.com/content-services/latest/tutorial/video/#tour-of-alfresco-share) per si vols saber més coses al respecte.

### Perfil i taulers

[Profiles and dashboards](https://docs.alfresco.com/content-services/community/using/dashboard/)

Pots personalitzar el teu perfil d’usuari i el teu tauler com vulguis sense afectar ningú més: ets l’única persona que veu el teu tauler.

Alguns detalls del teu perfil d’usuari són visibles pels teus companys, per això es bona idea que aquests detalls estiguin actualitzats.

El **dashboard** d’usuari és la teva pissarra de treball que et dóna una informació actualitzada de tot el que està passant a Alfresco (Sites, Tasques, etc) que és del teu inteŕes (i has configurat).

Cada **dashlet** et permet seguir una activitat concreta (Llocs, Tasques, etc)

**1.-** Modifica el dashboard perquè inclogui aquells dashlets que creguis més importants dashlets (només s’han de mostrar a la dreta)

{% sol %}{% image "share/dashboard.png" %}{% endsol %}

**2.-** Modifica el Layout perquè només sigui una columna

{% sol %}{% image "share/layout.png" %}{% endsol %}

**3.-** Modifica el dashlet “Web View” perquè mostri un lloc web de wikipedia (que no sigui aquest)

{% sol %}{% image "share/web-view.png" %}{% endsol %}

### Usuaris i grups

[Manage users and groups](https://docs.alfresco.com/content-services/community/admin/users-groups/)

Abans de poder configurar qualsevol mesura de seguretat, necessites usuaris i grups per treballar. Utilitza aquesta informació per administrar els vostres usuaris i grups a Community Edition.

**1.-** Modifica el teu perfil d’usuari

{% sol %}{% image "share/user-profile.png" %}{% endsol %}

**2.-** Modifica la teva contrasenya

**3.-** Crea un nou usuari alumne-2

{% sol %}{% image "share/new-user.png" %}{% endsol %}

**4.-** Segueix el nou usuari

{% sol %}{% image "share/follow-user.png" %}{% endsol %}

## Site

[Overview of sites](https://docs.alfresco.com/content-services/community/using/sites/). 

Un site és un lloc on pots compartir contingut i col·laborar amb altres membres del lloc.

**1.-** Crea un site private amb el nom alumne i que tingui una descripció coherent.

{% sol %}{% image "site/create-site.png" %}{% endsol %}
 
**2.-** Afegeix als usuaris alumne-1 com a Collaborator i l’alumne-2 com a Manager.

{% sol %}{% image "site/add-users.png" %}{% endsol %}


**3.-**  Qui és el Manager del site alumne?

{% sol %}
david i david-2
{% endsol %}

**4.-** Afegeix un dashlet Site Notice al dashboard del Site amb una entrada.

{% sol %}{% image "site/site-notice.png" %}{% endsol %}

### Contingut

[Content overview](https://docs.alfresco.com/content-services/community/using/content/). 

La llibreria de documents d’un site és on guardes i gestiones el contingut, com documents, imatges i videos.

**1.-** Crea un contingut fora de la Document Library amb el nom alumne-lluna

{% sol %}{% image "site/content.png" %}{% endsol %}

**2.-** Crea un document Plain Text amb el nom alumne-ter i contingut sobre el riu Ter

{% sol %}{% image "site/plain-text.png" %}{% endsol %}

**3.-** Crea un document HTML amb el nom alumne-llobregat i contingut sobre el riu Llobregat amb una fotografia del riu Llobregat.

{% sol %}{% image "site/html.png" %}{% endsol %}

**4.-** Puja una imatge del riu ebre amb nom alumne-ebre

{% sol %}{% image "site/image.png" %}{% endsol %}

**5.-** Afegeix un nou tag “Riu” i etiqueta el contingut que has creat.

{% sol %}{% image "site/tag.png" %}{% endsol %}

**6.-** Afegeix un comentari adient a la imatge del riu Ebre.

{% sol %}{% image "site/comment.png" %}{% endsol %}

**7.-** Modifica la imatge del riu ebre quan passa per Miravet

{% sol %}{% image "site/image-update.png" %}{% endsol %}

**8.-** Recupera la versió anterior de la imatge del riu Ebre.

{% sol %}{% image "site/image-revert.png" %}{% endsol %}

**9.-** Borra tot el contingut i torna a recuperar només els documents

{% sol %}{% image "site/trash.png" %}{% endsol %}

### Fitxers i carpetes

També pots tenir contingut fora de la Document Library a My Files, Shared Files o Repository

**1.-** Crea una carpeta alumne-riu amb el tag riu, i mou tot el contingut de rius a la nova carpeta

{% sol %}{% image "site/folder.png" %}{% endsol %}

**2.-** Descarrega tot el contingut de la carpeta alumne-riu

{% sol %}{% image "site/folder-download.png" %}{% endsol %}

### Usuaris i permisos

[User roles and permissions](https://docs.alfresco.com/content-services/community/using/permissions/). 

El rol d'un usuari determina què pot fer i què no pot fer en un lloc. Cada rol té un conjunt de permisos per defecte.

## Search

[Search](https://docs.alfresco.com/content-services/community/using/search/). Pots utilitzar el quadre de cerca de la barra d'eines per cercar fitxers, llocs i persones

{% image "search/search.png" %}

## Workflow

[Tasks and workflows](https://docs.alfresco.com/content-services/community/using/tasks/)

Les tasques i els fluxos de treball t’ajuden a fer un seguiment de les coses que heu de fer vosaltres i els altres usuaris. Pots crear una tasca o un flux de treball autònoms, o pots adjuntar-hi un fitxer.

**1.-** Crea una nova tasca urgent que l’alumne-2 ha d’acabar el proper dia: Has de crear un document del riu Segre

{% sol %}{% image "workflow/new-task.png" %}{% endsol %}

**2.-** Fes que l’alumne-2 completi la tasca

{% sol %}{% image "workflow/task-completed.png" %}{% endsol %}

**3.-** Crea una nova tasca urgent que l’alumne-2 ha d’acabar el proper dia: Has de crear un document del riu Fluvià. L’alumne-2 l’ha de reassignar a l’alumne-1.

{% sol %}{% image "workflow/new-task-urgent.png" %}{% endsol %}

**4.-** Fes que l’alumne-1 complet la tasca amb un comentari de que no és necessari crear el document:

{% sol %}{% image "workflow/task-comment.png" %}{% endsol %}

**5.-** Fes que alumne canceli el workflow ”Crea un document del riu Fluvià”

{% sol %}{% image "workflow/task-cancel.png" %}{% endsol %}

### Smart folders

[Smart Folders](https://docs.alfresco.com/content-services/community/using/smart-folders/). Una carpeta intel·ligent és una manera d'agrupar fitxers de diferents ubicacions d'Alfresco Share en una única carpeta, de manera que puguis trobar fitxers similars ràpidament.

**1.-** Crea un site amb contingut d’animals (almenys 5 pàgines). Pots copiar el contingut de Wikipedia.

**2.-** Explica, i mostra, com funcionen els diferents operadors de búsqueda.

## Admin

Alfresco consisteix en una sèrie de mòduls que cal desplegar i configurar conjuntament per formar la plataforma de serveis completa. Això ofereix als clients la flexibilitat per configurar la seva arquitectura de desplegament d'una manera coherent amb la seva estratègia de TI.

El desplegament d'Alfresco requereix que hi hagi una sèrie de components d'infraestructura en els quals s'instal·lin els components bàsics de la plataforma, i que s'ampliïn amb diversos components opcionals que proporcionen serveis addicionals.

El diagrama següent mostra els components clau d'una instal·lació típica de Content Services:

{% image "admin/components.png" %}

### Docker Compose

La manera més adequada i ràpida per desplegar les instàncies de desenvolupament i prova és mitjançant la composició de contenidors ({% link "/linux/docker/compose/" %}) que es descriu en el fitxer `docker-compose.yml`.

Important!. Necessites una màquina amb almenys 13 GB de memòria per distribuir entre els contenidors de Docker.

El fitxer que fem servir desplega la versió gratuïta Community i consisteix en aquest sistema:

{% image "admin/docker-compose.png" %}

Utilitzeu aquesta informació per verificar que el sistema s'ha iniciat correctament i per netejar el desplegament.

**1.-** Obre una nova finestra de terminal i canvieu el directori a la carpeta docker-compose si no heu iniciat els contenidors en segon pla.

```sh
$ docker-compose ps
```

Hauríeu de veure una llista dels serveis definits al fitxer docker-compose.yaml.

**3.-** Visualitza els fitxers de registre de cada servei `<nom-servei>`, o contenidor `<nom-contenidor>`:

```sh
$ docker-compose logs <service-name>
$ docker container logs <container-name>
```

Per exemple, per comprovar els registres per compartir, executeu qualsevol de les ordres següents:

```sh
$ docker-compose logs share
$ docker container logs docker-compose_share_1
```

Podeu afegir un paràmetre opcional `--tail=25` abans de `<container-name>` per mostrar les últimes 25 línies dels registres del contenidor seleccionat.

```sh
$ docker-compose logs --tail=25 share
$ docker container logs --tail=25 docker-compose_share_1
```

Comproveu si hi ha un missatge d'èxit:

```sh
Successfully retrieved license information from Alfresco.
```

Consulteu [Docker Compose a GitHub](https://github.com/Alfresco/acs-deployment/blob/master/docs/docker-compose/README.md) per obtenir informació tècnica addicional. 

###  Activitat

**1.-** Obre el teu navegador i mira que hi ha en cada endpoint: 

{% sol %}
| Service | Endpoint |
|-|-|
| Administration and REST APIs | http://localhost:8080/alfresco |
| Share | http://localhost:8080/share |
| Alfresco Content App | http://localhost:8080/content-app |
| Search Services administration | http://localhost:8083/solr |
{% endsol %}

**2.-** Explica que fa cada contenidor (servei)

```sh
$ docker-compose ps
```

{% sol %}
activemq: broker de missatges
alfresco: gestor de contingut
postgres: base de dades
share: interfície web s’usuari
solr6: servei de búsqueda
transform_core_aio: transformar documents
proxy: http proxy de share i content services
{% endsol %}

**3.-** Explica quina relació hi ha entre els diferents serveis (contenidors) tal com s’indica en aquest diagrama:

{% image "admin/service-diagram.png" %}

**4.-** Entra dins de cada contenidor i comprova quin contenidor executa java i quina versió utilitza.

{% sol %}
```sh
$ docker exec -it alfresco_... /bin/bash
$ java -version
$ exit
```
{% endsol %}

**5.-** Alfresco necessita molta memòria. Això és degut a que moltes aplicacions estan escrites en Java. Podries explicar perquè passa això?

### Administrar

[Admin](https://docs.alfresco.com/content-services/community/admin/). Hi ha una sèrie de processos i procediments per mantenir i administrar un entorn de producció de Community Edition.

[Back up and restore](https://docs.alfresco.com/content-services/community/admin/backup-restore/). Aquesta informació només descriu el procés per fer una còpia de seguretat del dipòsit de contingut.

**1.-** Com administrador haig de fer còpies de seguretat. De qué haig de fer còpia de seguretat, i en quins contenidors està el que has de copiar?

{% sol %}
Dades (PostgreSQL), Fitxers (Alfresco Content Services), Documents (Alfresco Search Services)
{% endsol %}
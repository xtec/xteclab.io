---
title: REST
description: 
---

## REST

**REST (Representational State Transfer)** és una forma força habitual d'interacció entre aplicacions i serveis de client a Internet. És un conjunt de restriccions que es tenen en compte a l'hora de crear aplicacions i serveis web.

Aquest estil arquitectònic es va desenvolupar per ajudar a crear fàcilment serveis web convenients. Un servei escrit pel que fa a les regles REST s'anomena RESTful .

**REST NO és un protocol ni un estàndard**

REST normalment funciona a sobre d'HTTP i és una de les maneres possibles d'utilitzar HTTP. No és una norma, sinó un conjunt de recomanacions útils.

### Breu història

L'any 2000, Roy Fielding va publicar la seva tesi doctoral sobre "Estils arquitectònics i disseny d'arquitectura de xarxa" . En aquest article, proposa la idea que si un sistema compleix certes sis condicions, es fa més còmode d'utilitzar. Va anomenar aquest concepte REST.

Amb aquesta idea, Roy es va convertir en un revolucionari per a la seva època. Anem a veure aquestes condicions i veure de què es tracta.

### Sis principis REST

El compliment dels sis principis importants següents garanteix que escriu un servei RESTful:

1. **Model d'interacció client-servidor**. En separar la interfície d'usuari del servidor del magatzem de dades, millorem i simplifiquem les operacions de l'aplicació.

2. **Sense estat**. Cada sol·licitud d'un client a un servidor ha de contenir tota la informació necessària i no pot dependre de cap estat emmagatzemat al costat del servidor. Segons REST, el servei no emmagatzema els resultats de l'operació anterior. En poques paraules, funciona segons el concepte "Preguntat, contestat, oblidat" .

3. **Cacheable**. Una parella sol·licitud-resposta es pot marcar com a memòria cau i emmagatzemada al costat de l'usuari. És possible fer una analogia amb l'emmagatzematge en memòria cau de pàgines web: si una pàgina es va descarregar una vegada, es podria accedir sense tornar a adreçar-se al servidor.

4. **Interfície uniforme**. L'arquitectura REST especifica que qualsevol servei REST ha de ser comprensible sense el seu desenvolupador.

5. **Sistema en capes**. Un client no pot dir si està connectat directament al servidor final o a un intermediari al llarg del camí.

6. **Codi sota demanda** (Opcional) . A petició, el servei ha de proporcionar codi executable en forma d'applet o script per executar-lo des del client. A la pràctica, s'utilitza molt poques vegades.

### Mètodes HTTP per a serveis RESTful

En el concepte REST, la interacció amb els recursos es realitza cridant l'URL del recurs i quatre mètodes HTTP bàsics: `GET`, `POST`, `PUT`, `DELETE`. 

Aquest model és molt semblant al mòdel CRUD de les bases de dades: CREATE, READ, UPDATE i DELETE.

| HTTP | BASE DE DADES |
|-|-|
| GET | READ |
| POST | CREATE  |
| PUT | UPDATE |
| DELETE | DELETE |

Considerem amb detall en quines situacions s'utilitzen.

* `GET` s'utilitza per recuperar o llegir el recurs. Si el mètode `GET` té èxit i no conté errors d'adreça, retorna una representació XML o JSON del recurs en combinació amb un codi d’estat `HTTP 200 (OK)` . En cas d'error, normalment es retorna el codi d'estat el codi `404 (NO TROBAT)` o `400 (DEMANDA DOBLE)`.

* `POST` s'utilitza per crear nous recursos. Si el recurs es crea correctament, es retorna el codi `HTTP 201` (creat) i també es passa l'adreça del recurs creat a la capçalera `Location`.

* El mètode `PUT` crea un recurs amb l'ID especificat o n'actualitza un d'existent. Si l'actualització té èxit, el codi és `200 (OK)` ; Es retorna `204 (SENSE CONTINGUT)` si no s'ha transmès cap contingut al cos de la resposta.

* El mètode `DELETE` s'utilitza per eliminar un recurs identificat per un URI (ID) específic. Si l'eliminació té èxit, es retorna el codi `HTTP 200 (OK)` juntament amb el cos de la resposta que conté les dades del recurs remot. També és possible utilitzar el codi `HTTP 204` (SENSE CONTINGUT) sense el cos de la resposta.

## Exemple

Suposem que tenim un servei web de llibres hipotètic que utilitza els mètodes descrits per gestionar els llibres com a recurs. Cada llibre emmagatzemat al servei té el seu propi identificador numèric. 

Llavors:

| | |
|-|-|
| POST | https://example.com/books/  | afegeix un llibre nou |
| GET | https://example.com/books/1 | obté un llibre existent |
| PUT  | https://example.com/books/1 | crea o actualitza un llibre amb l'identificador donat |
| DELETE | https://example.com/books/1 | elimina un llibre existent per l'identificador donat |
	
Així és el treball amb un servei web RESTful típic.

## Convencions de denominació de l'API

Utilizar un nom apropiat per un recurs fa que una API sigui intuitiva i fàcil d'utilitzar,

**1.-** Utilitza **noms** per representar recursos enlloc de verbs, i evita combinacions verb-nom.

✅ Correcte 

```
http://api.example.com/v1/store/items/{item-id} 
http://api.example.com/v1/store/employees/{emp-id}
http://api.example.com/v1/store/prices/{price-id}
http://api.example.com/v1/store/orders/{order-id} 
```

❌ Incorrecte

```
http://api.example.com/v1/store/CreateItems/{item-id} 
http://api.example.com/v1/store/getEmployees/{emp-id} 
http://api.example.com/v1/store/update-prices/{price-id}
http://api.example.com/v1/store/deleteOrders/{order-id}
```

Les URIs no tenen que indicar cap operació CRUD (Create, Read, Update, Delete). 

Exemple:

| | | |
|-|-|-|
| GET | example.com/employees/8346 | Read employee with employee id 8345 |
| POST | example.com/employees | Create an employee |
| PUT | example.com/employees/8345 | Update employee with employee id 8345 |
| DELETE | example.com/employees/8345 | Delete employee with employee id 8345 |

**2.-** Utilitza **noms en plural** pels recursos a menys que siguin recursos únics (singleton).

✅ Correcte:

```
http://api.example.com/v1/store/items/{item-id}
http://api.example.com/v1/store/employees/{emp-id}/address
```

❌ Incorrecte

```
http://api.example.com/v1/store/item/{item-id}
http://api.example.com/v1/store/employee/{emp-id}/address
```

**3.-** Utilita **guions (-)** per tal que sigui més fàcil llegir una URI.

✅ Correcte:

```
http://api.example.com/v1/store/vendor-management/{vendor-id}
http://api.example.com/v1/store/item-management/{item-id}/product-typ
http://api.example.com/v1/store/inventory-management
``

❌ Incorrecte:

```
http://api.example.com/v1/store/vendormanagement/{vendor-id}
http://api.example.com/v1/store/itemmanagement/{item-id}/producttype
http://api.example.com/v1/store/inventory_management
```

**4.-** Utlitza la barra inclinada `/` per mostrar una jerarquia, i  mai posis una `/` al final de la URI

✅ Correcte:

```
http://api.example.com/v1/store/items
``

❌ Incorrecte:

```
http://api.example.com/v1/store/items/
```

**5.-** Evita utilitzar extensions de fitxer per indicar el contingut de la resposta.

No són necessàries i fan que la URI sigui més llarga i complexa.

✅ Correcte:

```
http://api.example.com/v1/store/items
http://api.example.com/v1/store/products
```
❌ Incorrecte:

```
http://api.example.com/v1/store/items.json
http://api.example.com/v1/store/products.xml
```

**6.-** Versiona la teva API

Intenta sempre versionar la teva API perquè d'aquesta manera pots modificar, eliminar o afegir nous endpoints sense afectar les aplicacions client que utilitzen les verisons anteriors.

Per exemeple, a la versió `v1` de la nostra API tenima aquests endpoints:
```
http://api.example.com/v1/store/employees/{emp-id}
``

A la versió `v2` pots afegir aquests nous endpoints:

```
http://api.example.com/v1/store/items/{item-id}
http://api.example.com/v2/store/employees/{emp-id}/address
```

**7.** Utilitza components de consulta en URIs que representen una col.lecció de recursos per filtrar, ordenar o limitar els recursos que torna la consulta. 

```
http://api.example.com/v1/store/items?group=124
http://api.example.com/v1/store/employees?department=IT&region=USA
```

## TODO

* [https://restfulapi.net/resource-naming/](https://restfulapi.net/resource-naming/)

* [The Ultimate Guide to REST API Naming Convention](https://www.moesif.com/blog/technical/api-development/The-Ultimate-Guide-to-REST-API-Naming-Convention/)

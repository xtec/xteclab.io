---
title: HTTP
---

HTTP per interactuar amb les aplicacions client (com pot ser el navegador web).

Aquest protocol és flexible, i s'assembla a cridar funcions remotes. Aquestes "funcions" es crident mitjançant una línea en la capçalera del missatge que envia el client, i que s'anomena línia de solicitud.

GET /hello/david HTTP/1.1

La línia de sol·licitud comença especificant el mètode que es vol utilitzar, seguit del identificador del recurs que es solicita (una paǵina HTML, una imatge, etc.) i la versió del protocol.

<https://fasterthanli.me/articles/the-http-crash-course-nobody-asked-for>
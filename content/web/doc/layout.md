---
title: Layout
description: Un document s'organitza amb contenidors, files i columnes.
---

## Introducció

Per organitzar els diferents elements dins de la finestra utilizem l'element `<div>`.

## Flexbox

El contenidor és l'element bàsic de disseny, és obligatori per utilitzar el sistema de graella, i afegeix un marge a dreta i esquerra que varia en funció de l'amplada de la finestra.

```html
<div class="container">
  Hello, World!
</div>
```

{% panel %}
<div class="container border border-danger">
  Hello, World!
</div>
{% endpanel %}

Un contenidor està forma per un conjunt de files:

```html
<div class="container">
  <div class="row">Let me not to the marriage of true minds</div>
  <div class="row mt-2">Admit impediments; love is not love</div>
</div>
```

{% panel %}
<div class="container">
  <div class="row border border-danger p-2">Let me not to the marriage of true minds</div>
  <div class="row mt-2 border border-danger p-2">Admit impediments; love is not love</div>
</div>
{% endpanel %}

I una fila està formada per un conjunt de columnes:

```html
<div class="container">
  <div class="row">
    <div class="col">☘️</div>
    <div class="col">🐺</div>
  </div>
  <div class="row mt-2">
    <div class="col">🐸</div>
    <div class="col">🐋</div>
    <div class="col">🐦‍🔥</div>
  </div>
</div>
```

{% panel %}
<div class="container text-center">
  <div class="row">
    <div class="col fs-1 border border-danger p-2">☘️</div>
    <div class="col fs-1 border border-danger p-2">🐺</div>
  </div>
  <div class="row mt-2">
    <div class="col fs-1 border border-danger p-2">🐸</div>
    <div class="col fs-1 border border-danger p-2">🐋</div>
    <div class="col fs-1 border border-danger p-2">🐦‍🔥</div>
  </div>
</div>
{% endpanel %}

Pots veure que les columnes es reparteixen de manera equitativa l'espai dins d'una fila, si tu no dius altre cosa.

Una fila té una amplada de **12** unitats.

Per exemple pots dir que la columna on està el llop 🐺 ocupi `8` unitats, i la columna on està el fénix 🐦‍🔥 ocupi `10` unitats:

```html
<div class="container">
  <div class="row">
    <div class="col">☘️</div>
    <div class="col-8">🐺</div>
  </div>
  <div class="row mt-2">
    <div class="col">🐸</div>
    <div class="col">🐋</div>
    <div class="col-9">🐦‍🔥</div>
  </div>
</div>
```

Pots veure com les altres columnes s'adapten a l'espai que els queda disponible:

{% panel %}
<div class="container text-center">
  <div class="row">
    <div class="col fs-1 border border-danger p-2">☘️</div>
    <div class="col-8 fs-1 border border-danger p-2">🐺</div>
  </div>
  <div class="row mt-2">
    <div class="col fs-1 border border-danger p-2">🐸</div>
    <div class="col fs-1 border border-danger p-2">🐋</div>
    <div class="col-9 fs-1 border border-danger p-2">🐦‍🔥</div>
  </div>
</div>
{% endpanel %}

I que passa si totes les columnes volen ocupar 6 espais?

```html
<div class="container">
  <div class="row">
    <div class="col-6">☘️</div>
    <div class="col-6">🐺</div>
  </div>
  <div class="row mt-2">
    <div class="col-6">🐸</div>
    <div class="col-6">🐋</div>
    <div class="col-6">🐦‍🔥</div>
  </div>
</div>
```

Doncs que,

* A la primera fila es poden mostrar les dos columnes a la mateixa altura
* En la segona fila la tercera columna s'ha de mostrar a sota:

{% panel %}
<div class="container text-center">
  <div class="row">
    <div class="col-6 fs-1 border border-danger p-2">☘️</div>
    <div class="col-6 fs-1 border border-danger p-2">🐺</div>
  </div>
  <div class="row mt-2">
    <div class="col-6 fs-1 border border-danger p-2">🐸</div>
    <div class="col-6 fs-1 border border-danger p-2">🐋</div>
    <div class="col-6 fs-1 border border-danger p-2">🐦‍🔥</div>
  </div>
</div>
{% endpanel %}


### Breakpoint

Una document web ha d'adaptar el seu contingut a l'amplada disponbile en que s'està visualitzant.

Un **breakpoint** et permet definir els espais que ha d'ocupar una columna **a partir** d'una amplada de visualització.

```html
<div class="container">
  <div class="row">
    <div class="col-12 col-sm-6 col-lg-3">☘️</div>
    <div class="col-12 col-sm-6 col-lg-3">🐺</div>
  </div>
  <div class="row mt-2">
    <div class="col-12 col-sm-6 col-lg-3">🐸</div>
    <div class="col-12 col-sm-6 col-lg-3">🐋</div>
    <div class="col-12 col-sm-6 col-lg-3">🐦‍🔥</div>
  </div>
</div>
```

En aquest exemple diem que:

* Una columna ocupa 12 espais `col-12`
* Quan l'amplada és major que `sm` la columna ocupa 6 espais: `col-sm-6`.
* Quan l'amplada és major que `md` la columna ocupa 2 espais: `col-sm-3`.  

Modifica l'amplada de la finestra on està el navegador per veure com el document s'adapta a l'amplada disponible:

{% panel %}
<div class="container text-center">
  <div class="row">
    <div class="col-12 col-sm-6 col-lg-3 fs-1 border border-danger p-2">☘️</div>
    <div class="col-12 col-sm-6 col-lg-3 fs-1 border border-danger p-2">🐺</div>
  </div>
  <div class="row mt-2">
    <div class="col-12 col-sm-6 col-lg-3 fs-1 border border-danger p-2">🐸</div>
    <div class="col-12 col-sm-6 col-lg-3 fs-1 border border-danger p-2">🐋</div>
    <div class="col-12 col-sm-6 col-lg-3 fs-1 border border-danger p-2">🐦‍🔥</div>
  </div>
</div>
{% endpanel %}

A continuació tens els "class prefix" disponibles i quan comencen a aplicar-se:


| | xs | sm | md | lg | xl | xxl |
|-|-|-|-|-|-|-|
| |  | ≥ 576 px | ≥ 768 px | ≥ 992 px | ≥ 1200 px | ≥ 1400 px |
| **Class prefix** | `.col-` | `.col-sm-` | `.col-md-` | `.col-lg-` | `.col-xl-` | `.col-xxl-` |


### Amplada flexible

Enlloc de dir que una columna ha d'ocupar un número concret d'espais, o deixar que es reparteixi l'espai disponible amb les altres columnes de la fila de manera equitativa, pots definir que la columna s'ha d'adaptar al seu contingut:

```html
<div class="container">
  <div class="row">
    <div class="col">☘️</div>
    <div class="col-auto">🐺</div>
  </div>
  <div class="row mt-2">
    <div class="col">🐸</div>
    <div class="col-auto">🐋</div>
    <div class="col">🐦‍🔥</div>
  </div>
</div>
```

Pots veure que la columna del llop 🐺 i la de la ballena 🐋 s'ajusten al seu contingut i deixen el reste de l'espai per a les altres columnes:

{% panel %}
<div class="container text-center">
  <div class="row">
    <div class="col fs-1 border border-danger p-2">☘️</div>
    <div class="col-auto fs-1 border border-danger p-2">🐺</div>
  </div>
  <div class="row mt-2">
    <div class="col fs-1 border border-danger p-2">🐸</div>
    <div class="col-auto fs-1 border border-danger p-2">🐋</div>
    <div class="col fs-1 border border-danger p-2">🐦‍🔥</div>
  </div>
</div>
{% endpanel %}

<https://getbootstrap.com/docs/5.3/layout/grid/>

## Alineació

Utilitzeu les utilitats d'alineació de flexbox per alinear columnes verticalment i horitzontalment.


#### Alineament horitzontal

... TODO

{% panel %}
<div class="container">
  <div class="row justify-content-start m-1 border border-2">
    <div class="col-4 border border-2 border-primary">justify-content-start</div>
    <div class="col-4 border border-2 border-primary">justify-content-start</div>
  </div>
  <div class="row justify-content-center m-1 border border-2">
    <div class="col-4 border border-2 border-primary">justify-content-center</div>
    <div class="col-4 border border-2 border-primary">justify-content-center</div>
  </div>
  <div class="row justify-content-end m-1 border border-2">
    <div class="col-4 border border-2 border-primary">justify-content-end</div>
    <div class="col-4 border border-2 border-primary">justify-content-end</div>
  </div>
  <div class="row justify-content-around m-1 border border-2">
    <div class="col-4 border border-2 border-primary">justify-content-around</div>
    <div class="col-4 border border-2 border-primary">justify-content-around</div>
  </div>
  <div class="row justify-content-evenly m-1 border border-2">
    <div class="col-4 border border-2 border-primary">justify-content-evenly</div>
    <div class="col-4 border border-2 border-primary">justify-content-evenly</div>
  </div>
  <div class="row justify-content-between m-1 border border-2">
    <div class="col-4 border border-2 border-primary">justify-content-between</div>
    <div class="col-4 border border-2 border-primary">justify-content-between</div>
  </div>
</div>
{% endpanel %}

{% sol %}
```html
<div class="container">
  <div class="row justify-content-start">
    <div class="col-4">justify-content-start</div>
    <div class="col-4">justify-content-start</div>
  </div>
  <div class="row justify-content-center">
    <div class="col-4">justify-content-center</div>
    <div class="col-4">justify-content-center</div>
  </div>
  <div class="row justify-content-end">
    <div class="col-4">justify-content-end</div>
    <div class="col-4">justify-content-end</div>
  </div>
  <div class="row justify-content-around">
    <div class="col-4">justify-content-around</div>
    <div class="col-4">justify-content-around</div>
  </div>
  <div class="row justify-content-evenly">
    <div class="col-4">justify-content-evenly</div>
    <div class="col-4">justify-content-evenly</div>
  </div>
  <div class="row justify-content-between">
    <div class="col-4">justify-content-between</div>
    <div class="col-4">justify-content-between</div>
  </div>
</div>
```
{% endsol %}

#### Alineament vertical

<https://getbootstrap.com/docs/5.3/layout/columns/>



{% panel %}
<div class="container">
   <div class="row m-1 border border-2 border-danger">
       <div class="col align-self-start m-1 border border-2 border-primary">
           COM QUE HI HA TANTA GRIP, han hagut de clausurar la Universitat. D'ençà d'aquest fet, el meu germà i jo
           vivim a casa, a Palafrugell, amb la família. Som dos estudiants desvagats.
       </div>
       <div class="col align-self-center m-1 border border-2 border-primary">
           El meu germà, que és un gran afeccionat a jugar a futbol
       </div>
       <div class="col align-self-end m-1 border border-2 border-primary">
           -malgrat haver-s'hi ja trencat un braç i una cama-,
       </div>
   </div>
</div>
{% endpanel %}

{% sol %}
```html
<div class="container">
   <div class="row">
       <div class="col align-self-start">
           COM QUE HI HA TANTA GRIP, han hagut de clausurar la Universitat. D'ençà d'aquest fet, el meu germà i jo vivim a casa, a Palafrugell, amb la família. Som dos estudiants desvagats.
       </div>
       <div class="col align-self-center">
           El meu germà, que és un gran afeccionat a jugar a futbol
       </div>
       <div class="col align-self-end">
           -malgrat haver-s'hi ja trencat un braç i una cama-,
       </div>
   </div>
</div>
```
{% endsol %}

{% panel %}
<div class="container">
   <div class="row align-items-start m-1 border border-2 border-danger">
       <div class="col m-1 border border-2 border-primary">
           A l'hora de les postres, a dinar, apareixen a taula una gran plata de crema cremada i un pa de pessic
           deliciós, flonjo, daurat, amb un polsim de sucre ingràvid.
       </div>
       <div class="col m-1 border border-2 border-primary">
           La meva mare em diu:
       </div>
       <div class="col m-1 border border-2 border-primary">
           - Ja saps que avui fas vint-i-un anys?
       </div>
   </div>
   <div class="row align-items-center m-1 border border-2 border-danger">
       <div class="col m-1 border border-2 border-primary">
           A l'hora de les postres, a dinar, apareixen a taula una gran plata de crema cremada i un pa de pessic
           deliciós, flonjo, daurat, amb un polsim de sucre ingràvid.
       </div>
       <div class="col m-1 border border-2 border-primary">
           La meva mare em diu:
       </div>
       <div class="col m-1 border border-2 border-primary">
           - Ja saps que avui fas vint-i-un anys?
       </div>
   </div>
   <div class="row align-items-end m-1 border border-2 border-danger">
       <div class="col m-1 border border-2 border-primary">
           A l'hora de les postres, a dinar, apareixen a taula una gran plata de crema cremada i un pa de pessic
           deliciós, flonjo, daurat, amb un polsim de sucre ingràvid.
       </div>
       <div class="col m-1 border border-2 border-primary">
           La meva mare em diu:
       </div>
       <div class="col m-1 border border-2 border-primary">
           - Ja saps que avui fas vint-i-un anys?
       </div>
   </div>
</div>
{% endpanel %}


{% sol %}
```html
<div class="container">
   <div class="row align-items-start">
       <div class="col">
           A l'hora de les postres, a dinar, apareixen a taula una gran plata de crema cremada i un pa de pessic
           deliciós, flonjo, daurat, amb un polsim de sucre ingràvid.
       </div>
       <div class="col">
           La meva mare em diu:
       </div>
       <div class="col">
           - Ja saps que avui fas vint-i-un anys?
       </div>
   </div>
   <div class="row align-items-center">
       <div class="col">
           A l'hora de les postres, a dinar, apareixen a taula una gran plata de crema cremada i un pa de pessic
           deliciós, flonjo, daurat, amb un polsim de sucre ingràvid.
       </div>
       <div class="col">
           La meva mare em diu:
       </div>
       <div class="col">
           - Ja saps que avui fas vint-i-un anys?
       </div>
   </div>
   <div class="row align-items-end">
       <div class="col">
           A l'hora de les postres, a dinar, apareixen a taula una gran plata de crema cremada i un pa de pessic
           deliciós, flonjo, daurat, amb un polsim de sucre ingràvid.
       </div>
       <div class="col">
           La meva mare em diu:
       </div>
       <div class="col">
           - Ja saps que avui fas vint-i-un anys?
       </div>
   </div>
</div>
```
{% endsol %}

{% panel %}
<div class="container">
   <div class="row justify-content-evenly m-1 border border-2 border-danger rounded">
       <div class="col-3 align-self-end m-1 border border-2 border-primary rounded">A cada bugada perdem un llençol</div>
       <div class="col-6 m-1 border border-2 border-primary rounded">Ens parla de l'empitjorament, no pas sobtat, sinó progressiu, que va passant de mica en mica, d'una manera que sovint pot arribar a ser imperceptible.</div>
   </div>

   <div class="row justify-content-between m-1 border border-2 border-danger rounded">
       <div class="col-3 align-self-center m-1 border border-2 border-primary rounded">Qui no té un all té una ceba </div>
       <div class="col-6 m-1 border border-2 border-primary rounded">Un altre refrany "molt català", que expressa una manera d'ésser moderada. Nosaltres no som gens taxatius. La perfecció no existeix i per això podem trobar pèls a tot arreu. Tothom té un defecte o altre.</div>
   </div>
   <div class="row justify-content-end m-1 border border-2 border-danger rounded">
       <div class="col-3 align-self-start m-1 border border-2 border-primary rounded">Qui dia passa, any empeny</div>
       <div class="col-6 m-1 border border-2 border-primary rounded">La gràcia d'aquest refrany és que cadascú l'aplica com vol. Si sou dels qui van ajornant les solucions, dels qui es desentenen de les dificultats quotidianes, dels qui romancegen, fan el ronsa, gansegen, us encanten... (ja veieu que no cal dir procrastinar), aquest refrany us escau. Si sou dels qui van resolent les coses a mesura que es van presentant, de mica en mica, també fa per vosaltres.</div>
   </div>
</div>
{% endpanel %}

{% sol %}
```html
<div class="container">
   <div class="row justify-content-evenly">
       <div class="col-3 align-self-end">A cada bugada perdem un llençol</div>
       <div class="col-6">Ens parla de l'empitjorament, no pas sobtat, sinó progressiu, que va passant de mica en mica, d'una manera que sovint pot arribar a ser imperceptible.</div>
   </div>

   <div class="row justify-content-between">
       <div class="col-3 align-self-center">Qui no té un all té una ceba </div>
       <div class="col-6">Un altre refrany "molt català", que expressa una manera d'ésser moderada. Nosaltres no som gens taxatius. La perfecció no existeix i per això podem trobar pèls a tot arreu. Tothom té un defecte o altre.</div>
   </div>
   <div class="row justify-content-end">
       <div class="col-3 align-self-start">Qui dia passa, any empeny</div>
       <div class="col-6">La gràcia d'aquest refrany és que cadascú l'aplica com vol. Si sou dels qui van ajornant les solucions, dels qui es desentenen de les dificultats quotidianes, dels qui romancegen, fan el ronsa, gansegen, us encanten... (ja veieu que no cal dir procrastinar), aquest refrany us escau. Si sou dels qui van resolent les coses a mesura que es van presentant, de mica en mica, també fa per vosaltres.</div>
   </div>
</div>
```
{% endsol %}

## Gutter

Els "gutters" són el farciment entre les columnes, que s'utilitzen per espaiar i alinear el contingut de manera sensible al sistema de quadrícula Bootstrap.

<https://getbootstrap.com/docs/5.3/layout/gutters/>

## Avaluació

**1.-** Flexbox

{% panel %}
<div class="container">
  <h1 class="fs-3 text-center">Flexbox</h1>
  <p class="text-center font-monospace text-muted">.container > .row > .col</p>
  <div class="row justify-content-center mb-3 border border-1 border-danger'">
    <div class="col col-6 border border-3 border-primary rounded p-3 m -3 text-center">
      <img class="img-fluid" src="https://animals.sandiegozoo.org/sites/default/files/2016-08/animals_hero_bald_eagle.jpg">
    </div>
  </div>
  <div class="row justify-content-center mb-3 border border-1 border-danger">
    <div class="col border border-3 border-primary rounded p-3 m -3 text-center">FLEX</div>
    <div class="col border border-3 border-primary rounded p-3 m -3 text-center">FLEX</div>
    <div class="col border border-3 border-primary rounded p-3 m -3 text-center">FLEX</div>
  </div>
  <div class="row justify-content-evenly mb-3 border border-1 border-danger">
    <div class="col-3 border border-3 border-primary rounded p-3 m -3 text-center">
      <p>FLEX</p>
      <p>FLEX</p>
      <p>FLEX</p>
      <p>FLEX</p>
    </div>
    <div class="col-3 align-self-center border border-3 border-primary rounded p-3 m -3 text-center">
      <img class="img-fluid" src="https://cdn.britannica.com/92/152292-050-EAF28A45/Bald-eagle.jpg">
    </div>
  </div>
  <div class="row justify-content-around mb-3 border border-1 border-danger">
    <div class="col-3 border border-3 border-primary rounded p-3 m -3 text-center">
      <img class="img-fluid" src="https://www.collinsdictionary.com/images/full/butterfly_119422912_1000.jpg">
    </div>
    <div class="col-3 border border-3 border-primary rounded p-3 m -3 text-center">
      <img class="img-fluid" src="https://www.collinsdictionary.com/images/full/butterfly_119422912_1000.jpg">
    </div>
    <div class="col-3 border border-3 border-primary rounded p-3 m -3 text-center">
      <img class="img-fluid" src="https://www.collinsdictionary.com/images/full/butterfly_119422912_1000.jpg">
    </div>
  </div>
</div>
{% endpanel %}


{% sol %}
```html
<div class="container m-3">
  <h1 class="fs-3 text-center">Flexbox</h1>
  <p class="text-center font-monospace text-muted">.container > .row > .col</p>
  <div class="row justify-content-center">
    <div class="col col-6">
      <img class="img-fluid" src="https://animals.sandiegozoo.org/sites/default/files/2016-08/animals_hero_bald_eagle.jpg">
    </div>
  </div>
  <div class="row justify-content-center">
    <div class="col">FLEX</div>
    <div class="col">FLEX</div>
    <div class="col">FLEX</div>
  </div>
  <div class="row justify-content-evenly">
    <div class="col-3">
      <p>FLEX</p>
      <p>FLEX</p>
      <p>FLEX</p>
      <p>FLEX</p>
    </div>
    <div class="col-3 align-self-center">
      <img class="img-fluid" src="https://cdn.britannica.com/92/152292-050-EAF28A45/Bald-eagle.jpg">
    </div>
  </div>
  <div class="row justify-content-around">
    <div class="col-3">
      <img class="img-fluid" src="https://www.collinsdictionary.com/images/full/butterfly_119422912_1000.jpg">
    </div>
    <div class="col-3">
      <img class="img-fluid" src="https://www.collinsdictionary.com/images/full/butterfly_119422912_1000.jpg">
    </div>
    <div class="col-3">
      <img class="img-fluid" src="https://www.collinsdictionary.com/images/full/butterfly_119422912_1000.jpg">
    </div>
  </div>
</div>
```
{% endsol %}


---
title: Text
icon: webdoc.webp
description: Un document HTML és un document de text "pla" amb marques que indiquen el tipus de contingut, com s'ha de visualitzar, etc. 
---

## Introducció

A continuació tens un document HTML:

```html
<!doctype html>
<html>
  <head>
    <title>Hello</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href=" https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css"/> 
  </head>
  <body>
    <h1>Hello World!</h1>
    <p>Welcome to the world of HTML ...</p>
  </body>
</html>
```

## Document de text

Obre un terminal de {% link "/windows/powershell/" %},

Crea una carpeta `html` amb un fitxer `hello.html`:

```pwsh
new-item html/hello.html
```

Obre el fitxer `hello.html` amb el [Bloc de notes](https://ca.wikipedia.org/wiki/Bloc_de_notes) del Windows:

```pwsh
notepad html/hello.html
```

Copia el contingut del document HTML de l'inici al "notepad":

{% image "hello-notepad.png" %}

Guarda el fitxer i tanca el "notepad".

Els documents HTML es visualitzen amb navegadors.

Obre el document amb un navegador:

```pwsh
invoke-item html/hello.html
```

Pots veure que en el navegador només apareix el **text**!

{% image "hello-browser.png" %}

Tot el marcatge ha desaparegut, no es mostra!

I el text que està entre les marques `<h1>` i `</h1>` apareix més gran i fosc.

Qué és el marcatge?

**Tot el text que està dins dels caràcters `<` i `>` són marques.**

## Editor

Per treballar amb HTML faras servir l'editor {% link "/project/vscode/" %}.

Instal.la {% link "/tool/box/" %} tal com s'explica en el document:

```pwsh
Set-ExecutionPolicy Remote -Scope CurrentUser
Install-Module Box -scope CurrentUser -SkipPublisherCheck
```

Instal.la {% link "/project/vscode/" %}

```pwsh
install-code
```

Obre la carpeta `html` amb VS Code:

```pwsh
code html
```

VS Code té moltes ajudes per escriure HTML tal com s'explica en aquest document: [HTML Programming](https://code.visualstudio.com/docs/languages/html).

{% image "hello-vscode.png" %}

Instal.la l'extensió [Live Preview (Microsoft)](https://marketplace.visualstudio.com/items?itemName=ms-vscode.live-server) que ens permet **visualitzar** el document en el mateix editor amb un navegador integrat.

{% image "vscode-preview-install.png" %}

Un cop has instal.lat l’extensió apareixerà una icona nova a la part superior dreta de l’editor:

{% image "vscode-preview-show-icon.png" %}

Apreta la icona "**Show Preview**" ; apareixerà un nou panell a la dreta.

Cada cop que modifiques el document l'HTML en el panell de l’esquerra, s'actualitza el navegador del panell de la dreta:

{% image "vscode-preview-show.png" %}

## Marcatge

L'HTML té un conjunt de marques predefinides ("tags") que tenen un significat "semàntic" per als navegadors i els buscadors:

1. Un troç de text es marca amb un "tag" `<p>` d'obertura i un "tag" de tancament `</p>`.

2. El tag de tancament `</p>` ha de tenir el mateix nom que el tag d'obertura `<p>`

3. El tag de tancament `</p>` és un tag de tancament perquè comença amb `</` 

3. Entre el "tag" `<p>` d'obertura i un "tag" de tancament `</p>`, poden haver molts més tags.

A continuació tens un exemple:

```html
<!doctype html>
<html>
  <head>
    <title>Barcelona</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet"href=" https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css"> 
  </head>
  <body>
    <h1>Barcelona</h1>
    <p>Barcelona és un ciutat mediterrànea i la capital de catalunya.</p>
    <h2>Sagrada Familia</h2>
    <p>La Sagrada Familia és una basílica dissenyada per Antoni Gaudí ...</p>
  </body>
</html>
```

En el codi anterior:

* La marca d’obertura `<html>` indica que tot el que hi ha a continuació d'aquesta marca fins a la marca de tancament `</html>` és codi HTML.

* La marca d’obertura `<body>` indica que tot el que hi ha a continuació d'aquesta marca fins a la marca de tancament `</body>` s'ha de mostrar a la finestra principal del navegador.

* La marca d’obertura `<h1>` indica que tot el que hi ha a continuació d’aquesta marca fins a la marca de tancament `</h1>`  és una capçalera ("**h**eading"), o títol, de nivell 1.

* La marca d’obertura `<p>` indica que tot el que hi ha a continuació d’aquesta marca fins a la marca de tancament `</p>` és una paràgraf ("**p**aragraph).


### Estil

Un tag pot tenir atributs.

Els atributes són com "paràmetres" del tag.

Un dels atributs més utlitzats és `class`, que permet estilitzar el contingut.

Per exemple, si vull centrar un paràgraf:

```html
<p class="text-center">Estic al mitg</p>
```

La majoria dels atributs:

* Només es poden utilitzar en determinats elements.

* La majoria dels valors d'atributs estan predefinits o segueixen un format estipulat.

<p class="text-center fs-4">😊</p>

<p class="text-center fw-bold">L'atribut <code>class</code> és l'excepció més important al que acabem de dir.</p>

### Body, Head & Title

```html
<!doctype html>
<html>
  <head>
    <title>Aquest és el titol de la pàgina</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet"href=" https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css">
  </head>
  <body>
    <h1>Això és el cos de la pàgina</h1>
    <p>Tot el que estigui dins del cos de la pàgina web és el que es mostra al navegador.</p>
</body>
</html>
```


* *En teoria* només el que està dins l’element `<body>` es mostra dins de la finestra principal del navegador.

* Abans de l’element `<body>` és habitual que vagi l’element `<head>`. L’element `<head>` té diferents elements que no es renderitzen al navegador com meta-informació, enllaços a fulls d’estils o scripts, etc..

* L’element `<title>` està dins de l’element `<head>` i es mostra a la part superior del navegador, a sobre d'on normalment escrius l'URL de la pàgina que vols visitar, o a la pestanya d'aquesta pàgina.

{% image "body.png" %}

## Text

Les etiquetes (o "tags") *marquen* parts del document i proporcionen un significat addicional que permet als navegadors mostrar als usuaris l'estructura adequada per a la pàgina.

### Encapçalaments

HTML té **sis** "nivells" d'encapçalaments:

```html
<!doctype html>
<html>
 <head>
   <title>Encapçalaments</title>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet"href=" https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css">
 </head>
 <body>
   <h1>Aquest és un encapçalament principal</h1>
   <h2>Aquest és un encapçalament de nivell 2</h2>
   <h3>Aquest és un encapçalament de nivell 3</h3>
   <h4>Aquest és un encapçalament de nivell 4</h4>
   <h5>Aquest és un encapçalament de nivell 5</h5>
   <h6>Aquest és un encapçalament de nivell 6</h6>
</body>
</html>
```

* `<h1>` s'utilitza per als encapçalaments principals
* `<h2>` s'utilitza per als subtítols
* Si hi ha més seccions sota els subtítols, s'utilitza l'element `<h3>`, i així successivament...

Els navegadors mostren el contingut dels encapçalaments en diferents mides. El contingut d'un element `<h1>` és el més gran i el contingut d'un element `<h6>` és el més petit. La mida exacta en què cada navegador mostra els encapçalaments pot variar lleugerament.

{% panel "Headers" %}
<h1 style="all:revert">&lt;h1&gt;: Heading 1</h1>
<h2 style="all:revert">&lt;h2&gt;: Heading 2</h2>
<h3 style="all:revert">&lt;h3&gt;: Heading 3</h3>
<h4 style="all:revert">&lt;h4&gt;: Heading 4</h4>
<h5 style="all:revert">&lt;h5&gt;: Heading 5</h5>
<h6 style="all:revert">&lt;h6&gt;: Heading 6</h6>
{% endpanel %}

Els usuaris també poden ajustar la mida del text al seu navegador.

### Paràgrafs

{% panel "Paragraph" %}
<p class="border border-3 border-danger">Un paràgraf consta d'una o més frases que formen una unitat de discurs
    autònoma. L'inici d'un paràgraf s'indica amb una línia nova.</p>

  <p class="border border-3 border-danger">El text és més fàcil d'entendre quan es divideix en unitats de text. Per
    exemple, un llibre pot tenir capítols. Els capítols poden tenir subtítols. Sota cada encapçalament hi haurà un o més
    paràgrafs.</p>
{% endpanel %}

Per crear un paràgraf, envolta les paraules que componen el paràgraf amb una etiqueta d’obertura ` <p>` i una etiqueta de tancament `</p>`.

```html
<p class="border border-3 border-danger">Un paràgraf consta d'una o més frases que formen una unitat de discurs autònoma. L'inici d'un paràgraf s'indica amb una línia nova.</p>


<p class="border border-3 border-danger">El text és més fàcil d'entendre quan es divideix en unitats de text. Per exemple, un llibre pot tenir capítols. Els capítols poden tenir subtítols. Sota cada encapçalament hi haurà un o més paràgrafs.</p>
```


Per defecte, el navegador mostrarà cada paràgraf en una línia nova amb un, i només un espai, entre aquest i els paràgrafs posteriors.

Pots veure fem servir l’atribut `class` per estilitzar l’element `<p>` i ens mostri la vora de l’element, i així poder veure l’espai que introdueix el navegador.

### Superíndex i subíndex

```html
<p>Un byte són 8 bits i pot respresentar 2<sup>8</sup> nombres diferents.</p>

<p>El CO<sub>2</sub> és una mol.lècula ... .</p>
```


L’element `<sup>` s'utilitza per caràcters que han de ser superíndex, per exemple conceptes matemàtics com augmentar un nombre a una potència de 2: 2<sup>2</sup>

L’element `<sub>` s'utilitza per caràcters que han de ser subíndex, com les notes a peu de pàgina o fórmules químiques com H<sub>2</sub>O.

{% panel "Sup" %}
<p>Un byte són 8 bits i pot respresentar 2<sup>8</sup> nombres diferents.</p>
<p>El CO<sub>2</sub> és una mol.lècula ... .</p>
{% endpanel %}

### Espai blanc

```html
<p>La lluna s'allunya de la Terra.</p>


<p>La lluna
s'allunya de la Terra.</p>

<p>La lluna s'allunya
                         Terra.</p>
```

Quan el navegador troba dos o més espais al costat de l'altre, només mostra un espai. De la mateixa manera, si es troba amb un salt de línia, també el tracta com un espai únic. Això es coneix com **"white space collapsing"**.

{% panel "White Space" %}
<p>La lluna s'allunya de la Terra.</p>


<p>La lluna
s'allunya de la Terra.</p>

<p>La lluna s'allunya
                         Terra.</p>
{% endpanel %}

### Marcatge semàntic

{% panel "Emphasis" %}
<p>Aquesta joguina té moltes peces petites i, <em>potser</em>, <strong>no és apta per a nens menors de cinc anys.
</strong></p>
{% endpanel %}

Els tags `<strong>` i `<em>` es fan servir per donar significat *semàntic* a un troç de text.

* `<strong>` és per resaltar el text.
* `<em>` per enfatizar el text


```html
<p>Aquesta joguina té moltes peces petites i, <em>potser</em>, <strong>no és apta per a nens menors de cinc anys. </strong></p>
```

Un navegador mostra un `<strong>` en negreta, i un `<em>` en lletres itàliques.

Un assitent de veu puja el to en l'`<strong>` i utilitza un to diferent en l'`<em>`.

## Estil

Pots modificar l’estil d’un text tal com s’explica a [Text · Bootstrap v5.3](https://getbootstrap.com/docs/5.3/utilities/text/)

### Alineació

```html
<p class="text-start">Start aligned text on all viewport sizes.</p>
<p class="text-center">Center aligned text on all viewport sizes.</p>
<p class="text-end">End aligned text on all viewport sizes.</p>
```

Fixa't que és `start` i `end`, no `left` i `right`, perquè hi ha idiomes com l'àrab que s'escriu de dreta a esquerra i l'start seria a la dreta.

{% panel "Align" %}
<p class="text-start">Start aligned text on all viewport sizes.</p>
  <p class="text-center">Center aligned text on all viewport sizes.</p>
  <p class="text-end">End aligned text on all viewport sizes.</p>
{% endpanel %}

### font size

`fs` és un acrònim de font size i canvia el tamnay de la lletra de la secció marcada.

```html
<p class="fs-1">.fs-1 text</p>
<p class="fs-2">.fs-2 text</p>
<p class="fs-3">.fs-3 text</p>
<p class="fs-4">.fs-4 text</p>
<p class="fs-5">.fs-5 text</p>
<p class="fs-6">.fs-6 text</p>
```

Amb les etiquetes `<h1>`, …, `<h6>` també es canvia el tamany de la lletra, però per motius semàntics.

Amb `fs` només té un objectiu visual, semàticament una `<p>` segueix siguent una `<p>`.

{% panel "Font size" %}
 <p class="fs-1">.fs-1 text</p>
  <p class="fs-2">.fs-2 text</p>
  <p class="fs-3">.fs-3 text</p>
  <p class="fs-4">.fs-4 text</p>
  <p class="fs-5">.fs-5 text</p>
  <p class="fs-6">.fs-6 text</p>
{% endpanel %}

### font-weight

Amb `fw` pots controlar la “foscor” de l’element marcat:

```html
<p class="fw-bold">Bold text.</p>
<p class="fw-bolder">Bolder weight text (relative to the parent element).</p>
<p class="fw-semibold">Semibold weight text.</p>
<p class="fw-normal">Normal weight text.</p>
<p class="fw-light">Light weight text.</p>
<p class="fw-lighter">Lighter weight text (relative to the parent element).</p>
```

{% panel "Font Weight" %}
  <p class="fw-bold">Bold text.</p>
  <p class="fw-bolder">Bolder weight text (relative to the parent element).</p>
  <p class="fw-semibold">Semibold weight text.</p>
  <p class="fw-normal">Normal weight text.</p>
  <p class="fw-light">Light weight text.</p>
  <p class="fw-lighter">Lighter weight text (relative to the parent element).</p>
{% endpanel %}

### font style

```html
<p class="fst-italic">Italic text.</p>
<p class="fst-normal">Text with normal font style</p>
```

{% panel "Font Style/" %}
  <p class="fst-italic">Italic text.</p>
  <p class="fst-normal">Text with normal font style</p>
{% endpanel %}

### line height

```html
<p class="lh-1">This is a long paragraph written to show how the line-height of an element is affected by our utilities. Classes are applied to the element itself or sometimes the parent element. These classes can be customized as needed with our utility API.</p>
<p class="lh-sm">This is a long paragraph written to show how the line-height of an element is affected by our utilities. Classes are applied to the element itself or sometimes the parent element. These classes can be customized as needed with our utility API.</p>
<p class="lh-base">This is a long paragraph written to show how the line-height of an element is affected by our utilities. Classes are applied to the element itself or sometimes the parent element. These classes can be customized as needed with our utility API.</p>
<p class="lh-lg">This is a long paragraph written to show how the line-height of an element is affected by our utilities. Classes are applied to the element itself or sometimes the parent element. These classes can be customized as needed with our utility API.</p>
```

{% panel "Line Height" %}
  <p class="lh-1">This is a long paragraph written to show how the line-height of an element is affected by our
    utilities. Classes are applied to the element itself or sometimes the parent element. These classes can be
    customized as needed with our utility API.</p>
  <p class="lh-sm">This is a long paragraph written to show how the line-height of an element is affected by our
    utilities. Classes are applied to the element itself or sometimes the parent element. These classes can be
    customized as needed with our utility API.</p>
  <p class="lh-base">This is a long paragraph written to show how the line-height of an element is affected by our
    utilities. Classes are applied to the element itself or sometimes the parent element. These classes can be
    customized as needed with our utility API.</p>
  <p class="lh-lg">This is a long paragraph written to show how the line-height of an element is affected by our
    utilities. Classes are applied to the element itself or sometimes the parent element. These classes can be
    customized as needed with our utility API.</p>
{% endpanel %}

### Color

Pots modificar el color d’un text tal com s’explica a [Colors · Bootstrap v5.3](https://getbootstrap.com/docs/5.3/utilities/colors/).

```html
<p class="text-primary">.text-primary</p>
<p class="text-secondary">.text-secondary</p>
<p class="text-success">.text-success</p>
<p class="text-danger">.text-danger</p>
<p class="text-warning bg-dark">.text-warning</p>
<p class="text-info bg-dark">.text-info</p>
<p class="text-light bg-dark">.text-light</p>
<p class="text-dark">.text-dark</p>
<p class="text-body">.text-body</p>
<p class="text-muted">.text-muted</p>
<p class="text-white bg-dark">.text-white</p>
<p class="text-black-50">.text-black-50</p>
<p class="text-white-50 bg-dark">.text-white-50</p>
```

{% panel "Color" %}
  <p class="text-primary">.text-primary</p>
  <p class="text-secondary">.text-secondary</p>
  <p class="text-success">.text-success</p>
  <p class="text-danger">.text-danger</p>
  <p class="text-warning bg-dark">.text-warning</p>
  <p class="text-info bg-dark">.text-info</p>
  <p class="text-light bg-dark">.text-light</p>
  <p class="text-dark">.text-dark</p>
  <p class="text-body">.text-body</p>
  <p class="text-muted">.text-muted</p>
  <p class="text-white bg-dark">.text-white</p>
  <p class="text-black-50">.text-black-50</p>
  <p class="text-white-50 bg-dark">.text-white-50</p>
{% endpanel %}

### span

Si vols estilitzar un troç de text i prou pots utlitzar l'element neutre `<span>`.

**TODO** exemple

## Llistes

Una llista pot ser ordenada (té números per indicar l'ordre) o no.

### Llistes ordenades

```html
<ol>
 <li>Chop potatoes into quarters</li>
 <li>Simmer in salted water for 15-20  minutes until tender</li>
 <li>Heat milk, butter and nutmeg</li>
 <li>Drain potatoes and mash</li>
 <li>Mix in the milk mixture</li>
</ol>
```

Una llista ordenada es crea amb l’element `<ol>`: **o**rdered **l**ist.

Cada element de la llista es col·loca entre l'etiqueta d’obertura `<li>` i l'etiqueta de tancament `</li>` etiqueta (`li` vol dir **list item**).

{% panel "Ordered List" %}
  <ol>
    <li>Chop potatoes into quarters</li>
    <li>Simmer in salted water for 15-20 minutes until tender</li>
    <li>Heat milk, butter and nutmeg</li>
    <li>Drain potatoes and mash</li>
    <li>Mix in the milk mixture</li>
  </ol>
{% endpanel %}

Per defecte el navegador sagna les llistes.

### Llista no ordenada

```html
<ul>
 <li>1kg King Edward potatoes</li>
 <li>100ml milk</li>
 <li>50g salted butter</li>
 <li>Freshly grated nutmeg</li>
 <li>Salt and pepper to taste</li>
</ul>
```

La llista no ordenada es crea amb el l’element `<ul>`: **u*nordered **l**ist.

{% panel "Unordered List" %}
  <ul>
    <li>1kg King Edward potatoes</li>
    <li>100ml milk</li>
    <li>50g salted butter</li>
    <li>Freshly grated nutmeg</li>
    <li>Salt and pepper to taste</li>
  </ul>
{% endpanel %}

### Llistes imbricades

```html
<ul>
 <li>Mousses</li>
 <li>Pastries
   <ul>
     <li>Croissant</li>
     <li>Mille-feuille</li>
     <li>Palmier</li>
     <li>Profiterole</li>
   </ul>
 </li>
 <li>Tarts</li>
</ul>
```

Pots posar una segona llista dins d'un element `<li>` per crear una subllista o llista imbricada.

{% panel "List" %}
  <ul>
    <li>Mousses</li>
    <li>Pastries
      <ul>
        <li>Croissant</li>
        <li>Mille-feuille</li>
        <li>Palmier</li>
        <li>Profiterole</li>
      </ul>
    </li>
    <li>Tarts</li>
  </ul>
{% endpanel %}


Els navegadors mostren llistes imbricades amb un sagnat més enllà de la llista principal. 

A les llistes no ordenades imbricades, el navegador normalment canviarà també l'estil de la vinyeta.

### Estil

[List group -  Bootstrap v5.2.](https://getbootstrap.com/docs/5.2/components/list-group/) Els grups de llistes són un component flexible i potent per mostrar una sèrie de contingut.

```html
<ul class="list-group">
 <li class="list-group-item">An item</li>
 <li class="list-group-item">A second item</li>
 <li class="list-group-item">A third item</li>
</ul>
```

{% panel "List Group" %}
<ul class="list-group">
  <li class="list-group-item">An item</li>
  <li class="list-group-item">A second item</li>
  <li class="list-group-item">A third item</li>
</ul>
{% endpanel %}

Normalment s'estitlitza d'aquesta manera per crear menús.

## Taules

Les taules en HTML no són com les taules del Word perquè **no són quadrícules**, encara que ho puguin semblar, i és molt important que recordis això.

Una taula en HTML és un conjunt de files, i només files. No hi ha columnes encara que visualment sembli que la taula és una quadrícula amb files i columnes.

Una fila es marca amb `<tr>` ("**t**able **r**ow") i pot tenir tantes cel.les com tu vulguis. 

Les cel.les es marquen amb `<td>` ("**t**able **d**ata").

Per estilitzar les taules fem servir la fulla d’estils definida per bootstrap: [Taules · Bootstrap v5.1](https://getbootstrap.com/docs/5.1/content/tables/)

Si totes les files ("row") tenen el mateix número de cel.les ("data") es veurà una cuadricula

**Però si no tenen el mateix número de cel.les** no semblarà una taula com es pot veure en aquest exemple:

```html
<table class="table">
    <tr>
        <td>Gos</td>
        <td>Gat</td>
    </tr>
    <tr>
        <td>Tortuga</td>
    </tr>
    <tr>
        <td>Nutria</td>
        <td>Dolfí</td>
        <td>Sardina</td>
    </tr>
</table>
```

{% panel "Table Bad" %}
<div class="container mt-3">
    <table class="table">
      <tr class="border border-3 border-success">
        <td class="border border-3 border-danger">Gos</td>
        <td class="border border-3 border-danger">Gat</td>
      </tr>
      <tr class="border border-3 border-success">
        <td class="border border-3 border-danger">Tortuga</td>
      </tr>
      <tr class="border border-3 border-success">
        <td class="border border-3 border-danger">Nutria</td>
        <td class="border border-3 border-danger">Dolfí</td>
        <td class="border border-3 border-danger">Sardina</td>
      </tr>
    </table>
  </div>
{% endpanel %}

### Table heading

L’element `<th>` s'utilitza igual que l’element `<td>` però el seu propòsit és representar l'encapçalament d'una columna o d'una fila( `th` vol dir **t**able **h**eading).

```html
<table>
    <tr>
        <th></th>
        <th scope="col">Saturday</th>
        <th scope="col">Sunday</th>
    </tr>
    <tr>
        <th scope="row">Tickets sold:</th>
        <td>120</td>
        <td>135</td>
    </tr>
    <tr>
        <th scope="row">Total sales:</th>
        <td>$600</td>
        <td>$675</td>
    </tr>
</table>
```

{% panel "Table Heading" %}
<table class="table table-bordered">
    <tr>
        <th class="border border-3 border-success"></th>
        <th scope="col" class="border border-3 border-success">Saturday</th>
        <th scope="col" class="border border-3 border-success">Sunday</th>
      </tr>
      <tr>
        <th scope="row" class="border border-3 border-success">Tickets sold:</th>
        <td class="border border-3 border-danger">120</td>
        <td class="border border-3 border-danger">135</td>
      </tr>
      <tr>
        <th scope="row" class="border border-3 border-success">Total sales:</th>
        <td class="border border-3 border-danger">$600</td>
        <td class="border border-3 border-danger">$675</td>
      </tr>
    </table>
{% endpanel %}

Fins i tot si una cel·la no té contingut, has de fer servir un element `<td>` o `<th>` per representar la presència d'una cel·la buida, en cas contrari, la taula no es representarà correctament. (La primera cel·la de la primera fila d'aquest exemple mostra una cel·la buida.)

Utilitzar elements `<th>` per a encapçalaments ajuda a les persones que utilitzen lectors de pantalla, milloren la capacitat dels motors de cerca d'indexar les vostres pàgines i també us permeten controlar millor l'aparença de les taules quan comenceu a utilitzar CSS.

Pots utilitzar l'atribut `scope` a l’element `<th>` per indicar si és un encapçalament d'una columna o una fila. Pot prendre el  valor row per indicar un encapçalament per a una fila o col per indicar un encapçalament per a una columna.

Els navegadors solen mostrar el contingut de l’element `<th>` en negreta i al mig de la cel·la.

### Spanning columns

A vegades és possible que necessitis que les entrades d'una taula s'estiguin en més d'una columna.

{% panel "Table Span Col" %}
<table class="table table-bordered">
    <tr>
      <th class="border border-3 border-success"></th>
      <th class="border border-3 border-success">9am</th>
      <th class="border border-3 border-success">10am</th>
      <th class="border border-3 border-success">11am</th>
      <th class="border border-3 border-success">12am</th>
    </tr>
    <tr>
      <th class="border border-3 border-success">Monday</th>
      <td colspan="2" class="border border-3 border-danger">Geography</td>
      <td class="border border-3 border-danger">Math</td>
      <td class="border border-3 border-danger">Art</td>
    </tr>
    <tr>
      <th class="border border-3 border-success">Tuesday</th>
      <td colspan="3" class="border border-3 border-danger">Gym</td>
      <td class="border border-3 border-danger">Home Ec</td>
    </tr>
  </table>
{% endpanel %}

L'atribut `colspan` es pot utilitzar en l'element `<th>` o `<td>` element i indica quantes columnes ha de travessar aquesta cel·la.

```html
<table class="table table-bordered">
 <tr>
   <th></th>
   <th>9am</th>
   <th>10am</th>
   <th>11am</th>
   <th>12am</th>
 </tr>
 <tr>
   <th>Monday</th>
   <td colspan="2">Geography</td>
   <td>Math</td>
   <td>Art</td>
 </tr>
 <tr>
   <th>Tuesday</th>
   <td colspan="3">Gym</td>
   <td>Home Ec</td>
 </tr>
</table>
```

En aquest exemple pots veure un horari amb cinc columnes; la primera columna conté l'encapçalament d'aquesta fila (el dia), les quatre restants representen franges horàries d'una hora.

Si mires la cel·la de la taula que conté les paraules "Geografia", veuràs que el valor de l’atribut  colspan és 2, que vol dir que la cel.la s’ha d’extendre dues columnes. A la tercera fila, "Gym" s'estén a través de tres columnes.

Pots veure que la segona i la tercera fila tenen menys elements `<td>` que el número de columnes. Això és degut a que quan una cel·la s'estén més d'una columna, les cel.les `<td>` o `<th>` que haurien d’estar al lloc de les cel·les exteses no s'inclouen al codi.

### Spanning Rows

També és possible que necessites entrades en una taula per estendre's cap avall en més d'una fila.

{% panel %}
<table class="table table-bordered">
    <tr>
      <th class="border border-3 border-success"></th>
      <th class="border border-3 border-success">ABC</th>
      <th class="border border-3 border-success">BBC</th>
      <th class="border border-3 border-success">CNN</th>
    </tr>
    <tr>
      <th class="border border-3 border-success">6pm - 7pm</th>
      <td rowspan="2" class="border border-3 border-danger">Movie</td>
      <td class="border border-3 border-danger">Comedy</td>
      <td class="border border-3 border-danger">News</td>
    </tr>
    <tr>
      <th class="border border-3 border-success">7pm - 8pm</th>
      <td class="border border-3 border-danger">Sport</td>
      <td class="border border-3 border-danger">Current Affairs</td>
    </tr>
  </table>
{% endpanel %}

L'atribut `rowspan` es pot utilitzar en un element `<th>` o `<td>` per indicar quantes files ha d'omplir una cel·la a la taula.


```html
<table class="table table-bordered">
 <tr>
   <th></th>
   <th>ABC</th>
   <th>BBC</th>
   <th>CNN</th>
 </tr>
 <tr>
   <th>6pm - 7pm</th>
   <td rowspan="2">Movie</td>
   <td>Comedy</td>
   <td>News</td>
 </tr>
 <tr>
   <th>7pm - 8pm</th>
   <td>Sport</td>
   <td>Current Affairs</td>
 </tr>
</table>
```

A l'exemple pots veure que ABC mostra una pel·lícula de 18 a 20 hores, mentre que els canals de la BBC i CNN mostren dos programes durant aquest període de temps (cadascun d'ells dura una hora).

Si mires l'últim element `<tr>` només conté tres elements tot i que la taula té quatre “columnes”.

Això és perquè l’element `<tr>` que mostra l’horari de la película fa servir l’atribut rowspan per extendres cap avall i ocupar la cel·la de sota.


## Activitats

Has d'escriure el codi HTML dels troços que es mostren a continuació.

La solució no ha de ser exacta, però s'ha de semblar **bastant**.

**1.- Basic Chemistry**

<div class="border border-3 border-info rounded p-2 mt-4 mb-4">
  <h1>Química bàsica</h1>
  <p>Si tenim <del>30</del> <ins>50</ins> molècules de H<sup>2</sup>O <em>potser no tenim res</em>.</p>
  <p><u>Si fos</u> H<sub>2</sub>O <strong>potser</strong> tidriem quelcom</p>
</div>

{% sol %}
```html
<h1>Química bàsica</h1>
<p>Si tenim <del>30</del> <ins>50</ins> molècules de H<sup>2</sup>O <em>potser no tenim res</em>.</p>
<p><u>Si fos</u> H<sub>2</sub>O <strong>potser</strong> tidriem quelcom</p>
```
{% endsol %}

**2.- Quixote**

<div class="border border-3 border-info rounded p-2 mt-4 mb-4">
<h1 class="text-uppercase fs-3 text-center">Extracto de capítulos de la primera parte</h1>

<p class="text-center lh-1 fst-italic text-secondary"> Extracto de los capítulos que contiene esta famosa historia del valeroso caballero don Quijote de laMancha</p>

<h2 class="text-uppercase fs-5 text-center">Capítulo I</h2>

<p class="text-center lh-1 fst-italic text-secondary"> Que trata de la condición y ejercicio del famoso y valiente
    hidalgo don Quijote de la Mancha</p>


<p>En un lugar de la Mancha, de cuyo nombre no quiero acordarme, no ha mucho tiempo que vivía un hidalgo de los de
    lanza en astillero, adarga antigua, rocín flaco y galgo corredor</p>
</div>

{% sol %}
```html
<h1 class="text-uppercase fs-3 text-center">Extracto de capítulos de la primera parte</h1>

<p class="text-center lh-1 fst-italic text-secondary"> Extracto de los capítulos que contiene esta famosa historia del valeroso caballero don Quijote de laMancha</p>

<h2 class="text-uppercase fs-5 text-center">Capítulo I</h2>

<p class="text-center lh-1 fst-italic text-secondary"> Que trata de la condición y ejercicio del famoso y valiente
    hidalgo don Quijote de la Mancha</p>


<p>En un lugar de la Mancha, de cuyo nombre no quiero acordarme, no ha mucho tiempo que vivía un hidalgo de los de
    lanza en astillero, adarga antigua, rocín flaco y galgo corredor</p>
```
{% endsol %}

**3.- Un dia qualsevol**

<div class="border border-3 border-info rounded p-2 mt-4 mb-4">
<p class="lh-1 font-monospace text-info">Un dia qualsevol foradaré la terra i em faré un clot profund, perquè la mort
    m'arreplegui dempeus, reptador, temerari.</p>

  <p class="lh-lg text-success">Suportaré tossudament la pluja i arrelaré en el fang de mi mateix.Quiti de mots, em
    bastarà l'alè per afirmar una presència d'estricte vegetal.</p>
</div>

{% sol %}
```html
<p class="lh-1 font-monospace text-info">Un dia qualsevol foradaré la terra i em faré un clot profund, perquè la mort
    m'arreplegui dempeus, reptador, temerari.</p>

<p class="lh-lg text-success">Suportaré tossudament la pluja i arrelaré en el fang de mi mateix.Quiti de mots, em
    bastarà l'alè per afirmar una presència d'estricte vegetal.</p>
```
{% endsol %}

**4.- La vaca cega** 

<div class="border border-3 border-info rounded p-2 mt-4 mb-4">
<p class="fs-1 text-center fw-bold">Topant de cap en una i altra soca,</p>

  <p class="fs-2 text-center fw-bolder">avançant d'esma pel camí de l'aigua,</p>


  <p class="fs-3 text-center fw-semibold">se'n ve la vaca tota sola. És cega.</p>

  <p class="fs-4 text-center fw-normal">D'un cop de roc llançat amb massa traça,</p>

  <p class="fs-5 text-center fw-light">el vailet va buidar-li un ull, i en l'altre</p>

  <p class="fs-6 text-center fw-lighter">se li ha posat un tel: la vaca és cega.</p>
</div>

{% sol %}
```html
<p class="fs-1 text-center fw-bold">Topant de cap en una i altra soca,</p>

<p class="fs-2 text-center fw-bolder">avançant d'esma pel camí de l'aigua,</p>

<p class="fs-3 text-center fw-semibold">se'n ve la vaca tota sola. És cega.</p>

<p class="fs-4 text-center fw-normal">D'un cop de roc llançat amb massa traça,</p>

<p class="fs-5 text-center fw-light">el vailet va buidar-li un ull, i en l'altre</p>

<p class="fs-6 text-center fw-lighter">se li ha posat un tel: la vaca és cega.</p>
```
{% endsol %}

**5.- Vaixell de paper** 

<div class="border border-3 border-info rounded p-2 mt-4 mb-4">
<h3>Com es fa un vaixell de paper?</h3>
    <p>Material que necessitem:</p>
    <ul class="list-group mb-3">
      <li class="list-group-item">1 cartolina mida DIN A4</li>
      <li class="list-group-item">1 llapis</li>
      <li class="list-group-item">Goma</li>
    </ul>
    <p>Pasos a seguir per fer el vaixell</p>
    <ol class="list-group list-group-numbered">
      <li class="list-group-item">Dobleguem la cartolina per la meitat</li>
      <li class="list-group-item">Baixem els dos extrems superiors i els unim al centre</li>
      <li class="list-group-item">Pugem l'extrem inferior formant un rectangle i tocant els dos extrems superiors</li>
      <li class="list-group-item"> Dobleguem les puntes d'aquest rectangle cap al costat contrari</li>
    </ol>
</div>

{% sol %}
```html
<h3>Com es fa un vaixell de paper?</h3>
<p>Material que necessitem:</p>
<ul class="list-group mb-3">
  <li class="list-group-item">1 cartolina mida DIN A4</li>
  <li class="list-group-item">1 llapis</li>
  <li class="list-group-item">Goma</li>
</ul>
<p>Pasos a seguir per fer el vaixell</p>
<ol class="list-group list-group-numbered">
  <li class="list-group-item">Dobleguem la cartolina per la meitat</li>
  <li class="list-group-item">Baixem els dos extrems superiors i els unim al centre</li>
  <li class="list-group-item">Pugem l'extrem inferior formant un rectangle i tocant els dos extrems superiors</li>
  <li class="list-group-item"> Dobleguem les puntes d'aquest rectangle cap al costat contrari</li>
</ol>
```
{% endsol %}

**5.- Canelons** 

<div class="border border-3 border-info rounded p-2 mt-4 mb-4">
   <h1 class="text-center text-success">Canelons a la catalana o de Sant Esteve</h1>

    <p>Com fer canelons a la catalana o de Sant Esteve pas a pas</p>
    <figure class="figure">
      <img src="canelons.png" class="figure-img img-fluid rounded" alt="Canelons">
      <figcaption class="figure-caption">Uns canalons de Sant Esteve</figcaption>
    </figure>


    <ol class="list-group  list-group-numbered">
      <li class="list-group-item"> Agafem un bol i fiquem els diferents tipus de carn. Amb l'ajuda d'una forquilla
        aixafem i barregem totes les carns fins a obtenir una barreja més o menys homogènia.</li>
      <li class="list-group-item">En una fusta pelem i tallem la ceba al estil Brunoise.</li>
      <li class="list-group-item">En una paella afegim una mica d'oli d'oliva i deixem escalfar a foc mig fins que
        l'oli estigui ben calent.</li>
      <li class="list-group-item">Afegim la ceba i deixem cuinar uns 2-3 minuts a foc mig.</li>
      <li class="list-group-item">Seguidament afegim la barreja de carns de vedella ...</li>
    </ol>
</div>

{% sol %}
```html
<h1 class="text-center text-success">Canelons a la catalana o de Sant Esteve</h1>

<p>Com fer canelons a la catalana o de Sant Esteve pas a pas</p>
<figure class="figure">
  <img src="canelons.png" class="figure-img img-fluid rounded" alt="Canelons">
  <figcaption class="figure-caption">Uns canalons de Sant Esteve</figcaption>
</figure>

<ol class="list-group  list-group-numbered">
  <li class="list-group-item"> Agafem un bol i fiquem els diferents tipus de carn. Amb l'ajuda d'una forquilla
        aixafem i barregem totes les carns fins a obtenir una barreja més o menys homogènia.</li>
  <li class="list-group-item">En una fusta pelem i tallem la ceba al estil Brunoise.</li>
  <li class="list-group-item">En una paella afegim una mica d'oli d'oliva i deixem escalfar a foc mig fins que
        l'oli estigui ben calent.</li>
  <li class="list-group-item">Afegim la ceba i deixem cuinar uns 2-3 minuts a foc mig.</li>
  <li class="list-group-item">Seguidament afegim la barreja de carns de vedella ...</li>
</ol>
```
{% endsol %}

**6.- Dakar** 

<div class="border border-3 border-info rounded p-2 mt-4 mb-4">
  <h1 class="text-center fs-3">Clasificaciones 2023 al final de la etapa 9</h1>
    <p class="text-center fs-5">Distancia total recorrida: <strong>686 km</strong> </p>

    <table class="table table-striped table-sm">
      <thead>
        <tr>
          <th scope="col">Pos.</th>
          <th scope="col">N°</th>
          <th scope="col">Piloto-Equipo</th>
          <th scope="col">Equipo</th>
          <th scope="col">Tiempo</th>
          <th scope="col"> Diferencia</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="text-primary fw-bold">1</td>
          <td>77</td>
          <td class="fw-bold">(arg) LUCIANO BENAVIDES</td>
          <td>HUSQVARNA FACTORY RACING</td>
          <td class="font-monospace text-secondary">03h 18' 44''</td>
          <td></td>
        </tr>
        <tr>
          <td class="text-primary fw-bold">2</td>
          <td>8</td>
          <td class="fw-bold">(aus) TOBY PRICE</td>
          <td>RED BULL KTM FACTORY RACING</td>
          <td>03h 19' 46''</td>
          <td class="font-monospace text-secondary">+ 00h 01' 02''</td>
        </tr>
        <tr>
          <td class="text-primary fw-bold">3</td>
          <td>10</td>
          <td class="fw-bold">(usa) SKYLER HOWES</td>
          <td>HUSQVARNA FACTORY RACING</td>
          <td>03h 21' 41''</td>
          <td class="font-monospace text-secondary">+ 00h 02' 57''</td>
        </tr>
        <tr>
          <td class="text-primary fw-bold">4</td>
          <td>68</td>
          <td class="fw-bold">(esp) TOSHA SCHAREINA</td>
          <td>KTM RACING TEAM</td>
          <td>03h 23' 37''</td>
          <td class="font-monospace text-secondary">+ 00h 04' 53''</td>
        </tr>
        <tr>
          <td class="text-primary fw-bold">5</td>
          <td>17</td>
          <td class="fw-bold">(fra) ROMAIN DUMONTIER</td>
          <td>TEAM DUMONTIER RACING</td>
          <td>03h 24' 54''</td>
          <td class="font-monospace text-secondary">+ 00h 06' 10''</td>
        </tr>
      </tbody>
    </table>
</div>

{% sol %}
```html
<h1 class="text-center fs-3">Clasificaciones 2023 al final de la etapa 9</h1>
<p class="text-center fs-5">Distancia total recorrida: <strong>686 km</strong> </p>

<table class="table table-striped table-sm">
  <thead>
    <tr>
      <th scope="col">Pos.</th>
      <th scope="col">N°</th>
      <th scope="col">Piloto-Equipo</th>
      <th scope="col">Equipo</th>
      <th scope="col">Tiempo</th>
      <th scope="col"> Diferencia</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-primary fw-bold">1</td>
      <td>77</td>
      <td class="fw-bold">(arg) LUCIANO BENAVIDES</td>
      <td>HUSQVARNA FACTORY RACING</td>
      <td class="font-monospace text-secondary">03h 18' 44''</td>
      <td></td>
    </tr>
    <tr>
      <td class="text-primary fw-bold">2</td>
      <td>8</td>
      <td class="fw-bold">(aus) TOBY PRICE</td>
      <td>RED BULL KTM FACTORY RACING</td>
      <td>03h 19' 46''</td>
      <td class="font-monospace text-secondary">+ 00h 01' 02''</td>
    </tr>
    <tr>
      <td class="text-primary fw-bold">3</td>
      <td>10</td>
      <td class="fw-bold">(usa) SKYLER HOWES</td>
      <td>HUSQVARNA FACTORY RACING</td>
      <td>03h 21' 41''</td>
      <td class="font-monospace text-secondary">+ 00h 02' 57''</td>
    </tr>
    <tr>
      <td class="text-primary fw-bold">4</td>
      <td>68</td>
      <td class="fw-bold">(esp) TOSHA SCHAREINA</td>
      <td>KTM RACING TEAM</td>
      <td>03h 23' 37''</td>
      <td class="font-monospace text-secondary">+ 00h 04' 53''</td>
    </tr>
    <tr>
      <td class="text-primary fw-bold">5</td>
      <td>17</td>
      <td class="fw-bold">(fra) ROMAIN DUMONTIER</td>
      <td>TEAM DUMONTIER RACING</td>
      <td>03h 24' 54''</td>
      <td class="font-monospace text-secondary">+ 00h 06' 10''</td>
    </tr>
  </tbody>
</table>
```
{% endsol %}

**7.- Horari**

<div class="border border-3 border-info rounded p-2 mt-4 mb-4">
<h1 class="text-center fs-3">CFGS - Desenvolupament Aplicacions Web</h1>
<table class="table table-bordered">
  <caption>Horari del CGFS-DAW</caption>
  <tr>
    <th></th>
    <th scope="col">Dilluns</th>
    <th scope="col">Dimarts</th>
    <th scope="col">Dimecres</th>
    <th scope="col">Dijous</th>
    <th scope="col">Divendres</th>
  </tr>
  <tr>
    <th scope="col">8:30<br />9:30</th>
    <td rowspan="3">
      <div class="fw-bold text-danger">DAW-04</div>
      <div>de Mingo</div>
      <div class="text-muted fst-italic">or43</div>
    </td>
    <td rowspan="2">
      <div class="fw-bold text-danger">DAW-10</div>
      <div>Alonso</div>
      <div class="text-muted fst-italic">or43</div>
    </td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <th scope="col">9:30<br />10:30</th>
    <td rowspan="2">
      <div class="fw-bold text-danger">DAW-11</div>
      <div>Maria</div>
      <div class="text-muted fst-italic">or43</div>
    </td>
    <td rowspan="2">
      <div class="fw-bold text-danger">DAW-2</div>
      <div>Montesinos</div>
      <div class="text-muted fst-italic">or43</div>
    </td>
    <td rowspan="2">
      <div class="fw-bold text-danger">DAW-1</div>
      <div>Verdegal</div>
      <div class="text-muted fst-italic">or43</div>
    </td>
  </tr>
  <tr>
    <th scope="col">10:30<br />11:30</th>
    <td>
      <div class="fw-bold text-danger">DAW-3</div>
      <div>Bardají</div>
      <div class="text-muted fst-italic">or43</div>
    </td>
  </tr>
  <tr>
    <th scope="col">11:30<br />12:00</th>
    <td colspan="5"></td>
  </tr>
  <tr>
    <th scope="col">12:00<br />13:00</th>
    <td rowspan="3">
      <div class="fw-bold text-danger">DAW-3</div>
      <div>Bardají</div>
      <div class="text-muted fst-italic">or43</div>
    </td>
    <td>
      <div class="fw-bold text-danger">DAW-3</div>
      <div>Bardají</div>
      <div class="text-muted fst-italic">or43</div>
    </td>
    <td rowspan="2">
      <div class="fw-bold text-danger">DAW-3</div>
      <div>Bardají</div>
      <div class="text-muted fst-italic">or43</div>
    </td>
    <td>
      <div class="fw-bold text-danger">DAW-2</div>
      <div>Montesinos</div>
      <div class="text-muted fst-italic">or43</div>
    </td>
    <td rowspan="2">
      <div class="fw-bold text-danger">DAW-5</div>
    </td>
  </tr>
</table>
</div>

{% sol %}
```html
<h1 class="text-center fs-3">CFGS - Desenvolupament Aplicacions Web</h1>
<table class="table table-bordered">
  <caption>Horari del CGFS-DAW</caption>
  <tr>
    <th></th>
    <th scope="col">Dilluns</th>
    <th scope="col">Dimarts</th>
    <th scope="col">Dimecres</th>
    <th scope="col">Dijous</th>
    <th scope="col">Divendres</th>
  </tr>
  <tr>
    <th scope="col">8:30<br />9:30</th>
    <td rowspan="3">
      <div class="fw-bold text-danger">DAW-04</div>
      <div>de Mingo</div>
      <div class="text-muted fst-italic">or43</div>
    </td>
    <td rowspan="2">
      <div class="fw-bold text-danger">DAW-10</div>
      <div>Alonso</div>
      <div class="text-muted fst-italic">or43</div>
    </td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <th scope="col">9:30<br />10:30</th>
    <td rowspan="2">
      <div class="fw-bold text-danger">DAW-11</div>
      <div>Maria</div>
      <div class="text-muted fst-italic">or43</div>
    </td>
    <td rowspan="2">
      <div class="fw-bold text-danger">DAW-2</div>
      <div>Montesinos</div>
      <div class="text-muted fst-italic">or43</div>
    </td>
    <td rowspan="2">
      <div class="fw-bold text-danger">DAW-1</div>
      <div>Verdegal</div>
      <div class="text-muted fst-italic">or43</div>
    </td>
  </tr>
  <tr>
    <th scope="col">10:30<br />11:30</th>
    <td>
      <div class="fw-bold text-danger">DAW-3</div>
      <div>Bardají</div>
      <div class="text-muted fst-italic">or43</div>
    </td>
  </tr>
  <tr>
    <th scope="col">11:30<br />12:00</th>
    <td colspan="5"></td>
  </tr>
  <tr>
    <th scope="col">12:00<br />13:00</th>
    <td rowspan="3">
      <div class="fw-bold text-danger">DAW-3</div>
      <div>Bardají</div>
      <div class="text-muted fst-italic">or43</div>
    </td>
    <td>
      <div class="fw-bold text-danger">DAW-3</div>
      <div>Bardají</div>
      <div class="text-muted fst-italic">or43</div>
    </td>
    <td rowspan="2">
      <div class="fw-bold text-danger">DAW-3</div>
      <div>Bardají</div>
      <div class="text-muted fst-italic">or43</div>
    </td>
    <td>
      <div class="fw-bold text-danger">DAW-2</div>
      <div>Montesinos</div>
      <div class="text-muted fst-italic">or43</div>
    </td>
    <td rowspan="2">
      <div class="fw-bold text-danger">DAW-5</div>
    </td>
  </tr>
</table>
```
{% endsol %}

## Avaluació

**1.- Aigua pesant**

<div class="border border-3 border-info rounded p-2 mt-4 mb-4">
  <h1>Aigua pesant</h1>

  <p>L'<strong>aigua pesant</strong> és aigua (H<sub>2</sub>O) en la qual els àtoms d'hidrogen <u>han estat substituïts
      pel deuteri (D)</u>. La seva fórmula química és doncs D<sub>2</sub>O.</em></p>

  <ul>
    <li><strong>Fórmula:</strong> D<sub>2</sub>O</li>
    <li><strong>Densitat:</strong> 1,11 g/cm<sup>3</sup></li>
    <li><strong>Massa molar: </strong>20,0276 g/mol</li>
  </ul>
</div>

{% sol %}
```html
<h1>Aigua pesant</h1>

<p>L'<strong>aigua pesant</strong> és aigua (H<sub>2</sub>O) en la qual els àtoms d'hidrogen <u>han estat substituïts
    pel deuteri (D)</u>. La seva fórmula química és doncs D<sub>2</sub>O.</em></p>

<ul>
  <li><strong>Fórmula:</strong> D<sub>2</sub>O</li>
  <li><strong>Densitat:</strong> 1,11 g/cm<sup>3</sup></li>
  <li><strong>Massa molar: </strong>20,0276 g/mol</li>
</ul>
```
{% endsol %}

**2.- A un avariento**

En la poesia és un dels pocs casos en que és correcte fer servir el tag `<br/>` per trencar la línia.

<div class="border border-3 border-info rounded p-2 mt-4 mb-4">
    <h1 class="fs-4 text-center text-primary"> A un avariento</h1>

    <p class="text-start text-secondary lh-sm">En aqueste enterramiento <br>
      humilde, pobre y mezquino,<br>
      yace envuelto en oro fino <br>
      un hombre rico avariento.</p>

    <p class="text-end lh-lg text-danger"> Murió con cien mil dolores <br>
      sin poderlo remediar,<br>
      tan sólo por no gastar<br>
      ni aun gasta malos humores.</p>
    <p class="text-center fst-italic text-success">Francisco de Quevedo</p>
</div>

{% sol %}
```html
<h1 class="fs-4 text-center text-primary"> A un avariento</h1>

<p class="text-start text-secondary lh-sm">En aqueste enterramiento <br>
    humilde, pobre y mezquino,<br>
    yace envuelto en oro fino <br>
    un hombre rico avariento.</p>

<p class="text-end lh-lg text-danger"> Murió con cien mil dolores <br>
    sin poderlo remediar,<br>
    tan sólo por no gastar<br>
    ni aun gasta malos humores.</p>
<p class="text-center fst-italic text-success">Francisco de Quevedo</p>
```
{% endsol %}

**3.- Premi Turing**

<div class="border border-3 border-info rounded p-2 mt-4 mb-4">
<h1 class="fs-2">Premi Turing</h1>

    <p>El <strong>Premi Turing</strong> és considerat per molts com el Premi Nobel de la Informàtica.</p>

    <h2 class="fs-3">Guardonats</h2>

    <table class="table table-bordered table-striped table-sm">
      <tr class="fs-bold">
        <th>Any</th>
        <th>Nom</th>
        <th>Motiu</th>
      </tr>
      <tr>
        <td class="fw-light">1966</td>
        <td class="text-primary fw-semibold">Alan Perlis</td>
        <td class="lh-sm">Per la seva influència en les àrees de tècniques de programació avançades i construcció de
          compiladors.</td>
      </tr>
      <tr>
        <td class="fw-light">1967</td>
        <td class="text-primary fw-semibold">Maurice V. Wilkes</td>
        <td class="lh-sm">Pel disseny i construcció d'EDSAC, el primer ordinador de programa emmagatzemament en memòria
          interna.</td>
      </tr>
      <tr>
        <td class="fw-light">1968</td>
        <td class="text-primary fw-semibold">Richard Hamming</td>
        <td class="lh-sm">Per la seva feina en mètodes numèrics, sistemes de codificació automàtics, i pel
          desenvolupament de codis de detecció i correcció d'errors.</td>
      </tr>
      <tr>
        <td class="fw-light">1969</td>
        <td class="text-primary fw-semibold">Marvin Minsky</td>
        <td class="lh-sm">Per les seves aportacions a intel·ligència artificial.</td>
      </tr>
      <tr>
        <td class="fw-light">1970</td>
        <td class="text-primary fw-semibold">JH Wilkinson</td>
        <td class="lh-sm">Per les seves investigacions en anàlisi numèrica per facilitar l'ús de computadors digitals
          d'alta velocitat.</td>
      </tr>
    </table>
</div>

{% sol %}
```html
<h1 class="fs-2">Premi Turing</h1>

<p>El <strong>Premi Turing</strong> és considerat per molts com el Premi Nobel de la Informàtica.</p>

<h2 class="fs-3">Guardonats</h2>

<table class="table table-bordered table-striped table-sm">
  <tr class="fs-bold">
    <th>Any</th>
    <th>Nom</th>
    <th>Motiu</th>
  </tr>
  <tr>
    <td class="fw-light">1966</td>
    <td class="text-primary fw-semibold">Alan Perlis</td>
    <td class="lh-sm">Per la seva influència en les àrees de tècniques de programació avançades i construcció de
      compiladors.</td>
  </tr>
  <tr>
    <td class="fw-light">1967</td>
    <td class="text-primary fw-semibold">Maurice V. Wilkes</td>
    <td class="lh-sm">Pel disseny i construcció d'EDSAC, el primer ordinador de programa emmagatzemament en memòria
      interna.</td>
  </tr>
  <tr>
    <td class="fw-light">1968</td>
    <td class="text-primary fw-semibold">Richard Hamming</td>
    <td class="lh-sm">Per la seva feina en mètodes numèrics, sistemes de codificació automàtics, i pel
      desenvolupament de codis de detecció i correcció d'errors.</td>
  </tr>
  <tr>
    <td class="fw-light">1969</td>
    <td class="text-primary fw-semibold">Marvin Minsky</td>
    <td class="lh-sm">Per les seves aportacions a intel·ligència artificial.</td>
  </tr>
  <tr>
    <td class="fw-light">1970</td>
    <td class="text-primary fw-semibold">JH Wilkinson</td>
    <td class="lh-sm">Per les seves investigacions en anàlisi numèrica per facilitar l'ús de computadors digitals
      d'alta velocitat.</td>
  </tr>
</table>
```
{% endsol %}




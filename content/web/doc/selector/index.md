---
title: Selector
description: Un selector ens permet seleccionar de manera declarativa un subconjunt d'elements de la nostra pàgina
mermaid: true
---

## Introducció

Quan un navegador carrega un document HTML, converteix aquest document en un **mòdel d'objectes**: DOM (Document Object Module).

Un DOM representa el document en memòria com un arbre de nodes, o arbre DOM, on cada node representa un part del document.

El selectors són expresions que, igual que fas amb una base de dades, et pertmet crear una consulta que et tornarà un conjunt de nodes que cumpleixen els criteris del selector.

## Style

**En aquesta activitat, no utilitzem Boostrap**

Cada node del document HTML és un objecte que té propietats.

Una d'elles és `style` que al seu torn també tè propietats, com pot ser `color`, etc.

A continuació tens un document on modifiquem les propietats `color` i `margin` de la propietat `style` del node `<p>`:

```html
<!doctype html>
<html>
<head>
  <title>CSS</title>
</head>
<body>
  <p style="color:blue; margin-left: 10em;">No diguis blat fins que no el tinguis al sac i ben lligat:</p>
</body>
</html>
```

El resultat és aquest:

{% panel %}
<div class="container">
    <p style="color:blue; margin-left: 10em;">No diguis blat fins que no el tinguis al sac i ben lligat:</p>
</div>
{% endpanel %}


### Emmet

Quan escrius HTML amb l’editor VS Code pots descriure un part de l’estructura d’un document, i aquest es completarà de manera automàtica tal com s’explica en aquest document: [Emmet in Visual Studio Code](https://code.visualstudio.com/docs/editor/emmet).

{% image "emmet.gif" %}

Quan comences a escriure una abreviatura d'Emmet, pots veure l'abreviatura a la llista de suggeriments. 

En aquest document està explicat totes les ajudes que ofereix emmet : [Cheat Sheet](https://docs.emmet.io/cheat-sheet/).

Amb l'ajuda d'emmet estilitza aquest document:

{% panel %}
<div style="border: 3px solid blue; padding: 1em;">
  <p style="color: chocolate; font-size: 1em;">Arran de camí, de tornada, </p>
  <p style="color: crimson; font-size: 2em; margin-top: 1em;">L'escorça es fon amb el silenci.</p>
  <p style="color:darkmagenta ; font-size: .8em; margin-left: 5em;">Cap pregunta. Capvespre.</p>
</div>
{% endpanel %}

{% sol %}
```html
<!doctype html>
<html>
<head>
  <title>Style</title>
</head>
<body style="border: 3px solid blue; padding: 1em;">
  <p style="color: chocolate; font-size: 1em;">Arran de camí, de tornada, </p>
  <p style="color: crimson; font-size: 2em; margin-top: 1em;">L'escorça es fon amb el silenci.</p>
  <p style="color:darkmagenta ; font-size: .8em; margin-left: 5em;">Cap pregunta. Capvespre.</p>
</body>

</html>
```
{% endsol %}

### Regla

Enlloc de modificar les propietats dels nodes un a un, puc utilitzar un selector per seleccionar un conjunt de nodes a la vegada i modificar-ho tot al mateix temps.

**Les regles CSS utitlitzen selectors.**

A continuació tens una document HTML modificat amb una regla CSS:

{% panel %}
<div class="container">
  <h1 style="color: red; font-size: 2em;">I'm red!</h1>
</div>
{% endpanel %}

Un regla CSS es pot declarar dins de l'element `<head>` d'un document amb el bloc `<style>`:

```html
<!doctype html>
<html>
<head>
  <title>Selector</title>
  <meta charset="utf-8">

  <style>
    h1 {
      color: red;
      font-size: 2em;
    }
  </style>

</head>
<body>
  <h1>I'm red!</h1>
</body>
</html>
```

La regla comença amb un **selector**, que selecciona **tots** els elements `<h1>`:

```css
h1 {
    color: red;
    font-size: 2em;
}
```

Dins de` { }` tenim dos declaracions: una per a la propietat `color` i l'altre per a la propietat `font-size` (en unitats `em`).

Pots declarar tantes regles com vulguis dins d'un bloc `<style>`:

```html
<!doctype html>
<html>
<head>
  <title>Selector</title>
  <meta charset="utf-8">

  <style>
    h1 {
      color: red;
      font-size: 2em;
    }

    p {
      color: seagreen;
    }
  </style>

</head>
<body>
  <h1>I'm red!</h1>
  <p>I'm seagreen :-)</p>
  <p>It's nice</p>
</body>
</html>
```

En aquest cas tenim dos regles: una pels elements `<h1>`, una altra pels elements `<p>`:

{% panel %}
<div class="container">
  <h1 style="color: red; font-size: 2em;">I'm red!</h1>
  <p style="color: seagreen">I'm seagreen :-)</p>
  <p style="color: seagreen">It's nice</p>
</div>
{% endpanel %}

### Element

A continuació veurem diferents exemples de regles a partir d'aquest document HTML:

```html
<!doctype html>
<html>
<head>
    <title>Selector</title>
    <style>

    </style>
</head>
<body>
    <h1>I am a level one heading</h1>
    <p>This is a paragraph of text. In the text is a <span>span element</span> 
    and also a <a href="https://google.com">link</a>.</p>
    <p>This is the second paragraph. It contains an <em>emphasized</em> element.</p>
    <ul>
        <li>Item <span>one</span></li>
        <li>Item two</li>
        <li>Item <em>three</em></li>
    </ul>
</body>
</html>
```

Afegeix le regles del document anterior:

```css
h1 {
    color: red;
    font-size: 2em;
}

 p {
    color: seagreen;
}
```

Amb aquestes regles el document es veu així:

{% panel %}
<div class="container">
    <h1 style="color: red; font-size: 2em;">I am a level one heading</h1>
    <p style="color: seagreen">This is a paragraph of text. In the text is a <span>span element</span> 
    and also a <a href="https://google.com">link</a>.</p>
    <p style="color: seagreen">This is the second paragraph. It contains an <em>emphasized</em> element.</p>
    <ul>
        <li>Item <span>one</span></li>
        <li>Item two</li>
        <li>Item <em>three</em></li>
    </ul>
</div>
{% endpanel %}


Pots fer servir diversos selectors al mateix temps separant-los amb una coma:

```css
h1 { color: red; font-size: 2em; }

p, li {
    color: seagreen;
}
```

Pots veure que el selector s'aplica a tots els elements `p` i `li`:

{% panel %}
<div class="container">
    <h1 style="color: red; font-size: 2em;">I am a level one heading</h1>
    <p style="color: seagreen">This is a paragraph of text. In the text is a <span>span element</span> 
    and also a <a href="https://google.com">link</a>.</p>
    <p style="color: seagreen">This is the second paragraph. It contains an <em>emphasized</em> element.</p>
    <ul>
        <li style="color: seagreen">Item <span>one</span></li>
        <li style="color: seagreen">Item two</li>
        <li style="color: seagreen">Item <em>three</em></li>
    </ul>
</div>
{% endpanel %}

### Classe

També pots seleccionar elements a partir dels valors de l'atribut `class`.

És el sistema que utilitza Bootstrap per estilitzar els documents.

Modifica el segon element de la llista, afegint una atribut `class`:

```html
<ul>
  <li>Item one</li>
  <li class="special">Item two</li>
  <li>Item <em>three</em></li>
</ul>
```

Per indicar que el selectpr és d'un valor de l'atribut classe, has de posar un punt al inici del selector.

Afegeix aquesta regla al final de l'element `<style>`:

```css
.special {
  color: orange;
  font-weight: bold;
}
```

Pots veure que aquesta regla només s'aplica al segon element de la llista:

{% panel %}
<div class="container">
    <h1 style="color: red; font-size: 2em;">I am a level one heading</h1>
    <p style="color: seagreen">This is a paragraph of text. In the text is a <span>span element</span> 
    and also a <a href="https://google.com">link</a>.</p>
    <p style="color: seagreen">This is the second paragraph. It contains an <em>emphasized</em> element.</p>
    <ul>
        <li style="color: seagreen">Item <span>one</span></li>
        <li style="color: orange; font-weight: bold;">Item two</li>
        <li style="color: seagreen">Item <em>three</em></li>
    </ul>
</div>
{% endpanel %}

Les regles s'apliquen de dalt a baix (en cascada), i una regla que s'aplica després sobreescriu una que s'ha aplicat abans.


Pots aplicar la classe `special` a qualsevol element del document que vulguis que tingui el mateix aspecte que el segon element de la llista:

```html
<!doctype html>
<html>
<head>
    <title>Selector</title>

    <style>
      h1 { color: red; font-size: 2em; }
      p, li { color: seagreen;}
      .special {  color: orange; font-weight: bold;}
    </style>

</head>
<body>
    <h1>I am a level one heading</h1>
    <p>This is a paragraph of text. In the text is a <span class="special">span element</span> 
    and also a <a href="https://google.com">link</a>.</p>
    <p>This is the second paragraph. It contains an <em>emphasized</em> element.</p>
    <ul>
        <li>Item <span>one</span></li>
        <li class="special">Item two</li>
        <li>Item <em>three</em></li>
    </ul>
</body>
</html>
```

{% panel %}
<div class="container">
    <h1 style="color: red; font-size: 2em;">I am a level one heading</h1>
    <p style="color: seagreen">This is a paragraph of text. In the text is a <span style="color: orange; font-weight: bold;">span element</span> 
    and also a <a href="https://google.com">link</a>.</p>
    <p style="color: seagreen">This is the second paragraph. It contains an <em>emphasized</em> element.</p>
    <ul>
        <li style="color: seagreen">Item <span>one</span></li>
        <li style="color: orange; font-weight: bold;">Item two</li>
        <li style="color: seagreen">Item <em>three</em></li>
    </ul>
</div>
{% endpanel %}

Si vols ser més precís, pots seleccionar per element i classe a la vegada:

```css
li.special {
  color: orange;
  font-weight: bold;
}
```

Aquesta regla només s'aplica als elements `<li>` que tenen la un valor de classe `special`:

```html
<li class="special">
```
 
Per tant, aquesta regla CSS ja no s’aplica a l'element `<span>` encara que tingui l’atribut `class` amb el valor `special` perquè no és un element `<li>`:

{% panel %}
<div class="container">
    <h1 style="color: red; font-size: 2em;">I am a level one heading</h1>
    <p style="color: seagreen">This is a paragraph of text. In the text is a <span>span element</span> 
    and also a <a href="https://google.com">link</a>.</p>
    <p style="color: seagreen">This is the second paragraph. It contains an <em>emphasized</em> element.</p>
    <ul>
        <li style="color: seagreen">Item <span>one</span></li>
        <li style="color: orange; font-weight: bold;">Item two</li>
        <li style="color: seagreen">Item <em>three</em></li>
    </ul>
</div>
{% endpanel %}

### Ubicació

Un document HTML s'estructuren en forma d’arbre invertit:

<pre class="mermaid">
flowchart TD
  html
  html --> head
  html --> body
  body --> h1
  body --> p1["p"]
  body --> p2["p"]
  body --> p3["p"]
  p3 --> em1["em"]
  body --> ul
  ul --> li1["li"]
  ul --> li2["li"]
  ul --> li3["li"]
  li3 --> em2["em"]
</pre>

En el nostre document hi ha dos elements `<em>`.

Si vols pots selecciona un element `<em>` en funció de la seva ubicació respectecte un altre element-

Per exemple, aquesta regla només s'aplica a aquells elements `<em>` que són descendents d'un element `<li>`:

```css
li em {
  color: rebeccapurple;
}
```

Pots veure que ara el `<em>` del tercer element `<li>` és de color porpra, però el que hi ha dins de la `<p>` no s'ha modificat:

{% panel %}
<div class="container">
    <h1 style="color: red; font-size: 2em;">I am a level one heading</h1>
    <p style="color: seagreen">This is a paragraph of text. In the text is a <span>span element</span> 
    and also a <a href="https://google.com">link</a>.</p>
    <p style="color: seagreen">This is the second paragraph. It contains an <em>emphasized</em> element.</p>
    <ul>
        <li style="color: seagreen">Item <span>one</span></li>
        <li style="color: orange; font-weight: bold;">Item two</li>
        <li style="color: seagreen">Item <em style="color: rebeccapurple">three</em></li>
    </ul>
</div>
{% endpanel %}

També pots seleccionar un element que està a continuació de l'altre:

Per exemple, aquesta regla selecciona totes les `<p>`s que estan a continuació d'un `<h1>`:

```css
h1 + p {
  font-size: 150%;
}
```

{% panel %}
<div class="container">
    <h1 style="color: red; font-size: 2em;">I am a level one heading</h1>
    <p style="color: seagreen; font-size: 150%">This is a paragraph of text. In the text is a <span>span element</span> 
    and also a <a href="https://google.com">link</a>.</p>
    <p style="color: seagreen">This is the second paragraph. It contains an <em>emphasized</em> element.</p>
    <ul>
        <li style="color: seagreen">Item <span>one</span></li>
        <li style="color: orange; font-weight: bold;">Item two</li>
        <li style="color: seagreen">Item <em style="color: rebeccapurple">three</em></li>
    </ul>
</div>
{% endpanel %}

## StyleSheet

Enlloc de tenir un bloc `<style>` dins el document HTML, el pots tenir en un document apart.

Aquest document té extensió `css` i el poden utlitzar molts documents web a la vegada.

Per exemple, nosaltres fem servir `bootstrap.min.css` que està en aquesta URL: <https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.css>

```html
<!doctype html>
<html>
  <head>
    <title>Hello</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css"> 
  </head>
  <body>
  </body>
</html>
```

Pots veure que en el `<head>` crees un `<link>` de tipus `stylesheet` a la url <https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css>.

El navegador, al carregar el document web, segueix l'enllaç i processa la fulla d'estil.

Crea una fulla d'estil pròpia `style.css`:

```css
body { margin: 3em;}
p { color: red;}
```

Crea un document HTML amb un enllaç relatiu a la teva fulla d'estil:

{% sol %}
{% image "link.png" %}
{% endsol %}

## Script

El DOM és una de les APIs més utilitzades perquè pertme al codi que s'executa dins el navegador interactuar amb tots el nodes del document.

El selectors també es fan servir quan programem Javascript per seleccionar elements del DOM als quals aplicarem alguna acció.

Afegeix un bloc `<script>` al final del `<body>`:

```html
<!doctype html>
<html>
<head>
  <title>Script</title>
</head>
<body>
  <p>El poble és un vell tossut,</p>
  <p>és una noia que no té promès,</p>
  <p>és un petit comerciant en descrèdit,</p>
  <p>és un parent amb qui vam renyir fa molt de temps.</p>
  <script>
    document.querySelector("p").style.color = "red"
 </script>
</body>
</html>
```

El mètode `document.querySelector("p")` et torna el primer node que fa "match" amb el selector.

En el nostre exemple, modifiques el `style.color` de l'element `<p>`:

{% panel %}
<p style="color: red">El poble és un vell tossut,</p>
<p>és una noia que no té promès,</p>
<p>és un petit comerciant en descrèdit,</p>
<p>és un parent amb qui vam renyir fa molt de temps.</p>
{% endpanel %}

Si vols seleccionar tots els elements, utilitza el mètode `querySelectorAll()`:

```html
<!doctype html>
<html>
<head>
  <title>Script</title>
</head>
<body>
  <p>El poble és un vell tossut,</p>
  <p>és una noia que no té promès,</p>
  <p>és un petit comerciant en descrèdit,</p>
  <p>és un parent amb qui vam renyir fa molt de temps.</p>
  <script>
    document.querySelectorAll("p").forEach(e => e.style.color = "red")
  </script>
</body>
</html>
```

Amb un `foreach` pots modificar la propietat `style.color` de tots els elements:

{% panel %}
<p style="color: red">El poble és un vell tossut,</p>
<p style="color: red">és una noia que no té promès,</p>
<p style="color: red">és un petit comerciant en descrèdit,</p>
<p style="color: red">és un parent amb qui vam renyir fa molt de temps.</p>
{% endpanel %}

### Bootstrap

Molts cops has d'afegir les mateixes classes a un conjunt d'elements.

Amb javascript pots seleccionar els elements i aplicar les classes corresponents.


```html
<!doctype html>
<html>
<head>
  <title>Script</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
  <h1 style="color: chocolate;" class="fw-bold text-center fs-3 mt-5">El Poble</h1>
  <div class="border border-3 m-3 p-1">
    <p>El poble és un vell tossut,</p>
    <p>és una noia que no té promès,</p>
    <p>és un petit comerciant en descrèdit,</p>
    <p>és un parent amb qui vam renyir fa molt de temps.</p>
  </div>
  <script>
    document.querySelectorAll("p").forEach(
      e => e.classList.add("text-center", "fw-bold")
    )
  </script>
</body>
</html>
```

En aquest exemple, utilitzem Javascript per afegir les classes `text-center` i `fw-bold` a totes les `<p>`s:

{% panel %}
<h1 style="color: chocolate;" class="fw-bold text-center fs-3 mt-5">El Poble</h1>
  <div class="border border-3 m-3 p-1">
    <p class="text-center fw-bold">El poble és un vell tossut,</p>
    <p class="text-center fw-bold">és una noia que no té promès,</p>
    <p class="text-center fw-bold">és un petit comerciant en descrèdit,</p>
    <p class="text-center fw-bold">és un parent amb qui vam renyir fa molt de temps.</p>
  </div>
{% endpanel %}


## Activitat

**1.-** Setze jutges

{% panel %}
<h1 style="font-size: 2em; color: beige; background-color: blueviolet; padding: 2em;">Dites i refranys</h1>
<h3 style="color: orangered; margin-top: 4em; margin-left: 1em;">Embarbussaments i jocs de paraules</h2>

<p style="color: brown; background-color: antiquewhite; font-size: 1.5em; margin-top: 3em; margin-left: 1em;">Els nostres jutges es mengen el fetge del penjat, que es menjaria el fetge dels jutges en el cas de ser despenjat.</p>
<p style="color: darkslateblue;background-color: aqua; padding: 1em; margin-left: 4em;">Setze jutges d'un jutjat mengen fetges d'un penjat.</p>
<p style="color: darkslateblue;background-color: aqua; padding: 1em; margin-left: 4em;">Setze jutges d'un jutjat mengen fetge d'un heretge; setze jutges d'un jutjat mengen fetge d'un penjat que encara penja.</p>
{% endpanel %}

```html
<h1>Dites i refranys</h1>
<h3>Embarbussaments i jocs de paraules</h2>

<p>Els nostres jutges es mengen el fetge del penjat, que es menjaria el fetge dels jutges en el cas de ser despenjat.</p>
<p>Setze jutges d'un jutjat mengen fetges d'un penjat.</p>
<p>Setze jutges d'un jutjat mengen fetge d'un heretge; setze jutges d'un jutjat mengen fetge d'un penjat que encara penja.</p>
```

{% sol %}
```css
h1 {
   font-size: 2em;
   color: beige;
   background-color: blueviolet;
   padding: 2em;
}

h3 {
   color: orangered;
   margin-top: 4em;
   margin-left: 1em;
}

h2+p {
   color: brown;
   background-color: antiquewhite;
   font-size: 1.5em;
   margin-top: 3em;
   margin-left: 1em;
}

p {
   color: darkslateblue;
   background-color: aqua;
   padding: 1em;
   margin-left: 4em;
}
```
{% endsol %}

**2.-** Miquel Martí i Pol

{% panel %}
<h1 style="color: #375e97; font-size: 2em; font-family: Verdana, Geneva, Tahoma, sans-serif border-bottom: 1px solid #375e97;">Miquel Martí i Pol</h1>
<p class="ocupacio" style="color: oldlace; background-color: chocolate; font-weight: bold; font-size: 1.5em; padding: .5em;">Poeta</p>
<p style=" margin: 1em; color: darkgray; font-weight: bold;">Vaig cursar estudis primaris a l'escola parroquial del meu poble. A catorze anys vaig començar a treballar al despatx d'una fàbrica de filatura de cotó. Hi vaig treballar prop de trenta anys, fins que el 1973 una esclerosi múltiple em va obligar a deixar la feina i em va convertir en un pensionista de gran invalidesa. Abans, als dinou anys, havia patit una greu tuberculosi pulmonar. Casat dues vegades, tinc dos fills del primer matrimoni.</p>

<h3 style="color: #375e97; font-family: Verdana, Geneva, Tahoma, sans-serif;">Informació de contacte</h3>
<ul>
   <li style="color: azure; background-color: deepskyblue; font-weight: bolder; list-style-type: none; margin: .3em 3em; padding: .3em">Correu: <a  style="margin-left: 2em;" href="mailto:miquel@gmail.com">miquel@gmail.com</a></li>
   <li style="color: azure; background-color: deepskyblue; font-weight: bolder; list-style-type: none; margin: .3em 3em; padding: .3em">Web: <a style="margin-left: 2em;" href="https://miquelmartiipol.cat">https://miquelmartiipol.cat</a></li>
   <li style="color: azure; background-color: deepskyblue; font-weight: bolder; list-style-type: none; margin: .3em 3em; padding: .3em">Telefon: 93 850 03 55</li>
</ul>
{% endpanel %}


{% sol %}
```css
h1 {
   color: #375e97;
   font-size: 2em;
   font-family: Verdana, Geneva, Tahoma, sans-serif;
   border-bottom: 1px solid #375e97;
}

h3 {
   color: #375e97;
   font-family: Verdana, Geneva, Tahoma, sans-serif;
}

p {
   margin: 1em;
   color: darkgray;
   font-weight: bold;
}

p.ocupacio {
   color: oldlace;
   background-color: chocolate;
   font-weight: bold;
   font-size: 1.5em;
   padding: .5em;
}

li {
   color: azure;
   background-color: deepskyblue;
   font-weight: bolder;
   list-style-type: none;
   margin: .3em 3em;
   padding: .3em
}

a {
   margin-left: 2em;
}
```
{% endsol %}

## TODO

* <https://docs.google.com/document/d/1cg7TEJT_S31-sDYz78PTI6znyvOOiot559ffBcXpF2E>
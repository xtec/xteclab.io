---
title: Box
description: En HTML tot són capses (box) dins de capses vistes desde dalt.
---

## Introducció

Un document HTML es visualitza com capses dins de capses, on **tots** els elements són capses:

{% image "boxes.jpg" %}

No importa com és vegi un element: **tots els elements** ocupen l'espai d’un rectangle:

{% panel %}
<div class="border border-5 m-5">
    <ul class="border border-5 border-danger">
        <li class="border border-5 border-warning">Dilluns</li>
        <li class="border border-5 border-warning">Dimars</li>
        <li class="border border-5 border-warning">Dimecres</li>
    </ul>
</div>
{% endpanel %}

Pots veure el codi si vols:

{% sol %}
```html
<div class="border border-5 m-5">
    <ul class="border border-5 border-danger">
        <li class="border border-5 border-warning">Dilluns</li>
        <li class="border border-5 border-warning">Dimars</li>
        <li class="border border-5 border-warning">Dimecres</li>
    </ul>
</div>
```
{% endsol %}

## Display

Tots els elements tenen una propietat `display`.

La majoria dels elements tenen un valor per defecte per a la propietat `display`, que són `block` o `inline`.

Per exemple, per defecte la propietat `display` de l'element `<p>` és `block`.

Si afegeixes un element `<p>` dins de l'element `<body>`, l'element `<p>` ocupa tot l'espai **horitzontal** disponible encara que no li faci falta:

{% panel %}
<p class="border border-5">Hola!</p>
{% endpanel %}

Això vol dir que si afegeixo una altre element `<p>` aquest es colocarà abaix:

{% panel %}
<p class="border border-5">Hola!</p>
<p class="border border-5">Adeu!</p>
{% endpanel %}

Però, per defecte, vol dir que puc canviar la propietat `display` d'un element.

Boostrap té uns selectors per fer-ho: [Display Property](https://getbootstrap.com/docs/5.0/utilities/display/)

Per exemple, puc fer que les `<p>`s es disposin en línea (`inline`) amb `d-inline`:

{% panel %}
<p class="d-inline border border-5">Hola!</p>
<p class="d-inline border border-5">Adeu!</p>
{% endpanel %}

```html
<p class="d-inline border border-5">Hola!</p>
<p class="d-inline border border-5">Adeu!</p>
```

Pots veure que un element `inline` ocupa **només** l'espai horitzontal que necessita.

Per tant deixa espai horitzontal a l'altre element.

Però que passa si la segona `<p>` no vol ser `inline`?

{% panel %}
<p class="d-inline border border-5">Hola!</p>
<p class="border border-5">Adeu!</p>
{% endpanel %}

Doncs es coloca a sota per poder ocupar tot un espai horitzontal només per ella.

Als navegadors els hi és indiferent si és una `<p>` o un `<strong>`.

Per ells tots són capses que es coloquen una al costat de l'altre, o a sota de l'altre, dins d'una altre capsa.

### Activitat

A continuació tens un document HTML:

{% panel %}
<div class="border m-3">
    <p class="border">
        I am a paragraph. Some of the
        <span class="border d-block text-danger">words</span> have been wrapped in a
        <span class="border text-danger">span element</span>.
    </p>
    <ul class="border ">
        <li class="border d-inline">Item One</li>
        <li class="border d-inline">Item Two</li>
        <li>Item Three</li>
    </ul>
    <p class="border d-inline text-primary">I am a paragraph. A short one.</p>
    <p class="border d-inline">I am another paragraph. Also a short one.</p>
</div>
{% endpanel %}

Crea el HTML corresponent modificant el `display` per defecte dels elements corresponents:

{% sol %}
```html
<div class="border m-3">
    <p class="border">
        I am a paragraph. Some of the
        <span class="border d-block text-danger">words</span> have been wrapped in a
        <span class="border text-danger">span element</span>.
    </p>
    <ul class="border ">
        <li class="border d-inline">Item One</li>
        <li class="border d-inline">Item Two</li>
        <li>Item Three</li>
    </ul>
    <p class="border d-inline text-primary">I am a paragraph. A short one.</p>
    <p class="border d-inline">I am another paragraph. Also a short one.</p>
</div>
```
{% endsol %}

## Box

Tot element té 4 propietats que modifiquen com es visualitza la capsa:

{% image "box-model.png" %}

A continuació tens l'explicació:

| Propietat | Descripció |
|-|-|
|  | L'àrea on es mostra el contingut. |
| `border` | La vora de l'element que pot tenir gruix i color. |
| `padding` | L'espai en blanc entre el contingut i el `border`. |
| `margin` | L'espai en blanc entre el `border` i la els marges de la capsa. |

### Border

En aquest enllaç tens la documentació de com definir el `border`d'un element: [Borders](https://getbootstrap.com/docs/5.3/utilities/borders/)

A continuació tens 3 paràgrafs en que s'ha modificat la seva vora:

{% panel %}
<div class="container">
   <p class="m-3 border border-start-0 border-primary border-5 rounded">En un lugar de la mancha</p>
   <p class="m-3 border  border-primary border-2 rounded-circle border-success">de cuyo nombre no me quiero acordar</p>
   <p class="m-3 border border-danger border-3 rounded-pill">vivía el Hidalgo don Quijote de la Mancha</p>
</div>
{% endpanel %}

Escriu el codi HTML corresponent:

{% sol %}
```html
<div class="container">
   <p class="m-3 border border-start-0 border-primary border-5 rounded">En un lugar de la mancha</p>
   <p class="m-3 border  border-primary border-2 rounded-circle border-success">de cuyo nombre no me quiero acordar</p>
   <p class="m-3 border border-danger border-3 rounded-pill">vivía el Hidalgo don Quijote de la Mancha</p>
</div>
```
{% endsol %}

## Spacing

En aquest enllaç tens la documentació de com definir el `margin` i el `padding`d'un element: [Spacing](https://getbootstrap.com/docs/5.3/utilities/spacing/)

A continuació tens 3 paràgrafs en que s'ha modificat la seva vora:

{% panel %}
<div class="container border border-3">
  <h3 class="border border-3">Absència</h3>
  <p class="border border-3 m-5 ps-5">És bo tenir sempre a punt el recurs</p>
  <p class="border border-3 mb-5 pe-5">d'un mot que empleni el buit de tu, per fer-ne</p>
  <p class="border border-3 m-5 p-5">la pertinent cuirassa que em preservi</p>
  <p class="border border-3 me-5 pe-5">del malson de l'enyor i la tristesa.</p>
</div>
{% endpanel %}

Escriu el codi HTML corresponent:

{% sol %}
```html
<div class="container border border-3">
  <h3 class="border border-3">Absència</h3>
  <p class="border border-3 m-5 ps-5">És bo tenir sempre a punt el recurs</p>
  <p class="border border-3 mb-5 pe-5">d'un mot que empleni el buit de tu, per fer-ne</p>
  <p class="border border-3 m-5 p-5">la pertinent cuirassa que em preservi</p>
  <p class="border border-3 me-5 pe-5">del malson de l'enyor i la tristesa.</p>
</div>
```
{% endsol %}

### Firefox

Tant el navegador Firefox com el Chrome et permeten interactuar amb pàgines web amb un perfil de desenvolupador: [Firefox Developer Edition](https://www.mozilla.org/es-ES/firefox/developer/)

Una d'elles és el [Page Inspector](https://firefox-source-docs.mozilla.org/devtools-user/page_inspector/).

Crea aquest document:

```html
<!doctype html>
<html>
<head>
  <title>Firefox</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href=" https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css">
</head>
<body>
  <div class="container">
    <p class="m-5 p-5 border border-5">Hola món</p>
  </div>
  </div>
</body>
</html>
```

Obre'l amb el navegador web Firefox, i obre el menú "Herramientas para desarrolladores" ( o fes servir la combinació de tecles `Ctrl` + `Mayús` + `I`):

{% image "herramientas.png" %}

Amb el punter de Inspector selecciona un element; el menú "Disposición" et mostrarà les propietats del "Modelo de caja"

{% image "inspector.png" %}

Modifica les propietats de l'element `<p>` fent doble clic al tag `<p>`:

{% image "inspector-modifica.png" %}

## Estil

### Background

Document: [Background](https://getbootstrap.com/docs/5.3/utilities/background/)

Amb `bg-*` pots modificar el color de fons d'un element amb un color contextual:

{% panel %}
<div class="container">
  <div class="p-3 mb-2 bg-primary text-white">bg-primary</div>
  <div class="p-3 mb-2 bg-secondary text-white">bg-secondary</div>
  <div class="p-3 mb-2 bg-success text-white">bg-success</div>
  <div class="p-3 mb-2 bg-danger text-white">bg-danger</div>
</div>
{% endpanel %}


```html
<div class="container">
  <div class="p-3 mb-2 bg-primary text-white">bg-primary</div>
  <div class="p-3 mb-2 bg-secondary text-white">bg-secondary</div>
  <div class="p-3 mb-2 bg-success text-white">bg-success</div>
  <div class="p-3 mb-2 bg-danger text-white">bg-danger</div>
</div>
```

#### Activitat

{% panel %}
<div class="container">
    <p class="bg-danger text-white m-5 p-5 text-center border border-5 border-danger-subtle">El servidor ha tingut un problema molt gros !</p>
    <p class="text-center fs-1">🤕</p>
</div>
{% endpanel %}

{% sol %}
```html
<div class="container">
    <p class="bg-danger text-white m-5 p-5 text-center border border-5 border-danger-subtle">El servidor ha tingut un problema molt gros !</p>
    <p class="text-center fs-1">🤕</p>
</div>
```
{% endsol %}

### Sizing

Document: [Sizing](https://getbootstrap.com/docs/5.3/utilities/sizing/)

Ja saps que un elmement "block" ocupa tot l'espai horitzontal que té disponible.

Amb `w-*` pots limitar aquest espai:

{% panel %}
<div class="container">
  <div class="w-25 p-3 border">Width 25%</div>
  <div class="w-50 p-3 border">Width 50%</div>
  <div class="w-75 p-3 border">Width 75%</div>
  <div class="w-100 p-3 border ">Width 100%</div>
  <div class="w-auto p-3 border">Width auto</div>
</div>
{% endpanel %}

```html
<div class="container">
  <div class="w-25 p-3 border">Width 25%</div>
  <div class="w-50 p-3 border">Width 50%</div>
  <div class="w-75 p-3 border">Width 75%</div>
  <div class="w-100 p-3 border ">Width 100%</div>
  <div class="w-auto p-3 border">Width auto</div>
</div>
```

#### Activitat

{% panel %}
<div class="container m-3 p-3 bg-info">
    <div class="w-75 p-3 bg-primary">
        <div class="w-50 p-3 bg-success">
            <div class="w-50 p-3 bg-warning"></div>
        </div>
    </div>
</div>
{% endpanel %}

{% sol %}
```html
<div class="container m-3 p-3 bg-info">
    <div class="w-75 p-3 bg-primary">
        <div class="w-50 p-3 bg-success">
            <div class="w-50 p-3 bg-warning"></div>
        </div>
    </div>
</div>
```
{% endsol %}

#### Opacity

Document: [Opacity](https://getbootstrap.com/docs/5.3/utilities/opacity/)

Amb `opacity-*` pots controlar l'opacitat d'un element:

{% panel %}
<div class="container m-3">
   <div class="opacity-100 m-3 p-3 bg-success text-white">Cervantes</div>
   <div class="opacity-75 m-3 p-3 bg-success text-white">Quevedo</div>
   <div class="opacity-50 m-3 p-3 bg-success text-white">Machado</div>
   <div class="opacity-25 m-3 p-3 bg-success text-white ">Góngora</div>
</div>
{% endpanel %}

```html
<div class="container m-3">
   <div class="opacity-100 m-3 p-3 bg-success text-white">Cervantes</div>
   <div class="opacity-75 m-3 p-3 bg-success text-white">Quevedo</div>
   <div class="opacity-50 m-3 p-3 bg-success text-white">Machado</div>
   <div class="opacity-25 m-3 p-3 bg-success text-white ">Góngora</div>
</div>
```

## Avaluació

**1.-** Parc del Castell de l'Oreneta

{% panel %}
<div class="container m-3">
   <h1 class="m-3 p-2 bg-primary text-white rounded-top">Parc del Castell de l'Oreneta</h1>
   <p class="m-3 p-3 bg-secondary lh-sm text-white">És un gran espai forestal que connecta Barcelona amb la serra de Collserola. Les seves 17 hectàrees són el resultat de la suma de dues finques rurals i de les restes d'un castell, el de l'Oreneta, que dóna el seu nom al parc.</p>
   <p class="m-3 p-3 border border-danger border-4 rounded rounded-4">Es tracta d'una excursió en un bosc dins la ciutat, en el qual la intervenció municipal ha millorat la xarxa de camins, hi ha fet zones de descans i àrees per al joc infantil, la més gran amb taules de ping-pong. Un altre aspecte destacat són les zones de pícnic, especialment la que hi ha a la part més alta del parc. Les taules gaudeixen de l'ombra d'un gran garrofer i s'hi pot contemplar una excepcional vista panoràmica sobre Barcelona, des de Sant Adrià de Besòs fins al Prat de Llobregat. Al costat del parc hi ha les piscines públiques de Can Caralleu, ideals per arrodonir un dia d'estiu.</p>
</div>
{% endpanel %}

{% sol %}
```html
<div class="container m-3">
   <h1 class="m-3 p-2 bg-primary text-white rounded-top">Parc del Castell de l'Oreneta</h1>
   <p class="m-3 p-3 bg-secondary lh-sm text-white">És un gran espai forestal que connecta Barcelona amb la serra de Collserola. Les seves 17 hectàrees són el resultat de la suma de dues finques rurals i de les restes d'un castell, el de l'Oreneta, que dóna el seu nom al parc.</p>
   <p class="m-3 p-3 border border-danger border-4 rounded rounded-4">Es tracta d'una excursió en un bosc dins la ciutat, en el qual la intervenció municipal ha millorat la xarxa de camins, hi ha fet zones de descans i àrees per al joc infantil, la més gran amb taules de ping-pong. Un altre aspecte destacat són les zones de pícnic, especialment la que hi ha a la part més alta del parc. Les taules gaudeixen de l'ombra d'un gran garrofer i s'hi pot contemplar una excepcional vista panoràmica sobre Barcelona, des de Sant Adrià de Besòs fins al Prat de Llobregat. Al costat del parc hi ha les piscines públiques de Can Caralleu, ideals per arrodonir un dia d'estiu.</p>
</div>
```
{% endsol %}

**2.-** Puzzle

No cal que els colors siguin els mateixos !!

{% panel %}
<div class="container m-3 p-3 bg-info">
   <div class="w-75 m-3 p-3 bg-primary">
       <div class="w-25 p-3 bg-success">
           <div class="w-50 p-3 bg-warning"></div>
       </div>
       <div class="w-50 p-3 bg-success">
           <div class="w-75 p-3 bg-warning"></div>
       </div>
       <div class="w-100 p-3 bg-success">
           <div class="w-75 p-3 bg-warning"></div>
       </div>
   </div>
   <div class="w-75 m-3 p-3 bg-primary">
       <div class="w-100 p-3 bg-success">
           <div class="w-100 p-3 bg-warning">
               <div class="w-100 p-3 bg-danger">
                   <div class="w-100 p-3 bg-secondary"></div>
               </div>
           </div>
       </div>
   </div>
</div>
{% endpanel %}

{% sol %}
```html
<div class="container m-3 p-3 bg-info">
   <div class="w-75 m-3 p-3 bg-primary">
       <div class="w-25 p-3 bg-success">
           <div class="w-50 p-3 bg-warning"></div>
       </div>
       <div class="w-50 p-3 bg-success">
           <div class="w-75 p-3 bg-warning"></div>
       </div>
       <div class="w-100 p-3 bg-success">
           <div class="w-75 p-3 bg-warning"></div>
       </div>
   </div>
   <div class="w-75 m-3 p-3 bg-primary">
       <div class="w-100 p-3 bg-success">
           <div class="w-100 p-3 bg-warning">
               <div class="w-100 p-3 bg-danger">
                   <div class="w-100 p-3 bg-secondary"></div>
               </div>
           </div>
       </div>
   </div>
</div>
```
{% endsol %}

**3.-** Capses


{% panel %}
<div class="container border border-3 border-success rounded">
   <h1 class="fs-5 text-center">BOXES</h1>
   <p class="text-center border rounded-circle bg-primary text-white p-1">Margin, padding and border</p>
   <div class="border border-3 border-danger rounded bg-secondary text-white">BOX</div>
   <div class="border border-3 border-danger rounded m-3 p-3 bg-secondary text-white text-center">BOX</div>
   <div class="border border-3 border-danger rounded m-3 bg-secondary text-white">BOX</div>
   <div class="border border-3 border-danger rounded p-3 bg-secondary text-white text-center">BOX</div>
   <div class="border border-3 border-danger rounded p-3 mt-3 bg-secondary text-white">BOX</div>
   <div class="border border-3 border-danger rounded p-3 bg-info text-white text-end">BOX</div>
   <div class="border border-3 border-danger rounded m-3 bg-info text-white">BOX</div>
</div>
{% endpanel %}

{% sol %}
```html
<div class="container border border-3 border-success rounded">
   <h1 class="fs-5 text-center">BOXES</h1>
   <p class="text-center border rounded-circle bg-primary text-white p-1">Margin, padding and border</p>
   <div class="border border-3 border-danger rounded bg-secondary text-white">BOX</div>
   <div class="border border-3 border-danger rounded m-3 p-3 bg-secondary text-white text-center">BOX</div>
   <div class="border border-3 border-danger rounded m-3 bg-secondary text-white">BOX</div>
   <div class="border border-3 border-danger rounded p-3 bg-secondary text-white text-center">BOX</div>
   <div class="border border-3 border-danger rounded p-3 mt-3 bg-secondary text-white">BOX</div>
   <div class="border border-3 border-danger rounded p-3 bg-info text-white text-end">BOX</div>
   <div class="border border-3 border-danger rounded m-3 bg-info text-white">BOX</div>
</div>
```
{% endsol %}

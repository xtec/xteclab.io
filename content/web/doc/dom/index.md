---
title: DOM
description: Un document web pot tenir codi Javascript que s'executa en el navegador
---

## Introducció

Quan un usuari visita una pàgina web, el servidor torna un fitxer HTML al navegador que pot tenir aquest aspecte:

{% image "html-and-dom.png" %}

El que fa el navegador és llegir l'HTML i construrr el Document Object Model (DOM).

El DOM és una representació d'objectes dels elements HTML. Actua com un pont entre el codi i la interfície d'usuari i té una estructura en forma d'arbre amb relacions pares i fills.

{% image "dom-and-ui.png" %}

Pots utilitzar mètodes DOM i JavaScript per escoltar els esdeveniments de l'usuari i [manipular el DOM](https://developer.mozilla.org/docs/Learn/JavaScript/Client-side_web_APIs/Manipulating_documents) seleccionant, afegint, actualitzant i suprimint elements específics a la interfície d'usuari. La manipulació DOM et permet no només orientar-vos a elements específics, sinó també canviar-ne l'estil i el contingut.

Recursos addicionals:

* [Introducció al DOM](https://developer.mozilla.org/docs/Web/API/Document_Object_Model/Introduction)
* [Com veure el DOM a Google Chrome](https://developer.chrome.com/docs/devtools/dom/)
* [Com veure el DOM a Firefox](https://developer.mozilla.org/docs/Tools/Debugger/How_to/Highlight_and_inspect_DOM_nodes)

## Actualitzar la interfície d'usuari

Crea aquest fitxer `index.html`:

```html
<html>
  <body>
    <div></div>
  </body>
</html>
```

A continuació afegeix un indentificador únic a l'element `<div>`:

```html
<html>
  <body>
    <div id="app"></div>
  </body>
</html>
```

Per escriure JavaScript dins del fitxer HTML, afegiu un element `<script>`:

```html
<html>
  <body>
    <div id="app"></div>
  </body>
  <script type="text/javascript"></script>
</html>
```

Ara, dins de l'element `<script>` utilitza el mètode `getElementById` per seleccionar l'element `<div>` mitjançant el seu `id`:

```html
<html>
  <body>
    <div id="app"></div>
  </body>
  <script type="text/javascript">
    const app = document.getElementById('app');
  </script>
</html>
```

Pots continuar utilitzant els mètodes DOM per crear un nou element `<h1>`:

```html
<html>
  <body>
    <div id="app"></div>
  </body>
  <script type="text/javascript">
     
    // Creem un element de tipus text
    const headerContent = document.createTextNode('Develop. Preview. Ship.');

    // Creem un element H1 i afegim l'element de tipus text a l'element H1
    const header = document.createElement('h1');
    header.appendChild(headerContent);
 
    // Seleccionem l'elmenent que té la id 'app' i li afgegim l'element H1 que hem creat
    const app = document.getElementById('app');
    app.appendChild(header);

  </script>
</html>
```

Obre el fitxer amb l'extensió [Live Preview](https://marketplace.visualstudio.com/items?itemName=ms-vscode.live-server):

{% image "javascript.png" %}

Pots veure que el navegador executa el codi Javascript i afegeix una element `<h1>` amb el text 
"Develop. Preview. Ship."

### Activitat

Crea un document HTML amb un script que calcula una suma i afegeix el resultat a un element `<div>` buit:

{% sol %}
{% image "script.png" %}
{% endsol %}

## Biblioteques

El codi JavaScript es pot posar en un fitxer separat i adjuntar-lo a HTML amb l'atribut `src`, de manera que el podem reutilitzar - [Chartist - Getting started](https://gionkunz.github.io/chartist-js/getting-started.html):

```html
<!doctype html>
<html>
<head>
  <title>Chart</title>
  <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
  <script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
</head>
<body>
  <div class="ct-chart ct-perfect-fourth"></div>
  <script>
      var data = {
          labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri'],
          series: [ [5, 2, 4, 2, 0] ]
      };
      new Chartist.Line('.ct-chart', data);
  </script>
</body>
</html>
```

## TODO

* <https://ioc.xtec.cat/materials/FP/Recursos/fp_daw_m06_/web/fp_daw_m06_htmlindex/WebContent/u1/a1/continguts.html>

* <https://nextjs.org/learn/react-foundations/updating-ui-with-javascript>
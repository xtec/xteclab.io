---
title: Enllaç
icon: webdoc-hyperlink.png
description: Un document web pot tenir enllaços a qualsevol recurs accessible mitjançant una URI
---

## Introducció

Un document <span class="fs-5 text-bold">H</span>TML és un **Hyper** document perquè pots afegir enllaços que et permeten vincular el document a quasevol recurs que sigui accessible a Internet.

Potser avui en dia no et sorpren, però als anys 80 del segle passat era una passada!

## URL

Una URL (Uniform Resource Locator) permet localitzar un recurs a Internet de manera uniforme.

Està format per:

* Un protocol, que pot ser `http`, `https`, `file`, etc.
* L'adreça d'un servirdor, que pot ser una IP o un nom de domini que es resolt amb DNS
* Un path absolut dins el lloc web

Per exemple, `https://xtec.dev/web/doc/link/` és la URL d'aquesta pàgina:

| | |
|-|-|
| https | És el protcol: http sobre TLS |
| xtec.dev | Domini que resol a la IP 35.185.44.232 |
| /web/doc/link/ | El path del document |

**Localitzar** vol dir que la URL té la informació per trobar el recurs: en el nostre exemple, el navegador ha de resoldre el nom `xtec.dev` amb una IP.

Tu pots fer el mateix:

```pwsh
>  nslookup.exe xtec.dev
Non-authoritative answer:
Name:    xtec.dev
Addresses:  2600:1901:0:7b8a::
          35.185.44.232
```

**Uniforme** vol dir que una URL es pot fer servir per accedir a un recurs d'Internet (`http://`, `https://`) com a un fitxer del sistema de fitxers (`file://`), per posar un exemple.

## Enllaç

Amb el tag `<a>` pots enllaçar qualsevol recurs: un document HTML, una imatge, una película, etc.

<p class="text-center font-monospace fs-5">&lt;a href="https://www.google.es"&gt;Google&lt;/a&gt;</p>

El tag `<a>` té l'atribut específic `href` on es posa la [URL](https://ca.wikipedia.org/wiki/Localitzador_uniforme_de_recursos) del recurs: `http://..`, `https://..`, `file://..`, etc.

Dins del tag `<a>` es posa el nom del recurs o qualsevol altre cosa (per exemple, una imatge).

El que mostra el navegador és el que hi ha dins el tag `<a>`, no el valor de l'atribut `href`:

* Quan fas clic  a aquest enllaç, <a href="https://www.google.es">Google</a> , el navegador et porta a `https://www.google.es` perquè és el que posa l'atribut `href`.

* Però aquest enllaç, <a href="https://www.rshb.ru/en/index">CaixaBank</a>, encara que posi "Caixabank", no et porta al lloc web de "la Caixa".


### Activitats

**1.-** Crea una pàgina amb un enllaç a [Viquipèdia](https://ca.wikipedia.org/wiki/Portada) de les 4 capitals de provincia de Catalunya:

{% panel "Capitals de Catalunya" %}
    <ul>
        <li><a href="https://ca.wikipedia.org/wiki/Barcelona">Barcelona</a></li>
        <li><a href="https://ca.wikipedia.org/wiki/Girona">Girona</a></li>
        <li><a href="https://ca.wikipedia.org/wiki/Lleida">Girona</a></li>
        <li><a href="https://ca.wikipedia.org/wiki/Tarragona">Tarragona</a></li>
    </ul>
{% endpanel %}

{% sol %}
```html
<ul>
    <li><a href="https://ca.wikipedia.org/wiki/Barcelona">Barcelona</a></li>
    <li><a href="https://ca.wikipedia.org/wiki/Girona">Girona</a></li>
    <li><a href="https://ca.wikipedia.org/wiki/Lleida">Lleida</a></li>
    <li><a href="https://ca.wikipedia.org/wiki/Tarragona">Tarragona</a></li>
</ul>
```
{% endsol %}

**2.-** Crea un enllaç que posi "Web Oficial del FC Barcelona", però que la URL sigui <https://www.realmadrid.com/>:

{% sol %}
```html
<a href="https://www.realmadrid.com/">Web Oficial del FC Barcelona</a>
```
{% endsol %}

**3.-** Si l'enllaç és una pàgina HTML, el navegador "navega" a la pàgina que posa l'atribut `href`.

En altre cas, el que fa és descarregar el recurs.

Quins tipus de recurs vincula aquests enllaços:

{% panel "Capitals de Catalunya" %}
<ul>
   <li><a href="pont-bisbe.jpg">Pont del bisbe</a></li>
</ul>
{% endpanel %}

{% sol %}
Una imatge:
{% image "pont-bisbe.jpg" %}
{% endsol %}

## Imatge

El tag `<img>` s'utilitza per indicar al navegador que el recurs és una imatge **que ha de mostrar dins el document**.

Per exemple, podem incorporar un [Quoca](https://ca.wikipedia.org/wiki/Quoca) en el nostre document:

```html
<img 
  src="https://xtec.dev/web/doc/link/quokka.jpg"
  alt="A quokka" 
  title="The quokka is an Australian marsupial that is similar in size to the domestic cat." 
/>
```

{% image "quokka.jpg" %}

L'unic atribut obligatori és `src` que és la URL on està la imatge.

L'atribut `title` proporciona informació addicional sobre la imatge, i alguns navegador mostre el títol quan l'usuari situa el cursor sobre la imatge.

L'atribut `alt` dóna  una descripció precisa del contingut de la imatge perquè pugui ser entès pel programari lector de pantalla (utilitzat per persones amb discapacitat visual).

Encara que no et pugui semblar important, per a Google i els altres buscadors si.

<p class="fs-1 text-center">😯</p>

Per tant, si vols fer [SEO](https://ca.wikipedia.org/wiki/Optimitzaci%C3%B3_per_a_motors_de_cerca), ja saps que has de fer.


### Figura

Si vols enmacar una imatge ho pots fer amb els tag `<figure>`.

A més de la imatge pots afegir un títol amb l'element `<figcaption>`.

```html
<figure class="figure">
  <img 
    src="sea-otters-holding-hands.jpg"
    class="img-fluid" 
    alt="Fotografia de dues llúdrigues marines surant a l'aigua">
  <figcaption class="figure-caption">
    Les llúdrigues marines s'agafen de la mà quan dormen perquè no s'allunyin les unes de les altres.
  </figcaption>
</figure>
```

Observa que has d'afegir `class="img-fluid"` perquè la imatge s'adapti a l'espai disponible!


<figure class="figure">
  <img
    class="img-fluid" 
    src="sea-otters-holding-hands.jpg" 
    alt="Fotografia de dues llúdrigues marines surant a l'aigua">
  <figcaption class="figure-caption">
    Les llúdrigues marines s'agafen de la mà quan dormen perquè no s'allunyin les unes de les altres.
  </figcaption>
</figure>

Pots tenir més d'una imatge dins de l'element `<figure>` sempre que totes les imatges comparteixin el mateix títol.

## Video

L'element `<video>` incrusta un reproductor multimèdia que admet la reproducció de vídeo al document.

```html
<div class="text-center">
  <video controls width="250">
    <source src="https://xtec.dev/web/doc/link/flower.webm" type="video/webm" />
    <source src="https://xtec.dev/web/doc/link/flower.mp4" type="video/mp4" />
  </video>
</div>
```

<div class="text-center m-5">
  <video controls width="500">
    <source src="flower.webm" type="video/webm" />
    <source src="flower.mp4" type="video/mp4" />
  </video>
</div>

Més informació a [The Video Embed element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/video)

Pots inserir un video de [Youtube](https://www.youtube.com/) amb un `iframe`:

```html
<div class="text-center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/G7L4YzGAvMA?si=h-5MVwv7m_inDqWQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>
```

El codi del video el pots trobar abaix del video on posa **Comparteix** > **Insereix**:

<div class="text-center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/G7L4YzGAvMA?si=h-5MVwv7m_inDqWQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

## Audio

L'element `<audio>` incrusta un reproductor d'audio en el document:

```html
<div class="text-center m-3">
  <figure class="figure">
    <figcaption class="figure-caption">Wolf Howl</figcaption>
    <audio controls src="https://xtec.dev/web/doc/link/wolf-howl.mp3"></audio>
  </figure>
</div>
```

<div class="text-center m-3">
  <figure class="figure">
    <figcaption class="figure-caption">Wolf Howl</figcaption>
    <audio controls src="wolf-howl.mp3"></audio>
  </figure>
</div>


Més informació a [The Embed Audio element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/audio)

## LLoc web

Un lloc web es composa de diferents pàgines relacionades entre elles.

Pert tant, la primera part de la URL (protocol i servidor) és la mateixa per totes les pàgines.

**Les pàgines d'un mateix lloc web es poden enllaçar entre elles només amb el path**

Els navegadors, quan troben un enllaç només amb el path, ja saben que el protocol i el servidor de la pàgina enllaçada és el mateix que el de la pàgina que estan visitant.

### Australia

A continuació construiras un lloc web d'Australia semblant a <https://australia-ecde81.gitlab.io/>.

El codi font està a <https://gitlab.com/xtec/web/australia>

Crea una carpeta nova amb el nom `australia` i obre la carpeta amb {% link "/project/vscode/" %}.

```pwsh
> cd ~
> md australia
> code australia
```

### nav

L'element `<nav>` representa una secció d'una pàgina que té com a objectiu proporcionar enllaços de navegació, ja sigui dins del mateix document o a altres documents.

Crea el fitxer `index.html` amb l'ajuda de [Bootstrap - Nav and tabs](https://getbootstrap.com/docs/5.0/components/navs-tabs/).

{% panel %}
<div class="container">
    <nav class="nav">
       <a class="nav-link active" href="/">Inici</a>
       <a class="nav-link" href="/history.html">Història</a>
       <a class="nav-link" href="/wildlife/">Fauna</a>
    </nav>
    <hr/>

    <p>Austràlia és un estat de l'hemisferi sud que conforma un continent; i que inclou, a més, diverses illes
            petites dels oceans Pacífic i Índic.</p>
</div>
{% endpanel %}

{% sol %}
```html
<!doctype html>
<html>

<head>
    <title>Australia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href=" https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css">
</head>

<body>

    <nav class="nav nav-fill">
        <a class="nav-link active" href="/">Inici</a>
        <a class="nav-link" href="/history.html">Història</a>
        <a class="nav-link" href="/wildlife/">Fauna</a>
    </nav>

    <hr />
    <p>
        Austràlia és un estat de l'hemisferi sud que conforma un continent; i que inclou, a més, diverses illes
        petites dels oceans Pacífic i Índic.</p>
    </div>
</body>

</html>
```
{% endsol %}

Pots veure que el fitxer `index.html` té un enllaç amb `/history.html` i `/wildlife/`.

Si fas clic a l'enllaç "Història" et diu que el fitxer `c:\Users\david\australia\history.html` no existeix.

{% image "file-not-found.png" %}

"Live Preview" utilitza un servidor web "montat" a la carpeta del projecte, en el meu cas `c:\Users\david\australia\`, i des d'aquest punt muntatge resol els paths de la URL.

**Activitat**. Crea el fitxer `/history.html` (amb el menú de navegació corresponent)

{% sol %}

{% endsol %}

Ara ja pots navegar al document `/history.html`.

### index

Desde `/history.html` pots navegar a "Inici" que correspont al path `/`.

Si fas clic pots veure que funciona 🤔, però no hi ha cap fitxer `/.html` 🤨!

El que et mostra és el fitxer `/index.html` 🙄

**Un path a un directori és resol amb el fitxer `index.html` que està dins el directori.**

Ja pots crear el fitxer que correspon al path `/wildlife/`:

{% sol %}

{% panel "/wildlife/index.html" %}
```html
<p>TODO!</p>
```
{% endpanel %}

{% endsol %}


### Activitat

**1.-** Crea els fitxers `kangaroo.html` i `wombat.html`.

## id

A més de l'atribut `class`, un dels altres atributs més utilitzats i que es pot utilitzar en qualsevol element, com és el cas de `class`, és l'atribut `id`.

L'atribut `id` identifica de manera única un element dins d'un document.

Per exemple:

```html
<h1 id="turtle">Turtle</h1>
```

**Però molt important**, els navegador no verifiquen que no hi hagi `id`s duplicades, això és problema teu.

Si vols pots enllaçar a un element específic d'una pàgina afegint l'`id` de l'element al final del path amb `#`.

Per exemple, a continuació tens un element amb `id="turtle"`:

<p id="turtle" class="fs-1 text-center">🐢</p>


Si vols pots enllaçar a aquest element amb aquesta URL: <https://xtec.dev/web/doc/link/#turtle>

Si obres aquest enllaç, et portarà a la mateixa pàgina, però enlloc d'anar al principi, anirà on està la tortuga. 

De totes maneres, si l'enllaç és a la mateixa pàgina, amb la URL `#id` és suficient.

En aquest document hi ha una granota amb `id="frog"`.

Aquest enllaç et porta directament a la granota: <a href="#frog">Granota</a>.

```html
<a href="#frog">Granota</a>
```

### Activitat


```html
<a href="https://getbootstrap.com/docs/5.3/forms/select/#sizing">Forms - Select (Sizing)</a>
```


Per exemple, per enllaçar a la secció transfrom de la pàgina text de Bootstrap has d’escriure d'escriure:

`<a href="https://getbootstrap.com/docs/5.3/utilities/text/#text-transform">Text (Transform)</a>`


## Avaluació

**1.-** Escriu el text amb els enllaços corresponents:

{% panel %}
<div class="container m-3">
   <h1><a href="https://es.wikipedia.org/wiki/Platero_y_yo">Platero y yo</a></h1>

   <p>Platero es pequeño, peludo, suave; tan blando por fuera, que se diría todo de <a href="https://es.wikipedia.org/wiki/Algodón">algodón</a>, que no lleva huesos. Sólo los espejos de <a href="https://es.wikipedia.org/wiki/Azabache">azabache</a> de sus ojos son duros cual dos escarabajos de <a href="https://es.wikipedia.org/wiki/Cristal">cristal</a> negro.</p>
</div>
{% endpanel %}

{% sol %}
```html
<div class="container m-3">
   <h1><a href="https://es.wikipedia.org/wiki/Platero_y_yo">Platero y yo</a></h1>

   <p>Platero es pequeño, peludo, suave; tan blando por fuera, que se diría todo de <a href="https://es.wikipedia.org/wiki/Algodón">algodón</a>, que no lleva huesos. Sólo los espejos de <a href="https://es.wikipedia.org/wiki/Azabache">azabache</a> de sus ojos son duros cual dos escarabajos de <a href="https://es.wikipedia.org/wiki/Cristal">cristal</a> negro.</p>
</div>
```
{% endsol %}

**2.-** Escriu un enllaç a un imatge d'un cangur amb `alt` i `title`:

{% panel %}
<div class="container m-3">
   <img class="img-fluid" src="https://upload.wikimedia.org/wikipedia/commons/2/22/Kangur.rudy.drs.jpg" alt="Cangur" title="Cangur Vermell">
</div>
{% endpanel %}

{% sol %}
```html
<div class="container m-3">
   <img class="img-fluid" src="https://upload.wikimedia.org/wikipedia/commons/2/22/Kangur.rudy.drs.jpg" alt="Cangur" title="Cangur Vermell">
</div>
```
{% endsol %}

**3.-** Escriu un enllaç a un imatge d'un wombat amb `alt`, `title` i `figcaption`:

{% panel %}
<div class="container">
   <figure class="m-3 p-1 border border-3 rounded">
       <img class="img-fluid" src="https://upload.wikimedia.org/wikipedia/commons/1/18/Vombatus_ursinus_-Maria_Island_National_Park.jpg" alt="Wombat" title="Common wombat">
       <figcaption>Wombats are short-legged, muscular quadrupedal marsupials that are native to Australia.</figcaption>
   </figure>
</div>
{% endpanel %}

{% sol %}
```html
<div class="container">
   <figure class="m-3 p-1 border border-3 rounded">
       <img class="img-fluid" src="https://upload.wikimedia.org/wikipedia/commons/1/18/Vombatus_ursinus_-Maria_Island_National_Park.jpg" alt="Wombat" title="Common wombat">
       <figcaption>Wombats are short-legged, muscular quadrupedal marsupials that are native to Australia.</figcaption>
   </figure>
</div>
```
{% endsol %}

**4.-** Escriu el document amb els enllaços interns corresponents:

{% panel %}
<h1 class="fs-2 text-center">Platero y Yo</h1>
<h1 class="fs-4">Capítulos</h1>
<p><ol class="list-group">
   <li class="list-group-item"><a href="#prologuillo">Prologuillo</a></li>
   <li class="list-group-item"><a href="#platero">Platero</a></li>
   <li class="list-group-item"><a href="#mariposas">Mariposas blancas</a></li>
   <li class="list-group-item"><a href="#juegos">Juegos del anochecer</a></li>
</ol></p>
<h2 id="prologuillo" class="fs-4">Prologuillo</h2>
<p> Suele creerse que yo escribí Platero y yo para los niños, que es un libro para niños.</p>
<p>No. En 1913, "La Lectura", que sabía que yo estaba con ese libro, me pidió que adelantase un conjunto de sus páginas más idílicas para su "Biblioteca Juventud" Entonces, alterando la idea momentáneamente, escribí este prólogo:</p>
<h2 id="platero" class="fs-4">Platero</h2>
<p>Platero es pequeño, peludo, suave; tan blando por fuera, que se diría todo de algodón, que no lleva huesos. Sólo los espejos de azabache de sus ojos son duros cual dos escarabajos de cristal negro.</p>
<p>Lo dejo suelto, y se va al prado, y acaricia tibiamente con su hocico, rozándolas apenas, las florecillas rosas, celestes y gualdas.... Lo llamo dulcemente: "¿Platero?", y viene a mí con un trotecillo alegre que parece que se ríe, en no sé qué cascabeleo ideal....</p>
<h2 id="mariposas" class="fs-4">Mariposas blancas</h2>
<p>La noche cae, brumosa ya y morada. Vagas claridades malvas y verdes perduran tras la torre de la iglesia. El camino sube, lleno de sombras, de cansancio y de anhelo. De pronto, un hombre oscuro, con una gorra y un pincho, roja un instante la cara fea por la luz del cigarro, baja a nosotros de una casucha miserable, perdida entre sacas de carbón. Platero se amedrenta.</p>
<h2 id="juegos" class="fs-4">Juegos del anochecer</h2>
<p>Cuando, en el crepúsculo del pueblo, Platero y yo entramos, por la oscuridad morada de la calleja miserable que da al río seco, los niños pobres juegan a asustarse, fingiéndose mendigos.</p>
{% endpanel %}

{% sol %}
```html
<h1 class="fs-2 text-center">Platero y Yo</h1>
<h1 class="fs-4">Capítulos</h1>
<p><ol class="list-group">
   <li class="list-group-item"><a href="#prologuillo">Prologuillo</a></li>
   <li class="list-group-item"><a href="#platero">Platero</a></li>
   <li class="list-group-item"><a href="#mariposas">Mariposas blancas</a></li>
   <li class="list-group-item"><a href="#juegos">Juegos del anochecer</a></li>
</ol></p>
<h2 id="prologuillo" class="fs-4">Prologuillo</h2>
<p> Suele creerse que yo escribí Platero y yo para los niños, que es un libro para niños.</p>
<p>No. En 1913, "La Lectura", que sabía que yo estaba con ese libro, me pidió que adelantase un conjunto de sus páginas más idílicas para su "Biblioteca Juventud" Entonces, alterando la idea momentáneamente, escribí este prólogo:</p>
<h2 id="platero" class="fs-4">Platero</h2>
<p>Platero es pequeño, peludo, suave; tan blando por fuera, que se diría todo de algodón, que no lleva huesos. Sólo los espejos de azabache de sus ojos son duros cual dos escarabajos de cristal negro.</p>
<p>Lo dejo suelto, y se va al prado, y acaricia tibiamente con su hocico, rozándolas apenas, las florecillas rosas, celestes y gualdas.... Lo llamo dulcemente: "¿Platero?", y viene a mí con un trotecillo alegre que parece que se ríe, en no sé qué cascabeleo ideal....</p>
<h2 id="mariposas" class="fs-4">Mariposas blancas</h2>
<p>La noche cae, brumosa ya y morada. Vagas claridades malvas y verdes perduran tras la torre de la iglesia. El camino sube, lleno de sombras, de cansancio y de anhelo. De pronto, un hombre oscuro, con una gorra y un pincho, roja un instante la cara fea por la luz del cigarro, baja a nosotros de una casucha miserable, perdida entre sacas de carbón. Platero se amedrenta.</p>
<h2 id="juegos" class="fs-4">Juegos del anochecer</h2>
<p>Cuando, en el crepúsculo del pueblo, Platero y yo entramos, por la oscuridad morada de la calleja miserable que da al río seco, los niños pobres juegan a asustarse, fingiéndose mendigos.</p>
```
{% endsol %}

**5.-** Crea la pàgina "Ciutat Vella" del lloc web "Barcelona":

{% panel %}
<h1 class="text-center">Citutat Vella</h1>
<div class="container">
   <figure class="m-3 p-1 border border-3 rounded">
       <img class="img-fluid" src="pont-bisbe.jpg" alt="Pont del Bisbe" title="Pont del Bisbe">
       <figcaption class="text-center"><strong>Pont del Bisbe</strong>. Pont de marbre que uneix dos edificis al barri gòtic.
       </figcaption>
   </figure>
</div>
<h2>Museus</h2>
<ul class="list-group">
   <li class="list-group-item"><a href="../museus/muhba.html">MUHBA</a>. Museu d'Història de Barcelona MUHBA
   </li>
   <li class="list-group-item"><a href="../museus/picasso.html">Picasso</a> Museu Picasso de Barcelona</li>
</ul>
{% endpanel %}

## Conclusió

<p id="frog" class="fs-1 text-center">🐸</p>


## TODO

 A <https://pixabay.com/> ...

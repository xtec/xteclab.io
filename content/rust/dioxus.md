---
title: Dioxus
description: Dioxus is a framework for building cross-platform apps that run on web, desktop, and mobile.
---


## Tutorial

#### Windows

With `binstall`:

```pwsh
Set-ExecutionPolicy Unrestricted -Scope Process; iex (iwr "https://raw.githubusercontent.com/cargo-bins/cargo-binstall/main/install-from-binstall-release.ps1").Content
cargo binstall dioxus-cli
```

#### Project

Create a new project (accept default values):

```pwsh
dx new dioxus
```

```pwsh
cd diouxus
dx serve
```

<https://dioxuslabs.com/learn/0.6/guide/>


### Tailwind

Para estilizar el proyecto utilizaremos tailwind.

Instala {% link "/typescript/bun/" %}.

Instala el CLI de Tailwind CSS:

```pwsh
bun install tailwindcss @tailwindcss/cli
```

Inicializa el proyecto css tailwind:

```pwsh
bun tailwindcss init
```

Esto debería crear un archivo `tailwind.config.js` en la raíz del proyecto.

Edita el archivo `tailwind.config.js` para incluir los archivos rust:

```js
module.exports = {
     mode: "all" ,
     content: [
 // incluye todos los archivos rust, html y css en el directorio src
 "./src/**/*.{rs,html,css}" ,
 // incluye todos los archivos html en el directorio de salida (dist)
 "./dist/**/*.html" ,
     ],
     theme: {
         extend: {},
     },
     plugins: [],
 }                                
 ```



## TODO

* <https://github.com/terhechte/Ebou>


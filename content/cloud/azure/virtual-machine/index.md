---
title: Màquina virtual
---

## Introducció

Azure és una opció interessant quan es fa una activiat que necessitauna màquina al núvol amb una IP pública o en un centre de dades en una regió remota.

Si no t'has registrat com estudiant segueix els passos que s'indiquen a [Azure - Estudiant](/cloud/azure/student/).

Instal.la {% link "/tool/box/" %}.

Crear una màquina virtual:

```pwsh
> New-Azure nl
```

Un cop creada et pots connectar a la màquina virtual:

```pwsh
> Connect-Azure nl
```

Recorda d'eliminar la màquina quan ja no et faci falta:

```pwsh
> Remove-Azure nl
```

Al crear una màquina pots escollir el centre de dades on vols crear la màquina.

Crea una màquina a Estats Units:

```pwsh
> New-Azure us -Location westus
```

Tambè pots escollir el tamany de la màquina: 1, 2 o 4 cpus.

El límit és que només pots tenir "4" cpus per `Location`.

Intenta crear una màquina de 4 CPUs a `westus` i verifica que et dona error.

```pwsh
> New-Azure us-big -Location westus -Size 4
...
Error: Code=QuotaExceeded; Message=Operation could not be completed as it results in exceeding approved standardBSFamily Cores quota. Additional details - Deployment Model: Resource Manager, Location: westus, Current Limit: 4, Current Usage: 1, Additional Required: 4, (Minimum) New Limit Required: 5.
```

Elimina la màquina `us` i crea la màquina `us-big`.

També has de tenir en compte que les màquines petites no estan disponibles en tots els centre de dades.

Per exmple, al Japó et donarà error:

```pwsh
> New-Azure jp -Location japanwest
...
Error: Code=SkuNotAvailable; Message=The requested VM size for resource 'Following SKUs have failed for Capacity Restrictions: Standard_B1s' is currently not available in location 'japanwest'.
```

---
title: Estudiante
layout: default.njk
---

## Introducción

Com estudiant pots utilitzar el teu compte de correu educatiru per registrar-te com estudiant en Azure i tindrás un crèdit de 96€ durant un any.


Ves a (https://portal.azure.com) i busca **Education**:

{% image "education.png" %}

Fes clic a **Education** i a continuació en **Suscríbase ahora**:

{% image "subscribirse.png" %}

A continuació **Empezar gratis**:

{% image "empezar.png" %}

Omple les dades que et demanen i verifica el teu estat acadèmic:

{% image "verificar.png" %}

Omple les dades del teu perfil:

{% image "datos.png" %}

Accepta el contracte de client i registrat:

{% image "contrato.png" %}

Ja has completat el registre com estudiant i disposes de 100$:

{% image "registrado.png" %}

### Renovar

¿Puedo volver a obtener Azure for Students para el próximo año?

Sí. Puede renovar la suscripción de Microsoft Azure for Students después de un año. Le enviaremos correos electrónicos recordando que debe renovar justo antes de que pase el año. Para renovarla, solo debe volver a suscribirse a la oferta desde el sitio web de Microsoft Azure for Students.

```
The subscription '833b28f9-565b-4597-ab15-cdd44dxxxxxx' is disabled and therefore marked as read only. You cannot perform any write actions on this subscription until it is re-enabled.
```

(https://learn.microsoft.com/es-es/azure/education-hub/azure-dev-tools-teaching/program-faq)

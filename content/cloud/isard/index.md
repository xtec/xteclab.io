---
title: Isard
description: Isard es una plataforma de virtualización en la nube que proporciona máquinas virtuales a los centros educativos.
---

## Introducción

La plataforma está disponible en la siguiente URL: <https://elmeuescriptori.gestioeducativa.gencat.cat>.

Por defecto, las máquinas virtuales pueden salir a internet, pero no tienen IP pública.

Tienes que acceder con las credenciales que te ha dado tu profesor:

{% image "login.png" %}

## Box

**TODO** Hay que actualizar el sistema de identificación!!

La manera más fácil de gestionar máquinas virtuales es con la herramienta {% link "/tool/box/" %}.

Una vez instalado "box" tienes estos *cmdlets* para gestionar Isard:


* `Connect-Isard`. Per connectar-te a una màquina
* `Get-Isard`. Per veure totes les màquines que tens
* `New-Isard`. Per crear una màquina Ubuntu o Windows
* `Start-Isard`. Per arrencar una màquina
* `Stop-Isard`. Per parar una màquina

L'usuari de les màquines és `isard` i la contrasenya `pirineus`.

Crea una màquina Ubuntu amb el nom `esther`:

```pwsh
> new-isard esther
Inciar Sessió:
Nom d'usuari []: 
...
```

Has d'introduir el teus usuari i contrasenya d'Isard.

Et pots connectar a la màquina amb un [Visor Spice](https://virt-manager.org/):

```pwsh
> connect-isard esther
mars: Engegant la màquina ................. Fet
mars: Obtenint el fitxer de configuració ... Fet.
```

Pots veure com s'obre una finestra nova amb la interfície gràfica de la màquina `esther`:

{% image "virt-viewer.png" %}

Si vols pots crear i connectar-te a la vegada a la màquina (igual que pots fer en {% link "/windows/wsl/" %}):

```pwsh
> connect-isard raquel -new
```

Si vols crear una màquina Windows 11, has he passar el paràmetre `-os windows`.

Crea la màquina `laura` amb sistema operatiu {% link "/windows/" %}:

```pwsh
> new-isard laura -os windows
```

```pwsh
> get-isard

Nom     Estat     IP
---     -----     --
laura   Stopped
esther  Started   10.2.71.92
raquel  Started   10.2.112.92
```

Si vols pots arrancar la màquina `laura` i esperar que estigui arrencada amb el flag `-wait`:

```pwsh
> start-isard laura -wait
mars: Arrencant la màquina ............. Fet
```

Si vols pots consultar l'estat d'una sola màquina:

```pwsh
> get-isard laura

Nom   Estat   IP
---   -----   --
laura Started 10.2.156.92
```

Pots parar la màquina `laura` (també pots utilitzar el flag `-wait` si vols):

```pwsh
> stop-isard laura
```

## Red

Las máquinas virtuales tienen por defecto una interfaz de red **Default**.

Esta interfaz proporciona una dirección dinámica (DHCP) con DNS y pasarela a través de un router NAT.

Tiene conexión a Internet y está aislado de otras máquinas virtuales del sistema.

### Wireguard

Las máquinas virtuales también pueden tener una interfaz Wireguard .

Esta interfaz está vinculada a una red del sistema que asigna una dirección IP fija a la máquina virtual y permite conectarse desde una máquina que está fuera de Isard mediante una **red privada virtual** (VPN).

Crea una máquina Ubuntu 24.04 Desktop en Isard y verifica que la máquina virtual tiene habilitada la interfaz **Wireguard VPN**.

{% image "wireguard.png" %}

Accedeix al menú desplegable de l'usuari i selecciona l'opció **VPN**, que descarregarà el fitxer `isard-vpn.conf` a l'equip amfitrió.

{% image "vpn.png" %}

Recorda d'arrencar la màquina i mirar quina IP té !

{% image "ip.png" %}

#### Linux

Arrenca una màquina virtual Linux amb {% link "/windows/wsl/" %}.

Instal.la i configura wireguard:

```sh
$ sudo apt update && sudo apt install -y wireguard
$ sudo cp /mnt/c/Users/david/Downloads/isard-vpn.conf /etc/wireguard/wg0.conf
$ sudo wg-quick up wg0
```

Añade el servicio Wireguard a systemd:

```sh
> sudo systemctl enable --now wg-quick@wg0.service
```

Verifica que ahora tienes una interfaz `wg0`:

```sh
> ip --brief addr
lo               UNKNOWN        127.0.0.1/8 ::1/128
eth0             UP             172.19.149.106/20 fe80::215:5dff:fe88:a880/64 
wg0              UNKNOWN        10.0.29.109/32
```


Prueba que el tunel funciona haciendo un ping en la máquina virtual.

**¡Importante!** Utiliza la IP de tu máquina, no la de la mía.


```sh
$ ping -c 1 10.2.76.37
PING 10.2.76.37 (10.2.76.37) 56(84) bytes of data.
64 bytes from 10.2.76.37: icmp_seq=1 ttl=63 time=47.4 ms
...
```

Et pots connectar a la màquina d'Isard amb SSH tal com s'explica a {% link "p:/network/ssh/" %}.

Arrenca un servidor nginx a la màquina virtual i surt de la màquina virtual:

```sh
# Isard
$ sudo apt install -y nginx
...
$ exit
Connection to 10.2.76.37 closed.
```

Prova que pots accedir al servidor nginx:

```sh
$ curl 10.2.76.37 | head -n 10
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   612  100   612    0     0   7965      0 --:--:-- --:--:-- --:--:--  8052
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
```

Instal.la Google Chrome tal com s'explica a {% link "/windows/wsl/" %}.

Arrenca Google Chrome dins de la màquina WSL i accedeix des del navegador:

{% image "google-chrome.png" %}


#### Windows

**Important!** Aquesta activitat només es pot fer en un ordinador en que tinguis permisos d'administrador.

En el teu ordinador personal descarrega [WireGuard per a Windows a través d'aquest enllaç](https://download.wireguard.com/windows-client/wireguard-installer.exe).

Obre el programa i importa l'arxiu `isard-vpn.conf` que has descarregat al principi.

{% image "tunnel-import.png" %}

Activa el túnel:

{% image "tunnel-activate.png" %}

Pots veure que el tunel està actiu:

{% image "tunnel-activate.png" %}

**Important!** Fes servir la IP de la teva màquina, no la de la meva.

```pwsh
> ping 10.2.76.37

Pinging 10.2.76.37 with 32 bytes of data:
Reply from 10.2.76.37: bytes=32 time=37ms TTL=63
...
```

Obre un terminal de Powershell i verifica que et pots connectar per ssh amb la màquina virtual:

```pwsh
> ssh isard@10.2.76.37
isard@10.2.76.37's password:
Welcome to Ubuntu 22.04.2 LTS (GNU/Linux 5.19.0-46-generic x86_64)
Last login: Sun May 26 12:38:16 2024 from 10.0.29.109
isard@ubuntu:~$
```

Prova que pots accedir al servidor nginx des del terminal del Powershell:

```pwsh
> curl 10.2.76.37

StatusCode        : 200
StatusDescription : OK
...
```

Prova que pots accedir al servidor nginx des del navegador:

{% image "nginx.png" %}

### Personal

Les interfícies **Personal** et permeten configurar un entorn de xarxa privat per a un conjunt de màquines virtuals on es poden assignar les seves pròpies adreces IP, crear la configuració de la xarxa i controlar l'accés a la xarxa.

#### Ubuntu Server

Crea una màquina **Ubuntu 22.04 Desktop_Isardvdi_v1.1** amb el nom `ubuntu-1`, dos CPUs, 4GB de RAM i una interfície `Personal1`:

{% image "ubuntu-personal-new.png" %}

**Nota** Les imatges server, com la **Ubuntu 22.04 Server_Isardvdi_v1.3**, no funcionen amb una interfície `Wireguard VPN`.

Com que és una màquina client has d'activar el servidor ssh des d'un escriptori.

Inicia la màquina i desde el teu ordinador connecta't a la màquina `ubuntu-1`:

```pwsh
> ssh isard@10.2.127.123
The authenticity of host '10.2.127.123 (10.2.127.123)' can't be established.
...
isard@10.2.127.123's password:
$
```

Observa que la màquina té 3 interfícies:

```sh
$ ip -brief addr
lo               UNKNOWN        127.0.0.1/8 ::1/128
enp1s0           UP             192.168.122.214/22 fe80::cc46:102:1f0f:c495/64
enp2s0           UP             10.2.127.123/16 fe80::3198:1530:28f9:3bff/64
enp3s0           UP
```

* La `enp1s0` és la `Default` i té sortida a internet.
* La `enp2s0` és la `Wireguard VPN`
* La `enp3s0` és la `Personal1`.

Hem de configurar la IP de la interfície `enp3s0` amb la **xarxa privada** que escollim.

En el nostre cas farem servir la `10.1.1.0/24` i el host tindrà la IP `10.1.1.101`.

Com que és un ubuntu farem servir {$ link "/linux/netplan/" %}

```sh
$ sudo nano /etc/netplan/50-personal.yaml
```

El contingut del fitxer `50-personal.yaml` és el que es mostra a continuació:

```yaml
network:
  version: 2
  ethernets:
    enp3s0:
      dhcp4: no
      addresses: [10.1.1.101/24]
```

A continuació ja podem aplicar la nova configuració:

```sh
$ sudo netplan apply
```

I verificar que funciona:

```sh
$ ip --brief addr

lo               UNKNOWN        127.0.0.1/8 ::1/128
enp1s0           UP             192.168.122.214/22 fe80::cc46:102:1f0f:c495/64
enp2s0           UP             10.2.127.123/16 fe80::3198:1530:28f9:3bff/64
enp3s0           UP             10.1.1.101/24 fe80::5054:ff:fe54:ff35/64

$ ping -c 1 10.1.1.101

PING 10.1.1.101 (10.1.1.101) 56(84) bytes of data.
64 bytes from 10.1.1.101: icmp_seq=1 ttl=64 time=0.022 ms

--- 10.1.1.101 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.022/0.022/0.022/0.000 ms
```

Ara fem un try i apply de la configuració:

##### Activitat

1. Crea una màquina **Ubuntu 22.04 Desktop_Isardvdi_v1.1** amb el nom `ubuntu-2`, dos CPUs, 4GB de RAM i una interfície `Personal1`.

2. Configura la interfície de la màquina `ubuntu-2` amb la IP `10.1.1.102`.

3. Verifica que tens connectivitat entre les dos màquines amb la xarxa `10.1.1.0/24`.


#### Windows

Crea una màquina **Windows 10 T3_Isardvdi_v1.4** amb 2 CPUs, 4G de RAM i una interfície **Personal1**.

{% image "windows-personal-new.png" %}

Connecta't a la màquina fent servir un visor web o el remote-viewer i configura la IP de la interfície.

Per configurar les xarxes al buscador podem escriure "connexions"

{% image "personal-windows-connexions.png" %}


## Pendent

* [Viewers](https://isard.gitlab.io/isardvdi-docs/user/viewers/viewers/)  

* [IsardVDI Documentation](https://isard.gitlab.io/isardvdi-docs/index.ca/)

* [Manual usuari](https://hackmd.io/@yoselin/r1FN_UaQ5)

* [Montilivi](https://daw.institutmontilivi.cat/DAW-MP08/isardVDI/isardVDI000/)
---
title: Suricata
---

## Introducció

Suricata és un motor de detecció d'amenaces que ofereiex  capacitats com ara la detecció d’intrusions (IDS – Intrusion Detection System),  la prevenció d'intrusions (IPS – Intrusion Prevention System) i la monitorització de seguretat de xarxa (NSM – Network Security Monitoring).

## Entorn de treball

Crea un màquina virtual Ubuntu 22.04 a [Isard VDI](/tools/isard/).

Baixa el projecte suricata de xtec:

```sh
$ sudo apt install git
$ git clone https://gitlab.com/xtec/suricata
$ cd suricata
```

Instal.la [Ansible](/tools/ansible/):

```sh
$ ./ansible.sh
```

Mira quines inteficies té la màquina virtual:

```sh
$ ip --brief addr

lo               UNKNOWN        127.0.0.1/8 ::1/128 
enp1s0           UP             192.168.123.11/22 fe80::b4a1:54f4:7454:d2fd/64 
enp2s0           UP             10.2.76.37/16 fe80::d18f:22b8:b29b:935c/64 
```

A continuació instal.larem Suricata a la interficie `enp1s0` amb el playbook `suricata.yaml`:

```sh
$ ansible-playbook suricata.yaml --ask-become-pass
```

Per probar Suricata farem servir un fitxer amb regles local enlloc de les més de 30000 regles amb les que ve per defecte la instal.lació.

Carrega la configuració de regles locals:

```sh
./suricata.sh load

Notice: suricata: This is Suricata version 7.0.4 RELEASE running in SYSTEM mode
...
Info: detect: 2 signatures processed. 1 are IP-only rules, 0 are inspecting packet payload, 1 inspect application layer, 0 are decoder event only
Notice: suricata: Configuration provided was successfully loaded. Exiting.
```

Verifica que s’ha creat un fitxer `local.rules` amb dos regles d’exemple:

```sh
$ cat local.rules

alert ssh any any -> any any (msg: "SSH connection found"; flow:to_server, not_established; sid:1000001; rev:1;)
alert icmp any any -> any any (msg: "ICMP Packet found"; sid:1000002; rev:1;)
```

Fes un `ping` a google.es:

```sh
$ ping -c 2 google.es

PING google.es (172.217.18.3) 56(84) bytes of data.
64 bytes from fra24s22-in-f3.1e100.net (172.217.18.3): icmp_seq=1 ttl=118 time=1.04 ms
64 bytes from fra02s19-in-f3.1e100.net (172.217.18.3): icmp_seq=2 ttl=118 time=1.10 ms

--- google.es ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 1.042/1.070/1.099/0.028 ms
```

Verifica que suricata a registra’t el ping

```sh
$ ./suricata.sh log

05/12/2024-20:17:22.622369  [**] [1:1000002:1] ICMP Packet found [**] [Classification: (null)] [Priority: 3] {ICMP} 192.168.123.11:8 -> 172.217.18.3:0
05/12/2024-20:17:22.623347  [**] [1:1000002:1] ICMP Packet found [**] [Classification: (null)] [Priority: 3] {ICMP} 172.217.18.3:0 -> 192.168.123.11:0
```

Pots veure que suricata captura el paquet d’anada i de tornada del ping a google.es (`172.217.18.3`) mijançant la regla `1000002`.

També que encara que has fet dos pings Suricata només registra el primer.

**Activitat**. Verifica que si tornes a fer un altre ping a `google.es` no queda registrat a no ser que el ping es faci a una altra ip.

**Activitat**. Verifica que si fas un ping a `google.com` queda registrat

La màquina virtual té dues interfícies i només hem habilitat suricata a la interficie `enp1s0`.

Verifica que si fem un ping a `facebook.com` a través de la interficie `enp2s0` no queda registrat (tampoc funciona)

```sh
$ ip -brief addr

lo               UNKNOWN        127.0.0.1/8 ::1/128 
enp1s0           UP             192.168.123.11/22 fe80::b4a1:54f4:7454:d2fd/64 
enp2s0           UP             10.2.76.37/16 fe80::d18f:22b8:b29b:935c/64
```

El ping s'ha fet per la interfície `enp1s0` que té la IP `192.168.123.122`.

També pots fer un ping a través de la interfície `enp2s0` (no funciona):

```sh
$ ping -c 2 -I enp2s0 facebook.com
PING facebook.com (157.240.0.35) from 10.2.76.37 enp2s0: 56(84) bytes of data.
From ubuntu (10.2.76.37) icmp_seq=1 Destination Host Unreachable
From ubuntu (10.2.76.37) icmp_seq=2 Destination Host Unreachable

--- facebook.com ping statistics ---
2 packets transmitted, 0 received, +2 errors, 100% packet loss, time 1023ms
pipe 2
```
## Signatures

Una signatura és semblant a una regla `nftables` de [Netfilter](/network/netfilter/).

Una signatura consta de **tres** parts: `action`, `header` y `options`.

Farem servir aquesta regla como exemple:

```
alert icmp any any -> any any (msg: "ICMP Packet found"; sid:1000002; rev:1;
```

### Action

Una regla comença indicant que fer quan la regla fa "match": `pass`, `alert`, `reject` o `drop`.

En el nostre exemple la regla genera una alerta que queda registrada per un anàlisis posterior.

Si hem configurat suricata en **mode IPS** (ho hem fet), podem fer que suricata actui com un firewall bloquejant paquets.

Modifica la regla per tal de que suricata elimini els paquets `icmp` (`drop) i verifica que funciona:

```sh
$ nano local.rules
$ ./suricata.sh load
$ ping google.es

PING google.es (172.217.18.3) 56(84) bytes of data.
^C
--- google.es ping statistics ---
4 packets transmitted, 0 received, 100% packet loss, time 3063ms
```

Pots veure el log:

```sh
$ ./suricata.sh log | tail -n 1

05/12/2024-20:30:53.002917  [Drop] [**] [1:1000002:1] ICMP Packet found [**] [Classification: (null)] [Priority: 3] {ICMP} 192.168.123.11:8 -> 172.217.18.3:0
```

Modifica la regla icmp per tal que suricata deixi d'escanejar el paquet y aquest segueixi el seu fluxe normal amb `pass`.

```sh
$ nano local.rules
$ ./suricata load
```

Borra el log de suricata i verifica que el `ping` a google.es ja no es registra:

```sh
$ ./suricata.sh log reset
$ ping -c 1 google.es > /dev/null
$ ./suricata.sh log
```



Modifica la regla per tal que faci un `reject`: elimina el paquet i torna un paquete TCP "reset".

```sh
$ nano local.rules
$ ./suricata.sh load
```

Verifica que funciona:

```sh
$ ping -c 1 google.es > /dev/null
```


Important documentació, i **revisant!** de [Google Docs - Suricata](https://docs.google.com/document/d/1Gu8BYlajLo_w2VyWimUlbAapM9QiFoeZAz9Cf0IYg50).

## TODO

[silent drops](https://redmine.openinfosecfoundation.org/issues/1865)

### SID

El interval `1000000`-`1999999` està reservat per a regles personalitzades.

EL interval `2200000`-`2299999` està reservat per a regles integrades de Suricata.

Altres interval de SID estan documentats a la pàgina [Emerging Threats SID Allocation ](https://doc.emergingthreats.net/bin/view/Main/SidAllocation).

### JSON

Els logs de suricata es guarden a la carpeta `/var/log/suricata`.

```sh
$ sudo apt install -y jq
$ cat /var/log/suricata/eve.json | jq
```

## Azure

Crea una nova màquina a Azure:

```pwsh
> New-Azure suricata -size 2
> Connect-Azure suricata
```

La màquina Azure només té una interficie `eth0`.

A azure l'usuari `box` no necessita password per fer `sudo`:

```sh
$ ansible-playbook suricata.yaml

TODO connection reset
```

Torna a connectar-te a la màquina virtual:

```pwsh
> Connect-Azure suricata
```

## Referències

* [Understanding Suricata Signatures](https://www.digitalocean.com/community/tutorials/understanding-suricata-signatures)
* [](https://www.criticaldesign.net/post/how-to-setup-a-suricata-ips)
---
title: Aprender
icon: learn.png
description: Conjunto de actividades para aprender informática.
---


{% pages ["/bio/", "/cloud/", "/cluster/", "/data/", "/kotlin/", "/linux/", "/network/", "/project/", "/python/", "/rust/", "/security/", "/typescript/", "/web/", "/windows/"] %}

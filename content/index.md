---
title: Inicio
description: Xtec.dev és un espai per aprendre informàtica format per un conjunt d'activitats d'aprenentatge.
---

#### Formación profesional

{% pages ["/fp/asir/", "/fp/dam/", "/fp/daw/", "/fp/bio/"] %}


#### Actividades

{% pages ["/learn/", "/site/"] %}




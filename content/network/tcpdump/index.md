---
title: Tcpdump
---

## Introducció

Com ja saps una capçalera TCP/IP té tota aquesta informació:

{% image "header.png" %}

[tcpdump](https://es.wikipedia.org/wiki/Tcpdump) és la principal eina d'anàlisi de xarxa del món, combinant potència i simplicitat en una única interfície de línia d'ordres.
L’objectiu principal d’aquesta activitat és aprendre a aïllar el trànsit de diverses maneres, com ara per IP, port, protocol o aplicació per ajudar-te a trobar el que busques.

## Exemples

A continuació veurem alguns exemples.

Crea una màquina server a {% link "/cloud/isard/" %}.

Ha de tenir les interficies "Default", "Wireguard VPN" i "provençana1".

Configura la interficie "provençana1" amb la IP 10.0.0.X/24 on X és el teu número de PC:

```sh
$ sudo ip addr add dev enp3s0 10.0.0.x/24
$ sudo ip link set dev enp3s0 up
```

Modifica el nom del host:

```sh
$ sudo hostnamectl set-hostname box-1
```

Surt i torna a entrar a la màquina.

### Interfície

Pots verificar la màquina té 4 interfícies:

```sh
$ ip --brief addr
lo               UNKNOWN        127.0.0.1/8 ::1/128 
enp1s0           UP             192.168.122.129/22 metric 100 fe80::5054:ff:fe0c:b8a/64 
enp2s0           UP             10.2.39.218/16 metric 100 fe80::5054:ff:fe50:123f/64 
enp3s0           UP             10.0.0.40/24 fe80::5054:ff:fe15:2fa1/64 
```

Si vols veure tot el tràfic que està passant a la interfície `enp1s0`, executa aquesta ordre:

```sh
$ sudo tcpdump -i enp1s0
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on enp1s0, link-type EN10MB (Ethernet), snapshot length 262144 bytes
22:56:19.332171 STP 802.1d, Config, Flags [none], bridge-id 8000.52:54:00:47:b3:69.8018, length 35
22:56:21.316214 STP 802.1d, Config, Flags [none], bridge-id 8000.52:54:00:47:b3:69.8018, length 35
22:56:23.332214 STP 802.1d, Config, Flags [none], bridge-id 8000.52:54:00:47:b3:69.8018, length 35
^C
3 packets captured
3 packets received by filter
0 packets dropped by kernel
```

La interfície `enp1s0` és la que proporciona accés a a internet.

Per lo vist rep paquets [STP](https://ccnadesdecero.es/spanning-tree-protocol-stp-como-funciona/)

Torna a executar un altre cop `tcpdump` filtrant els paquets `STP`, afegint l'opció `-nn` per tal que mostri els ports amb un número (no pas amb el nom que es fa servir per identificar-lo):

```sh
$ sudo tcpdump -nn -i enp1s0 not arp and not llc
```

Obre un altre terminal i fes `ping` a `dns.google`:

```sh
$ ping -c 1 dns.google > /dev/null
```

Pots veure el registre del ping, un paquet d'anada i un de tornada:

```sh
23:12:41.033699 IP 192.168.122.129.52596 > 10.100.1.252.53: 33115+ [1au] A? google.dns. (39)
23:12:41.033805 IP 192.168.122.129.51224 > 10.100.1.252.53: 33082+ [1au] AAAA? google.dns. (39)
23:12:41.038295 IP 10.100.1.252.53 > 192.168.122.129.51224: 33082 NXDomain 0/1/1 (114)
23:12:41.038395 IP 10.100.1.252.53 > 192.168.122.129.52596: 33115 NXDomain 0/1/1 (114)
23:12:41.038901 IP 192.168.122.129.33890 > 10.100.1.252.53: 34062+ [1au] A? google.dns. (39)
23:12:41.039019 IP 192.168.122.129.33714 > 10.100.1.252.53: 39363+ [1au] AAAA? google.dns. (39)
23:12:41.042373 IP 10.100.1.252.53 > 192.168.122.129.33890: 34062 NXDomain 0/1/1 (114)
23:12:41.042406 IP 10.100.1.252.53 > 192.168.122.129.33714: 39363 NXDomain 0/1/1 (114)
```

### Port

Ja saps que per resoldre un domini s’ha de fer una consulta DNS.

I una consulta DNS es fa al port `53/udp` i a vegades al port `53/tcp`.

A continuació direm a tcpdump que monitorizi totes les interfícies, però només els paquets que utilitzen el port 53:

```sh
$ sudo tcpdump -n -i any port 53
```

Des de l'altre terminal:

```sh
$ dig www.facebook.com +short
star-mini.c10r.facebook.com.
157.240.251.35
```

El resultat de tcpdump:

```sh
23:20:04.983569 lo    In  IP 127.0.0.1.47613 > 127.0.0.53.53: 46295+ [1au] A? www.facebook.com. (57)
23:20:04.983918 enp1s0 Out IP 192.168.122.129.54651 > 10.100.1.252.53: 16445+ [1au] A? www.facebook.com. (45)
23:20:04.991463 enp1s0 In  IP 10.100.1.252.53 > 192.168.122.129.54651: 16445 2/0/1 CNAME star-mini.c10r.facebook.com., A 157.240.251.35 (90)
23:20:04.991700 lo    In  IP 127.0.0.53.53 > 127.0.0.1.47613: 46295 2/0/1 CNAME star-mini.c10r.facebook.com., A 157.240.251.35 (90)
```

Pots veure que el **destí** del primer paquet és `127.0.0.53.53`.

Mira el fitxer `/etc/resolv.conf/` i sabras el motiu:

```sh
$ more /etc/resolv.conf  | grep nameserver
nameserver 127.0.0.53
```

També observa que l'**origen** del primer paquet no és el port `53`, sino un port efímer aleatori per sobre del rang ???.

### IP

Fins ara hem vist com podem filtrar per interfície i port.

També podem filtrar per IP:

```sh
$ sudo tcpdump -nn host 1.1.1.1

23:29:32.208290 IP 192.168.122.129 > 1.1.1.1: ICMP echo request, id 1422, seq 1, length 64
23:29:32.209520 IP 1.1.1.1 > 192.168.122.129: ICMP echo reply, id 1422, seq 1, length 64
```

```sh
$ ping -c 1 1.1.1.1 > /dev/null
```
De la mateixa manera que un port pot ser origen o destí, una IP també pot ser origen o destí:

En el primer paquet `1.1.1.1` és el destí, en el segon paquet `1.1.1.1` és l'origen.

Si vols pots ser més precís:

```sh
$ sudo tcpdump -nn src 1.1.1.1

23:31:36.547962 IP 1.1.1.1 > 192.168.122.129: ICMP echo reply, id 1478, seq 1, length 64
```

```sh
$ ping -c 1 1.1.1.1 > /dev/null
```

Només tens el paquet de tornada, el que te origen (`src`) `1.1.1.1`:


### Protocol

També pots filtrar tràfic per protocol.

Per exemple `icmp`:

```sh
$ sudo tcpdump -nn icmp
23:49:03.568296 IP 192.168.122.129 > 1.1.1.1: ICMP echo request, id 1649, seq 1, length 64
23:49:03.569383 IP 1.1.1.1 > 192.168.122.129: ICMP echo reply, id 1649, seq 1, length 64
```

```sh
$ ping -c 1 1.1.1.1 > /dev/null
```

O per `tcp`:

```sh
$ sudo tcpdump -nn tcp
```

```sh
$ ping -c 1 1.1.1.1 > /dev/null
```

Un ping no genera paquets TCP

```sh
$ sudo tcpdump -nn tcp

23:47:31.391490 IP 192.168.122.129.56192 > 157.240.251.35.80: Flags [S], seq 1282762392, win 64240, options [mss 1460,sackOK,TS val 3417673470 ecr 0,nop,wscale 7], length 0
23:47:31.392617 IP 157.240.251.35.80 > 192.168.122.129.56192: Flags [S.], seq 3239409835, ack 1282762393, win 65535, options [mss 1392,sackOK,TS val 703340791 ecr 3417673470,nop,wscale 8], length 0
23:47:31.392662 IP 192.168.122.129.56192 > 157.240.251.35.80: Flags [.], ack 1, win 502, options [nop,nop,TS val 3417673471 ecr 703340791], length 0
23:47:31.392722 IP 192.168.122.129.56192 > 157.240.251.35.80: Flags [P.], seq 1:80, ack 1, win 502, options [nop,nop,TS val 3417673471 ecr 703340791], length 79: HTTP: GET / HTTP/1.1
23:47:31.393735 IP 157.240.251.35.80 > 192.168.122.129.56192: Flags [.], ack 80, win 256, options [nop,nop,TS val 703340792 ecr 3417673471], length 0
23:47:31.393817 IP 157.240.251.35.80 > 192.168.122.129.56192: Flags [P.], seq 1:201, ack 80, win 256, options [nop,nop,TS val 703340792 ecr 3417673471], length 200: HTTP: HTTP/1.1 301 Moved Permanently
23:47:31.393841 IP 192.168.122.129.56192 > 157.240.251.35.80: Flags [.], ack 201, win 501, options [nop,nop,TS val 3417673472 ecr 703340792], length 0
23:47:31.393966 IP 192.168.122.129.56192 > 157.240.251.35.80: Flags [F.], seq 80, ack 201, win 501, options [nop,nop,TS val 3417673473 ecr 703340792], length 0
23:47:31.394977 IP 157.240.251.35.80 > 192.168.122.129.56192: Flags [F.], seq 201, ack 81, win 256, options [nop,nop,TS val 703340793 ecr 3417673473], length 0
23:47:31.394999 IP 192.168.122.129.56192 > 157.240.251.35.80: Flags [.], ack 202, win 501, options [nop,nop,TS val 3417673474 ecr 703340793], length 0

```

```sh
$ curl -s www.facebook.com > /dev/null
```

Pots veure paquets TCP al port 80 (HTTP)

O per `udp` :

```sh
$ sudo tcpdump -nn udp
23:50:18.944284 IP 192.168.122.129.45814 > 10.100.1.252.53: 7722+ [1au] A? www.facebook.com. (45)
23:50:18.944480 IP 192.168.122.129.55128 > 10.100.1.252.53: 21782+ [1au] AAAA? www.facebook.com. (45)
23:50:19.074110 IP 10.100.1.252.53 > 192.168.122.129.55128: 21782 2/0/1 CNAME star-mini.c10r.facebook.com., AAAA 2a03:2880:f176:84:face:b00c:0:25de (102)
23:50:19.087958 IP 10.100.1.252.53 > 192.168.122.129.45814: 7722 2/0/1 CNAME star-mini.c10r.facebook.com., A 157.240.251.35 (90)
```

```sh
$ curl -s www.facebook.com > /dev/null
```

Pots veure paquets UDP de consulta al DNS.

## TODO

Fes tot el que posa aquest document: [Google docs](https://docs.google.com/document/d/1skikHVGAEUtfH-UZ-v8Wz7p8lGJVdBOe8yTAgPqjqms/)

## Activitat

**x.-** 

1. Instal.la [Wireshark](https://www.wireshark.org/) en la màquina WSL:

```sh
$ sudo apt install wireshark
```

2. Baixa un fitxer `pcap`.

3. Analitza el fitxer amb wireshark
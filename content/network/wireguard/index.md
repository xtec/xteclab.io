---
title: Wireguard
descriptipon: Wireguard proporciona confidencialitat i autenticitat entre dos adreçes IP
---


## Introducció

Wireguard és aplicació que permet crear xarxes privades virtuals.

Crea tres màquines virtuals: `box-1`, `box-2` i `box-3` a {% link "/cloud/isard/" %}.

Les màquines han de ser "Ubuntu 24.04 Server" amb 2 CPUs, 4 GBs de RAM i una interficie de l'institut:


{% image "new-server.png" %}

**Conecta't a la màquina `box-1` amb `ssh` !**

<https://isard.gitlab.io/isardvdi-docs/user/private_and_personal_networks/private_and_personal_networks.ca/>

Activa la interfícies `enp3s0` amb una IP fixa:

```sh
box-1$ sudo ip addr add dev enp3s0 10.0.0.101/24
box-1$ sudo ip link set dev enp3s0 up
```

Fes el mateix amb `box-2` (IP 10.0.0.102/24).

Verifica que pots fer `ping` desde `box-1` a `box-2`:

```sh
box-1$ ping 10.0.0.102
PING 10.0.0.102 (10.0.0.102) 56(84) bytes of data.
64 bytes from 10.0.0.102: icmp_seq=1 ttl=64 time=1.06 ms
64 bytes from 10.0.0.102: icmp_seq=2 ttl=64 time=1.09 ms
^C
--- 10.0.0.102 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 1.055/1.090/1.108/0.021 ms
```

El primer pas és instal·lar WireGuard :

```sh
box-1$ sudo apt update && sudo apt install -y wireguard
```

### Interficie

Wireguard s'integra de manera completament transparent amb el reste d’elements del sistema perquè el que fa és crear interfícies virtuals que externament funcionen com qualsevol altre interfície física.

Afegeix una nova interfície mitjançant `ip-link`:

```sh
box-1$ sudo ip link add dev wg0 type wireguard
```

Pots veure que s’ha creat la interfície:

```sh
box1$ ip --brief addr
lo               UNKNOWN        127.0.0.1/8 ::1/128 
enp1s0           UP             192.168.123.21/22 metric 100 fe80::5054:ff:fe6d:dc1b/64 
enp2s0           UP             10.2.104.208/16 metric 100 fe80::5054:ff:fe6f:440e/64 
enp3s0           UP             10.0.0.101/24 fe80::5054:ff:fe28:e931/64 
wg0              DOWN
```

### Xarxa

Una xarxa virtual funciona amb una xarxa IP privada.

Pots utilitzar la que vulguis, l'única condició és que no l’estigui fent servir una altre interfície.

Assigna una adreça IP privada a cada interfície amb `ip-address`:

```sh
box-1$ sudo ip addr add dev wg0 10.10.0.101/24        
box-1$ ip --brief addr
lo               UNKNOWN        127.0.0.1/8 ::1/128 
enp1s0           UP             192.168.123.21/22 metric 100 fe80::5054:ff:fe6d:dc1b/64 
enp2s0           UP             10.2.104.208/16 metric 100 fe80::5054:ff:fe6f:440e/64 
enp3s0           UP             10.0.0.101/24 fe80::5054:ff:fe28:e931/64 
wg0              DOWN           10.10.0.101/24  
```

Activa la interfície amb `ip-link`:

```sh
box-1$ sudo ip link set wg0 up
```

Verifica amb ping que la IP privada funciona:

```sh
box-1$ ping 10.10.0.101
PING 10.10.0.101 (10.10.0.101) 56(84) bytes of data.
64 bytes from 10.10.0.101: icmp_seq=1 ttl=64 time=0.033 ms
64 bytes from 10.10.0.101: icmp_seq=2 ttl=64 time=0.033 ms
^C
--- 10.10.0.101 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1036ms
rtt min/avg/max/mdev = 0.033/0.033/0.033/0.000 ms
```

### Nginx

A continuació prova amb nginx que la interficie `wg0` funciona sense problemes.

```sh
box-1$ sudo apt install -y nginx
box-1$ echo "box-1" | sudo tee /var/www/html/index.html
```

Verifica que le servidor nginx funciona a la IP 10.10.0.101:

```sh
box-1$ curl 10.10.0.101
box-1
```

## Xarxa

Una xarxa té sentit si hi ha més d’un node conectat.

### Identitat

Wireguard fa servir criptografia asimètrica [Curve25519](https://es.wikipedia.org/wiki/Curve25519)

La clau pública permet identificar els diferents nodes de la xarxa virtual.

Amb `wg` pots generar claus privades:

```sh
box-1$ wg genkey
gKwwK1zIljwcuNlWCo0nv4GJ9seARMX8B9h9+7Ph134=
```

Configura la interfície `wg0` una la clau privada:

```sh
box-1$ umask 077
box-1$ wg genkey > key
box-1$ sudo wg set wg0 private-key key
```

Amb `wg` pots veure que la interficie `wg0`:

1. Està configurada amb una clau pública que és el seu identificador.
2. Un port aleatori on està escoltant:

```sh
box-1$ ~$ sudo wg show
interface: wg0
  public key: XDx76G3NJ5vYzyi9SMge1z6lQV8Y+SWnKECidlThAi4=
  private key: (hidden)
  listening port: 54377
```

Com ja saps, wireguard no necessita que li diguis quina és la clau pública, perquè tenint la clau privada pots generar la pública:

```sh
box-1$ wg pubkey < key
XDx76G3NJ5vYzyi9SMge1z6lQV8Y+SWnKECidlThAi4=
```

Amb la comanda `wg showconf` pots saber quina és la clau privada: 

```sh
box-1$ $ sudo wg showconf wg0
[Interface]
ListenPort = 54377
PrivateKey = 6Ekvbq/UJFfspvZJHv4/9CPCVsh12s1uf/ADgHP83W8=
```

### Peer

Una interfície wireguard fa servir `UDP` (no es pot atacar amd DDOS) i és indetectable amb {% link "/network/nmap/" %}.

Una interfície wireguard només respon a un paquet `UDP` si el remitent és un node de confiança.

Un node és de confiança si has registrat la seva clau pública com a un peer.

Per tant, configura la màquina `box-2` amb una interficie wireguard:

```sh
box-2$ ip --brief addr
lo               UNKNOWN        127.0.0.1/8 ::1/128 
enp1s0           UP             192.168.122.110/22 metric 100 fe80::5054:ff:fe10:4957/64 
enp2s0           UP             10.2.145.44/16 metric 100 fe80::5054:ff:fe19:4915/64 
enp3s0           UP             10.0.0.102/24 fe80::5054:ff:fe64:1a66/64 
wg0              UNKNOWN        10.10.0.101/24 
```

La "id" d'un node és la seva clau pública.

Mira quina és la id de la intefície `wg0` de `box-2` i en quin port està escoltant:

```sh
box-2$ sudo wg show wg0
interface: wg0
  public key: YaCFmNYzvB8tl3wotX5Nx3HJMasJ1IQVnYyOncBZ0zs=
  private key: (hidden)
  listening port: 38240
```

Ja pots configura a `box-2` com a peer de `box-1`:

```sh
box-1$ sudo wg set wg0 peer YaCFmNYzvB8tl3wotX5Nx3HJMasJ1IQVnYyOncBZ0zs= allowed-ips 10.10.0.102/32 endpoint 10.0.0.102:38240
```

Fixa't que has de dir a través de quina interfície real (`endpoint`) s'ha de crear la conenxió virtual encriptada. 

Pots veure que la intefície `wg0` accepta connexions des de la IP `10.10.0.102`.

```sh
box-1$ sudo wg show wg0
interface: wg0
  public key: XDx76G3NJ5vYzyi9SMge1z6lQV8Y+SWnKECidlThAi4=
  private key: (hidden)
  listening port: 54377

peer: YaCFmNYzvB8tl3wotX5Nx3HJMasJ1IQVnYyOncBZ0zs=
  endpoint: 10.0.0.102:38240
  allowed ips: 10.10.0.102/32
```

Ara configura `box-1` com a peer de `box-2`:

```sh
box-2$ sudo wg set wg0 peer XDx76G3NJ5vYzyi9SMge1z6lQV8Y+SWnKECidlThAi4= allowed-ips 10.10.0.102/32 endpoint 10.0.0.101:
54377
```

{% image "peers.png" %}

Ja pots fer `ping` de `box-2` a `box-1` a través de la interficie `wg0`:

```sh
$ ping 10.10.0.101
PING 10.10.0.101 (10.10.0.101) 56(84) bytes of data.
64 bytes from 10.10.0.101: icmp_seq=1 ttl=64 time=0.020 ms
64 bytes from 10.10.0.101: icmp_seq=2 ttl=64 time=0.033 ms
^C
--- 10.10.0.101 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2081ms
rtt min/avg/max/mdev = 0.020/0.029/0.034/0.006 ms
```

I al revés, de `box-1` a `box-2` a través de la interficie `wg0`.


L'ordre `wg show` et mostra que hi ha hagut tràfic en la interfície `wg0`:

```sh
box-2$ sudo wg show wg0
interface: wg0
  public key: YaCFmNYzvB8tl3wotX5Nx3HJMasJ1IQVnYyOncBZ0zs=
  private key: (hidden)
  listening port: 38240

peer: XDx76G3NJ5vYzyi9SMge1z6lQV8Y+SWnKECidlThAi4=
  endpoint: 10.0.0.101:54377
  allowed ips: 10.10.0.101/32
  latest handshake: 18 seconds ago
  transfer: 1.39 KiB received, 124 B sent
```

Però com pots estar segur de que el peer està ben configurat?

Aixeca un servidor nginx a `box-1` que respongui amb el seu nom:

```sh
box-1$ sudo apt install -y nginx
box-1$ echo "box-1" | sudo tee /var/www/html/index.html
```

Des de `box-2` verifica que et pots conectar al servidor nginx de `box-1` desde la interficie `enp2s0` i `enp3s0`:

```sh
box-2$ curl 10.0.0.101
box-1
box-2$ curl 10.10.0.101
box-1
```

## Continua


[Google Docs - Wireguard](https://docs.google.com/document/d/1z0dIEIC6iXNY7n0PPS0SYbeGFM2gTeBG5aDFantGNwU)

## Netplan

Generate the private and public keys in the first peer. Run the following commands with administrator privileges:

```sh
wg genkey > private.key
wg pubkey < private.key > public.key
```

Do the same in the second peer:

```sh
wg genkey > private.key
wg pubkey < private.key > public.key
```

Use the following configuration in the `first peer` (replace the keys and IP addresses as needed):

```sh
network:
  tunnels:
    wg0:
      mode: wireguard
      port: 51820
      key: UMjI9WbobURkCDh2RT8SRM5osFI7siiR/sPOuuTIDns=
      addresses:
        - 172.16.0.1/24
      peers:
        - allowed-ips: [172.16.0.0/24]
          endpoint: 10.86.126.56:51820
          keys:
            public: AIm+QeCoC23zInKASmhu6z/3iaT0R2IKraB7WwYB5ms=
```

In the YAML file above, `key` is the first peer’s `private key` and `public` is the second peer’s `public key`. `endpoint` is the second peer IP address.

Use the following configuration in the second peer:

```sh
network:
  tunnels:
    wg0:
      mode: wireguard
      port: 51820
      key: UAmjvLDVuV384OWFJkmI4bG8AIAZAfV7LarshnV3+lc=
      addresses:
        - 172.16.0.2/24
      peers:
        - allowed-ips: [172.16.0.0/24]
          endpoint: 10.86.126.40:51820
          keys:
            public: EdNnZ1/2OJZ9HcScSVcwDVUsctCkKQ/xzjEyd3lZFFs=
```

In the YAML file above, `key` is the second peer’s `private key` and `public` is the first peer’s `public key`. `endpoint` is the `first peer`'s IP address.

**TODO**: <https://netplan.readthedocs.io/en/stable/examples/#how-to-connect-your-home-computer-to-a-cloud-instance-with-a-wireguard-vpn>

## Debug

<https://www.procustodibus.com/blog/2023/05/troubleshooting-wireguard-with-tcpdump/>


sudo tcpdump -niany udp port 54377

## Referències

* [Wireguard Guide](https://github.com/mikeroyal/WireGuard-Guide/blob/main/README.md)
* <https://www.ivpn.net/knowledgebase/linux/linux-autostart-wireguard-in-systemd/>
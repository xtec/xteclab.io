---
title: Nmap
---

## Introducció

Nmap és una eina per a la detecció de serveis d'internet i l'exploració de xarxa.

Quan inicia una exploració, NMap envia paquets a una o més IPs i espera la resposta. Això permet que NMap determini si els sistemes de destinació responen al trànsit de xarxa i en quins ports.

En examinar en detall les característiques del trànsit, NMap també pot endevinar el sistema operatiu de la màquina que està radera de la IP.

La funcionalitat de NMap s'ha ampliat amb més de 550 scripts que s'executen mitjançant el motor de scripts NMap.

Ves en compte quan executes un script que hagis trobat per Internet perquè molts necessiten permisos de root. 

Primer mira exactament que està fent!

### Entorn de treball

Crea una màquina virtual a {% link "/cloud/isard/" %}.

La màquina han de ser "Ubuntu 24.04 Server" amb 8 CPUs, 16 GBs de RAM i una interficie de l'institut.

Activa la interfícies `enp3s0` amb una IP fixa, on `XX` és el número de teu ordinador:

```sh
$ sudo ip addr add dev enp3s0 10.0.0.0XX/24
$ sudo ip link set dev enp3s0 up
```

Instal.la `nmap` :

```sh
$ sudo apt update && sudo apt install -y nmap
```

### Escanejar un host

**Important!** Tingues en compte que quan estem escanejant sistemes reals els teus resultats poden ser diferents perquè les coses canvien.

Per provar amb finalitat educatives, podem escanejar el host `scanme.nmap.org`. 

**Aquest permís només inclou l'escaneig mitjançant Nmap**, sense exploits o atacs de denegació de servei. 

A continuació anem a escanejar **tots els ports TCP reservats** (del 1 a l 1024) del host que hi ha darrera l'adreça web `scanme.nmap.org`:

```sh
$ nmap scanme.nmap.org
Starting Nmap 7.94SVN ( https://nmap.org ) at 2024-11-20 22:38 UTC
Nmap scan report for scanme.nmap.org (45.33.32.156)
Host is up (0.15s latency).
Other addresses for scanme.nmap.org (not scanned): 2600:3c01::f03c:91ff:fe18:bb2f
Not shown: 996 closed tcp ports (conn-refused)
PORT      STATE SERVICE
22/tcp    open  ssh
80/tcp    open  http
9929/tcp  open  nping-echo
31337/tcp open  Elite

Nmap done: 1 IP address (1 host up) scanned in 21.50 seconds
```

EL resultat mostra que:

* L'adreça `scanme.nmap.org` resol a l'adreça `45.33.32.156`.
* El servidor té 4 serveis accessibles als ports `22/tcp`, `80/tcp` `9929/tcp` i `31337/tcp`.
* El nom del servei és orientatiu (en molts casos està equivocat)

Si vols saber perquè tarda tant en obtenir la informació, executa la mateixa comanda amb l'opció `-v` per activar el mode detallat:

```sh
$ nmap -v scanme.nmap.org
Starting Nmap 7.94SVN ( https://nmap.org ) at 2024-11-20 22:39 UTC
Initiating Ping Scan at 22:39
Scanning scanme.nmap.org (45.33.32.156) [2 ports]
Completed Ping Scan at 22:39, 0.15s elapsed (1 total hosts)
Initiating Parallel DNS resolution of 1 host. at 22:39
Completed Parallel DNS resolution of 1 host. at 22:39, 0.00s elapsed
Initiating Connect Scan at 22:39
Scanning scanme.nmap.org (45.33.32.156) [1000 ports]
Discovered open port 22/tcp on 45.33.32.156
Discovered open port 80/tcp on 45.33.32.156
Discovered open port 31337/tcp on 45.33.32.156
Discovered open port 9929/tcp on 45.33.32.156
...
```

El host `scanme.nmap.org` resolt a la `IP 45.33.32.156`

Pots veure que darrera la IP `45.33.32.156` hi ha un servidor ssh al port `22`. 

Pots tractar de connectar-te a veure que passa:

```sh
$ ssh root@45.33.32.156
The authenticity of host '45.33.32.156 (45.33.32.156)' can't be established.
ED25519 key fingerprint is SHA256:FI7R9RbIFL8J0JynWurg3sSFT30ai8Du6MfPJ0wiqvI.
This key is not known by any other names.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '45.33.32.156' (ED25519) to the list of known hosts.
root@45.33.32.156's password:
```

El sistema no és molt segur perquè permet fer un login amb l'usuari root amb contrasenya i no fa servir només claus criptogràfiques.

També té un servei web. Anem a veure que respon.

Instal.la un navegador:

```sh
$ sudo apt install -y nginx
```

I mira que respon el servidor web:

```sh
$ lynx 45.33.32.156
```

Però recorda, una cosa és **explorar una mica que no és ilegal**, i altre cosa és fer exploracions intrusives i exploits que només es poden fer amb autorització.

```sh
$ nmap www.santander.es
Starting Nmap 7.94SVN ( https://nmap.org ) at 2024-11-20 22:48 UTC
Nmap scan report for www.santander.es (194.224.167.58)
Host is up (0.045s latency).
Not shown: 989 filtered tcp ports (no-response)
PORT     STATE SERVICE
21/tcp   open  ftp
80/tcp   open  http
81/tcp   open  hosts2-ns
443/tcp  open  https
1935/tcp open  rtmp
2001/tcp open  dc
2020/tcp open  xinupageserver
8080/tcp open  http-proxy
8085/tcp open  unknown
8088/tcp open  radan-http
8443/tcp open  https-alt

Nmap done: 1 IP address (1 host up) scanned in 4.42 seconds
```

Anem a veure que respon la IP `194.224.167.58` al port `80`

Com que és un servei http farem servir `lynx`:

```sh
$ lynx 194.224.167.58
...
Alert!: Unexpected network read error; connection aborted.
Can't Access `http://194.224.167.58/'
Alert!: Unable to access document.

lynx: Can't access startfile
```

Com que som informàtics farem servir la supereina `curl`, a veure que està passant:

```sh
$ curl -vI http://194.224.167.58
*   Trying 194.224.167.58:80...
* Connected to 194.224.167.58 (194.224.167.58) port 80
> HEAD / HTTP/1.1
> Host: 194.224.167.58
> User-Agent: curl/8.5.0
> Accept: */*
> 
* Recv failure: Connection reset by peer
* Closing connection
curl: (56) Recv failure: Connection reset by peer
```

Mirar que l’adreça web està ben resolta amb `nslookup`:

```sh
$ nslookup www.santander.es
...

Non-authoritative answer:
Name:   www.santander.es
Address: 194.224.167.58
```

Com que el host també té un servei https al port `443` anem a provar aquest servei amb l’eina `curl`:

```sh
$ curl https://194.224.167.58
curl: (60) SSL: no alternative certificate subject name matches target host name '194.224.167.58'
More details here: https://curl.se/docs/sslcerts.html

curl failed to verify the legitimacy of the server and therefore could not
establish a secure connection to it. To learn more about this situation and
how to fix it, please visit the web page mentioned above.
```

El servei respon que no té un certifcat per la IP `192.224.167.58`.

Podem dir a `curl` de que res, que tiri endavant amb l’opció `-k`:

```sh
$ curl -k  https://194.224.167.58
curl: (56) Recv failure: Connection reset by peer
```

Ara l'error és en el protocol {% link "/network/tls/" %}

Potser és millor dir les les coses pel seu nom, en aquest cas `www.santander.es`:

```sh
$ curl -s https://www.santander.es | head -n 10
<!DOCTYPE html>
<html xml:lang="es" lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Ayuntamiento Santander" />
    <meta name="Generator" content="Drupal 7 (http://drupal.org)" />
<link rel="alternate" type="application/rss+xml" title="Portal Ayuntamiento Santander RSS" href="//www.santander.es/rss.xml" />
<link rel="shortcut icon" href="//www.santander.es/sites/default/themes/custom/ayuntamiento/favicon.ico" type="image/vnd.microsoft.icon" />
```

El lloc web és de l’ajuntament de Santander.

Al final es tractava de dir les coses pel seu nom.

Si faig un `curl` al port 80 amb el nom correcte, llavors si que em respon amb un resposta `302 Moved Temporarily` al servei https:

```sh
$ curl -v http://www.santander.es
...
< HTTP/1.0 302 Moved Temporarily
< Location: https://www.santander.es/
...
```

Encara queden molts serveis per descobrir, explorar i aprendre en aquestes IPs, però ara no tenim temps.

Per cert, com alumne quan un docent et demana que escanejis un host i només ensenyes la sortida de `nmap`, ja saps quina avaluació et farà de la teva tasca.

### Escanejar una xarxa

En l’exemple anterior hem escanejat el host `192.224.167.58` en que està la web de l’ajuntament de Santander, però això no vol dir que el host sigui de l’ajuntament de Santander.

És molt freqüent que darrera un host hi hagi molts dominis d’Internet.

```sh
$ dig -x 194.224.167.58
...
;; AUTHORITY SECTION:
167.224.194.in-addr.arpa. 3600  IN      SOA     minerva.ttd.net. hostmaster.ttd.net. 2023041710 86400 7200 2592000 172800
...
```

Si busques a google "minerva hostmaster" apareixerà una web d’un proveïdor de hosting: <https://www.minervahosting.com/>

Això és molt important, perquè quan estas escanejant un host, a qui estas afectant és al propietari del equips i la xarxa, en aquest cas segurament serà Minerva Hosting i no pas l’ajutament de Santander.

Mentres ens mantiguem dins dels límits podem seguir treballant, i en aquest cas anirem al límit del que es considera permés.

En aquest exemple llançem una exploració `SYN` furtiva contra cada host que es troba en rang `/30`, les 4 adreces contigues on resideix el host `www.santander.es`.

També intentem determinar quin sistema operatiu s'està executant a cada host que està en funcionament. 

Això requereix privilegis de `root` perquè `nmap` manipula els paquets `SYN` per poder detectar el sistema operatiu.

```sh
$ sudo nmap -sS -O www.santander.es/30
Starting Nmap 7.94SVN ( https://nmap.org ) at 2024-11-21 07:32 UTC

Nmap scan report for 194.224.167.56
...
Aggressive OS guesses: Microsoft Windows Server 2022 (92%), Microsoft Windows 11 21H2 (85%), Microsoft Windows Server 2016 (85%)

Nmap scan report for www.santander.es (194.224.167.58)
Running (JUST GUESSING): Microsoft Windows 2016|2012|2022 (92%)
OS CPE: cpe:/o:microsoft:windows_server_2016 cpe:/o:microsoft:windows_server_2012:r2
%)
No exact OS matches for host (test conditions non-ideal).

Nmap scan report for 194.224.167.59
Host is up (0.045s latency).
Not shown: 996 filtered tcp ports (no-response)
PORT     STATE  SERVICE
80/tcp   open   http
4443/tcp closed pharos
8080/tcp closed http-proxy
8443/tcp closed https-alt
Device type: general purpose
Running (JUST GUESSING): Linux 4.X|3.X|2.6.X|5.X (96%)
OS CPE: cpe:/o:linux:linux_kernel:4.4 cpe:/o:linux:linux_kernel:3 cpe:/o:linux:linux_kernel:2.6.32 cpe:/o:linux:linux_kernel:5.1
Aggressive OS guesses: Linux 4.4 (96%), Linux 4.0 (94%), Linux 3.11 - 4.1 (92%), Linux 2.6.32 (92%), Linux 2.6.32 or 3.10 (91%), Linux 3.10 - 3.12 (91%), Linux 2.6.32 - 2.6.35 (90%), Linux 2.6.32 - 2.6.39 (90%), Linux 4.9 (89%), Linux 3.13 (89%)
No exact OS matches for host (test conditions non-ideal).
```

Si proves la IP `194.224.167.59` pots veure que també hi ha un servei de l'ajuntament de Santander:

```sh
$ curl 194.224.167.59
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>302 Found</title>
</head><body>
<h1>Found</h1>
<p>The document has moved <a href="https://www.santander.es/ayuntamiento/gobierno-municipal/organigrama-ayto-concejalias/concejalia-servicios/agencia-de">here</a>.</p>
</body></html>
```

{% panel "Precaució" %}
Tingues en compte que estem just al límit del que està permés: el procediment anterior es molt utilitzat per entrar a un host “per la porta del veí”.

Normalment el paquet d'IPs pertanyen a un mateix CPD, i si el host en que vull penetrar està molt ben protegit potser el del costat que estan en la mateixa LAN no.

Per tant, el millor és entrar dins de la LAN per el host més débil i a partir d'aquest host seguir penetrant dins de la xarxa.

Si visc en un edifici la porta d'entrada a l'edifici és compartida amb els altres veïns, i encara que tingui una porta blindada a cas si el veí no la te i compartim terrassa poden entrar pel pìs del veí.

Per això, el que hem fet ara es pot interpretar com el primer pas d'un atac d'intrusió que intenta accedir al host més dèbil.

{% endpanel %}

## Port 

Una IP pot hostetjar diversos serveis d'internet, però cada servei necessita un direcció única.

És per això que **un servei a més d'escoltar en una IP també escolta en un port**.

I és per aquest motiu que l'adreça d'un servei d'Internet es composa de IP:PORT !!

Hi ha un conveni respecte quins ports ha d'utilitzar cada servei que es conneix com “well-know ports”, però només és un conveni: un servei pot escoltar en qualsevol port (sempre que ja no estigui ocupat).

Instal.la {% link "/linux/docker/" %}:

```sh
$ curl -L sh.xtec.dev/docker.sh | sh
```

Arrenca 4 servidors nginx en 4 ports diferents.

```sh 
$ docker run --rm -d -p 80:80 nginx
$ docker run --rm -d -p 81:80 nginx
$ docker run --rm -d -p 82:80 nginx
$ docker run --rm -d -p 83:80 nginx
```

Com que estem en el nostre servidor no tenim restriccions, amb un `nmap` senzill fariem, però anem a tope:

```sh
$ sudo nmap -sS -O 127.0.0.1

Starting Nmap 7.94SVN ( https://nmap.org ) at 2024-11-21 07:47 UTC
Nmap scan report for localhost (127.0.0.1)
Host is up (0.000090s latency).
Not shown: 995 closed tcp ports (reset)
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
81/tcp open  hosts2-ns
82/tcp open  xfer
83/tcp open  mit-ml-dev
Device type: general purpose
Running: Linux 2.6.X
OS CPE: cpe:/o:linux:linux_kernel:2.6.32
OS details: Linux 2.6.32
Network Distance: 0 hops

OS detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 1.37 seconds
```

Pots veure que:

* El nom dels serveis que dona `nmap` pels ports `81`,`82` i `83` no són correctes.
* El nostre kernel no és un `Linux 2.6.X`

En el port `83/tcp` hi ha un servei web:

```sh
$ curl -s 127.0.0.1:81 | head -n 5
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
```

El nostre kernel és més modern:

```sh
$ uname -a
Linux ubuntu-server 6.8.0-49-generic #49-Ubuntu SMP PREEMPT_DYNAMIC Mon Nov  4 02:06:24 UTC 2024 x86_64 x86_64 x86_64 GNU/Linux
```

## Fases

Una exploració nmap es realitza en fases, on cada fase comença on acaba l'altre.

Com pots veure a continuació, `nmap` no només es dedica a escanejar ports.

### Resolució IPs

Nmap escaneja IPs, però per facilitar la tasca als usuaris també accepta noms de host

Per tant, el primer pas és resoldre els hosts mitjançant DNS

Si passes les opcions `-sL -n` , `nmap` imprimirà les IPs objectiu i no realitzarà cap exploració adicional:

```sh$ nmap -sL -n www.caixabank.es
Starting Nmap 7.94SVN ( https://nmap.org ) at 2024-11-21 07:54 UTC
Nmap scan report for www.caixabank.es (172.64.155.160)
Other addresses for www.caixabank.es (not scanned): 104.18.32.96 2606:4700:4400::6812:2060 2606:4700:4400::ac40:9ba0
Nmap done: 1 IP address (0 hosts up) scanned in 0.01 seconds
```

La IP de `www.caixabank.es` és `172.64.155.160`, encara que hi ha altres adreces.

### Descobriment del host

Les exploracions de la xarxa solen començar per descobrir quins objectius de la xarxa estan en línia i, per tant, val la pena investigar-ho més a fons. Aquest procés s'anomena descobriment d'amfitrió o exploració de ping. 

Nmap ofereix moltes tècniques de descobriment d'amfitrions, que van des de peticions ARP ràpides fins a combinacions elaborades de TCP, ICMP, i altres tipus de sondes. 

Aquesta fase s'executa de manera predeterminada, tot i que la pots saltar amb  l'opció `-PN`  si estàs segur que totes les IP de destinació estan actives.

Per exemple, si executo aquest `nmap`:

```sh 
$ nmap 192.68.56.200
Starting Nmap 7.94SVN ( https://nmap.org ) at 2024-11-21 07:57 UTC
Note: Host seems down. If it is really up, but blocking our ping probes, try -Pn
Nmap done: 1 IP address (0 hosts up) scanned in 3.04 seconds
```

La resposta és que el host sembla que no està actiu.

Però si dica a `nmap` que tiri endavant:

```sh
$ nmap -Pn 192.68.56.200
Starting Nmap 7.94SVN ( https://nmap.org ) at 2024-11-21 08:00 UTC
Nmap scan report for 192.68.56.200
Host is up (0.0041s latency).
All 1000 scanned ports on 192.68.56.200 are in ignored states.
Not shown: 997 filtered tcp ports (no-response), 3 filtered tcp ports (host-unreach)

Nmap done: 1 IP address (1 host up) scanned in 92.90 secondss
```

Si només vol conèixer quins host estan en línia i res més, utilitza les opcions `-sP -n`:

```sh
$ nmap -sP -n www.caprabo.es/28

Starting Nmap 7.94SVN ( https://nmap.org ) at 2024-11-21 08:04 UTC
Nmap scan report for 52.16.240.196
Host is up (0.028s latency).
Nmap scan report for 52.16.240.197
Host is up (0.027s latency).
Nmap scan report for 52.16.240.198
Host is up (0.027s latency).
Nmap scan report for 52.16.240.204
Host is up (0.026s latency).
Nmap scan report for 52.16.240.205
Host is up (0.030s latency).
Nmap scan report for 52.16.240.207
Host is up (0.029s latency).
Nmap done: 16 IP addresses (6 hosts up) scanned in 1.37 seconds
```

### Resolució DNS inversa

Un cop Nmap ha determinat quins amfitrions s'han d'escanejar, cerca els noms DNS inversos de tots els amfitrions trobats en línia per l'exploració de ping.

De vegades, el nom d'un amfitrió proporciona pistes sobre la seva funció, i els noms fan que el resultat sigui més llegible que proporcionar només números IP.

Aquest pas es pot saltar amb l'opció `-n` (sense resolució):

```sh
$ nmap 88.87.219.211
Starting Nmap 7.94SVN ( https://nmap.org ) at 2024-11-21 08:05 UTC
Nmap scan report for 211.grupogodo.com (88.87.219.211)
Host is up (0.027s latency).
Not shown: 997 filtered tcp ports (no-response)
PORT    STATE  SERVICE
80/tcp  open   http
113/tcp closed ident
443/tcp open   https

Nmap done: 1 IP address (1 host up) scanned in 4.53 seconds
```

```sh 
$ nmap -n 88.87.219.211
Starting Nmap 7.94SVN ( https://nmap.org ) at 2024-11-21 08:05 UTC
Nmap scan report for 88.87.219.211
Host is up (0.028s latency).
Not shown: 997 filtered tcp ports (no-response)
PORT    STATE  SERVICE
80/tcp  open   http
113/tcp closed ident
443/tcp open   https

Nmap done: 1 IP address (1 host up) scanned in 4.49 seconds
```

### Escaneig de ports

Aquesta és l'operació fonamental de Nmap. 

S'envien sondes i les respostes (o no respostes) a aquestes sondes s'utilitzen per classificar els ports remots en estats com ara `open`, `closed` o `filtered`

L'exploració de ports es realitza de manera predeterminada, tot i que la pots ometre per realitzar algunes de les fases posteriors de traceroute i Nmap Scripting Engine especificant les seves opcions de línia d'ordres particulars (com ara `--traceroute` i `--script`) juntament amb una exploració de ping (`-sP`). 

### Detecció de versions 

Si es troben alguns ports en estat `open`, Nmap pot ser capaç d'intentar determinar quin programari de servidor s'està  executant al sistema remot.

Ho fa enviant una varietat de sondes i fent coincidir les respostes amb una base de dades de milers de signatures de servei conegudes.

La detecció de versions s'activa amb l'opció `-sV`.

```sh
$ nmap -sV www.caixabank.es

Starting Nmap 7.94SVN ( https://nmap.org ) at 2024-11-21 08:08 UTC
Nmap scan report for www.caixabank.es (104.18.32.96)
Host is up (0.0011s latency).
Other addresses for www.caixabank.es (not scanned): 172.64.155.160 2606:4700:4400::6812:2060 2606:4700:4400::ac40:9ba0
Not shown: 996 filtered tcp ports (no-response)
PORT     STATE SERVICE  VERSION
80/tcp   open  http     Cloudflare http proxy
443/tcp  open  ssl/http Cloudflare http proxy
8080/tcp open  http     Cloudflare http proxy
8443/tcp open  ssl/http Cloudflare http proxy

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 19.12 seconds
```

De totes maneres sabem que radera està [Cloudfare](https://www.cloudflare.com/es-es/), que és un dels CDN (“Content Distribute Networks”) més utilitzats.

Quan has fet aquest escaneix l'has fet a Cloudflare, i a qui has molestat és a Cloudflare no a CaixaBank.

CaixaBank pot tenir un CPD propi, que és un dels motius pels quals fa servir Cloudflare.

Per exemple en el cas de BBVA fa servir [Akamai](https://www.akamai.com/es) com a CDN, encara que gran part de la seva infraestructura la té a Google Cloud.

```sh
$ nmap -sV www.bbva.es

Starting Nmap 7.94SVN ( https://nmap.org ) at 2024-11-21 08:10 UTC
Nmap scan report for www.bbva.es (104.75.90.159)
Host is up (0.00093s latency).
rDNS record for 104.75.90.159: a104-75-90-159.deploy.static.akamaitechnologies.com
Not shown: 998 filtered tcp ports (no-response)
PORT    STATE SERVICE  VERSION
80/tcp  open  http     AkamaiGHost (Akamai's HTTP Acceleration/Mirror service)
443/tcp open  ssl/http AkamaiGHost (Akamai's HTTP Acceleration/Mirror service)

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 17.28 seconds
```

En aquest cas la signatura del servei està a la base de dades i pots obtenir informació sobre com funciona aquest servei i les vulnerabilitats que té a  Google o llocs web específics.

Per exemple, pots mirar aquest article: [Akamai vulnerability could have let hackers attack millions of sites](https://www.thestack.technology/akamai-vulnerability-server-side-cache-poisoning-http-header-fun/)

### Detecció del sistema operatiu

Amb l'opció `-O` pots demanar a nmap que detecti el sistema operatiu.

Els diferents sistemes operatius implementen estàndards de xarxa de maneres subtilment diferents. Mesurant aquestes diferències sovint és possible determinar el sistema operatiu que s'executa en un host remot.

Nmap combina les respostes a un conjunt estàndard de sondes amb una base de dades de més de mil respostes conegudes de sistemes operatius.

```sh
$ sudo nmap -O www.uab.cat 
Starting Nmap 7.94SVN ( https://nmap.org ) at 2024-11-21 08:14 UTC
Nmap scan report for www.uab.cat (158.109.120.133)
Host is up (0.043s latency).
rDNS record for 158.109.120.133: www-balan.uab.es
Not shown: 996 filtered tcp ports (no-response)
PORT     STATE  SERVICE
80/tcp   open   http
113/tcp  closed ident
443/tcp  open   https
8008/tcp open   http
OS fingerprint not ideal because: Didn't receive UDP response. Please try again with -sSU
No OS matches for host

OS detection performed. Please report any incorrect results at https://nmap.org/submit/ .
```

Prova de nou amb l'opció `-sSU` tal com et suggreixen.

Prova amb `www.barcelona.cat`.

### Traceroute

Enlloc de traceroute pots utilitzar l'opció `--traceroute` amb `nmap`.

```sh
$ sudo nmap --traceroute www.barcelona.cat
...

TRACEROUTE (using port 80/tcp)
HOP RTT     ADDRESS
1   0.43 ms _gateway (192.168.120.1)
2   0.44 ms 172.31.255.1
3   0.53 ms 140.91.198.126
4   0.95 ms 99.83.69.231
5   1.19 ms 99.83.69.230
6   ... 12
13  0.99 ms server-18-244-18-65.fra56.r.cloudfront.net (18.244.18.65)  

Nmap done: 1 IP address (1 host up) scanned in 7.82 seconds

```

### Scripts

Nmap té un motor d'execució d'scripts (NSE) que permet escriure scripts per executar amb nmap amb el llenguatge Lua.

Els scripts, que pots trobar a Internet (ves en compte) s'xecuten amb l'opció `--script`.

### Sortida 

Finalment Nmap recull tota la informació que ha recopilat i per defecte l'escriu a la pantalla

També pot escriure el resultat en un fitxer per ser processat posteriorment:

```sh
nmap www.barcelona.cat > bcn.txt
```

## Descobrir un host

### List scan

Amb l'opció `-sLp  ens limitem a resoldre els noms dels hosts a adreces IPs, i la resolució inversa DNS:

```sh
$ nmap -sL www.renfe.es
Starting Nmap 7.94SVN ( https://nmap.org ) at 2024-11-21 08:24 UTC
Nmap scan report for www.renfe.es (23.215.22.77)
Other addresses for www.renfe.es (not scanned): 2a02:26f0:1700:188::104a 2a02:26f0:1700:19e::104a
rDNS record for 23.215.22.77: a23-215-22-77.deploy.static.akamaitechnologies.com
Nmap done: 1 IP address (0 hosts up) scanned in 0.04 seconds
```

També podem veure els registres rDNS que estan a las IPs proximes a `www.renfe.es`:

```sh
$ nmap -sL www.renfe.es/30
Starting Nmap 7.94SVN ( https://nmap.org ) at 2024-11-21 08:25 UTC
Nmap scan report for a23-215-22-76.deploy.static.akamaitechnologies.com (23.215.22.76)
Nmap scan report for www.renfe.es (23.215.22.77)
Other addresses for www.renfe.es (not scanned): 2a02:26f0:1700:19e::104a 2a02:26f0:1700:188::104a
rDNS record for 23.215.22.77: a23-215-22-77.deploy.static.akamaitechnologies.com
Nmap scan report for a23-215-22-78.deploy.static.akamaitechnologies.com (23.215.22.78)
Nmap scan report for a23-215-22-79.deploy.static.akamaitechnologies.com (23.215.22.79)
Nmap done: 4 IP addresses (0 hosts up) scanned in 0.05 seconds
```

### Ping scan

Amb l'opció `-sP` ens limitem a detectar host actius sense realitzar un escaneig de ports:

```sh
$ nmap -sP www.eldiario.es/30
Starting Nmap 7.94SVN ( https://nmap.org ) at 2024-11-21 08:26 UTC
Nmap scan report for 185.103.37.72
Host is up (0.027s latency).
Nmap scan report for 185.103.37.73
Host is up (0.027s latency).
Nmap scan report for 185.103.37.74
Host is up (0.027s latency).
Nmap scan report for www.eldiario.es (185.103.37.75)
Host is up (0.030s latency).
Other addresses for www.eldiario.es (not scanned): 185.103.37.76 185.103.37.77 185.57.173.171 185.57.173.172 185.76.11.193 185.103.37.72 185.103.37.73 185.103.37.74 2a00:de40:10:545::8 2a00:de40:10:545::9 2a00:de40:10:545::a 2a00:de40:10:545::b 2a00:de40:10:545::c 2a02:6ea0:c310:2::4 2a00:de40:10:545::4 2a00:de40:10:545::6 2a00:de40:10:545::7
Nmap done: 4 IP addresses (4 hosts up) scanned in 0.10 seconds
```

Això és útil per saber quins són els teus veïns de xarxa:

```sh
$ nmap -sP -T4 www.lwn.net/24 | tail -n 5
Nmap scan report for 173-255-236-253.ip.linodeusercontent.com (173.255.236.253)
Host is up (0.094s latency).
Nmap scan report for 173-255-236-254.ip.linodeusercontent.com (173.255.236.254)
Host is up (0.089s latency).
Nmap done: 256 IP addresses (187 hosts up) scanned in 2.47 seconds
```

L'opció `-T` indica el nivell de rapidesa de l'escàner (de l'1 al 5).

A partir dels noms de domini queda clar que [www.lwn.net](http://www.lwn.net) utilitza el proveïdor <https://www.linode.com>.

Per tant, si algú volgués atacar aquest servei podria intentar entrar en una màquina veïna per realitzar un ataca local ethernet amb eines com [Ettercap](https://www.ettercap-project.org/) o [Dsniff](https://www.monkey.org/~dugsong/dsniff/).

Si l'usuari té privilegis de root, `-sP` envia una solicitud echo ICMP i un paquet TCP ACK al port 80 o solicituts ARP en una xarxa ethernet local.

En cas contrari s'envia un paquet SYN.

### No ping scan

Ping scan permet filtra quins host estan actius, però molts tallafocs fan que els host siguin invisibles.

Pots utilitzar l'opció (`-pn`) per desactivar aquest filtre.

## Docker

Per fer proves amb `nmap`, pots configurar xarxes amb docker que et permeten fer simulacions ràpides i fàcils de crear.

Mira {% link "/linux/docker/compose/" %}

### Una xarxa

Crea el fitxer `docker-compose.yml`:

```docker-compose
services:
    cassandra:
        image: cassandra:3.7
    nginx:
        image: nginx:1.22
    postgres:
        image: postgres:14.5-alpine
        environment:
            - POSTGRES_PASSWORD=password
    redis:
        image: redis:alpine
```

Executa el fitxer:

```sh
$ docker compose -p one up -d
[+] Running 4/4
 ✔ Container one-nginx-1      Started          0.6s 
 ✔ Container one-postgres-1   Started          0.7s 
 ✔ Container one-redis-1      Started          0.5s 
 ✔ Container one-cassandra-1  Started          0.6s
 ```

Fes un scanner de la xarxa privada:

```sh 
$ docker exec -it one-nginx-1 bash
$ more /etc/hosts | grep 172
172.23.0.5      acb5c20e1d4c
```

La IP del host és `172.23.0.5`.

{% sol %}

```sh
$ apt update && apt install -y nmap
$  nmap -p- -sV 172.23.0.5/24
Starting Nmap 7.80 ( https://nmap.org ) at 2024-11-21 22:37 UTC
Nmap scan report for ubuntu-server (172.23.0.1)
Host is up (0.000015s latency).
Not shown: 65533 closed ports
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.5
22/tcp open  ssh     OpenSSH 9.6p1 Ubuntu 3ubuntu13.5 (Ubuntu Linux; protocol 2.0)
MAC Address: 02:42:2C:FF:A4:93 (Unknown)
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Nmap scan report for one-postgres-1.one_default (172.23.0.3)
Host is up (0.000015s latency).
Not shown: 65534 closed ports
PORT     STATE SERVICE    VERSION
5432/tcp open  postgresql PostgreSQL DB 9.6.0 or later
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port5432-TCP:V=7.80%I=7%D=11/21%Time=673FB61B%P=x86_64-pc-linux-gnu%r(S
SF:MBProgNeg,8C,"E\0\0\0\x8bSFATAL\0VFATAL\0C0A000\0Munsupported\x20fronte
SF:nd\x20protocol\x2065363\.19778:\x20server\x20supports\x203\.0\x20to\x20
SF:3\.0\0Fpostmaster\.c\0L2132\0RProcessStartupPacket\0\0");
MAC Address: 02:42:AC:17:00:03 (Unknown)

Nmap scan report for one-cassandra-1.one_default (172.23.0.4)
Host is up (0.000016s latency).
Not shown: 65533 closed ports
PORT     STATE SERVICE          VERSION
7000/tcp open  afs3-fileserver?
9042/tcp open  cassandra-native Apache Cassandra 3.0.0 - 3.9 (native protocol version 3-4)
MAC Address: 02:42:AC:17:00:04 (Unknown)

Nmap scan report for one-redis-1.one_default (172.23.0.5)
Host is up (0.000016s latency).
Not shown: 65534 closed ports
PORT     STATE SERVICE VERSION
6379/tcp open  redis   Redis key-value store 7.4.1
MAC Address: 02:42:AC:17:00:05 (Unknown)

Nmap scan report for bd791afe5841 (172.23.0.2)
Host is up (0.000010s latency).
Not shown: 65534 closed ports
PORT   STATE SERVICE VERSION
80/tcp open  http    nginx 1.22.1

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 256 IP addresses (5 hosts up) scanned in 23.24 seconds
```
{% endsol %}

Explica la topologia:

{% sol %}

Les IPs poden ser diferents, depenent dels contenidors que tinguis en execució. 

| IP | Port | Service | Image |
|-|-|-|-|
| 172.23.0.2 | 6379/tcp | Redis key-value store 7.4.1 | redis:alpine |
| 172.23.0.3 | 80/tcp | nginx 1.22.1 |  nginx:1.22 |
| 172.23.0.4 | 5432/tcp | PostgreSQL DB 9.6.0 or later | postgres:14.5-alpine |
| 172.23.0.5 | 7000/tcp | afs3-fileserver? | cassandra:3.7 |
| 172.23.0.5 | 9042/tcp | Apache Cassandra 3.0.0 - 3.9 (native protocol version 3-4) | cassandra:3.7 |

{% endsol %}

Elimina tots els contenidors:

```sh
$ docker rm -vf $(docker ps -a -q)
```

## Vàries xarxes

Mofifica el fitxer `docker-compose.yml`:

```docker-compose
services:
 apache:
   image: httpd:2.4.55
   networks:
     hub:
 pg_1:
   image: postgres:14.5-alpine
   environment:
     - POSTGRES_PASSWORD=password
   networks:
     hub:
     postgres:
 pg_2:
   image: postgres:14.5-alpine
   environment:
     - POSTGRES_PASSWORD=password
   networks:
     postgres:
 redis_1:
   image: redis:7.0.8-alpine
   networks:
     hub:
     redis:
 redis_2:
   image: redis:7.0.8-alpine
   networks:
     redis:

networks:
 hub:
 postgres:
 redis:
```

Crear els contenidors:

```sh
$ docker compose -p three up -d
```

Escaneja les xarxes i explica la topologia:

{% sol %}

```sh
$ docker exec -it three-pg_1-1 sh
$ apk update && apk add nmap nmap-scripts
$ nmap -sV 172.25.0.4/24
```

| IP | Port | Service | Image |
|-|-|-|-|
| 172.25.0.3 | 80/tcp | Apache httpd 2.4.55 | httpd:2.4.55 |
| 172.25.0.4 | 5432/tcp |  PostgreSQL DB 9.6.0 or later | postgres:14.5-alpine |

{% endsol %}

**Continua** [Google Docs - Nmap](https://docs.google.com/document/d/1G9n8P9-qNCR8mL3f5IxM2VdD7K3zrPcCnaQ34eotkxU/edit?usp=share_link)


## Traceroute

Escaneja a `www.nytimes.com` amb l'opció `--traceroute` i explica la informació que has trobat:

```sh
$  sudo nmap -Pn -T4 --traceroute www.nytimes.com

Nmap scan report for www.nytimes.com (151.101.129.164)
Host is up (0.00093s latency).
Other addresses for www.nytimes.com (not scanned): 151.101.193.164 151.101.1.164 151.101.65.164
Not shown: 998 filtered tcp ports (no-response)
PORT    STATE SERVICE
80/tcp  open  http
443/tcp open  https

TRACEROUTE (using port 80/tcp)
HOP RTT     ADDRESS
1   0.40 ms _gateway (192.168.120.1)
2   0.41 ms 172.31.255.1
3   0.53 ms 140.91.198.71
4   1.10 ms 217.243.179.18
5   ...
6   0.87 ms 217.243.178.99
7   0.88 ms 151.101.129.164

Nmap done: 1 IP address (1 host up) scanned in 62.80 seconds 
```

**Encara que el “New York Times” és un diari de Nova York (Estats Units) el seu servidor està molt aprop.**

Instal.la el paquet `geoip-bin`:

Mira on està la IP:

```sh
$ geoiplookup 151.101.129.164
GeoIP Country Edition: US, United States
```

**Activitat**. Fes un nmap a {% link "/cloud/isard/" %} per saber on està el centre de dades, etc.


## Python

Instal.la {% link "/python/poetry/" %}:

```sh
$ curl -sSL https://install.python-poetry.org | python3 -
```

Surt i torna a obrir un altre shell.

Crea un projecte `scan`, instal.la la dependència i arrenca un entorn virtual:

```sh
$ poetry new scan --name app
$ cd scan
$ poetry shell
$ poetry add python-nmap
```

Crea el fixer `scan.py`:

```py
import nmap

nm = nmap.PortScanner()
result = nm.scan('proven.cat')
print(result)
```

El resultat és un `dict`:

```py
{
    "nmap": {
        "command_line": "nmap -oX - -sV proven.cat",
        "scaninfo": {
            "tcp": {
                "method": "connect",
                "services": "",
            }
        },
        "scanstats": {
            "timestr": "Thu Nov 21 11:44:17 2024",
            "elapsed": "19.22",
            "uphosts": "1",
            "downhosts": "0",
            "totalhosts": "1",
        },
    },
    "scan": {
        "81.42.248.211": {
            "hostnames": [
                {"name": "proven.cat", "type": "user"},
                {"name": "211.red-81-42-248.staticip.rima-tde.net", "type": "PTR"},
            ],
            "addresses": {"ipv4": "81.42.248.211"},
            "vendor": {},
            "status": {"state": "up", "reason": "syn-ack"},
            "tcp": {
                80: {
                    "state": "open",
                    "reason": "syn-ack",
                    "name": "http",
                    "product": "",
                    "version": "",
                    "extrainfo": "",
                    "conf": "10",
                    "cpe": "",
                },
                443: {
                    "state": "open",
                    "reason": "syn-ack",
                    "name": "http",
                    "product": "Apache httpd",
                    "version": "",
                    "extrainfo": "",
                    "conf": "10",
                    "cpe": "cpe:/a:apache:http_server",
                },
            },
        }
    },
}
```

Aquest `dict` el pots processar per obtenir els resultas que tu vols utilitzar.

Per exemple, si vols les IPs:

```py
import nmap

nm = nmap.PortScanner()
result = nm.scan("-p22 proven.cat")
# print result
for host in result["scan"].keys():
    print(host)
```

```py
81.42.248.211
```

O vols les IPs i els ports:

```py
import nmap

nm = nmap.PortScanner()
result = nm.scan("proven.cat")
for host in result["scan"].keys():
    for port in result["scan"][host]["tcp"]:
       portNode = result["scan"][host]["tcp"][port]
       print(port, ": ", portNode['state'], " ", portNode['name'], " ", portNode['product'])
```

```py
80 :  open   http   
443 :  open   http   Apache httpd
```

També pots fer un script que busqui els hosts que tenen `ssh`:

```py
import nmap

nm = nmap.PortScanner()
result = nm.scan("-p22 caixabank.cat/28")
# print result
for host in result["scan"].keys():
    tcp = result["scan"][host]["tcp"]
    if 22 in tcp.keys():
        print(f"ssh root@{host}")
```

### Activitat

Fes un script que a més de localitzar el nodes amb `ssh`, també intenti fer un login amb un usuari i una contrasenya.

Per això has d'utlitzar la llibreria `paramiko` tal com s'explica a {% link "/network/ssh" %}

**Important** Aquest activitat l'has de probar amb les màquines dels altres alumnes que estan conectats a la xarxa de l'Institut.

## Més informació

Segueix llegint -> [Integración de Python con Nmap](https://thehackerway.com/2022/05/19/integracion-de-python-con-nmap/
)
Aquí tens alguns articles que expliquen com fer servir nmap desde Python:

* [Using Nmap Port Scanner with Python](https://www.studytonight.com/network-programming-in-python/integrating-port-scanner-with-nmap)
* [Nmap con Python 3](https://www.pythonparatodo.com/?p=251)
* [Descubriendo la red con Python y Nmap - Parte 1](https://hacking-etico.com/2015/03/06/descubriendo-la-red-con-python-y-nmap-parte-1/)
* [Improve Nmap Performance with These Brilliant Scripts](https://dev.to/baptistsec/improve-nmap-performance-with-these-brilliant-scripts-2kc0)
* [Python Nmap Module Fully Explained with 8 Programs](https://www.pythonpool.com/python-nmap/)


## Script

**PENDENT**

## Activitats

Està en aquest document: [Google Docs - Nmap](https://docs.google.com/document/d/1G9n8P9-qNCR8mL3f5IxM2VdD7K3zrPcCnaQ34eotkxU/edit?usp=share_link)



---
title: Jetbrains
---

## IDEs

{% pages ["/kotlin/idea"] %}

## Llicènciea educativa gratuïta

Com estudiant o professor, pots utilitzar totes les IDEs de JetBrains per **ús personal** tant a l'escola com a casa, tal com s'explica a [Free Educational Licenses](https://www.jetbrains.com/community/education/#students/)


{% panel "Correu de l'Institut" %}

En aquest enllaç estan tots els correus `.cat` que es poden utilitzar: <https://github.com/JetBrains/swot/tree/master/lib/domains/cat>.

Per exemple, estan `@office.proven.cat`, `@xtec.cat`, etc.

Verifica que el correu del teu Institut està a la llista.

{% endpanel %}

Ves a aquest enllaç, i registra't amb el correu de l'Institut: <https://www.jetbrains.com/shop/eform/students>

{% image "form.png" %}

A continuació, rebras un correu a l'adreça de l'Institut:


{% image "email.png" %}

Fes clic a l'enllaç i registra't.
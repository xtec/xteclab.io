---
title: Box
description: Box és una mòdul de PowerShell que et permet gestionar màquines virtuals.
---

![PowerShell Gallery Version](https://img.shields.io/powershellgallery/v/Box)

## Introducció

Box forma part de la Galeria de PowerShell: <https://www.powershellgallery.com/packages/Box>

Si no saps que és PowerShell, en aquest enllaç trobarás tota la informació: [Powershell](/powershell/).

És un projecte que està a <https://gitlab.com/xtec/box>

## Instal.la

Obre una sessió de PowerShell.

Habilita l'execució d'scripts remots:

```pwsh
Set-ExecutionPolicy Remote -Scope CurrentUser
```

Instal.la el mòdul:

```pwsh
Install-Module Box -scope CurrentUser -SkipPublisherCheck
```

### Actualitzar el mòdul

Per actualitzar el mòdul:

```pwsh
Update-Module Box
```

Tanca la sessió i obre una nova sessió.


## Màquines virtuals

{% pages ["p:/windows/wsl/", "p:/cloud/isard/", "p:/cloud/azure/virtual-machine/"] %}

## IDEs

Per a la instal.lació de les IDEs, i de totes les eines necessàries per desenvolupar aplicacions, fem servir {% link "/tool/scoop/" %}.

{% pages ["/project/vscode/", "/kotlin/idea/"] %}




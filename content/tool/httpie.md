---
title: httpie
---

## Introducció

[HTTPie](https://httpie.io/)

Instal.la `httpie` per poder fer les consultes HTTP:

```sh
$ sudo apt install -y httpie
```

## TODO

* [How to Post JSON Data with HTTPie?](https://apidog.com/blog/httpie-post-json/)
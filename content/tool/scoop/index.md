---
title: Scoop
description: Gestor d'aplicacions de Windows.
---

## Introducció

[Scoop.sh](https://scoop.sh/) és un gestor d'aplicacions escrit en {% link "/windows/powershell/" %} que et permet instal.lar aplicacions a nivell d'usuari sense permisos d'administrador.

Està pensant sobretot per informàtics.

Obre un terminal de Powershell i instal.la `scoop`:

```pwsh
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
Invoke-RestMethod -Uri https://get.scoop.sh | Invoke-Expression
```

## Instal.lar una aplicació

Per exemple podem instal.lar `git` en el nostre espai d'usuari:

```pwsh
> scoop install git
Updating Scoop...
Updating Buckets...
...
```

Ja tens la versió portable de [Git](https://git-scm.com/) disponible en el teu espai d'usuari:

```pwsh
> git --version
git version 2.46.0.windows.1
```

Scoop descarrega i gestiona totes les aplicacions d'una manera asèptica sense contaminar el teu espai d'uauari.

Tots els fixes es guarden en la carpeta `~\scoop`:

```pwsh
ls ~\scoop                 

Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----          1/9/2024     16:10                apps
d-----         12/8/2024     17:53                buckets
d-----          1/9/2024     16:11                cache
d-----         15/8/2024     16:34                persist
d-----          1/9/2024     16:11                shims
```

Scoop crea "shims" dins del directori `~\scoop\shims`, que són accessible en el `PATH`, i que et permet executar l'aplicació dese la linia d'ordres.  

## Buscar una aplicació

A la pàgina web de [Scoop.sh](https://scoop.sh/) pots buscar paquets, per exemple {% link "/python/" %}:

{% image "search-python.png" %}

També ho pots fer desde el terminal:

```pwsh
> scoop search python
Results from local buckets...

Name       Version   Source Binaries
----       -------   ------ --------
python     3.12.5    main
winpython  3.12.4.1  main
```

Ja saps que pots instal.lar la versió `3.12.5` de Python:

```pwsh
> scoop install python
Installing 'dark' (3.14) [64bit] from 'main' bucket
...
'python' (3.12.5) was installed successfully!
```

Ja pots executar l'intèrpret de Python:

```pwsh
> python
Python 3.12.5 (tags/v3.12.5:ff3bc82, Aug  6 2024, 20:45:27) [MSC v.1940 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> exit()
> 
```

## Bucket

El paquets d'scoop exiteixen com a part de repositoris Git, anomentats buckets.

Per exemple, [Netbeans](https://netbeans.apache.org/front/main/index.html) forma part del bucket "extras".

Primer has d'instal.lar el bucket "extras":

```pwsh
> scoop bucket add extras
```

Ja pots instal.lar netbeans:

```pwsh
> scoop install extras/netbeans
...
'netbeans' (22) was installed successfully!
'netbeans' suggests installing 'java/openjdk11'.
'netbeans' suggests installing 'java/openjdk17'.
```

A més d'instal.lar `netbeans`, et recomanen també instal.lar `java`:

```pwsh
> scoop bucket add java
> scoop install java/openjdk21
```

Ja pots executar `netbeans` des la linia d'ordres:

{% image "netbeans.png" %}

També ho pots fer des de la icona:

{% image "icons.png" %}

## Manteniment

Pots eliminar un paquet, per exemple `python`:

```pwsh
> scoop uninstall python
Uninstalling 'python' (3.12.5).
...
'python' was uninstalled.
```

O actualitzar tots els paquets a la vegada:

```pwsh
scoop update --all 
idea: 2024.2-242.20224.300 -> 2024.2.1-242.21829.142
nodejs-lts: 20.16.0 -> 20.17.0
pwsh: 7.4.4 -> 7.4.5
pycharm: 2024.2-242.20224.347 -> 2024.2.1-242.21829.153
vscode: 1.92.1 -> 1.92.2
Updating 5 outdated apps:
Updating 'idea' (2024.2-242.20224.300 -> 2024.2.1-242.21829.142)
Downloading new version
ideaIC-2024.2.1.win.zip (858,6 MB) [========================================
...
```

En el teus cas segurament tot està al dia! 

## Ajuda

Suposo que ja tens una primera idea de que és scoop i de com funciona.

😊 A partir d'aquí ja pots continuar tu sol!

Amb la subordre `help` tens totes les opcions disponibles:

```pwsh
> scoop help
Usage: scoop <command> [<args>]

Available commands are listed below.

Type 'scoop help <command>' to get more help for a specific command.

Command    Summary
-------    -------
alias      Manage scoop aliases
bucket     Manage Scoop buckets
cache      Show or clear the download cache
cat        Show content of specified manifest.
checkup    Check for potential problems
cleanup    Cleanup apps by removing old versions
config     Get or set configuration values
create     Create a custom app manifest
depends    List dependencies for an app, in the order they'll be installed
download   Download apps in the cache folder and verify hashes
export     Exports installed apps, buckets (and optionally configs) in JSON format
help       Show help for a command
hold       Hold an app to disable updates
home       Opens the app homepage
import     Imports apps, buckets and configs from a Scoopfile in JSON format
info       Display information about an app
install    Install apps
list       List installed apps
prefix     Returns the path to the specified app
reset      Reset an app to resolve conflicts
search     Search available apps
shim       Manipulate Scoop shims
status     Show status and check for new app versions
unhold     Unhold an app to enable updates
uninstall  Uninstall an app
update     Update apps, or Scoop itself
virustotal Look for app's hash or url on virustotal.com
which      Locate a shim/executable (similar to 'which' on Linux)
```

🌐 I sempre pots buscar per Internet ara que ja tens la formació inicial necessària. 

## Activitat

**1.-** A continuació tens una llista de les les IDEs que es fan servir més en entorn professional.

Instal.la cada una amb scoop:

* [IDEA](https://www.jetbrains.com/es-es/idea/) per Java, {% link "/kotlin/" %}, etc
* [PyCharm](https://www.jetbrains.com/pycharm/) per {% link "/python/" %}
* [Android Studio](https://developer.android.com/studio?hl=es-419) per Android
* [VS Code](https://code.visualstudio.com/) per {% link "/typescript/" %} i (Python, PHP, etc)

{% sol %}
PENDENT
{% endsol %}

**2.-** A continuació tens una llista dels entorns més utilitzats en windows.

Instal.la cada una amb scoop:

* [.NET](https://dotnet.microsoft.com/es-es/)
* Flutter
* Java
* Javascript
* Python

{% sol %}
```pwsh
scoop install main/dotnet-sdk
...
```
{% endsol %}

**3.-** La forma més fàcil de tenir un entorn de desenvolupament personalitzat en qualsevol ordinador és crear el teu script en {% link "/windows/powershell/" %}.

Crea un script amb el nom `dev.ps1`.

{% sol %}

```pwsh
# Install scoop

if (-Not(Get-Command "scoop" -ErrorAction SilentlyContinue)) { 
    Invoke-RestMethod -Uri https://get.scoop.sh | Invoke-Expression
}


# Git

if (-Not(Get-Command "git" -ErrorAction SilentlyContinue)) {
    scoop install git
    # git config --global user.name "David de Mingo"
    # git config --global user.email "ddemingo@xtec.cat"
}

# NETBEANS

if (-Not(Get-Command "netbeans" -ErrorAction SilentlyContinue)) {

    scoop bucket add extras
    scoop bucket add java
    scoop install java/openjdk21
    scoop install extras/netbeans
}

# VS Code

if (-Not(Get-Command "code" -ErrorAction SilentlyContinue)) {

    scoop install extras/vscode
    code --install-extension "ms-vscode-remote.remote-wsl"
}
```

{% endsol %}


## TODO

<https://claytonerrington.com/blog/cleaning-up-scoop/>

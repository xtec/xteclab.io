---
title: Editar
description: Informació respecte l'edició de la documentació
---

## Introducció

Aquest lloc es genera de manera estàtica amb [11ty](https://www.11ty.dev/).

Si pertanys al [Grup XTEC](https://gitlab.com/xtec) de gitlab pots editar directament el projecte.

Els documents estan en format {% link "/web/doc/markdown/" %}

Tot el contingut formatiu està en aquest projecte: [https://gitlab.com/xtec/xtec.gitlab.io](https://gitlab.com/xtec/xtec.gitlab.io).

Tots els projectes que es fan servir en les diferents activitats estan allotjats a [https://gitlab.com/xtec/](https://gitlab.com/xtec/).

Per tenir una còpia de tots els projectes segueix les instruccions que s'expliquen a [https://gitlab.com/xtec/clone](https://gitlab.com/xtec/clone)


### Gitlab IDE

La manera més senzilla és editar els documents directament al Editor Web de {% link "/project/gitlab/" %}.

<a href="https://gitlab.com/-/ide/project/xtec/xtec.gitlab.io/edit/main/-/"><i class="bi bi-pencil-square"></i></a> <https://gitlab.com/-/ide/project/xtec/xtec.gitlab.io/edit/main/-/>

Tota la documentació està al directori `content`:

{% image "gitlab-web-ide.png" %}

Són fitxer markdown.


### Entorn local

Quan estas editant la documentació pots arrencar un navegador perquè generi de manera dinàmica el lloc web.

TODO acabar d'explicar !

### Windows

Instal.la {% link "/project/vscode/" %}.

Obre una consola de {% link "/windows/powershell/" %} i executa `npm run start` mitjançant l'script `node.ps1`:

```pwsh
> .\node.ps1
```

### Linux

Per executar el projecte en un entorn local:

```sh
$ ./node.sh npm install
$ ./node.sh npm run start
...
[1] [11ty] Watching...
[1] [11ty] Server at http://localhost:8080/
```

### Búsqueda

TODO 

### Search

[PageFind](https://pagefind.app/)

```sh
$ node.sh npx -y pagefind --site _site --serve
```

## Shortcodes

### Panel

Pots emmarcar contingut en un "panel" amb el "paired shortcode" `panel` tal com es mostra a continuació:

<pre class="language-markup">
<code class="language-markup">
{% raw %}
{% panel "Barcelona" %}

**Barcelona** és una ciutat i metròpoli a la costa mediterrània de la península Ibèrica.

{% endpanel %}
{% endraw %}
</code></pre>


{% panel "Barcelona" %}
**Barcelona** és una ciutat i metròpoli a la costa mediterrània de la península Ibèrica.
{% endpanel %}

### Link

Per enllaçar una pàgina utilitzar el shortcut `link`:


| | | |
|-|-|-|
| {% raw %}<code>{% link "/python/computation/" %}</code>{% endraw %} | {% link "/python/computation/" %} | Path absolut |
| {% raw %}<code>{% link "p:/python/computation/" %}`</code>{% endraw %} | {% link "p:/python/computation/" %} | Si `p:` a l'inici, s'afageix el titol de la página pare |
| {% raw %}<code>{% link "./about/" %}`</code>{% endraw %} | {% link "./about/" %} | Si `./` a l'inici, enllaç relatiu respecte la pàgina pare |
| {% raw %}<code>{% link "p:./about/" %}`</code>{% endraw %} | {% link "./about/" %} | Si `p:./` a l'inici, enllaç relatiu respecte la pàgina pare i s'afageix el titol de la página pare |


### Solució

Per amagar la solució utiliza el "paired shortcode" `sol` tal com es mostra a continuació:

<pre class="language-dockerfile">
<code class="language-dockerfile">
{% raw %}{% sol %}
```dockerfile
FROM alpine:latest
RUN apk update && apk add nmap
ENTRYPOINT ["nmap"]
CMD ["localhost"]
```
{% endsol %}{% endraw %}
</code></pre>

{% sol %}
```dockerfile
FROM alpine:latest
RUN apk update && apk add nmap
ENTRYPOINT ["nmap"]
CMD ["localhost"]
```
{% endsol %}

### Youtube

Per inserir un video de youtube:

<pre class="language-markup">
<code class="language-markup">
{% raw %}{% youtube "ef0V67SorBE?si=V-5vrnMDM7GxgPdR" %}{% endraw %}
</code></pre>

{% youtube "ef0V67SorBE?si=V-5vrnMDM7GxgPdR" %}

## Codi

Per marcar el codi..

En aquest enllaç tens tots els codis de llenguatges que pots utilitzar: [Supported Languages](https://prismjs.com/#supported-languages)

Per mostrar canvis en el codi afegeix el prefix `diff` al nom del llenguatge, i utilitza `+` o `-` al començament de cada linia per marcar que la linia s'ha afegit o s'ha eliminat.


{% raw %}
```diff-js
+function myFunction() {
   // …
-  return true;
 }
```
{% endraw %}



[Syntax Highlight](https://www.11ty.dev/docs/plugins/syntaxhighlight/)


#### Screen to Gif

<https://www.screentogif.com/>
<https://defkey.com/screentogif-2-41-1-shortcuts>

F7 	Start/Pause
	
F8 	Stop
	
F9 	Discard 

dpi 72
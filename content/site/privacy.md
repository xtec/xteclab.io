---
title: Privacidad y Términos
description: Política de Privacidad es informarte sobre qué datos recogemos, por qué los recogemos y cómo puedes actualizarlos, gestionarlos, exportarlos y eliminarlos.
---

**xtec.dev** está sujeto a la normativa de protección de datos de la Unión Europea.

Cuando utilizas nuestros servicios **no recogemos ninguna información**: no tratamos datos personales ni rastreamos información de navegación de los visitantes.
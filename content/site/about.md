---
title: Sobre nosotros
description: Nuestra misión es proporcionar formación gratuita, de calidad y accessible para todos.
---

Este sitio web está formado por diferentes actividades de aprendizaje que tienen como objeto formar profesionales cualificados en el mundo de la informática.

Todo el contenido tiene licencia [CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/deed.es) con el objetivo de crear un hub de conocimiento abierto y compartido.

Por tanto, usted es libre de compartir bajo los siguientes términos: atribución, no comercial y sin derivadas.

Si eres un profesional de la informática que quiere compartir parte de su conocimiento este es tu sitio!


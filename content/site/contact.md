---
title: Contacto
description: Hola, ¿cómo podemos ayudarte?
---


#### Ayuda y asistencia

¿Tienes alguna pregunta o necesitas informar de un problema?

Puedes contactar por correo electrónico con:

| | `@xtec.cat` | |
|-|-|-|
| **David de Mingo** | `ddemingo` | [Institut Provençana](https://proven.cat/intraweb/index.php/informatica-i-comunicacions) |
| **Miquel Amorós** | `mamoro10` | [Institut Provençana](https://proven.cat/intraweb/index.php/informatica-i-comunicacions) |




---
title: Programación de Servicios y Procesos (0490)
icon: kotlin-coroutine.webp
description: Este módulo profesional contiene la formación necesaria para la utilización de las capacidades ofrecidas por el sistema operativo para la gestión y desarrollo de aplicaciones con procesos concurrentes, el desarrollo de aplicaciones y servicios ejecutados a través de la red y la utilización de mecanismos de seguridad en el desarrollo de aplicaciones.
---

## Paral.lelisme i concurrència

{% pages ["/kotlin/thread/", "/kotlin/coroutines/" ] %}

## Sockets

{% pages ["/kotlin/socket/"] %}

## Serveis web

## Seguretat
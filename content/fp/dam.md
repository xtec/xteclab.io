---
title: Aplicaciones multiplataforma
icon: kotlin.png
description: El CFGS de Desarrollo de aplicaciones muliplataforma te proporciona la formación necesaria para desarrollar aplicaciones que se ejecutan en móbiles, servidores, ordenadores personales y cualquier otro dispositivo que se conecta a Internet.
---

### Módulos profesionales

#### Primero

{% pages ["/fp/bd/", "/fp/prog-kt/", "/fp/lmsgi/", "/fp/ed/"] %}

#### Segundo

{% pages ["/fp/ad/", "/fp/di/" ,"/fp/psp/"] %}


### Descripción

Desde 2019, {% link "/kotlin" %} es el lenguaje de programación preferido por Google para desarrollar aplicaciones Android.

Kotlin es utilizado por más del 60% de los desarrolladores profesionales de Android, y el 95% de las 1000 principales aplicaciones de Android contienen código Kotlin: [ Build Better Apps with Kotlin](https://developer.android.com/kotlin/build-better-apps)

Desde 2024, [Kotlin Multiplatform](https://kotlinlang.org/docs/multiplatform.html) es la plataforma recomendada por Google para compartir lógica entre Android, iOs y Web.

El equipo de Google Docs utiliza KMP para la lógica de negocio compartida, y el equipo de Google Worskpace ya está planificando extender su uso a toda la "suite" de aplicaciones.



---
title: Bioinformática
icon: python.webp
description: El CFGS de "Desarrollo de Aplicaciones Web (perfil professional Bioinformática)" te proporciona la formación necesaria para desarrollar aplicaciones de análisis de datos, "big data" e inteligencia artificial.
---


### Mòdulos profesionales

#### Primero

{% pages ["/fp/bd/", "/fp/prog-py/", "/fp/lmsgi/", "/fp/ed/", "/fp/dnf/"] %}

#### Segundo

{% pages ["/fp/dwes-py/", "/fp/ecb/"] %}


### Descripción

La bioinformática hoy en día se entiende de forma muy ancha, donde la representación de datos y análisis mediante herramientas informáticas son de gran valor para una gran variedad de tareas en la búsqueda de conocimientos en ciencias biológicas.

La bioinformática está cambiando los artefactos cognitivos fundamentales utilizados para representar, manipular y comunicar información biológica, en que se utilizan sistemas de información para descubrir relaciones complejas entre conjuntos de datos tanto empíricos como computacionales.

de análisis de datos, big data e inteligencia artificial, los que tienen competencia suficiente para llevarlo a cabo. 

A diferencia del CFGS DAW en que se desarrollan aplicaciones web orientadas a transacciones, en el CFGS DAW con perfil en bioinformática se desarrollan aplicaciones web orientadas a datos.

Este hecho implica arquitecturas, tecnologías y enfoques metodológicos distintos.


El CFGS de "Desarrollo de Aplicaciones Web con perfil (perfil professional Bioinformática)":

* Incorpora los módulos específicos de {% link "/fp/dnf/" %} y {% link "/fp/ecb/" %}

* Adpata los módulos {% link "/fp/prog-py/" %}, {% link "/fp/ed/" %} y {% link "/fp/dwes-py/" %}, a las exigencias tecnológicas concretas de los módulos específicos de bioinformática.


Aunque el contexto de trabajo sea la bioinformática, este ciclo formativo te forma de manera génerica en big data, análisis de datos e inteligencia artificial.


#### Aplicación Web

Cuando se habla de aplicación web, normalmente se piensa la aplicación clásica de un sitio web con un servidor y una base de datos.
Este concepto es erróneo, porque muchas aplicaciones web carecen de interfaz gráfica ni una base de datos al lado. Utilizan los protocolos web para comunicarse con otras aplicaciones en lugar de protocolos binarios específicos: el protocolo HTTP funciona, es confiable e interoperable. Estos son los motivos por los que desde el principio Amazon se construyó en torno a aplicaciones, o servicios, web.

Los problemas de la bioinformática son los del big data e inteligencia artificial: analizar grandes volúmenes de datos y extraer información. Esto se puede hacer con supercomputadores que utilizan las instituciones investigadoras (un problema de hardware) o con clusters de miles de computadores como hacen las empresas (un problema de software). 

#### Python

Cada entorno posee su lenguaje de programación, que es el que utiliza la industria y el mundo académico para desarrollar los entornos computacionales correspondientes.

{% link "/python/" %} es un lenguaje fácil de utilizar, muy productivo en código de alto nivel y uno de los que mejor rendimiento tiene en entornos de mucha demanda computacional: el motivo es su perfecta integración con librerías escritas en C y C++, y actualmente, en Rust.

En el ámbito del big data, el análisis de datos y la inteligencia artificial el lenguaje que más se utiliza es Python, y bioinformática forma parte de ese ecosistema. El motivo es que en este tipo de aplicaciones más del 95% del código que se ejecuta es código C, y Python es una interfaz para utilizar las librerías correspondientes a alto nivel.

Quizá Python sea un lenguaje "fácil", pero no así las librerías que se utilizan y la necesidad de conocimientos avanzados de programación, o la complejidad que comporta gestionar un proyecto de este tipo.

#### Arquitectura

Las aplicaciones "bioinformáticas" no están pensadas para SEO, sino para ejecutar computaciones de alto rendimiento, a diferencia de una aplicación web típica donde su enfoque es soportar un alto volumen de operaciones transaccionales y, en caso de necesitar una interfaz web, ésta es completamente "client-side".

Otro requisito es que normalmente se ejecutan bajo demanda, deben desplegarse deprisa, implican un gran volumen de datos y recursos "según demanda".









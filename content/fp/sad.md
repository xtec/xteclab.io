---
title: Seguretat i alta disponibilitat (0378)
icon: cluster.png
---

#### UF1 - Seguretat física, lògica i legislació

{% pages ["p:/linux/users/", "p:/linux/apparmor/", "p:/security/cryptography/", "/data/protection/"] %}


#### UF2 - Seguretat activa i accés remot

{% pages ["p:/network/ssh/", "p:/network/tls/", "p:/network/wireguard/", "p:/network/nmap/" ] %}

#### UF3 - Tallafocs i servidors intermediaris

{% pages ["p:/network/tcpdump/", "p:/linux/ufw/", "p:/linux/netfilter/", "p:/network/nginx/", "p:/security/suricata/", "p:/network/envoy/" ] %} 

#### UF4 - Alta disponibilitat

{% pages ["/linux/docker/", "/linux/kubernetes/", "/data/minio/" ] %}

---
title: Aplicaciones Web
icon: typescript.png
description: El CFGS de Desarrollo de aplicaciones web te proporciona la formación necesaria para desarrollar aplicaciones web tanto cliente como servidor.
---

### Módulos profesionales

#### Primero

{% pages ["/fp/bd/", "/fp/prog-ts/", "/fp/lmsgi/", "/fp/ed/"] %}
---
title: Programación - Kotlin (0485)
icon: kotlin.png
---

### Fonaments

{% pages ["/kotlin/computation/", "/kotlin/control-structures/", "/kotlin/function/", "/kotlin/extension-function/"] %}


### Input/Output

{% pages ["p:/kotlin/serialization/basics/","/kotlin/file/"] %}
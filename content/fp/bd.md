---
title: Bases de datos (0484)
icon: database.png
description: Este módulo profesional contiene la formación necesaria para desempeñar funciones relacionadas con la gestión de la información almacenada en bases de datos y el desarrollo de aplicaciones que acceden a bases de datos.
---

### Sqlite

{% pages ["/data/sqlite/table/", "/data/sqlite/select/", "/data/sqlite/group/", "/data/sqlite/relation/"] %}

### Postgres

{% pages ["/data/postgres/install/", "/data/postgres/neon/", "/data/postgres/row-level-security/"] %}

### MongoDB

{% pages ["/data/mongodb/basics/", "/data/mongodb/indexes/", "/data/mongodb/aggregations/", "/data/mongodb/transactions/", "/data/mongodb/access-control/", "/data/mongodb/atlas/"] %}



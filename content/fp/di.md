---
title: Desarrollo de interfaces (0488)
icon: compose.png
description: Este módulo profesional contiene la formación necesaria para el desarrollo de interfaces de usuario, la creación de informes, la preparación de aplicaciones para su distribución, la elaboración de los elementos de ayuda y la evaluación del funcionamiento de aplicaciones.
---

{% pages ["/kotlin/compose/composition/", "/kotlin/compose/recomposition/", "/kotlin/compose/navigation/", "/kotlin/compose/effects/"] %}
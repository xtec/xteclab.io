---
title: Entorns de desenvolupament (0487)
icon: development.png
--- 

#### Desenvolupament de programari

{% pages ["/project/vscode/", "/windows/wsl/", "/cloud/isard/", "/project/git/", "/project/gitlab/" , "/project/mermaid/" ] %}


#### Kotlin

{% pages ["/kotlin/gradle/"] %}

#### Python

{% pages [ "/python/test/", "/python/poetry/", "/python/refactoring/", "/python/debug/", "/python/profiling/"] %}

#### Projecte

{% pages ["/project/management/", "/project/scrum/", "/project/kanban/", "/project/user-story/"] %}





---
title: Seguretat informàtica
icon: security.png
---

## UF1 - Seguretat passiva

{% pages ["p:/linux/shell/", "p:/linux/users/", "p:/linux/apparmor/", "p:/windows/powershell/intro/", "p:/windows/users/" ] %}

## UF2 - Còpies de seguretat

{% pages ["p:/linux/archive/", "p:/network/ssh/", "p:/data/filebase/", "p:/data/minio/" ] %}

## UF3- Legislació de seguretat i protecció de dades

{% pages ["p:/data/protection/" ] %}

## UF4- Seguretat activa

{% pages ["p:/security/cryptography/", "p:/security/gpg/", "p:/security/certificate/", "p:/security/recovery/" ] %}

## UF5 - Tallafocs i monitoratge de xarxes

**Monitoratge de xarxes**

{% pages ["p:/linux/docker/container/", "p:/linux/docker/storage/", "p:/linux/docker/network/", "p:/linux/docker/compose/", "p:/network/nmap/", "p:/network/tcpdump/", "p:/network/arkime/", "p:/security/openvas/" ] %}

**Tallafocs**

{% pages ["p:/linux/ufw/", "p:/linux/netfilter/", "p:/security/suricata/" ] %}



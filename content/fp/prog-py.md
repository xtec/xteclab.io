---
title: Programación - Python (0485) 
icon: python.webp
--- 

### Fonaments

{% pages ["/python/computation/", "/python/algorithm/", "/python/sequence/", "/python/data/", "/python/function/", "/python/object/", "/python/typing/", "/python/module/", "/python/textual/"] %}

### Input/Output

{% pages ["/python/file/", "/python/http/", "/python/pydantic/"] %}



---
title: Disseny de nous fàrmacs (CO43)
icon: drug.png
description: La química informàtica utilitza ordinadors per descobrir relacions complexes entre conjunts de dades tant empíriques com computacionals.
---

## Activitats

### Molecula

{% pages ["/bio/molecule/matter/", "/bio/molecule/atom/", "/bio/molecule/molecule/", "/bio/molecule/molecule-representation/", "/bio/molecule/database/", "/bio/molecule/database-search/" ] %}

### Proteines

{% pages ["/bio/protein/protein/", "/bio/protein/uniprot/", "/bio/protein/protein-data-bank/"] %}


### Fàrmacs

{% pages ["/bio/drug/"] %}


### Disseny de fàrmacs

{% pages ["/bio/drug-design/molecular-similarity/", "/bio/drug-design/discovery-and-design/", "/bio/drug-design/docking/", "/bio/drug-design/machine-learning/"] %}

## Introducció

La química moderna va evolucionar a partir de les necessitats de "descobriment de fàrmacs" de la indústria farmacèutica.

La representació de dades i análisis mitjançant eines informàtiques són de gran valor per a una gran varietat de tasques en la recerca de coneixements en ciències químiques, i la quimioinormàtica és valuosa pels alumnes més enllà dels de les ciències farmacèutiques. 

De fet, podem anar un pas més enllà i afirmar que la quimioinformàtica està canviant els artefactes cognitius fonamentals utilitzats per representar, manipular i comunicar informació química, i en un món d'accés instantani a dades digitals interconnectades, una comprensió fonamental de la quimformàtica és una habilitat essencial per a qualsevol químic.

La química informàtica utilitza ordinadors per descobrir relacions complexes entre conjunts de dades tant empíriques com computacionals.






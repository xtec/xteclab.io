---
title: Eines computacionals en bioinformàtica (C044)
icon: bioinformatic.png
---

### Informàtica mèdica

#### Anàlisis de dades

{% pages ["/python/numpy/", "/python/matplotlib/", "/python/statistics/", "/python/pandas/", "/python/polars/", "/python/patito/", "/typescript/plotly/"] %}

### Ciències òmiques

#### Àcids nucleics i proteïnes

{% pages ["/bio/omic/nucleic-acid/", "/bio/omic/gene-expression/", "/bio/omic/sequence-formats/", "/bio/omic/entrez/", "/bio/omic/protein/"] %}

#### Análisis de seqüències

{% pages ["/bio/omic/sequence-alignment/", "/bio/omic/blast/", "/bio/omic/multiple-sequence-alignment/"] %}
---
title: Llenguatges de marques (0373)
icon: typescript.png
--- 

#### Document estàtic

{% pages ["/web/doc/text/", "/web/doc/link/", "/web/doc/box/", "/web/doc/selector/", "/web/doc/layout/", "/web/doc/form/" ] %}

#### Document dinàmic

{% pages ["/typescript/computation/", "/typescript/algorithm/","/typescript/sequence/", "/typescript/function/", "/typescript/object/", "/typescript/react/tsx", "/typescript/react/component/", "/typescript/react/router/"] %}

#### Document intercanvi informació

{% pages [ "/data/json/", "/typescript/json-schema/", "/typescript/react/state/", "/typescript/react/effect/" ] %}

#### MongoDB

{% pages ["p:/data/mongodb/basics/", "p:/typescript/mongodb/", "p:/data/mongodb/atlas/"] %}

#### Odoo

{% pages ["p:/web/app/odoo/user/", "p:/web/app/odoo/database/"] %}

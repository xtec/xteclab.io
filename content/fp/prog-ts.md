---
title: Programación - Typescript (0485)
icon: typescript.png
---

### Fundamentos

{% pages ["/typescript/computation/", "/typescript/algorithm/","/typescript/sequence/", "/typescript/function/", "/typescript/object/"] %}

### Datos

{% pages ["/typescript/kysely/"] %}
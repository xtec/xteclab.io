---
title: Acceso a datos (0486)
icon: database-access.webp
description: El módulo profesional «Acceso a Datos» contiene la formación necesaria para desarrollar aplicaciones y componentes que gestionan, acceden y utilizan bases de datos.
---

#### Serialización

{% pages ["p:/kotlin/data/", "p:/data/json/", "p:/kotlin/file/","p:/kotlin/gradle/"] %}

#### SQLite

{% pages ["p:/kotlin/sqldelight/basics", "p:/kotlin/sqldelight/android/"] %}

#### Postgres

{% pages ["p:/kotlin/postgres/", "p:/data/postgres/row-level-security/"] %}

#### MongoDB

{% pages ["p:/data/mongodb/access-control/", "p:/kotlin/mongodb/", "p:/kotlin/compose/mongodb/"] %}


#### Supabase














const fs = require('fs');
const path = require("path");
const { TRUE } = require('sass');
const { v4: uuidv4 } = require('uuid');

module.exports = function (config) {

  config.addAsyncShortcode("breadcrumb", async function () {
    return breadcrumb(this.ctx)
  })

  config.addPairedShortcode("panel", function (content, title) {


    html = `\n<div class="border border-4 border-secondary rounded rounded-4 p-3 mt-4 mb-4">\n`
    //html = `<div class="card mt-5 mb-4"><div class="card-header">${header}</div><div class="card-body"><p class="card-text">\n\n`
    if (title) {
      html += `<p class="fs-5 fw-bold border-dark text-center border-bottom border-2">${title}</p>\n\n`
    }
    html += content
    html += "</div>"

    return html

  });

  // TODO emoji que funcioni
  config.addAsyncShortcode("emoji", async function (emoji) {
    return `<p class="fs-1 text-center">${emoji}</p>`
  })

  config.addAsyncShortcode("frame", async function (src) {
    return frame(this.ctx, src)
  })


  config.addAsyncShortcode("image", async function (src) {
    return image(this.ctx, src)
  })

  config.addAsyncShortcode("link", async function (page) {
    const result = await getLink(this.ctx, page)
    return result.link
  });

  config.addAsyncShortcode("pages", async function (pages) {
    return renderPages(this.ctx, pages)
  });

  config.addPairedShortcode("sol", function (content) {

    const id = uuidv4()

    html = `<p class="d-inline-flex gap-1"><button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#${id}" aria-expanded="false" aria-controls="${id}">Solució</button></p><div class="collapse" id="${id}"><div class="card card-body">\n\n`
    html += content
    html += "\n\n</div></div>"

    return html

  });

  config.addAsyncShortcode("title", async function () {
    return title(this.ctx)
  });

  config.addAsyncShortcode("youtube", async function imageShortCode(id) {

    return `<div class="text-center mt-5 mb-4">
<iframe width="560" height="315" src="https://www.youtube.com/embed/${id}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>`
  });
};





async function frame(ctx, src) {
  return `<div class="embed-responsive embed-responsive-16by9 border border-3 border-info rounded p-2 mt-4 mb-4"><p class="text-center"><a href="${src}" target="_blank">Link</a></p><iframe class="embed-responsive-item" src="${src}" width="100%" height="100%" allowfullscreen frameborder="0"></iframe></div>`
}

async function breadcrumb(ctx) {
  let url = ctx.page.url
  let pages = ctx.collections.all.filter((page) => url.startsWith(page.url))

  pages = pages.sort(function (a, b) {
    if (a.url.length > b.url.length) return 1; return -1
  })

  let html = `<nav style="--bs-breadcrumb-divider: ' > ';" aria-label="breadcrumb"><ol class="breadcrumb">`
  for (const page of pages) {
    html += `<li class="breadcrumb-item active" aria-current="page"><a href="${page.url}">${page.data.title}</a></li>`
  }
  html += "</ol></nav>"

  return html
}

async function image(ctx, src) {
  //return properties(ctx.page)

  let input = path.join(ctx.page.inputPath, '../')
  input = path.join(input, src)

  if (!fs.existsSync(input)) {
    throw new Error(`${ctx.page.filePathStem}: Image not found '${src}'`)
  }

  let output = path.join(ctx.page.outputPath, '../')
  output = path.join(output, src)
  output = path.join(ctx.eleventy.env.root, output)

  // TODO verificar si input és més nou que output

  let directory = path.join(output, '../')
  if (!fs.existsSync(directory)) {
    fs.mkdirSync(directory, { recursive: true })
  }

  fs.copyFile(input, output, (err) => { if (err) throw err })

  let url = path.join(ctx.page.url, src)

  return `<div class="row mt-4 mb-4 justify-content-center"><div class="col col-lg-9"><a href="${url}" class="image-link" target="_blank"><image src="${url}" class="img-fluid"/></a></div></div>`;

}


async function getLink(ctx, path, streched) {

  let url = path

  if (!url.endsWith("/")) {
    //console.log(`[xtec] WARN ${ctx.page.url} Link has to end with /: ${path}`)
    url = url + "/"
  }

  parent = false
  if (url.startsWith("p:")) {
    parent = true
    url = url.slice(2)
  }


  if (!url.startsWith("/")) {
    pageUrl = ctx.page.url
    if (url.startsWith("./")) {
      url = url.slice(2)
      idx = pageUrl.lastIndexOf("/", pageUrl.length - 2)
      if (idx != 0) {
        pageUrl = pageUrl.substring(0, idx + 1)
      }
    }
    url = pageUrl + url
  }

  const page = ctx.collections.all.find((page) => page.url == url)

  if (page == null)
    throw new Error(`Page not found ${url}`)

  var name = page.data.title

  if (parent) {
    idx = url.lastIndexOf("/", url.length - 2)
    if (idx != 0) {
      const urlp = url.substring(0, idx + 1)
      const refp = ctx.collections.all.find((page) => page.url == urlp)
      name = `${refp.data.title} - ${name}`
    }
  }

  // TODO s'ha de millorar
  if (streched)
    return { link: `<a href="${url}" class="stretched-link">${name}</a>`, page: page } 

  return { link: `<a href="${url}">${name}</a>`, page: page }
}

async function renderPages(ctx, pages) {

  let html = ''
  // this.page.inputPath
  //Object.keys(this.ctx).forEach((prop)=> html += `<p>${prop}</p>`);
  //this.ctx.collections.all.forEach((page) => html += `<p>${page.url}</p>` )

  for (const path of pages) {

    const result = await getLink(ctx, path, true)

    const url = getUrl(ctx, path)
    const page = getPage(ctx, url)

    html += '<div class="row position-relative mt-4 p-1 border border-1 border-secondary rounded"><div class="col-1">'
    let icon = page.data.icon
    if (icon != undefined) {
      icon = `/asset/icon/${icon}`
      const url = await getImageUrl(ctx.eleventy.env.root, page, icon)
      html += `<img src="${url}" class="img-fluid"/>`
    }
    html += '</div><div class="col-10"><span>'
    html += result.link
    html += '</span><p>'
    html += result.page.data.description
    html += '</p></div></div>'
  }

  return html
}

async function title(ctx) {

  let url = ctx.page.url
  let pages = ctx.collections.all.filter((page) => url.startsWith(page.url))

  pages = pages.sort(function (a, b) {
    if (a.url.length > b.url.length) return 1; return -1
  })


  root = pages.shift()
  if (pages.length == 1) {
    return root.data.title
  }

  const title = pages.map((page) => page.data.title).join(" - ")

  return title
}


// UTILS

async function getImageUrl(root, page, src) {
  //return properties(ctx.page)

  let input
  if (src.startsWith("/")) {
    // TODO get content path from ????
    input = path.join("./content", src)
  } else {
    input = path.join(page.inputPath, '../')
    input = path.join(input, src)
  }

  // If path without extension we have to find the file that match ...
  /*
  if (path.extname(input) == "") {
    input = input + "."
    const parent = getParentPath(input)
    const imageFile = fs.readdirSync(parent).find(fileName => input.startsWith(fileName))
    throw new Error(`${input}: ${imageFile}`)
  }*/

  if (!fs.existsSync(input)) {
    throw new Error(`${page.filePathStem}: Image not found '${src}'`)
  }

  let output = path.join(page.outputPath, '../')
  output = path.join(output, src)
  output = path.join(root, output)

  // TODO verificar si input és més nou que output

  let directory = path.join(output, '../')
  if (!fs.existsSync(directory)) {
    fs.mkdirSync(directory, { recursive: true })
  }

  fs.copyFile(input, output, (err) => { if (err) throw err })

  let url = path.join(page.url, src)

  return url
}


function getPage(ctx, url) {

  const page = ctx.collections.all.find((page) => page.url == url)

  if (page == null)
    throw new Error(`Page not found ${url}`)

  return page
}

function getParentPath(path) {
  idx = path.lastIndexOf("\\", path.length - 2)
  if (idx == -1)
    return "\\"

  return path.substring(0, idx + 1)
}

function getUrl(ctx, path) {

  let url = path

  if (!url.endsWith("/")) {
    //console.log(`[xtec] WARN ${ctx.page.url} Link has to end with /: ${path}`)
    url = url + "/"
  }

  if (url.startsWith("p:")) {
    url = url.slice(2)
  }


  if (!url.startsWith("/")) {
    pageUrl = ctx.page.url
    if (url.startsWith("./")) {
      url = url.slice(2)
      idx = pageUrl.lastIndexOf("/", pageUrl.length - 2)
      if (idx != 0) {
        pageUrl = pageUrl.substring(0, idx + 1)
      }
    }
    url = pageUrl + url
  }

  return url
}


function printProperties(object) {
  let html = '<table class="table">'
  Object.keys(object).forEach((prop) => html += `<tr><td>${prop}</td><td>${object[prop]}</td></tr>`)
  html += "</table>"
  //this.ctx.collections.all.forEach((page) => html += `<p>${page.url}</p>` )
  return html
}



#!/bin/bash

if [ -z "$1" ]; then
  set -- "${1:-sh}"
fi

docker run --rm  -it --name xtec -p 8080:8080 --mount type=bind,source="$(pwd)",target=/site -w /site node:22-alpine "$@"


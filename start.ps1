if (-Not(Get-Command "scoop" -ErrorAction SilentlyContinue)) { 
    Write-Host "Installing scoop"
    Invoke-RestMethod -Uri https://get.scoop.sh | Invoke-Expression
  }

if (-Not(Get-Command node -ErrorAction SilentlyContinue)) {
    scoop install nodejs-lts
}

if (-Not(Test-Path "node_modules")) {
    npm install
}

npm run start